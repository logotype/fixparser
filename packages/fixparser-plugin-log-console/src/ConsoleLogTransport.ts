import type { ILogTransporter, LogMessage } from 'fixparser-common';

/**
 * Logger output format options.
 *
 * - 'console': Output log in plain text format
 * - 'json': Output log in JSON format
 *
 * @public
 */
export type ConsoleFormat = 'console' | 'json';

/**
 * A LogTransporter implementation for logging to the console.
 * It supports both text (console) and JSON formats.
 */
export class ConsoleLogTransport implements ILogTransporter {
    private format: ConsoleFormat;

    constructor({ format = 'json' }: { format: ConsoleFormat }) {
        this.format = format;
    }

    /**
     * Configures the format for console logging (either 'console' for text or 'json').
     */
    configure(config: { format: 'console' | 'json' }): void {
        this.format = config.format || 'json';
    }

    /**
     * Sends the log message to the console in the configured format.
     */
    async send(log: LogMessage): Promise<void> {
        if (this.format === 'json') {
            console.log(JSON.stringify(log));
        } else {
            const { name, id, message, level, ...additionalProperties } = log;
            const kv = Object.entries(additionalProperties).map(([key, value]) => `${key}: ${value}`);
            let logMessage = '';
            if (name) {
                logMessage += `${name} `;
            }
            logMessage += `${id}: ${message}`;
            console.log(logMessage, kv.join(', '));
        }
    }

    /**
     * Flushes the log buffer (if any buffering mechanism exists).
     */
    async flush(): Promise<void> {
        // No flushing needed for console transport
    }

    /**
     * Closes the transport (not needed for console, but keeping the method for consistency).
     */
    async close(): Promise<void> {
        // No close logic needed for console transport
    }

    /**
     * Returns the status of the transport (always "connected" for console).
     */
    status(): string {
        return 'connected';
    }
}
