import type { LogMessage } from 'fixparser-common';
import { type ConsoleFormat, ConsoleLogTransport } from '../src/ConsoleLogTransport';

describe('ConsoleLogTransport', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should log an info message when level is info', () => {
        const consoleTransport = new ConsoleLogTransport({ format: 'console' as ConsoleFormat });
        console.log = jest.fn();

        const message: LogMessage = {
            name: 'TestLogger',
            id: 'mock-uuid',
            level: 'info',
            message: 'This is an info message',
            timestamp: 1629064307365,
        };
        consoleTransport.send(message);

        expect(console.log).toHaveBeenCalledWith(
            'TestLogger mock-uuid: This is an info message',
            'timestamp: 1629064307365',
        );
    });

    it('should log a warning message in json format', () => {
        const consoleTransport = new ConsoleLogTransport({ format: 'json' as ConsoleFormat });
        console.warn = jest.fn();

        const message: LogMessage = {
            name: 'TestLogger',
            id: 'mock-uuid',
            level: 'warn',
            message: 'This is a warning message',
            timestamp: 1629064307365,
        };
        consoleTransport.send(message);

        expect(console.log).toHaveBeenCalledWith(
            JSON.stringify({
                name: 'TestLogger',
                id: 'mock-uuid',
                level: 'warn',
                message: 'This is a warning message',
                timestamp: 1629064307365,
            }),
        );
    });
});
