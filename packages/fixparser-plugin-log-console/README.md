# fixparser-plugin-log-console

The `fixparser-plugin-log-console` plugin provides an easy way to log messages to the console in either a human-readable text or JSON format. It is designed to work seamlessly with the `FIXParser` TypeScript library, making it ideal for logging FIX message parsing activities.

## Features
- Output logs in either plain text or JSON format.
- Supports dynamic format configuration after initialization.
- Simple integration with `FIXParser` logging system.

## Installation

To install the plugin, ensure you have `FIXParser` installed, then run the following command:

```bash
npm install fixparser-console-log
```

## Usage

```typescript
import { ConsoleLogTransport } from 'fixparser-console-log';
import { Logger } from 'fixparser';  // Assuming Logger is part of FIXParser

// Create an instance of ConsoleLogTransport with the desired format
const consoleLogTransport = new ConsoleLogTransport({ format: 'json' });

// Add the transport to the logger
const logger = new Logger({ name: 'TestLogger', level: 'info', transport: consoleLogTransport });

```

## Configuration Options

The `ConsoleLogTransport` constructor accepts the following option:

- **`format`**: Specifies the output format of the logs. Choose from:
    - `'console'` (text format)
    - `'json'` (structured JSON format)

## License
This project is licensed under a Commercial License - see the [LICENSE](https://gitlab.com/logotype/fixparser/-/blob/dev/LICENSE.md) file for details.