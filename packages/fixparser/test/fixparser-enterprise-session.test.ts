import { FIXParser } from '../src/FIXParser';
import { FIXServer } from '../src/FIXServer';
import { Logger } from '../src/logger/Logger';

import { MessageBuffer } from 'fixparser-common';
import { Field } from '../src/fields/Field';
import { Message } from '../src/message/Message';
import { handleLogon } from '../src/session/SessionLogon';
import { timestamp } from '../src/util/util';

import { EncryptMethod } from '../src/fieldtypes';
import { Field as FieldType } from '../src/fieldtypes/Field';
import { Message as MessageType } from '../src/fieldtypes/Message';
import { heartBeat } from '../src/messagetemplates/MessageTemplates';
import { handleLogout } from '../src/session/SessionLogout';
import { handleResendRequest } from '../src/session/SessionResendRequest';
import { handleSequenceReset } from '../src/session/SessionSequenceReset';
import { handleTestRequest } from '../src/session/SessionTestRequest';

jest.mock('../src/FIXServer');
const mockFIXServer = FIXServer as jest.MockedClass<typeof FIXServer>;

const mockLogger = Logger as jest.MockedClass<typeof Logger>;
mockFIXServer.prototype.logger = new mockLogger();
mockLogger.prototype.log = jest.fn();

mockFIXServer.prototype.messageStoreIn = {
    _seqNum: 1,
    getNextMsgSeqNum: jest.fn(function () {
        return this._seqNum;
    }),
    setNextMsgSeqNum: jest.fn(function (seqNum) {
        this._seqNum = seqNum;
        return this._seqNum;
    }),
};

let mockLoggerWarning: jest.Mock;

const mockSend = jest.fn();
mockFIXServer.prototype.send = mockSend;

const mockGetNextTargetMsgSeqNum = jest.fn().mockReturnValue(1000);
mockFIXServer.prototype.getNextTargetMsgSeqNum = mockGetNextTargetMsgSeqNum;

jest.mock('../src/FIXParser');
const mockFIXParser = FIXParser as jest.MockedClass<typeof FIXParser>;
mockFIXServer.prototype.fixParser = new mockFIXParser();

jest.mock('../src/messagetemplates/MessageTemplates', () => ({
    sequenceReset: jest.fn().mockImplementation((parser, seqNo, newSeqNo, gapFillFlag) => ({
        description: 'a sequencereset message',
        seqNo,
        newSeqNo,
        gapFillFlag,
        encode: jest.fn(),
    })),
    heartBeat: jest.fn().mockReturnValue({ encode: jest.fn(), description: 'a heartbeat message' }),
}));

describe('FIXParser', () => {
    describe('Session', () => {
        it('Handle Logon (FIXServer)', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIXT.1.1';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;
            mockFIXServerInstance.messageStoreIn.setNextMsgSeqNum(1);

            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 0),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);

            expect(mb.size()).toEqual(3);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Logon),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.ResetSeqNumFlag, 'Y'),
                new Field(FieldType.EncryptMethod, EncryptMethod.None),
                new Field(FieldType.HeartBtInt, 112233),
            );
            const result: boolean = handleLogon(mockFIXServerInstance, mb, message);
            expect(result).toBeTruthy();
            expect(mockFIXServerInstance.fixVersion).toEqual(TEST_FIX_VERSION);
            expect(mockFIXServerInstance.fixParser.fixVersion).toEqual(TEST_FIX_VERSION);
            expect(mockFIXServerInstance.fixParser.heartBeatInterval).toEqual(112233);
            expect(mockFIXServerInstance.heartBeatInterval).toEqual(112233);
            expect(mb.size()).toEqual(3);
        });

        it('Handle Logout', () => {
            const fixServer = new FIXServer({ logging: false });

            fixServer.socket = jest.fn();

            const msg = new Message();
            const spyCreateMessage = jest.spyOn(fixServer, 'createMessage').mockReturnValue(msg);
            const spySend = jest.spyOn(fixServer, 'send');

            const SENDER = 'xyz';
            const TARGET = 'abc';
            fixServer.parserName = 'FIXServer';
            fixServer.protocol = 'tcp';
            fixServer.sender = TARGET;
            fixServer.target = SENDER;
            const message = new Message(
                'FIX.0.1',
                new Field(FieldType.BeginString, 'FIX.0.1'),
                new Field(FieldType.MsgType, MessageType.Logout),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
                new Field(FieldType.TargetCompID, TARGET),
            );
            handleLogout(fixServer, message);
            expect(spyCreateMessage).toHaveBeenCalled();
            expect(spySend).toHaveBeenCalledWith(msg);
            expect(fixServer.stopHeartbeat).toHaveBeenCalled();
            expect(fixServer.close).toHaveBeenCalled();
        });

        it('Handle Resend Request - Mixed admin/session messages', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.NewOrderSingle),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ExecutionReport),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 1),
                new Field(FieldType.EndSeqNo, 3),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(3);

            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 1,
                    newSeqNo: 1,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[1]).toContainEqual(
                expect.objectContaining({ messageSequence: 2 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[2]).toContainEqual(
                expect.objectContaining({ messageSequence: 3 }),
            );
        });

        it('Handle Resend Request - Mixed admin/session messages + GapFill missing', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;
            mockFIXServerInstance.logger = {
                log: jest.fn(),
                logWarning: jest.fn(),
            };
            mockLoggerWarning = mockFIXServerInstance.logger.logWarning;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.NewOrderSingle),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ExecutionReport),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 1),
                new Field(FieldType.EndSeqNo, 8),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockLoggerWarning).toHaveBeenCalledWith({
                message:
                    'FIXServer (TCP): -- Could not find message with sequence 5. Sending SequenceReset-GapFill from 5 to 8...',
            });
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(5);
            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 1,
                    newSeqNo: 1,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[1]).toContainEqual(
                expect.objectContaining({ messageSequence: 2 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[2]).toContainEqual(
                expect.objectContaining({ messageSequence: 3 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[3]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 4,
                    newSeqNo: 4,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[4]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 5,
                    newSeqNo: 8,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.setNextTargetMsgSeqNum.mock.calls[0][0]).toEqual(9);
        });

        it('Handle Resend Request - Mixed admin/session messages (EndSeqNo<0>)', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;
            mockFIXServerInstance.logger = {
                log: jest.fn(),
                logWarning: jest.fn(),
            };
            mockLoggerWarning = mockFIXServerInstance.logger.logWarning;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.NewOrderSingle),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ExecutionReport),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 1),
                new Field(FieldType.EndSeqNo, 0),
            );
            expect(mockFIXServerInstance.getNextTargetMsgSeqNum).toHaveBeenCalledTimes(0);
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockLoggerWarning).toHaveBeenCalledWith({
                message:
                    'FIXServer (TCP): -- Could not find message with sequence 5. Sending SequenceReset-GapFill from 5 to 999...',
            });
            expect(mockFIXServerInstance.getNextTargetMsgSeqNum).toHaveBeenCalledTimes(2);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(5);
            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 1,
                    newSeqNo: 1,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[1]).toContainEqual(
                expect.objectContaining({ messageSequence: 2 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[2]).toContainEqual(
                expect.objectContaining({ messageSequence: 3 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[3]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 4,
                    newSeqNo: 4,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[4]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 5,
                    newSeqNo: 999,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.setNextTargetMsgSeqNum.mock.calls[0][0]).toEqual(1000);
        });

        it('Handle Resend Request - Only session messages', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.NewOrderSingle),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ExecutionReport),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Confirmation),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.News),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 1),
                new Field(FieldType.EndSeqNo, 3),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(3);

            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({ messageSequence: 1 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[1]).toContainEqual(
                expect.objectContaining({ messageSequence: 2 }),
            );
            expect(mockFIXServerInstance.send.mock.calls[2]).toContainEqual(
                expect.objectContaining({ messageSequence: 3 }),
            );
        });

        it('Handle Resend Request - Only admin messages', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Logon),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.TestRequest),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 1),
                new Field(FieldType.EndSeqNo, 3),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(2);

            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 1,
                    newSeqNo: 1,
                    gapFillFlag: 'Y',
                }),
            );
            expect(mockFIXServerInstance.send.mock.calls[1]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 2,
                    newSeqNo: 2,
                    gapFillFlag: 'Y',
                }),
            );
        });

        it('Handle Resend Request - Single admin message', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Logon),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.TestRequest),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 4),
                new Field(FieldType.EndSeqNo, 4),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(1);

            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({
                    description: 'a sequencereset message',
                    seqNo: 4,
                    newSeqNo: 4,
                    gapFillFlag: 'Y',
                }),
            );
        });

        it('Handle Resend Request - Single session message', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.0.0';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            // Admin message - filtered out and replaced by a SequenceReset
            const hb1: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Logon),
                new Field(FieldType.MsgSeqNum, 1),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb2: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Heartbeat),
                new Field(FieldType.MsgSeqNum, 2),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Session message
            const hb3: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.Confirmation),
                new Field(FieldType.MsgSeqNum, 3),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );
            // Admin message - filtered out and replaced by a SequenceReset
            const hb4: Message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.MarketDataSnapshotFullRefresh),
                new Field(FieldType.MsgSeqNum, 4),
                new Field(FieldType.SenderCompID, SENDER),
                new Field(FieldType.TargetCompID, TARGET),
                new Field(FieldType.SendingTime, timestamp(new Date(), new mockLogger())),
            );

            const mb = new MessageBuffer<Message>();
            mb.add(hb1);
            mb.add(hb2);
            mb.add(hb3);
            mb.add(hb4);

            expect(mb.size()).toEqual(4);

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.ResendRequest),
                new Field(FieldType.BeginSeqNo, 3),
                new Field(FieldType.EndSeqNo, 3),
            );
            handleResendRequest(mockFIXServerInstance, mb, message);
            expect(mockFIXServerInstance.send).toHaveBeenCalledTimes(1);

            expect(mockFIXServerInstance.send.mock.calls[0]).toContainEqual(
                expect.objectContaining({ messageSequence: 3 }),
            );
        });

        it('Handle Sequence Reset (FIXServer)', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.4.9';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.MsgType, MessageType.SequenceReset),
                new Field(FieldType.NewSeqNo, 8899),
            );
            handleSequenceReset(mockFIXServerInstance, message);
            expect(mockFIXServerInstance.messageStoreIn.getNextMsgSeqNum()).toEqual(8898);
        });

        it('Handle Test Request (FIXServer)', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.4.9';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXServer';
            mockFIXServerInstance.protocol = 'tcp';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.TestReqID, 'This is the test request...'),
            );

            handleTestRequest(mockFIXServerInstance, message);
            expect(heartBeat).toHaveBeenCalledWith(
                mockFIXServerInstance,
                expect.objectContaining({ tag: FieldType.TestReqID, value: 'This is the test request...' }),
            );
            expect(mockFIXServerInstance.send).toHaveBeenCalledWith(
                expect.objectContaining({ description: 'a heartbeat message' }),
            );
        });

        it('Handle Test Request (FIXParser)', () => {
            const mockFIXServerInstance = new mockFIXServer();

            const TEST_FIX_VERSION: string = 'FIX.4.9';
            const SENDER = 'abc';
            const TARGET = 'xyz';

            mockFIXServerInstance.parserName = 'FIXParser';
            mockFIXServerInstance.protocol = 'websocket';
            mockFIXServerInstance.sender = TARGET;
            mockFIXServerInstance.target = SENDER;

            const message = new Message(
                TEST_FIX_VERSION,
                new Field(FieldType.BeginString, TEST_FIX_VERSION),
                new Field(FieldType.TestReqID, 'This is the test request 2...'),
            );

            handleTestRequest(mockFIXServerInstance, message);
            expect(heartBeat).toHaveBeenCalledWith(
                mockFIXServerInstance,
                expect.objectContaining({ tag: FieldType.TestReqID, value: 'This is the test request 2...' }),
            );
            expect(mockFIXServerInstance.send).toHaveBeenCalledWith(
                expect.objectContaining({ description: 'a heartbeat message' }),
            );
        });
    });
});
