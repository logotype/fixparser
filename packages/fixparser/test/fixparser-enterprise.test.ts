import { randomInt } from 'node:crypto';
import { Socket } from 'node:net';
import { TLSSocket, connect } from 'node:tls';

import { HttpsProxyAgent } from 'https-proxy-agent';
import { ProxyAgent } from 'proxy-agent';
import WebSocket from 'ws';

import { FIXParser } from '../src/FIXParser';
import { mockLicense } from './setup';

jest.mock('proxy-agent');
const mockProxyAgent = ProxyAgent as jest.MockedClass<typeof ProxyAgent>;

jest.mock('ws');
const mockWebSocket = WebSocket as jest.MockedClass<typeof WebSocket>;

jest.mock('node:tls');
const mockTLSConnect = connect as jest.MockedFunction<typeof connect>;
const mockTLSSocket = TLSSocket as jest.MockedClass<typeof TLSSocket>;

const mockTLSSocketSetEncoding = jest.fn();
mockTLSSocket.prototype.setEncoding = mockTLSSocketSetEncoding;

const mockTLSSocketPipe = jest.fn().mockReturnValue(() => null);
mockTLSSocketPipe.mockReturnValue(new mockTLSSocket(new Socket(), {}));
mockTLSSocket.prototype.pipe = mockTLSSocketPipe;

mockTLSConnect.mockReturnValue(new mockTLSSocket(new Socket(), {}));

const RANDOMIZED_PORT = randomInt(9900, 12000);

describe('FIXParser', () => {
    describe('#connect', () => {
        it('without license', () => {
            mockLicense.mockReturnValue(false);
            const fixParserInstance1: FIXParser = new FIXParser();

            expect(() => {
                // Attempt to connect with a client
                fixParserInstance1.connect({
                    host: 'localhost',
                    port: RANDOMIZED_PORT,
                    protocol: 'websocket',
                });
            }).toThrow('Requires FIXParser Pro license');

            expect(mockLicense).toHaveBeenCalled();
            mockLicense.mockReturnValue(true);
        });

        it('using protocol prefix', () => {
            const fixParserInstance2: FIXParser = new FIXParser();
            const hostWithPrefix: string = 'wss://localhost';
            fixParserInstance2.connect({
                host: hostWithPrefix,
                port: RANDOMIZED_PORT,
                protocol: 'websocket',
            });
            expect(mockWebSocket).toHaveBeenCalledWith(`wss://localhost:${RANDOMIZED_PORT}`);
        });

        it('using proxy', () => {
            const fixParserInstance3: FIXParser = new FIXParser();
            const proxyURL: string = 'wss://localhostproxy';

            fixParserInstance3.connect({
                host: 'localhost',
                port: RANDOMIZED_PORT,
                protocol: 'websocket',
                proxy: new ProxyAgent({ httpsAgent: new HttpsProxyAgent(proxyURL) }),
            });

            expect(mockProxyAgent).toHaveBeenCalled();
            expect(mockWebSocket).toHaveBeenCalledWith(`ws://localhost:${RANDOMIZED_PORT}`, {
                agent: mockProxyAgent.mock.instances[0],
            });
        });

        it('using tls-tcp', () => {
            const fixParserInstance3: FIXParser = new FIXParser();

            fixParserInstance3.connect({
                host: 'localhost',
                port: RANDOMIZED_PORT,
                protocol: 'tls-tcp',
                tlsOptions: {
                    key: 'someKey',
                    cert: 'someCert',
                    rejectUnauthorized: false,
                },
            });

            expect(mockTLSSocketSetEncoding).toHaveBeenCalledWith('utf8');
            expect(mockTLSSocketSetEncoding).toHaveBeenCalledTimes(1);
            expect(mockTLSConnect).toHaveBeenCalledWith(
                RANDOMIZED_PORT,
                'localhost',
                {
                    host: 'localhost',
                    port: RANDOMIZED_PORT,
                    key: 'someKey',
                    cert: 'someCert',
                    rejectUnauthorized: false,
                },
                expect.any(Function),
            );
        });

        it('using tls-tcp with SNI', () => {
            const fixParserInstance3: FIXParser = new FIXParser();

            fixParserInstance3.connect({
                host: 'localhost',
                port: RANDOMIZED_PORT,
                protocol: 'tls-tcp',
                tlsOptions: {
                    key: 'someKey',
                    cert: 'someCert',
                    servername: 'fixgw01.router.example.com',
                },
            });

            expect(mockTLSSocketSetEncoding).toHaveBeenCalledWith('utf8');
            expect(mockTLSSocketSetEncoding).toHaveBeenCalledTimes(1);
            expect(mockTLSConnect).toHaveBeenCalledWith(
                RANDOMIZED_PORT,
                'localhost',
                {
                    cert: 'someCert',
                    key: 'someKey',
                    host: 'localhost',
                    port: RANDOMIZED_PORT,
                    servername: 'fixgw01.router.example.com',
                },
                expect.any(Function),
            );
        });
    });
});
