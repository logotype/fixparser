import { AllocGroupStatus } from '../../src/fieldtypes/AllocGroupStatus';

describe('AllocGroupStatus', () => {
    test('should have the correct values', () => {
        expect(AllocGroupStatus.Added).toBe(0);
        expect(AllocGroupStatus.Canceled).toBe(1);
        expect(AllocGroupStatus.Replaced).toBe(2);
        expect(AllocGroupStatus.Changed).toBe(3);
        expect(AllocGroupStatus.Pending).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocGroupStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocGroupStatus.Added,
            AllocGroupStatus.Canceled,
            AllocGroupStatus.Replaced,
            AllocGroupStatus.Changed,
            AllocGroupStatus.Pending,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocGroupStatus type', () => {
        const validate = (value: AllocGroupStatus) => {
            expect(Object.values(AllocGroupStatus)).toContain(value);
        };

        validate(AllocGroupStatus.Added);
        validate(AllocGroupStatus.Canceled);
        validate(AllocGroupStatus.Replaced);
        validate(AllocGroupStatus.Changed);
        validate(AllocGroupStatus.Pending);
    });

    test('should be immutable', () => {
        const ref = AllocGroupStatus;
        expect(() => {
            ref.Added = 10;
        }).toThrowError();
    });
});
