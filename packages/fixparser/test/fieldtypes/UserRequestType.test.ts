import { UserRequestType } from '../../src/fieldtypes/UserRequestType';

describe('UserRequestType', () => {
    test('should have the correct values', () => {
        expect(UserRequestType.LogOnUser).toBe(1);
        expect(UserRequestType.LogOffUser).toBe(2);
        expect(UserRequestType.ChangePasswordForUser).toBe(3);
        expect(UserRequestType.RequestIndividualUserStatus).toBe(4);
        expect(UserRequestType.RequestThrottleLimit).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UserRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UserRequestType.LogOnUser,
            UserRequestType.LogOffUser,
            UserRequestType.ChangePasswordForUser,
            UserRequestType.RequestIndividualUserStatus,
            UserRequestType.RequestThrottleLimit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UserRequestType type', () => {
        const validate = (value: UserRequestType) => {
            expect(Object.values(UserRequestType)).toContain(value);
        };

        validate(UserRequestType.LogOnUser);
        validate(UserRequestType.LogOffUser);
        validate(UserRequestType.ChangePasswordForUser);
        validate(UserRequestType.RequestIndividualUserStatus);
        validate(UserRequestType.RequestThrottleLimit);
    });

    test('should be immutable', () => {
        const ref = UserRequestType;
        expect(() => {
            ref.LogOnUser = 10;
        }).toThrowError();
    });
});
