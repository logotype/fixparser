import { MatchType } from '../../src/fieldtypes/MatchType';

describe('MatchType', () => {
    test('should have the correct values', () => {
        expect(MatchType.ExactMatchPlus4BadgesExecTime).toBe('A1');
        expect(MatchType.ExactMatchPlus4Badges).toBe('A2');
        expect(MatchType.ACTAcceptedTrade).toBe('M3');
        expect(MatchType.ExactMatchPlus2BadgesExecTime).toBe('A3');
        expect(MatchType.ACTDefaultTrade).toBe('M4');
        expect(MatchType.ExactMatchPlus2Badges).toBe('A4');
        expect(MatchType.ACTDefaultAfterM2).toBe('M5');
        expect(MatchType.ExactMatchPlusExecTime).toBe('A5');
        expect(MatchType.ACTM6Match).toBe('M6');
        expect(MatchType.StampedAdvisoriesOrSpecialistAccepts).toBe('AQ');
        expect(MatchType.A1ExactMatchSummarizedQuantity).toBe('S1');
        expect(MatchType.A2ExactMatchSummarizedQuantity).toBe('S2');
        expect(MatchType.A3ExactMatchSummarizedQuantity).toBe('S3');
        expect(MatchType.A4ExactMatchSummarizedQuantity).toBe('S4');
        expect(MatchType.A5ExactMatchSummarizedQuantity).toBe('S5');
        expect(MatchType.ExactMatchMinusBadgesTimes).toBe('M1');
        expect(MatchType.SummarizedMatchMinusBadgesTimes).toBe('M2');
        expect(MatchType.OCSLockedIn).toBe('MT');
        expect(MatchType.OnePartyTradeReport).toBe('1');
        expect(MatchType.TwoPartyTradeReport).toBe('2');
        expect(MatchType.ConfirmedTradeReport).toBe('3');
        expect(MatchType.AutoMatch).toBe('4');
        expect(MatchType.CrossAuction).toBe('5');
        expect(MatchType.CounterOrderSelection).toBe('6');
        expect(MatchType.CallAuction).toBe('7');
        expect(MatchType.Issuing).toBe('8');
        expect(MatchType.SystematicInternaliser).toBe('9');
        expect(MatchType.AutoMatchLastLook).toBe('10');
        expect(MatchType.CrossAuctionLastLook).toBe('11');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MatchType.ExactMatchPlus4BadgesExecTime,
            MatchType.ExactMatchPlus4Badges,
            MatchType.ACTAcceptedTrade,
            MatchType.ExactMatchPlus2BadgesExecTime,
            MatchType.ACTDefaultTrade,
            MatchType.ExactMatchPlus2Badges,
            MatchType.ACTDefaultAfterM2,
            MatchType.ExactMatchPlusExecTime,
            MatchType.ACTM6Match,
            MatchType.StampedAdvisoriesOrSpecialistAccepts,
            MatchType.A1ExactMatchSummarizedQuantity,
            MatchType.A2ExactMatchSummarizedQuantity,
            MatchType.A3ExactMatchSummarizedQuantity,
            MatchType.A4ExactMatchSummarizedQuantity,
            MatchType.A5ExactMatchSummarizedQuantity,
            MatchType.ExactMatchMinusBadgesTimes,
            MatchType.SummarizedMatchMinusBadgesTimes,
            MatchType.OCSLockedIn,
            MatchType.OnePartyTradeReport,
            MatchType.TwoPartyTradeReport,
            MatchType.ConfirmedTradeReport,
            MatchType.AutoMatch,
            MatchType.CrossAuction,
            MatchType.CounterOrderSelection,
            MatchType.CallAuction,
            MatchType.Issuing,
            MatchType.SystematicInternaliser,
            MatchType.AutoMatchLastLook,
            MatchType.CrossAuctionLastLook,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MatchType type', () => {
        const validate = (value: MatchType) => {
            expect(Object.values(MatchType)).toContain(value);
        };

        validate(MatchType.ExactMatchPlus4BadgesExecTime);
        validate(MatchType.ExactMatchPlus4Badges);
        validate(MatchType.ACTAcceptedTrade);
        validate(MatchType.ExactMatchPlus2BadgesExecTime);
        validate(MatchType.ACTDefaultTrade);
        validate(MatchType.ExactMatchPlus2Badges);
        validate(MatchType.ACTDefaultAfterM2);
        validate(MatchType.ExactMatchPlusExecTime);
        validate(MatchType.ACTM6Match);
        validate(MatchType.StampedAdvisoriesOrSpecialistAccepts);
        validate(MatchType.A1ExactMatchSummarizedQuantity);
        validate(MatchType.A2ExactMatchSummarizedQuantity);
        validate(MatchType.A3ExactMatchSummarizedQuantity);
        validate(MatchType.A4ExactMatchSummarizedQuantity);
        validate(MatchType.A5ExactMatchSummarizedQuantity);
        validate(MatchType.ExactMatchMinusBadgesTimes);
        validate(MatchType.SummarizedMatchMinusBadgesTimes);
        validate(MatchType.OCSLockedIn);
        validate(MatchType.OnePartyTradeReport);
        validate(MatchType.TwoPartyTradeReport);
        validate(MatchType.ConfirmedTradeReport);
        validate(MatchType.AutoMatch);
        validate(MatchType.CrossAuction);
        validate(MatchType.CounterOrderSelection);
        validate(MatchType.CallAuction);
        validate(MatchType.Issuing);
        validate(MatchType.SystematicInternaliser);
        validate(MatchType.AutoMatchLastLook);
        validate(MatchType.CrossAuctionLastLook);
    });

    test('should be immutable', () => {
        const ref = MatchType;
        expect(() => {
            ref.ExactMatchPlus4BadgesExecTime = 10;
        }).toThrowError();
    });
});
