import { PaymentScheduleType } from '../../src/fieldtypes/PaymentScheduleType';

describe('PaymentScheduleType', () => {
    test('should have the correct values', () => {
        expect(PaymentScheduleType.Notional).toBe(0);
        expect(PaymentScheduleType.CashFlow).toBe(1);
        expect(PaymentScheduleType.FXLinkedNotional).toBe(2);
        expect(PaymentScheduleType.FixedRate).toBe(3);
        expect(PaymentScheduleType.FutureValueNotional).toBe(4);
        expect(PaymentScheduleType.KnownAmount).toBe(5);
        expect(PaymentScheduleType.FloatingRateMultiplier).toBe(6);
        expect(PaymentScheduleType.Spread).toBe(7);
        expect(PaymentScheduleType.CapRate).toBe(8);
        expect(PaymentScheduleType.FloorRate).toBe(9);
        expect(PaymentScheduleType.NonDeliverableSettlPaymentDates).toBe(10);
        expect(PaymentScheduleType.NonDeliverableSettlCalculationDates).toBe(11);
        expect(PaymentScheduleType.NonDeliverableFXFixingDates).toBe(12);
        expect(PaymentScheduleType.SettlPeriodNotnl).toBe(13);
        expect(PaymentScheduleType.SettlPeriodPx).toBe(14);
        expect(PaymentScheduleType.CalcPeriod).toBe(15);
        expect(PaymentScheduleType.DividendAccrualRateMultiplier).toBe(16);
        expect(PaymentScheduleType.DividendAccrualRateSpread).toBe(17);
        expect(PaymentScheduleType.DividendAccrualCapRate).toBe(18);
        expect(PaymentScheduleType.DividendAccrualFloorRate).toBe(19);
        expect(PaymentScheduleType.CompoundingRateMultiplier).toBe(20);
        expect(PaymentScheduleType.CompoundingRateSpread).toBe(21);
        expect(PaymentScheduleType.CompoundingCapRate).toBe(22);
        expect(PaymentScheduleType.CompoundingFloorRate).toBe(23);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentScheduleType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentScheduleType.Notional,
            PaymentScheduleType.CashFlow,
            PaymentScheduleType.FXLinkedNotional,
            PaymentScheduleType.FixedRate,
            PaymentScheduleType.FutureValueNotional,
            PaymentScheduleType.KnownAmount,
            PaymentScheduleType.FloatingRateMultiplier,
            PaymentScheduleType.Spread,
            PaymentScheduleType.CapRate,
            PaymentScheduleType.FloorRate,
            PaymentScheduleType.NonDeliverableSettlPaymentDates,
            PaymentScheduleType.NonDeliverableSettlCalculationDates,
            PaymentScheduleType.NonDeliverableFXFixingDates,
            PaymentScheduleType.SettlPeriodNotnl,
            PaymentScheduleType.SettlPeriodPx,
            PaymentScheduleType.CalcPeriod,
            PaymentScheduleType.DividendAccrualRateMultiplier,
            PaymentScheduleType.DividendAccrualRateSpread,
            PaymentScheduleType.DividendAccrualCapRate,
            PaymentScheduleType.DividendAccrualFloorRate,
            PaymentScheduleType.CompoundingRateMultiplier,
            PaymentScheduleType.CompoundingRateSpread,
            PaymentScheduleType.CompoundingCapRate,
            PaymentScheduleType.CompoundingFloorRate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentScheduleType type', () => {
        const validate = (value: PaymentScheduleType) => {
            expect(Object.values(PaymentScheduleType)).toContain(value);
        };

        validate(PaymentScheduleType.Notional);
        validate(PaymentScheduleType.CashFlow);
        validate(PaymentScheduleType.FXLinkedNotional);
        validate(PaymentScheduleType.FixedRate);
        validate(PaymentScheduleType.FutureValueNotional);
        validate(PaymentScheduleType.KnownAmount);
        validate(PaymentScheduleType.FloatingRateMultiplier);
        validate(PaymentScheduleType.Spread);
        validate(PaymentScheduleType.CapRate);
        validate(PaymentScheduleType.FloorRate);
        validate(PaymentScheduleType.NonDeliverableSettlPaymentDates);
        validate(PaymentScheduleType.NonDeliverableSettlCalculationDates);
        validate(PaymentScheduleType.NonDeliverableFXFixingDates);
        validate(PaymentScheduleType.SettlPeriodNotnl);
        validate(PaymentScheduleType.SettlPeriodPx);
        validate(PaymentScheduleType.CalcPeriod);
        validate(PaymentScheduleType.DividendAccrualRateMultiplier);
        validate(PaymentScheduleType.DividendAccrualRateSpread);
        validate(PaymentScheduleType.DividendAccrualCapRate);
        validate(PaymentScheduleType.DividendAccrualFloorRate);
        validate(PaymentScheduleType.CompoundingRateMultiplier);
        validate(PaymentScheduleType.CompoundingRateSpread);
        validate(PaymentScheduleType.CompoundingCapRate);
        validate(PaymentScheduleType.CompoundingFloorRate);
    });

    test('should be immutable', () => {
        const ref = PaymentScheduleType;
        expect(() => {
            ref.Notional = 10;
        }).toThrowError();
    });
});
