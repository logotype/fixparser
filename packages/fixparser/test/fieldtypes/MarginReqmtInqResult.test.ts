import { MarginReqmtInqResult } from '../../src/fieldtypes/MarginReqmtInqResult';

describe('MarginReqmtInqResult', () => {
    test('should have the correct values', () => {
        expect(MarginReqmtInqResult.Successful).toBe(0);
        expect(MarginReqmtInqResult.InvalidOrUnknownInstrument).toBe(1);
        expect(MarginReqmtInqResult.InvalidOrUnknownMarginClass).toBe(2);
        expect(MarginReqmtInqResult.InvalidParties).toBe(3);
        expect(MarginReqmtInqResult.InvalidTransportTypeReq).toBe(4);
        expect(MarginReqmtInqResult.InvalidDestinationReq).toBe(5);
        expect(MarginReqmtInqResult.NoMarginReqFound).toBe(6);
        expect(MarginReqmtInqResult.MarginReqInquiryQualifierNotSupported).toBe(7);
        expect(MarginReqmtInqResult.UnauthorizedForMarginReqInquiry).toBe(8);
        expect(MarginReqmtInqResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarginReqmtInqResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarginReqmtInqResult.Successful,
            MarginReqmtInqResult.InvalidOrUnknownInstrument,
            MarginReqmtInqResult.InvalidOrUnknownMarginClass,
            MarginReqmtInqResult.InvalidParties,
            MarginReqmtInqResult.InvalidTransportTypeReq,
            MarginReqmtInqResult.InvalidDestinationReq,
            MarginReqmtInqResult.NoMarginReqFound,
            MarginReqmtInqResult.MarginReqInquiryQualifierNotSupported,
            MarginReqmtInqResult.UnauthorizedForMarginReqInquiry,
            MarginReqmtInqResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarginReqmtInqResult type', () => {
        const validate = (value: MarginReqmtInqResult) => {
            expect(Object.values(MarginReqmtInqResult)).toContain(value);
        };

        validate(MarginReqmtInqResult.Successful);
        validate(MarginReqmtInqResult.InvalidOrUnknownInstrument);
        validate(MarginReqmtInqResult.InvalidOrUnknownMarginClass);
        validate(MarginReqmtInqResult.InvalidParties);
        validate(MarginReqmtInqResult.InvalidTransportTypeReq);
        validate(MarginReqmtInqResult.InvalidDestinationReq);
        validate(MarginReqmtInqResult.NoMarginReqFound);
        validate(MarginReqmtInqResult.MarginReqInquiryQualifierNotSupported);
        validate(MarginReqmtInqResult.UnauthorizedForMarginReqInquiry);
        validate(MarginReqmtInqResult.Other);
    });

    test('should be immutable', () => {
        const ref = MarginReqmtInqResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
