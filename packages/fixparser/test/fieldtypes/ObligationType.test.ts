import { ObligationType } from '../../src/fieldtypes/ObligationType';

describe('ObligationType', () => {
    test('should have the correct values', () => {
        expect(ObligationType.Bond).toBe('0');
        expect(ObligationType.ConvertBond).toBe('1');
        expect(ObligationType.Mortgage).toBe('2');
        expect(ObligationType.Loan).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ObligationType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ObligationType.Bond,
            ObligationType.ConvertBond,
            ObligationType.Mortgage,
            ObligationType.Loan,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ObligationType type', () => {
        const validate = (value: ObligationType) => {
            expect(Object.values(ObligationType)).toContain(value);
        };

        validate(ObligationType.Bond);
        validate(ObligationType.ConvertBond);
        validate(ObligationType.Mortgage);
        validate(ObligationType.Loan);
    });

    test('should be immutable', () => {
        const ref = ObligationType;
        expect(() => {
            ref.Bond = 10;
        }).toThrowError();
    });
});
