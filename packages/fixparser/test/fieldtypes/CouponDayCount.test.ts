import { CouponDayCount } from '../../src/fieldtypes/CouponDayCount';

describe('CouponDayCount', () => {
    test('should have the correct values', () => {
        expect(CouponDayCount.OneOne).toBe(0);
        expect(CouponDayCount.ThirtyThreeSixtyUS).toBe(1);
        expect(CouponDayCount.ThirtyThreeSixtySIA).toBe(2);
        expect(CouponDayCount.ThirtyThreeSixtyM).toBe(3);
        expect(CouponDayCount.ThirtyEThreeSixty).toBe(4);
        expect(CouponDayCount.ThirtyEThreeSixtyISDA).toBe(5);
        expect(CouponDayCount.ActThreeSixty).toBe(6);
        expect(CouponDayCount.ActThreeSixtyFiveFixed).toBe(7);
        expect(CouponDayCount.ActActAFB).toBe(8);
        expect(CouponDayCount.ActActICMA).toBe(9);
        expect(CouponDayCount.ActActISMAUltimo).toBe(10);
        expect(CouponDayCount.ActActISDA).toBe(11);
        expect(CouponDayCount.BusTwoFiftyTwo).toBe(12);
        expect(CouponDayCount.ThirtyEPlusThreeSixty).toBe(13);
        expect(CouponDayCount.ActThreeSixtyFiveL).toBe(14);
        expect(CouponDayCount.NLThreeSixtyFive).toBe(15);
        expect(CouponDayCount.NLThreeSixty).toBe(16);
        expect(CouponDayCount.Act364).toBe(17);
        expect(CouponDayCount.ThirtyThreeSixtyFive).toBe(18);
        expect(CouponDayCount.ThirtyActual).toBe(19);
        expect(CouponDayCount.ThirtyThreeSixtyICMA).toBe(20);
        expect(CouponDayCount.ThirtyETwoThreeSixty).toBe(21);
        expect(CouponDayCount.ThirtyEThreeThreeSixty).toBe(22);
        expect(CouponDayCount.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CouponDayCount.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CouponDayCount.OneOne,
            CouponDayCount.ThirtyThreeSixtyUS,
            CouponDayCount.ThirtyThreeSixtySIA,
            CouponDayCount.ThirtyThreeSixtyM,
            CouponDayCount.ThirtyEThreeSixty,
            CouponDayCount.ThirtyEThreeSixtyISDA,
            CouponDayCount.ActThreeSixty,
            CouponDayCount.ActThreeSixtyFiveFixed,
            CouponDayCount.ActActAFB,
            CouponDayCount.ActActICMA,
            CouponDayCount.ActActISMAUltimo,
            CouponDayCount.ActActISDA,
            CouponDayCount.BusTwoFiftyTwo,
            CouponDayCount.ThirtyEPlusThreeSixty,
            CouponDayCount.ActThreeSixtyFiveL,
            CouponDayCount.NLThreeSixtyFive,
            CouponDayCount.NLThreeSixty,
            CouponDayCount.Act364,
            CouponDayCount.ThirtyThreeSixtyFive,
            CouponDayCount.ThirtyActual,
            CouponDayCount.ThirtyThreeSixtyICMA,
            CouponDayCount.ThirtyETwoThreeSixty,
            CouponDayCount.ThirtyEThreeThreeSixty,
            CouponDayCount.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CouponDayCount type', () => {
        const validate = (value: CouponDayCount) => {
            expect(Object.values(CouponDayCount)).toContain(value);
        };

        validate(CouponDayCount.OneOne);
        validate(CouponDayCount.ThirtyThreeSixtyUS);
        validate(CouponDayCount.ThirtyThreeSixtySIA);
        validate(CouponDayCount.ThirtyThreeSixtyM);
        validate(CouponDayCount.ThirtyEThreeSixty);
        validate(CouponDayCount.ThirtyEThreeSixtyISDA);
        validate(CouponDayCount.ActThreeSixty);
        validate(CouponDayCount.ActThreeSixtyFiveFixed);
        validate(CouponDayCount.ActActAFB);
        validate(CouponDayCount.ActActICMA);
        validate(CouponDayCount.ActActISMAUltimo);
        validate(CouponDayCount.ActActISDA);
        validate(CouponDayCount.BusTwoFiftyTwo);
        validate(CouponDayCount.ThirtyEPlusThreeSixty);
        validate(CouponDayCount.ActThreeSixtyFiveL);
        validate(CouponDayCount.NLThreeSixtyFive);
        validate(CouponDayCount.NLThreeSixty);
        validate(CouponDayCount.Act364);
        validate(CouponDayCount.ThirtyThreeSixtyFive);
        validate(CouponDayCount.ThirtyActual);
        validate(CouponDayCount.ThirtyThreeSixtyICMA);
        validate(CouponDayCount.ThirtyETwoThreeSixty);
        validate(CouponDayCount.ThirtyEThreeThreeSixty);
        validate(CouponDayCount.Other);
    });

    test('should be immutable', () => {
        const ref = CouponDayCount;
        expect(() => {
            ref.OneOne = 10;
        }).toThrowError();
    });
});
