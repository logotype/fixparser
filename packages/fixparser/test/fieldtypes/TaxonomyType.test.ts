import { TaxonomyType } from '../../src/fieldtypes/TaxonomyType';

describe('TaxonomyType', () => {
    test('should have the correct values', () => {
        expect(TaxonomyType.ISINOrAltInstrmtID).toBe('I');
        expect(TaxonomyType.InterimTaxonomy).toBe('E');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TaxonomyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TaxonomyType.ISINOrAltInstrmtID, TaxonomyType.InterimTaxonomy];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TaxonomyType type', () => {
        const validate = (value: TaxonomyType) => {
            expect(Object.values(TaxonomyType)).toContain(value);
        };

        validate(TaxonomyType.ISINOrAltInstrmtID);
        validate(TaxonomyType.InterimTaxonomy);
    });

    test('should be immutable', () => {
        const ref = TaxonomyType;
        expect(() => {
            ref.ISINOrAltInstrmtID = 10;
        }).toThrowError();
    });
});
