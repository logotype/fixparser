import { AdjustmentType } from '../../src/fieldtypes/AdjustmentType';

describe('AdjustmentType', () => {
    test('should have the correct values', () => {
        expect(AdjustmentType.ProcessRequestAsMarginDisposition).toBe(0);
        expect(AdjustmentType.DeltaPlus).toBe(1);
        expect(AdjustmentType.DeltaMinus).toBe(2);
        expect(AdjustmentType.Final).toBe(3);
        expect(AdjustmentType.CustomerSpecificPosition).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AdjustmentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AdjustmentType.ProcessRequestAsMarginDisposition,
            AdjustmentType.DeltaPlus,
            AdjustmentType.DeltaMinus,
            AdjustmentType.Final,
            AdjustmentType.CustomerSpecificPosition,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AdjustmentType type', () => {
        const validate = (value: AdjustmentType) => {
            expect(Object.values(AdjustmentType)).toContain(value);
        };

        validate(AdjustmentType.ProcessRequestAsMarginDisposition);
        validate(AdjustmentType.DeltaPlus);
        validate(AdjustmentType.DeltaMinus);
        validate(AdjustmentType.Final);
        validate(AdjustmentType.CustomerSpecificPosition);
    });

    test('should be immutable', () => {
        const ref = AdjustmentType;
        expect(() => {
            ref.ProcessRequestAsMarginDisposition = 10;
        }).toThrowError();
    });
});
