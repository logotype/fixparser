import { LegalConfirm } from '../../src/fieldtypes/LegalConfirm';

describe('LegalConfirm', () => {
    test('should have the correct values', () => {
        expect(LegalConfirm.DoesNotConsituteALegalConfirm).toBe('N');
        expect(LegalConfirm.LegalConfirm).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LegalConfirm.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [LegalConfirm.DoesNotConsituteALegalConfirm, LegalConfirm.LegalConfirm];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LegalConfirm type', () => {
        const validate = (value: LegalConfirm) => {
            expect(Object.values(LegalConfirm)).toContain(value);
        };

        validate(LegalConfirm.DoesNotConsituteALegalConfirm);
        validate(LegalConfirm.LegalConfirm);
    });

    test('should be immutable', () => {
        const ref = LegalConfirm;
        expect(() => {
            ref.DoesNotConsituteALegalConfirm = 10;
        }).toThrowError();
    });
});
