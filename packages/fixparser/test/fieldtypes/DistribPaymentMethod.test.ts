import { DistribPaymentMethod } from '../../src/fieldtypes/DistribPaymentMethod';

describe('DistribPaymentMethod', () => {
    test('should have the correct values', () => {
        expect(DistribPaymentMethod.CREST).toBe(1);
        expect(DistribPaymentMethod.NSCC).toBe(2);
        expect(DistribPaymentMethod.Euroclear).toBe(3);
        expect(DistribPaymentMethod.Clearstream).toBe(4);
        expect(DistribPaymentMethod.Cheque).toBe(5);
        expect(DistribPaymentMethod.TelegraphicTransfer).toBe(6);
        expect(DistribPaymentMethod.FedWire).toBe(7);
        expect(DistribPaymentMethod.DirectCredit).toBe(8);
        expect(DistribPaymentMethod.ACHCredit).toBe(9);
        expect(DistribPaymentMethod.BPAY).toBe(10);
        expect(DistribPaymentMethod.HighValueClearingSystemHVACS).toBe(11);
        expect(DistribPaymentMethod.ReinvestInFund).toBe(12);
        expect(DistribPaymentMethod.Other).toBe(999);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DistribPaymentMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DistribPaymentMethod.CREST,
            DistribPaymentMethod.NSCC,
            DistribPaymentMethod.Euroclear,
            DistribPaymentMethod.Clearstream,
            DistribPaymentMethod.Cheque,
            DistribPaymentMethod.TelegraphicTransfer,
            DistribPaymentMethod.FedWire,
            DistribPaymentMethod.DirectCredit,
            DistribPaymentMethod.ACHCredit,
            DistribPaymentMethod.BPAY,
            DistribPaymentMethod.HighValueClearingSystemHVACS,
            DistribPaymentMethod.ReinvestInFund,
            DistribPaymentMethod.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DistribPaymentMethod type', () => {
        const validate = (value: DistribPaymentMethod) => {
            expect(Object.values(DistribPaymentMethod)).toContain(value);
        };

        validate(DistribPaymentMethod.CREST);
        validate(DistribPaymentMethod.NSCC);
        validate(DistribPaymentMethod.Euroclear);
        validate(DistribPaymentMethod.Clearstream);
        validate(DistribPaymentMethod.Cheque);
        validate(DistribPaymentMethod.TelegraphicTransfer);
        validate(DistribPaymentMethod.FedWire);
        validate(DistribPaymentMethod.DirectCredit);
        validate(DistribPaymentMethod.ACHCredit);
        validate(DistribPaymentMethod.BPAY);
        validate(DistribPaymentMethod.HighValueClearingSystemHVACS);
        validate(DistribPaymentMethod.ReinvestInFund);
        validate(DistribPaymentMethod.Other);
    });

    test('should be immutable', () => {
        const ref = DistribPaymentMethod;
        expect(() => {
            ref.CREST = 10;
        }).toThrowError();
    });
});
