import { RefOrdIDReason } from '../../src/fieldtypes/RefOrdIDReason';

describe('RefOrdIDReason', () => {
    test('should have the correct values', () => {
        expect(RefOrdIDReason.GTCFromPreviousDay).toBe(0);
        expect(RefOrdIDReason.PartialFillRemaining).toBe(1);
        expect(RefOrdIDReason.OrderChanged).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RefOrdIDReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RefOrdIDReason.GTCFromPreviousDay,
            RefOrdIDReason.PartialFillRemaining,
            RefOrdIDReason.OrderChanged,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RefOrdIDReason type', () => {
        const validate = (value: RefOrdIDReason) => {
            expect(Object.values(RefOrdIDReason)).toContain(value);
        };

        validate(RefOrdIDReason.GTCFromPreviousDay);
        validate(RefOrdIDReason.PartialFillRemaining);
        validate(RefOrdIDReason.OrderChanged);
    });

    test('should be immutable', () => {
        const ref = RefOrdIDReason;
        expect(() => {
            ref.GTCFromPreviousDay = 10;
        }).toThrowError();
    });
});
