import { DiscretionRoundDirection } from '../../src/fieldtypes/DiscretionRoundDirection';

describe('DiscretionRoundDirection', () => {
    test('should have the correct values', () => {
        expect(DiscretionRoundDirection.MoreAggressive).toBe(1);
        expect(DiscretionRoundDirection.MorePassive).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionRoundDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DiscretionRoundDirection.MoreAggressive, DiscretionRoundDirection.MorePassive];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DiscretionRoundDirection type', () => {
        const validate = (value: DiscretionRoundDirection) => {
            expect(Object.values(DiscretionRoundDirection)).toContain(value);
        };

        validate(DiscretionRoundDirection.MoreAggressive);
        validate(DiscretionRoundDirection.MorePassive);
    });

    test('should be immutable', () => {
        const ref = DiscretionRoundDirection;
        expect(() => {
            ref.MoreAggressive = 10;
        }).toThrowError();
    });
});
