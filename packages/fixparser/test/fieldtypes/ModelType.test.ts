import { ModelType } from '../../src/fieldtypes/ModelType';

describe('ModelType', () => {
    test('should have the correct values', () => {
        expect(ModelType.UtilityProvidedStandardModel).toBe(0);
        expect(ModelType.ProprietaryModel).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ModelType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ModelType.UtilityProvidedStandardModel, ModelType.ProprietaryModel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ModelType type', () => {
        const validate = (value: ModelType) => {
            expect(Object.values(ModelType)).toContain(value);
        };

        validate(ModelType.UtilityProvidedStandardModel);
        validate(ModelType.ProprietaryModel);
    });

    test('should be immutable', () => {
        const ref = ModelType;
        expect(() => {
            ref.UtilityProvidedStandardModel = 10;
        }).toThrowError();
    });
});
