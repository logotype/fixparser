import { TradePriceCondition } from '../../src/fieldtypes/TradePriceCondition';

describe('TradePriceCondition', () => {
    test('should have the correct values', () => {
        expect(TradePriceCondition.SpecialCumDividend).toBe(0);
        expect(TradePriceCondition.SpecialCumRights).toBe(1);
        expect(TradePriceCondition.SpecialExDividend).toBe(2);
        expect(TradePriceCondition.SpecialExRights).toBe(3);
        expect(TradePriceCondition.SpecialCumCoupon).toBe(4);
        expect(TradePriceCondition.SpecialCumCapitalRepayments).toBe(5);
        expect(TradePriceCondition.SpecialExCoupon).toBe(6);
        expect(TradePriceCondition.SpecialExCapitalRepayments).toBe(7);
        expect(TradePriceCondition.CashSettlement).toBe(8);
        expect(TradePriceCondition.SpecialCumBonus).toBe(9);
        expect(TradePriceCondition.SpecialPrice).toBe(10);
        expect(TradePriceCondition.SpecialExBonus).toBe(11);
        expect(TradePriceCondition.GuaranteedDelivery).toBe(12);
        expect(TradePriceCondition.SpecialDividend).toBe(13);
        expect(TradePriceCondition.PriceImprovement).toBe(14);
        expect(TradePriceCondition.NonPriceFormingTrade).toBe(15);
        expect(TradePriceCondition.TradeExemptedFromTradingObligation).toBe(16);
        expect(TradePriceCondition.PricePending).toBe(17);
        expect(TradePriceCondition.PriceNotApplicable).toBe(18);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradePriceCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradePriceCondition.SpecialCumDividend,
            TradePriceCondition.SpecialCumRights,
            TradePriceCondition.SpecialExDividend,
            TradePriceCondition.SpecialExRights,
            TradePriceCondition.SpecialCumCoupon,
            TradePriceCondition.SpecialCumCapitalRepayments,
            TradePriceCondition.SpecialExCoupon,
            TradePriceCondition.SpecialExCapitalRepayments,
            TradePriceCondition.CashSettlement,
            TradePriceCondition.SpecialCumBonus,
            TradePriceCondition.SpecialPrice,
            TradePriceCondition.SpecialExBonus,
            TradePriceCondition.GuaranteedDelivery,
            TradePriceCondition.SpecialDividend,
            TradePriceCondition.PriceImprovement,
            TradePriceCondition.NonPriceFormingTrade,
            TradePriceCondition.TradeExemptedFromTradingObligation,
            TradePriceCondition.PricePending,
            TradePriceCondition.PriceNotApplicable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradePriceCondition type', () => {
        const validate = (value: TradePriceCondition) => {
            expect(Object.values(TradePriceCondition)).toContain(value);
        };

        validate(TradePriceCondition.SpecialCumDividend);
        validate(TradePriceCondition.SpecialCumRights);
        validate(TradePriceCondition.SpecialExDividend);
        validate(TradePriceCondition.SpecialExRights);
        validate(TradePriceCondition.SpecialCumCoupon);
        validate(TradePriceCondition.SpecialCumCapitalRepayments);
        validate(TradePriceCondition.SpecialExCoupon);
        validate(TradePriceCondition.SpecialExCapitalRepayments);
        validate(TradePriceCondition.CashSettlement);
        validate(TradePriceCondition.SpecialCumBonus);
        validate(TradePriceCondition.SpecialPrice);
        validate(TradePriceCondition.SpecialExBonus);
        validate(TradePriceCondition.GuaranteedDelivery);
        validate(TradePriceCondition.SpecialDividend);
        validate(TradePriceCondition.PriceImprovement);
        validate(TradePriceCondition.NonPriceFormingTrade);
        validate(TradePriceCondition.TradeExemptedFromTradingObligation);
        validate(TradePriceCondition.PricePending);
        validate(TradePriceCondition.PriceNotApplicable);
    });

    test('should be immutable', () => {
        const ref = TradePriceCondition;
        expect(() => {
            ref.SpecialCumDividend = 10;
        }).toThrowError();
    });
});
