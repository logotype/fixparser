import { QuoteEntryStatus } from '../../src/fieldtypes/QuoteEntryStatus';

describe('QuoteEntryStatus', () => {
    test('should have the correct values', () => {
        expect(QuoteEntryStatus.Accepted).toBe(0);
        expect(QuoteEntryStatus.Rejected).toBe(5);
        expect(QuoteEntryStatus.RemovedFromMarket).toBe(6);
        expect(QuoteEntryStatus.Expired).toBe(7);
        expect(QuoteEntryStatus.LockedMarketWarning).toBe(12);
        expect(QuoteEntryStatus.CrossMarketWarning).toBe(13);
        expect(QuoteEntryStatus.CanceledDueToLockMarket).toBe(14);
        expect(QuoteEntryStatus.CanceledDueToCrossMarket).toBe(15);
        expect(QuoteEntryStatus.Active).toBe(16);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteEntryStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteEntryStatus.Accepted,
            QuoteEntryStatus.Rejected,
            QuoteEntryStatus.RemovedFromMarket,
            QuoteEntryStatus.Expired,
            QuoteEntryStatus.LockedMarketWarning,
            QuoteEntryStatus.CrossMarketWarning,
            QuoteEntryStatus.CanceledDueToLockMarket,
            QuoteEntryStatus.CanceledDueToCrossMarket,
            QuoteEntryStatus.Active,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteEntryStatus type', () => {
        const validate = (value: QuoteEntryStatus) => {
            expect(Object.values(QuoteEntryStatus)).toContain(value);
        };

        validate(QuoteEntryStatus.Accepted);
        validate(QuoteEntryStatus.Rejected);
        validate(QuoteEntryStatus.RemovedFromMarket);
        validate(QuoteEntryStatus.Expired);
        validate(QuoteEntryStatus.LockedMarketWarning);
        validate(QuoteEntryStatus.CrossMarketWarning);
        validate(QuoteEntryStatus.CanceledDueToLockMarket);
        validate(QuoteEntryStatus.CanceledDueToCrossMarket);
        validate(QuoteEntryStatus.Active);
    });

    test('should be immutable', () => {
        const ref = QuoteEntryStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
