import { QuoteResponseLevel } from '../../src/fieldtypes/QuoteResponseLevel';

describe('QuoteResponseLevel', () => {
    test('should have the correct values', () => {
        expect(QuoteResponseLevel.NoAcknowledgement).toBe(0);
        expect(QuoteResponseLevel.AcknowledgeOnlyNegativeOrErroneousQuotes).toBe(1);
        expect(QuoteResponseLevel.AcknowledgeEachQuoteMessage).toBe(2);
        expect(QuoteResponseLevel.SummaryAcknowledgement).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteResponseLevel.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteResponseLevel.NoAcknowledgement,
            QuoteResponseLevel.AcknowledgeOnlyNegativeOrErroneousQuotes,
            QuoteResponseLevel.AcknowledgeEachQuoteMessage,
            QuoteResponseLevel.SummaryAcknowledgement,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteResponseLevel type', () => {
        const validate = (value: QuoteResponseLevel) => {
            expect(Object.values(QuoteResponseLevel)).toContain(value);
        };

        validate(QuoteResponseLevel.NoAcknowledgement);
        validate(QuoteResponseLevel.AcknowledgeOnlyNegativeOrErroneousQuotes);
        validate(QuoteResponseLevel.AcknowledgeEachQuoteMessage);
        validate(QuoteResponseLevel.SummaryAcknowledgement);
    });

    test('should be immutable', () => {
        const ref = QuoteResponseLevel;
        expect(() => {
            ref.NoAcknowledgement = 10;
        }).toThrowError();
    });
});
