import { RiskLimitCheckRequestType } from '../../src/fieldtypes/RiskLimitCheckRequestType';

describe('RiskLimitCheckRequestType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckRequestType.AllOrNone).toBe(0);
        expect(RiskLimitCheckRequestType.Partial).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RiskLimitCheckRequestType.AllOrNone, RiskLimitCheckRequestType.Partial];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckRequestType type', () => {
        const validate = (value: RiskLimitCheckRequestType) => {
            expect(Object.values(RiskLimitCheckRequestType)).toContain(value);
        };

        validate(RiskLimitCheckRequestType.AllOrNone);
        validate(RiskLimitCheckRequestType.Partial);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckRequestType;
        expect(() => {
            ref.AllOrNone = 10;
        }).toThrowError();
    });
});
