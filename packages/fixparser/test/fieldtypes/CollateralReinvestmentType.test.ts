import { CollateralReinvestmentType } from '../../src/fieldtypes/CollateralReinvestmentType';

describe('CollateralReinvestmentType', () => {
    test('should have the correct values', () => {
        expect(CollateralReinvestmentType.MoneyMarketFund).toBe(0);
        expect(CollateralReinvestmentType.OtherComingledPool).toBe(1);
        expect(CollateralReinvestmentType.RepoMarket).toBe(2);
        expect(CollateralReinvestmentType.DirectPurchaseOfSecurities).toBe(3);
        expect(CollateralReinvestmentType.OtherInvestments).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollateralReinvestmentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollateralReinvestmentType.MoneyMarketFund,
            CollateralReinvestmentType.OtherComingledPool,
            CollateralReinvestmentType.RepoMarket,
            CollateralReinvestmentType.DirectPurchaseOfSecurities,
            CollateralReinvestmentType.OtherInvestments,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollateralReinvestmentType type', () => {
        const validate = (value: CollateralReinvestmentType) => {
            expect(Object.values(CollateralReinvestmentType)).toContain(value);
        };

        validate(CollateralReinvestmentType.MoneyMarketFund);
        validate(CollateralReinvestmentType.OtherComingledPool);
        validate(CollateralReinvestmentType.RepoMarket);
        validate(CollateralReinvestmentType.DirectPurchaseOfSecurities);
        validate(CollateralReinvestmentType.OtherInvestments);
    });

    test('should be immutable', () => {
        const ref = CollateralReinvestmentType;
        expect(() => {
            ref.MoneyMarketFund = 10;
        }).toThrowError();
    });
});
