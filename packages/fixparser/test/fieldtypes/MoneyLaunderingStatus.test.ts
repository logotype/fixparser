import { MoneyLaunderingStatus } from '../../src/fieldtypes/MoneyLaunderingStatus';

describe('MoneyLaunderingStatus', () => {
    test('should have the correct values', () => {
        expect(MoneyLaunderingStatus.Passed).toBe('Y');
        expect(MoneyLaunderingStatus.NotChecked).toBe('N');
        expect(MoneyLaunderingStatus.ExemptBelowLimit).toBe('1');
        expect(MoneyLaunderingStatus.ExemptMoneyType).toBe('2');
        expect(MoneyLaunderingStatus.ExemptAuthorised).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MoneyLaunderingStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MoneyLaunderingStatus.Passed,
            MoneyLaunderingStatus.NotChecked,
            MoneyLaunderingStatus.ExemptBelowLimit,
            MoneyLaunderingStatus.ExemptMoneyType,
            MoneyLaunderingStatus.ExemptAuthorised,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MoneyLaunderingStatus type', () => {
        const validate = (value: MoneyLaunderingStatus) => {
            expect(Object.values(MoneyLaunderingStatus)).toContain(value);
        };

        validate(MoneyLaunderingStatus.Passed);
        validate(MoneyLaunderingStatus.NotChecked);
        validate(MoneyLaunderingStatus.ExemptBelowLimit);
        validate(MoneyLaunderingStatus.ExemptMoneyType);
        validate(MoneyLaunderingStatus.ExemptAuthorised);
    });

    test('should be immutable', () => {
        const ref = MoneyLaunderingStatus;
        expect(() => {
            ref.Passed = 10;
        }).toThrowError();
    });
});
