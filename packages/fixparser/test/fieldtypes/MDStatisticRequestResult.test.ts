import { MDStatisticRequestResult } from '../../src/fieldtypes/MDStatisticRequestResult';

describe('MDStatisticRequestResult', () => {
    test('should have the correct values', () => {
        expect(MDStatisticRequestResult.Successful).toBe(0);
        expect(MDStatisticRequestResult.InvalidOrUnknownMarket).toBe(1);
        expect(MDStatisticRequestResult.InvalidOrUnknownMarketSegment).toBe(2);
        expect(MDStatisticRequestResult.InvalidOrUnknownSecurityList).toBe(3);
        expect(MDStatisticRequestResult.InvalidOrUnknownInstruments).toBe(4);
        expect(MDStatisticRequestResult.InvalidParties).toBe(5);
        expect(MDStatisticRequestResult.TradeDateOutOfSupportedRange).toBe(6);
        expect(MDStatisticRequestResult.UnsupportedStatisticType).toBe(7);
        expect(MDStatisticRequestResult.UnsupportedScopeOrSubScope).toBe(8);
        expect(MDStatisticRequestResult.UnsupportedScopeType).toBe(9);
        expect(MDStatisticRequestResult.MarketDepthNotSupported).toBe(10);
        expect(MDStatisticRequestResult.FrequencyNotSupported).toBe(11);
        expect(MDStatisticRequestResult.UnsupportedStatisticInterval).toBe(12);
        expect(MDStatisticRequestResult.UnsupportedStatisticDateRange).toBe(13);
        expect(MDStatisticRequestResult.UnsupportedStatisticTimeRange).toBe(14);
        expect(MDStatisticRequestResult.UnsupportedRatioType).toBe(15);
        expect(MDStatisticRequestResult.InvalidOrUnknownTradeInputSource).toBe(16);
        expect(MDStatisticRequestResult.InvalidOrUnknownTradingSession).toBe(17);
        expect(MDStatisticRequestResult.UnauthorizedForStatisticRequest).toBe(18);
        expect(MDStatisticRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticRequestResult.Successful,
            MDStatisticRequestResult.InvalidOrUnknownMarket,
            MDStatisticRequestResult.InvalidOrUnknownMarketSegment,
            MDStatisticRequestResult.InvalidOrUnknownSecurityList,
            MDStatisticRequestResult.InvalidOrUnknownInstruments,
            MDStatisticRequestResult.InvalidParties,
            MDStatisticRequestResult.TradeDateOutOfSupportedRange,
            MDStatisticRequestResult.UnsupportedStatisticType,
            MDStatisticRequestResult.UnsupportedScopeOrSubScope,
            MDStatisticRequestResult.UnsupportedScopeType,
            MDStatisticRequestResult.MarketDepthNotSupported,
            MDStatisticRequestResult.FrequencyNotSupported,
            MDStatisticRequestResult.UnsupportedStatisticInterval,
            MDStatisticRequestResult.UnsupportedStatisticDateRange,
            MDStatisticRequestResult.UnsupportedStatisticTimeRange,
            MDStatisticRequestResult.UnsupportedRatioType,
            MDStatisticRequestResult.InvalidOrUnknownTradeInputSource,
            MDStatisticRequestResult.InvalidOrUnknownTradingSession,
            MDStatisticRequestResult.UnauthorizedForStatisticRequest,
            MDStatisticRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticRequestResult type', () => {
        const validate = (value: MDStatisticRequestResult) => {
            expect(Object.values(MDStatisticRequestResult)).toContain(value);
        };

        validate(MDStatisticRequestResult.Successful);
        validate(MDStatisticRequestResult.InvalidOrUnknownMarket);
        validate(MDStatisticRequestResult.InvalidOrUnknownMarketSegment);
        validate(MDStatisticRequestResult.InvalidOrUnknownSecurityList);
        validate(MDStatisticRequestResult.InvalidOrUnknownInstruments);
        validate(MDStatisticRequestResult.InvalidParties);
        validate(MDStatisticRequestResult.TradeDateOutOfSupportedRange);
        validate(MDStatisticRequestResult.UnsupportedStatisticType);
        validate(MDStatisticRequestResult.UnsupportedScopeOrSubScope);
        validate(MDStatisticRequestResult.UnsupportedScopeType);
        validate(MDStatisticRequestResult.MarketDepthNotSupported);
        validate(MDStatisticRequestResult.FrequencyNotSupported);
        validate(MDStatisticRequestResult.UnsupportedStatisticInterval);
        validate(MDStatisticRequestResult.UnsupportedStatisticDateRange);
        validate(MDStatisticRequestResult.UnsupportedStatisticTimeRange);
        validate(MDStatisticRequestResult.UnsupportedRatioType);
        validate(MDStatisticRequestResult.InvalidOrUnknownTradeInputSource);
        validate(MDStatisticRequestResult.InvalidOrUnknownTradingSession);
        validate(MDStatisticRequestResult.UnauthorizedForStatisticRequest);
        validate(MDStatisticRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = MDStatisticRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
