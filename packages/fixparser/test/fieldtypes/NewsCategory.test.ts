import { NewsCategory } from '../../src/fieldtypes/NewsCategory';

describe('NewsCategory', () => {
    test('should have the correct values', () => {
        expect(NewsCategory.CompanyNews).toBe(0);
        expect(NewsCategory.MarketplaceNews).toBe(1);
        expect(NewsCategory.FinancialMarketNews).toBe(2);
        expect(NewsCategory.TechnicalNews).toBe(3);
        expect(NewsCategory.OtherNews).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NewsCategory.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NewsCategory.CompanyNews,
            NewsCategory.MarketplaceNews,
            NewsCategory.FinancialMarketNews,
            NewsCategory.TechnicalNews,
            NewsCategory.OtherNews,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NewsCategory type', () => {
        const validate = (value: NewsCategory) => {
            expect(Object.values(NewsCategory)).toContain(value);
        };

        validate(NewsCategory.CompanyNews);
        validate(NewsCategory.MarketplaceNews);
        validate(NewsCategory.FinancialMarketNews);
        validate(NewsCategory.TechnicalNews);
        validate(NewsCategory.OtherNews);
    });

    test('should be immutable', () => {
        const ref = NewsCategory;
        expect(() => {
            ref.CompanyNews = 10;
        }).toThrowError();
    });
});
