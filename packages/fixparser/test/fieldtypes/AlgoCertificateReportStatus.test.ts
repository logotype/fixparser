import { AlgoCertificateReportStatus } from '../../src/fieldtypes/AlgoCertificateReportStatus';

describe('AlgoCertificateReportStatus', () => {
    test('should have the correct values', () => {
        expect(AlgoCertificateReportStatus.Received).toBe(0);
        expect(AlgoCertificateReportStatus.Accepted).toBe(1);
        expect(AlgoCertificateReportStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgoCertificateReportStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AlgoCertificateReportStatus.Received,
            AlgoCertificateReportStatus.Accepted,
            AlgoCertificateReportStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgoCertificateReportStatus type', () => {
        const validate = (value: AlgoCertificateReportStatus) => {
            expect(Object.values(AlgoCertificateReportStatus)).toContain(value);
        };

        validate(AlgoCertificateReportStatus.Received);
        validate(AlgoCertificateReportStatus.Accepted);
        validate(AlgoCertificateReportStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = AlgoCertificateReportStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
