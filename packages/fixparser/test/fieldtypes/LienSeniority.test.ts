import { LienSeniority } from '../../src/fieldtypes/LienSeniority';

describe('LienSeniority', () => {
    test('should have the correct values', () => {
        expect(LienSeniority.Unknown).toBe(0);
        expect(LienSeniority.FirstLien).toBe(1);
        expect(LienSeniority.SecondLien).toBe(2);
        expect(LienSeniority.ThirdLien).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LienSeniority.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LienSeniority.Unknown,
            LienSeniority.FirstLien,
            LienSeniority.SecondLien,
            LienSeniority.ThirdLien,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LienSeniority type', () => {
        const validate = (value: LienSeniority) => {
            expect(Object.values(LienSeniority)).toContain(value);
        };

        validate(LienSeniority.Unknown);
        validate(LienSeniority.FirstLien);
        validate(LienSeniority.SecondLien);
        validate(LienSeniority.ThirdLien);
    });

    test('should be immutable', () => {
        const ref = LienSeniority;
        expect(() => {
            ref.Unknown = 10;
        }).toThrowError();
    });
});
