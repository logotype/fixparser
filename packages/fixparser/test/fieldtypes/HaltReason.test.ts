import { HaltReason } from '../../src/fieldtypes/HaltReason';

describe('HaltReason', () => {
    test('should have the correct values', () => {
        expect(HaltReason.NewsDissemination).toBe(0);
        expect(HaltReason.OrderInflux).toBe(1);
        expect(HaltReason.OrderImbalance).toBe(2);
        expect(HaltReason.AdditionalInformation).toBe(3);
        expect(HaltReason.NewsPending).toBe(4);
        expect(HaltReason.EquipmentChangeover).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            HaltReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            HaltReason.NewsDissemination,
            HaltReason.OrderInflux,
            HaltReason.OrderImbalance,
            HaltReason.AdditionalInformation,
            HaltReason.NewsPending,
            HaltReason.EquipmentChangeover,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for HaltReason type', () => {
        const validate = (value: HaltReason) => {
            expect(Object.values(HaltReason)).toContain(value);
        };

        validate(HaltReason.NewsDissemination);
        validate(HaltReason.OrderInflux);
        validate(HaltReason.OrderImbalance);
        validate(HaltReason.AdditionalInformation);
        validate(HaltReason.NewsPending);
        validate(HaltReason.EquipmentChangeover);
    });

    test('should be immutable', () => {
        const ref = HaltReason;
        expect(() => {
            ref.NewsDissemination = 10;
        }).toThrowError();
    });
});
