import { TradSesStatus } from '../../src/fieldtypes/TradSesStatus';

describe('TradSesStatus', () => {
    test('should have the correct values', () => {
        expect(TradSesStatus.Unknown).toBe(0);
        expect(TradSesStatus.Halted).toBe(1);
        expect(TradSesStatus.Open).toBe(2);
        expect(TradSesStatus.Closed).toBe(3);
        expect(TradSesStatus.PreOpen).toBe(4);
        expect(TradSesStatus.PreClose).toBe(5);
        expect(TradSesStatus.RequestRejected).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradSesStatus.Unknown,
            TradSesStatus.Halted,
            TradSesStatus.Open,
            TradSesStatus.Closed,
            TradSesStatus.PreOpen,
            TradSesStatus.PreClose,
            TradSesStatus.RequestRejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesStatus type', () => {
        const validate = (value: TradSesStatus) => {
            expect(Object.values(TradSesStatus)).toContain(value);
        };

        validate(TradSesStatus.Unknown);
        validate(TradSesStatus.Halted);
        validate(TradSesStatus.Open);
        validate(TradSesStatus.Closed);
        validate(TradSesStatus.PreOpen);
        validate(TradSesStatus.PreClose);
        validate(TradSesStatus.RequestRejected);
    });

    test('should be immutable', () => {
        const ref = TradSesStatus;
        expect(() => {
            ref.Unknown = 10;
        }).toThrowError();
    });
});
