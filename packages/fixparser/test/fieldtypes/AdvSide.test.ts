import { AdvSide } from '../../src/fieldtypes/AdvSide';

describe('AdvSide', () => {
    test('should have the correct values', () => {
        expect(AdvSide.Buy).toBe('B');
        expect(AdvSide.Sell).toBe('S');
        expect(AdvSide.Trade).toBe('T');
        expect(AdvSide.Cross).toBe('X');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AdvSide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AdvSide.Buy, AdvSide.Sell, AdvSide.Trade, AdvSide.Cross];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AdvSide type', () => {
        const validate = (value: AdvSide) => {
            expect(Object.values(AdvSide)).toContain(value);
        };

        validate(AdvSide.Buy);
        validate(AdvSide.Sell);
        validate(AdvSide.Trade);
        validate(AdvSide.Cross);
    });

    test('should be immutable', () => {
        const ref = AdvSide;
        expect(() => {
            ref.Buy = 10;
        }).toThrowError();
    });
});
