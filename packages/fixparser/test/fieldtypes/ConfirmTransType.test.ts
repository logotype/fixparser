import { ConfirmTransType } from '../../src/fieldtypes/ConfirmTransType';

describe('ConfirmTransType', () => {
    test('should have the correct values', () => {
        expect(ConfirmTransType.New).toBe(0);
        expect(ConfirmTransType.Replace).toBe(1);
        expect(ConfirmTransType.Cancel).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ConfirmTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ConfirmTransType.New, ConfirmTransType.Replace, ConfirmTransType.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ConfirmTransType type', () => {
        const validate = (value: ConfirmTransType) => {
            expect(Object.values(ConfirmTransType)).toContain(value);
        };

        validate(ConfirmTransType.New);
        validate(ConfirmTransType.Replace);
        validate(ConfirmTransType.Cancel);
    });

    test('should be immutable', () => {
        const ref = ConfirmTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
