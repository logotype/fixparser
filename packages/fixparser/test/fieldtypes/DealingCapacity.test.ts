import { DealingCapacity } from '../../src/fieldtypes/DealingCapacity';

describe('DealingCapacity', () => {
    test('should have the correct values', () => {
        expect(DealingCapacity.Agent).toBe('A');
        expect(DealingCapacity.Principal).toBe('P');
        expect(DealingCapacity.RisklessPrincipal).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DealingCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DealingCapacity.Agent, DealingCapacity.Principal, DealingCapacity.RisklessPrincipal];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DealingCapacity type', () => {
        const validate = (value: DealingCapacity) => {
            expect(Object.values(DealingCapacity)).toContain(value);
        };

        validate(DealingCapacity.Agent);
        validate(DealingCapacity.Principal);
        validate(DealingCapacity.RisklessPrincipal);
    });

    test('should be immutable', () => {
        const ref = DealingCapacity;
        expect(() => {
            ref.Agent = 10;
        }).toThrowError();
    });
});
