import { ProgRptReqs } from '../../src/fieldtypes/ProgRptReqs';

describe('ProgRptReqs', () => {
    test('should have the correct values', () => {
        expect(ProgRptReqs.BuySideRequests).toBe(1);
        expect(ProgRptReqs.SellSideSends).toBe(2);
        expect(ProgRptReqs.RealTimeExecutionReports).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProgRptReqs.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProgRptReqs.BuySideRequests,
            ProgRptReqs.SellSideSends,
            ProgRptReqs.RealTimeExecutionReports,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProgRptReqs type', () => {
        const validate = (value: ProgRptReqs) => {
            expect(Object.values(ProgRptReqs)).toContain(value);
        };

        validate(ProgRptReqs.BuySideRequests);
        validate(ProgRptReqs.SellSideSends);
        validate(ProgRptReqs.RealTimeExecutionReports);
    });

    test('should be immutable', () => {
        const ref = ProgRptReqs;
        expect(() => {
            ref.BuySideRequests = 10;
        }).toThrowError();
    });
});
