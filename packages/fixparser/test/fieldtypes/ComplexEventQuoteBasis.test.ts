import { ComplexEventQuoteBasis } from '../../src/fieldtypes/ComplexEventQuoteBasis';

describe('ComplexEventQuoteBasis', () => {
    test('should have the correct values', () => {
        expect(ComplexEventQuoteBasis.Currency1PerCurrency2).toBe(0);
        expect(ComplexEventQuoteBasis.Currency2PerCurrency1).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventQuoteBasis.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventQuoteBasis.Currency1PerCurrency2,
            ComplexEventQuoteBasis.Currency2PerCurrency1,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventQuoteBasis type', () => {
        const validate = (value: ComplexEventQuoteBasis) => {
            expect(Object.values(ComplexEventQuoteBasis)).toContain(value);
        };

        validate(ComplexEventQuoteBasis.Currency1PerCurrency2);
        validate(ComplexEventQuoteBasis.Currency2PerCurrency1);
    });

    test('should be immutable', () => {
        const ref = ComplexEventQuoteBasis;
        expect(() => {
            ref.Currency1PerCurrency2 = 10;
        }).toThrowError();
    });
});
