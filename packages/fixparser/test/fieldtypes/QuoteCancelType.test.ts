import { QuoteCancelType } from '../../src/fieldtypes/QuoteCancelType';

describe('QuoteCancelType', () => {
    test('should have the correct values', () => {
        expect(QuoteCancelType.CancelForOneOrMoreSecurities).toBe(1);
        expect(QuoteCancelType.CancelForSecurityType).toBe(2);
        expect(QuoteCancelType.CancelForUnderlyingSecurity).toBe(3);
        expect(QuoteCancelType.CancelAllQuotes).toBe(4);
        expect(QuoteCancelType.CancelSpecifiedSingleQuote).toBe(5);
        expect(QuoteCancelType.CancelByTypeOfQuote).toBe(6);
        expect(QuoteCancelType.CancelForSecurityIssuer).toBe(7);
        expect(QuoteCancelType.CancelForIssuerOfUnderlyingSecurity).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteCancelType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteCancelType.CancelForOneOrMoreSecurities,
            QuoteCancelType.CancelForSecurityType,
            QuoteCancelType.CancelForUnderlyingSecurity,
            QuoteCancelType.CancelAllQuotes,
            QuoteCancelType.CancelSpecifiedSingleQuote,
            QuoteCancelType.CancelByTypeOfQuote,
            QuoteCancelType.CancelForSecurityIssuer,
            QuoteCancelType.CancelForIssuerOfUnderlyingSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteCancelType type', () => {
        const validate = (value: QuoteCancelType) => {
            expect(Object.values(QuoteCancelType)).toContain(value);
        };

        validate(QuoteCancelType.CancelForOneOrMoreSecurities);
        validate(QuoteCancelType.CancelForSecurityType);
        validate(QuoteCancelType.CancelForUnderlyingSecurity);
        validate(QuoteCancelType.CancelAllQuotes);
        validate(QuoteCancelType.CancelSpecifiedSingleQuote);
        validate(QuoteCancelType.CancelByTypeOfQuote);
        validate(QuoteCancelType.CancelForSecurityIssuer);
        validate(QuoteCancelType.CancelForIssuerOfUnderlyingSecurity);
    });

    test('should be immutable', () => {
        const ref = QuoteCancelType;
        expect(() => {
            ref.CancelForOneOrMoreSecurities = 10;
        }).toThrowError();
    });
});
