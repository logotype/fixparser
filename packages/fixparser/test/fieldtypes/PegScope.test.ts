import { PegScope } from '../../src/fieldtypes/PegScope';

describe('PegScope', () => {
    test('should have the correct values', () => {
        expect(PegScope.Local).toBe(1);
        expect(PegScope.National).toBe(2);
        expect(PegScope.Global).toBe(3);
        expect(PegScope.NationalExcludingLocal).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PegScope.Local, PegScope.National, PegScope.Global, PegScope.NationalExcludingLocal];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegScope type', () => {
        const validate = (value: PegScope) => {
            expect(Object.values(PegScope)).toContain(value);
        };

        validate(PegScope.Local);
        validate(PegScope.National);
        validate(PegScope.Global);
        validate(PegScope.NationalExcludingLocal);
    });

    test('should be immutable', () => {
        const ref = PegScope;
        expect(() => {
            ref.Local = 10;
        }).toThrowError();
    });
});
