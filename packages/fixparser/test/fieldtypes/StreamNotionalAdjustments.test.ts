import { StreamNotionalAdjustments } from '../../src/fieldtypes/StreamNotionalAdjustments';

describe('StreamNotionalAdjustments', () => {
    test('should have the correct values', () => {
        expect(StreamNotionalAdjustments.Execution).toBe(0);
        expect(StreamNotionalAdjustments.PortfolioRebalancing).toBe(1);
        expect(StreamNotionalAdjustments.Standard).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamNotionalAdjustments.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StreamNotionalAdjustments.Execution,
            StreamNotionalAdjustments.PortfolioRebalancing,
            StreamNotionalAdjustments.Standard,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamNotionalAdjustments type', () => {
        const validate = (value: StreamNotionalAdjustments) => {
            expect(Object.values(StreamNotionalAdjustments)).toContain(value);
        };

        validate(StreamNotionalAdjustments.Execution);
        validate(StreamNotionalAdjustments.PortfolioRebalancing);
        validate(StreamNotionalAdjustments.Standard);
    });

    test('should be immutable', () => {
        const ref = StreamNotionalAdjustments;
        expect(() => {
            ref.Execution = 10;
        }).toThrowError();
    });
});
