import { RegistRejReasonCode } from '../../src/fieldtypes/RegistRejReasonCode';

describe('RegistRejReasonCode', () => {
    test('should have the correct values', () => {
        expect(RegistRejReasonCode.InvalidAccountType).toBe(1);
        expect(RegistRejReasonCode.InvalidTaxExemptType).toBe(2);
        expect(RegistRejReasonCode.InvalidOwnershipType).toBe(3);
        expect(RegistRejReasonCode.NoRegDetails).toBe(4);
        expect(RegistRejReasonCode.InvalidRegSeqNo).toBe(5);
        expect(RegistRejReasonCode.InvalidRegDetails).toBe(6);
        expect(RegistRejReasonCode.InvalidMailingDetails).toBe(7);
        expect(RegistRejReasonCode.InvalidMailingInstructions).toBe(8);
        expect(RegistRejReasonCode.InvalidInvestorID).toBe(9);
        expect(RegistRejReasonCode.InvalidInvestorIDSource).toBe(10);
        expect(RegistRejReasonCode.InvalidDateOfBirth).toBe(11);
        expect(RegistRejReasonCode.InvalidCountry).toBe(12);
        expect(RegistRejReasonCode.InvalidDistribInstns).toBe(13);
        expect(RegistRejReasonCode.InvalidPercentage).toBe(14);
        expect(RegistRejReasonCode.InvalidPaymentMethod).toBe(15);
        expect(RegistRejReasonCode.InvalidAccountName).toBe(16);
        expect(RegistRejReasonCode.InvalidAgentCode).toBe(17);
        expect(RegistRejReasonCode.InvalidAccountNum).toBe(18);
        expect(RegistRejReasonCode.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegistRejReasonCode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegistRejReasonCode.InvalidAccountType,
            RegistRejReasonCode.InvalidTaxExemptType,
            RegistRejReasonCode.InvalidOwnershipType,
            RegistRejReasonCode.NoRegDetails,
            RegistRejReasonCode.InvalidRegSeqNo,
            RegistRejReasonCode.InvalidRegDetails,
            RegistRejReasonCode.InvalidMailingDetails,
            RegistRejReasonCode.InvalidMailingInstructions,
            RegistRejReasonCode.InvalidInvestorID,
            RegistRejReasonCode.InvalidInvestorIDSource,
            RegistRejReasonCode.InvalidDateOfBirth,
            RegistRejReasonCode.InvalidCountry,
            RegistRejReasonCode.InvalidDistribInstns,
            RegistRejReasonCode.InvalidPercentage,
            RegistRejReasonCode.InvalidPaymentMethod,
            RegistRejReasonCode.InvalidAccountName,
            RegistRejReasonCode.InvalidAgentCode,
            RegistRejReasonCode.InvalidAccountNum,
            RegistRejReasonCode.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegistRejReasonCode type', () => {
        const validate = (value: RegistRejReasonCode) => {
            expect(Object.values(RegistRejReasonCode)).toContain(value);
        };

        validate(RegistRejReasonCode.InvalidAccountType);
        validate(RegistRejReasonCode.InvalidTaxExemptType);
        validate(RegistRejReasonCode.InvalidOwnershipType);
        validate(RegistRejReasonCode.NoRegDetails);
        validate(RegistRejReasonCode.InvalidRegSeqNo);
        validate(RegistRejReasonCode.InvalidRegDetails);
        validate(RegistRejReasonCode.InvalidMailingDetails);
        validate(RegistRejReasonCode.InvalidMailingInstructions);
        validate(RegistRejReasonCode.InvalidInvestorID);
        validate(RegistRejReasonCode.InvalidInvestorIDSource);
        validate(RegistRejReasonCode.InvalidDateOfBirth);
        validate(RegistRejReasonCode.InvalidCountry);
        validate(RegistRejReasonCode.InvalidDistribInstns);
        validate(RegistRejReasonCode.InvalidPercentage);
        validate(RegistRejReasonCode.InvalidPaymentMethod);
        validate(RegistRejReasonCode.InvalidAccountName);
        validate(RegistRejReasonCode.InvalidAgentCode);
        validate(RegistRejReasonCode.InvalidAccountNum);
        validate(RegistRejReasonCode.Other);
    });

    test('should be immutable', () => {
        const ref = RegistRejReasonCode;
        expect(() => {
            ref.InvalidAccountType = 10;
        }).toThrowError();
    });
});
