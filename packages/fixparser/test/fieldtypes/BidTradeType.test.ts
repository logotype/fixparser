import { BidTradeType } from '../../src/fieldtypes/BidTradeType';

describe('BidTradeType', () => {
    test('should have the correct values', () => {
        expect(BidTradeType.Agency).toBe('A');
        expect(BidTradeType.VWAPGuarantee).toBe('G');
        expect(BidTradeType.GuaranteedClose).toBe('J');
        expect(BidTradeType.RiskTrade).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BidTradeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BidTradeType.Agency,
            BidTradeType.VWAPGuarantee,
            BidTradeType.GuaranteedClose,
            BidTradeType.RiskTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BidTradeType type', () => {
        const validate = (value: BidTradeType) => {
            expect(Object.values(BidTradeType)).toContain(value);
        };

        validate(BidTradeType.Agency);
        validate(BidTradeType.VWAPGuarantee);
        validate(BidTradeType.GuaranteedClose);
        validate(BidTradeType.RiskTrade);
    });

    test('should be immutable', () => {
        const ref = BidTradeType;
        expect(() => {
            ref.Agency = 10;
        }).toThrowError();
    });
});
