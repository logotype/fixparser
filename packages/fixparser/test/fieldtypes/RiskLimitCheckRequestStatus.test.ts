import { RiskLimitCheckRequestStatus } from '../../src/fieldtypes/RiskLimitCheckRequestStatus';

describe('RiskLimitCheckRequestStatus', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckRequestStatus.Approved).toBe(0);
        expect(RiskLimitCheckRequestStatus.PartiallyApproved).toBe(1);
        expect(RiskLimitCheckRequestStatus.Rejected).toBe(2);
        expect(RiskLimitCheckRequestStatus.ApprovalPending).toBe(3);
        expect(RiskLimitCheckRequestStatus.Cancelled).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitCheckRequestStatus.Approved,
            RiskLimitCheckRequestStatus.PartiallyApproved,
            RiskLimitCheckRequestStatus.Rejected,
            RiskLimitCheckRequestStatus.ApprovalPending,
            RiskLimitCheckRequestStatus.Cancelled,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckRequestStatus type', () => {
        const validate = (value: RiskLimitCheckRequestStatus) => {
            expect(Object.values(RiskLimitCheckRequestStatus)).toContain(value);
        };

        validate(RiskLimitCheckRequestStatus.Approved);
        validate(RiskLimitCheckRequestStatus.PartiallyApproved);
        validate(RiskLimitCheckRequestStatus.Rejected);
        validate(RiskLimitCheckRequestStatus.ApprovalPending);
        validate(RiskLimitCheckRequestStatus.Cancelled);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckRequestStatus;
        expect(() => {
            ref.Approved = 10;
        }).toThrowError();
    });
});
