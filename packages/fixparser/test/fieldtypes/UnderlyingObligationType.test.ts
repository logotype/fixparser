import { UnderlyingObligationType } from '../../src/fieldtypes/UnderlyingObligationType';

describe('UnderlyingObligationType', () => {
    test('should have the correct values', () => {
        expect(UnderlyingObligationType.Bond).toBe('0');
        expect(UnderlyingObligationType.ConvertibleBond).toBe('1');
        expect(UnderlyingObligationType.Mortgage).toBe('2');
        expect(UnderlyingObligationType.Loan).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingObligationType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnderlyingObligationType.Bond,
            UnderlyingObligationType.ConvertibleBond,
            UnderlyingObligationType.Mortgage,
            UnderlyingObligationType.Loan,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for UnderlyingObligationType type', () => {
        const validate = (value: UnderlyingObligationType) => {
            expect(Object.values(UnderlyingObligationType)).toContain(value);
        };

        validate(UnderlyingObligationType.Bond);
        validate(UnderlyingObligationType.ConvertibleBond);
        validate(UnderlyingObligationType.Mortgage);
        validate(UnderlyingObligationType.Loan);
    });

    test('should be immutable', () => {
        const ref = UnderlyingObligationType;
        expect(() => {
            ref.Bond = 10;
        }).toThrowError();
    });
});
