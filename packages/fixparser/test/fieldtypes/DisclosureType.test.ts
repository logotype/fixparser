import { DisclosureType } from '../../src/fieldtypes/DisclosureType';

describe('DisclosureType', () => {
    test('should have the correct values', () => {
        expect(DisclosureType.Volume).toBe(1);
        expect(DisclosureType.Price).toBe(2);
        expect(DisclosureType.Side).toBe(3);
        expect(DisclosureType.AON).toBe(4);
        expect(DisclosureType.General).toBe(5);
        expect(DisclosureType.ClearingAccount).toBe(6);
        expect(DisclosureType.CMTAAccount).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DisclosureType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DisclosureType.Volume,
            DisclosureType.Price,
            DisclosureType.Side,
            DisclosureType.AON,
            DisclosureType.General,
            DisclosureType.ClearingAccount,
            DisclosureType.CMTAAccount,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DisclosureType type', () => {
        const validate = (value: DisclosureType) => {
            expect(Object.values(DisclosureType)).toContain(value);
        };

        validate(DisclosureType.Volume);
        validate(DisclosureType.Price);
        validate(DisclosureType.Side);
        validate(DisclosureType.AON);
        validate(DisclosureType.General);
        validate(DisclosureType.ClearingAccount);
        validate(DisclosureType.CMTAAccount);
    });

    test('should be immutable', () => {
        const ref = DisclosureType;
        expect(() => {
            ref.Volume = 10;
        }).toThrowError();
    });
});
