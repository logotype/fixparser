import { StreamAsgnReqType } from '../../src/fieldtypes/StreamAsgnReqType';

describe('StreamAsgnReqType', () => {
    test('should have the correct values', () => {
        expect(StreamAsgnReqType.StreamAssignmentForNewCustomer).toBe(1);
        expect(StreamAsgnReqType.StreamAssignmentForExistingCustomer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamAsgnReqType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StreamAsgnReqType.StreamAssignmentForNewCustomer,
            StreamAsgnReqType.StreamAssignmentForExistingCustomer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamAsgnReqType type', () => {
        const validate = (value: StreamAsgnReqType) => {
            expect(Object.values(StreamAsgnReqType)).toContain(value);
        };

        validate(StreamAsgnReqType.StreamAssignmentForNewCustomer);
        validate(StreamAsgnReqType.StreamAssignmentForExistingCustomer);
    });

    test('should be immutable', () => {
        const ref = StreamAsgnReqType;
        expect(() => {
            ref.StreamAssignmentForNewCustomer = 10;
        }).toThrowError();
    });
});
