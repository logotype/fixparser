import { DeliveryScheduleToleranceType } from '../../src/fieldtypes/DeliveryScheduleToleranceType';

describe('DeliveryScheduleToleranceType', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleToleranceType.Absolute).toBe(0);
        expect(DeliveryScheduleToleranceType.Percentage).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleToleranceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryScheduleToleranceType.Absolute, DeliveryScheduleToleranceType.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleToleranceType type', () => {
        const validate = (value: DeliveryScheduleToleranceType) => {
            expect(Object.values(DeliveryScheduleToleranceType)).toContain(value);
        };

        validate(DeliveryScheduleToleranceType.Absolute);
        validate(DeliveryScheduleToleranceType.Percentage);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleToleranceType;
        expect(() => {
            ref.Absolute = 10;
        }).toThrowError();
    });
});
