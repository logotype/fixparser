import { PaymentStreamInflationLagDayType } from '../../src/fieldtypes/PaymentStreamInflationLagDayType';

describe('PaymentStreamInflationLagDayType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamInflationLagDayType.Business).toBe(0);
        expect(PaymentStreamInflationLagDayType.Calendar).toBe(1);
        expect(PaymentStreamInflationLagDayType.CommodityBusiness).toBe(2);
        expect(PaymentStreamInflationLagDayType.CurrencyBusiness).toBe(3);
        expect(PaymentStreamInflationLagDayType.ExchangeBusiness).toBe(4);
        expect(PaymentStreamInflationLagDayType.ScheduledTradingDay).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamInflationLagDayType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamInflationLagDayType.Business,
            PaymentStreamInflationLagDayType.Calendar,
            PaymentStreamInflationLagDayType.CommodityBusiness,
            PaymentStreamInflationLagDayType.CurrencyBusiness,
            PaymentStreamInflationLagDayType.ExchangeBusiness,
            PaymentStreamInflationLagDayType.ScheduledTradingDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamInflationLagDayType type', () => {
        const validate = (value: PaymentStreamInflationLagDayType) => {
            expect(Object.values(PaymentStreamInflationLagDayType)).toContain(value);
        };

        validate(PaymentStreamInflationLagDayType.Business);
        validate(PaymentStreamInflationLagDayType.Calendar);
        validate(PaymentStreamInflationLagDayType.CommodityBusiness);
        validate(PaymentStreamInflationLagDayType.CurrencyBusiness);
        validate(PaymentStreamInflationLagDayType.ExchangeBusiness);
        validate(PaymentStreamInflationLagDayType.ScheduledTradingDay);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamInflationLagDayType;
        expect(() => {
            ref.Business = 10;
        }).toThrowError();
    });
});
