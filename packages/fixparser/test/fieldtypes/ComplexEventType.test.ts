import { ComplexEventType } from '../../src/fieldtypes/ComplexEventType';

describe('ComplexEventType', () => {
    test('should have the correct values', () => {
        expect(ComplexEventType.Capped).toBe(1);
        expect(ComplexEventType.Trigger).toBe(2);
        expect(ComplexEventType.KnockInUp).toBe(3);
        expect(ComplexEventType.KnockInDown).toBe(4);
        expect(ComplexEventType.KnockOutUp).toBe(5);
        expect(ComplexEventType.KnockOutDown).toBe(6);
        expect(ComplexEventType.Underlying).toBe(7);
        expect(ComplexEventType.ResetBarrier).toBe(8);
        expect(ComplexEventType.RollingBarrier).toBe(9);
        expect(ComplexEventType.OneTouch).toBe(10);
        expect(ComplexEventType.NoTouch).toBe(11);
        expect(ComplexEventType.DblOneTouch).toBe(12);
        expect(ComplexEventType.DblNoTouch).toBe(13);
        expect(ComplexEventType.FXComposite).toBe(14);
        expect(ComplexEventType.FXQuanto).toBe(15);
        expect(ComplexEventType.FXCrssCcy).toBe(16);
        expect(ComplexEventType.StrkSpread).toBe(17);
        expect(ComplexEventType.ClndrSpread).toBe(18);
        expect(ComplexEventType.PxObsvtn).toBe(19);
        expect(ComplexEventType.PassThrough).toBe(20);
        expect(ComplexEventType.StrkSched).toBe(21);
        expect(ComplexEventType.EquityValuation).toBe(22);
        expect(ComplexEventType.DividendValuation).toBe(23);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventType.Capped,
            ComplexEventType.Trigger,
            ComplexEventType.KnockInUp,
            ComplexEventType.KnockInDown,
            ComplexEventType.KnockOutUp,
            ComplexEventType.KnockOutDown,
            ComplexEventType.Underlying,
            ComplexEventType.ResetBarrier,
            ComplexEventType.RollingBarrier,
            ComplexEventType.OneTouch,
            ComplexEventType.NoTouch,
            ComplexEventType.DblOneTouch,
            ComplexEventType.DblNoTouch,
            ComplexEventType.FXComposite,
            ComplexEventType.FXQuanto,
            ComplexEventType.FXCrssCcy,
            ComplexEventType.StrkSpread,
            ComplexEventType.ClndrSpread,
            ComplexEventType.PxObsvtn,
            ComplexEventType.PassThrough,
            ComplexEventType.StrkSched,
            ComplexEventType.EquityValuation,
            ComplexEventType.DividendValuation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventType type', () => {
        const validate = (value: ComplexEventType) => {
            expect(Object.values(ComplexEventType)).toContain(value);
        };

        validate(ComplexEventType.Capped);
        validate(ComplexEventType.Trigger);
        validate(ComplexEventType.KnockInUp);
        validate(ComplexEventType.KnockInDown);
        validate(ComplexEventType.KnockOutUp);
        validate(ComplexEventType.KnockOutDown);
        validate(ComplexEventType.Underlying);
        validate(ComplexEventType.ResetBarrier);
        validate(ComplexEventType.RollingBarrier);
        validate(ComplexEventType.OneTouch);
        validate(ComplexEventType.NoTouch);
        validate(ComplexEventType.DblOneTouch);
        validate(ComplexEventType.DblNoTouch);
        validate(ComplexEventType.FXComposite);
        validate(ComplexEventType.FXQuanto);
        validate(ComplexEventType.FXCrssCcy);
        validate(ComplexEventType.StrkSpread);
        validate(ComplexEventType.ClndrSpread);
        validate(ComplexEventType.PxObsvtn);
        validate(ComplexEventType.PassThrough);
        validate(ComplexEventType.StrkSched);
        validate(ComplexEventType.EquityValuation);
        validate(ComplexEventType.DividendValuation);
    });

    test('should be immutable', () => {
        const ref = ComplexEventType;
        expect(() => {
            ref.Capped = 10;
        }).toThrowError();
    });
});
