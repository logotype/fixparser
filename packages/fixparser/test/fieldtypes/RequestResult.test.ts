import { RequestResult } from '../../src/fieldtypes/RequestResult';

describe('RequestResult', () => {
    test('should have the correct values', () => {
        expect(RequestResult.ValidRequest).toBe(0);
        expect(RequestResult.InvalidOrUnsupportedRequest).toBe(1);
        expect(RequestResult.NoDataFound).toBe(2);
        expect(RequestResult.NotAuthorized).toBe(3);
        expect(RequestResult.DataTemporarilyUnavailable).toBe(4);
        expect(RequestResult.RequestForDataNotSupported).toBe(5);
        expect(RequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RequestResult.ValidRequest,
            RequestResult.InvalidOrUnsupportedRequest,
            RequestResult.NoDataFound,
            RequestResult.NotAuthorized,
            RequestResult.DataTemporarilyUnavailable,
            RequestResult.RequestForDataNotSupported,
            RequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RequestResult type', () => {
        const validate = (value: RequestResult) => {
            expect(Object.values(RequestResult)).toContain(value);
        };

        validate(RequestResult.ValidRequest);
        validate(RequestResult.InvalidOrUnsupportedRequest);
        validate(RequestResult.NoDataFound);
        validate(RequestResult.NotAuthorized);
        validate(RequestResult.DataTemporarilyUnavailable);
        validate(RequestResult.RequestForDataNotSupported);
        validate(RequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = RequestResult;
        expect(() => {
            ref.ValidRequest = 10;
        }).toThrowError();
    });
});
