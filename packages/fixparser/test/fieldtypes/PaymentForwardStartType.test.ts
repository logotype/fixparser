import { PaymentForwardStartType } from '../../src/fieldtypes/PaymentForwardStartType';

describe('PaymentForwardStartType', () => {
    test('should have the correct values', () => {
        expect(PaymentForwardStartType.Prepaid).toBe(0);
        expect(PaymentForwardStartType.Postpaid).toBe(1);
        expect(PaymentForwardStartType.Variable).toBe(2);
        expect(PaymentForwardStartType.Fixed).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentForwardStartType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentForwardStartType.Prepaid,
            PaymentForwardStartType.Postpaid,
            PaymentForwardStartType.Variable,
            PaymentForwardStartType.Fixed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentForwardStartType type', () => {
        const validate = (value: PaymentForwardStartType) => {
            expect(Object.values(PaymentForwardStartType)).toContain(value);
        };

        validate(PaymentForwardStartType.Prepaid);
        validate(PaymentForwardStartType.Postpaid);
        validate(PaymentForwardStartType.Variable);
        validate(PaymentForwardStartType.Fixed);
    });

    test('should be immutable', () => {
        const ref = PaymentForwardStartType;
        expect(() => {
            ref.Prepaid = 10;
        }).toThrowError();
    });
});
