import { OrderHandlingInstSource } from '../../src/fieldtypes/OrderHandlingInstSource';

describe('OrderHandlingInstSource', () => {
    test('should have the correct values', () => {
        expect(OrderHandlingInstSource.FINRAOATS).toBe(1);
        expect(OrderHandlingInstSource.FIAExecutionSourceCode).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderHandlingInstSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OrderHandlingInstSource.FINRAOATS, OrderHandlingInstSource.FIAExecutionSourceCode];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderHandlingInstSource type', () => {
        const validate = (value: OrderHandlingInstSource) => {
            expect(Object.values(OrderHandlingInstSource)).toContain(value);
        };

        validate(OrderHandlingInstSource.FINRAOATS);
        validate(OrderHandlingInstSource.FIAExecutionSourceCode);
    });

    test('should be immutable', () => {
        const ref = OrderHandlingInstSource;
        expect(() => {
            ref.FINRAOATS = 10;
        }).toThrowError();
    });
});
