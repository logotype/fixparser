import { CalculationMethod } from '../../src/fieldtypes/CalculationMethod';

describe('CalculationMethod', () => {
    test('should have the correct values', () => {
        expect(CalculationMethod.Automatic).toBe(0);
        expect(CalculationMethod.Manual).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CalculationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CalculationMethod.Automatic, CalculationMethod.Manual];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CalculationMethod type', () => {
        const validate = (value: CalculationMethod) => {
            expect(Object.values(CalculationMethod)).toContain(value);
        };

        validate(CalculationMethod.Automatic);
        validate(CalculationMethod.Manual);
    });

    test('should be immutable', () => {
        const ref = CalculationMethod;
        expect(() => {
            ref.Automatic = 10;
        }).toThrowError();
    });
});
