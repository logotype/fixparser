import { AlgorithmicTradeIndicator } from '../../src/fieldtypes/AlgorithmicTradeIndicator';

describe('AlgorithmicTradeIndicator', () => {
    test('should have the correct values', () => {
        expect(AlgorithmicTradeIndicator.NonAlgorithmicTrade).toBe(0);
        expect(AlgorithmicTradeIndicator.AlgorithmicTrade).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgorithmicTradeIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AlgorithmicTradeIndicator.NonAlgorithmicTrade,
            AlgorithmicTradeIndicator.AlgorithmicTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgorithmicTradeIndicator type', () => {
        const validate = (value: AlgorithmicTradeIndicator) => {
            expect(Object.values(AlgorithmicTradeIndicator)).toContain(value);
        };

        validate(AlgorithmicTradeIndicator.NonAlgorithmicTrade);
        validate(AlgorithmicTradeIndicator.AlgorithmicTrade);
    });

    test('should be immutable', () => {
        const ref = AlgorithmicTradeIndicator;
        expect(() => {
            ref.NonAlgorithmicTrade = 10;
        }).toThrowError();
    });
});
