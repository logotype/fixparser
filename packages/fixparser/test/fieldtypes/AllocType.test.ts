import { AllocType } from '../../src/fieldtypes/AllocType';

describe('AllocType', () => {
    test('should have the correct values', () => {
        expect(AllocType.Calculated).toBe(1);
        expect(AllocType.Preliminary).toBe(2);
        expect(AllocType.SellsideCalculatedUsingPreliminary).toBe(3);
        expect(AllocType.SellsideCalculatedWithoutPreliminary).toBe(4);
        expect(AllocType.ReadyToBook).toBe(5);
        expect(AllocType.BuysideReadyToBook).toBe(6);
        expect(AllocType.WarehouseInstruction).toBe(7);
        expect(AllocType.RequestToIntermediary).toBe(8);
        expect(AllocType.Accept).toBe(9);
        expect(AllocType.Reject).toBe(10);
        expect(AllocType.AcceptPending).toBe(11);
        expect(AllocType.IncompleteGroup).toBe(12);
        expect(AllocType.CompleteGroup).toBe(13);
        expect(AllocType.ReversalPending).toBe(14);
        expect(AllocType.ReopenGroup).toBe(15);
        expect(AllocType.CancelGroup).toBe(16);
        expect(AllocType.Giveup).toBe(17);
        expect(AllocType.Takeup).toBe(18);
        expect(AllocType.RefuseTakeup).toBe(19);
        expect(AllocType.InitiateReversal).toBe(20);
        expect(AllocType.Reverse).toBe(21);
        expect(AllocType.RefuseReversal).toBe(22);
        expect(AllocType.SubAllocationGiveup).toBe(23);
        expect(AllocType.ApproveGiveup).toBe(24);
        expect(AllocType.ApproveTakeup).toBe(25);
        expect(AllocType.NotionalValueAveragePxGroupAlloc).toBe(26);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocType.Calculated,
            AllocType.Preliminary,
            AllocType.SellsideCalculatedUsingPreliminary,
            AllocType.SellsideCalculatedWithoutPreliminary,
            AllocType.ReadyToBook,
            AllocType.BuysideReadyToBook,
            AllocType.WarehouseInstruction,
            AllocType.RequestToIntermediary,
            AllocType.Accept,
            AllocType.Reject,
            AllocType.AcceptPending,
            AllocType.IncompleteGroup,
            AllocType.CompleteGroup,
            AllocType.ReversalPending,
            AllocType.ReopenGroup,
            AllocType.CancelGroup,
            AllocType.Giveup,
            AllocType.Takeup,
            AllocType.RefuseTakeup,
            AllocType.InitiateReversal,
            AllocType.Reverse,
            AllocType.RefuseReversal,
            AllocType.SubAllocationGiveup,
            AllocType.ApproveGiveup,
            AllocType.ApproveTakeup,
            AllocType.NotionalValueAveragePxGroupAlloc,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocType type', () => {
        const validate = (value: AllocType) => {
            expect(Object.values(AllocType)).toContain(value);
        };

        validate(AllocType.Calculated);
        validate(AllocType.Preliminary);
        validate(AllocType.SellsideCalculatedUsingPreliminary);
        validate(AllocType.SellsideCalculatedWithoutPreliminary);
        validate(AllocType.ReadyToBook);
        validate(AllocType.BuysideReadyToBook);
        validate(AllocType.WarehouseInstruction);
        validate(AllocType.RequestToIntermediary);
        validate(AllocType.Accept);
        validate(AllocType.Reject);
        validate(AllocType.AcceptPending);
        validate(AllocType.IncompleteGroup);
        validate(AllocType.CompleteGroup);
        validate(AllocType.ReversalPending);
        validate(AllocType.ReopenGroup);
        validate(AllocType.CancelGroup);
        validate(AllocType.Giveup);
        validate(AllocType.Takeup);
        validate(AllocType.RefuseTakeup);
        validate(AllocType.InitiateReversal);
        validate(AllocType.Reverse);
        validate(AllocType.RefuseReversal);
        validate(AllocType.SubAllocationGiveup);
        validate(AllocType.ApproveGiveup);
        validate(AllocType.ApproveTakeup);
        validate(AllocType.NotionalValueAveragePxGroupAlloc);
    });

    test('should be immutable', () => {
        const ref = AllocType;
        expect(() => {
            ref.Calculated = 10;
        }).toThrowError();
    });
});
