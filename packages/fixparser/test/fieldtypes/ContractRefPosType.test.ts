import { ContractRefPosType } from '../../src/fieldtypes/ContractRefPosType';

describe('ContractRefPosType', () => {
    test('should have the correct values', () => {
        expect(ContractRefPosType.TwoComponentIntercommoditySpread).toBe(0);
        expect(ContractRefPosType.IndexOrBasket).toBe(1);
        expect(ContractRefPosType.TwoComponentLocationBasis).toBe(2);
        expect(ContractRefPosType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ContractRefPosType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ContractRefPosType.TwoComponentIntercommoditySpread,
            ContractRefPosType.IndexOrBasket,
            ContractRefPosType.TwoComponentLocationBasis,
            ContractRefPosType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ContractRefPosType type', () => {
        const validate = (value: ContractRefPosType) => {
            expect(Object.values(ContractRefPosType)).toContain(value);
        };

        validate(ContractRefPosType.TwoComponentIntercommoditySpread);
        validate(ContractRefPosType.IndexOrBasket);
        validate(ContractRefPosType.TwoComponentLocationBasis);
        validate(ContractRefPosType.Other);
    });

    test('should be immutable', () => {
        const ref = ContractRefPosType;
        expect(() => {
            ref.TwoComponentIntercommoditySpread = 10;
        }).toThrowError();
    });
});
