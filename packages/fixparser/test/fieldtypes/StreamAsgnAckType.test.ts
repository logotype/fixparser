import { StreamAsgnAckType } from '../../src/fieldtypes/StreamAsgnAckType';

describe('StreamAsgnAckType', () => {
    test('should have the correct values', () => {
        expect(StreamAsgnAckType.AssignmentAccepted).toBe(0);
        expect(StreamAsgnAckType.AssignmentRejected).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamAsgnAckType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StreamAsgnAckType.AssignmentAccepted, StreamAsgnAckType.AssignmentRejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamAsgnAckType type', () => {
        const validate = (value: StreamAsgnAckType) => {
            expect(Object.values(StreamAsgnAckType)).toContain(value);
        };

        validate(StreamAsgnAckType.AssignmentAccepted);
        validate(StreamAsgnAckType.AssignmentRejected);
    });

    test('should be immutable', () => {
        const ref = StreamAsgnAckType;
        expect(() => {
            ref.AssignmentAccepted = 10;
        }).toThrowError();
    });
});
