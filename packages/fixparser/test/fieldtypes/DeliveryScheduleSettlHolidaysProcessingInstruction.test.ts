import { DeliveryScheduleSettlHolidaysProcessingInstruction } from '../../src/fieldtypes/DeliveryScheduleSettlHolidaysProcessingInstruction';

describe('DeliveryScheduleSettlHolidaysProcessingInstruction', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleSettlHolidaysProcessingInstruction.DoNotIncludeHolidays).toBe(0);
        expect(DeliveryScheduleSettlHolidaysProcessingInstruction.IncludeHolidays).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleSettlHolidaysProcessingInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryScheduleSettlHolidaysProcessingInstruction.DoNotIncludeHolidays,
            DeliveryScheduleSettlHolidaysProcessingInstruction.IncludeHolidays,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleSettlHolidaysProcessingInstruction type', () => {
        const validate = (value: DeliveryScheduleSettlHolidaysProcessingInstruction) => {
            expect(Object.values(DeliveryScheduleSettlHolidaysProcessingInstruction)).toContain(value);
        };

        validate(DeliveryScheduleSettlHolidaysProcessingInstruction.DoNotIncludeHolidays);
        validate(DeliveryScheduleSettlHolidaysProcessingInstruction.IncludeHolidays);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleSettlHolidaysProcessingInstruction;
        expect(() => {
            ref.DoNotIncludeHolidays = 10;
        }).toThrowError();
    });
});
