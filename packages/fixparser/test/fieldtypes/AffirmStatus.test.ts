import { AffirmStatus } from '../../src/fieldtypes/AffirmStatus';

describe('AffirmStatus', () => {
    test('should have the correct values', () => {
        expect(AffirmStatus.Received).toBe(1);
        expect(AffirmStatus.ConfirmRejected).toBe(2);
        expect(AffirmStatus.Affirmed).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AffirmStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AffirmStatus.Received, AffirmStatus.ConfirmRejected, AffirmStatus.Affirmed];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AffirmStatus type', () => {
        const validate = (value: AffirmStatus) => {
            expect(Object.values(AffirmStatus)).toContain(value);
        };

        validate(AffirmStatus.Received);
        validate(AffirmStatus.ConfirmRejected);
        validate(AffirmStatus.Affirmed);
    });

    test('should be immutable', () => {
        const ref = AffirmStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
