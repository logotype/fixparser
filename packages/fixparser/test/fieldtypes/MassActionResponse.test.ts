import { MassActionResponse } from '../../src/fieldtypes/MassActionResponse';

describe('MassActionResponse', () => {
    test('should have the correct values', () => {
        expect(MassActionResponse.Rejected).toBe(0);
        expect(MassActionResponse.Accepted).toBe(1);
        expect(MassActionResponse.Completed).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassActionResponse.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassActionResponse.Rejected,
            MassActionResponse.Accepted,
            MassActionResponse.Completed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassActionResponse type', () => {
        const validate = (value: MassActionResponse) => {
            expect(Object.values(MassActionResponse)).toContain(value);
        };

        validate(MassActionResponse.Rejected);
        validate(MassActionResponse.Accepted);
        validate(MassActionResponse.Completed);
    });

    test('should be immutable', () => {
        const ref = MassActionResponse;
        expect(() => {
            ref.Rejected = 10;
        }).toThrowError();
    });
});
