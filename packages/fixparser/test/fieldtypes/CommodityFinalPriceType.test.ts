import { CommodityFinalPriceType } from '../../src/fieldtypes/CommodityFinalPriceType';

describe('CommodityFinalPriceType', () => {
    test('should have the correct values', () => {
        expect(CommodityFinalPriceType.ArgusMcCloskey).toBe(0);
        expect(CommodityFinalPriceType.Baltic).toBe(1);
        expect(CommodityFinalPriceType.Exchange).toBe(2);
        expect(CommodityFinalPriceType.GlobalCoal).toBe(3);
        expect(CommodityFinalPriceType.IHSMcCloskey).toBe(4);
        expect(CommodityFinalPriceType.Platts).toBe(5);
        expect(CommodityFinalPriceType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CommodityFinalPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CommodityFinalPriceType.ArgusMcCloskey,
            CommodityFinalPriceType.Baltic,
            CommodityFinalPriceType.Exchange,
            CommodityFinalPriceType.GlobalCoal,
            CommodityFinalPriceType.IHSMcCloskey,
            CommodityFinalPriceType.Platts,
            CommodityFinalPriceType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CommodityFinalPriceType type', () => {
        const validate = (value: CommodityFinalPriceType) => {
            expect(Object.values(CommodityFinalPriceType)).toContain(value);
        };

        validate(CommodityFinalPriceType.ArgusMcCloskey);
        validate(CommodityFinalPriceType.Baltic);
        validate(CommodityFinalPriceType.Exchange);
        validate(CommodityFinalPriceType.GlobalCoal);
        validate(CommodityFinalPriceType.IHSMcCloskey);
        validate(CommodityFinalPriceType.Platts);
        validate(CommodityFinalPriceType.Other);
    });

    test('should be immutable', () => {
        const ref = CommodityFinalPriceType;
        expect(() => {
            ref.ArgusMcCloskey = 10;
        }).toThrowError();
    });
});
