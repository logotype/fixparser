import { TradeAllocGroupInstruction } from '../../src/fieldtypes/TradeAllocGroupInstruction';

describe('TradeAllocGroupInstruction', () => {
    test('should have the correct values', () => {
        expect(TradeAllocGroupInstruction.Add).toBe(0);
        expect(TradeAllocGroupInstruction.DoNotAdd).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAllocGroupInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradeAllocGroupInstruction.Add, TradeAllocGroupInstruction.DoNotAdd];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAllocGroupInstruction type', () => {
        const validate = (value: TradeAllocGroupInstruction) => {
            expect(Object.values(TradeAllocGroupInstruction)).toContain(value);
        };

        validate(TradeAllocGroupInstruction.Add);
        validate(TradeAllocGroupInstruction.DoNotAdd);
    });

    test('should be immutable', () => {
        const ref = TradeAllocGroupInstruction;
        expect(() => {
            ref.Add = 10;
        }).toThrowError();
    });
});
