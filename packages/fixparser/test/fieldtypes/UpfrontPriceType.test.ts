import { UpfrontPriceType } from '../../src/fieldtypes/UpfrontPriceType';

describe('UpfrontPriceType', () => {
    test('should have the correct values', () => {
        expect(UpfrontPriceType.Percentage).toBe(1);
        expect(UpfrontPriceType.FixedAmount).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UpfrontPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [UpfrontPriceType.Percentage, UpfrontPriceType.FixedAmount];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UpfrontPriceType type', () => {
        const validate = (value: UpfrontPriceType) => {
            expect(Object.values(UpfrontPriceType)).toContain(value);
        };

        validate(UpfrontPriceType.Percentage);
        validate(UpfrontPriceType.FixedAmount);
    });

    test('should be immutable', () => {
        const ref = UpfrontPriceType;
        expect(() => {
            ref.Percentage = 10;
        }).toThrowError();
    });
});
