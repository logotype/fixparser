import { QuoteModelType } from '../../src/fieldtypes/QuoteModelType';

describe('QuoteModelType', () => {
    test('should have the correct values', () => {
        expect(QuoteModelType.QuoteEntry).toBe(1);
        expect(QuoteModelType.QuoteModification).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteModelType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [QuoteModelType.QuoteEntry, QuoteModelType.QuoteModification];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteModelType type', () => {
        const validate = (value: QuoteModelType) => {
            expect(Object.values(QuoteModelType)).toContain(value);
        };

        validate(QuoteModelType.QuoteEntry);
        validate(QuoteModelType.QuoteModification);
    });

    test('should be immutable', () => {
        const ref = QuoteModelType;
        expect(() => {
            ref.QuoteEntry = 10;
        }).toThrowError();
    });
});
