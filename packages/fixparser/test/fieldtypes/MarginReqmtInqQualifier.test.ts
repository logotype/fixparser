import { MarginReqmtInqQualifier } from '../../src/fieldtypes/MarginReqmtInqQualifier';

describe('MarginReqmtInqQualifier', () => {
    test('should have the correct values', () => {
        expect(MarginReqmtInqQualifier.Summary).toBe(0);
        expect(MarginReqmtInqQualifier.Detail).toBe(1);
        expect(MarginReqmtInqQualifier.ExcessDeficit).toBe(2);
        expect(MarginReqmtInqQualifier.NetPosition).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarginReqmtInqQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarginReqmtInqQualifier.Summary,
            MarginReqmtInqQualifier.Detail,
            MarginReqmtInqQualifier.ExcessDeficit,
            MarginReqmtInqQualifier.NetPosition,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarginReqmtInqQualifier type', () => {
        const validate = (value: MarginReqmtInqQualifier) => {
            expect(Object.values(MarginReqmtInqQualifier)).toContain(value);
        };

        validate(MarginReqmtInqQualifier.Summary);
        validate(MarginReqmtInqQualifier.Detail);
        validate(MarginReqmtInqQualifier.ExcessDeficit);
        validate(MarginReqmtInqQualifier.NetPosition);
    });

    test('should be immutable', () => {
        const ref = MarginReqmtInqQualifier;
        expect(() => {
            ref.Summary = 10;
        }).toThrowError();
    });
});
