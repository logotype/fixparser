import { Urgency } from '../../src/fieldtypes/Urgency';

describe('Urgency', () => {
    test('should have the correct values', () => {
        expect(Urgency.Normal).toBe('0');
        expect(Urgency.Flash).toBe('1');
        expect(Urgency.Background).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Urgency.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [Urgency.Normal, Urgency.Flash, Urgency.Background];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Urgency type', () => {
        const validate = (value: Urgency) => {
            expect(Object.values(Urgency)).toContain(value);
        };

        validate(Urgency.Normal);
        validate(Urgency.Flash);
        validate(Urgency.Background);
    });

    test('should be immutable', () => {
        const ref = Urgency;
        expect(() => {
            ref.Normal = 10;
        }).toThrowError();
    });
});
