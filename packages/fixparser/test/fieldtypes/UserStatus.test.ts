import { UserStatus } from '../../src/fieldtypes/UserStatus';

describe('UserStatus', () => {
    test('should have the correct values', () => {
        expect(UserStatus.LoggedIn).toBe(1);
        expect(UserStatus.NotLoggedIn).toBe(2);
        expect(UserStatus.UserNotRecognised).toBe(3);
        expect(UserStatus.PasswordIncorrect).toBe(4);
        expect(UserStatus.PasswordChanged).toBe(5);
        expect(UserStatus.Other).toBe(6);
        expect(UserStatus.ForcedUserLogoutByExchange).toBe(7);
        expect(UserStatus.SessionShutdownWarning).toBe(8);
        expect(UserStatus.ThrottleParametersChanged).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UserStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UserStatus.LoggedIn,
            UserStatus.NotLoggedIn,
            UserStatus.UserNotRecognised,
            UserStatus.PasswordIncorrect,
            UserStatus.PasswordChanged,
            UserStatus.Other,
            UserStatus.ForcedUserLogoutByExchange,
            UserStatus.SessionShutdownWarning,
            UserStatus.ThrottleParametersChanged,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UserStatus type', () => {
        const validate = (value: UserStatus) => {
            expect(Object.values(UserStatus)).toContain(value);
        };

        validate(UserStatus.LoggedIn);
        validate(UserStatus.NotLoggedIn);
        validate(UserStatus.UserNotRecognised);
        validate(UserStatus.PasswordIncorrect);
        validate(UserStatus.PasswordChanged);
        validate(UserStatus.Other);
        validate(UserStatus.ForcedUserLogoutByExchange);
        validate(UserStatus.SessionShutdownWarning);
        validate(UserStatus.ThrottleParametersChanged);
    });

    test('should be immutable', () => {
        const ref = UserStatus;
        expect(() => {
            ref.LoggedIn = 10;
        }).toThrowError();
    });
});
