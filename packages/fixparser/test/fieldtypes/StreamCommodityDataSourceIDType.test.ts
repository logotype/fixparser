import { StreamCommodityDataSourceIDType } from '../../src/fieldtypes/StreamCommodityDataSourceIDType';

describe('StreamCommodityDataSourceIDType', () => {
    test('should have the correct values', () => {
        expect(StreamCommodityDataSourceIDType.City).toBe(0);
        expect(StreamCommodityDataSourceIDType.Airport).toBe(1);
        expect(StreamCommodityDataSourceIDType.WeatherStation).toBe(2);
        expect(StreamCommodityDataSourceIDType.WeatherIndex).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamCommodityDataSourceIDType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StreamCommodityDataSourceIDType.City,
            StreamCommodityDataSourceIDType.Airport,
            StreamCommodityDataSourceIDType.WeatherStation,
            StreamCommodityDataSourceIDType.WeatherIndex,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamCommodityDataSourceIDType type', () => {
        const validate = (value: StreamCommodityDataSourceIDType) => {
            expect(Object.values(StreamCommodityDataSourceIDType)).toContain(value);
        };

        validate(StreamCommodityDataSourceIDType.City);
        validate(StreamCommodityDataSourceIDType.Airport);
        validate(StreamCommodityDataSourceIDType.WeatherStation);
        validate(StreamCommodityDataSourceIDType.WeatherIndex);
    });

    test('should be immutable', () => {
        const ref = StreamCommodityDataSourceIDType;
        expect(() => {
            ref.City = 10;
        }).toThrowError();
    });
});
