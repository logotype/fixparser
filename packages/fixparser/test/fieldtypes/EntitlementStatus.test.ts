import { EntitlementStatus } from '../../src/fieldtypes/EntitlementStatus';

describe('EntitlementStatus', () => {
    test('should have the correct values', () => {
        expect(EntitlementStatus.Accepted).toBe(0);
        expect(EntitlementStatus.AcceptedWithChanges).toBe(1);
        expect(EntitlementStatus.Rejected).toBe(2);
        expect(EntitlementStatus.Pending).toBe(3);
        expect(EntitlementStatus.Requested).toBe(4);
        expect(EntitlementStatus.Deferred).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EntitlementStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EntitlementStatus.Accepted,
            EntitlementStatus.AcceptedWithChanges,
            EntitlementStatus.Rejected,
            EntitlementStatus.Pending,
            EntitlementStatus.Requested,
            EntitlementStatus.Deferred,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EntitlementStatus type', () => {
        const validate = (value: EntitlementStatus) => {
            expect(Object.values(EntitlementStatus)).toContain(value);
        };

        validate(EntitlementStatus.Accepted);
        validate(EntitlementStatus.AcceptedWithChanges);
        validate(EntitlementStatus.Rejected);
        validate(EntitlementStatus.Pending);
        validate(EntitlementStatus.Requested);
        validate(EntitlementStatus.Deferred);
    });

    test('should be immutable', () => {
        const ref = EntitlementStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
