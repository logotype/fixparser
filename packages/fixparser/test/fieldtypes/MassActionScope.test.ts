import { MassActionScope } from '../../src/fieldtypes/MassActionScope';

describe('MassActionScope', () => {
    test('should have the correct values', () => {
        expect(MassActionScope.AllOrdersForASecurity).toBe(1);
        expect(MassActionScope.AllOrdersForAnUnderlyingSecurity).toBe(2);
        expect(MassActionScope.AllOrdersForAProduct).toBe(3);
        expect(MassActionScope.AllOrdersForACFICode).toBe(4);
        expect(MassActionScope.AllOrdersForASecurityType).toBe(5);
        expect(MassActionScope.AllOrdersForATradingSession).toBe(6);
        expect(MassActionScope.AllOrders).toBe(7);
        expect(MassActionScope.AllOrdersForAMarket).toBe(8);
        expect(MassActionScope.AllOrdersForAMarketSegment).toBe(9);
        expect(MassActionScope.AllOrdersForASecurityGroup).toBe(10);
        expect(MassActionScope.CancelForSecurityIssuer).toBe(11);
        expect(MassActionScope.CancelForIssuerOfUnderlyingSecurity).toBe(12);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassActionScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassActionScope.AllOrdersForASecurity,
            MassActionScope.AllOrdersForAnUnderlyingSecurity,
            MassActionScope.AllOrdersForAProduct,
            MassActionScope.AllOrdersForACFICode,
            MassActionScope.AllOrdersForASecurityType,
            MassActionScope.AllOrdersForATradingSession,
            MassActionScope.AllOrders,
            MassActionScope.AllOrdersForAMarket,
            MassActionScope.AllOrdersForAMarketSegment,
            MassActionScope.AllOrdersForASecurityGroup,
            MassActionScope.CancelForSecurityIssuer,
            MassActionScope.CancelForIssuerOfUnderlyingSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassActionScope type', () => {
        const validate = (value: MassActionScope) => {
            expect(Object.values(MassActionScope)).toContain(value);
        };

        validate(MassActionScope.AllOrdersForASecurity);
        validate(MassActionScope.AllOrdersForAnUnderlyingSecurity);
        validate(MassActionScope.AllOrdersForAProduct);
        validate(MassActionScope.AllOrdersForACFICode);
        validate(MassActionScope.AllOrdersForASecurityType);
        validate(MassActionScope.AllOrdersForATradingSession);
        validate(MassActionScope.AllOrders);
        validate(MassActionScope.AllOrdersForAMarket);
        validate(MassActionScope.AllOrdersForAMarketSegment);
        validate(MassActionScope.AllOrdersForASecurityGroup);
        validate(MassActionScope.CancelForSecurityIssuer);
        validate(MassActionScope.CancelForIssuerOfUnderlyingSecurity);
    });

    test('should be immutable', () => {
        const ref = MassActionScope;
        expect(() => {
            ref.AllOrdersForASecurity = 10;
        }).toThrowError();
    });
});
