import { MaturityMonthYearFormat } from '../../src/fieldtypes/MaturityMonthYearFormat';

describe('MaturityMonthYearFormat', () => {
    test('should have the correct values', () => {
        expect(MaturityMonthYearFormat.YearMonthOnly).toBe(0);
        expect(MaturityMonthYearFormat.YearMonthDay).toBe(1);
        expect(MaturityMonthYearFormat.YearMonthWeek).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MaturityMonthYearFormat.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MaturityMonthYearFormat.YearMonthOnly,
            MaturityMonthYearFormat.YearMonthDay,
            MaturityMonthYearFormat.YearMonthWeek,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MaturityMonthYearFormat type', () => {
        const validate = (value: MaturityMonthYearFormat) => {
            expect(Object.values(MaturityMonthYearFormat)).toContain(value);
        };

        validate(MaturityMonthYearFormat.YearMonthOnly);
        validate(MaturityMonthYearFormat.YearMonthDay);
        validate(MaturityMonthYearFormat.YearMonthWeek);
    });

    test('should be immutable', () => {
        const ref = MaturityMonthYearFormat;
        expect(() => {
            ref.YearMonthOnly = 10;
        }).toThrowError();
    });
});
