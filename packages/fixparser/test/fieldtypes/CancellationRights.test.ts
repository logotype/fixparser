import { CancellationRights } from '../../src/fieldtypes/CancellationRights';

describe('CancellationRights', () => {
    test('should have the correct values', () => {
        expect(CancellationRights.Yes).toBe('Y');
        expect(CancellationRights.NoExecutionOnly).toBe('N');
        expect(CancellationRights.NoWaiverAgreement).toBe('M');
        expect(CancellationRights.NoInstitutional).toBe('O');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CancellationRights.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CancellationRights.Yes,
            CancellationRights.NoExecutionOnly,
            CancellationRights.NoWaiverAgreement,
            CancellationRights.NoInstitutional,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CancellationRights type', () => {
        const validate = (value: CancellationRights) => {
            expect(Object.values(CancellationRights)).toContain(value);
        };

        validate(CancellationRights.Yes);
        validate(CancellationRights.NoExecutionOnly);
        validate(CancellationRights.NoWaiverAgreement);
        validate(CancellationRights.NoInstitutional);
    });

    test('should be immutable', () => {
        const ref = CancellationRights;
        expect(() => {
            ref.Yes = 10;
        }).toThrowError();
    });
});
