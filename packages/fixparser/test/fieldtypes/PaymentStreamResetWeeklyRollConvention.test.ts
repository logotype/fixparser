import { PaymentStreamResetWeeklyRollConvention } from '../../src/fieldtypes/PaymentStreamResetWeeklyRollConvention';

describe('PaymentStreamResetWeeklyRollConvention', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamResetWeeklyRollConvention.Monday).toBe('MON');
        expect(PaymentStreamResetWeeklyRollConvention.Tuesday).toBe('TUE');
        expect(PaymentStreamResetWeeklyRollConvention.Wednesday).toBe('WED');
        expect(PaymentStreamResetWeeklyRollConvention.Thursday).toBe('THU');
        expect(PaymentStreamResetWeeklyRollConvention.Friday).toBe('FRI');
        expect(PaymentStreamResetWeeklyRollConvention.Saturday).toBe('SAT');
        expect(PaymentStreamResetWeeklyRollConvention.Sunday).toBe('SUN');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamResetWeeklyRollConvention.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamResetWeeklyRollConvention.Monday,
            PaymentStreamResetWeeklyRollConvention.Tuesday,
            PaymentStreamResetWeeklyRollConvention.Wednesday,
            PaymentStreamResetWeeklyRollConvention.Thursday,
            PaymentStreamResetWeeklyRollConvention.Friday,
            PaymentStreamResetWeeklyRollConvention.Saturday,
            PaymentStreamResetWeeklyRollConvention.Sunday,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PaymentStreamResetWeeklyRollConvention type', () => {
        const validate = (value: PaymentStreamResetWeeklyRollConvention) => {
            expect(Object.values(PaymentStreamResetWeeklyRollConvention)).toContain(value);
        };

        validate(PaymentStreamResetWeeklyRollConvention.Monday);
        validate(PaymentStreamResetWeeklyRollConvention.Tuesday);
        validate(PaymentStreamResetWeeklyRollConvention.Wednesday);
        validate(PaymentStreamResetWeeklyRollConvention.Thursday);
        validate(PaymentStreamResetWeeklyRollConvention.Friday);
        validate(PaymentStreamResetWeeklyRollConvention.Saturday);
        validate(PaymentStreamResetWeeklyRollConvention.Sunday);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamResetWeeklyRollConvention;
        expect(() => {
            ref.Monday = 10;
        }).toThrowError();
    });
});
