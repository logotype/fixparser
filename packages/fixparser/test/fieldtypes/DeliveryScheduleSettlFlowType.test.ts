import { DeliveryScheduleSettlFlowType } from '../../src/fieldtypes/DeliveryScheduleSettlFlowType';

describe('DeliveryScheduleSettlFlowType', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleSettlFlowType.AllTimes).toBe(0);
        expect(DeliveryScheduleSettlFlowType.OnPeak).toBe(1);
        expect(DeliveryScheduleSettlFlowType.OffPeak).toBe(2);
        expect(DeliveryScheduleSettlFlowType.Base).toBe(3);
        expect(DeliveryScheduleSettlFlowType.BlockHours).toBe(4);
        expect(DeliveryScheduleSettlFlowType.Other).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleSettlFlowType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryScheduleSettlFlowType.AllTimes,
            DeliveryScheduleSettlFlowType.OnPeak,
            DeliveryScheduleSettlFlowType.OffPeak,
            DeliveryScheduleSettlFlowType.Base,
            DeliveryScheduleSettlFlowType.BlockHours,
            DeliveryScheduleSettlFlowType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleSettlFlowType type', () => {
        const validate = (value: DeliveryScheduleSettlFlowType) => {
            expect(Object.values(DeliveryScheduleSettlFlowType)).toContain(value);
        };

        validate(DeliveryScheduleSettlFlowType.AllTimes);
        validate(DeliveryScheduleSettlFlowType.OnPeak);
        validate(DeliveryScheduleSettlFlowType.OffPeak);
        validate(DeliveryScheduleSettlFlowType.Base);
        validate(DeliveryScheduleSettlFlowType.BlockHours);
        validate(DeliveryScheduleSettlFlowType.Other);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleSettlFlowType;
        expect(() => {
            ref.AllTimes = 10;
        }).toThrowError();
    });
});
