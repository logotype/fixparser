import { PaymentStreamFloorRateBuySide } from '../../src/fieldtypes/PaymentStreamFloorRateBuySide';

describe('PaymentStreamFloorRateBuySide', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamFloorRateBuySide.Buyer).toBe(1);
        expect(PaymentStreamFloorRateBuySide.Seller).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamFloorRateBuySide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamFloorRateBuySide.Buyer, PaymentStreamFloorRateBuySide.Seller];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamFloorRateBuySide type', () => {
        const validate = (value: PaymentStreamFloorRateBuySide) => {
            expect(Object.values(PaymentStreamFloorRateBuySide)).toContain(value);
        };

        validate(PaymentStreamFloorRateBuySide.Buyer);
        validate(PaymentStreamFloorRateBuySide.Seller);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamFloorRateBuySide;
        expect(() => {
            ref.Buyer = 10;
        }).toThrowError();
    });
});
