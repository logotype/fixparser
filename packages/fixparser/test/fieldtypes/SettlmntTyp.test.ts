import { SettlmntTyp } from '../../src/fieldtypes/SettlmntTyp';

describe('SettlmntTyp', () => {
    test('should have the correct values', () => {
        expect(SettlmntTyp.Regular).toBe('0');
        expect(SettlmntTyp.Cash).toBe('1');
        expect(SettlmntTyp.NextDay).toBe('2');
        expect(SettlmntTyp.TPlus2).toBe('3');
        expect(SettlmntTyp.TPlus3).toBe('4');
        expect(SettlmntTyp.TPlus4).toBe('5');
        expect(SettlmntTyp.Future).toBe('6');
        expect(SettlmntTyp.WhenAndIfIssued).toBe('7');
        expect(SettlmntTyp.SellersOption).toBe('8');
        expect(SettlmntTyp.TPlus5).toBe('9');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlmntTyp.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlmntTyp.Regular,
            SettlmntTyp.Cash,
            SettlmntTyp.NextDay,
            SettlmntTyp.TPlus2,
            SettlmntTyp.TPlus3,
            SettlmntTyp.TPlus4,
            SettlmntTyp.Future,
            SettlmntTyp.WhenAndIfIssued,
            SettlmntTyp.SellersOption,
            SettlmntTyp.TPlus5,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlmntTyp type', () => {
        const validate = (value: SettlmntTyp) => {
            expect(Object.values(SettlmntTyp)).toContain(value);
        };

        validate(SettlmntTyp.Regular);
        validate(SettlmntTyp.Cash);
        validate(SettlmntTyp.NextDay);
        validate(SettlmntTyp.TPlus2);
        validate(SettlmntTyp.TPlus3);
        validate(SettlmntTyp.TPlus4);
        validate(SettlmntTyp.Future);
        validate(SettlmntTyp.WhenAndIfIssued);
        validate(SettlmntTyp.SellersOption);
        validate(SettlmntTyp.TPlus5);
    });

    test('should be immutable', () => {
        const ref = SettlmntTyp;
        expect(() => {
            ref.Regular = 10;
        }).toThrowError();
    });
});
