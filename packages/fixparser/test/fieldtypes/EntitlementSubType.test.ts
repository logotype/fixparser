import { EntitlementSubType } from '../../src/fieldtypes/EntitlementSubType';

describe('EntitlementSubType', () => {
    test('should have the correct values', () => {
        expect(EntitlementSubType.OrderEntry).toBe(1);
        expect(EntitlementSubType.HItLift).toBe(2);
        expect(EntitlementSubType.ViewIndicativePx).toBe(3);
        expect(EntitlementSubType.ViewExecutablePx).toBe(4);
        expect(EntitlementSubType.SingleQuote).toBe(5);
        expect(EntitlementSubType.StreamingQuotes).toBe(6);
        expect(EntitlementSubType.SingleBroker).toBe(7);
        expect(EntitlementSubType.MultiBrokers).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EntitlementSubType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EntitlementSubType.OrderEntry,
            EntitlementSubType.HItLift,
            EntitlementSubType.ViewIndicativePx,
            EntitlementSubType.ViewExecutablePx,
            EntitlementSubType.SingleQuote,
            EntitlementSubType.StreamingQuotes,
            EntitlementSubType.SingleBroker,
            EntitlementSubType.MultiBrokers,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EntitlementSubType type', () => {
        const validate = (value: EntitlementSubType) => {
            expect(Object.values(EntitlementSubType)).toContain(value);
        };

        validate(EntitlementSubType.OrderEntry);
        validate(EntitlementSubType.HItLift);
        validate(EntitlementSubType.ViewIndicativePx);
        validate(EntitlementSubType.ViewExecutablePx);
        validate(EntitlementSubType.SingleQuote);
        validate(EntitlementSubType.StreamingQuotes);
        validate(EntitlementSubType.SingleBroker);
        validate(EntitlementSubType.MultiBrokers);
    });

    test('should be immutable', () => {
        const ref = EntitlementSubType;
        expect(() => {
            ref.OrderEntry = 10;
        }).toThrowError();
    });
});
