import { PartyRelationship } from '../../src/fieldtypes/PartyRelationship';

describe('PartyRelationship', () => {
    test('should have the correct values', () => {
        expect(PartyRelationship.IsAlso).toBe(0);
        expect(PartyRelationship.ClearsFor).toBe(1);
        expect(PartyRelationship.ClearsThrough).toBe(2);
        expect(PartyRelationship.TradesFor).toBe(3);
        expect(PartyRelationship.TradesThrough).toBe(4);
        expect(PartyRelationship.Sponsors).toBe(5);
        expect(PartyRelationship.SponsoredThrough).toBe(6);
        expect(PartyRelationship.ProvidesGuaranteeFor).toBe(7);
        expect(PartyRelationship.IsGuaranteedBy).toBe(8);
        expect(PartyRelationship.MemberOf).toBe(9);
        expect(PartyRelationship.HasMembers).toBe(10);
        expect(PartyRelationship.ProvidesMarketplaceFor).toBe(11);
        expect(PartyRelationship.ParticipantOfMarketplace).toBe(12);
        expect(PartyRelationship.CarriesPositionsFor).toBe(13);
        expect(PartyRelationship.PostsTradesTo).toBe(14);
        expect(PartyRelationship.EntersTradesFor).toBe(15);
        expect(PartyRelationship.EntersTradesThrough).toBe(16);
        expect(PartyRelationship.ProvidesQuotesTo).toBe(17);
        expect(PartyRelationship.RequestsQuotesFrom).toBe(18);
        expect(PartyRelationship.InvestsFor).toBe(19);
        expect(PartyRelationship.InvestsThrough).toBe(20);
        expect(PartyRelationship.BrokersTradesFor).toBe(21);
        expect(PartyRelationship.BrokersTradesThrough).toBe(22);
        expect(PartyRelationship.ProvidesTradingServicesFor).toBe(23);
        expect(PartyRelationship.UsesTradingServicesOf).toBe(24);
        expect(PartyRelationship.ApprovesOf).toBe(25);
        expect(PartyRelationship.ApprovedBy).toBe(26);
        expect(PartyRelationship.ParentFirmFor).toBe(27);
        expect(PartyRelationship.SubsidiaryOf).toBe(28);
        expect(PartyRelationship.RegulatoryOwnerOf).toBe(29);
        expect(PartyRelationship.OwnedByRegulatory).toBe(30);
        expect(PartyRelationship.Controls).toBe(31);
        expect(PartyRelationship.IsControlledBy).toBe(32);
        expect(PartyRelationship.LegalOwnerOf).toBe(33);
        expect(PartyRelationship.OwnedByLegal).toBe(34);
        expect(PartyRelationship.BeneficialOwnerOf).toBe(35);
        expect(PartyRelationship.OwnedByBeneficial).toBe(36);
        expect(PartyRelationship.SettlesFor).toBe(37);
        expect(PartyRelationship.SettlesThrough).toBe(38);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyRelationship.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyRelationship.IsAlso,
            PartyRelationship.ClearsFor,
            PartyRelationship.ClearsThrough,
            PartyRelationship.TradesFor,
            PartyRelationship.TradesThrough,
            PartyRelationship.Sponsors,
            PartyRelationship.SponsoredThrough,
            PartyRelationship.ProvidesGuaranteeFor,
            PartyRelationship.IsGuaranteedBy,
            PartyRelationship.MemberOf,
            PartyRelationship.HasMembers,
            PartyRelationship.ProvidesMarketplaceFor,
            PartyRelationship.ParticipantOfMarketplace,
            PartyRelationship.CarriesPositionsFor,
            PartyRelationship.PostsTradesTo,
            PartyRelationship.EntersTradesFor,
            PartyRelationship.EntersTradesThrough,
            PartyRelationship.ProvidesQuotesTo,
            PartyRelationship.RequestsQuotesFrom,
            PartyRelationship.InvestsFor,
            PartyRelationship.InvestsThrough,
            PartyRelationship.BrokersTradesFor,
            PartyRelationship.BrokersTradesThrough,
            PartyRelationship.ProvidesTradingServicesFor,
            PartyRelationship.UsesTradingServicesOf,
            PartyRelationship.ApprovesOf,
            PartyRelationship.ApprovedBy,
            PartyRelationship.ParentFirmFor,
            PartyRelationship.SubsidiaryOf,
            PartyRelationship.RegulatoryOwnerOf,
            PartyRelationship.OwnedByRegulatory,
            PartyRelationship.Controls,
            PartyRelationship.IsControlledBy,
            PartyRelationship.LegalOwnerOf,
            PartyRelationship.OwnedByLegal,
            PartyRelationship.BeneficialOwnerOf,
            PartyRelationship.OwnedByBeneficial,
            PartyRelationship.SettlesFor,
            PartyRelationship.SettlesThrough,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyRelationship type', () => {
        const validate = (value: PartyRelationship) => {
            expect(Object.values(PartyRelationship)).toContain(value);
        };

        validate(PartyRelationship.IsAlso);
        validate(PartyRelationship.ClearsFor);
        validate(PartyRelationship.ClearsThrough);
        validate(PartyRelationship.TradesFor);
        validate(PartyRelationship.TradesThrough);
        validate(PartyRelationship.Sponsors);
        validate(PartyRelationship.SponsoredThrough);
        validate(PartyRelationship.ProvidesGuaranteeFor);
        validate(PartyRelationship.IsGuaranteedBy);
        validate(PartyRelationship.MemberOf);
        validate(PartyRelationship.HasMembers);
        validate(PartyRelationship.ProvidesMarketplaceFor);
        validate(PartyRelationship.ParticipantOfMarketplace);
        validate(PartyRelationship.CarriesPositionsFor);
        validate(PartyRelationship.PostsTradesTo);
        validate(PartyRelationship.EntersTradesFor);
        validate(PartyRelationship.EntersTradesThrough);
        validate(PartyRelationship.ProvidesQuotesTo);
        validate(PartyRelationship.RequestsQuotesFrom);
        validate(PartyRelationship.InvestsFor);
        validate(PartyRelationship.InvestsThrough);
        validate(PartyRelationship.BrokersTradesFor);
        validate(PartyRelationship.BrokersTradesThrough);
        validate(PartyRelationship.ProvidesTradingServicesFor);
        validate(PartyRelationship.UsesTradingServicesOf);
        validate(PartyRelationship.ApprovesOf);
        validate(PartyRelationship.ApprovedBy);
        validate(PartyRelationship.ParentFirmFor);
        validate(PartyRelationship.SubsidiaryOf);
        validate(PartyRelationship.RegulatoryOwnerOf);
        validate(PartyRelationship.OwnedByRegulatory);
        validate(PartyRelationship.Controls);
        validate(PartyRelationship.IsControlledBy);
        validate(PartyRelationship.LegalOwnerOf);
        validate(PartyRelationship.OwnedByLegal);
        validate(PartyRelationship.BeneficialOwnerOf);
        validate(PartyRelationship.OwnedByBeneficial);
        validate(PartyRelationship.SettlesFor);
        validate(PartyRelationship.SettlesThrough);
    });

    test('should be immutable', () => {
        const ref = PartyRelationship;
        expect(() => {
            ref.IsAlso = 10;
        }).toThrowError();
    });
});
