import { ExecPriceType } from '../../src/fieldtypes/ExecPriceType';

describe('ExecPriceType', () => {
    test('should have the correct values', () => {
        expect(ExecPriceType.BidPrice).toBe('B');
        expect(ExecPriceType.CreationPrice).toBe('C');
        expect(ExecPriceType.CreationPricePlusAdjustmentPercent).toBe('D');
        expect(ExecPriceType.CreationPricePlusAdjustmentAmount).toBe('E');
        expect(ExecPriceType.OfferPrice).toBe('O');
        expect(ExecPriceType.OfferPriceMinusAdjustmentPercent).toBe('P');
        expect(ExecPriceType.OfferPriceMinusAdjustmentAmount).toBe('Q');
        expect(ExecPriceType.SinglePrice).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecPriceType.BidPrice,
            ExecPriceType.CreationPrice,
            ExecPriceType.CreationPricePlusAdjustmentPercent,
            ExecPriceType.CreationPricePlusAdjustmentAmount,
            ExecPriceType.OfferPrice,
            ExecPriceType.OfferPriceMinusAdjustmentPercent,
            ExecPriceType.OfferPriceMinusAdjustmentAmount,
            ExecPriceType.SinglePrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExecPriceType type', () => {
        const validate = (value: ExecPriceType) => {
            expect(Object.values(ExecPriceType)).toContain(value);
        };

        validate(ExecPriceType.BidPrice);
        validate(ExecPriceType.CreationPrice);
        validate(ExecPriceType.CreationPricePlusAdjustmentPercent);
        validate(ExecPriceType.CreationPricePlusAdjustmentAmount);
        validate(ExecPriceType.OfferPrice);
        validate(ExecPriceType.OfferPriceMinusAdjustmentPercent);
        validate(ExecPriceType.OfferPriceMinusAdjustmentAmount);
        validate(ExecPriceType.SinglePrice);
    });

    test('should be immutable', () => {
        const ref = ExecPriceType;
        expect(() => {
            ref.BidPrice = 10;
        }).toThrowError();
    });
});
