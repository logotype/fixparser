import { TradeCollateralization } from '../../src/fieldtypes/TradeCollateralization';

describe('TradeCollateralization', () => {
    test('should have the correct values', () => {
        expect(TradeCollateralization.Uncollateralized).toBe(0);
        expect(TradeCollateralization.PartiallyCollateralized).toBe(1);
        expect(TradeCollateralization.OneWayCollaterallization).toBe(2);
        expect(TradeCollateralization.FullyCollateralized).toBe(3);
        expect(TradeCollateralization.NetExposure).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeCollateralization.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeCollateralization.Uncollateralized,
            TradeCollateralization.PartiallyCollateralized,
            TradeCollateralization.OneWayCollaterallization,
            TradeCollateralization.FullyCollateralized,
            TradeCollateralization.NetExposure,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeCollateralization type', () => {
        const validate = (value: TradeCollateralization) => {
            expect(Object.values(TradeCollateralization)).toContain(value);
        };

        validate(TradeCollateralization.Uncollateralized);
        validate(TradeCollateralization.PartiallyCollateralized);
        validate(TradeCollateralization.OneWayCollaterallization);
        validate(TradeCollateralization.FullyCollateralized);
        validate(TradeCollateralization.NetExposure);
    });

    test('should be immutable', () => {
        const ref = TradeCollateralization;
        expect(() => {
            ref.Uncollateralized = 10;
        }).toThrowError();
    });
});
