import { BusinessRejectReason } from '../../src/fieldtypes/BusinessRejectReason';

describe('BusinessRejectReason', () => {
    test('should have the correct values', () => {
        expect(BusinessRejectReason.Other).toBe(0);
        expect(BusinessRejectReason.UnknownID).toBe(1);
        expect(BusinessRejectReason.UnknownSecurity).toBe(2);
        expect(BusinessRejectReason.UnsupportedMessageType).toBe(3);
        expect(BusinessRejectReason.ApplicationNotAvailable).toBe(4);
        expect(BusinessRejectReason.ConditionallyRequiredFieldMissing).toBe(5);
        expect(BusinessRejectReason.NotAuthorized).toBe(6);
        expect(BusinessRejectReason.DeliverToFirmNotAvailableAtThisTime).toBe(7);
        expect(BusinessRejectReason.ThrottleLimitExceeded).toBe(8);
        expect(BusinessRejectReason.ThrottleLimitExceededSessionDisconnected).toBe(9);
        expect(BusinessRejectReason.ThrottledMessagesRejectedOnRequest).toBe(10);
        expect(BusinessRejectReason.InvalidPriceIncrement).toBe(18);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BusinessRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BusinessRejectReason.Other,
            BusinessRejectReason.UnknownID,
            BusinessRejectReason.UnknownSecurity,
            BusinessRejectReason.UnsupportedMessageType,
            BusinessRejectReason.ApplicationNotAvailable,
            BusinessRejectReason.ConditionallyRequiredFieldMissing,
            BusinessRejectReason.NotAuthorized,
            BusinessRejectReason.DeliverToFirmNotAvailableAtThisTime,
            BusinessRejectReason.ThrottleLimitExceeded,
            BusinessRejectReason.ThrottleLimitExceededSessionDisconnected,
            BusinessRejectReason.ThrottledMessagesRejectedOnRequest,
            BusinessRejectReason.InvalidPriceIncrement,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BusinessRejectReason type', () => {
        const validate = (value: BusinessRejectReason) => {
            expect(Object.values(BusinessRejectReason)).toContain(value);
        };

        validate(BusinessRejectReason.Other);
        validate(BusinessRejectReason.UnknownID);
        validate(BusinessRejectReason.UnknownSecurity);
        validate(BusinessRejectReason.UnsupportedMessageType);
        validate(BusinessRejectReason.ApplicationNotAvailable);
        validate(BusinessRejectReason.ConditionallyRequiredFieldMissing);
        validate(BusinessRejectReason.NotAuthorized);
        validate(BusinessRejectReason.DeliverToFirmNotAvailableAtThisTime);
        validate(BusinessRejectReason.ThrottleLimitExceeded);
        validate(BusinessRejectReason.ThrottleLimitExceededSessionDisconnected);
        validate(BusinessRejectReason.ThrottledMessagesRejectedOnRequest);
        validate(BusinessRejectReason.InvalidPriceIncrement);
    });

    test('should be immutable', () => {
        const ref = BusinessRejectReason;
        expect(() => {
            ref.Other = 10;
        }).toThrowError();
    });
});
