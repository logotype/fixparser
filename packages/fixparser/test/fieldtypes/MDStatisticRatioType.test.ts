import { MDStatisticRatioType } from '../../src/fieldtypes/MDStatisticRatioType';

describe('MDStatisticRatioType', () => {
    test('should have the correct values', () => {
        expect(MDStatisticRatioType.BuyersToSellers).toBe(1);
        expect(MDStatisticRatioType.UpticksToDownticks).toBe(2);
        expect(MDStatisticRatioType.MarketMakerToNonMarketMaker).toBe(3);
        expect(MDStatisticRatioType.AutomatedToNonAutomated).toBe(4);
        expect(MDStatisticRatioType.OrdersToTrades).toBe(5);
        expect(MDStatisticRatioType.QuotesToTrades).toBe(6);
        expect(MDStatisticRatioType.OrdersAndQuotesToTrades).toBe(7);
        expect(MDStatisticRatioType.FailedToTotalTradedValue).toBe(8);
        expect(MDStatisticRatioType.BenefitsToTotalTradedValue).toBe(9);
        expect(MDStatisticRatioType.FeesToTotalTradedValue).toBe(10);
        expect(MDStatisticRatioType.TradeVolumeToTotalTradedVolume).toBe(11);
        expect(MDStatisticRatioType.OrdersToTotalNumberOrders).toBe(12);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticRatioType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticRatioType.BuyersToSellers,
            MDStatisticRatioType.UpticksToDownticks,
            MDStatisticRatioType.MarketMakerToNonMarketMaker,
            MDStatisticRatioType.AutomatedToNonAutomated,
            MDStatisticRatioType.OrdersToTrades,
            MDStatisticRatioType.QuotesToTrades,
            MDStatisticRatioType.OrdersAndQuotesToTrades,
            MDStatisticRatioType.FailedToTotalTradedValue,
            MDStatisticRatioType.BenefitsToTotalTradedValue,
            MDStatisticRatioType.FeesToTotalTradedValue,
            MDStatisticRatioType.TradeVolumeToTotalTradedVolume,
            MDStatisticRatioType.OrdersToTotalNumberOrders,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticRatioType type', () => {
        const validate = (value: MDStatisticRatioType) => {
            expect(Object.values(MDStatisticRatioType)).toContain(value);
        };

        validate(MDStatisticRatioType.BuyersToSellers);
        validate(MDStatisticRatioType.UpticksToDownticks);
        validate(MDStatisticRatioType.MarketMakerToNonMarketMaker);
        validate(MDStatisticRatioType.AutomatedToNonAutomated);
        validate(MDStatisticRatioType.OrdersToTrades);
        validate(MDStatisticRatioType.QuotesToTrades);
        validate(MDStatisticRatioType.OrdersAndQuotesToTrades);
        validate(MDStatisticRatioType.FailedToTotalTradedValue);
        validate(MDStatisticRatioType.BenefitsToTotalTradedValue);
        validate(MDStatisticRatioType.FeesToTotalTradedValue);
        validate(MDStatisticRatioType.TradeVolumeToTotalTradedVolume);
        validate(MDStatisticRatioType.OrdersToTotalNumberOrders);
    });

    test('should be immutable', () => {
        const ref = MDStatisticRatioType;
        expect(() => {
            ref.BuyersToSellers = 10;
        }).toThrowError();
    });
});
