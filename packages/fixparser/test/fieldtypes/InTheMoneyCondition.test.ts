import { InTheMoneyCondition } from '../../src/fieldtypes/InTheMoneyCondition';

describe('InTheMoneyCondition', () => {
    test('should have the correct values', () => {
        expect(InTheMoneyCondition.StandardITM).toBe(0);
        expect(InTheMoneyCondition.ATMITM).toBe(1);
        expect(InTheMoneyCondition.ATMCallITM).toBe(2);
        expect(InTheMoneyCondition.ATMPutITM).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            InTheMoneyCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            InTheMoneyCondition.StandardITM,
            InTheMoneyCondition.ATMITM,
            InTheMoneyCondition.ATMCallITM,
            InTheMoneyCondition.ATMPutITM,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for InTheMoneyCondition type', () => {
        const validate = (value: InTheMoneyCondition) => {
            expect(Object.values(InTheMoneyCondition)).toContain(value);
        };

        validate(InTheMoneyCondition.StandardITM);
        validate(InTheMoneyCondition.ATMITM);
        validate(InTheMoneyCondition.ATMCallITM);
        validate(InTheMoneyCondition.ATMPutITM);
    });

    test('should be immutable', () => {
        const ref = InTheMoneyCondition;
        expect(() => {
            ref.StandardITM = 10;
        }).toThrowError();
    });
});
