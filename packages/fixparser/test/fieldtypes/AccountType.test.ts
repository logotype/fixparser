import { AccountType } from '../../src/fieldtypes/AccountType';

describe('AccountType', () => {
    test('should have the correct values', () => {
        expect(AccountType.CarriedCustomerSide).toBe(1);
        expect(AccountType.CarriedNonCustomerSide).toBe(2);
        expect(AccountType.HouseTrader).toBe(3);
        expect(AccountType.FloorTrader).toBe(4);
        expect(AccountType.CarriedNonCustomerSideCrossMargined).toBe(6);
        expect(AccountType.HouseTraderCrossMargined).toBe(7);
        expect(AccountType.JointBackOfficeAccount).toBe(8);
        expect(AccountType.EquitiesSpecialist).toBe(9);
        expect(AccountType.OptionsMarketMaker).toBe(10);
        expect(AccountType.OptionsFirmAccount).toBe(11);
        expect(AccountType.AccountCustomerNonCustomerOrders).toBe(12);
        expect(AccountType.AccountOrdersMultipleCustomers).toBe(13);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AccountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AccountType.CarriedCustomerSide,
            AccountType.CarriedNonCustomerSide,
            AccountType.HouseTrader,
            AccountType.FloorTrader,
            AccountType.CarriedNonCustomerSideCrossMargined,
            AccountType.HouseTraderCrossMargined,
            AccountType.JointBackOfficeAccount,
            AccountType.EquitiesSpecialist,
            AccountType.OptionsMarketMaker,
            AccountType.OptionsFirmAccount,
            AccountType.AccountCustomerNonCustomerOrders,
            AccountType.AccountOrdersMultipleCustomers,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AccountType type', () => {
        const validate = (value: AccountType) => {
            expect(Object.values(AccountType)).toContain(value);
        };

        validate(AccountType.CarriedCustomerSide);
        validate(AccountType.CarriedNonCustomerSide);
        validate(AccountType.HouseTrader);
        validate(AccountType.FloorTrader);
        validate(AccountType.CarriedNonCustomerSideCrossMargined);
        validate(AccountType.HouseTraderCrossMargined);
        validate(AccountType.JointBackOfficeAccount);
        validate(AccountType.EquitiesSpecialist);
        validate(AccountType.OptionsMarketMaker);
        validate(AccountType.OptionsFirmAccount);
        validate(AccountType.AccountCustomerNonCustomerOrders);
        validate(AccountType.AccountOrdersMultipleCustomers);
    });

    test('should be immutable', () => {
        const ref = AccountType;
        expect(() => {
            ref.CarriedCustomerSide = 10;
        }).toThrowError();
    });
});
