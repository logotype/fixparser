import { LastLiquidityInd } from '../../src/fieldtypes/LastLiquidityInd';

describe('LastLiquidityInd', () => {
    test('should have the correct values', () => {
        expect(LastLiquidityInd.NeitherAddedNorRemovedLiquidity).toBe(0);
        expect(LastLiquidityInd.AddedLiquidity).toBe(1);
        expect(LastLiquidityInd.RemovedLiquidity).toBe(2);
        expect(LastLiquidityInd.LiquidityRoutedOut).toBe(3);
        expect(LastLiquidityInd.Auction).toBe(4);
        expect(LastLiquidityInd.TriggeredStopOrder).toBe(5);
        expect(LastLiquidityInd.TriggeredContingencyOrder).toBe(6);
        expect(LastLiquidityInd.TriggeredMarketOrder).toBe(7);
        expect(LastLiquidityInd.RemovedLiquidityAfterFirmOrderCommitment).toBe(8);
        expect(LastLiquidityInd.AuctionExecutionAfterFirmOrderCommitment).toBe(9);
        expect(LastLiquidityInd.Unknown).toBe(10);
        expect(LastLiquidityInd.Other).toBe(11);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LastLiquidityInd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LastLiquidityInd.NeitherAddedNorRemovedLiquidity,
            LastLiquidityInd.AddedLiquidity,
            LastLiquidityInd.RemovedLiquidity,
            LastLiquidityInd.LiquidityRoutedOut,
            LastLiquidityInd.Auction,
            LastLiquidityInd.TriggeredStopOrder,
            LastLiquidityInd.TriggeredContingencyOrder,
            LastLiquidityInd.TriggeredMarketOrder,
            LastLiquidityInd.RemovedLiquidityAfterFirmOrderCommitment,
            LastLiquidityInd.AuctionExecutionAfterFirmOrderCommitment,
            LastLiquidityInd.Unknown,
            LastLiquidityInd.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LastLiquidityInd type', () => {
        const validate = (value: LastLiquidityInd) => {
            expect(Object.values(LastLiquidityInd)).toContain(value);
        };

        validate(LastLiquidityInd.NeitherAddedNorRemovedLiquidity);
        validate(LastLiquidityInd.AddedLiquidity);
        validate(LastLiquidityInd.RemovedLiquidity);
        validate(LastLiquidityInd.LiquidityRoutedOut);
        validate(LastLiquidityInd.Auction);
        validate(LastLiquidityInd.TriggeredStopOrder);
        validate(LastLiquidityInd.TriggeredContingencyOrder);
        validate(LastLiquidityInd.TriggeredMarketOrder);
        validate(LastLiquidityInd.RemovedLiquidityAfterFirmOrderCommitment);
        validate(LastLiquidityInd.AuctionExecutionAfterFirmOrderCommitment);
        validate(LastLiquidityInd.Unknown);
        validate(LastLiquidityInd.Other);
    });

    test('should be immutable', () => {
        const ref = LastLiquidityInd;
        expect(() => {
            ref.NeitherAddedNorRemovedLiquidity = 10;
        }).toThrowError();
    });
});
