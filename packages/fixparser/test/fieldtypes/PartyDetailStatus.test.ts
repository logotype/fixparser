import { PartyDetailStatus } from '../../src/fieldtypes/PartyDetailStatus';

describe('PartyDetailStatus', () => {
    test('should have the correct values', () => {
        expect(PartyDetailStatus.Active).toBe(0);
        expect(PartyDetailStatus.Suspended).toBe(1);
        expect(PartyDetailStatus.Halted).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyDetailStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PartyDetailStatus.Active, PartyDetailStatus.Suspended, PartyDetailStatus.Halted];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyDetailStatus type', () => {
        const validate = (value: PartyDetailStatus) => {
            expect(Object.values(PartyDetailStatus)).toContain(value);
        };

        validate(PartyDetailStatus.Active);
        validate(PartyDetailStatus.Suspended);
        validate(PartyDetailStatus.Halted);
    });

    test('should be immutable', () => {
        const ref = PartyDetailStatus;
        expect(() => {
            ref.Active = 10;
        }).toThrowError();
    });
});
