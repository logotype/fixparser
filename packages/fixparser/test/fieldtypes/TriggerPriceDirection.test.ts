import { TriggerPriceDirection } from '../../src/fieldtypes/TriggerPriceDirection';

describe('TriggerPriceDirection', () => {
    test('should have the correct values', () => {
        expect(TriggerPriceDirection.Up).toBe('U');
        expect(TriggerPriceDirection.Down).toBe('D');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerPriceDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TriggerPriceDirection.Up, TriggerPriceDirection.Down];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerPriceDirection type', () => {
        const validate = (value: TriggerPriceDirection) => {
            expect(Object.values(TriggerPriceDirection)).toContain(value);
        };

        validate(TriggerPriceDirection.Up);
        validate(TriggerPriceDirection.Down);
    });

    test('should be immutable', () => {
        const ref = TriggerPriceDirection;
        expect(() => {
            ref.Up = 10;
        }).toThrowError();
    });
});
