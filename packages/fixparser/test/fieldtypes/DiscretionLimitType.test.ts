import { DiscretionLimitType } from '../../src/fieldtypes/DiscretionLimitType';

describe('DiscretionLimitType', () => {
    test('should have the correct values', () => {
        expect(DiscretionLimitType.OrBetter).toBe(0);
        expect(DiscretionLimitType.Strict).toBe(1);
        expect(DiscretionLimitType.OrWorse).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionLimitType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DiscretionLimitType.OrBetter,
            DiscretionLimitType.Strict,
            DiscretionLimitType.OrWorse,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DiscretionLimitType type', () => {
        const validate = (value: DiscretionLimitType) => {
            expect(Object.values(DiscretionLimitType)).toContain(value);
        };

        validate(DiscretionLimitType.OrBetter);
        validate(DiscretionLimitType.Strict);
        validate(DiscretionLimitType.OrWorse);
    });

    test('should be immutable', () => {
        const ref = DiscretionLimitType;
        expect(() => {
            ref.OrBetter = 10;
        }).toThrowError();
    });
});
