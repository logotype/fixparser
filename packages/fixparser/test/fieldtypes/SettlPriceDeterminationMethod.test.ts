import { SettlPriceDeterminationMethod } from '../../src/fieldtypes/SettlPriceDeterminationMethod';

describe('SettlPriceDeterminationMethod', () => {
    test('should have the correct values', () => {
        expect(SettlPriceDeterminationMethod.Unknown).toBe(0);
        expect(SettlPriceDeterminationMethod.LastTradePrice).toBe(1);
        expect(SettlPriceDeterminationMethod.LastBidPrice).toBe(2);
        expect(SettlPriceDeterminationMethod.LastOfferPrice).toBe(3);
        expect(SettlPriceDeterminationMethod.MidPrice).toBe(4);
        expect(SettlPriceDeterminationMethod.AverageLastTradePrice).toBe(5);
        expect(SettlPriceDeterminationMethod.AverageLastTradePeriod).toBe(6);
        expect(SettlPriceDeterminationMethod.UnderlyingPrice).toBe(7);
        expect(SettlPriceDeterminationMethod.CalculatedPrice).toBe(8);
        expect(SettlPriceDeterminationMethod.ManualPrice).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlPriceDeterminationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlPriceDeterminationMethod.Unknown,
            SettlPriceDeterminationMethod.LastTradePrice,
            SettlPriceDeterminationMethod.LastBidPrice,
            SettlPriceDeterminationMethod.LastOfferPrice,
            SettlPriceDeterminationMethod.MidPrice,
            SettlPriceDeterminationMethod.AverageLastTradePrice,
            SettlPriceDeterminationMethod.AverageLastTradePeriod,
            SettlPriceDeterminationMethod.UnderlyingPrice,
            SettlPriceDeterminationMethod.CalculatedPrice,
            SettlPriceDeterminationMethod.ManualPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlPriceDeterminationMethod type', () => {
        const validate = (value: SettlPriceDeterminationMethod) => {
            expect(Object.values(SettlPriceDeterminationMethod)).toContain(value);
        };

        validate(SettlPriceDeterminationMethod.Unknown);
        validate(SettlPriceDeterminationMethod.LastTradePrice);
        validate(SettlPriceDeterminationMethod.LastBidPrice);
        validate(SettlPriceDeterminationMethod.LastOfferPrice);
        validate(SettlPriceDeterminationMethod.MidPrice);
        validate(SettlPriceDeterminationMethod.AverageLastTradePrice);
        validate(SettlPriceDeterminationMethod.AverageLastTradePeriod);
        validate(SettlPriceDeterminationMethod.UnderlyingPrice);
        validate(SettlPriceDeterminationMethod.CalculatedPrice);
        validate(SettlPriceDeterminationMethod.ManualPrice);
    });

    test('should be immutable', () => {
        const ref = SettlPriceDeterminationMethod;
        expect(() => {
            ref.Unknown = 10;
        }).toThrowError();
    });
});
