import { SettlInstSource } from '../../src/fieldtypes/SettlInstSource';

describe('SettlInstSource', () => {
    test('should have the correct values', () => {
        expect(SettlInstSource.BrokerCredit).toBe('1');
        expect(SettlInstSource.Institution).toBe('2');
        expect(SettlInstSource.Investor).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlInstSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SettlInstSource.BrokerCredit, SettlInstSource.Institution, SettlInstSource.Investor];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlInstSource type', () => {
        const validate = (value: SettlInstSource) => {
            expect(Object.values(SettlInstSource)).toContain(value);
        };

        validate(SettlInstSource.BrokerCredit);
        validate(SettlInstSource.Institution);
        validate(SettlInstSource.Investor);
    });

    test('should be immutable', () => {
        const ref = SettlInstSource;
        expect(() => {
            ref.BrokerCredit = 10;
        }).toThrowError();
    });
});
