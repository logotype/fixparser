import { DueToRelated } from '../../src/fieldtypes/DueToRelated';

describe('DueToRelated', () => {
    test('should have the correct values', () => {
        expect(DueToRelated.NotRelatedToSecurityHalt).toBe('N');
        expect(DueToRelated.RelatedToSecurityHalt).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DueToRelated.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DueToRelated.NotRelatedToSecurityHalt, DueToRelated.RelatedToSecurityHalt];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DueToRelated type', () => {
        const validate = (value: DueToRelated) => {
            expect(Object.values(DueToRelated)).toContain(value);
        };

        validate(DueToRelated.NotRelatedToSecurityHalt);
        validate(DueToRelated.RelatedToSecurityHalt);
    });

    test('should be immutable', () => {
        const ref = DueToRelated;
        expect(() => {
            ref.NotRelatedToSecurityHalt = 10;
        }).toThrowError();
    });
});
