import { PaymentStreamPaymentDateOffsetUnit } from '../../src/fieldtypes/PaymentStreamPaymentDateOffsetUnit';

describe('PaymentStreamPaymentDateOffsetUnit', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamPaymentDateOffsetUnit.Day).toBe('D');
        expect(PaymentStreamPaymentDateOffsetUnit.Week).toBe('Wk');
        expect(PaymentStreamPaymentDateOffsetUnit.Month).toBe('Mo');
        expect(PaymentStreamPaymentDateOffsetUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamPaymentDateOffsetUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamPaymentDateOffsetUnit.Day,
            PaymentStreamPaymentDateOffsetUnit.Week,
            PaymentStreamPaymentDateOffsetUnit.Month,
            PaymentStreamPaymentDateOffsetUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PaymentStreamPaymentDateOffsetUnit type', () => {
        const validate = (value: PaymentStreamPaymentDateOffsetUnit) => {
            expect(Object.values(PaymentStreamPaymentDateOffsetUnit)).toContain(value);
        };

        validate(PaymentStreamPaymentDateOffsetUnit.Day);
        validate(PaymentStreamPaymentDateOffsetUnit.Week);
        validate(PaymentStreamPaymentDateOffsetUnit.Month);
        validate(PaymentStreamPaymentDateOffsetUnit.Year);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamPaymentDateOffsetUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
