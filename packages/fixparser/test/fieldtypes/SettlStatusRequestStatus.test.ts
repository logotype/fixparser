import { SettlStatusRequestStatus } from '../../src/fieldtypes/SettlStatusRequestStatus';

describe('SettlStatusRequestStatus', () => {
    test('should have the correct values', () => {
        expect(SettlStatusRequestStatus.Received).toBe(0);
        expect(SettlStatusRequestStatus.Accepted).toBe(1);
        expect(SettlStatusRequestStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlStatusRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlStatusRequestStatus.Received,
            SettlStatusRequestStatus.Accepted,
            SettlStatusRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlStatusRequestStatus type', () => {
        const validate = (value: SettlStatusRequestStatus) => {
            expect(Object.values(SettlStatusRequestStatus)).toContain(value);
        };

        validate(SettlStatusRequestStatus.Received);
        validate(SettlStatusRequestStatus.Accepted);
        validate(SettlStatusRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = SettlStatusRequestStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
