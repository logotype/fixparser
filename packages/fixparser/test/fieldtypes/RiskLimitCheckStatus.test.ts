import { RiskLimitCheckStatus } from '../../src/fieldtypes/RiskLimitCheckStatus';

describe('RiskLimitCheckStatus', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckStatus.Accepted).toBe(0);
        expect(RiskLimitCheckStatus.Rejected).toBe(1);
        expect(RiskLimitCheckStatus.ClaimRequired).toBe(2);
        expect(RiskLimitCheckStatus.PreDefinedLimitCheckSucceeded).toBe(3);
        expect(RiskLimitCheckStatus.PreDefinedLimitCheckFailed).toBe(4);
        expect(RiskLimitCheckStatus.PreDefinedAutoAcceptRuleInvoked).toBe(5);
        expect(RiskLimitCheckStatus.PreDefinedAutoRejectRuleInvoked).toBe(6);
        expect(RiskLimitCheckStatus.AcceptedByClearingFirm).toBe(7);
        expect(RiskLimitCheckStatus.RejectedByClearingFirm).toBe(8);
        expect(RiskLimitCheckStatus.Pending).toBe(9);
        expect(RiskLimitCheckStatus.AcceptedByCreditHub).toBe(10);
        expect(RiskLimitCheckStatus.RejectedByCreditHub).toBe(11);
        expect(RiskLimitCheckStatus.PendingCreditHubCheck).toBe(12);
        expect(RiskLimitCheckStatus.AcceptedByExecVenue).toBe(13);
        expect(RiskLimitCheckStatus.RejectedByExecVenue).toBe(14);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitCheckStatus.Accepted,
            RiskLimitCheckStatus.Rejected,
            RiskLimitCheckStatus.ClaimRequired,
            RiskLimitCheckStatus.PreDefinedLimitCheckSucceeded,
            RiskLimitCheckStatus.PreDefinedLimitCheckFailed,
            RiskLimitCheckStatus.PreDefinedAutoAcceptRuleInvoked,
            RiskLimitCheckStatus.PreDefinedAutoRejectRuleInvoked,
            RiskLimitCheckStatus.AcceptedByClearingFirm,
            RiskLimitCheckStatus.RejectedByClearingFirm,
            RiskLimitCheckStatus.Pending,
            RiskLimitCheckStatus.AcceptedByCreditHub,
            RiskLimitCheckStatus.RejectedByCreditHub,
            RiskLimitCheckStatus.PendingCreditHubCheck,
            RiskLimitCheckStatus.AcceptedByExecVenue,
            RiskLimitCheckStatus.RejectedByExecVenue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckStatus type', () => {
        const validate = (value: RiskLimitCheckStatus) => {
            expect(Object.values(RiskLimitCheckStatus)).toContain(value);
        };

        validate(RiskLimitCheckStatus.Accepted);
        validate(RiskLimitCheckStatus.Rejected);
        validate(RiskLimitCheckStatus.ClaimRequired);
        validate(RiskLimitCheckStatus.PreDefinedLimitCheckSucceeded);
        validate(RiskLimitCheckStatus.PreDefinedLimitCheckFailed);
        validate(RiskLimitCheckStatus.PreDefinedAutoAcceptRuleInvoked);
        validate(RiskLimitCheckStatus.PreDefinedAutoRejectRuleInvoked);
        validate(RiskLimitCheckStatus.AcceptedByClearingFirm);
        validate(RiskLimitCheckStatus.RejectedByClearingFirm);
        validate(RiskLimitCheckStatus.Pending);
        validate(RiskLimitCheckStatus.AcceptedByCreditHub);
        validate(RiskLimitCheckStatus.RejectedByCreditHub);
        validate(RiskLimitCheckStatus.PendingCreditHubCheck);
        validate(RiskLimitCheckStatus.AcceptedByExecVenue);
        validate(RiskLimitCheckStatus.RejectedByExecVenue);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
