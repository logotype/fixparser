import { ExpirationQtyType } from '../../src/fieldtypes/ExpirationQtyType';

describe('ExpirationQtyType', () => {
    test('should have the correct values', () => {
        expect(ExpirationQtyType.AutoExercise).toBe(1);
        expect(ExpirationQtyType.NonAutoExercise).toBe(2);
        expect(ExpirationQtyType.FinalWillBeExercised).toBe(3);
        expect(ExpirationQtyType.ContraryIntention).toBe(4);
        expect(ExpirationQtyType.Difference).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExpirationQtyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExpirationQtyType.AutoExercise,
            ExpirationQtyType.NonAutoExercise,
            ExpirationQtyType.FinalWillBeExercised,
            ExpirationQtyType.ContraryIntention,
            ExpirationQtyType.Difference,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExpirationQtyType type', () => {
        const validate = (value: ExpirationQtyType) => {
            expect(Object.values(ExpirationQtyType)).toContain(value);
        };

        validate(ExpirationQtyType.AutoExercise);
        validate(ExpirationQtyType.NonAutoExercise);
        validate(ExpirationQtyType.FinalWillBeExercised);
        validate(ExpirationQtyType.ContraryIntention);
        validate(ExpirationQtyType.Difference);
    });

    test('should be immutable', () => {
        const ref = ExpirationQtyType;
        expect(() => {
            ref.AutoExercise = 10;
        }).toThrowError();
    });
});
