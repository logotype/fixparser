import { EntitlementRequestResult } from '../../src/fieldtypes/EntitlementRequestResult';

describe('EntitlementRequestResult', () => {
    test('should have the correct values', () => {
        expect(EntitlementRequestResult.Successful).toBe(0);
        expect(EntitlementRequestResult.InvalidParty).toBe(1);
        expect(EntitlementRequestResult.InvalidRelatedParty).toBe(2);
        expect(EntitlementRequestResult.InvalidEntitlementType).toBe(3);
        expect(EntitlementRequestResult.InvalidEntitlementID).toBe(4);
        expect(EntitlementRequestResult.InvalidEntitlementAttribute).toBe(5);
        expect(EntitlementRequestResult.InvalidInstrumentScope).toBe(6);
        expect(EntitlementRequestResult.InvalidMarketSegmentScope).toBe(7);
        expect(EntitlementRequestResult.InvalidStartDate).toBe(8);
        expect(EntitlementRequestResult.InvalidEndDate).toBe(9);
        expect(EntitlementRequestResult.InstrumentScopeNotSupported).toBe(10);
        expect(EntitlementRequestResult.MarketSegmentScopeNotSupported).toBe(11);
        expect(EntitlementRequestResult.EntitlementNotApprovedForParty).toBe(12);
        expect(EntitlementRequestResult.EntitlementAlreadyDefinedForParty).toBe(13);
        expect(EntitlementRequestResult.InstrumentNotApprovedForParty).toBe(14);
        expect(EntitlementRequestResult.NotAuthorized).toBe(98);
        expect(EntitlementRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EntitlementRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EntitlementRequestResult.Successful,
            EntitlementRequestResult.InvalidParty,
            EntitlementRequestResult.InvalidRelatedParty,
            EntitlementRequestResult.InvalidEntitlementType,
            EntitlementRequestResult.InvalidEntitlementID,
            EntitlementRequestResult.InvalidEntitlementAttribute,
            EntitlementRequestResult.InvalidInstrumentScope,
            EntitlementRequestResult.InvalidMarketSegmentScope,
            EntitlementRequestResult.InvalidStartDate,
            EntitlementRequestResult.InvalidEndDate,
            EntitlementRequestResult.InstrumentScopeNotSupported,
            EntitlementRequestResult.MarketSegmentScopeNotSupported,
            EntitlementRequestResult.EntitlementNotApprovedForParty,
            EntitlementRequestResult.EntitlementAlreadyDefinedForParty,
            EntitlementRequestResult.InstrumentNotApprovedForParty,
            EntitlementRequestResult.NotAuthorized,
            EntitlementRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EntitlementRequestResult type', () => {
        const validate = (value: EntitlementRequestResult) => {
            expect(Object.values(EntitlementRequestResult)).toContain(value);
        };

        validate(EntitlementRequestResult.Successful);
        validate(EntitlementRequestResult.InvalidParty);
        validate(EntitlementRequestResult.InvalidRelatedParty);
        validate(EntitlementRequestResult.InvalidEntitlementType);
        validate(EntitlementRequestResult.InvalidEntitlementID);
        validate(EntitlementRequestResult.InvalidEntitlementAttribute);
        validate(EntitlementRequestResult.InvalidInstrumentScope);
        validate(EntitlementRequestResult.InvalidMarketSegmentScope);
        validate(EntitlementRequestResult.InvalidStartDate);
        validate(EntitlementRequestResult.InvalidEndDate);
        validate(EntitlementRequestResult.InstrumentScopeNotSupported);
        validate(EntitlementRequestResult.MarketSegmentScopeNotSupported);
        validate(EntitlementRequestResult.EntitlementNotApprovedForParty);
        validate(EntitlementRequestResult.EntitlementAlreadyDefinedForParty);
        validate(EntitlementRequestResult.InstrumentNotApprovedForParty);
        validate(EntitlementRequestResult.NotAuthorized);
        validate(EntitlementRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = EntitlementRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
