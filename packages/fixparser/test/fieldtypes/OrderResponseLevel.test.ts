import { OrderResponseLevel } from '../../src/fieldtypes/OrderResponseLevel';

describe('OrderResponseLevel', () => {
    test('should have the correct values', () => {
        expect(OrderResponseLevel.NoAck).toBe(0);
        expect(OrderResponseLevel.MinimumAck).toBe(1);
        expect(OrderResponseLevel.AckEach).toBe(2);
        expect(OrderResponseLevel.SummaryAck).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderResponseLevel.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderResponseLevel.NoAck,
            OrderResponseLevel.MinimumAck,
            OrderResponseLevel.AckEach,
            OrderResponseLevel.SummaryAck,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderResponseLevel type', () => {
        const validate = (value: OrderResponseLevel) => {
            expect(Object.values(OrderResponseLevel)).toContain(value);
        };

        validate(OrderResponseLevel.NoAck);
        validate(OrderResponseLevel.MinimumAck);
        validate(OrderResponseLevel.AckEach);
        validate(OrderResponseLevel.SummaryAck);
    });

    test('should be immutable', () => {
        const ref = OrderResponseLevel;
        expect(() => {
            ref.NoAck = 10;
        }).toThrowError();
    });
});
