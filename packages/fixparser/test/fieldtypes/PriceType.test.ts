import { PriceType } from '../../src/fieldtypes/PriceType';

describe('PriceType', () => {
    test('should have the correct values', () => {
        expect(PriceType.Percentage).toBe(1);
        expect(PriceType.PerUnit).toBe(2);
        expect(PriceType.FixedAmount).toBe(3);
        expect(PriceType.Discount).toBe(4);
        expect(PriceType.Premium).toBe(5);
        expect(PriceType.Spread).toBe(6);
        expect(PriceType.TEDPrice).toBe(7);
        expect(PriceType.TEDYield).toBe(8);
        expect(PriceType.Yield).toBe(9);
        expect(PriceType.FixedCabinetTradePrice).toBe(10);
        expect(PriceType.VariableCabinetTradePrice).toBe(11);
        expect(PriceType.PriceSpread).toBe(12);
        expect(PriceType.ProductTicksInHalves).toBe(13);
        expect(PriceType.ProductTicksInFourths).toBe(14);
        expect(PriceType.ProductTicksInEighths).toBe(15);
        expect(PriceType.ProductTicksInSixteenths).toBe(16);
        expect(PriceType.ProductTicksInThirtySeconds).toBe(17);
        expect(PriceType.ProductTicksInSixtyFourths).toBe(18);
        expect(PriceType.ProductTicksInOneTwentyEighths).toBe(19);
        expect(PriceType.NormalRateRepresentation).toBe(20);
        expect(PriceType.InverseRateRepresentation).toBe(21);
        expect(PriceType.BasisPoints).toBe(22);
        expect(PriceType.UpfrontPoints).toBe(23);
        expect(PriceType.InterestRate).toBe(24);
        expect(PriceType.PercentageNotional).toBe(25);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PriceType.Percentage,
            PriceType.PerUnit,
            PriceType.FixedAmount,
            PriceType.Discount,
            PriceType.Premium,
            PriceType.Spread,
            PriceType.TEDPrice,
            PriceType.TEDYield,
            PriceType.Yield,
            PriceType.FixedCabinetTradePrice,
            PriceType.VariableCabinetTradePrice,
            PriceType.PriceSpread,
            PriceType.ProductTicksInHalves,
            PriceType.ProductTicksInFourths,
            PriceType.ProductTicksInEighths,
            PriceType.ProductTicksInSixteenths,
            PriceType.ProductTicksInThirtySeconds,
            PriceType.ProductTicksInSixtyFourths,
            PriceType.ProductTicksInOneTwentyEighths,
            PriceType.NormalRateRepresentation,
            PriceType.InverseRateRepresentation,
            PriceType.BasisPoints,
            PriceType.UpfrontPoints,
            PriceType.InterestRate,
            PriceType.PercentageNotional,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PriceType type', () => {
        const validate = (value: PriceType) => {
            expect(Object.values(PriceType)).toContain(value);
        };

        validate(PriceType.Percentage);
        validate(PriceType.PerUnit);
        validate(PriceType.FixedAmount);
        validate(PriceType.Discount);
        validate(PriceType.Premium);
        validate(PriceType.Spread);
        validate(PriceType.TEDPrice);
        validate(PriceType.TEDYield);
        validate(PriceType.Yield);
        validate(PriceType.FixedCabinetTradePrice);
        validate(PriceType.VariableCabinetTradePrice);
        validate(PriceType.PriceSpread);
        validate(PriceType.ProductTicksInHalves);
        validate(PriceType.ProductTicksInFourths);
        validate(PriceType.ProductTicksInEighths);
        validate(PriceType.ProductTicksInSixteenths);
        validate(PriceType.ProductTicksInThirtySeconds);
        validate(PriceType.ProductTicksInSixtyFourths);
        validate(PriceType.ProductTicksInOneTwentyEighths);
        validate(PriceType.NormalRateRepresentation);
        validate(PriceType.InverseRateRepresentation);
        validate(PriceType.BasisPoints);
        validate(PriceType.UpfrontPoints);
        validate(PriceType.InterestRate);
        validate(PriceType.PercentageNotional);
    });

    test('should be immutable', () => {
        const ref = PriceType;
        expect(() => {
            ref.Percentage = 10;
        }).toThrowError();
    });
});
