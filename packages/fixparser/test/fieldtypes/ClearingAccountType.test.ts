import { ClearingAccountType } from '../../src/fieldtypes/ClearingAccountType';

describe('ClearingAccountType', () => {
    test('should have the correct values', () => {
        expect(ClearingAccountType.Customer).toBe(1);
        expect(ClearingAccountType.Firm).toBe(2);
        expect(ClearingAccountType.MarketMaker).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearingAccountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ClearingAccountType.Customer,
            ClearingAccountType.Firm,
            ClearingAccountType.MarketMaker,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ClearingAccountType type', () => {
        const validate = (value: ClearingAccountType) => {
            expect(Object.values(ClearingAccountType)).toContain(value);
        };

        validate(ClearingAccountType.Customer);
        validate(ClearingAccountType.Firm);
        validate(ClearingAccountType.MarketMaker);
    });

    test('should be immutable', () => {
        const ref = ClearingAccountType;
        expect(() => {
            ref.Customer = 10;
        }).toThrowError();
    });
});
