import { AllocTransType } from '../../src/fieldtypes/AllocTransType';

describe('AllocTransType', () => {
    test('should have the correct values', () => {
        expect(AllocTransType.New).toBe('0');
        expect(AllocTransType.Replace).toBe('1');
        expect(AllocTransType.Cancel).toBe('2');
        expect(AllocTransType.Preliminary).toBe('3');
        expect(AllocTransType.Calculated).toBe('4');
        expect(AllocTransType.CalculatedWithoutPreliminary).toBe('5');
        expect(AllocTransType.Reversal).toBe('6');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocTransType.New,
            AllocTransType.Replace,
            AllocTransType.Cancel,
            AllocTransType.Preliminary,
            AllocTransType.Calculated,
            AllocTransType.CalculatedWithoutPreliminary,
            AllocTransType.Reversal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AllocTransType type', () => {
        const validate = (value: AllocTransType) => {
            expect(Object.values(AllocTransType)).toContain(value);
        };

        validate(AllocTransType.New);
        validate(AllocTransType.Replace);
        validate(AllocTransType.Cancel);
        validate(AllocTransType.Preliminary);
        validate(AllocTransType.Calculated);
        validate(AllocTransType.CalculatedWithoutPreliminary);
        validate(AllocTransType.Reversal);
    });

    test('should be immutable', () => {
        const ref = AllocTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
