import { SettlDisruptionProvision } from '../../src/fieldtypes/SettlDisruptionProvision';

describe('SettlDisruptionProvision', () => {
    test('should have the correct values', () => {
        expect(SettlDisruptionProvision.Negotiation).toBe(1);
        expect(SettlDisruptionProvision.Cancellation).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlDisruptionProvision.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SettlDisruptionProvision.Negotiation, SettlDisruptionProvision.Cancellation];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlDisruptionProvision type', () => {
        const validate = (value: SettlDisruptionProvision) => {
            expect(Object.values(SettlDisruptionProvision)).toContain(value);
        };

        validate(SettlDisruptionProvision.Negotiation);
        validate(SettlDisruptionProvision.Cancellation);
    });

    test('should be immutable', () => {
        const ref = SettlDisruptionProvision;
        expect(() => {
            ref.Negotiation = 10;
        }).toThrowError();
    });
});
