import { ProvisionOptionExerciseEarliestDateOffsetUnit } from '../../src/fieldtypes/ProvisionOptionExerciseEarliestDateOffsetUnit';

describe('ProvisionOptionExerciseEarliestDateOffsetUnit', () => {
    test('should have the correct values', () => {
        expect(ProvisionOptionExerciseEarliestDateOffsetUnit.Day).toBe('D');
        expect(ProvisionOptionExerciseEarliestDateOffsetUnit.Week).toBe('Wk');
        expect(ProvisionOptionExerciseEarliestDateOffsetUnit.Month).toBe('Mo');
        expect(ProvisionOptionExerciseEarliestDateOffsetUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionOptionExerciseEarliestDateOffsetUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionOptionExerciseEarliestDateOffsetUnit.Day,
            ProvisionOptionExerciseEarliestDateOffsetUnit.Week,
            ProvisionOptionExerciseEarliestDateOffsetUnit.Month,
            ProvisionOptionExerciseEarliestDateOffsetUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ProvisionOptionExerciseEarliestDateOffsetUnit type', () => {
        const validate = (value: ProvisionOptionExerciseEarliestDateOffsetUnit) => {
            expect(Object.values(ProvisionOptionExerciseEarliestDateOffsetUnit)).toContain(value);
        };

        validate(ProvisionOptionExerciseEarliestDateOffsetUnit.Day);
        validate(ProvisionOptionExerciseEarliestDateOffsetUnit.Week);
        validate(ProvisionOptionExerciseEarliestDateOffsetUnit.Month);
        validate(ProvisionOptionExerciseEarliestDateOffsetUnit.Year);
    });

    test('should be immutable', () => {
        const ref = ProvisionOptionExerciseEarliestDateOffsetUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
