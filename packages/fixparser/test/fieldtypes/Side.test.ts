import { Side } from '../../src/fieldtypes/Side';

describe('Side', () => {
    test('should have the correct values', () => {
        expect(Side.Buy).toBe('1');
        expect(Side.Sell).toBe('2');
        expect(Side.BuyMinus).toBe('3');
        expect(Side.SellPlus).toBe('4');
        expect(Side.SellShort).toBe('5');
        expect(Side.SellShortExempt).toBe('6');
        expect(Side.Undisclosed).toBe('7');
        expect(Side.Cross).toBe('8');
        expect(Side.CrossShort).toBe('9');
        expect(Side.CrossShortExempt).toBe('A');
        expect(Side.AsDefined).toBe('B');
        expect(Side.Opposite).toBe('C');
        expect(Side.Subscribe).toBe('D');
        expect(Side.Redeem).toBe('E');
        expect(Side.Lend).toBe('F');
        expect(Side.Borrow).toBe('G');
        expect(Side.SellUndisclosed).toBe('H');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Side.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Side.Buy,
            Side.Sell,
            Side.BuyMinus,
            Side.SellPlus,
            Side.SellShort,
            Side.SellShortExempt,
            Side.Undisclosed,
            Side.Cross,
            Side.CrossShort,
            Side.CrossShortExempt,
            Side.AsDefined,
            Side.Opposite,
            Side.Subscribe,
            Side.Redeem,
            Side.Lend,
            Side.Borrow,
            Side.SellUndisclosed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Side type', () => {
        const validate = (value: Side) => {
            expect(Object.values(Side)).toContain(value);
        };

        validate(Side.Buy);
        validate(Side.Sell);
        validate(Side.BuyMinus);
        validate(Side.SellPlus);
        validate(Side.SellShort);
        validate(Side.SellShortExempt);
        validate(Side.Undisclosed);
        validate(Side.Cross);
        validate(Side.CrossShort);
        validate(Side.CrossShortExempt);
        validate(Side.AsDefined);
        validate(Side.Opposite);
        validate(Side.Subscribe);
        validate(Side.Redeem);
        validate(Side.Lend);
        validate(Side.Borrow);
        validate(Side.SellUndisclosed);
    });

    test('should be immutable', () => {
        const ref = Side;
        expect(() => {
            ref.Buy = 10;
        }).toThrowError();
    });
});
