import { MDReqRejReason } from '../../src/fieldtypes/MDReqRejReason';

describe('MDReqRejReason', () => {
    test('should have the correct values', () => {
        expect(MDReqRejReason.UnknownSymbol).toBe('0');
        expect(MDReqRejReason.DuplicateMDReqID).toBe('1');
        expect(MDReqRejReason.InsufficientBandwidth).toBe('2');
        expect(MDReqRejReason.InsufficientPermissions).toBe('3');
        expect(MDReqRejReason.UnsupportedSubscriptionRequestType).toBe('4');
        expect(MDReqRejReason.UnsupportedMarketDepth).toBe('5');
        expect(MDReqRejReason.UnsupportedMDUpdateType).toBe('6');
        expect(MDReqRejReason.UnsupportedAggregatedBook).toBe('7');
        expect(MDReqRejReason.UnsupportedMDEntryType).toBe('8');
        expect(MDReqRejReason.UnsupportedTradingSessionID).toBe('9');
        expect(MDReqRejReason.UnsupportedScope).toBe('A');
        expect(MDReqRejReason.UnsupportedOpenCloseSettleFlag).toBe('B');
        expect(MDReqRejReason.UnsupportedMDImplicitDelete).toBe('C');
        expect(MDReqRejReason.InsufficientCredit).toBe('D');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDReqRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDReqRejReason.UnknownSymbol,
            MDReqRejReason.DuplicateMDReqID,
            MDReqRejReason.InsufficientBandwidth,
            MDReqRejReason.InsufficientPermissions,
            MDReqRejReason.UnsupportedSubscriptionRequestType,
            MDReqRejReason.UnsupportedMarketDepth,
            MDReqRejReason.UnsupportedMDUpdateType,
            MDReqRejReason.UnsupportedAggregatedBook,
            MDReqRejReason.UnsupportedMDEntryType,
            MDReqRejReason.UnsupportedTradingSessionID,
            MDReqRejReason.UnsupportedScope,
            MDReqRejReason.UnsupportedOpenCloseSettleFlag,
            MDReqRejReason.UnsupportedMDImplicitDelete,
            MDReqRejReason.InsufficientCredit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MDReqRejReason type', () => {
        const validate = (value: MDReqRejReason) => {
            expect(Object.values(MDReqRejReason)).toContain(value);
        };

        validate(MDReqRejReason.UnknownSymbol);
        validate(MDReqRejReason.DuplicateMDReqID);
        validate(MDReqRejReason.InsufficientBandwidth);
        validate(MDReqRejReason.InsufficientPermissions);
        validate(MDReqRejReason.UnsupportedSubscriptionRequestType);
        validate(MDReqRejReason.UnsupportedMarketDepth);
        validate(MDReqRejReason.UnsupportedMDUpdateType);
        validate(MDReqRejReason.UnsupportedAggregatedBook);
        validate(MDReqRejReason.UnsupportedMDEntryType);
        validate(MDReqRejReason.UnsupportedTradingSessionID);
        validate(MDReqRejReason.UnsupportedScope);
        validate(MDReqRejReason.UnsupportedOpenCloseSettleFlag);
        validate(MDReqRejReason.UnsupportedMDImplicitDelete);
        validate(MDReqRejReason.InsufficientCredit);
    });

    test('should be immutable', () => {
        const ref = MDReqRejReason;
        expect(() => {
            ref.UnknownSymbol = 10;
        }).toThrowError();
    });
});
