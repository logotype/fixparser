import { MDStatisticIntervalType } from '../../src/fieldtypes/MDStatisticIntervalType';

describe('MDStatisticIntervalType', () => {
    test('should have the correct values', () => {
        expect(MDStatisticIntervalType.SlidingWindow).toBe(1);
        expect(MDStatisticIntervalType.SlidingWindowPeak).toBe(2);
        expect(MDStatisticIntervalType.FixedDateRange).toBe(3);
        expect(MDStatisticIntervalType.FixedTimeRange).toBe(4);
        expect(MDStatisticIntervalType.CurrentTimeUnit).toBe(5);
        expect(MDStatisticIntervalType.PreviousTimeUnit).toBe(6);
        expect(MDStatisticIntervalType.MaximumRange).toBe(7);
        expect(MDStatisticIntervalType.MaximumRangeUpToPreviousTimeUnit).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticIntervalType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticIntervalType.SlidingWindow,
            MDStatisticIntervalType.SlidingWindowPeak,
            MDStatisticIntervalType.FixedDateRange,
            MDStatisticIntervalType.FixedTimeRange,
            MDStatisticIntervalType.CurrentTimeUnit,
            MDStatisticIntervalType.PreviousTimeUnit,
            MDStatisticIntervalType.MaximumRange,
            MDStatisticIntervalType.MaximumRangeUpToPreviousTimeUnit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticIntervalType type', () => {
        const validate = (value: MDStatisticIntervalType) => {
            expect(Object.values(MDStatisticIntervalType)).toContain(value);
        };

        validate(MDStatisticIntervalType.SlidingWindow);
        validate(MDStatisticIntervalType.SlidingWindowPeak);
        validate(MDStatisticIntervalType.FixedDateRange);
        validate(MDStatisticIntervalType.FixedTimeRange);
        validate(MDStatisticIntervalType.CurrentTimeUnit);
        validate(MDStatisticIntervalType.PreviousTimeUnit);
        validate(MDStatisticIntervalType.MaximumRange);
        validate(MDStatisticIntervalType.MaximumRangeUpToPreviousTimeUnit);
    });

    test('should be immutable', () => {
        const ref = MDStatisticIntervalType;
        expect(() => {
            ref.SlidingWindow = 10;
        }).toThrowError();
    });
});
