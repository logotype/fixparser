import { OrdRejReason } from '../../src/fieldtypes/OrdRejReason';

describe('OrdRejReason', () => {
    test('should have the correct values', () => {
        expect(OrdRejReason.BrokerCredit).toBe(0);
        expect(OrdRejReason.UnknownSymbol).toBe(1);
        expect(OrdRejReason.ExchangeClosed).toBe(2);
        expect(OrdRejReason.OrderExceedsLimit).toBe(3);
        expect(OrdRejReason.TooLateToEnter).toBe(4);
        expect(OrdRejReason.UnknownOrder).toBe(5);
        expect(OrdRejReason.DuplicateOrder).toBe(6);
        expect(OrdRejReason.DuplicateOfAVerballyCommunicatedOrder).toBe(7);
        expect(OrdRejReason.StaleOrder).toBe(8);
        expect(OrdRejReason.TradeAlongRequired).toBe(9);
        expect(OrdRejReason.InvalidInvestorID).toBe(10);
        expect(OrdRejReason.UnsupportedOrderCharacteristic).toBe(11);
        expect(OrdRejReason.SurveillanceOption).toBe(12);
        expect(OrdRejReason.IncorrectQuantity).toBe(13);
        expect(OrdRejReason.IncorrectAllocatedQuantity).toBe(14);
        expect(OrdRejReason.UnknownAccount).toBe(15);
        expect(OrdRejReason.PriceExceedsCurrentPriceBand).toBe(16);
        expect(OrdRejReason.InvalidPriceIncrement).toBe(18);
        expect(OrdRejReason.ReferencePriceNotAvailable).toBe(19);
        expect(OrdRejReason.NotionalValueExceedsThreshold).toBe(20);
        expect(OrdRejReason.AlgorithmRiskThresholdBreached).toBe(21);
        expect(OrdRejReason.ShortSellNotPermitted).toBe(22);
        expect(OrdRejReason.ShortSellSecurityPreBorrowRestriction).toBe(23);
        expect(OrdRejReason.ShortSellAccountPreBorrowRestriction).toBe(24);
        expect(OrdRejReason.InsufficientCreditLimit).toBe(25);
        expect(OrdRejReason.ExceededClipSizeLimit).toBe(26);
        expect(OrdRejReason.ExceededMaxNotionalOrderAmt).toBe(27);
        expect(OrdRejReason.ExceededDV01PV01Limit).toBe(28);
        expect(OrdRejReason.ExceededCS01Limit).toBe(29);
        expect(OrdRejReason.LastLook).toBe(30);
        expect(OrdRejReason.LastLookLatency).toBe(31);
        expect(OrdRejReason.UnavailablePriceLiquidity).toBe(32);
        expect(OrdRejReason.InvalidMissingEntitlements).toBe(33);
        expect(OrdRejReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrdRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrdRejReason.BrokerCredit,
            OrdRejReason.UnknownSymbol,
            OrdRejReason.ExchangeClosed,
            OrdRejReason.OrderExceedsLimit,
            OrdRejReason.TooLateToEnter,
            OrdRejReason.UnknownOrder,
            OrdRejReason.DuplicateOrder,
            OrdRejReason.DuplicateOfAVerballyCommunicatedOrder,
            OrdRejReason.StaleOrder,
            OrdRejReason.TradeAlongRequired,
            OrdRejReason.InvalidInvestorID,
            OrdRejReason.UnsupportedOrderCharacteristic,
            OrdRejReason.SurveillanceOption,
            OrdRejReason.IncorrectQuantity,
            OrdRejReason.IncorrectAllocatedQuantity,
            OrdRejReason.UnknownAccount,
            OrdRejReason.PriceExceedsCurrentPriceBand,
            OrdRejReason.InvalidPriceIncrement,
            OrdRejReason.ReferencePriceNotAvailable,
            OrdRejReason.NotionalValueExceedsThreshold,
            OrdRejReason.AlgorithmRiskThresholdBreached,
            OrdRejReason.ShortSellNotPermitted,
            OrdRejReason.ShortSellSecurityPreBorrowRestriction,
            OrdRejReason.ShortSellAccountPreBorrowRestriction,
            OrdRejReason.InsufficientCreditLimit,
            OrdRejReason.ExceededClipSizeLimit,
            OrdRejReason.ExceededMaxNotionalOrderAmt,
            OrdRejReason.ExceededDV01PV01Limit,
            OrdRejReason.ExceededCS01Limit,
            OrdRejReason.LastLook,
            OrdRejReason.LastLookLatency,
            OrdRejReason.UnavailablePriceLiquidity,
            OrdRejReason.InvalidMissingEntitlements,
            OrdRejReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrdRejReason type', () => {
        const validate = (value: OrdRejReason) => {
            expect(Object.values(OrdRejReason)).toContain(value);
        };

        validate(OrdRejReason.BrokerCredit);
        validate(OrdRejReason.UnknownSymbol);
        validate(OrdRejReason.ExchangeClosed);
        validate(OrdRejReason.OrderExceedsLimit);
        validate(OrdRejReason.TooLateToEnter);
        validate(OrdRejReason.UnknownOrder);
        validate(OrdRejReason.DuplicateOrder);
        validate(OrdRejReason.DuplicateOfAVerballyCommunicatedOrder);
        validate(OrdRejReason.StaleOrder);
        validate(OrdRejReason.TradeAlongRequired);
        validate(OrdRejReason.InvalidInvestorID);
        validate(OrdRejReason.UnsupportedOrderCharacteristic);
        validate(OrdRejReason.SurveillanceOption);
        validate(OrdRejReason.IncorrectQuantity);
        validate(OrdRejReason.IncorrectAllocatedQuantity);
        validate(OrdRejReason.UnknownAccount);
        validate(OrdRejReason.PriceExceedsCurrentPriceBand);
        validate(OrdRejReason.InvalidPriceIncrement);
        validate(OrdRejReason.ReferencePriceNotAvailable);
        validate(OrdRejReason.NotionalValueExceedsThreshold);
        validate(OrdRejReason.AlgorithmRiskThresholdBreached);
        validate(OrdRejReason.ShortSellNotPermitted);
        validate(OrdRejReason.ShortSellSecurityPreBorrowRestriction);
        validate(OrdRejReason.ShortSellAccountPreBorrowRestriction);
        validate(OrdRejReason.InsufficientCreditLimit);
        validate(OrdRejReason.ExceededClipSizeLimit);
        validate(OrdRejReason.ExceededMaxNotionalOrderAmt);
        validate(OrdRejReason.ExceededDV01PV01Limit);
        validate(OrdRejReason.ExceededCS01Limit);
        validate(OrdRejReason.LastLook);
        validate(OrdRejReason.LastLookLatency);
        validate(OrdRejReason.UnavailablePriceLiquidity);
        validate(OrdRejReason.InvalidMissingEntitlements);
        validate(OrdRejReason.Other);
    });

    test('should be immutable', () => {
        const ref = OrdRejReason;
        expect(() => {
            ref.BrokerCredit = 10;
        }).toThrowError();
    });
});
