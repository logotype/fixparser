import { AggressorIndicator } from '../../src/fieldtypes/AggressorIndicator';

describe('AggressorIndicator', () => {
    test('should have the correct values', () => {
        expect(AggressorIndicator.OrderInitiatorIsAggressor).toBe('Y');
        expect(AggressorIndicator.OrderInitiatorIsPassive).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AggressorIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AggressorIndicator.OrderInitiatorIsAggressor,
            AggressorIndicator.OrderInitiatorIsPassive,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AggressorIndicator type', () => {
        const validate = (value: AggressorIndicator) => {
            expect(Object.values(AggressorIndicator)).toContain(value);
        };

        validate(AggressorIndicator.OrderInitiatorIsAggressor);
        validate(AggressorIndicator.OrderInitiatorIsPassive);
    });

    test('should be immutable', () => {
        const ref = AggressorIndicator;
        expect(() => {
            ref.OrderInitiatorIsAggressor = 10;
        }).toThrowError();
    });
});
