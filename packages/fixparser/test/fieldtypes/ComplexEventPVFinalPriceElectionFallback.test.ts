import { ComplexEventPVFinalPriceElectionFallback } from '../../src/fieldtypes/ComplexEventPVFinalPriceElectionFallback';

describe('ComplexEventPVFinalPriceElectionFallback', () => {
    test('should have the correct values', () => {
        expect(ComplexEventPVFinalPriceElectionFallback.Close).toBe(0);
        expect(ComplexEventPVFinalPriceElectionFallback.HedgeElection).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventPVFinalPriceElectionFallback.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventPVFinalPriceElectionFallback.Close,
            ComplexEventPVFinalPriceElectionFallback.HedgeElection,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventPVFinalPriceElectionFallback type', () => {
        const validate = (value: ComplexEventPVFinalPriceElectionFallback) => {
            expect(Object.values(ComplexEventPVFinalPriceElectionFallback)).toContain(value);
        };

        validate(ComplexEventPVFinalPriceElectionFallback.Close);
        validate(ComplexEventPVFinalPriceElectionFallback.HedgeElection);
    });

    test('should be immutable', () => {
        const ref = ComplexEventPVFinalPriceElectionFallback;
        expect(() => {
            ref.Close = 10;
        }).toThrowError();
    });
});
