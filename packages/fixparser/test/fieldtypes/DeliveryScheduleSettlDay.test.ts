import { DeliveryScheduleSettlDay } from '../../src/fieldtypes/DeliveryScheduleSettlDay';

describe('DeliveryScheduleSettlDay', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleSettlDay.Monday).toBe(1);
        expect(DeliveryScheduleSettlDay.Tuesday).toBe(2);
        expect(DeliveryScheduleSettlDay.Wednesday).toBe(3);
        expect(DeliveryScheduleSettlDay.Thursday).toBe(4);
        expect(DeliveryScheduleSettlDay.Friday).toBe(5);
        expect(DeliveryScheduleSettlDay.Saturday).toBe(6);
        expect(DeliveryScheduleSettlDay.Sunday).toBe(7);
        expect(DeliveryScheduleSettlDay.AllWeekdays).toBe(8);
        expect(DeliveryScheduleSettlDay.AllDays).toBe(9);
        expect(DeliveryScheduleSettlDay.AllWeekends).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleSettlDay.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryScheduleSettlDay.Monday,
            DeliveryScheduleSettlDay.Tuesday,
            DeliveryScheduleSettlDay.Wednesday,
            DeliveryScheduleSettlDay.Thursday,
            DeliveryScheduleSettlDay.Friday,
            DeliveryScheduleSettlDay.Saturday,
            DeliveryScheduleSettlDay.Sunday,
            DeliveryScheduleSettlDay.AllWeekdays,
            DeliveryScheduleSettlDay.AllDays,
            DeliveryScheduleSettlDay.AllWeekends,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleSettlDay type', () => {
        const validate = (value: DeliveryScheduleSettlDay) => {
            expect(Object.values(DeliveryScheduleSettlDay)).toContain(value);
        };

        validate(DeliveryScheduleSettlDay.Monday);
        validate(DeliveryScheduleSettlDay.Tuesday);
        validate(DeliveryScheduleSettlDay.Wednesday);
        validate(DeliveryScheduleSettlDay.Thursday);
        validate(DeliveryScheduleSettlDay.Friday);
        validate(DeliveryScheduleSettlDay.Saturday);
        validate(DeliveryScheduleSettlDay.Sunday);
        validate(DeliveryScheduleSettlDay.AllWeekdays);
        validate(DeliveryScheduleSettlDay.AllDays);
        validate(DeliveryScheduleSettlDay.AllWeekends);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleSettlDay;
        expect(() => {
            ref.Monday = 10;
        }).toThrowError();
    });
});
