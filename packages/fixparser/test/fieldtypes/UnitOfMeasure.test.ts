import { UnitOfMeasure } from '../../src/fieldtypes/UnitOfMeasure';

describe('UnitOfMeasure', () => {
    test('should have the correct values', () => {
        expect(UnitOfMeasure.BillionCubicFeet).toBe('Bcf');
        expect(UnitOfMeasure.Allowances).toBe('Alw');
        expect(UnitOfMeasure.CubicMeters).toBe('CBM');
        expect(UnitOfMeasure.Barrels).toBe('Bbl');
        expect(UnitOfMeasure.Gigajoules).toBe('GJ');
        expect(UnitOfMeasure.BoardFeet).toBe('BDFT');
        expect(UnitOfMeasure.HeatRate).toBe('kHR');
        expect(UnitOfMeasure.Bushels).toBe('Bu');
        expect(UnitOfMeasure.KilowattHours).toBe('kWh');
        expect(UnitOfMeasure.Currency).toBe('Ccy');
        expect(UnitOfMeasure.MegaHeatRate).toBe('MHR');
        expect(UnitOfMeasure.CoolingDegreeDay).toBe('CDD');
        expect(UnitOfMeasure.OneMillionBTU).toBe('MMBtu');
        expect(UnitOfMeasure.CertifiedEmissionsReduction).toBe('CER');
        expect(UnitOfMeasure.MegawattHours).toBe('MWh');
        expect(UnitOfMeasure.CriticalPrecipDay).toBe('CPD');
        expect(UnitOfMeasure.Therms).toBe('thm');
        expect(UnitOfMeasure.ClimateReserveTonnes).toBe('CRT');
        expect(UnitOfMeasure.TonsOfCarbonDioxide).toBe('tnCO2');
        expect(UnitOfMeasure.Hundredweight).toBe('cwt');
        expect(UnitOfMeasure.Day).toBe('day');
        expect(UnitOfMeasure.DryMetricTons).toBe('dt');
        expect(UnitOfMeasure.EnvAllwncCert).toBe('EnvAllwnc');
        expect(UnitOfMeasure.EnvironmentalCredit).toBe('EnvCrd');
        expect(UnitOfMeasure.EnvironmentalOffset).toBe('EnvOfst');
        expect(UnitOfMeasure.Grams).toBe('g');
        expect(UnitOfMeasure.Gallons).toBe('Gal');
        expect(UnitOfMeasure.GrossTons).toBe('GT');
        expect(UnitOfMeasure.HeatingDegreeDay).toBe('HDD');
        expect(UnitOfMeasure.IndexPoint).toBe('IPNT');
        expect(UnitOfMeasure.Kilograms).toBe('kg');
        expect(UnitOfMeasure.Kiloliters).toBe('kL');
        expect(UnitOfMeasure.KilowattYear).toBe('kW-a');
        expect(UnitOfMeasure.KilowattDay).toBe('kW-d');
        expect(UnitOfMeasure.KilowattHour).toBe('kW-h');
        expect(UnitOfMeasure.KilowattMonth).toBe('kW-M');
        expect(UnitOfMeasure.KilowattMinute).toBe('kW-min');
        expect(UnitOfMeasure.Liters).toBe('L');
        expect(UnitOfMeasure.Pounds).toBe('lbs');
        expect(UnitOfMeasure.MegawattYear).toBe('MW-a');
        expect(UnitOfMeasure.MegawattDay).toBe('MW-d');
        expect(UnitOfMeasure.MegawattHour).toBe('MW-h');
        expect(UnitOfMeasure.MegawattMonth).toBe('MW-M');
        expect(UnitOfMeasure.MegawattMinute).toBe('MW-min');
        expect(UnitOfMeasure.TroyOunces).toBe('oz_tr');
        expect(UnitOfMeasure.PrincipalWithRelationToDebtInstrument).toBe('PRINC');
        expect(UnitOfMeasure.MetricTons).toBe('t');
        expect(UnitOfMeasure.Tons).toBe('tn');
        expect(UnitOfMeasure.Are).toBe('a');
        expect(UnitOfMeasure.Acre).toBe('ac');
        expect(UnitOfMeasure.Centiliter).toBe('cL');
        expect(UnitOfMeasure.Centimeter).toBe('cM');
        expect(UnitOfMeasure.DieselGallonEquivalent).toBe('DGE');
        expect(UnitOfMeasure.Foot).toBe('ft');
        expect(UnitOfMeasure.GBGallon).toBe('Gal_gb');
        expect(UnitOfMeasure.GasolineGallonEquivalent).toBe('GGE');
        expect(UnitOfMeasure.Hectare).toBe('ha');
        expect(UnitOfMeasure.Inch).toBe('in');
        expect(UnitOfMeasure.Kilometer).toBe('kM');
        expect(UnitOfMeasure.Meter).toBe('M');
        expect(UnitOfMeasure.Mile).toBe('mi');
        expect(UnitOfMeasure.Milliliter).toBe('mL');
        expect(UnitOfMeasure.Millimeter).toBe('mM');
        expect(UnitOfMeasure.USOunce).toBe('oz');
        expect(UnitOfMeasure.Piece).toBe('pc');
        expect(UnitOfMeasure.USPint).toBe('pt');
        expect(UnitOfMeasure.GBPint).toBe('pt_gb');
        expect(UnitOfMeasure.USQuart).toBe('qt');
        expect(UnitOfMeasure.GBQuart).toBe('qt_gb');
        expect(UnitOfMeasure.SquareCentimeter).toBe('SqcM');
        expect(UnitOfMeasure.SquareFoot).toBe('Sqft');
        expect(UnitOfMeasure.SquareInch).toBe('Sqin');
        expect(UnitOfMeasure.SquareKilometer).toBe('SqkM');
        expect(UnitOfMeasure.SquareMeter).toBe('SqM');
        expect(UnitOfMeasure.SquareMile).toBe('Sqmi');
        expect(UnitOfMeasure.SquareMillimeter).toBe('SqmM');
        expect(UnitOfMeasure.SquareYard).toBe('Sqyd');
        expect(UnitOfMeasure.Yard).toBe('yd');
        expect(UnitOfMeasure.MillionBarrels).toBe('MMbbl');
        expect(UnitOfMeasure.USDollars).toBe('USD');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnitOfMeasure.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnitOfMeasure.BillionCubicFeet,
            UnitOfMeasure.Allowances,
            UnitOfMeasure.CubicMeters,
            UnitOfMeasure.Barrels,
            UnitOfMeasure.Gigajoules,
            UnitOfMeasure.BoardFeet,
            UnitOfMeasure.HeatRate,
            UnitOfMeasure.Bushels,
            UnitOfMeasure.KilowattHours,
            UnitOfMeasure.Currency,
            UnitOfMeasure.MegaHeatRate,
            UnitOfMeasure.CoolingDegreeDay,
            UnitOfMeasure.OneMillionBTU,
            UnitOfMeasure.CertifiedEmissionsReduction,
            UnitOfMeasure.MegawattHours,
            UnitOfMeasure.CriticalPrecipDay,
            UnitOfMeasure.Therms,
            UnitOfMeasure.ClimateReserveTonnes,
            UnitOfMeasure.TonsOfCarbonDioxide,
            UnitOfMeasure.Hundredweight,
            UnitOfMeasure.Day,
            UnitOfMeasure.DryMetricTons,
            UnitOfMeasure.EnvAllwncCert,
            UnitOfMeasure.EnvironmentalCredit,
            UnitOfMeasure.EnvironmentalOffset,
            UnitOfMeasure.Grams,
            UnitOfMeasure.Gallons,
            UnitOfMeasure.GrossTons,
            UnitOfMeasure.HeatingDegreeDay,
            UnitOfMeasure.IndexPoint,
            UnitOfMeasure.Kilograms,
            UnitOfMeasure.Kiloliters,
            UnitOfMeasure.KilowattYear,
            UnitOfMeasure.KilowattDay,
            UnitOfMeasure.KilowattHour,
            UnitOfMeasure.KilowattMonth,
            UnitOfMeasure.KilowattMinute,
            UnitOfMeasure.Liters,
            UnitOfMeasure.Pounds,
            UnitOfMeasure.MegawattYear,
            UnitOfMeasure.MegawattDay,
            UnitOfMeasure.MegawattHour,
            UnitOfMeasure.MegawattMonth,
            UnitOfMeasure.MegawattMinute,
            UnitOfMeasure.TroyOunces,
            UnitOfMeasure.PrincipalWithRelationToDebtInstrument,
            UnitOfMeasure.MetricTons,
            UnitOfMeasure.Tons,
            UnitOfMeasure.Are,
            UnitOfMeasure.Acre,
            UnitOfMeasure.Centiliter,
            UnitOfMeasure.Centimeter,
            UnitOfMeasure.DieselGallonEquivalent,
            UnitOfMeasure.Foot,
            UnitOfMeasure.GBGallon,
            UnitOfMeasure.GasolineGallonEquivalent,
            UnitOfMeasure.Hectare,
            UnitOfMeasure.Inch,
            UnitOfMeasure.Kilometer,
            UnitOfMeasure.Meter,
            UnitOfMeasure.Mile,
            UnitOfMeasure.Milliliter,
            UnitOfMeasure.Millimeter,
            UnitOfMeasure.USOunce,
            UnitOfMeasure.Piece,
            UnitOfMeasure.USPint,
            UnitOfMeasure.GBPint,
            UnitOfMeasure.USQuart,
            UnitOfMeasure.GBQuart,
            UnitOfMeasure.SquareCentimeter,
            UnitOfMeasure.SquareFoot,
            UnitOfMeasure.SquareInch,
            UnitOfMeasure.SquareKilometer,
            UnitOfMeasure.SquareMeter,
            UnitOfMeasure.SquareMile,
            UnitOfMeasure.SquareMillimeter,
            UnitOfMeasure.SquareYard,
            UnitOfMeasure.Yard,
            UnitOfMeasure.MillionBarrels,
            UnitOfMeasure.USDollars,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for UnitOfMeasure type', () => {
        const validate = (value: UnitOfMeasure) => {
            expect(Object.values(UnitOfMeasure)).toContain(value);
        };

        validate(UnitOfMeasure.BillionCubicFeet);
        validate(UnitOfMeasure.Allowances);
        validate(UnitOfMeasure.CubicMeters);
        validate(UnitOfMeasure.Barrels);
        validate(UnitOfMeasure.Gigajoules);
        validate(UnitOfMeasure.BoardFeet);
        validate(UnitOfMeasure.HeatRate);
        validate(UnitOfMeasure.Bushels);
        validate(UnitOfMeasure.KilowattHours);
        validate(UnitOfMeasure.Currency);
        validate(UnitOfMeasure.MegaHeatRate);
        validate(UnitOfMeasure.CoolingDegreeDay);
        validate(UnitOfMeasure.OneMillionBTU);
        validate(UnitOfMeasure.CertifiedEmissionsReduction);
        validate(UnitOfMeasure.MegawattHours);
        validate(UnitOfMeasure.CriticalPrecipDay);
        validate(UnitOfMeasure.Therms);
        validate(UnitOfMeasure.ClimateReserveTonnes);
        validate(UnitOfMeasure.TonsOfCarbonDioxide);
        validate(UnitOfMeasure.Hundredweight);
        validate(UnitOfMeasure.Day);
        validate(UnitOfMeasure.DryMetricTons);
        validate(UnitOfMeasure.EnvAllwncCert);
        validate(UnitOfMeasure.EnvironmentalCredit);
        validate(UnitOfMeasure.EnvironmentalOffset);
        validate(UnitOfMeasure.Grams);
        validate(UnitOfMeasure.Gallons);
        validate(UnitOfMeasure.GrossTons);
        validate(UnitOfMeasure.HeatingDegreeDay);
        validate(UnitOfMeasure.IndexPoint);
        validate(UnitOfMeasure.Kilograms);
        validate(UnitOfMeasure.Kiloliters);
        validate(UnitOfMeasure.KilowattYear);
        validate(UnitOfMeasure.KilowattDay);
        validate(UnitOfMeasure.KilowattHour);
        validate(UnitOfMeasure.KilowattMonth);
        validate(UnitOfMeasure.KilowattMinute);
        validate(UnitOfMeasure.Liters);
        validate(UnitOfMeasure.Pounds);
        validate(UnitOfMeasure.MegawattYear);
        validate(UnitOfMeasure.MegawattDay);
        validate(UnitOfMeasure.MegawattHour);
        validate(UnitOfMeasure.MegawattMonth);
        validate(UnitOfMeasure.MegawattMinute);
        validate(UnitOfMeasure.TroyOunces);
        validate(UnitOfMeasure.PrincipalWithRelationToDebtInstrument);
        validate(UnitOfMeasure.MetricTons);
        validate(UnitOfMeasure.Tons);
        validate(UnitOfMeasure.Are);
        validate(UnitOfMeasure.Acre);
        validate(UnitOfMeasure.Centiliter);
        validate(UnitOfMeasure.Centimeter);
        validate(UnitOfMeasure.DieselGallonEquivalent);
        validate(UnitOfMeasure.Foot);
        validate(UnitOfMeasure.GBGallon);
        validate(UnitOfMeasure.GasolineGallonEquivalent);
        validate(UnitOfMeasure.Hectare);
        validate(UnitOfMeasure.Inch);
        validate(UnitOfMeasure.Kilometer);
        validate(UnitOfMeasure.Meter);
        validate(UnitOfMeasure.Mile);
        validate(UnitOfMeasure.Milliliter);
        validate(UnitOfMeasure.Millimeter);
        validate(UnitOfMeasure.USOunce);
        validate(UnitOfMeasure.Piece);
        validate(UnitOfMeasure.USPint);
        validate(UnitOfMeasure.GBPint);
        validate(UnitOfMeasure.USQuart);
        validate(UnitOfMeasure.GBQuart);
        validate(UnitOfMeasure.SquareCentimeter);
        validate(UnitOfMeasure.SquareFoot);
        validate(UnitOfMeasure.SquareInch);
        validate(UnitOfMeasure.SquareKilometer);
        validate(UnitOfMeasure.SquareMeter);
        validate(UnitOfMeasure.SquareMile);
        validate(UnitOfMeasure.SquareMillimeter);
        validate(UnitOfMeasure.SquareYard);
        validate(UnitOfMeasure.Yard);
        validate(UnitOfMeasure.MillionBarrels);
        validate(UnitOfMeasure.USDollars);
    });

    test('should be immutable', () => {
        const ref = UnitOfMeasure;
        expect(() => {
            ref.BillionCubicFeet = 10;
        }).toThrowError();
    });
});
