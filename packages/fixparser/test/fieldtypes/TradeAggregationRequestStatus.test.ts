import { TradeAggregationRequestStatus } from '../../src/fieldtypes/TradeAggregationRequestStatus';

describe('TradeAggregationRequestStatus', () => {
    test('should have the correct values', () => {
        expect(TradeAggregationRequestStatus.Accepted).toBe(0);
        expect(TradeAggregationRequestStatus.Rejected).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAggregationRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradeAggregationRequestStatus.Accepted, TradeAggregationRequestStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAggregationRequestStatus type', () => {
        const validate = (value: TradeAggregationRequestStatus) => {
            expect(Object.values(TradeAggregationRequestStatus)).toContain(value);
        };

        validate(TradeAggregationRequestStatus.Accepted);
        validate(TradeAggregationRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TradeAggregationRequestStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
