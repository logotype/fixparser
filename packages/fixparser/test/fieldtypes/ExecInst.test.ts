import { ExecInst } from '../../src/fieldtypes/ExecInst';

describe('ExecInst', () => {
    test('should have the correct values', () => {
        expect(ExecInst.StayOnOfferSide).toBe('0');
        expect(ExecInst.NotHeld).toBe('1');
        expect(ExecInst.Work).toBe('2');
        expect(ExecInst.GoAlong).toBe('3');
        expect(ExecInst.OverTheDay).toBe('4');
        expect(ExecInst.Held).toBe('5');
        expect(ExecInst.ParticipateDoNotInitiate).toBe('6');
        expect(ExecInst.StrictScale).toBe('7');
        expect(ExecInst.TryToScale).toBe('8');
        expect(ExecInst.StayOnBidSide).toBe('9');
        expect(ExecInst.NoCross).toBe('A');
        expect(ExecInst.OKToCross).toBe('B');
        expect(ExecInst.CallFirst).toBe('C');
        expect(ExecInst.PercentOfVolume).toBe('D');
        expect(ExecInst.DoNotIncrease).toBe('E');
        expect(ExecInst.DoNotReduce).toBe('F');
        expect(ExecInst.AllOrNone).toBe('G');
        expect(ExecInst.ReinstateOnSystemFailure).toBe('H');
        expect(ExecInst.InstitutionsOnly).toBe('I');
        expect(ExecInst.ReinstateOnTradingHalt).toBe('J');
        expect(ExecInst.CancelOnTradingHalt).toBe('K');
        expect(ExecInst.LastPeg).toBe('L');
        expect(ExecInst.MidPricePeg).toBe('M');
        expect(ExecInst.NonNegotiable).toBe('N');
        expect(ExecInst.OpeningPeg).toBe('O');
        expect(ExecInst.MarketPeg).toBe('P');
        expect(ExecInst.CancelOnSystemFailure).toBe('Q');
        expect(ExecInst.PrimaryPeg).toBe('R');
        expect(ExecInst.Suspend).toBe('S');
        expect(ExecInst.FixedPegToLocalBestBidOrOfferAtTimeOfOrder).toBe('T');
        expect(ExecInst.CustomerDisplayInstruction).toBe('U');
        expect(ExecInst.Netting).toBe('V');
        expect(ExecInst.PegToVWAP).toBe('W');
        expect(ExecInst.TradeAlong).toBe('X');
        expect(ExecInst.TryToStop).toBe('Y');
        expect(ExecInst.CancelIfNotBest).toBe('Z');
        expect(ExecInst.TrailingStopPeg).toBe('a');
        expect(ExecInst.StrictLimit).toBe('b');
        expect(ExecInst.IgnorePriceValidityChecks).toBe('c');
        expect(ExecInst.PegToLimitPrice).toBe('d');
        expect(ExecInst.WorkToTargetStrategy).toBe('e');
        expect(ExecInst.IntermarketSweep).toBe('f');
        expect(ExecInst.ExternalRoutingAllowed).toBe('g');
        expect(ExecInst.ExternalRoutingNotAllowed).toBe('h');
        expect(ExecInst.ImbalanceOnly).toBe('i');
        expect(ExecInst.SingleExecutionRequestedForBlockTrade).toBe('j');
        expect(ExecInst.BestExecution).toBe('k');
        expect(ExecInst.SuspendOnSystemFailure).toBe('l');
        expect(ExecInst.SuspendOnTradingHalt).toBe('m');
        expect(ExecInst.ReinstateOnConnectionLoss).toBe('n');
        expect(ExecInst.CancelOnConnectionLoss).toBe('o');
        expect(ExecInst.SuspendOnConnectionLoss).toBe('p');
        expect(ExecInst.Release).toBe('q');
        expect(ExecInst.ExecuteAsDeltaNeutral).toBe('r');
        expect(ExecInst.ExecuteAsDurationNeutral).toBe('s');
        expect(ExecInst.ExecuteAsFXNeutral).toBe('t');
        expect(ExecInst.MinGuaranteedFillEligible).toBe('u');
        expect(ExecInst.BypassNonDisplayLiquidity).toBe('v');
        expect(ExecInst.Lock).toBe('w');
        expect(ExecInst.IgnoreNotionalValueChecks).toBe('x');
        expect(ExecInst.TrdAtRefPx).toBe('y');
        expect(ExecInst.AllowFacilitation).toBe('z');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecInst.StayOnOfferSide,
            ExecInst.NotHeld,
            ExecInst.Work,
            ExecInst.GoAlong,
            ExecInst.OverTheDay,
            ExecInst.Held,
            ExecInst.ParticipateDoNotInitiate,
            ExecInst.StrictScale,
            ExecInst.TryToScale,
            ExecInst.StayOnBidSide,
            ExecInst.NoCross,
            ExecInst.OKToCross,
            ExecInst.CallFirst,
            ExecInst.PercentOfVolume,
            ExecInst.DoNotIncrease,
            ExecInst.DoNotReduce,
            ExecInst.AllOrNone,
            ExecInst.ReinstateOnSystemFailure,
            ExecInst.InstitutionsOnly,
            ExecInst.ReinstateOnTradingHalt,
            ExecInst.CancelOnTradingHalt,
            ExecInst.LastPeg,
            ExecInst.MidPricePeg,
            ExecInst.NonNegotiable,
            ExecInst.OpeningPeg,
            ExecInst.MarketPeg,
            ExecInst.CancelOnSystemFailure,
            ExecInst.PrimaryPeg,
            ExecInst.Suspend,
            ExecInst.FixedPegToLocalBestBidOrOfferAtTimeOfOrder,
            ExecInst.CustomerDisplayInstruction,
            ExecInst.Netting,
            ExecInst.PegToVWAP,
            ExecInst.TradeAlong,
            ExecInst.TryToStop,
            ExecInst.CancelIfNotBest,
            ExecInst.TrailingStopPeg,
            ExecInst.StrictLimit,
            ExecInst.IgnorePriceValidityChecks,
            ExecInst.PegToLimitPrice,
            ExecInst.WorkToTargetStrategy,
            ExecInst.IntermarketSweep,
            ExecInst.ExternalRoutingAllowed,
            ExecInst.ExternalRoutingNotAllowed,
            ExecInst.ImbalanceOnly,
            ExecInst.SingleExecutionRequestedForBlockTrade,
            ExecInst.BestExecution,
            ExecInst.SuspendOnSystemFailure,
            ExecInst.SuspendOnTradingHalt,
            ExecInst.ReinstateOnConnectionLoss,
            ExecInst.CancelOnConnectionLoss,
            ExecInst.SuspendOnConnectionLoss,
            ExecInst.Release,
            ExecInst.ExecuteAsDeltaNeutral,
            ExecInst.ExecuteAsDurationNeutral,
            ExecInst.ExecuteAsFXNeutral,
            ExecInst.MinGuaranteedFillEligible,
            ExecInst.BypassNonDisplayLiquidity,
            ExecInst.Lock,
            ExecInst.IgnoreNotionalValueChecks,
            ExecInst.TrdAtRefPx,
            ExecInst.AllowFacilitation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExecInst type', () => {
        const validate = (value: ExecInst) => {
            expect(Object.values(ExecInst)).toContain(value);
        };

        validate(ExecInst.StayOnOfferSide);
        validate(ExecInst.NotHeld);
        validate(ExecInst.Work);
        validate(ExecInst.GoAlong);
        validate(ExecInst.OverTheDay);
        validate(ExecInst.Held);
        validate(ExecInst.ParticipateDoNotInitiate);
        validate(ExecInst.StrictScale);
        validate(ExecInst.TryToScale);
        validate(ExecInst.StayOnBidSide);
        validate(ExecInst.NoCross);
        validate(ExecInst.OKToCross);
        validate(ExecInst.CallFirst);
        validate(ExecInst.PercentOfVolume);
        validate(ExecInst.DoNotIncrease);
        validate(ExecInst.DoNotReduce);
        validate(ExecInst.AllOrNone);
        validate(ExecInst.ReinstateOnSystemFailure);
        validate(ExecInst.InstitutionsOnly);
        validate(ExecInst.ReinstateOnTradingHalt);
        validate(ExecInst.CancelOnTradingHalt);
        validate(ExecInst.LastPeg);
        validate(ExecInst.MidPricePeg);
        validate(ExecInst.NonNegotiable);
        validate(ExecInst.OpeningPeg);
        validate(ExecInst.MarketPeg);
        validate(ExecInst.CancelOnSystemFailure);
        validate(ExecInst.PrimaryPeg);
        validate(ExecInst.Suspend);
        validate(ExecInst.FixedPegToLocalBestBidOrOfferAtTimeOfOrder);
        validate(ExecInst.CustomerDisplayInstruction);
        validate(ExecInst.Netting);
        validate(ExecInst.PegToVWAP);
        validate(ExecInst.TradeAlong);
        validate(ExecInst.TryToStop);
        validate(ExecInst.CancelIfNotBest);
        validate(ExecInst.TrailingStopPeg);
        validate(ExecInst.StrictLimit);
        validate(ExecInst.IgnorePriceValidityChecks);
        validate(ExecInst.PegToLimitPrice);
        validate(ExecInst.WorkToTargetStrategy);
        validate(ExecInst.IntermarketSweep);
        validate(ExecInst.ExternalRoutingAllowed);
        validate(ExecInst.ExternalRoutingNotAllowed);
        validate(ExecInst.ImbalanceOnly);
        validate(ExecInst.SingleExecutionRequestedForBlockTrade);
        validate(ExecInst.BestExecution);
        validate(ExecInst.SuspendOnSystemFailure);
        validate(ExecInst.SuspendOnTradingHalt);
        validate(ExecInst.ReinstateOnConnectionLoss);
        validate(ExecInst.CancelOnConnectionLoss);
        validate(ExecInst.SuspendOnConnectionLoss);
        validate(ExecInst.Release);
        validate(ExecInst.ExecuteAsDeltaNeutral);
        validate(ExecInst.ExecuteAsDurationNeutral);
        validate(ExecInst.ExecuteAsFXNeutral);
        validate(ExecInst.MinGuaranteedFillEligible);
        validate(ExecInst.BypassNonDisplayLiquidity);
        validate(ExecInst.Lock);
        validate(ExecInst.IgnoreNotionalValueChecks);
        validate(ExecInst.TrdAtRefPx);
        validate(ExecInst.AllowFacilitation);
    });

    test('should be immutable', () => {
        const ref = ExecInst;
        expect(() => {
            ref.StayOnOfferSide = 10;
        }).toThrowError();
    });
});
