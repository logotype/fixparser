import { GTBookingInst } from '../../src/fieldtypes/GTBookingInst';

describe('GTBookingInst', () => {
    test('should have the correct values', () => {
        expect(GTBookingInst.BookOutAllTradesOnDayOfExecution).toBe(0);
        expect(GTBookingInst.AccumulateUntilFilledOrExpired).toBe(1);
        expect(GTBookingInst.AccumulateUntilVerballyNotifiedOtherwise).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            GTBookingInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            GTBookingInst.BookOutAllTradesOnDayOfExecution,
            GTBookingInst.AccumulateUntilFilledOrExpired,
            GTBookingInst.AccumulateUntilVerballyNotifiedOtherwise,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for GTBookingInst type', () => {
        const validate = (value: GTBookingInst) => {
            expect(Object.values(GTBookingInst)).toContain(value);
        };

        validate(GTBookingInst.BookOutAllTradesOnDayOfExecution);
        validate(GTBookingInst.AccumulateUntilFilledOrExpired);
        validate(GTBookingInst.AccumulateUntilVerballyNotifiedOtherwise);
    });

    test('should be immutable', () => {
        const ref = GTBookingInst;
        expect(() => {
            ref.BookOutAllTradesOnDayOfExecution = 10;
        }).toThrowError();
    });
});
