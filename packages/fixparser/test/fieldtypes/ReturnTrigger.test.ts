import { ReturnTrigger } from '../../src/fieldtypes/ReturnTrigger';

describe('ReturnTrigger', () => {
    test('should have the correct values', () => {
        expect(ReturnTrigger.Dividend).toBe(1);
        expect(ReturnTrigger.Variance).toBe(2);
        expect(ReturnTrigger.Volatility).toBe(3);
        expect(ReturnTrigger.TotalReturn).toBe(4);
        expect(ReturnTrigger.ContractForDifference).toBe(5);
        expect(ReturnTrigger.CreditDefault).toBe(6);
        expect(ReturnTrigger.SpreadBet).toBe(7);
        expect(ReturnTrigger.Price).toBe(8);
        expect(ReturnTrigger.ForwardPriceUnderlyingInstrument).toBe(9);
        expect(ReturnTrigger.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnTrigger.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReturnTrigger.Dividend,
            ReturnTrigger.Variance,
            ReturnTrigger.Volatility,
            ReturnTrigger.TotalReturn,
            ReturnTrigger.ContractForDifference,
            ReturnTrigger.CreditDefault,
            ReturnTrigger.SpreadBet,
            ReturnTrigger.Price,
            ReturnTrigger.ForwardPriceUnderlyingInstrument,
            ReturnTrigger.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnTrigger type', () => {
        const validate = (value: ReturnTrigger) => {
            expect(Object.values(ReturnTrigger)).toContain(value);
        };

        validate(ReturnTrigger.Dividend);
        validate(ReturnTrigger.Variance);
        validate(ReturnTrigger.Volatility);
        validate(ReturnTrigger.TotalReturn);
        validate(ReturnTrigger.ContractForDifference);
        validate(ReturnTrigger.CreditDefault);
        validate(ReturnTrigger.SpreadBet);
        validate(ReturnTrigger.Price);
        validate(ReturnTrigger.ForwardPriceUnderlyingInstrument);
        validate(ReturnTrigger.Other);
    });

    test('should be immutable', () => {
        const ref = ReturnTrigger;
        expect(() => {
            ref.Dividend = 10;
        }).toThrowError();
    });
});
