import { BidRequestTransType } from '../../src/fieldtypes/BidRequestTransType';

describe('BidRequestTransType', () => {
    test('should have the correct values', () => {
        expect(BidRequestTransType.Cancel).toBe('C');
        expect(BidRequestTransType.New).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BidRequestTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BidRequestTransType.Cancel, BidRequestTransType.New];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BidRequestTransType type', () => {
        const validate = (value: BidRequestTransType) => {
            expect(Object.values(BidRequestTransType)).toContain(value);
        };

        validate(BidRequestTransType.Cancel);
        validate(BidRequestTransType.New);
    });

    test('should be immutable', () => {
        const ref = BidRequestTransType;
        expect(() => {
            ref.Cancel = 10;
        }).toThrowError();
    });
});
