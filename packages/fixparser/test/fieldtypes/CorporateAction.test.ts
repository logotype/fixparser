import { CorporateAction } from '../../src/fieldtypes/CorporateAction';

describe('CorporateAction', () => {
    test('should have the correct values', () => {
        expect(CorporateAction.ExDividend).toBe('A');
        expect(CorporateAction.ExDistribution).toBe('B');
        expect(CorporateAction.ExRights).toBe('C');
        expect(CorporateAction.New).toBe('D');
        expect(CorporateAction.ExInterest).toBe('E');
        expect(CorporateAction.CashDividend).toBe('F');
        expect(CorporateAction.StockDividend).toBe('G');
        expect(CorporateAction.NonIntegerStockSplit).toBe('H');
        expect(CorporateAction.ReverseStockSplit).toBe('I');
        expect(CorporateAction.StandardIntegerStockSplit).toBe('J');
        expect(CorporateAction.PositionConsolidation).toBe('K');
        expect(CorporateAction.LiquidationReorganization).toBe('L');
        expect(CorporateAction.MergerReorganization).toBe('M');
        expect(CorporateAction.RightsOffering).toBe('N');
        expect(CorporateAction.ShareholderMeeting).toBe('O');
        expect(CorporateAction.Spinoff).toBe('P');
        expect(CorporateAction.TenderOffer).toBe('Q');
        expect(CorporateAction.Warrant).toBe('R');
        expect(CorporateAction.SpecialAction).toBe('S');
        expect(CorporateAction.SymbolConversion).toBe('T');
        expect(CorporateAction.CUSIP).toBe('U');
        expect(CorporateAction.LeapRollover).toBe('V');
        expect(CorporateAction.SuccessionEvent).toBe('W');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CorporateAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CorporateAction.ExDividend,
            CorporateAction.ExDistribution,
            CorporateAction.ExRights,
            CorporateAction.New,
            CorporateAction.ExInterest,
            CorporateAction.CashDividend,
            CorporateAction.StockDividend,
            CorporateAction.NonIntegerStockSplit,
            CorporateAction.ReverseStockSplit,
            CorporateAction.StandardIntegerStockSplit,
            CorporateAction.PositionConsolidation,
            CorporateAction.LiquidationReorganization,
            CorporateAction.MergerReorganization,
            CorporateAction.RightsOffering,
            CorporateAction.ShareholderMeeting,
            CorporateAction.Spinoff,
            CorporateAction.TenderOffer,
            CorporateAction.Warrant,
            CorporateAction.SpecialAction,
            CorporateAction.SymbolConversion,
            CorporateAction.CUSIP,
            CorporateAction.LeapRollover,
            CorporateAction.SuccessionEvent,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CorporateAction type', () => {
        const validate = (value: CorporateAction) => {
            expect(Object.values(CorporateAction)).toContain(value);
        };

        validate(CorporateAction.ExDividend);
        validate(CorporateAction.ExDistribution);
        validate(CorporateAction.ExRights);
        validate(CorporateAction.New);
        validate(CorporateAction.ExInterest);
        validate(CorporateAction.CashDividend);
        validate(CorporateAction.StockDividend);
        validate(CorporateAction.NonIntegerStockSplit);
        validate(CorporateAction.ReverseStockSplit);
        validate(CorporateAction.StandardIntegerStockSplit);
        validate(CorporateAction.PositionConsolidation);
        validate(CorporateAction.LiquidationReorganization);
        validate(CorporateAction.MergerReorganization);
        validate(CorporateAction.RightsOffering);
        validate(CorporateAction.ShareholderMeeting);
        validate(CorporateAction.Spinoff);
        validate(CorporateAction.TenderOffer);
        validate(CorporateAction.Warrant);
        validate(CorporateAction.SpecialAction);
        validate(CorporateAction.SymbolConversion);
        validate(CorporateAction.CUSIP);
        validate(CorporateAction.LeapRollover);
        validate(CorporateAction.SuccessionEvent);
    });

    test('should be immutable', () => {
        const ref = CorporateAction;
        expect(() => {
            ref.ExDividend = 10;
        }).toThrowError();
    });
});
