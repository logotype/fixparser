import { TestSuiteRequestStatus } from '../../src/fieldtypes/TestSuiteRequestStatus';

describe('TestSuiteRequestStatus', () => {
    test('should have the correct values', () => {
        expect(TestSuiteRequestStatus.Received).toBe(0);
        expect(TestSuiteRequestStatus.Accepted).toBe(1);
        expect(TestSuiteRequestStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestSuiteRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TestSuiteRequestStatus.Received,
            TestSuiteRequestStatus.Accepted,
            TestSuiteRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestSuiteRequestStatus type', () => {
        const validate = (value: TestSuiteRequestStatus) => {
            expect(Object.values(TestSuiteRequestStatus)).toContain(value);
        };

        validate(TestSuiteRequestStatus.Received);
        validate(TestSuiteRequestStatus.Accepted);
        validate(TestSuiteRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TestSuiteRequestStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
