import { ShortSaleRestriction } from '../../src/fieldtypes/ShortSaleRestriction';

describe('ShortSaleRestriction', () => {
    test('should have the correct values', () => {
        expect(ShortSaleRestriction.NoRestrictions).toBe(0);
        expect(ShortSaleRestriction.SecurityNotShortable).toBe(1);
        expect(ShortSaleRestriction.SecurityNotShortableAtOrBelowBestBid).toBe(2);
        expect(ShortSaleRestriction.SecurityNotShortableWithoutPreBorrow).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ShortSaleRestriction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ShortSaleRestriction.NoRestrictions,
            ShortSaleRestriction.SecurityNotShortable,
            ShortSaleRestriction.SecurityNotShortableAtOrBelowBestBid,
            ShortSaleRestriction.SecurityNotShortableWithoutPreBorrow,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ShortSaleRestriction type', () => {
        const validate = (value: ShortSaleRestriction) => {
            expect(Object.values(ShortSaleRestriction)).toContain(value);
        };

        validate(ShortSaleRestriction.NoRestrictions);
        validate(ShortSaleRestriction.SecurityNotShortable);
        validate(ShortSaleRestriction.SecurityNotShortableAtOrBelowBestBid);
        validate(ShortSaleRestriction.SecurityNotShortableWithoutPreBorrow);
    });

    test('should be immutable', () => {
        const ref = ShortSaleRestriction;
        expect(() => {
            ref.NoRestrictions = 10;
        }).toThrowError();
    });
});
