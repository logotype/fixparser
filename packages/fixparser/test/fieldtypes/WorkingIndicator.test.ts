import { WorkingIndicator } from '../../src/fieldtypes/WorkingIndicator';

describe('WorkingIndicator', () => {
    test('should have the correct values', () => {
        expect(WorkingIndicator.NotWorking).toBe('N');
        expect(WorkingIndicator.Working).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            WorkingIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [WorkingIndicator.NotWorking, WorkingIndicator.Working];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for WorkingIndicator type', () => {
        const validate = (value: WorkingIndicator) => {
            expect(Object.values(WorkingIndicator)).toContain(value);
        };

        validate(WorkingIndicator.NotWorking);
        validate(WorkingIndicator.Working);
    });

    test('should be immutable', () => {
        const ref = WorkingIndicator;
        expect(() => {
            ref.NotWorking = 10;
        }).toThrowError();
    });
});
