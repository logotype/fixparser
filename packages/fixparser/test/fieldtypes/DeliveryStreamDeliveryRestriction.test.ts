import { DeliveryStreamDeliveryRestriction } from '../../src/fieldtypes/DeliveryStreamDeliveryRestriction';

describe('DeliveryStreamDeliveryRestriction', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamDeliveryRestriction.Firm).toBe(1);
        expect(DeliveryStreamDeliveryRestriction.NonFirm).toBe(2);
        expect(DeliveryStreamDeliveryRestriction.ForceMajeure).toBe(3);
        expect(DeliveryStreamDeliveryRestriction.SystemFirm).toBe(4);
        expect(DeliveryStreamDeliveryRestriction.UnitFirm).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamDeliveryRestriction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryStreamDeliveryRestriction.Firm,
            DeliveryStreamDeliveryRestriction.NonFirm,
            DeliveryStreamDeliveryRestriction.ForceMajeure,
            DeliveryStreamDeliveryRestriction.SystemFirm,
            DeliveryStreamDeliveryRestriction.UnitFirm,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamDeliveryRestriction type', () => {
        const validate = (value: DeliveryStreamDeliveryRestriction) => {
            expect(Object.values(DeliveryStreamDeliveryRestriction)).toContain(value);
        };

        validate(DeliveryStreamDeliveryRestriction.Firm);
        validate(DeliveryStreamDeliveryRestriction.NonFirm);
        validate(DeliveryStreamDeliveryRestriction.ForceMajeure);
        validate(DeliveryStreamDeliveryRestriction.SystemFirm);
        validate(DeliveryStreamDeliveryRestriction.UnitFirm);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamDeliveryRestriction;
        expect(() => {
            ref.Firm = 10;
        }).toThrowError();
    });
});
