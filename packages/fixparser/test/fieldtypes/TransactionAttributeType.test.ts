import { TransactionAttributeType } from '../../src/fieldtypes/TransactionAttributeType';

describe('TransactionAttributeType', () => {
    test('should have the correct values', () => {
        expect(TransactionAttributeType.ExclusiveArrangement).toBe(0);
        expect(TransactionAttributeType.CollateralReuse).toBe(1);
        expect(TransactionAttributeType.CollateralArrangmentType).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransactionAttributeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TransactionAttributeType.ExclusiveArrangement,
            TransactionAttributeType.CollateralReuse,
            TransactionAttributeType.CollateralArrangmentType,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransactionAttributeType type', () => {
        const validate = (value: TransactionAttributeType) => {
            expect(Object.values(TransactionAttributeType)).toContain(value);
        };

        validate(TransactionAttributeType.ExclusiveArrangement);
        validate(TransactionAttributeType.CollateralReuse);
        validate(TransactionAttributeType.CollateralArrangmentType);
    });

    test('should be immutable', () => {
        const ref = TransactionAttributeType;
        expect(() => {
            ref.ExclusiveArrangement = 10;
        }).toThrowError();
    });
});
