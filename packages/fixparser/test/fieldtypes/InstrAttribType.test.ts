import { InstrAttribType } from '../../src/fieldtypes/InstrAttribType';

describe('InstrAttribType', () => {
    test('should have the correct values', () => {
        expect(InstrAttribType.Flat).toBe(1);
        expect(InstrAttribType.ZeroCoupon).toBe(2);
        expect(InstrAttribType.InterestBearing).toBe(3);
        expect(InstrAttribType.NoPeriodicPayments).toBe(4);
        expect(InstrAttribType.VariableRate).toBe(5);
        expect(InstrAttribType.LessFeeForPut).toBe(6);
        expect(InstrAttribType.SteppedCoupon).toBe(7);
        expect(InstrAttribType.CouponPeriod).toBe(8);
        expect(InstrAttribType.When).toBe(9);
        expect(InstrAttribType.OriginalIssueDiscount).toBe(10);
        expect(InstrAttribType.Callable).toBe(11);
        expect(InstrAttribType.EscrowedToMaturity).toBe(12);
        expect(InstrAttribType.EscrowedToRedemptionDate).toBe(13);
        expect(InstrAttribType.PreRefunded).toBe(14);
        expect(InstrAttribType.InDefault).toBe(15);
        expect(InstrAttribType.Unrated).toBe(16);
        expect(InstrAttribType.Taxable).toBe(17);
        expect(InstrAttribType.Indexed).toBe(18);
        expect(InstrAttribType.SubjectToAlternativeMinimumTax).toBe(19);
        expect(InstrAttribType.OriginalIssueDiscountPrice).toBe(20);
        expect(InstrAttribType.CallableBelowMaturityValue).toBe(21);
        expect(InstrAttribType.CallableWithoutNotice).toBe(22);
        expect(InstrAttribType.PriceTickRulesForSecurity).toBe(23);
        expect(InstrAttribType.TradeTypeEligibilityDetailsForSecurity).toBe(24);
        expect(InstrAttribType.InstrumentDenominator).toBe(25);
        expect(InstrAttribType.InstrumentNumerator).toBe(26);
        expect(InstrAttribType.InstrumentPricePrecision).toBe(27);
        expect(InstrAttribType.InstrumentStrikePrice).toBe(28);
        expect(InstrAttribType.TradeableIndicator).toBe(29);
        expect(InstrAttribType.InstrumentEligibleAnonOrders).toBe(30);
        expect(InstrAttribType.MinGuaranteedFillVolume).toBe(31);
        expect(InstrAttribType.MinGuaranteedFillStatus).toBe(32);
        expect(InstrAttribType.TradeAtSettlementEligibility).toBe(33);
        expect(InstrAttribType.TestInstrument).toBe(34);
        expect(InstrAttribType.DummyInstrument).toBe(35);
        expect(InstrAttribType.NegativeSettlementPriceEligibility).toBe(36);
        expect(InstrAttribType.NegativeStrikePriceEligibility).toBe(37);
        expect(InstrAttribType.USStdContractInd).toBe(38);
        expect(InstrAttribType.AdmittedToTradingOnTradingVenue).toBe(39);
        expect(InstrAttribType.AverageDailyNotionalAmount).toBe(40);
        expect(InstrAttribType.AverageDailyNumberTrades).toBe(41);
        expect(InstrAttribType.Text).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            InstrAttribType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            InstrAttribType.Flat,
            InstrAttribType.ZeroCoupon,
            InstrAttribType.InterestBearing,
            InstrAttribType.NoPeriodicPayments,
            InstrAttribType.VariableRate,
            InstrAttribType.LessFeeForPut,
            InstrAttribType.SteppedCoupon,
            InstrAttribType.CouponPeriod,
            InstrAttribType.When,
            InstrAttribType.OriginalIssueDiscount,
            InstrAttribType.Callable,
            InstrAttribType.EscrowedToMaturity,
            InstrAttribType.EscrowedToRedemptionDate,
            InstrAttribType.PreRefunded,
            InstrAttribType.InDefault,
            InstrAttribType.Unrated,
            InstrAttribType.Taxable,
            InstrAttribType.Indexed,
            InstrAttribType.SubjectToAlternativeMinimumTax,
            InstrAttribType.OriginalIssueDiscountPrice,
            InstrAttribType.CallableBelowMaturityValue,
            InstrAttribType.CallableWithoutNotice,
            InstrAttribType.PriceTickRulesForSecurity,
            InstrAttribType.TradeTypeEligibilityDetailsForSecurity,
            InstrAttribType.InstrumentDenominator,
            InstrAttribType.InstrumentNumerator,
            InstrAttribType.InstrumentPricePrecision,
            InstrAttribType.InstrumentStrikePrice,
            InstrAttribType.TradeableIndicator,
            InstrAttribType.InstrumentEligibleAnonOrders,
            InstrAttribType.MinGuaranteedFillVolume,
            InstrAttribType.MinGuaranteedFillStatus,
            InstrAttribType.TradeAtSettlementEligibility,
            InstrAttribType.TestInstrument,
            InstrAttribType.DummyInstrument,
            InstrAttribType.NegativeSettlementPriceEligibility,
            InstrAttribType.NegativeStrikePriceEligibility,
            InstrAttribType.USStdContractInd,
            InstrAttribType.AdmittedToTradingOnTradingVenue,
            InstrAttribType.AverageDailyNotionalAmount,
            InstrAttribType.AverageDailyNumberTrades,
            InstrAttribType.Text,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for InstrAttribType type', () => {
        const validate = (value: InstrAttribType) => {
            expect(Object.values(InstrAttribType)).toContain(value);
        };

        validate(InstrAttribType.Flat);
        validate(InstrAttribType.ZeroCoupon);
        validate(InstrAttribType.InterestBearing);
        validate(InstrAttribType.NoPeriodicPayments);
        validate(InstrAttribType.VariableRate);
        validate(InstrAttribType.LessFeeForPut);
        validate(InstrAttribType.SteppedCoupon);
        validate(InstrAttribType.CouponPeriod);
        validate(InstrAttribType.When);
        validate(InstrAttribType.OriginalIssueDiscount);
        validate(InstrAttribType.Callable);
        validate(InstrAttribType.EscrowedToMaturity);
        validate(InstrAttribType.EscrowedToRedemptionDate);
        validate(InstrAttribType.PreRefunded);
        validate(InstrAttribType.InDefault);
        validate(InstrAttribType.Unrated);
        validate(InstrAttribType.Taxable);
        validate(InstrAttribType.Indexed);
        validate(InstrAttribType.SubjectToAlternativeMinimumTax);
        validate(InstrAttribType.OriginalIssueDiscountPrice);
        validate(InstrAttribType.CallableBelowMaturityValue);
        validate(InstrAttribType.CallableWithoutNotice);
        validate(InstrAttribType.PriceTickRulesForSecurity);
        validate(InstrAttribType.TradeTypeEligibilityDetailsForSecurity);
        validate(InstrAttribType.InstrumentDenominator);
        validate(InstrAttribType.InstrumentNumerator);
        validate(InstrAttribType.InstrumentPricePrecision);
        validate(InstrAttribType.InstrumentStrikePrice);
        validate(InstrAttribType.TradeableIndicator);
        validate(InstrAttribType.InstrumentEligibleAnonOrders);
        validate(InstrAttribType.MinGuaranteedFillVolume);
        validate(InstrAttribType.MinGuaranteedFillStatus);
        validate(InstrAttribType.TradeAtSettlementEligibility);
        validate(InstrAttribType.TestInstrument);
        validate(InstrAttribType.DummyInstrument);
        validate(InstrAttribType.NegativeSettlementPriceEligibility);
        validate(InstrAttribType.NegativeStrikePriceEligibility);
        validate(InstrAttribType.USStdContractInd);
        validate(InstrAttribType.AdmittedToTradingOnTradingVenue);
        validate(InstrAttribType.AverageDailyNotionalAmount);
        validate(InstrAttribType.AverageDailyNumberTrades);
        validate(InstrAttribType.Text);
    });

    test('should be immutable', () => {
        const ref = InstrAttribType;
        expect(() => {
            ref.Flat = 10;
        }).toThrowError();
    });
});
