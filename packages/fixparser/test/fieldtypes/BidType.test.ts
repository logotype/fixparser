import { BidType } from '../../src/fieldtypes/BidType';

describe('BidType', () => {
    test('should have the correct values', () => {
        expect(BidType.NonDisclosed).toBe(1);
        expect(BidType.Disclosed).toBe(2);
        expect(BidType.NoBiddingProcess).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BidType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BidType.NonDisclosed, BidType.Disclosed, BidType.NoBiddingProcess];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BidType type', () => {
        const validate = (value: BidType) => {
            expect(Object.values(BidType)).toContain(value);
        };

        validate(BidType.NonDisclosed);
        validate(BidType.Disclosed);
        validate(BidType.NoBiddingProcess);
    });

    test('should be immutable', () => {
        const ref = BidType;
        expect(() => {
            ref.NonDisclosed = 10;
        }).toThrowError();
    });
});
