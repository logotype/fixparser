import { DeskType } from '../../src/fieldtypes/DeskType';

describe('DeskType', () => {
    test('should have the correct values', () => {
        expect(DeskType.Agency).toBe('A');
        expect(DeskType.Arbitrage).toBe('AR');
        expect(DeskType.BlockTrading).toBe('B');
        expect(DeskType.ConvertibleDesk).toBe('C');
        expect(DeskType.CentralRiskBooks).toBe('CR');
        expect(DeskType.Derivatives).toBe('D');
        expect(DeskType.EquityCapitalMarkets).toBe('EC');
        expect(DeskType.International).toBe('IN');
        expect(DeskType.Institutional).toBe('IS');
        expect(DeskType.Other).toBe('O');
        expect(DeskType.PreferredTrading).toBe('PF');
        expect(DeskType.Proprietary).toBe('PR');
        expect(DeskType.ProgramTrading).toBe('PT');
        expect(DeskType.Sales).toBe('S');
        expect(DeskType.Swaps).toBe('SW');
        expect(DeskType.TradingDeskSystem).toBe('T');
        expect(DeskType.Treasury).toBe('TR');
        expect(DeskType.FloorBroker).toBe('FB');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeskType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeskType.Agency,
            DeskType.Arbitrage,
            DeskType.BlockTrading,
            DeskType.ConvertibleDesk,
            DeskType.CentralRiskBooks,
            DeskType.Derivatives,
            DeskType.EquityCapitalMarkets,
            DeskType.International,
            DeskType.Institutional,
            DeskType.Other,
            DeskType.PreferredTrading,
            DeskType.Proprietary,
            DeskType.ProgramTrading,
            DeskType.Sales,
            DeskType.Swaps,
            DeskType.TradingDeskSystem,
            DeskType.Treasury,
            DeskType.FloorBroker,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DeskType type', () => {
        const validate = (value: DeskType) => {
            expect(Object.values(DeskType)).toContain(value);
        };

        validate(DeskType.Agency);
        validate(DeskType.Arbitrage);
        validate(DeskType.BlockTrading);
        validate(DeskType.ConvertibleDesk);
        validate(DeskType.CentralRiskBooks);
        validate(DeskType.Derivatives);
        validate(DeskType.EquityCapitalMarkets);
        validate(DeskType.International);
        validate(DeskType.Institutional);
        validate(DeskType.Other);
        validate(DeskType.PreferredTrading);
        validate(DeskType.Proprietary);
        validate(DeskType.ProgramTrading);
        validate(DeskType.Sales);
        validate(DeskType.Swaps);
        validate(DeskType.TradingDeskSystem);
        validate(DeskType.Treasury);
        validate(DeskType.FloorBroker);
    });

    test('should be immutable', () => {
        const ref = DeskType;
        expect(() => {
            ref.Agency = 10;
        }).toThrowError();
    });
});
