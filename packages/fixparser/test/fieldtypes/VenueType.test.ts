import { VenueType } from '../../src/fieldtypes/VenueType';

describe('VenueType', () => {
    test('should have the correct values', () => {
        expect(VenueType.Electronic).toBe('E');
        expect(VenueType.Pit).toBe('P');
        expect(VenueType.ExPit).toBe('X');
        expect(VenueType.ClearingHouse).toBe('C');
        expect(VenueType.RegisteredMarket).toBe('R');
        expect(VenueType.OffMarket).toBe('O');
        expect(VenueType.CentralLimitOrderBook).toBe('B');
        expect(VenueType.QuoteDrivenMarket).toBe('Q');
        expect(VenueType.DarkOrderBook).toBe('D');
        expect(VenueType.AuctionDrivenMarket).toBe('A');
        expect(VenueType.QuoteNegotiation).toBe('N');
        expect(VenueType.VoiceNegotiation).toBe('V');
        expect(VenueType.HybridMarket).toBe('H');
        expect(VenueType.OtherMarket).toBe('z');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            VenueType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            VenueType.Electronic,
            VenueType.Pit,
            VenueType.ExPit,
            VenueType.ClearingHouse,
            VenueType.RegisteredMarket,
            VenueType.OffMarket,
            VenueType.CentralLimitOrderBook,
            VenueType.QuoteDrivenMarket,
            VenueType.DarkOrderBook,
            VenueType.AuctionDrivenMarket,
            VenueType.QuoteNegotiation,
            VenueType.VoiceNegotiation,
            VenueType.HybridMarket,
            VenueType.OtherMarket,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for VenueType type', () => {
        const validate = (value: VenueType) => {
            expect(Object.values(VenueType)).toContain(value);
        };

        validate(VenueType.Electronic);
        validate(VenueType.Pit);
        validate(VenueType.ExPit);
        validate(VenueType.ClearingHouse);
        validate(VenueType.RegisteredMarket);
        validate(VenueType.OffMarket);
        validate(VenueType.CentralLimitOrderBook);
        validate(VenueType.QuoteDrivenMarket);
        validate(VenueType.DarkOrderBook);
        validate(VenueType.AuctionDrivenMarket);
        validate(VenueType.QuoteNegotiation);
        validate(VenueType.VoiceNegotiation);
        validate(VenueType.HybridMarket);
        validate(VenueType.OtherMarket);
    });

    test('should be immutable', () => {
        const ref = VenueType;
        expect(() => {
            ref.Electronic = 10;
        }).toThrowError();
    });
});
