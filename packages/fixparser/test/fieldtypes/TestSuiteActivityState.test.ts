import { TestSuiteActivityState } from '../../src/fieldtypes/TestSuiteActivityState';

describe('TestSuiteActivityState', () => {
    test('should have the correct values', () => {
        expect(TestSuiteActivityState.Scheduled).toBe(0);
        expect(TestSuiteActivityState.Completed).toBe(1);
        expect(TestSuiteActivityState.Cancelled).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestSuiteActivityState.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TestSuiteActivityState.Scheduled,
            TestSuiteActivityState.Completed,
            TestSuiteActivityState.Cancelled,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestSuiteActivityState type', () => {
        const validate = (value: TestSuiteActivityState) => {
            expect(Object.values(TestSuiteActivityState)).toContain(value);
        };

        validate(TestSuiteActivityState.Scheduled);
        validate(TestSuiteActivityState.Completed);
        validate(TestSuiteActivityState.Cancelled);
    });

    test('should be immutable', () => {
        const ref = TestSuiteActivityState;
        expect(() => {
            ref.Scheduled = 10;
        }).toThrowError();
    });
});
