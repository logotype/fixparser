import { InViewOfCommon } from '../../src/fieldtypes/InViewOfCommon';

describe('InViewOfCommon', () => {
    test('should have the correct values', () => {
        expect(InViewOfCommon.HaltWasNotRelatedToAHaltOfTheCommonStock).toBe('N');
        expect(InViewOfCommon.HaltWasDueToCommonStockBeingHalted).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            InViewOfCommon.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            InViewOfCommon.HaltWasNotRelatedToAHaltOfTheCommonStock,
            InViewOfCommon.HaltWasDueToCommonStockBeingHalted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for InViewOfCommon type', () => {
        const validate = (value: InViewOfCommon) => {
            expect(Object.values(InViewOfCommon)).toContain(value);
        };

        validate(InViewOfCommon.HaltWasNotRelatedToAHaltOfTheCommonStock);
        validate(InViewOfCommon.HaltWasDueToCommonStockBeingHalted);
    });

    test('should be immutable', () => {
        const ref = InViewOfCommon;
        expect(() => {
            ref.HaltWasNotRelatedToAHaltOfTheCommonStock = 10;
        }).toThrowError();
    });
});
