import { UnderlyingFXRateCalc } from '../../src/fieldtypes/UnderlyingFXRateCalc';

describe('UnderlyingFXRateCalc', () => {
    test('should have the correct values', () => {
        expect(UnderlyingFXRateCalc.Divide).toBe('D');
        expect(UnderlyingFXRateCalc.Multiply).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingFXRateCalc.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [UnderlyingFXRateCalc.Divide, UnderlyingFXRateCalc.Multiply];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for UnderlyingFXRateCalc type', () => {
        const validate = (value: UnderlyingFXRateCalc) => {
            expect(Object.values(UnderlyingFXRateCalc)).toContain(value);
        };

        validate(UnderlyingFXRateCalc.Divide);
        validate(UnderlyingFXRateCalc.Multiply);
    });

    test('should be immutable', () => {
        const ref = UnderlyingFXRateCalc;
        expect(() => {
            ref.Divide = 10;
        }).toThrowError();
    });
});
