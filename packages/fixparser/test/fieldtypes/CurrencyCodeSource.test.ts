import { CurrencyCodeSource } from '../../src/fieldtypes/CurrencyCodeSource';

describe('CurrencyCodeSource', () => {
    test('should have the correct values', () => {
        expect(CurrencyCodeSource.CUSIP).toBe('1');
        expect(CurrencyCodeSource.SEDOL).toBe('2');
        expect(CurrencyCodeSource.ISINNumber).toBe('4');
        expect(CurrencyCodeSource.ISOCurrencyCode).toBe('6');
        expect(CurrencyCodeSource.FinancialInstrumentGlobalIdentifier).toBe('S');
        expect(CurrencyCodeSource.DigitalTokenIdentifier).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CurrencyCodeSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CurrencyCodeSource.CUSIP,
            CurrencyCodeSource.SEDOL,
            CurrencyCodeSource.ISINNumber,
            CurrencyCodeSource.ISOCurrencyCode,
            CurrencyCodeSource.FinancialInstrumentGlobalIdentifier,
            CurrencyCodeSource.DigitalTokenIdentifier,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CurrencyCodeSource type', () => {
        const validate = (value: CurrencyCodeSource) => {
            expect(Object.values(CurrencyCodeSource)).toContain(value);
        };

        validate(CurrencyCodeSource.CUSIP);
        validate(CurrencyCodeSource.SEDOL);
        validate(CurrencyCodeSource.ISINNumber);
        validate(CurrencyCodeSource.ISOCurrencyCode);
        validate(CurrencyCodeSource.FinancialInstrumentGlobalIdentifier);
        validate(CurrencyCodeSource.DigitalTokenIdentifier);
    });

    test('should be immutable', () => {
        const ref = CurrencyCodeSource;
        expect(() => {
            ref.CUSIP = 10;
        }).toThrowError();
    });
});
