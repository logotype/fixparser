import { LimitAmtType } from '../../src/fieldtypes/LimitAmtType';

describe('LimitAmtType', () => {
    test('should have the correct values', () => {
        expect(LimitAmtType.CreditLimit).toBe(0);
        expect(LimitAmtType.GrossPositionLimit).toBe(1);
        expect(LimitAmtType.NetPositionLimit).toBe(2);
        expect(LimitAmtType.RiskExposureLimit).toBe(3);
        expect(LimitAmtType.LongPositionLimit).toBe(4);
        expect(LimitAmtType.ShortPositionLimit).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LimitAmtType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LimitAmtType.CreditLimit,
            LimitAmtType.GrossPositionLimit,
            LimitAmtType.NetPositionLimit,
            LimitAmtType.RiskExposureLimit,
            LimitAmtType.LongPositionLimit,
            LimitAmtType.ShortPositionLimit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LimitAmtType type', () => {
        const validate = (value: LimitAmtType) => {
            expect(Object.values(LimitAmtType)).toContain(value);
        };

        validate(LimitAmtType.CreditLimit);
        validate(LimitAmtType.GrossPositionLimit);
        validate(LimitAmtType.NetPositionLimit);
        validate(LimitAmtType.RiskExposureLimit);
        validate(LimitAmtType.LongPositionLimit);
        validate(LimitAmtType.ShortPositionLimit);
    });

    test('should be immutable', () => {
        const ref = LimitAmtType;
        expect(() => {
            ref.CreditLimit = 10;
        }).toThrowError();
    });
});
