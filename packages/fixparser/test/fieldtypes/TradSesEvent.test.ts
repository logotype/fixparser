import { TradSesEvent } from '../../src/fieldtypes/TradSesEvent';

describe('TradSesEvent', () => {
    test('should have the correct values', () => {
        expect(TradSesEvent.TradingResumes).toBe(0);
        expect(TradSesEvent.ChangeOfTradingSession).toBe(1);
        expect(TradSesEvent.ChangeOfTradingSubsession).toBe(2);
        expect(TradSesEvent.ChangeOfTradingStatus).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesEvent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradSesEvent.TradingResumes,
            TradSesEvent.ChangeOfTradingSession,
            TradSesEvent.ChangeOfTradingSubsession,
            TradSesEvent.ChangeOfTradingStatus,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesEvent type', () => {
        const validate = (value: TradSesEvent) => {
            expect(Object.values(TradSesEvent)).toContain(value);
        };

        validate(TradSesEvent.TradingResumes);
        validate(TradSesEvent.ChangeOfTradingSession);
        validate(TradSesEvent.ChangeOfTradingSubsession);
        validate(TradSesEvent.ChangeOfTradingStatus);
    });

    test('should be immutable', () => {
        const ref = TradSesEvent;
        expect(() => {
            ref.TradingResumes = 10;
        }).toThrowError();
    });
});
