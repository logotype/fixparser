import { PayReportTransType } from '../../src/fieldtypes/PayReportTransType';

describe('PayReportTransType', () => {
    test('should have the correct values', () => {
        expect(PayReportTransType.New).toBe(0);
        expect(PayReportTransType.Replace).toBe(1);
        expect(PayReportTransType.Status).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PayReportTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PayReportTransType.New, PayReportTransType.Replace, PayReportTransType.Status];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PayReportTransType type', () => {
        const validate = (value: PayReportTransType) => {
            expect(Object.values(PayReportTransType)).toContain(value);
        };

        validate(PayReportTransType.New);
        validate(PayReportTransType.Replace);
        validate(PayReportTransType.Status);
    });

    test('should be immutable', () => {
        const ref = PayReportTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
