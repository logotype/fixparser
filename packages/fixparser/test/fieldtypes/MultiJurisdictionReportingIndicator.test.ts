import { MultiJurisdictionReportingIndicator } from '../../src/fieldtypes/MultiJurisdictionReportingIndicator';

describe('MultiJurisdictionReportingIndicator', () => {
    test('should have the correct values', () => {
        expect(MultiJurisdictionReportingIndicator.NotMultiJrsdctnEligible).toBe(0);
        expect(MultiJurisdictionReportingIndicator.MultiJrsdctnEligible).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MultiJurisdictionReportingIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MultiJurisdictionReportingIndicator.NotMultiJrsdctnEligible,
            MultiJurisdictionReportingIndicator.MultiJrsdctnEligible,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MultiJurisdictionReportingIndicator type', () => {
        const validate = (value: MultiJurisdictionReportingIndicator) => {
            expect(Object.values(MultiJurisdictionReportingIndicator)).toContain(value);
        };

        validate(MultiJurisdictionReportingIndicator.NotMultiJrsdctnEligible);
        validate(MultiJurisdictionReportingIndicator.MultiJrsdctnEligible);
    });

    test('should be immutable', () => {
        const ref = MultiJurisdictionReportingIndicator;
        expect(() => {
            ref.NotMultiJrsdctnEligible = 10;
        }).toThrowError();
    });
});
