import { TerminationType } from '../../src/fieldtypes/TerminationType';

describe('TerminationType', () => {
    test('should have the correct values', () => {
        expect(TerminationType.Overnight).toBe(1);
        expect(TerminationType.Term).toBe(2);
        expect(TerminationType.Flexible).toBe(3);
        expect(TerminationType.Open).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TerminationType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TerminationType.Overnight,
            TerminationType.Term,
            TerminationType.Flexible,
            TerminationType.Open,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TerminationType type', () => {
        const validate = (value: TerminationType) => {
            expect(Object.values(TerminationType)).toContain(value);
        };

        validate(TerminationType.Overnight);
        validate(TerminationType.Term);
        validate(TerminationType.Flexible);
        validate(TerminationType.Open);
    });

    test('should be immutable', () => {
        const ref = TerminationType;
        expect(() => {
            ref.Overnight = 10;
        }).toThrowError();
    });
});
