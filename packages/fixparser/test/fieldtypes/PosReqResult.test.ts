import { PosReqResult } from '../../src/fieldtypes/PosReqResult';

describe('PosReqResult', () => {
    test('should have the correct values', () => {
        expect(PosReqResult.ValidRequest).toBe(0);
        expect(PosReqResult.InvalidOrUnsupportedRequest).toBe(1);
        expect(PosReqResult.NoPositionsFoundThatMatchCriteria).toBe(2);
        expect(PosReqResult.NotAuthorizedToRequestPositions).toBe(3);
        expect(PosReqResult.RequestForPositionNotSupported).toBe(4);
        expect(PosReqResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosReqResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosReqResult.ValidRequest,
            PosReqResult.InvalidOrUnsupportedRequest,
            PosReqResult.NoPositionsFoundThatMatchCriteria,
            PosReqResult.NotAuthorizedToRequestPositions,
            PosReqResult.RequestForPositionNotSupported,
            PosReqResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosReqResult type', () => {
        const validate = (value: PosReqResult) => {
            expect(Object.values(PosReqResult)).toContain(value);
        };

        validate(PosReqResult.ValidRequest);
        validate(PosReqResult.InvalidOrUnsupportedRequest);
        validate(PosReqResult.NoPositionsFoundThatMatchCriteria);
        validate(PosReqResult.NotAuthorizedToRequestPositions);
        validate(PosReqResult.RequestForPositionNotSupported);
        validate(PosReqResult.Other);
    });

    test('should be immutable', () => {
        const ref = PosReqResult;
        expect(() => {
            ref.ValidRequest = 10;
        }).toThrowError();
    });
});
