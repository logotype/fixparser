import { OrdStatus } from '../../src/fieldtypes/OrdStatus';

describe('OrdStatus', () => {
    test('should have the correct values', () => {
        expect(OrdStatus.New).toBe('0');
        expect(OrdStatus.PartiallyFilled).toBe('1');
        expect(OrdStatus.Filled).toBe('2');
        expect(OrdStatus.DoneForDay).toBe('3');
        expect(OrdStatus.Canceled).toBe('4');
        expect(OrdStatus.Replaced).toBe('5');
        expect(OrdStatus.PendingCancel).toBe('6');
        expect(OrdStatus.Stopped).toBe('7');
        expect(OrdStatus.Rejected).toBe('8');
        expect(OrdStatus.Suspended).toBe('9');
        expect(OrdStatus.PendingNew).toBe('A');
        expect(OrdStatus.Calculated).toBe('B');
        expect(OrdStatus.Expired).toBe('C');
        expect(OrdStatus.AcceptedForBidding).toBe('D');
        expect(OrdStatus.PendingReplace).toBe('E');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrdStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrdStatus.New,
            OrdStatus.PartiallyFilled,
            OrdStatus.Filled,
            OrdStatus.DoneForDay,
            OrdStatus.Canceled,
            OrdStatus.Replaced,
            OrdStatus.PendingCancel,
            OrdStatus.Stopped,
            OrdStatus.Rejected,
            OrdStatus.Suspended,
            OrdStatus.PendingNew,
            OrdStatus.Calculated,
            OrdStatus.Expired,
            OrdStatus.AcceptedForBidding,
            OrdStatus.PendingReplace,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrdStatus type', () => {
        const validate = (value: OrdStatus) => {
            expect(Object.values(OrdStatus)).toContain(value);
        };

        validate(OrdStatus.New);
        validate(OrdStatus.PartiallyFilled);
        validate(OrdStatus.Filled);
        validate(OrdStatus.DoneForDay);
        validate(OrdStatus.Canceled);
        validate(OrdStatus.Replaced);
        validate(OrdStatus.PendingCancel);
        validate(OrdStatus.Stopped);
        validate(OrdStatus.Rejected);
        validate(OrdStatus.Suspended);
        validate(OrdStatus.PendingNew);
        validate(OrdStatus.Calculated);
        validate(OrdStatus.Expired);
        validate(OrdStatus.AcceptedForBidding);
        validate(OrdStatus.PendingReplace);
    });

    test('should be immutable', () => {
        const ref = OrdStatus;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
