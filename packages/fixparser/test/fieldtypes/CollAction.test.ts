import { CollAction } from '../../src/fieldtypes/CollAction';

describe('CollAction', () => {
    test('should have the correct values', () => {
        expect(CollAction.Retain).toBe(0);
        expect(CollAction.Add).toBe(1);
        expect(CollAction.Remove).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CollAction.Retain, CollAction.Add, CollAction.Remove];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollAction type', () => {
        const validate = (value: CollAction) => {
            expect(Object.values(CollAction)).toContain(value);
        };

        validate(CollAction.Retain);
        validate(CollAction.Add);
        validate(CollAction.Remove);
    });

    test('should be immutable', () => {
        const ref = CollAction;
        expect(() => {
            ref.Retain = 10;
        }).toThrowError();
    });
});
