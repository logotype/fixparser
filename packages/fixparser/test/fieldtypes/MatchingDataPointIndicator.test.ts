import { MatchingDataPointIndicator } from '../../src/fieldtypes/MatchingDataPointIndicator';

describe('MatchingDataPointIndicator', () => {
    test('should have the correct values', () => {
        expect(MatchingDataPointIndicator.Mandatory).toBe(1);
        expect(MatchingDataPointIndicator.Optional).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchingDataPointIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MatchingDataPointIndicator.Mandatory, MatchingDataPointIndicator.Optional];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MatchingDataPointIndicator type', () => {
        const validate = (value: MatchingDataPointIndicator) => {
            expect(Object.values(MatchingDataPointIndicator)).toContain(value);
        };

        validate(MatchingDataPointIndicator.Mandatory);
        validate(MatchingDataPointIndicator.Optional);
    });

    test('should be immutable', () => {
        const ref = MatchingDataPointIndicator;
        expect(() => {
            ref.Mandatory = 10;
        }).toThrowError();
    });
});
