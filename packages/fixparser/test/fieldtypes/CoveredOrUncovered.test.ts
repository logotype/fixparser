import { CoveredOrUncovered } from '../../src/fieldtypes/CoveredOrUncovered';

describe('CoveredOrUncovered', () => {
    test('should have the correct values', () => {
        expect(CoveredOrUncovered.Covered).toBe(0);
        expect(CoveredOrUncovered.Uncovered).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CoveredOrUncovered.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CoveredOrUncovered.Covered, CoveredOrUncovered.Uncovered];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CoveredOrUncovered type', () => {
        const validate = (value: CoveredOrUncovered) => {
            expect(Object.values(CoveredOrUncovered)).toContain(value);
        };

        validate(CoveredOrUncovered.Covered);
        validate(CoveredOrUncovered.Uncovered);
    });

    test('should be immutable', () => {
        const ref = CoveredOrUncovered;
        expect(() => {
            ref.Covered = 10;
        }).toThrowError();
    });
});
