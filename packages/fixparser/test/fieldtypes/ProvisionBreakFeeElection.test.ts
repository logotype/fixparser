import { ProvisionBreakFeeElection } from '../../src/fieldtypes/ProvisionBreakFeeElection';

describe('ProvisionBreakFeeElection', () => {
    test('should have the correct values', () => {
        expect(ProvisionBreakFeeElection.FlatFee).toBe(0);
        expect(ProvisionBreakFeeElection.AmortizedFee).toBe(1);
        expect(ProvisionBreakFeeElection.FundingFee).toBe(2);
        expect(ProvisionBreakFeeElection.FlatAndFundingFee).toBe(3);
        expect(ProvisionBreakFeeElection.AmortizedAndFundingFee).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionBreakFeeElection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionBreakFeeElection.FlatFee,
            ProvisionBreakFeeElection.AmortizedFee,
            ProvisionBreakFeeElection.FundingFee,
            ProvisionBreakFeeElection.FlatAndFundingFee,
            ProvisionBreakFeeElection.AmortizedAndFundingFee,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionBreakFeeElection type', () => {
        const validate = (value: ProvisionBreakFeeElection) => {
            expect(Object.values(ProvisionBreakFeeElection)).toContain(value);
        };

        validate(ProvisionBreakFeeElection.FlatFee);
        validate(ProvisionBreakFeeElection.AmortizedFee);
        validate(ProvisionBreakFeeElection.FundingFee);
        validate(ProvisionBreakFeeElection.FlatAndFundingFee);
        validate(ProvisionBreakFeeElection.AmortizedAndFundingFee);
    });

    test('should be immutable', () => {
        const ref = ProvisionBreakFeeElection;
        expect(() => {
            ref.FlatFee = 10;
        }).toThrowError();
    });
});
