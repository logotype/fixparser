import { IOIQualifier } from '../../src/fieldtypes/IOIQualifier';

describe('IOIQualifier', () => {
    test('should have the correct values', () => {
        expect(IOIQualifier.AllOrNone).toBe('A');
        expect(IOIQualifier.MarketOnClose).toBe('B');
        expect(IOIQualifier.AtTheClose).toBe('C');
        expect(IOIQualifier.VWAP).toBe('D');
        expect(IOIQualifier.Axe).toBe('E');
        expect(IOIQualifier.AxeOnBid).toBe('F');
        expect(IOIQualifier.AxeOnOffer).toBe('G');
        expect(IOIQualifier.ClientNaturalWorking).toBe('H');
        expect(IOIQualifier.InTouchWith).toBe('I');
        expect(IOIQualifier.PositionWanted).toBe('J');
        expect(IOIQualifier.MarketMaking).toBe('K');
        expect(IOIQualifier.Limit).toBe('L');
        expect(IOIQualifier.MoreBehind).toBe('M');
        expect(IOIQualifier.ClientNaturalBlock).toBe('N');
        expect(IOIQualifier.AtTheOpen).toBe('O');
        expect(IOIQualifier.TakingAPosition).toBe('P');
        expect(IOIQualifier.AtTheMarket).toBe('Q');
        expect(IOIQualifier.ReadyToTrade).toBe('R');
        expect(IOIQualifier.PortfolioShown).toBe('S');
        expect(IOIQualifier.ThroughTheDay).toBe('T');
        expect(IOIQualifier.Unwind).toBe('U');
        expect(IOIQualifier.Versus).toBe('V');
        expect(IOIQualifier.Indication).toBe('W');
        expect(IOIQualifier.CrossingOpportunity).toBe('X');
        expect(IOIQualifier.AtTheMidpoint).toBe('Y');
        expect(IOIQualifier.PreOpen).toBe('Z');
        expect(IOIQualifier.QuantityNegotiable).toBe('1');
        expect(IOIQualifier.AllowLateBids).toBe('2');
        expect(IOIQualifier.ImmediateOrCounter).toBe('3');
        expect(IOIQualifier.AutoTrade).toBe('4');
        expect(IOIQualifier.AutomaticSpot).toBe('a');
        expect(IOIQualifier.PlatformCalculatedSpot).toBe('b');
        expect(IOIQualifier.OutsideSpread).toBe('c');
        expect(IOIQualifier.DeferredSpot).toBe('d');
        expect(IOIQualifier.NegotiatedSpot).toBe('n');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOIQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            IOIQualifier.AllOrNone,
            IOIQualifier.MarketOnClose,
            IOIQualifier.AtTheClose,
            IOIQualifier.VWAP,
            IOIQualifier.Axe,
            IOIQualifier.AxeOnBid,
            IOIQualifier.AxeOnOffer,
            IOIQualifier.ClientNaturalWorking,
            IOIQualifier.InTouchWith,
            IOIQualifier.PositionWanted,
            IOIQualifier.MarketMaking,
            IOIQualifier.Limit,
            IOIQualifier.MoreBehind,
            IOIQualifier.ClientNaturalBlock,
            IOIQualifier.AtTheOpen,
            IOIQualifier.TakingAPosition,
            IOIQualifier.AtTheMarket,
            IOIQualifier.ReadyToTrade,
            IOIQualifier.PortfolioShown,
            IOIQualifier.ThroughTheDay,
            IOIQualifier.Unwind,
            IOIQualifier.Versus,
            IOIQualifier.Indication,
            IOIQualifier.CrossingOpportunity,
            IOIQualifier.AtTheMidpoint,
            IOIQualifier.PreOpen,
            IOIQualifier.QuantityNegotiable,
            IOIQualifier.AllowLateBids,
            IOIQualifier.ImmediateOrCounter,
            IOIQualifier.AutoTrade,
            IOIQualifier.AutomaticSpot,
            IOIQualifier.PlatformCalculatedSpot,
            IOIQualifier.OutsideSpread,
            IOIQualifier.DeferredSpot,
            IOIQualifier.NegotiatedSpot,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOIQualifier type', () => {
        const validate = (value: IOIQualifier) => {
            expect(Object.values(IOIQualifier)).toContain(value);
        };

        validate(IOIQualifier.AllOrNone);
        validate(IOIQualifier.MarketOnClose);
        validate(IOIQualifier.AtTheClose);
        validate(IOIQualifier.VWAP);
        validate(IOIQualifier.Axe);
        validate(IOIQualifier.AxeOnBid);
        validate(IOIQualifier.AxeOnOffer);
        validate(IOIQualifier.ClientNaturalWorking);
        validate(IOIQualifier.InTouchWith);
        validate(IOIQualifier.PositionWanted);
        validate(IOIQualifier.MarketMaking);
        validate(IOIQualifier.Limit);
        validate(IOIQualifier.MoreBehind);
        validate(IOIQualifier.ClientNaturalBlock);
        validate(IOIQualifier.AtTheOpen);
        validate(IOIQualifier.TakingAPosition);
        validate(IOIQualifier.AtTheMarket);
        validate(IOIQualifier.ReadyToTrade);
        validate(IOIQualifier.PortfolioShown);
        validate(IOIQualifier.ThroughTheDay);
        validate(IOIQualifier.Unwind);
        validate(IOIQualifier.Versus);
        validate(IOIQualifier.Indication);
        validate(IOIQualifier.CrossingOpportunity);
        validate(IOIQualifier.AtTheMidpoint);
        validate(IOIQualifier.PreOpen);
        validate(IOIQualifier.QuantityNegotiable);
        validate(IOIQualifier.AllowLateBids);
        validate(IOIQualifier.ImmediateOrCounter);
        validate(IOIQualifier.AutoTrade);
        validate(IOIQualifier.AutomaticSpot);
        validate(IOIQualifier.PlatformCalculatedSpot);
        validate(IOIQualifier.OutsideSpread);
        validate(IOIQualifier.DeferredSpot);
        validate(IOIQualifier.NegotiatedSpot);
    });

    test('should be immutable', () => {
        const ref = IOIQualifier;
        expect(() => {
            ref.AllOrNone = 10;
        }).toThrowError();
    });
});
