import { PaymentStreamAveragingMethod } from '../../src/fieldtypes/PaymentStreamAveragingMethod';

describe('PaymentStreamAveragingMethod', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamAveragingMethod.Unweighted).toBe(0);
        expect(PaymentStreamAveragingMethod.Weighted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamAveragingMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamAveragingMethod.Unweighted, PaymentStreamAveragingMethod.Weighted];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamAveragingMethod type', () => {
        const validate = (value: PaymentStreamAveragingMethod) => {
            expect(Object.values(PaymentStreamAveragingMethod)).toContain(value);
        };

        validate(PaymentStreamAveragingMethod.Unweighted);
        validate(PaymentStreamAveragingMethod.Weighted);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamAveragingMethod;
        expect(() => {
            ref.Unweighted = 10;
        }).toThrowError();
    });
});
