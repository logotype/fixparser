import { OpenClose } from '../../src/fieldtypes/OpenClose';

describe('OpenClose', () => {
    test('should have the correct values', () => {
        expect(OpenClose.Close).toBe('C');
        expect(OpenClose.Open).toBe('O');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OpenClose.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OpenClose.Close, OpenClose.Open];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OpenClose type', () => {
        const validate = (value: OpenClose) => {
            expect(Object.values(OpenClose)).toContain(value);
        };

        validate(OpenClose.Close);
        validate(OpenClose.Open);
    });

    test('should be immutable', () => {
        const ref = OpenClose;
        expect(() => {
            ref.Close = 10;
        }).toThrowError();
    });
});
