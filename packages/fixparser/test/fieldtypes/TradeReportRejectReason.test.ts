import { TradeReportRejectReason } from '../../src/fieldtypes/TradeReportRejectReason';

describe('TradeReportRejectReason', () => {
    test('should have the correct values', () => {
        expect(TradeReportRejectReason.Successful).toBe(0);
        expect(TradeReportRejectReason.InvalidPartyInformation).toBe(1);
        expect(TradeReportRejectReason.UnknownInstrument).toBe(2);
        expect(TradeReportRejectReason.UnauthorizedToReportTrades).toBe(3);
        expect(TradeReportRejectReason.InvalidTradeType).toBe(4);
        expect(TradeReportRejectReason.PriceExceedsCurrentPriceBand).toBe(5);
        expect(TradeReportRejectReason.ReferencePriceNotAvailable).toBe(6);
        expect(TradeReportRejectReason.NotionalValueExceedsThreshold).toBe(7);
        expect(TradeReportRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeReportRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeReportRejectReason.Successful,
            TradeReportRejectReason.InvalidPartyInformation,
            TradeReportRejectReason.UnknownInstrument,
            TradeReportRejectReason.UnauthorizedToReportTrades,
            TradeReportRejectReason.InvalidTradeType,
            TradeReportRejectReason.PriceExceedsCurrentPriceBand,
            TradeReportRejectReason.ReferencePriceNotAvailable,
            TradeReportRejectReason.NotionalValueExceedsThreshold,
            TradeReportRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeReportRejectReason type', () => {
        const validate = (value: TradeReportRejectReason) => {
            expect(Object.values(TradeReportRejectReason)).toContain(value);
        };

        validate(TradeReportRejectReason.Successful);
        validate(TradeReportRejectReason.InvalidPartyInformation);
        validate(TradeReportRejectReason.UnknownInstrument);
        validate(TradeReportRejectReason.UnauthorizedToReportTrades);
        validate(TradeReportRejectReason.InvalidTradeType);
        validate(TradeReportRejectReason.PriceExceedsCurrentPriceBand);
        validate(TradeReportRejectReason.ReferencePriceNotAvailable);
        validate(TradeReportRejectReason.NotionalValueExceedsThreshold);
        validate(TradeReportRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = TradeReportRejectReason;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
