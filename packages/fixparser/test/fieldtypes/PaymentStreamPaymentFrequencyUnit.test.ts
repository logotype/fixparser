import { PaymentStreamPaymentFrequencyUnit } from '../../src/fieldtypes/PaymentStreamPaymentFrequencyUnit';

describe('PaymentStreamPaymentFrequencyUnit', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamPaymentFrequencyUnit.Day).toBe('D');
        expect(PaymentStreamPaymentFrequencyUnit.Week).toBe('Wk');
        expect(PaymentStreamPaymentFrequencyUnit.Month).toBe('Mo');
        expect(PaymentStreamPaymentFrequencyUnit.Year).toBe('Yr');
        expect(PaymentStreamPaymentFrequencyUnit.Term).toBe('T');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamPaymentFrequencyUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamPaymentFrequencyUnit.Day,
            PaymentStreamPaymentFrequencyUnit.Week,
            PaymentStreamPaymentFrequencyUnit.Month,
            PaymentStreamPaymentFrequencyUnit.Year,
            PaymentStreamPaymentFrequencyUnit.Term,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PaymentStreamPaymentFrequencyUnit type', () => {
        const validate = (value: PaymentStreamPaymentFrequencyUnit) => {
            expect(Object.values(PaymentStreamPaymentFrequencyUnit)).toContain(value);
        };

        validate(PaymentStreamPaymentFrequencyUnit.Day);
        validate(PaymentStreamPaymentFrequencyUnit.Week);
        validate(PaymentStreamPaymentFrequencyUnit.Month);
        validate(PaymentStreamPaymentFrequencyUnit.Year);
        validate(PaymentStreamPaymentFrequencyUnit.Term);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamPaymentFrequencyUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
