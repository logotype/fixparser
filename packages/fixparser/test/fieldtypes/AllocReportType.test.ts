import { AllocReportType } from '../../src/fieldtypes/AllocReportType';

describe('AllocReportType', () => {
    test('should have the correct values', () => {
        expect(AllocReportType.PreliminaryRequestToIntermediary).toBe(2);
        expect(AllocReportType.SellsideCalculatedUsingPreliminary).toBe(3);
        expect(AllocReportType.SellsideCalculatedWithoutPreliminary).toBe(4);
        expect(AllocReportType.WarehouseRecap).toBe(5);
        expect(AllocReportType.RequestToIntermediary).toBe(8);
        expect(AllocReportType.Accept).toBe(9);
        expect(AllocReportType.Reject).toBe(10);
        expect(AllocReportType.AcceptPending).toBe(11);
        expect(AllocReportType.Complete).toBe(12);
        expect(AllocReportType.ReversePending).toBe(14);
        expect(AllocReportType.Giveup).toBe(15);
        expect(AllocReportType.Takeup).toBe(16);
        expect(AllocReportType.Reversal).toBe(17);
        expect(AllocReportType.Alleged).toBe(18);
        expect(AllocReportType.SubAllocationGiveup).toBe(19);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocReportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocReportType.PreliminaryRequestToIntermediary,
            AllocReportType.SellsideCalculatedUsingPreliminary,
            AllocReportType.SellsideCalculatedWithoutPreliminary,
            AllocReportType.WarehouseRecap,
            AllocReportType.RequestToIntermediary,
            AllocReportType.Accept,
            AllocReportType.Reject,
            AllocReportType.AcceptPending,
            AllocReportType.Complete,
            AllocReportType.ReversePending,
            AllocReportType.Giveup,
            AllocReportType.Takeup,
            AllocReportType.Reversal,
            AllocReportType.Alleged,
            AllocReportType.SubAllocationGiveup,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocReportType type', () => {
        const validate = (value: AllocReportType) => {
            expect(Object.values(AllocReportType)).toContain(value);
        };

        validate(AllocReportType.PreliminaryRequestToIntermediary);
        validate(AllocReportType.SellsideCalculatedUsingPreliminary);
        validate(AllocReportType.SellsideCalculatedWithoutPreliminary);
        validate(AllocReportType.WarehouseRecap);
        validate(AllocReportType.RequestToIntermediary);
        validate(AllocReportType.Accept);
        validate(AllocReportType.Reject);
        validate(AllocReportType.AcceptPending);
        validate(AllocReportType.Complete);
        validate(AllocReportType.ReversePending);
        validate(AllocReportType.Giveup);
        validate(AllocReportType.Takeup);
        validate(AllocReportType.Reversal);
        validate(AllocReportType.Alleged);
        validate(AllocReportType.SubAllocationGiveup);
    });

    test('should be immutable', () => {
        const ref = AllocReportType;
        expect(() => {
            ref.PreliminaryRequestToIntermediary = 10;
        }).toThrowError();
    });
});
