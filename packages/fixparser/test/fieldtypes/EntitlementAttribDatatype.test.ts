import { EntitlementAttribDatatype } from '../../src/fieldtypes/EntitlementAttribDatatype';

describe('EntitlementAttribDatatype', () => {
    test('should have the correct values', () => {
        expect(EntitlementAttribDatatype.Int).toBe(1);
        expect(EntitlementAttribDatatype.Length).toBe(2);
        expect(EntitlementAttribDatatype.NumInGroup).toBe(3);
        expect(EntitlementAttribDatatype.SeqNum).toBe(4);
        expect(EntitlementAttribDatatype.TagNum).toBe(5);
        expect(EntitlementAttribDatatype.Float).toBe(6);
        expect(EntitlementAttribDatatype.Qty).toBe(7);
        expect(EntitlementAttribDatatype.Price).toBe(8);
        expect(EntitlementAttribDatatype.PriceOffset).toBe(9);
        expect(EntitlementAttribDatatype.Amt).toBe(10);
        expect(EntitlementAttribDatatype.Percentage).toBe(11);
        expect(EntitlementAttribDatatype.Char).toBe(12);
        expect(EntitlementAttribDatatype.Boolean).toBe(13);
        expect(EntitlementAttribDatatype.String).toBe(14);
        expect(EntitlementAttribDatatype.MultipleCharValue).toBe(15);
        expect(EntitlementAttribDatatype.Currency).toBe(16);
        expect(EntitlementAttribDatatype.Exchange).toBe(17);
        expect(EntitlementAttribDatatype.MonthYear).toBe(18);
        expect(EntitlementAttribDatatype.UTCTimestamp).toBe(19);
        expect(EntitlementAttribDatatype.UTCTimeOnly).toBe(20);
        expect(EntitlementAttribDatatype.LocalMktDate).toBe(21);
        expect(EntitlementAttribDatatype.UTCDateOnly).toBe(22);
        expect(EntitlementAttribDatatype.Data).toBe(23);
        expect(EntitlementAttribDatatype.MultipleStringValue).toBe(24);
        expect(EntitlementAttribDatatype.Country).toBe(25);
        expect(EntitlementAttribDatatype.Language).toBe(26);
        expect(EntitlementAttribDatatype.TZTimeOnly).toBe(27);
        expect(EntitlementAttribDatatype.TZTimestamp).toBe(28);
        expect(EntitlementAttribDatatype.Tenor).toBe(29);
        expect(EntitlementAttribDatatype.DayOfMonth).toBe(30);
        expect(EntitlementAttribDatatype.XMLData).toBe(31);
        expect(EntitlementAttribDatatype.Pattern).toBe(32);
        expect(EntitlementAttribDatatype.Reserved100Plus).toBe(33);
        expect(EntitlementAttribDatatype.Reserved1000Plus).toBe(34);
        expect(EntitlementAttribDatatype.Reserved4000Plus).toBe(35);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EntitlementAttribDatatype.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EntitlementAttribDatatype.Int,
            EntitlementAttribDatatype.Length,
            EntitlementAttribDatatype.NumInGroup,
            EntitlementAttribDatatype.SeqNum,
            EntitlementAttribDatatype.TagNum,
            EntitlementAttribDatatype.Float,
            EntitlementAttribDatatype.Qty,
            EntitlementAttribDatatype.Price,
            EntitlementAttribDatatype.PriceOffset,
            EntitlementAttribDatatype.Amt,
            EntitlementAttribDatatype.Percentage,
            EntitlementAttribDatatype.Char,
            EntitlementAttribDatatype.Boolean,
            EntitlementAttribDatatype.String,
            EntitlementAttribDatatype.MultipleCharValue,
            EntitlementAttribDatatype.Currency,
            EntitlementAttribDatatype.Exchange,
            EntitlementAttribDatatype.MonthYear,
            EntitlementAttribDatatype.UTCTimestamp,
            EntitlementAttribDatatype.UTCTimeOnly,
            EntitlementAttribDatatype.LocalMktDate,
            EntitlementAttribDatatype.UTCDateOnly,
            EntitlementAttribDatatype.Data,
            EntitlementAttribDatatype.MultipleStringValue,
            EntitlementAttribDatatype.Country,
            EntitlementAttribDatatype.Language,
            EntitlementAttribDatatype.TZTimeOnly,
            EntitlementAttribDatatype.TZTimestamp,
            EntitlementAttribDatatype.Tenor,
            EntitlementAttribDatatype.DayOfMonth,
            EntitlementAttribDatatype.XMLData,
            EntitlementAttribDatatype.Pattern,
            EntitlementAttribDatatype.Reserved100Plus,
            EntitlementAttribDatatype.Reserved1000Plus,
            EntitlementAttribDatatype.Reserved4000Plus,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EntitlementAttribDatatype type', () => {
        const validate = (value: EntitlementAttribDatatype) => {
            expect(Object.values(EntitlementAttribDatatype)).toContain(value);
        };

        validate(EntitlementAttribDatatype.Int);
        validate(EntitlementAttribDatatype.Length);
        validate(EntitlementAttribDatatype.NumInGroup);
        validate(EntitlementAttribDatatype.SeqNum);
        validate(EntitlementAttribDatatype.TagNum);
        validate(EntitlementAttribDatatype.Float);
        validate(EntitlementAttribDatatype.Qty);
        validate(EntitlementAttribDatatype.Price);
        validate(EntitlementAttribDatatype.PriceOffset);
        validate(EntitlementAttribDatatype.Amt);
        validate(EntitlementAttribDatatype.Percentage);
        validate(EntitlementAttribDatatype.Char);
        validate(EntitlementAttribDatatype.Boolean);
        validate(EntitlementAttribDatatype.String);
        validate(EntitlementAttribDatatype.MultipleCharValue);
        validate(EntitlementAttribDatatype.Currency);
        validate(EntitlementAttribDatatype.Exchange);
        validate(EntitlementAttribDatatype.MonthYear);
        validate(EntitlementAttribDatatype.UTCTimestamp);
        validate(EntitlementAttribDatatype.UTCTimeOnly);
        validate(EntitlementAttribDatatype.LocalMktDate);
        validate(EntitlementAttribDatatype.UTCDateOnly);
        validate(EntitlementAttribDatatype.Data);
        validate(EntitlementAttribDatatype.MultipleStringValue);
        validate(EntitlementAttribDatatype.Country);
        validate(EntitlementAttribDatatype.Language);
        validate(EntitlementAttribDatatype.TZTimeOnly);
        validate(EntitlementAttribDatatype.TZTimestamp);
        validate(EntitlementAttribDatatype.Tenor);
        validate(EntitlementAttribDatatype.DayOfMonth);
        validate(EntitlementAttribDatatype.XMLData);
        validate(EntitlementAttribDatatype.Pattern);
        validate(EntitlementAttribDatatype.Reserved100Plus);
        validate(EntitlementAttribDatatype.Reserved1000Plus);
        validate(EntitlementAttribDatatype.Reserved4000Plus);
    });

    test('should be immutable', () => {
        const ref = EntitlementAttribDatatype;
        expect(() => {
            ref.Int = 10;
        }).toThrowError();
    });
});
