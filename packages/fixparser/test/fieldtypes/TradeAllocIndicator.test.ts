import { TradeAllocIndicator } from '../../src/fieldtypes/TradeAllocIndicator';

describe('TradeAllocIndicator', () => {
    test('should have the correct values', () => {
        expect(TradeAllocIndicator.AllocationNotRequired).toBe(0);
        expect(TradeAllocIndicator.AllocationRequired).toBe(1);
        expect(TradeAllocIndicator.UseAllocationProvidedWithTheTrade).toBe(2);
        expect(TradeAllocIndicator.AllocationGiveUpExecutor).toBe(3);
        expect(TradeAllocIndicator.AllocationFromExecutor).toBe(4);
        expect(TradeAllocIndicator.AllocationToClaimAccount).toBe(5);
        expect(TradeAllocIndicator.TradeSplit).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAllocIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeAllocIndicator.AllocationNotRequired,
            TradeAllocIndicator.AllocationRequired,
            TradeAllocIndicator.UseAllocationProvidedWithTheTrade,
            TradeAllocIndicator.AllocationGiveUpExecutor,
            TradeAllocIndicator.AllocationFromExecutor,
            TradeAllocIndicator.AllocationToClaimAccount,
            TradeAllocIndicator.TradeSplit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAllocIndicator type', () => {
        const validate = (value: TradeAllocIndicator) => {
            expect(Object.values(TradeAllocIndicator)).toContain(value);
        };

        validate(TradeAllocIndicator.AllocationNotRequired);
        validate(TradeAllocIndicator.AllocationRequired);
        validate(TradeAllocIndicator.UseAllocationProvidedWithTheTrade);
        validate(TradeAllocIndicator.AllocationGiveUpExecutor);
        validate(TradeAllocIndicator.AllocationFromExecutor);
        validate(TradeAllocIndicator.AllocationToClaimAccount);
        validate(TradeAllocIndicator.TradeSplit);
    });

    test('should be immutable', () => {
        const ref = TradeAllocIndicator;
        expect(() => {
            ref.AllocationNotRequired = 10;
        }).toThrowError();
    });
});
