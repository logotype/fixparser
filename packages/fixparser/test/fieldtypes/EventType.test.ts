import { EventType } from '../../src/fieldtypes/EventType';

describe('EventType', () => {
    test('should have the correct values', () => {
        expect(EventType.Put).toBe(1);
        expect(EventType.Call).toBe(2);
        expect(EventType.Tender).toBe(3);
        expect(EventType.SinkingFundCall).toBe(4);
        expect(EventType.Activation).toBe(5);
        expect(EventType.Inactiviation).toBe(6);
        expect(EventType.LastEligibleTradeDate).toBe(7);
        expect(EventType.SwapStartDate).toBe(8);
        expect(EventType.SwapEndDate).toBe(9);
        expect(EventType.SwapRollDate).toBe(10);
        expect(EventType.SwapNextStartDate).toBe(11);
        expect(EventType.SwapNextRollDate).toBe(12);
        expect(EventType.FirstDeliveryDate).toBe(13);
        expect(EventType.LastDeliveryDate).toBe(14);
        expect(EventType.InitialInventoryDueDate).toBe(15);
        expect(EventType.FinalInventoryDueDate).toBe(16);
        expect(EventType.FirstIntentDate).toBe(17);
        expect(EventType.LastIntentDate).toBe(18);
        expect(EventType.PositionRemovalDate).toBe(19);
        expect(EventType.MinimumNotice).toBe(20);
        expect(EventType.DeliveryStartTime).toBe(21);
        expect(EventType.DeliveryEndTime).toBe(22);
        expect(EventType.FirstNoticeDate).toBe(23);
        expect(EventType.LastNoticeDate).toBe(24);
        expect(EventType.FirstExerciseDate).toBe(25);
        expect(EventType.RedemptionDate).toBe(26);
        expect(EventType.TrdCntntnEfctvDt).toBe(27);
        expect(EventType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EventType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EventType.Put,
            EventType.Call,
            EventType.Tender,
            EventType.SinkingFundCall,
            EventType.Activation,
            EventType.Inactiviation,
            EventType.LastEligibleTradeDate,
            EventType.SwapStartDate,
            EventType.SwapEndDate,
            EventType.SwapRollDate,
            EventType.SwapNextStartDate,
            EventType.SwapNextRollDate,
            EventType.FirstDeliveryDate,
            EventType.LastDeliveryDate,
            EventType.InitialInventoryDueDate,
            EventType.FinalInventoryDueDate,
            EventType.FirstIntentDate,
            EventType.LastIntentDate,
            EventType.PositionRemovalDate,
            EventType.MinimumNotice,
            EventType.DeliveryStartTime,
            EventType.DeliveryEndTime,
            EventType.FirstNoticeDate,
            EventType.LastNoticeDate,
            EventType.FirstExerciseDate,
            EventType.RedemptionDate,
            EventType.TrdCntntnEfctvDt,
            EventType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EventType type', () => {
        const validate = (value: EventType) => {
            expect(Object.values(EventType)).toContain(value);
        };

        validate(EventType.Put);
        validate(EventType.Call);
        validate(EventType.Tender);
        validate(EventType.SinkingFundCall);
        validate(EventType.Activation);
        validate(EventType.Inactiviation);
        validate(EventType.LastEligibleTradeDate);
        validate(EventType.SwapStartDate);
        validate(EventType.SwapEndDate);
        validate(EventType.SwapRollDate);
        validate(EventType.SwapNextStartDate);
        validate(EventType.SwapNextRollDate);
        validate(EventType.FirstDeliveryDate);
        validate(EventType.LastDeliveryDate);
        validate(EventType.InitialInventoryDueDate);
        validate(EventType.FinalInventoryDueDate);
        validate(EventType.FirstIntentDate);
        validate(EventType.LastIntentDate);
        validate(EventType.PositionRemovalDate);
        validate(EventType.MinimumNotice);
        validate(EventType.DeliveryStartTime);
        validate(EventType.DeliveryEndTime);
        validate(EventType.FirstNoticeDate);
        validate(EventType.LastNoticeDate);
        validate(EventType.FirstExerciseDate);
        validate(EventType.RedemptionDate);
        validate(EventType.TrdCntntnEfctvDt);
        validate(EventType.Other);
    });

    test('should be immutable', () => {
        const ref = EventType;
        expect(() => {
            ref.Put = 10;
        }).toThrowError();
    });
});
