import { ThrottleAction } from '../../src/fieldtypes/ThrottleAction';

describe('ThrottleAction', () => {
    test('should have the correct values', () => {
        expect(ThrottleAction.QueueInbound).toBe(0);
        expect(ThrottleAction.QueueOutbound).toBe(1);
        expect(ThrottleAction.Reject).toBe(2);
        expect(ThrottleAction.Disconnect).toBe(3);
        expect(ThrottleAction.Warning).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ThrottleAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ThrottleAction.QueueInbound,
            ThrottleAction.QueueOutbound,
            ThrottleAction.Reject,
            ThrottleAction.Disconnect,
            ThrottleAction.Warning,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ThrottleAction type', () => {
        const validate = (value: ThrottleAction) => {
            expect(Object.values(ThrottleAction)).toContain(value);
        };

        validate(ThrottleAction.QueueInbound);
        validate(ThrottleAction.QueueOutbound);
        validate(ThrottleAction.Reject);
        validate(ThrottleAction.Disconnect);
        validate(ThrottleAction.Warning);
    });

    test('should be immutable', () => {
        const ref = ThrottleAction;
        expect(() => {
            ref.QueueInbound = 10;
        }).toThrowError();
    });
});
