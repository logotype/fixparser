import { UnderlyingPriceDeterminationMethod } from '../../src/fieldtypes/UnderlyingPriceDeterminationMethod';

describe('UnderlyingPriceDeterminationMethod', () => {
    test('should have the correct values', () => {
        expect(UnderlyingPriceDeterminationMethod.Regular).toBe(1);
        expect(UnderlyingPriceDeterminationMethod.SpecialReference).toBe(2);
        expect(UnderlyingPriceDeterminationMethod.OptimalValue).toBe(3);
        expect(UnderlyingPriceDeterminationMethod.AverageValue).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingPriceDeterminationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnderlyingPriceDeterminationMethod.Regular,
            UnderlyingPriceDeterminationMethod.SpecialReference,
            UnderlyingPriceDeterminationMethod.OptimalValue,
            UnderlyingPriceDeterminationMethod.AverageValue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UnderlyingPriceDeterminationMethod type', () => {
        const validate = (value: UnderlyingPriceDeterminationMethod) => {
            expect(Object.values(UnderlyingPriceDeterminationMethod)).toContain(value);
        };

        validate(UnderlyingPriceDeterminationMethod.Regular);
        validate(UnderlyingPriceDeterminationMethod.SpecialReference);
        validate(UnderlyingPriceDeterminationMethod.OptimalValue);
        validate(UnderlyingPriceDeterminationMethod.AverageValue);
    });

    test('should be immutable', () => {
        const ref = UnderlyingPriceDeterminationMethod;
        expect(() => {
            ref.Regular = 10;
        }).toThrowError();
    });
});
