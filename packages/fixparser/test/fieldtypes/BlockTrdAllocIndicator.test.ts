import { BlockTrdAllocIndicator } from '../../src/fieldtypes/BlockTrdAllocIndicator';

describe('BlockTrdAllocIndicator', () => {
    test('should have the correct values', () => {
        expect(BlockTrdAllocIndicator.BlockToBeAllocated).toBe(0);
        expect(BlockTrdAllocIndicator.BlockNotToBeAllocated).toBe(1);
        expect(BlockTrdAllocIndicator.AllocatedTrade).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BlockTrdAllocIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BlockTrdAllocIndicator.BlockToBeAllocated,
            BlockTrdAllocIndicator.BlockNotToBeAllocated,
            BlockTrdAllocIndicator.AllocatedTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BlockTrdAllocIndicator type', () => {
        const validate = (value: BlockTrdAllocIndicator) => {
            expect(Object.values(BlockTrdAllocIndicator)).toContain(value);
        };

        validate(BlockTrdAllocIndicator.BlockToBeAllocated);
        validate(BlockTrdAllocIndicator.BlockNotToBeAllocated);
        validate(BlockTrdAllocIndicator.AllocatedTrade);
    });

    test('should be immutable', () => {
        const ref = BlockTrdAllocIndicator;
        expect(() => {
            ref.BlockToBeAllocated = 10;
        }).toThrowError();
    });
});
