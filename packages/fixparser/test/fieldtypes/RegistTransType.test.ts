import { RegistTransType } from '../../src/fieldtypes/RegistTransType';

describe('RegistTransType', () => {
    test('should have the correct values', () => {
        expect(RegistTransType.New).toBe('0');
        expect(RegistTransType.Cancel).toBe('2');
        expect(RegistTransType.Replace).toBe('1');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegistTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RegistTransType.New, RegistTransType.Cancel, RegistTransType.Replace];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for RegistTransType type', () => {
        const validate = (value: RegistTransType) => {
            expect(Object.values(RegistTransType)).toContain(value);
        };

        validate(RegistTransType.New);
        validate(RegistTransType.Cancel);
        validate(RegistTransType.Replace);
    });

    test('should be immutable', () => {
        const ref = RegistTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
