import { RateSourceType } from '../../src/fieldtypes/RateSourceType';

describe('RateSourceType', () => {
    test('should have the correct values', () => {
        expect(RateSourceType.Primary).toBe(0);
        expect(RateSourceType.Secondary).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RateSourceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RateSourceType.Primary, RateSourceType.Secondary];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RateSourceType type', () => {
        const validate = (value: RateSourceType) => {
            expect(Object.values(RateSourceType)).toContain(value);
        };

        validate(RateSourceType.Primary);
        validate(RateSourceType.Secondary);
    });

    test('should be immutable', () => {
        const ref = RateSourceType;
        expect(() => {
            ref.Primary = 10;
        }).toThrowError();
    });
});
