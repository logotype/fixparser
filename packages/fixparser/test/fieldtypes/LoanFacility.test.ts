import { LoanFacility } from '../../src/fieldtypes/LoanFacility';

describe('LoanFacility', () => {
    test('should have the correct values', () => {
        expect(LoanFacility.BridgeLoan).toBe(0);
        expect(LoanFacility.LetterOfCredit).toBe(1);
        expect(LoanFacility.RevolvingLoan).toBe(2);
        expect(LoanFacility.SwinglineFunding).toBe(3);
        expect(LoanFacility.TermLoan).toBe(4);
        expect(LoanFacility.TradeClaim).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LoanFacility.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LoanFacility.BridgeLoan,
            LoanFacility.LetterOfCredit,
            LoanFacility.RevolvingLoan,
            LoanFacility.SwinglineFunding,
            LoanFacility.TermLoan,
            LoanFacility.TradeClaim,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LoanFacility type', () => {
        const validate = (value: LoanFacility) => {
            expect(Object.values(LoanFacility)).toContain(value);
        };

        validate(LoanFacility.BridgeLoan);
        validate(LoanFacility.LetterOfCredit);
        validate(LoanFacility.RevolvingLoan);
        validate(LoanFacility.SwinglineFunding);
        validate(LoanFacility.TermLoan);
        validate(LoanFacility.TradeClaim);
    });

    test('should be immutable', () => {
        const ref = LoanFacility;
        expect(() => {
            ref.BridgeLoan = 10;
        }).toThrowError();
    });
});
