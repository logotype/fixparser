import { TrdRegTimestampManualIndicator } from '../../src/fieldtypes/TrdRegTimestampManualIndicator';

describe('TrdRegTimestampManualIndicator', () => {
    test('should have the correct values', () => {
        expect(TrdRegTimestampManualIndicator.NotManuallyCaptured).toBe('N');
        expect(TrdRegTimestampManualIndicator.ManuallyCaptured).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdRegTimestampManualIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdRegTimestampManualIndicator.NotManuallyCaptured,
            TrdRegTimestampManualIndicator.ManuallyCaptured,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TrdRegTimestampManualIndicator type', () => {
        const validate = (value: TrdRegTimestampManualIndicator) => {
            expect(Object.values(TrdRegTimestampManualIndicator)).toContain(value);
        };

        validate(TrdRegTimestampManualIndicator.NotManuallyCaptured);
        validate(TrdRegTimestampManualIndicator.ManuallyCaptured);
    });

    test('should be immutable', () => {
        const ref = TrdRegTimestampManualIndicator;
        expect(() => {
            ref.NotManuallyCaptured = 10;
        }).toThrowError();
    });
});
