import { ThrottleType } from '../../src/fieldtypes/ThrottleType';

describe('ThrottleType', () => {
    test('should have the correct values', () => {
        expect(ThrottleType.InboundRate).toBe(0);
        expect(ThrottleType.OutstandingRequests).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ThrottleType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ThrottleType.InboundRate, ThrottleType.OutstandingRequests];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ThrottleType type', () => {
        const validate = (value: ThrottleType) => {
            expect(Object.values(ThrottleType)).toContain(value);
        };

        validate(ThrottleType.InboundRate);
        validate(ThrottleType.OutstandingRequests);
    });

    test('should be immutable', () => {
        const ref = ThrottleType;
        expect(() => {
            ref.InboundRate = 10;
        }).toThrowError();
    });
});
