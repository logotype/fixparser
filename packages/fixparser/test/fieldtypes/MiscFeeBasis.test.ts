import { MiscFeeBasis } from '../../src/fieldtypes/MiscFeeBasis';

describe('MiscFeeBasis', () => {
    test('should have the correct values', () => {
        expect(MiscFeeBasis.Absolute).toBe(0);
        expect(MiscFeeBasis.PerUnit).toBe(1);
        expect(MiscFeeBasis.Percentage).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MiscFeeBasis.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MiscFeeBasis.Absolute, MiscFeeBasis.PerUnit, MiscFeeBasis.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MiscFeeBasis type', () => {
        const validate = (value: MiscFeeBasis) => {
            expect(Object.values(MiscFeeBasis)).toContain(value);
        };

        validate(MiscFeeBasis.Absolute);
        validate(MiscFeeBasis.PerUnit);
        validate(MiscFeeBasis.Percentage);
    });

    test('should be immutable', () => {
        const ref = MiscFeeBasis;
        expect(() => {
            ref.Absolute = 10;
        }).toThrowError();
    });
});
