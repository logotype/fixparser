import { CollApplType } from '../../src/fieldtypes/CollApplType';

describe('CollApplType', () => {
    test('should have the correct values', () => {
        expect(CollApplType.SpecificDeposit).toBe(0);
        expect(CollApplType.General).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollApplType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CollApplType.SpecificDeposit, CollApplType.General];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollApplType type', () => {
        const validate = (value: CollApplType) => {
            expect(Object.values(CollApplType)).toContain(value);
        };

        validate(CollApplType.SpecificDeposit);
        validate(CollApplType.General);
    });

    test('should be immutable', () => {
        const ref = CollApplType;
        expect(() => {
            ref.SpecificDeposit = 10;
        }).toThrowError();
    });
});
