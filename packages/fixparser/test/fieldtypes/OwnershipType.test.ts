import { OwnershipType } from '../../src/fieldtypes/OwnershipType';

describe('OwnershipType', () => {
    test('should have the correct values', () => {
        expect(OwnershipType.JointInvestors).toBe('J');
        expect(OwnershipType.TenantsInCommon).toBe('T');
        expect(OwnershipType.JointTrustees).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OwnershipType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OwnershipType.JointInvestors,
            OwnershipType.TenantsInCommon,
            OwnershipType.JointTrustees,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OwnershipType type', () => {
        const validate = (value: OwnershipType) => {
            expect(Object.values(OwnershipType)).toContain(value);
        };

        validate(OwnershipType.JointInvestors);
        validate(OwnershipType.TenantsInCommon);
        validate(OwnershipType.JointTrustees);
    });

    test('should be immutable', () => {
        const ref = OwnershipType;
        expect(() => {
            ref.JointInvestors = 10;
        }).toThrowError();
    });
});
