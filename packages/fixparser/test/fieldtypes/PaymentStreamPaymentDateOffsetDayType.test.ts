import { PaymentStreamPaymentDateOffsetDayType } from '../../src/fieldtypes/PaymentStreamPaymentDateOffsetDayType';

describe('PaymentStreamPaymentDateOffsetDayType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamPaymentDateOffsetDayType.Business).toBe(0);
        expect(PaymentStreamPaymentDateOffsetDayType.Calendar).toBe(1);
        expect(PaymentStreamPaymentDateOffsetDayType.CommodityBusiness).toBe(2);
        expect(PaymentStreamPaymentDateOffsetDayType.CurrencyBusiness).toBe(3);
        expect(PaymentStreamPaymentDateOffsetDayType.ExchangeBusiness).toBe(4);
        expect(PaymentStreamPaymentDateOffsetDayType.ScheduledTradingDay).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamPaymentDateOffsetDayType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamPaymentDateOffsetDayType.Business,
            PaymentStreamPaymentDateOffsetDayType.Calendar,
            PaymentStreamPaymentDateOffsetDayType.CommodityBusiness,
            PaymentStreamPaymentDateOffsetDayType.CurrencyBusiness,
            PaymentStreamPaymentDateOffsetDayType.ExchangeBusiness,
            PaymentStreamPaymentDateOffsetDayType.ScheduledTradingDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamPaymentDateOffsetDayType type', () => {
        const validate = (value: PaymentStreamPaymentDateOffsetDayType) => {
            expect(Object.values(PaymentStreamPaymentDateOffsetDayType)).toContain(value);
        };

        validate(PaymentStreamPaymentDateOffsetDayType.Business);
        validate(PaymentStreamPaymentDateOffsetDayType.Calendar);
        validate(PaymentStreamPaymentDateOffsetDayType.CommodityBusiness);
        validate(PaymentStreamPaymentDateOffsetDayType.CurrencyBusiness);
        validate(PaymentStreamPaymentDateOffsetDayType.ExchangeBusiness);
        validate(PaymentStreamPaymentDateOffsetDayType.ScheduledTradingDay);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamPaymentDateOffsetDayType;
        expect(() => {
            ref.Business = 10;
        }).toThrowError();
    });
});
