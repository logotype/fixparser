import { ReleaseInstruction } from '../../src/fieldtypes/ReleaseInstruction';

describe('ReleaseInstruction', () => {
    test('should have the correct values', () => {
        expect(ReleaseInstruction.ISO).toBe(1);
        expect(ReleaseInstruction.NoAwayMarketBetterCheck).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReleaseInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ReleaseInstruction.ISO, ReleaseInstruction.NoAwayMarketBetterCheck];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReleaseInstruction type', () => {
        const validate = (value: ReleaseInstruction) => {
            expect(Object.values(ReleaseInstruction)).toContain(value);
        };

        validate(ReleaseInstruction.ISO);
        validate(ReleaseInstruction.NoAwayMarketBetterCheck);
    });

    test('should be immutable', () => {
        const ref = ReleaseInstruction;
        expect(() => {
            ref.ISO = 10;
        }).toThrowError();
    });
});
