import { AllocHandlInst } from '../../src/fieldtypes/AllocHandlInst';

describe('AllocHandlInst', () => {
    test('should have the correct values', () => {
        expect(AllocHandlInst.Match).toBe(1);
        expect(AllocHandlInst.Forward).toBe(2);
        expect(AllocHandlInst.ForwardAndMatch).toBe(3);
        expect(AllocHandlInst.AutoClaimGiveUp).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocHandlInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocHandlInst.Match,
            AllocHandlInst.Forward,
            AllocHandlInst.ForwardAndMatch,
            AllocHandlInst.AutoClaimGiveUp,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocHandlInst type', () => {
        const validate = (value: AllocHandlInst) => {
            expect(Object.values(AllocHandlInst)).toContain(value);
        };

        validate(AllocHandlInst.Match);
        validate(AllocHandlInst.Forward);
        validate(AllocHandlInst.ForwardAndMatch);
        validate(AllocHandlInst.AutoClaimGiveUp);
    });

    test('should be immutable', () => {
        const ref = AllocHandlInst;
        expect(() => {
            ref.Match = 10;
        }).toThrowError();
    });
});
