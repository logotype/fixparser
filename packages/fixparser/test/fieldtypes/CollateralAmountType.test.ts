import { CollateralAmountType } from '../../src/fieldtypes/CollateralAmountType';

describe('CollateralAmountType', () => {
    test('should have the correct values', () => {
        expect(CollateralAmountType.MarketValuation).toBe(0);
        expect(CollateralAmountType.PortfolioValue).toBe(1);
        expect(CollateralAmountType.ValueConfirmed).toBe(2);
        expect(CollateralAmountType.CollateralCreditValue).toBe(3);
        expect(CollateralAmountType.AdditionalCollateralValue).toBe(4);
        expect(CollateralAmountType.EstimatedMarketValuation).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollateralAmountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollateralAmountType.MarketValuation,
            CollateralAmountType.PortfolioValue,
            CollateralAmountType.ValueConfirmed,
            CollateralAmountType.CollateralCreditValue,
            CollateralAmountType.AdditionalCollateralValue,
            CollateralAmountType.EstimatedMarketValuation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollateralAmountType type', () => {
        const validate = (value: CollateralAmountType) => {
            expect(Object.values(CollateralAmountType)).toContain(value);
        };

        validate(CollateralAmountType.MarketValuation);
        validate(CollateralAmountType.PortfolioValue);
        validate(CollateralAmountType.ValueConfirmed);
        validate(CollateralAmountType.CollateralCreditValue);
        validate(CollateralAmountType.AdditionalCollateralValue);
        validate(CollateralAmountType.EstimatedMarketValuation);
    });

    test('should be immutable', () => {
        const ref = CollateralAmountType;
        expect(() => {
            ref.MarketValuation = 10;
        }).toThrowError();
    });
});
