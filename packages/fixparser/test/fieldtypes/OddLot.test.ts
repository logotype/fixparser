import { OddLot } from '../../src/fieldtypes/OddLot';

describe('OddLot', () => {
    test('should have the correct values', () => {
        expect(OddLot.TreatAsRoundLot).toBe('N');
        expect(OddLot.TreatAsOddLot).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OddLot.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OddLot.TreatAsRoundLot, OddLot.TreatAsOddLot];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OddLot type', () => {
        const validate = (value: OddLot) => {
            expect(Object.values(OddLot)).toContain(value);
        };

        validate(OddLot.TreatAsRoundLot);
        validate(OddLot.TreatAsOddLot);
    });

    test('should be immutable', () => {
        const ref = OddLot;
        expect(() => {
            ref.TreatAsRoundLot = 10;
        }).toThrowError();
    });
});
