import { TradeHandlingInstr } from '../../src/fieldtypes/TradeHandlingInstr';

describe('TradeHandlingInstr', () => {
    test('should have the correct values', () => {
        expect(TradeHandlingInstr.TradeConfirmation).toBe('0');
        expect(TradeHandlingInstr.TwoPartyReport).toBe('1');
        expect(TradeHandlingInstr.OnePartyReportForMatching).toBe('2');
        expect(TradeHandlingInstr.OnePartyReportForPassThrough).toBe('3');
        expect(TradeHandlingInstr.AutomatedFloorOrderRouting).toBe('4');
        expect(TradeHandlingInstr.TwoPartyReportForClaim).toBe('5');
        expect(TradeHandlingInstr.OnePartyReport).toBe('6');
        expect(TradeHandlingInstr.ThirdPtyRptForPassThrough).toBe('7');
        expect(TradeHandlingInstr.OnePartyReportAutoMatch).toBe('8');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeHandlingInstr.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeHandlingInstr.TradeConfirmation,
            TradeHandlingInstr.TwoPartyReport,
            TradeHandlingInstr.OnePartyReportForMatching,
            TradeHandlingInstr.OnePartyReportForPassThrough,
            TradeHandlingInstr.AutomatedFloorOrderRouting,
            TradeHandlingInstr.TwoPartyReportForClaim,
            TradeHandlingInstr.OnePartyReport,
            TradeHandlingInstr.ThirdPtyRptForPassThrough,
            TradeHandlingInstr.OnePartyReportAutoMatch,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TradeHandlingInstr type', () => {
        const validate = (value: TradeHandlingInstr) => {
            expect(Object.values(TradeHandlingInstr)).toContain(value);
        };

        validate(TradeHandlingInstr.TradeConfirmation);
        validate(TradeHandlingInstr.TwoPartyReport);
        validate(TradeHandlingInstr.OnePartyReportForMatching);
        validate(TradeHandlingInstr.OnePartyReportForPassThrough);
        validate(TradeHandlingInstr.AutomatedFloorOrderRouting);
        validate(TradeHandlingInstr.TwoPartyReportForClaim);
        validate(TradeHandlingInstr.OnePartyReport);
        validate(TradeHandlingInstr.ThirdPtyRptForPassThrough);
        validate(TradeHandlingInstr.OnePartyReportAutoMatch);
    });

    test('should be immutable', () => {
        const ref = TradeHandlingInstr;
        expect(() => {
            ref.TradeConfirmation = 10;
        }).toThrowError();
    });
});
