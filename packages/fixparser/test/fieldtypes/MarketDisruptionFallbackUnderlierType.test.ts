import { MarketDisruptionFallbackUnderlierType } from '../../src/fieldtypes/MarketDisruptionFallbackUnderlierType';

describe('MarketDisruptionFallbackUnderlierType', () => {
    test('should have the correct values', () => {
        expect(MarketDisruptionFallbackUnderlierType.Basket).toBe(0);
        expect(MarketDisruptionFallbackUnderlierType.Bond).toBe(1);
        expect(MarketDisruptionFallbackUnderlierType.Cash).toBe(2);
        expect(MarketDisruptionFallbackUnderlierType.Commodity).toBe(3);
        expect(MarketDisruptionFallbackUnderlierType.ConvertibleBond).toBe(4);
        expect(MarketDisruptionFallbackUnderlierType.Equity).toBe(5);
        expect(MarketDisruptionFallbackUnderlierType.ExchangeTradedFund).toBe(6);
        expect(MarketDisruptionFallbackUnderlierType.Future).toBe(7);
        expect(MarketDisruptionFallbackUnderlierType.Index).toBe(8);
        expect(MarketDisruptionFallbackUnderlierType.Loan).toBe(9);
        expect(MarketDisruptionFallbackUnderlierType.Mortgage).toBe(10);
        expect(MarketDisruptionFallbackUnderlierType.MutualFund).toBe(11);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketDisruptionFallbackUnderlierType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketDisruptionFallbackUnderlierType.Basket,
            MarketDisruptionFallbackUnderlierType.Bond,
            MarketDisruptionFallbackUnderlierType.Cash,
            MarketDisruptionFallbackUnderlierType.Commodity,
            MarketDisruptionFallbackUnderlierType.ConvertibleBond,
            MarketDisruptionFallbackUnderlierType.Equity,
            MarketDisruptionFallbackUnderlierType.ExchangeTradedFund,
            MarketDisruptionFallbackUnderlierType.Future,
            MarketDisruptionFallbackUnderlierType.Index,
            MarketDisruptionFallbackUnderlierType.Loan,
            MarketDisruptionFallbackUnderlierType.Mortgage,
            MarketDisruptionFallbackUnderlierType.MutualFund,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketDisruptionFallbackUnderlierType type', () => {
        const validate = (value: MarketDisruptionFallbackUnderlierType) => {
            expect(Object.values(MarketDisruptionFallbackUnderlierType)).toContain(value);
        };

        validate(MarketDisruptionFallbackUnderlierType.Basket);
        validate(MarketDisruptionFallbackUnderlierType.Bond);
        validate(MarketDisruptionFallbackUnderlierType.Cash);
        validate(MarketDisruptionFallbackUnderlierType.Commodity);
        validate(MarketDisruptionFallbackUnderlierType.ConvertibleBond);
        validate(MarketDisruptionFallbackUnderlierType.Equity);
        validate(MarketDisruptionFallbackUnderlierType.ExchangeTradedFund);
        validate(MarketDisruptionFallbackUnderlierType.Future);
        validate(MarketDisruptionFallbackUnderlierType.Index);
        validate(MarketDisruptionFallbackUnderlierType.Loan);
        validate(MarketDisruptionFallbackUnderlierType.Mortgage);
        validate(MarketDisruptionFallbackUnderlierType.MutualFund);
    });

    test('should be immutable', () => {
        const ref = MarketDisruptionFallbackUnderlierType;
        expect(() => {
            ref.Basket = 10;
        }).toThrowError();
    });
});
