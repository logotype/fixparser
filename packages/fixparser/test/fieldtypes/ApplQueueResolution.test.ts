import { ApplQueueResolution } from '../../src/fieldtypes/ApplQueueResolution';

describe('ApplQueueResolution', () => {
    test('should have the correct values', () => {
        expect(ApplQueueResolution.NoActionTaken).toBe(0);
        expect(ApplQueueResolution.QueueFlushed).toBe(1);
        expect(ApplQueueResolution.OverlayLast).toBe(2);
        expect(ApplQueueResolution.EndSession).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplQueueResolution.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplQueueResolution.NoActionTaken,
            ApplQueueResolution.QueueFlushed,
            ApplQueueResolution.OverlayLast,
            ApplQueueResolution.EndSession,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplQueueResolution type', () => {
        const validate = (value: ApplQueueResolution) => {
            expect(Object.values(ApplQueueResolution)).toContain(value);
        };

        validate(ApplQueueResolution.NoActionTaken);
        validate(ApplQueueResolution.QueueFlushed);
        validate(ApplQueueResolution.OverlayLast);
        validate(ApplQueueResolution.EndSession);
    });

    test('should be immutable', () => {
        const ref = ApplQueueResolution;
        expect(() => {
            ref.NoActionTaken = 10;
        }).toThrowError();
    });
});
