import { PaymentStreamRateSpreadPositionType } from '../../src/fieldtypes/PaymentStreamRateSpreadPositionType';

describe('PaymentStreamRateSpreadPositionType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRateSpreadPositionType.Short).toBe(0);
        expect(PaymentStreamRateSpreadPositionType.Long).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRateSpreadPositionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamRateSpreadPositionType.Short, PaymentStreamRateSpreadPositionType.Long];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamRateSpreadPositionType type', () => {
        const validate = (value: PaymentStreamRateSpreadPositionType) => {
            expect(Object.values(PaymentStreamRateSpreadPositionType)).toContain(value);
        };

        validate(PaymentStreamRateSpreadPositionType.Short);
        validate(PaymentStreamRateSpreadPositionType.Long);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRateSpreadPositionType;
        expect(() => {
            ref.Short = 10;
        }).toThrowError();
    });
});
