import { AllocGroupSubQtyType } from '../../src/fieldtypes/AllocGroupSubQtyType';

describe('AllocGroupSubQtyType', () => {
    test('should have the correct values', () => {
        expect(AllocGroupSubQtyType.TradeType).toBe(1);
        expect(AllocGroupSubQtyType.TradePublicationIndicator).toBe(2);
        expect(AllocGroupSubQtyType.OrderHandlingInstruction).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocGroupSubQtyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocGroupSubQtyType.TradeType,
            AllocGroupSubQtyType.TradePublicationIndicator,
            AllocGroupSubQtyType.OrderHandlingInstruction,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocGroupSubQtyType type', () => {
        const validate = (value: AllocGroupSubQtyType) => {
            expect(Object.values(AllocGroupSubQtyType)).toContain(value);
        };

        validate(AllocGroupSubQtyType.TradeType);
        validate(AllocGroupSubQtyType.TradePublicationIndicator);
        validate(AllocGroupSubQtyType.OrderHandlingInstruction);
    });

    test('should be immutable', () => {
        const ref = AllocGroupSubQtyType;
        expect(() => {
            ref.TradeType = 10;
        }).toThrowError();
    });
});
