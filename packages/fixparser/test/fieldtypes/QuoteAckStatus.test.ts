import { QuoteAckStatus } from '../../src/fieldtypes/QuoteAckStatus';

describe('QuoteAckStatus', () => {
    test('should have the correct values', () => {
        expect(QuoteAckStatus.ReceivedNotYetProcessed).toBe(0);
        expect(QuoteAckStatus.Accepted).toBe(1);
        expect(QuoteAckStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteAckStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteAckStatus.ReceivedNotYetProcessed,
            QuoteAckStatus.Accepted,
            QuoteAckStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteAckStatus type', () => {
        const validate = (value: QuoteAckStatus) => {
            expect(Object.values(QuoteAckStatus)).toContain(value);
        };

        validate(QuoteAckStatus.ReceivedNotYetProcessed);
        validate(QuoteAckStatus.Accepted);
        validate(QuoteAckStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = QuoteAckStatus;
        expect(() => {
            ref.ReceivedNotYetProcessed = 10;
        }).toThrowError();
    });
});
