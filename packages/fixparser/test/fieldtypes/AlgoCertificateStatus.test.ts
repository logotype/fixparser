import { AlgoCertificateStatus } from '../../src/fieldtypes/AlgoCertificateStatus';

describe('AlgoCertificateStatus', () => {
    test('should have the correct values', () => {
        expect(AlgoCertificateStatus.Draft).toBe(0);
        expect(AlgoCertificateStatus.Approved).toBe(1);
        expect(AlgoCertificateStatus.Submitted).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgoCertificateStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AlgoCertificateStatus.Draft,
            AlgoCertificateStatus.Approved,
            AlgoCertificateStatus.Submitted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgoCertificateStatus type', () => {
        const validate = (value: AlgoCertificateStatus) => {
            expect(Object.values(AlgoCertificateStatus)).toContain(value);
        };

        validate(AlgoCertificateStatus.Draft);
        validate(AlgoCertificateStatus.Approved);
        validate(AlgoCertificateStatus.Submitted);
    });

    test('should be immutable', () => {
        const ref = AlgoCertificateStatus;
        expect(() => {
            ref.Draft = 10;
        }).toThrowError();
    });
});
