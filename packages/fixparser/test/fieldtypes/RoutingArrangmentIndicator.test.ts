import { RoutingArrangmentIndicator } from '../../src/fieldtypes/RoutingArrangmentIndicator';

describe('RoutingArrangmentIndicator', () => {
    test('should have the correct values', () => {
        expect(RoutingArrangmentIndicator.NoRoutingArrangmentInPlace).toBe(0);
        expect(RoutingArrangmentIndicator.RoutingArrangementInPlace).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RoutingArrangmentIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RoutingArrangmentIndicator.NoRoutingArrangmentInPlace,
            RoutingArrangmentIndicator.RoutingArrangementInPlace,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RoutingArrangmentIndicator type', () => {
        const validate = (value: RoutingArrangmentIndicator) => {
            expect(Object.values(RoutingArrangmentIndicator)).toContain(value);
        };

        validate(RoutingArrangmentIndicator.NoRoutingArrangmentInPlace);
        validate(RoutingArrangmentIndicator.RoutingArrangementInPlace);
    });

    test('should be immutable', () => {
        const ref = RoutingArrangmentIndicator;
        expect(() => {
            ref.NoRoutingArrangmentInPlace = 10;
        }).toThrowError();
    });
});
