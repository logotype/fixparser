import { RoutingType } from '../../src/fieldtypes/RoutingType';

describe('RoutingType', () => {
    test('should have the correct values', () => {
        expect(RoutingType.TargetFirm).toBe(1);
        expect(RoutingType.TargetList).toBe(2);
        expect(RoutingType.BlockFirm).toBe(3);
        expect(RoutingType.BlockList).toBe(4);
        expect(RoutingType.TargetPerson).toBe(5);
        expect(RoutingType.BlockPerson).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RoutingType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RoutingType.TargetFirm,
            RoutingType.TargetList,
            RoutingType.BlockFirm,
            RoutingType.BlockList,
            RoutingType.TargetPerson,
            RoutingType.BlockPerson,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RoutingType type', () => {
        const validate = (value: RoutingType) => {
            expect(Object.values(RoutingType)).toContain(value);
        };

        validate(RoutingType.TargetFirm);
        validate(RoutingType.TargetList);
        validate(RoutingType.BlockFirm);
        validate(RoutingType.BlockList);
        validate(RoutingType.TargetPerson);
        validate(RoutingType.BlockPerson);
    });

    test('should be immutable', () => {
        const ref = RoutingType;
        expect(() => {
            ref.TargetFirm = 10;
        }).toThrowError();
    });
});
