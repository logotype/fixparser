import { TradeReportingIndicator } from '../../src/fieldtypes/TradeReportingIndicator';

describe('TradeReportingIndicator', () => {
    test('should have the correct values', () => {
        expect(TradeReportingIndicator.NotReported).toBe(0);
        expect(TradeReportingIndicator.OnBook).toBe(1);
        expect(TradeReportingIndicator.SISeller).toBe(2);
        expect(TradeReportingIndicator.SIBuyer).toBe(3);
        expect(TradeReportingIndicator.NonSISeller).toBe(4);
        expect(TradeReportingIndicator.SubDelegationByFirm).toBe(5);
        expect(TradeReportingIndicator.Reportable).toBe(6);
        expect(TradeReportingIndicator.NonSIBuyer).toBe(7);
        expect(TradeReportingIndicator.OffBook).toBe(8);
        expect(TradeReportingIndicator.NotReportable).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeReportingIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeReportingIndicator.NotReported,
            TradeReportingIndicator.OnBook,
            TradeReportingIndicator.SISeller,
            TradeReportingIndicator.SIBuyer,
            TradeReportingIndicator.NonSISeller,
            TradeReportingIndicator.SubDelegationByFirm,
            TradeReportingIndicator.Reportable,
            TradeReportingIndicator.NonSIBuyer,
            TradeReportingIndicator.OffBook,
            TradeReportingIndicator.NotReportable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeReportingIndicator type', () => {
        const validate = (value: TradeReportingIndicator) => {
            expect(Object.values(TradeReportingIndicator)).toContain(value);
        };

        validate(TradeReportingIndicator.NotReported);
        validate(TradeReportingIndicator.OnBook);
        validate(TradeReportingIndicator.SISeller);
        validate(TradeReportingIndicator.SIBuyer);
        validate(TradeReportingIndicator.NonSISeller);
        validate(TradeReportingIndicator.SubDelegationByFirm);
        validate(TradeReportingIndicator.Reportable);
        validate(TradeReportingIndicator.NonSIBuyer);
        validate(TradeReportingIndicator.OffBook);
        validate(TradeReportingIndicator.NotReportable);
    });

    test('should be immutable', () => {
        const ref = TradeReportingIndicator;
        expect(() => {
            ref.NotReported = 10;
        }).toThrowError();
    });
});
