import { FundingSource } from '../../src/fieldtypes/FundingSource';

describe('FundingSource', () => {
    test('should have the correct values', () => {
        expect(FundingSource.Repo).toBe(0);
        expect(FundingSource.Cash).toBe(1);
        expect(FundingSource.FreeCedits).toBe(2);
        expect(FundingSource.CustomerShortSales).toBe(3);
        expect(FundingSource.BrokerShortSales).toBe(4);
        expect(FundingSource.UnsecuredBorrowing).toBe(5);
        expect(FundingSource.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            FundingSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            FundingSource.Repo,
            FundingSource.Cash,
            FundingSource.FreeCedits,
            FundingSource.CustomerShortSales,
            FundingSource.BrokerShortSales,
            FundingSource.UnsecuredBorrowing,
            FundingSource.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for FundingSource type', () => {
        const validate = (value: FundingSource) => {
            expect(Object.values(FundingSource)).toContain(value);
        };

        validate(FundingSource.Repo);
        validate(FundingSource.Cash);
        validate(FundingSource.FreeCedits);
        validate(FundingSource.CustomerShortSales);
        validate(FundingSource.BrokerShortSales);
        validate(FundingSource.UnsecuredBorrowing);
        validate(FundingSource.Other);
    });

    test('should be immutable', () => {
        const ref = FundingSource;
        expect(() => {
            ref.Repo = 10;
        }).toThrowError();
    });
});
