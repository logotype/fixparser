import { ApplLevelRecoveryIndicator } from '../../src/fieldtypes/ApplLevelRecoveryIndicator';

describe('ApplLevelRecoveryIndicator', () => {
    test('should have the correct values', () => {
        expect(ApplLevelRecoveryIndicator.NoApplRecoveryNeeded).toBe(0);
        expect(ApplLevelRecoveryIndicator.ApplRecoveryNeeded).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplLevelRecoveryIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplLevelRecoveryIndicator.NoApplRecoveryNeeded,
            ApplLevelRecoveryIndicator.ApplRecoveryNeeded,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplLevelRecoveryIndicator type', () => {
        const validate = (value: ApplLevelRecoveryIndicator) => {
            expect(Object.values(ApplLevelRecoveryIndicator)).toContain(value);
        };

        validate(ApplLevelRecoveryIndicator.NoApplRecoveryNeeded);
        validate(ApplLevelRecoveryIndicator.ApplRecoveryNeeded);
    });

    test('should be immutable', () => {
        const ref = ApplLevelRecoveryIndicator;
        expect(() => {
            ref.NoApplRecoveryNeeded = 10;
        }).toThrowError();
    });
});
