import { DeliveryStreamToleranceOptionSide } from '../../src/fieldtypes/DeliveryStreamToleranceOptionSide';

describe('DeliveryStreamToleranceOptionSide', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamToleranceOptionSide.Buyer).toBe(1);
        expect(DeliveryStreamToleranceOptionSide.Seller).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamToleranceOptionSide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryStreamToleranceOptionSide.Buyer, DeliveryStreamToleranceOptionSide.Seller];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamToleranceOptionSide type', () => {
        const validate = (value: DeliveryStreamToleranceOptionSide) => {
            expect(Object.values(DeliveryStreamToleranceOptionSide)).toContain(value);
        };

        validate(DeliveryStreamToleranceOptionSide.Buyer);
        validate(DeliveryStreamToleranceOptionSide.Seller);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamToleranceOptionSide;
        expect(() => {
            ref.Buyer = 10;
        }).toThrowError();
    });
});
