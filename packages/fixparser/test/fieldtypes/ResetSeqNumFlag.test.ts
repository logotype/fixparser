import { ResetSeqNumFlag } from '../../src/fieldtypes/ResetSeqNumFlag';

describe('ResetSeqNumFlag', () => {
    test('should have the correct values', () => {
        expect(ResetSeqNumFlag.No).toBe('N');
        expect(ResetSeqNumFlag.Yes).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ResetSeqNumFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ResetSeqNumFlag.No, ResetSeqNumFlag.Yes];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ResetSeqNumFlag type', () => {
        const validate = (value: ResetSeqNumFlag) => {
            expect(Object.values(ResetSeqNumFlag)).toContain(value);
        };

        validate(ResetSeqNumFlag.No);
        validate(ResetSeqNumFlag.Yes);
    });

    test('should be immutable', () => {
        const ref = ResetSeqNumFlag;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
