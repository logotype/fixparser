import { LotType } from '../../src/fieldtypes/LotType';

describe('LotType', () => {
    test('should have the correct values', () => {
        expect(LotType.OddLot).toBe('1');
        expect(LotType.RoundLot).toBe('2');
        expect(LotType.BlockLot).toBe('3');
        expect(LotType.RoundLotBasedUpon).toBe('4');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LotType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [LotType.OddLot, LotType.RoundLot, LotType.BlockLot, LotType.RoundLotBasedUpon];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LotType type', () => {
        const validate = (value: LotType) => {
            expect(Object.values(LotType)).toContain(value);
        };

        validate(LotType.OddLot);
        validate(LotType.RoundLot);
        validate(LotType.BlockLot);
        validate(LotType.RoundLotBasedUpon);
    });

    test('should be immutable', () => {
        const ref = LotType;
        expect(() => {
            ref.OddLot = 10;
        }).toThrowError();
    });
});
