import { PostTradePaymentDebitOrCredit } from '../../src/fieldtypes/PostTradePaymentDebitOrCredit';

describe('PostTradePaymentDebitOrCredit', () => {
    test('should have the correct values', () => {
        expect(PostTradePaymentDebitOrCredit.DebitPay).toBe(0);
        expect(PostTradePaymentDebitOrCredit.CreditReceive).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PostTradePaymentDebitOrCredit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PostTradePaymentDebitOrCredit.DebitPay, PostTradePaymentDebitOrCredit.CreditReceive];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PostTradePaymentDebitOrCredit type', () => {
        const validate = (value: PostTradePaymentDebitOrCredit) => {
            expect(Object.values(PostTradePaymentDebitOrCredit)).toContain(value);
        };

        validate(PostTradePaymentDebitOrCredit.DebitPay);
        validate(PostTradePaymentDebitOrCredit.CreditReceive);
    });

    test('should be immutable', () => {
        const ref = PostTradePaymentDebitOrCredit;
        expect(() => {
            ref.DebitPay = 10;
        }).toThrowError();
    });
});
