import { IOIShares } from '../../src/fieldtypes/IOIShares';

describe('IOIShares', () => {
    test('should have the correct values', () => {
        expect(IOIShares.Large).toBe('L');
        expect(IOIShares.Medium).toBe('M');
        expect(IOIShares.Small).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOIShares.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IOIShares.Large, IOIShares.Medium, IOIShares.Small];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOIShares type', () => {
        const validate = (value: IOIShares) => {
            expect(Object.values(IOIShares)).toContain(value);
        };

        validate(IOIShares.Large);
        validate(IOIShares.Medium);
        validate(IOIShares.Small);
    });

    test('should be immutable', () => {
        const ref = IOIShares;
        expect(() => {
            ref.Large = 10;
        }).toThrowError();
    });
});
