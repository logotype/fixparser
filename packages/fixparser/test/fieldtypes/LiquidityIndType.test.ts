import { LiquidityIndType } from '../../src/fieldtypes/LiquidityIndType';

describe('LiquidityIndType', () => {
    test('should have the correct values', () => {
        expect(LiquidityIndType.FiveDayMovingAverage).toBe(1);
        expect(LiquidityIndType.TwentyDayMovingAverage).toBe(2);
        expect(LiquidityIndType.NormalMarketSize).toBe(3);
        expect(LiquidityIndType.Other).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LiquidityIndType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LiquidityIndType.FiveDayMovingAverage,
            LiquidityIndType.TwentyDayMovingAverage,
            LiquidityIndType.NormalMarketSize,
            LiquidityIndType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LiquidityIndType type', () => {
        const validate = (value: LiquidityIndType) => {
            expect(Object.values(LiquidityIndType)).toContain(value);
        };

        validate(LiquidityIndType.FiveDayMovingAverage);
        validate(LiquidityIndType.TwentyDayMovingAverage);
        validate(LiquidityIndType.NormalMarketSize);
        validate(LiquidityIndType.Other);
    });

    test('should be immutable', () => {
        const ref = LiquidityIndType;
        expect(() => {
            ref.FiveDayMovingAverage = 10;
        }).toThrowError();
    });
});
