import { AssignmentMethod } from '../../src/fieldtypes/AssignmentMethod';

describe('AssignmentMethod', () => {
    test('should have the correct values', () => {
        expect(AssignmentMethod.ProRata).toBe('P');
        expect(AssignmentMethod.Random).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AssignmentMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AssignmentMethod.ProRata, AssignmentMethod.Random];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AssignmentMethod type', () => {
        const validate = (value: AssignmentMethod) => {
            expect(Object.values(AssignmentMethod)).toContain(value);
        };

        validate(AssignmentMethod.ProRata);
        validate(AssignmentMethod.Random);
    });

    test('should be immutable', () => {
        const ref = AssignmentMethod;
        expect(() => {
            ref.ProRata = 10;
        }).toThrowError();
    });
});
