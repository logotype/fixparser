import { ListOrderStatus } from '../../src/fieldtypes/ListOrderStatus';

describe('ListOrderStatus', () => {
    test('should have the correct values', () => {
        expect(ListOrderStatus.InBiddingProcess).toBe(1);
        expect(ListOrderStatus.ReceivedForExecution).toBe(2);
        expect(ListOrderStatus.Executing).toBe(3);
        expect(ListOrderStatus.Cancelling).toBe(4);
        expect(ListOrderStatus.Alert).toBe(5);
        expect(ListOrderStatus.AllDone).toBe(6);
        expect(ListOrderStatus.Reject).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListOrderStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ListOrderStatus.InBiddingProcess,
            ListOrderStatus.ReceivedForExecution,
            ListOrderStatus.Executing,
            ListOrderStatus.Cancelling,
            ListOrderStatus.Alert,
            ListOrderStatus.AllDone,
            ListOrderStatus.Reject,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ListOrderStatus type', () => {
        const validate = (value: ListOrderStatus) => {
            expect(Object.values(ListOrderStatus)).toContain(value);
        };

        validate(ListOrderStatus.InBiddingProcess);
        validate(ListOrderStatus.ReceivedForExecution);
        validate(ListOrderStatus.Executing);
        validate(ListOrderStatus.Cancelling);
        validate(ListOrderStatus.Alert);
        validate(ListOrderStatus.AllDone);
        validate(ListOrderStatus.Reject);
    });

    test('should be immutable', () => {
        const ref = ListOrderStatus;
        expect(() => {
            ref.InBiddingProcess = 10;
        }).toThrowError();
    });
});
