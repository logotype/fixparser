import { MarketDisruptionProvision } from '../../src/fieldtypes/MarketDisruptionProvision';

describe('MarketDisruptionProvision', () => {
    test('should have the correct values', () => {
        expect(MarketDisruptionProvision.NotApplicable).toBe(0);
        expect(MarketDisruptionProvision.Applicable).toBe(1);
        expect(MarketDisruptionProvision.AsInMasterAgreement).toBe(2);
        expect(MarketDisruptionProvision.AsInConfirmation).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketDisruptionProvision.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketDisruptionProvision.NotApplicable,
            MarketDisruptionProvision.Applicable,
            MarketDisruptionProvision.AsInMasterAgreement,
            MarketDisruptionProvision.AsInConfirmation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketDisruptionProvision type', () => {
        const validate = (value: MarketDisruptionProvision) => {
            expect(Object.values(MarketDisruptionProvision)).toContain(value);
        };

        validate(MarketDisruptionProvision.NotApplicable);
        validate(MarketDisruptionProvision.Applicable);
        validate(MarketDisruptionProvision.AsInMasterAgreement);
        validate(MarketDisruptionProvision.AsInConfirmation);
    });

    test('should be immutable', () => {
        const ref = MarketDisruptionProvision;
        expect(() => {
            ref.NotApplicable = 10;
        }).toThrowError();
    });
});
