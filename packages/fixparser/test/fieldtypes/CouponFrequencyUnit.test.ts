import { CouponFrequencyUnit } from '../../src/fieldtypes/CouponFrequencyUnit';

describe('CouponFrequencyUnit', () => {
    test('should have the correct values', () => {
        expect(CouponFrequencyUnit.Day).toBe('D');
        expect(CouponFrequencyUnit.Week).toBe('Wk');
        expect(CouponFrequencyUnit.Month).toBe('Mo');
        expect(CouponFrequencyUnit.Year).toBe('Yr');
        expect(CouponFrequencyUnit.Hour).toBe('H');
        expect(CouponFrequencyUnit.Minute).toBe('Min');
        expect(CouponFrequencyUnit.Second).toBe('S');
        expect(CouponFrequencyUnit.Term).toBe('T');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CouponFrequencyUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CouponFrequencyUnit.Day,
            CouponFrequencyUnit.Week,
            CouponFrequencyUnit.Month,
            CouponFrequencyUnit.Year,
            CouponFrequencyUnit.Hour,
            CouponFrequencyUnit.Minute,
            CouponFrequencyUnit.Second,
            CouponFrequencyUnit.Term,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CouponFrequencyUnit type', () => {
        const validate = (value: CouponFrequencyUnit) => {
            expect(Object.values(CouponFrequencyUnit)).toContain(value);
        };

        validate(CouponFrequencyUnit.Day);
        validate(CouponFrequencyUnit.Week);
        validate(CouponFrequencyUnit.Month);
        validate(CouponFrequencyUnit.Year);
        validate(CouponFrequencyUnit.Hour);
        validate(CouponFrequencyUnit.Minute);
        validate(CouponFrequencyUnit.Second);
        validate(CouponFrequencyUnit.Term);
    });

    test('should be immutable', () => {
        const ref = CouponFrequencyUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
