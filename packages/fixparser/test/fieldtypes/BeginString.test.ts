import { BeginString } from '../../src/fieldtypes/BeginString';

describe('BeginString', () => {
    test('should have the correct values', () => {
        expect(BeginString.FIX42).toBe('FIX.4.2');
        expect(BeginString.FIX44).toBe('FIX.4.4');
        expect(BeginString.FIXT11).toBe('FIXT.1.1');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BeginString.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BeginString.FIX42, BeginString.FIX44, BeginString.FIXT11];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BeginString type', () => {
        const validate = (value: BeginString) => {
            expect(Object.values(BeginString)).toContain(value);
        };

        validate(BeginString.FIX42);
        validate(BeginString.FIX44);
        validate(BeginString.FIXT11);
    });

    test('should be immutable', () => {
        const ref = BeginString;
        expect(() => {
            ref.FIX42 = 10;
        }).toThrowError();
    });
});
