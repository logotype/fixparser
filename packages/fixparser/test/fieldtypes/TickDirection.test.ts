import { TickDirection } from '../../src/fieldtypes/TickDirection';

describe('TickDirection', () => {
    test('should have the correct values', () => {
        expect(TickDirection.PlusTick).toBe('0');
        expect(TickDirection.ZeroPlusTick).toBe('1');
        expect(TickDirection.MinusTick).toBe('2');
        expect(TickDirection.ZeroMinusTick).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TickDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TickDirection.PlusTick,
            TickDirection.ZeroPlusTick,
            TickDirection.MinusTick,
            TickDirection.ZeroMinusTick,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TickDirection type', () => {
        const validate = (value: TickDirection) => {
            expect(Object.values(TickDirection)).toContain(value);
        };

        validate(TickDirection.PlusTick);
        validate(TickDirection.ZeroPlusTick);
        validate(TickDirection.MinusTick);
        validate(TickDirection.ZeroMinusTick);
    });

    test('should be immutable', () => {
        const ref = TickDirection;
        expect(() => {
            ref.PlusTick = 10;
        }).toThrowError();
    });
});
