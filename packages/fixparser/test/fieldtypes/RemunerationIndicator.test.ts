import { RemunerationIndicator } from '../../src/fieldtypes/RemunerationIndicator';

describe('RemunerationIndicator', () => {
    test('should have the correct values', () => {
        expect(RemunerationIndicator.NoRemunerationPaid).toBe(0);
        expect(RemunerationIndicator.RemunerationPaid).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RemunerationIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RemunerationIndicator.NoRemunerationPaid, RemunerationIndicator.RemunerationPaid];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RemunerationIndicator type', () => {
        const validate = (value: RemunerationIndicator) => {
            expect(Object.values(RemunerationIndicator)).toContain(value);
        };

        validate(RemunerationIndicator.NoRemunerationPaid);
        validate(RemunerationIndicator.RemunerationPaid);
    });

    test('should be immutable', () => {
        const ref = RemunerationIndicator;
        expect(() => {
            ref.NoRemunerationPaid = 10;
        }).toThrowError();
    });
});
