import { PriorityIndicator } from '../../src/fieldtypes/PriorityIndicator';

describe('PriorityIndicator', () => {
    test('should have the correct values', () => {
        expect(PriorityIndicator.PriorityUnchanged).toBe(0);
        expect(PriorityIndicator.LostPriorityAsResultOfOrderChange).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriorityIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PriorityIndicator.PriorityUnchanged,
            PriorityIndicator.LostPriorityAsResultOfOrderChange,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PriorityIndicator type', () => {
        const validate = (value: PriorityIndicator) => {
            expect(Object.values(PriorityIndicator)).toContain(value);
        };

        validate(PriorityIndicator.PriorityUnchanged);
        validate(PriorityIndicator.LostPriorityAsResultOfOrderChange);
    });

    test('should be immutable', () => {
        const ref = PriorityIndicator;
        expect(() => {
            ref.PriorityUnchanged = 10;
        }).toThrowError();
    });
});
