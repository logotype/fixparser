import { DividendAmountType } from '../../src/fieldtypes/DividendAmountType';

describe('DividendAmountType', () => {
    test('should have the correct values', () => {
        expect(DividendAmountType.RecordAmount).toBe(0);
        expect(DividendAmountType.ExAmount).toBe(1);
        expect(DividendAmountType.PaidAmount).toBe(2);
        expect(DividendAmountType.PerMasterConfirm).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DividendAmountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DividendAmountType.RecordAmount,
            DividendAmountType.ExAmount,
            DividendAmountType.PaidAmount,
            DividendAmountType.PerMasterConfirm,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DividendAmountType type', () => {
        const validate = (value: DividendAmountType) => {
            expect(Object.values(DividendAmountType)).toContain(value);
        };

        validate(DividendAmountType.RecordAmount);
        validate(DividendAmountType.ExAmount);
        validate(DividendAmountType.PaidAmount);
        validate(DividendAmountType.PerMasterConfirm);
    });

    test('should be immutable', () => {
        const ref = DividendAmountType;
        expect(() => {
            ref.RecordAmount = 10;
        }).toThrowError();
    });
});
