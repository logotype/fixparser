import { AveragePriceType } from '../../src/fieldtypes/AveragePriceType';

describe('AveragePriceType', () => {
    test('should have the correct values', () => {
        expect(AveragePriceType.TimeWeightedAveragePrice).toBe(0);
        expect(AveragePriceType.VolumeWeightedAveragePrice).toBe(1);
        expect(AveragePriceType.PercentOfVolumeAveragePrice).toBe(2);
        expect(AveragePriceType.LimitOrderAveragePrice).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AveragePriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AveragePriceType.TimeWeightedAveragePrice,
            AveragePriceType.VolumeWeightedAveragePrice,
            AveragePriceType.PercentOfVolumeAveragePrice,
            AveragePriceType.LimitOrderAveragePrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AveragePriceType type', () => {
        const validate = (value: AveragePriceType) => {
            expect(Object.values(AveragePriceType)).toContain(value);
        };

        validate(AveragePriceType.TimeWeightedAveragePrice);
        validate(AveragePriceType.VolumeWeightedAveragePrice);
        validate(AveragePriceType.PercentOfVolumeAveragePrice);
        validate(AveragePriceType.LimitOrderAveragePrice);
    });

    test('should be immutable', () => {
        const ref = AveragePriceType;
        expect(() => {
            ref.TimeWeightedAveragePrice = 10;
        }).toThrowError();
    });
});
