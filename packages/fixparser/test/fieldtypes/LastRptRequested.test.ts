import { LastRptRequested } from '../../src/fieldtypes/LastRptRequested';

describe('LastRptRequested', () => {
    test('should have the correct values', () => {
        expect(LastRptRequested.NotLastMessage).toBe('N');
        expect(LastRptRequested.LastMessage).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LastRptRequested.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [LastRptRequested.NotLastMessage, LastRptRequested.LastMessage];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LastRptRequested type', () => {
        const validate = (value: LastRptRequested) => {
            expect(Object.values(LastRptRequested)).toContain(value);
        };

        validate(LastRptRequested.NotLastMessage);
        validate(LastRptRequested.LastMessage);
    });

    test('should be immutable', () => {
        const ref = LastRptRequested;
        expect(() => {
            ref.NotLastMessage = 10;
        }).toThrowError();
    });
});
