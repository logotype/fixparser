import { RoundingDirection } from '../../src/fieldtypes/RoundingDirection';

describe('RoundingDirection', () => {
    test('should have the correct values', () => {
        expect(RoundingDirection.RoundToNearest).toBe('0');
        expect(RoundingDirection.RoundDown).toBe('1');
        expect(RoundingDirection.RoundUp).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RoundingDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RoundingDirection.RoundToNearest,
            RoundingDirection.RoundDown,
            RoundingDirection.RoundUp,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for RoundingDirection type', () => {
        const validate = (value: RoundingDirection) => {
            expect(Object.values(RoundingDirection)).toContain(value);
        };

        validate(RoundingDirection.RoundToNearest);
        validate(RoundingDirection.RoundDown);
        validate(RoundingDirection.RoundUp);
    });

    test('should be immutable', () => {
        const ref = RoundingDirection;
        expect(() => {
            ref.RoundToNearest = 10;
        }).toThrowError();
    });
});
