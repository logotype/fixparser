import { PartyRole } from '../../src/fieldtypes/PartyRole';

describe('PartyRole', () => {
    test('should have the correct values', () => {
        expect(PartyRole.ExecutingFirm).toBe(1);
        expect(PartyRole.BrokerOfCredit).toBe(2);
        expect(PartyRole.ClientID).toBe(3);
        expect(PartyRole.ClearingFirm).toBe(4);
        expect(PartyRole.InvestorID).toBe(5);
        expect(PartyRole.IntroducingFirm).toBe(6);
        expect(PartyRole.EnteringFirm).toBe(7);
        expect(PartyRole.Locate).toBe(8);
        expect(PartyRole.FundManagerClientID).toBe(9);
        expect(PartyRole.SettlementLocation).toBe(10);
        expect(PartyRole.OrderOriginationTrader).toBe(11);
        expect(PartyRole.ExecutingTrader).toBe(12);
        expect(PartyRole.OrderOriginationFirm).toBe(13);
        expect(PartyRole.GiveupClearingFirmDepr).toBe(14);
        expect(PartyRole.CorrespondantClearingFirm).toBe(15);
        expect(PartyRole.ExecutingSystem).toBe(16);
        expect(PartyRole.ContraFirm).toBe(17);
        expect(PartyRole.ContraClearingFirm).toBe(18);
        expect(PartyRole.SponsoringFirm).toBe(19);
        expect(PartyRole.UnderlyingContraFirm).toBe(20);
        expect(PartyRole.ClearingOrganization).toBe(21);
        expect(PartyRole.Exchange).toBe(22);
        expect(PartyRole.CustomerAccount).toBe(24);
        expect(PartyRole.CorrespondentClearingOrganization).toBe(25);
        expect(PartyRole.CorrespondentBroker).toBe(26);
        expect(PartyRole.Buyer).toBe(27);
        expect(PartyRole.Custodian).toBe(28);
        expect(PartyRole.Intermediary).toBe(29);
        expect(PartyRole.Agent).toBe(30);
        expect(PartyRole.SubCustodian).toBe(31);
        expect(PartyRole.Beneficiary).toBe(32);
        expect(PartyRole.InterestedParty).toBe(33);
        expect(PartyRole.RegulatoryBody).toBe(34);
        expect(PartyRole.LiquidityProvider).toBe(35);
        expect(PartyRole.EnteringTrader).toBe(36);
        expect(PartyRole.ContraTrader).toBe(37);
        expect(PartyRole.PositionAccount).toBe(38);
        expect(PartyRole.ContraInvestorID).toBe(39);
        expect(PartyRole.TransferToFirm).toBe(40);
        expect(PartyRole.ContraPositionAccount).toBe(41);
        expect(PartyRole.ContraExchange).toBe(42);
        expect(PartyRole.InternalCarryAccount).toBe(43);
        expect(PartyRole.OrderEntryOperatorID).toBe(44);
        expect(PartyRole.SecondaryAccountNumber).toBe(45);
        expect(PartyRole.ForeignFirm).toBe(46);
        expect(PartyRole.ThirdPartyAllocationFirm).toBe(47);
        expect(PartyRole.ClaimingAccount).toBe(48);
        expect(PartyRole.AssetManager).toBe(49);
        expect(PartyRole.PledgorAccount).toBe(50);
        expect(PartyRole.PledgeeAccount).toBe(51);
        expect(PartyRole.LargeTraderReportableAccount).toBe(52);
        expect(PartyRole.TraderMnemonic).toBe(53);
        expect(PartyRole.SenderLocation).toBe(54);
        expect(PartyRole.SessionID).toBe(55);
        expect(PartyRole.AcceptableCounterparty).toBe(56);
        expect(PartyRole.UnacceptableCounterparty).toBe(57);
        expect(PartyRole.EnteringUnit).toBe(58);
        expect(PartyRole.ExecutingUnit).toBe(59);
        expect(PartyRole.IntroducingBroker).toBe(60);
        expect(PartyRole.QuoteOriginator).toBe(61);
        expect(PartyRole.ReportOriginator).toBe(62);
        expect(PartyRole.SystematicInternaliser).toBe(63);
        expect(PartyRole.MultilateralTradingFacility).toBe(64);
        expect(PartyRole.RegulatedMarket).toBe(65);
        expect(PartyRole.MarketMaker).toBe(66);
        expect(PartyRole.InvestmentFirm).toBe(67);
        expect(PartyRole.HostCompetentAuthority).toBe(68);
        expect(PartyRole.HomeCompetentAuthority).toBe(69);
        expect(PartyRole.CompetentAuthorityLiquidity).toBe(70);
        expect(PartyRole.CompetentAuthorityTransactionVenue).toBe(71);
        expect(PartyRole.ReportingIntermediary).toBe(72);
        expect(PartyRole.ExecutionVenue).toBe(73);
        expect(PartyRole.MarketDataEntryOriginator).toBe(74);
        expect(PartyRole.LocationID).toBe(75);
        expect(PartyRole.DeskID).toBe(76);
        expect(PartyRole.MarketDataMarket).toBe(77);
        expect(PartyRole.AllocationEntity).toBe(78);
        expect(PartyRole.PrimeBroker).toBe(79);
        expect(PartyRole.StepOutFirm).toBe(80);
        expect(PartyRole.BrokerClearingID).toBe(81);
        expect(PartyRole.CentralRegistrationDepository).toBe(82);
        expect(PartyRole.ClearingAccount).toBe(83);
        expect(PartyRole.AcceptableSettlingCounterparty).toBe(84);
        expect(PartyRole.UnacceptableSettlingCounterparty).toBe(85);
        expect(PartyRole.CLSMemberBank).toBe(86);
        expect(PartyRole.InConcertGroup).toBe(87);
        expect(PartyRole.InConcertControllingEntity).toBe(88);
        expect(PartyRole.LargePositionsReportingAccount).toBe(89);
        expect(PartyRole.SettlementFirm).toBe(90);
        expect(PartyRole.SettlementAccount).toBe(91);
        expect(PartyRole.ReportingMarketCenter).toBe(92);
        expect(PartyRole.RelatedReportingMarketCenter).toBe(93);
        expect(PartyRole.AwayMarket).toBe(94);
        expect(PartyRole.GiveupTradingFirm).toBe(95);
        expect(PartyRole.TakeupTradingFirm).toBe(96);
        expect(PartyRole.GiveupClearingFirm).toBe(97);
        expect(PartyRole.TakeupClearingFirm).toBe(98);
        expect(PartyRole.OriginatingMarket).toBe(99);
        expect(PartyRole.MarginAccount).toBe(100);
        expect(PartyRole.CollateralAssetAccount).toBe(101);
        expect(PartyRole.DataRepository).toBe(102);
        expect(PartyRole.CalculationAgent).toBe(103);
        expect(PartyRole.ExerciseNoticeSender).toBe(104);
        expect(PartyRole.ExerciseNoticeReceiver).toBe(105);
        expect(PartyRole.RateReferenceBank).toBe(106);
        expect(PartyRole.Correspondent).toBe(107);
        expect(PartyRole.BeneficiaryBank).toBe(109);
        expect(PartyRole.Borrower).toBe(110);
        expect(PartyRole.PrimaryObligator).toBe(111);
        expect(PartyRole.Guarantor).toBe(112);
        expect(PartyRole.ExcludedReferenceEntity).toBe(113);
        expect(PartyRole.DeterminingParty).toBe(114);
        expect(PartyRole.HedgingParty).toBe(115);
        expect(PartyRole.ReportingEntity).toBe(116);
        expect(PartyRole.SalesPerson).toBe(117);
        expect(PartyRole.Operator).toBe(118);
        expect(PartyRole.CSD).toBe(119);
        expect(PartyRole.ICSD).toBe(120);
        expect(PartyRole.TradingSubAccount).toBe(121);
        expect(PartyRole.InvestmentDecisionMaker).toBe(122);
        expect(PartyRole.PublishingIntermediary).toBe(123);
        expect(PartyRole.CSDParticipant).toBe(124);
        expect(PartyRole.Issuer).toBe(125);
        expect(PartyRole.ContraCustomerAccount).toBe(126);
        expect(PartyRole.ContraInvestmentDecisionMaker).toBe(127);
        expect(PartyRole.AuthorizingPerson).toBe(128);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyRole.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyRole.ExecutingFirm,
            PartyRole.BrokerOfCredit,
            PartyRole.ClientID,
            PartyRole.ClearingFirm,
            PartyRole.InvestorID,
            PartyRole.IntroducingFirm,
            PartyRole.EnteringFirm,
            PartyRole.Locate,
            PartyRole.FundManagerClientID,
            PartyRole.SettlementLocation,
            PartyRole.OrderOriginationTrader,
            PartyRole.ExecutingTrader,
            PartyRole.OrderOriginationFirm,
            PartyRole.GiveupClearingFirmDepr,
            PartyRole.CorrespondantClearingFirm,
            PartyRole.ExecutingSystem,
            PartyRole.ContraFirm,
            PartyRole.ContraClearingFirm,
            PartyRole.SponsoringFirm,
            PartyRole.UnderlyingContraFirm,
            PartyRole.ClearingOrganization,
            PartyRole.Exchange,
            PartyRole.CustomerAccount,
            PartyRole.CorrespondentClearingOrganization,
            PartyRole.CorrespondentBroker,
            PartyRole.Buyer,
            PartyRole.Custodian,
            PartyRole.Intermediary,
            PartyRole.Agent,
            PartyRole.SubCustodian,
            PartyRole.Beneficiary,
            PartyRole.InterestedParty,
            PartyRole.RegulatoryBody,
            PartyRole.LiquidityProvider,
            PartyRole.EnteringTrader,
            PartyRole.ContraTrader,
            PartyRole.PositionAccount,
            PartyRole.ContraInvestorID,
            PartyRole.TransferToFirm,
            PartyRole.ContraPositionAccount,
            PartyRole.ContraExchange,
            PartyRole.InternalCarryAccount,
            PartyRole.OrderEntryOperatorID,
            PartyRole.SecondaryAccountNumber,
            PartyRole.ForeignFirm,
            PartyRole.ThirdPartyAllocationFirm,
            PartyRole.ClaimingAccount,
            PartyRole.AssetManager,
            PartyRole.PledgorAccount,
            PartyRole.PledgeeAccount,
            PartyRole.LargeTraderReportableAccount,
            PartyRole.TraderMnemonic,
            PartyRole.SenderLocation,
            PartyRole.SessionID,
            PartyRole.AcceptableCounterparty,
            PartyRole.UnacceptableCounterparty,
            PartyRole.EnteringUnit,
            PartyRole.ExecutingUnit,
            PartyRole.IntroducingBroker,
            PartyRole.QuoteOriginator,
            PartyRole.ReportOriginator,
            PartyRole.SystematicInternaliser,
            PartyRole.MultilateralTradingFacility,
            PartyRole.RegulatedMarket,
            PartyRole.MarketMaker,
            PartyRole.InvestmentFirm,
            PartyRole.HostCompetentAuthority,
            PartyRole.HomeCompetentAuthority,
            PartyRole.CompetentAuthorityLiquidity,
            PartyRole.CompetentAuthorityTransactionVenue,
            PartyRole.ReportingIntermediary,
            PartyRole.ExecutionVenue,
            PartyRole.MarketDataEntryOriginator,
            PartyRole.LocationID,
            PartyRole.DeskID,
            PartyRole.MarketDataMarket,
            PartyRole.AllocationEntity,
            PartyRole.PrimeBroker,
            PartyRole.StepOutFirm,
            PartyRole.BrokerClearingID,
            PartyRole.CentralRegistrationDepository,
            PartyRole.ClearingAccount,
            PartyRole.AcceptableSettlingCounterparty,
            PartyRole.UnacceptableSettlingCounterparty,
            PartyRole.CLSMemberBank,
            PartyRole.InConcertGroup,
            PartyRole.InConcertControllingEntity,
            PartyRole.LargePositionsReportingAccount,
            PartyRole.SettlementFirm,
            PartyRole.SettlementAccount,
            PartyRole.ReportingMarketCenter,
            PartyRole.RelatedReportingMarketCenter,
            PartyRole.AwayMarket,
            PartyRole.GiveupTradingFirm,
            PartyRole.TakeupTradingFirm,
            PartyRole.GiveupClearingFirm,
            PartyRole.TakeupClearingFirm,
            PartyRole.OriginatingMarket,
            PartyRole.MarginAccount,
            PartyRole.CollateralAssetAccount,
            PartyRole.DataRepository,
            PartyRole.CalculationAgent,
            PartyRole.ExerciseNoticeSender,
            PartyRole.ExerciseNoticeReceiver,
            PartyRole.RateReferenceBank,
            PartyRole.Correspondent,
            PartyRole.BeneficiaryBank,
            PartyRole.Borrower,
            PartyRole.PrimaryObligator,
            PartyRole.Guarantor,
            PartyRole.ExcludedReferenceEntity,
            PartyRole.DeterminingParty,
            PartyRole.HedgingParty,
            PartyRole.ReportingEntity,
            PartyRole.SalesPerson,
            PartyRole.Operator,
            PartyRole.CSD,
            PartyRole.ICSD,
            PartyRole.TradingSubAccount,
            PartyRole.InvestmentDecisionMaker,
            PartyRole.PublishingIntermediary,
            PartyRole.CSDParticipant,
            PartyRole.Issuer,
            PartyRole.ContraCustomerAccount,
            PartyRole.ContraInvestmentDecisionMaker,
            PartyRole.AuthorizingPerson,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyRole type', () => {
        const validate = (value: PartyRole) => {
            expect(Object.values(PartyRole)).toContain(value);
        };

        validate(PartyRole.ExecutingFirm);
        validate(PartyRole.BrokerOfCredit);
        validate(PartyRole.ClientID);
        validate(PartyRole.ClearingFirm);
        validate(PartyRole.InvestorID);
        validate(PartyRole.IntroducingFirm);
        validate(PartyRole.EnteringFirm);
        validate(PartyRole.Locate);
        validate(PartyRole.FundManagerClientID);
        validate(PartyRole.SettlementLocation);
        validate(PartyRole.OrderOriginationTrader);
        validate(PartyRole.ExecutingTrader);
        validate(PartyRole.OrderOriginationFirm);
        validate(PartyRole.GiveupClearingFirmDepr);
        validate(PartyRole.CorrespondantClearingFirm);
        validate(PartyRole.ExecutingSystem);
        validate(PartyRole.ContraFirm);
        validate(PartyRole.ContraClearingFirm);
        validate(PartyRole.SponsoringFirm);
        validate(PartyRole.UnderlyingContraFirm);
        validate(PartyRole.ClearingOrganization);
        validate(PartyRole.Exchange);
        validate(PartyRole.CustomerAccount);
        validate(PartyRole.CorrespondentClearingOrganization);
        validate(PartyRole.CorrespondentBroker);
        validate(PartyRole.Buyer);
        validate(PartyRole.Custodian);
        validate(PartyRole.Intermediary);
        validate(PartyRole.Agent);
        validate(PartyRole.SubCustodian);
        validate(PartyRole.Beneficiary);
        validate(PartyRole.InterestedParty);
        validate(PartyRole.RegulatoryBody);
        validate(PartyRole.LiquidityProvider);
        validate(PartyRole.EnteringTrader);
        validate(PartyRole.ContraTrader);
        validate(PartyRole.PositionAccount);
        validate(PartyRole.ContraInvestorID);
        validate(PartyRole.TransferToFirm);
        validate(PartyRole.ContraPositionAccount);
        validate(PartyRole.ContraExchange);
        validate(PartyRole.InternalCarryAccount);
        validate(PartyRole.OrderEntryOperatorID);
        validate(PartyRole.SecondaryAccountNumber);
        validate(PartyRole.ForeignFirm);
        validate(PartyRole.ThirdPartyAllocationFirm);
        validate(PartyRole.ClaimingAccount);
        validate(PartyRole.AssetManager);
        validate(PartyRole.PledgorAccount);
        validate(PartyRole.PledgeeAccount);
        validate(PartyRole.LargeTraderReportableAccount);
        validate(PartyRole.TraderMnemonic);
        validate(PartyRole.SenderLocation);
        validate(PartyRole.SessionID);
        validate(PartyRole.AcceptableCounterparty);
        validate(PartyRole.UnacceptableCounterparty);
        validate(PartyRole.EnteringUnit);
        validate(PartyRole.ExecutingUnit);
        validate(PartyRole.IntroducingBroker);
        validate(PartyRole.QuoteOriginator);
        validate(PartyRole.ReportOriginator);
        validate(PartyRole.SystematicInternaliser);
        validate(PartyRole.MultilateralTradingFacility);
        validate(PartyRole.RegulatedMarket);
        validate(PartyRole.MarketMaker);
        validate(PartyRole.InvestmentFirm);
        validate(PartyRole.HostCompetentAuthority);
        validate(PartyRole.HomeCompetentAuthority);
        validate(PartyRole.CompetentAuthorityLiquidity);
        validate(PartyRole.CompetentAuthorityTransactionVenue);
        validate(PartyRole.ReportingIntermediary);
        validate(PartyRole.ExecutionVenue);
        validate(PartyRole.MarketDataEntryOriginator);
        validate(PartyRole.LocationID);
        validate(PartyRole.DeskID);
        validate(PartyRole.MarketDataMarket);
        validate(PartyRole.AllocationEntity);
        validate(PartyRole.PrimeBroker);
        validate(PartyRole.StepOutFirm);
        validate(PartyRole.BrokerClearingID);
        validate(PartyRole.CentralRegistrationDepository);
        validate(PartyRole.ClearingAccount);
        validate(PartyRole.AcceptableSettlingCounterparty);
        validate(PartyRole.UnacceptableSettlingCounterparty);
        validate(PartyRole.CLSMemberBank);
        validate(PartyRole.InConcertGroup);
        validate(PartyRole.InConcertControllingEntity);
        validate(PartyRole.LargePositionsReportingAccount);
        validate(PartyRole.SettlementFirm);
        validate(PartyRole.SettlementAccount);
        validate(PartyRole.ReportingMarketCenter);
        validate(PartyRole.RelatedReportingMarketCenter);
        validate(PartyRole.AwayMarket);
        validate(PartyRole.GiveupTradingFirm);
        validate(PartyRole.TakeupTradingFirm);
        validate(PartyRole.GiveupClearingFirm);
        validate(PartyRole.TakeupClearingFirm);
        validate(PartyRole.OriginatingMarket);
        validate(PartyRole.MarginAccount);
        validate(PartyRole.CollateralAssetAccount);
        validate(PartyRole.DataRepository);
        validate(PartyRole.CalculationAgent);
        validate(PartyRole.ExerciseNoticeSender);
        validate(PartyRole.ExerciseNoticeReceiver);
        validate(PartyRole.RateReferenceBank);
        validate(PartyRole.Correspondent);
        validate(PartyRole.BeneficiaryBank);
        validate(PartyRole.Borrower);
        validate(PartyRole.PrimaryObligator);
        validate(PartyRole.Guarantor);
        validate(PartyRole.ExcludedReferenceEntity);
        validate(PartyRole.DeterminingParty);
        validate(PartyRole.HedgingParty);
        validate(PartyRole.ReportingEntity);
        validate(PartyRole.SalesPerson);
        validate(PartyRole.Operator);
        validate(PartyRole.CSD);
        validate(PartyRole.ICSD);
        validate(PartyRole.TradingSubAccount);
        validate(PartyRole.InvestmentDecisionMaker);
        validate(PartyRole.PublishingIntermediary);
        validate(PartyRole.CSDParticipant);
        validate(PartyRole.Issuer);
        validate(PartyRole.ContraCustomerAccount);
        validate(PartyRole.ContraInvestmentDecisionMaker);
        validate(PartyRole.AuthorizingPerson);
    });

    test('should be immutable', () => {
        const ref = PartyRole;
        expect(() => {
            ref.ExecutingFirm = 10;
        }).toThrowError();
    });
});
