import { PosMaintStatus } from '../../src/fieldtypes/PosMaintStatus';

describe('PosMaintStatus', () => {
    test('should have the correct values', () => {
        expect(PosMaintStatus.Accepted).toBe(0);
        expect(PosMaintStatus.AcceptedWithWarnings).toBe(1);
        expect(PosMaintStatus.Rejected).toBe(2);
        expect(PosMaintStatus.Completed).toBe(3);
        expect(PosMaintStatus.CompletedWithWarnings).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosMaintStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosMaintStatus.Accepted,
            PosMaintStatus.AcceptedWithWarnings,
            PosMaintStatus.Rejected,
            PosMaintStatus.Completed,
            PosMaintStatus.CompletedWithWarnings,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosMaintStatus type', () => {
        const validate = (value: PosMaintStatus) => {
            expect(Object.values(PosMaintStatus)).toContain(value);
        };

        validate(PosMaintStatus.Accepted);
        validate(PosMaintStatus.AcceptedWithWarnings);
        validate(PosMaintStatus.Rejected);
        validate(PosMaintStatus.Completed);
        validate(PosMaintStatus.CompletedWithWarnings);
    });

    test('should be immutable', () => {
        const ref = PosMaintStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
