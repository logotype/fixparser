import { ExerciseStyle } from '../../src/fieldtypes/ExerciseStyle';

describe('ExerciseStyle', () => {
    test('should have the correct values', () => {
        expect(ExerciseStyle.European).toBe(0);
        expect(ExerciseStyle.American).toBe(1);
        expect(ExerciseStyle.Bermuda).toBe(2);
        expect(ExerciseStyle.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExerciseStyle.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExerciseStyle.European,
            ExerciseStyle.American,
            ExerciseStyle.Bermuda,
            ExerciseStyle.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExerciseStyle type', () => {
        const validate = (value: ExerciseStyle) => {
            expect(Object.values(ExerciseStyle)).toContain(value);
        };

        validate(ExerciseStyle.European);
        validate(ExerciseStyle.American);
        validate(ExerciseStyle.Bermuda);
        validate(ExerciseStyle.Other);
    });

    test('should be immutable', () => {
        const ref = ExerciseStyle;
        expect(() => {
            ref.European = 10;
        }).toThrowError();
    });
});
