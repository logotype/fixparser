import { DiscretionMoveType } from '../../src/fieldtypes/DiscretionMoveType';

describe('DiscretionMoveType', () => {
    test('should have the correct values', () => {
        expect(DiscretionMoveType.Floating).toBe(0);
        expect(DiscretionMoveType.Fixed).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionMoveType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DiscretionMoveType.Floating, DiscretionMoveType.Fixed];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DiscretionMoveType type', () => {
        const validate = (value: DiscretionMoveType) => {
            expect(Object.values(DiscretionMoveType)).toContain(value);
        };

        validate(DiscretionMoveType.Floating);
        validate(DiscretionMoveType.Fixed);
    });

    test('should be immutable', () => {
        const ref = DiscretionMoveType;
        expect(() => {
            ref.Floating = 10;
        }).toThrowError();
    });
});
