import { RegulatoryTransactionType } from '../../src/fieldtypes/RegulatoryTransactionType';

describe('RegulatoryTransactionType', () => {
    test('should have the correct values', () => {
        expect(RegulatoryTransactionType.None).toBe(0);
        expect(RegulatoryTransactionType.SEFRequiredTransaction).toBe(1);
        expect(RegulatoryTransactionType.SEFPermittedTransaction).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegulatoryTransactionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegulatoryTransactionType.None,
            RegulatoryTransactionType.SEFRequiredTransaction,
            RegulatoryTransactionType.SEFPermittedTransaction,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegulatoryTransactionType type', () => {
        const validate = (value: RegulatoryTransactionType) => {
            expect(Object.values(RegulatoryTransactionType)).toContain(value);
        };

        validate(RegulatoryTransactionType.None);
        validate(RegulatoryTransactionType.SEFRequiredTransaction);
        validate(RegulatoryTransactionType.SEFPermittedTransaction);
    });

    test('should be immutable', () => {
        const ref = RegulatoryTransactionType;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
