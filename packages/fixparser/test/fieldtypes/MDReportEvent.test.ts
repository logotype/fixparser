import { MDReportEvent } from '../../src/fieldtypes/MDReportEvent';

describe('MDReportEvent', () => {
    test('should have the correct values', () => {
        expect(MDReportEvent.StartInstrumentRefData).toBe(1);
        expect(MDReportEvent.EndInstrumentRefData).toBe(2);
        expect(MDReportEvent.StartOffMarketTrades).toBe(3);
        expect(MDReportEvent.EndOffMarketTrades).toBe(4);
        expect(MDReportEvent.StartOrderBookTrades).toBe(5);
        expect(MDReportEvent.EndOrderBookTrades).toBe(6);
        expect(MDReportEvent.StartOpenInterest).toBe(7);
        expect(MDReportEvent.EndOpenInterest).toBe(8);
        expect(MDReportEvent.StartSettlementPrices).toBe(9);
        expect(MDReportEvent.EndSettlementPrices).toBe(10);
        expect(MDReportEvent.StartStatsRefData).toBe(11);
        expect(MDReportEvent.EndStatsRefData).toBe(12);
        expect(MDReportEvent.StartStatistics).toBe(13);
        expect(MDReportEvent.EndStatistics).toBe(14);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDReportEvent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDReportEvent.StartInstrumentRefData,
            MDReportEvent.EndInstrumentRefData,
            MDReportEvent.StartOffMarketTrades,
            MDReportEvent.EndOffMarketTrades,
            MDReportEvent.StartOrderBookTrades,
            MDReportEvent.EndOrderBookTrades,
            MDReportEvent.StartOpenInterest,
            MDReportEvent.EndOpenInterest,
            MDReportEvent.StartSettlementPrices,
            MDReportEvent.EndSettlementPrices,
            MDReportEvent.StartStatsRefData,
            MDReportEvent.EndStatsRefData,
            MDReportEvent.StartStatistics,
            MDReportEvent.EndStatistics,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDReportEvent type', () => {
        const validate = (value: MDReportEvent) => {
            expect(Object.values(MDReportEvent)).toContain(value);
        };

        validate(MDReportEvent.StartInstrumentRefData);
        validate(MDReportEvent.EndInstrumentRefData);
        validate(MDReportEvent.StartOffMarketTrades);
        validate(MDReportEvent.EndOffMarketTrades);
        validate(MDReportEvent.StartOrderBookTrades);
        validate(MDReportEvent.EndOrderBookTrades);
        validate(MDReportEvent.StartOpenInterest);
        validate(MDReportEvent.EndOpenInterest);
        validate(MDReportEvent.StartSettlementPrices);
        validate(MDReportEvent.EndSettlementPrices);
        validate(MDReportEvent.StartStatsRefData);
        validate(MDReportEvent.EndStatsRefData);
        validate(MDReportEvent.StartStatistics);
        validate(MDReportEvent.EndStatistics);
    });

    test('should be immutable', () => {
        const ref = MDReportEvent;
        expect(() => {
            ref.StartInstrumentRefData = 10;
        }).toThrowError();
    });
});
