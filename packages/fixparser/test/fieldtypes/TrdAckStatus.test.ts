import { TrdAckStatus } from '../../src/fieldtypes/TrdAckStatus';

describe('TrdAckStatus', () => {
    test('should have the correct values', () => {
        expect(TrdAckStatus.Accepted).toBe(0);
        expect(TrdAckStatus.Rejected).toBe(1);
        expect(TrdAckStatus.Received).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdAckStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TrdAckStatus.Accepted, TrdAckStatus.Rejected, TrdAckStatus.Received];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdAckStatus type', () => {
        const validate = (value: TrdAckStatus) => {
            expect(Object.values(TrdAckStatus)).toContain(value);
        };

        validate(TrdAckStatus.Accepted);
        validate(TrdAckStatus.Rejected);
        validate(TrdAckStatus.Received);
    });

    test('should be immutable', () => {
        const ref = TrdAckStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
