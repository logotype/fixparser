import { DeliveryStreamType } from '../../src/fieldtypes/DeliveryStreamType';

describe('DeliveryStreamType', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamType.Periodic).toBe(0);
        expect(DeliveryStreamType.Initial).toBe(1);
        expect(DeliveryStreamType.Single).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryStreamType.Periodic, DeliveryStreamType.Initial, DeliveryStreamType.Single];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamType type', () => {
        const validate = (value: DeliveryStreamType) => {
            expect(Object.values(DeliveryStreamType)).toContain(value);
        };

        validate(DeliveryStreamType.Periodic);
        validate(DeliveryStreamType.Initial);
        validate(DeliveryStreamType.Single);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamType;
        expect(() => {
            ref.Periodic = 10;
        }).toThrowError();
    });
});
