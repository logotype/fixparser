import { MassOrderRequestStatus } from '../../src/fieldtypes/MassOrderRequestStatus';

describe('MassOrderRequestStatus', () => {
    test('should have the correct values', () => {
        expect(MassOrderRequestStatus.Accepted).toBe(1);
        expect(MassOrderRequestStatus.AcceptedWithAdditionalEvents).toBe(2);
        expect(MassOrderRequestStatus.Rejected).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassOrderRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassOrderRequestStatus.Accepted,
            MassOrderRequestStatus.AcceptedWithAdditionalEvents,
            MassOrderRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassOrderRequestStatus type', () => {
        const validate = (value: MassOrderRequestStatus) => {
            expect(Object.values(MassOrderRequestStatus)).toContain(value);
        };

        validate(MassOrderRequestStatus.Accepted);
        validate(MassOrderRequestStatus.AcceptedWithAdditionalEvents);
        validate(MassOrderRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = MassOrderRequestStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
