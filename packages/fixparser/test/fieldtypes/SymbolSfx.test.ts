import { SymbolSfx } from '../../src/fieldtypes/SymbolSfx';

describe('SymbolSfx', () => {
    test('should have the correct values', () => {
        expect(SymbolSfx.EUCPWithLumpSumInterest).toBe('CD');
        expect(SymbolSfx.WhenIssued).toBe('WI');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SymbolSfx.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SymbolSfx.EUCPWithLumpSumInterest, SymbolSfx.WhenIssued];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SymbolSfx type', () => {
        const validate = (value: SymbolSfx) => {
            expect(Object.values(SymbolSfx)).toContain(value);
        };

        validate(SymbolSfx.EUCPWithLumpSumInterest);
        validate(SymbolSfx.WhenIssued);
    });

    test('should be immutable', () => {
        const ref = SymbolSfx;
        expect(() => {
            ref.EUCPWithLumpSumInterest = 10;
        }).toThrowError();
    });
});
