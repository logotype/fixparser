import { ClearingInstruction } from '../../src/fieldtypes/ClearingInstruction';

describe('ClearingInstruction', () => {
    test('should have the correct values', () => {
        expect(ClearingInstruction.ProcessNormally).toBe(0);
        expect(ClearingInstruction.ExcludeFromAllNetting).toBe(1);
        expect(ClearingInstruction.BilateralNettingOnly).toBe(2);
        expect(ClearingInstruction.ExClearing).toBe(3);
        expect(ClearingInstruction.SpecialTrade).toBe(4);
        expect(ClearingInstruction.MultilateralNetting).toBe(5);
        expect(ClearingInstruction.ClearAgainstCentralCounterparty).toBe(6);
        expect(ClearingInstruction.ExcludeFromCentralCounterparty).toBe(7);
        expect(ClearingInstruction.ManualMode).toBe(8);
        expect(ClearingInstruction.AutomaticPostingMode).toBe(9);
        expect(ClearingInstruction.AutomaticGiveUpMode).toBe(10);
        expect(ClearingInstruction.QualifiedServiceRepresentativeQSR).toBe(11);
        expect(ClearingInstruction.CustomerTrade).toBe(12);
        expect(ClearingInstruction.SelfClearing).toBe(13);
        expect(ClearingInstruction.BuyIn).toBe(14);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearingInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ClearingInstruction.ProcessNormally,
            ClearingInstruction.ExcludeFromAllNetting,
            ClearingInstruction.BilateralNettingOnly,
            ClearingInstruction.ExClearing,
            ClearingInstruction.SpecialTrade,
            ClearingInstruction.MultilateralNetting,
            ClearingInstruction.ClearAgainstCentralCounterparty,
            ClearingInstruction.ExcludeFromCentralCounterparty,
            ClearingInstruction.ManualMode,
            ClearingInstruction.AutomaticPostingMode,
            ClearingInstruction.AutomaticGiveUpMode,
            ClearingInstruction.QualifiedServiceRepresentativeQSR,
            ClearingInstruction.CustomerTrade,
            ClearingInstruction.SelfClearing,
            ClearingInstruction.BuyIn,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ClearingInstruction type', () => {
        const validate = (value: ClearingInstruction) => {
            expect(Object.values(ClearingInstruction)).toContain(value);
        };

        validate(ClearingInstruction.ProcessNormally);
        validate(ClearingInstruction.ExcludeFromAllNetting);
        validate(ClearingInstruction.BilateralNettingOnly);
        validate(ClearingInstruction.ExClearing);
        validate(ClearingInstruction.SpecialTrade);
        validate(ClearingInstruction.MultilateralNetting);
        validate(ClearingInstruction.ClearAgainstCentralCounterparty);
        validate(ClearingInstruction.ExcludeFromCentralCounterparty);
        validate(ClearingInstruction.ManualMode);
        validate(ClearingInstruction.AutomaticPostingMode);
        validate(ClearingInstruction.AutomaticGiveUpMode);
        validate(ClearingInstruction.QualifiedServiceRepresentativeQSR);
        validate(ClearingInstruction.CustomerTrade);
        validate(ClearingInstruction.SelfClearing);
        validate(ClearingInstruction.BuyIn);
    });

    test('should be immutable', () => {
        const ref = ClearingInstruction;
        expect(() => {
            ref.ProcessNormally = 10;
        }).toThrowError();
    });
});
