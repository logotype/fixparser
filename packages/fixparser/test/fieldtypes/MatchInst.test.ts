import { MatchInst } from '../../src/fieldtypes/MatchInst';

describe('MatchInst', () => {
    test('should have the correct values', () => {
        expect(MatchInst.Match).toBe(1);
        expect(MatchInst.DoNotMatch).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MatchInst.Match, MatchInst.DoNotMatch];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MatchInst type', () => {
        const validate = (value: MatchInst) => {
            expect(Object.values(MatchInst)).toContain(value);
        };

        validate(MatchInst.Match);
        validate(MatchInst.DoNotMatch);
    });

    test('should be immutable', () => {
        const ref = MatchInst;
        expect(() => {
            ref.Match = 10;
        }).toThrowError();
    });
});
