import { RestructuringType } from '../../src/fieldtypes/RestructuringType';

describe('RestructuringType', () => {
    test('should have the correct values', () => {
        expect(RestructuringType.FullRestructuring).toBe('FR');
        expect(RestructuringType.ModifiedRestructuring).toBe('MR');
        expect(RestructuringType.ModifiedModRestructuring).toBe('MM');
        expect(RestructuringType.NoRestructuringSpecified).toBe('XR');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RestructuringType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RestructuringType.FullRestructuring,
            RestructuringType.ModifiedRestructuring,
            RestructuringType.ModifiedModRestructuring,
            RestructuringType.NoRestructuringSpecified,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for RestructuringType type', () => {
        const validate = (value: RestructuringType) => {
            expect(Object.values(RestructuringType)).toContain(value);
        };

        validate(RestructuringType.FullRestructuring);
        validate(RestructuringType.ModifiedRestructuring);
        validate(RestructuringType.ModifiedModRestructuring);
        validate(RestructuringType.NoRestructuringSpecified);
    });

    test('should be immutable', () => {
        const ref = RestructuringType;
        expect(() => {
            ref.FullRestructuring = 10;
        }).toThrowError();
    });
});
