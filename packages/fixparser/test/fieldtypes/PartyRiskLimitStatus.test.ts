import { PartyRiskLimitStatus } from '../../src/fieldtypes/PartyRiskLimitStatus';

describe('PartyRiskLimitStatus', () => {
    test('should have the correct values', () => {
        expect(PartyRiskLimitStatus.Disabled).toBe(0);
        expect(PartyRiskLimitStatus.Enabled).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyRiskLimitStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PartyRiskLimitStatus.Disabled, PartyRiskLimitStatus.Enabled];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyRiskLimitStatus type', () => {
        const validate = (value: PartyRiskLimitStatus) => {
            expect(Object.values(PartyRiskLimitStatus)).toContain(value);
        };

        validate(PartyRiskLimitStatus.Disabled);
        validate(PartyRiskLimitStatus.Enabled);
    });

    test('should be immutable', () => {
        const ref = PartyRiskLimitStatus;
        expect(() => {
            ref.Disabled = 10;
        }).toThrowError();
    });
});
