import { Product } from '../../src/fieldtypes/Product';

describe('Product', () => {
    test('should have the correct values', () => {
        expect(Product.AGENCY).toBe(1);
        expect(Product.COMMODITY).toBe(2);
        expect(Product.CORPORATE).toBe(3);
        expect(Product.CURRENCY).toBe(4);
        expect(Product.EQUITY).toBe(5);
        expect(Product.GOVERNMENT).toBe(6);
        expect(Product.INDEX).toBe(7);
        expect(Product.LOAN).toBe(8);
        expect(Product.MONEYMARKET).toBe(9);
        expect(Product.MORTGAGE).toBe(10);
        expect(Product.MUNICIPAL).toBe(11);
        expect(Product.OTHER).toBe(12);
        expect(Product.FINANCING).toBe(13);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Product.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Product.AGENCY,
            Product.COMMODITY,
            Product.CORPORATE,
            Product.CURRENCY,
            Product.EQUITY,
            Product.GOVERNMENT,
            Product.INDEX,
            Product.LOAN,
            Product.MONEYMARKET,
            Product.MORTGAGE,
            Product.MUNICIPAL,
            Product.OTHER,
            Product.FINANCING,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for Product type', () => {
        const validate = (value: Product) => {
            expect(Object.values(Product)).toContain(value);
        };

        validate(Product.AGENCY);
        validate(Product.COMMODITY);
        validate(Product.CORPORATE);
        validate(Product.CURRENCY);
        validate(Product.EQUITY);
        validate(Product.GOVERNMENT);
        validate(Product.INDEX);
        validate(Product.LOAN);
        validate(Product.MONEYMARKET);
        validate(Product.MORTGAGE);
        validate(Product.MUNICIPAL);
        validate(Product.OTHER);
        validate(Product.FINANCING);
    });

    test('should be immutable', () => {
        const ref = Product;
        expect(() => {
            ref.AGENCY = 10;
        }).toThrowError();
    });
});
