import { MultiLegReportingType } from '../../src/fieldtypes/MultiLegReportingType';

describe('MultiLegReportingType', () => {
    test('should have the correct values', () => {
        expect(MultiLegReportingType.SingleSecurity).toBe('1');
        expect(MultiLegReportingType.IndividualLegOfAMultiLegSecurity).toBe('2');
        expect(MultiLegReportingType.MultiLegSecurity).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MultiLegReportingType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MultiLegReportingType.SingleSecurity,
            MultiLegReportingType.IndividualLegOfAMultiLegSecurity,
            MultiLegReportingType.MultiLegSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MultiLegReportingType type', () => {
        const validate = (value: MultiLegReportingType) => {
            expect(Object.values(MultiLegReportingType)).toContain(value);
        };

        validate(MultiLegReportingType.SingleSecurity);
        validate(MultiLegReportingType.IndividualLegOfAMultiLegSecurity);
        validate(MultiLegReportingType.MultiLegSecurity);
    });

    test('should be immutable', () => {
        const ref = MultiLegReportingType;
        expect(() => {
            ref.SingleSecurity = 10;
        }).toThrowError();
    });
});
