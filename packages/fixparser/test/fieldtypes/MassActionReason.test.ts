import { MassActionReason } from '../../src/fieldtypes/MassActionReason';

describe('MassActionReason', () => {
    test('should have the correct values', () => {
        expect(MassActionReason.None).toBe(0);
        expect(MassActionReason.TradingRiskControl).toBe(1);
        expect(MassActionReason.ClearingRiskControl).toBe(2);
        expect(MassActionReason.MarketMakerProtection).toBe(3);
        expect(MassActionReason.StopTrading).toBe(4);
        expect(MassActionReason.EmergencyAction).toBe(5);
        expect(MassActionReason.SessionLossLogout).toBe(6);
        expect(MassActionReason.DuplicateLogin).toBe(7);
        expect(MassActionReason.ProductNotTraded).toBe(8);
        expect(MassActionReason.InstrumentNotTraded).toBe(9);
        expect(MassActionReason.CompleInstrumentDeleted).toBe(10);
        expect(MassActionReason.CircuitBreakerActivated).toBe(11);
        expect(MassActionReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassActionReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassActionReason.None,
            MassActionReason.TradingRiskControl,
            MassActionReason.ClearingRiskControl,
            MassActionReason.MarketMakerProtection,
            MassActionReason.StopTrading,
            MassActionReason.EmergencyAction,
            MassActionReason.SessionLossLogout,
            MassActionReason.DuplicateLogin,
            MassActionReason.ProductNotTraded,
            MassActionReason.InstrumentNotTraded,
            MassActionReason.CompleInstrumentDeleted,
            MassActionReason.CircuitBreakerActivated,
            MassActionReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassActionReason type', () => {
        const validate = (value: MassActionReason) => {
            expect(Object.values(MassActionReason)).toContain(value);
        };

        validate(MassActionReason.None);
        validate(MassActionReason.TradingRiskControl);
        validate(MassActionReason.ClearingRiskControl);
        validate(MassActionReason.MarketMakerProtection);
        validate(MassActionReason.StopTrading);
        validate(MassActionReason.EmergencyAction);
        validate(MassActionReason.SessionLossLogout);
        validate(MassActionReason.DuplicateLogin);
        validate(MassActionReason.ProductNotTraded);
        validate(MassActionReason.InstrumentNotTraded);
        validate(MassActionReason.CompleInstrumentDeleted);
        validate(MassActionReason.CircuitBreakerActivated);
        validate(MassActionReason.Other);
    });

    test('should be immutable', () => {
        const ref = MassActionReason;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
