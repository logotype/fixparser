import { CollInquiryResult } from '../../src/fieldtypes/CollInquiryResult';

describe('CollInquiryResult', () => {
    test('should have the correct values', () => {
        expect(CollInquiryResult.Successful).toBe(0);
        expect(CollInquiryResult.InvalidOrUnknownInstrument).toBe(1);
        expect(CollInquiryResult.InvalidOrUnknownCollateralType).toBe(2);
        expect(CollInquiryResult.InvalidParties).toBe(3);
        expect(CollInquiryResult.InvalidTransportTypeRequested).toBe(4);
        expect(CollInquiryResult.InvalidDestinationRequested).toBe(5);
        expect(CollInquiryResult.NoCollateralFoundForTheTradeSpecified).toBe(6);
        expect(CollInquiryResult.NoCollateralFoundForTheOrderSpecified).toBe(7);
        expect(CollInquiryResult.CollateralInquiryTypeNotSupported).toBe(8);
        expect(CollInquiryResult.UnauthorizedForCollateralInquiry).toBe(9);
        expect(CollInquiryResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollInquiryResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollInquiryResult.Successful,
            CollInquiryResult.InvalidOrUnknownInstrument,
            CollInquiryResult.InvalidOrUnknownCollateralType,
            CollInquiryResult.InvalidParties,
            CollInquiryResult.InvalidTransportTypeRequested,
            CollInquiryResult.InvalidDestinationRequested,
            CollInquiryResult.NoCollateralFoundForTheTradeSpecified,
            CollInquiryResult.NoCollateralFoundForTheOrderSpecified,
            CollInquiryResult.CollateralInquiryTypeNotSupported,
            CollInquiryResult.UnauthorizedForCollateralInquiry,
            CollInquiryResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollInquiryResult type', () => {
        const validate = (value: CollInquiryResult) => {
            expect(Object.values(CollInquiryResult)).toContain(value);
        };

        validate(CollInquiryResult.Successful);
        validate(CollInquiryResult.InvalidOrUnknownInstrument);
        validate(CollInquiryResult.InvalidOrUnknownCollateralType);
        validate(CollInquiryResult.InvalidParties);
        validate(CollInquiryResult.InvalidTransportTypeRequested);
        validate(CollInquiryResult.InvalidDestinationRequested);
        validate(CollInquiryResult.NoCollateralFoundForTheTradeSpecified);
        validate(CollInquiryResult.NoCollateralFoundForTheOrderSpecified);
        validate(CollInquiryResult.CollateralInquiryTypeNotSupported);
        validate(CollInquiryResult.UnauthorizedForCollateralInquiry);
        validate(CollInquiryResult.Other);
    });

    test('should be immutable', () => {
        const ref = CollInquiryResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
