import { TradingSessionID } from '../../src/fieldtypes/TradingSessionID';

describe('TradingSessionID', () => {
    test('should have the correct values', () => {
        expect(TradingSessionID.Day).toBe('1');
        expect(TradingSessionID.HalfDay).toBe('2');
        expect(TradingSessionID.Morning).toBe('3');
        expect(TradingSessionID.Afternoon).toBe('4');
        expect(TradingSessionID.Evening).toBe('5');
        expect(TradingSessionID.AfterHours).toBe('6');
        expect(TradingSessionID.Holiday).toBe('7');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradingSessionID.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradingSessionID.Day,
            TradingSessionID.HalfDay,
            TradingSessionID.Morning,
            TradingSessionID.Afternoon,
            TradingSessionID.Evening,
            TradingSessionID.AfterHours,
            TradingSessionID.Holiday,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TradingSessionID type', () => {
        const validate = (value: TradingSessionID) => {
            expect(Object.values(TradingSessionID)).toContain(value);
        };

        validate(TradingSessionID.Day);
        validate(TradingSessionID.HalfDay);
        validate(TradingSessionID.Morning);
        validate(TradingSessionID.Afternoon);
        validate(TradingSessionID.Evening);
        validate(TradingSessionID.AfterHours);
        validate(TradingSessionID.Holiday);
    });

    test('should be immutable', () => {
        const ref = TradingSessionID;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
