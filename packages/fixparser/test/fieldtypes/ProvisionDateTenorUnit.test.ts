import { ProvisionDateTenorUnit } from '../../src/fieldtypes/ProvisionDateTenorUnit';

describe('ProvisionDateTenorUnit', () => {
    test('should have the correct values', () => {
        expect(ProvisionDateTenorUnit.Day).toBe('D');
        expect(ProvisionDateTenorUnit.Week).toBe('Wk');
        expect(ProvisionDateTenorUnit.Month).toBe('Mo');
        expect(ProvisionDateTenorUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionDateTenorUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionDateTenorUnit.Day,
            ProvisionDateTenorUnit.Week,
            ProvisionDateTenorUnit.Month,
            ProvisionDateTenorUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ProvisionDateTenorUnit type', () => {
        const validate = (value: ProvisionDateTenorUnit) => {
            expect(Object.values(ProvisionDateTenorUnit)).toContain(value);
        };

        validate(ProvisionDateTenorUnit.Day);
        validate(ProvisionDateTenorUnit.Week);
        validate(ProvisionDateTenorUnit.Month);
        validate(ProvisionDateTenorUnit.Year);
    });

    test('should be immutable', () => {
        const ref = ProvisionDateTenorUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
