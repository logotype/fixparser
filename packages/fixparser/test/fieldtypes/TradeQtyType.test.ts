import { TradeQtyType } from '../../src/fieldtypes/TradeQtyType';

describe('TradeQtyType', () => {
    test('should have the correct values', () => {
        expect(TradeQtyType.ClearedQuantity).toBe(0);
        expect(TradeQtyType.LongSideClaimedQuantity).toBe(1);
        expect(TradeQtyType.ShortSideClaimedQuantity).toBe(2);
        expect(TradeQtyType.LongSideRejectedQuantity).toBe(3);
        expect(TradeQtyType.ShortSideRejectedQuantity).toBe(4);
        expect(TradeQtyType.PendingQuantity).toBe(5);
        expect(TradeQtyType.TransactionQuantity).toBe(6);
        expect(TradeQtyType.RemainingQuantity).toBe(7);
        expect(TradeQtyType.PreviousRemainingQuantity).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeQtyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeQtyType.ClearedQuantity,
            TradeQtyType.LongSideClaimedQuantity,
            TradeQtyType.ShortSideClaimedQuantity,
            TradeQtyType.LongSideRejectedQuantity,
            TradeQtyType.ShortSideRejectedQuantity,
            TradeQtyType.PendingQuantity,
            TradeQtyType.TransactionQuantity,
            TradeQtyType.RemainingQuantity,
            TradeQtyType.PreviousRemainingQuantity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeQtyType type', () => {
        const validate = (value: TradeQtyType) => {
            expect(Object.values(TradeQtyType)).toContain(value);
        };

        validate(TradeQtyType.ClearedQuantity);
        validate(TradeQtyType.LongSideClaimedQuantity);
        validate(TradeQtyType.ShortSideClaimedQuantity);
        validate(TradeQtyType.LongSideRejectedQuantity);
        validate(TradeQtyType.ShortSideRejectedQuantity);
        validate(TradeQtyType.PendingQuantity);
        validate(TradeQtyType.TransactionQuantity);
        validate(TradeQtyType.RemainingQuantity);
        validate(TradeQtyType.PreviousRemainingQuantity);
    });

    test('should be immutable', () => {
        const ref = TradeQtyType;
        expect(() => {
            ref.ClearedQuantity = 10;
        }).toThrowError();
    });
});
