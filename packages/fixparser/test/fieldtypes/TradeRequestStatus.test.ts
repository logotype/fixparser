import { TradeRequestStatus } from '../../src/fieldtypes/TradeRequestStatus';

describe('TradeRequestStatus', () => {
    test('should have the correct values', () => {
        expect(TradeRequestStatus.Accepted).toBe(0);
        expect(TradeRequestStatus.Completed).toBe(1);
        expect(TradeRequestStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeRequestStatus.Accepted,
            TradeRequestStatus.Completed,
            TradeRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeRequestStatus type', () => {
        const validate = (value: TradeRequestStatus) => {
            expect(Object.values(TradeRequestStatus)).toContain(value);
        };

        validate(TradeRequestStatus.Accepted);
        validate(TradeRequestStatus.Completed);
        validate(TradeRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TradeRequestStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
