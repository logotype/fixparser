import { VerificationMethod } from '../../src/fieldtypes/VerificationMethod';

describe('VerificationMethod', () => {
    test('should have the correct values', () => {
        expect(VerificationMethod.NonElectronic).toBe(0);
        expect(VerificationMethod.Electronic).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            VerificationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [VerificationMethod.NonElectronic, VerificationMethod.Electronic];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for VerificationMethod type', () => {
        const validate = (value: VerificationMethod) => {
            expect(Object.values(VerificationMethod)).toContain(value);
        };

        validate(VerificationMethod.NonElectronic);
        validate(VerificationMethod.Electronic);
    });

    test('should be immutable', () => {
        const ref = VerificationMethod;
        expect(() => {
            ref.NonElectronic = 10;
        }).toThrowError();
    });
});
