import { ReturnRatePriceType } from '../../src/fieldtypes/ReturnRatePriceType';

describe('ReturnRatePriceType', () => {
    test('should have the correct values', () => {
        expect(ReturnRatePriceType.AbsoluteTerms).toBe(0);
        expect(ReturnRatePriceType.PercentageOfNotional).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRatePriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ReturnRatePriceType.AbsoluteTerms, ReturnRatePriceType.PercentageOfNotional];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRatePriceType type', () => {
        const validate = (value: ReturnRatePriceType) => {
            expect(Object.values(ReturnRatePriceType)).toContain(value);
        };

        validate(ReturnRatePriceType.AbsoluteTerms);
        validate(ReturnRatePriceType.PercentageOfNotional);
    });

    test('should be immutable', () => {
        const ref = ReturnRatePriceType;
        expect(() => {
            ref.AbsoluteTerms = 10;
        }).toThrowError();
    });
});
