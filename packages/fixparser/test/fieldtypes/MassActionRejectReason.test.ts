import { MassActionRejectReason } from '../../src/fieldtypes/MassActionRejectReason';

describe('MassActionRejectReason', () => {
    test('should have the correct values', () => {
        expect(MassActionRejectReason.MassActionNotSupported).toBe(0);
        expect(MassActionRejectReason.InvalidOrUnknownSecurity).toBe(1);
        expect(MassActionRejectReason.InvalidOrUnknownUnderlyingSecurity).toBe(2);
        expect(MassActionRejectReason.InvalidOrUnknownProduct).toBe(3);
        expect(MassActionRejectReason.InvalidOrUnknownCFICode).toBe(4);
        expect(MassActionRejectReason.InvalidOrUnknownSecurityType).toBe(5);
        expect(MassActionRejectReason.InvalidOrUnknownTradingSession).toBe(6);
        expect(MassActionRejectReason.InvalidOrUnknownMarket).toBe(7);
        expect(MassActionRejectReason.InvalidOrUnknownMarketSegment).toBe(8);
        expect(MassActionRejectReason.InvalidOrUnknownSecurityGroup).toBe(9);
        expect(MassActionRejectReason.InvalidOrUnknownSecurityIssuer).toBe(10);
        expect(MassActionRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity).toBe(11);
        expect(MassActionRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassActionRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassActionRejectReason.MassActionNotSupported,
            MassActionRejectReason.InvalidOrUnknownSecurity,
            MassActionRejectReason.InvalidOrUnknownUnderlyingSecurity,
            MassActionRejectReason.InvalidOrUnknownProduct,
            MassActionRejectReason.InvalidOrUnknownCFICode,
            MassActionRejectReason.InvalidOrUnknownSecurityType,
            MassActionRejectReason.InvalidOrUnknownTradingSession,
            MassActionRejectReason.InvalidOrUnknownMarket,
            MassActionRejectReason.InvalidOrUnknownMarketSegment,
            MassActionRejectReason.InvalidOrUnknownSecurityGroup,
            MassActionRejectReason.InvalidOrUnknownSecurityIssuer,
            MassActionRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity,
            MassActionRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassActionRejectReason type', () => {
        const validate = (value: MassActionRejectReason) => {
            expect(Object.values(MassActionRejectReason)).toContain(value);
        };

        validate(MassActionRejectReason.MassActionNotSupported);
        validate(MassActionRejectReason.InvalidOrUnknownSecurity);
        validate(MassActionRejectReason.InvalidOrUnknownUnderlyingSecurity);
        validate(MassActionRejectReason.InvalidOrUnknownProduct);
        validate(MassActionRejectReason.InvalidOrUnknownCFICode);
        validate(MassActionRejectReason.InvalidOrUnknownSecurityType);
        validate(MassActionRejectReason.InvalidOrUnknownTradingSession);
        validate(MassActionRejectReason.InvalidOrUnknownMarket);
        validate(MassActionRejectReason.InvalidOrUnknownMarketSegment);
        validate(MassActionRejectReason.InvalidOrUnknownSecurityGroup);
        validate(MassActionRejectReason.InvalidOrUnknownSecurityIssuer);
        validate(MassActionRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity);
        validate(MassActionRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = MassActionRejectReason;
        expect(() => {
            ref.MassActionNotSupported = 10;
        }).toThrowError();
    });
});
