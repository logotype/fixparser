import { RiskLimitAction } from '../../src/fieldtypes/RiskLimitAction';

describe('RiskLimitAction', () => {
    test('should have the correct values', () => {
        expect(RiskLimitAction.QueueInbound).toBe(0);
        expect(RiskLimitAction.QueueOutbound).toBe(1);
        expect(RiskLimitAction.Reject).toBe(2);
        expect(RiskLimitAction.Disconnect).toBe(3);
        expect(RiskLimitAction.Warning).toBe(4);
        expect(RiskLimitAction.PingCreditCheckWithRevalidation).toBe(5);
        expect(RiskLimitAction.PingCreditCheckNoRevalidation).toBe(6);
        expect(RiskLimitAction.PushCreditCheckWithRevalidation).toBe(7);
        expect(RiskLimitAction.PushCreditCheckNoRevalidation).toBe(8);
        expect(RiskLimitAction.Suspend).toBe(9);
        expect(RiskLimitAction.HaltTrading).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitAction.QueueInbound,
            RiskLimitAction.QueueOutbound,
            RiskLimitAction.Reject,
            RiskLimitAction.Disconnect,
            RiskLimitAction.Warning,
            RiskLimitAction.PingCreditCheckWithRevalidation,
            RiskLimitAction.PingCreditCheckNoRevalidation,
            RiskLimitAction.PushCreditCheckWithRevalidation,
            RiskLimitAction.PushCreditCheckNoRevalidation,
            RiskLimitAction.Suspend,
            RiskLimitAction.HaltTrading,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitAction type', () => {
        const validate = (value: RiskLimitAction) => {
            expect(Object.values(RiskLimitAction)).toContain(value);
        };

        validate(RiskLimitAction.QueueInbound);
        validate(RiskLimitAction.QueueOutbound);
        validate(RiskLimitAction.Reject);
        validate(RiskLimitAction.Disconnect);
        validate(RiskLimitAction.Warning);
        validate(RiskLimitAction.PingCreditCheckWithRevalidation);
        validate(RiskLimitAction.PingCreditCheckNoRevalidation);
        validate(RiskLimitAction.PushCreditCheckWithRevalidation);
        validate(RiskLimitAction.PushCreditCheckNoRevalidation);
        validate(RiskLimitAction.Suspend);
        validate(RiskLimitAction.HaltTrading);
    });

    test('should be immutable', () => {
        const ref = RiskLimitAction;
        expect(() => {
            ref.QueueInbound = 10;
        }).toThrowError();
    });
});
