import { CommType } from '../../src/fieldtypes/CommType';

describe('CommType', () => {
    test('should have the correct values', () => {
        expect(CommType.PerUnit).toBe('1');
        expect(CommType.Percent).toBe('2');
        expect(CommType.Absolute).toBe('3');
        expect(CommType.PercentageWaivedCashDiscount).toBe('4');
        expect(CommType.PercentageWaivedEnhancedUnits).toBe('5');
        expect(CommType.PointsPerBondOrContract).toBe('6');
        expect(CommType.BasisPoints).toBe('7');
        expect(CommType.AmountPerContract).toBe('8');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CommType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CommType.PerUnit,
            CommType.Percent,
            CommType.Absolute,
            CommType.PercentageWaivedCashDiscount,
            CommType.PercentageWaivedEnhancedUnits,
            CommType.PointsPerBondOrContract,
            CommType.BasisPoints,
            CommType.AmountPerContract,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CommType type', () => {
        const validate = (value: CommType) => {
            expect(Object.values(CommType)).toContain(value);
        };

        validate(CommType.PerUnit);
        validate(CommType.Percent);
        validate(CommType.Absolute);
        validate(CommType.PercentageWaivedCashDiscount);
        validate(CommType.PercentageWaivedEnhancedUnits);
        validate(CommType.PointsPerBondOrContract);
        validate(CommType.BasisPoints);
        validate(CommType.AmountPerContract);
    });

    test('should be immutable', () => {
        const ref = CommType;
        expect(() => {
            ref.PerUnit = 10;
        }).toThrowError();
    });
});
