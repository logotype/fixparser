import { SecurityClassificationReason } from '../../src/fieldtypes/SecurityClassificationReason';

describe('SecurityClassificationReason', () => {
    test('should have the correct values', () => {
        expect(SecurityClassificationReason.Fee).toBe(0);
        expect(SecurityClassificationReason.CreditControls).toBe(1);
        expect(SecurityClassificationReason.Margin).toBe(2);
        expect(SecurityClassificationReason.EntitlementOrEligibility).toBe(3);
        expect(SecurityClassificationReason.MarketData).toBe(4);
        expect(SecurityClassificationReason.AccountSelection).toBe(5);
        expect(SecurityClassificationReason.DeliveryProcess).toBe(6);
        expect(SecurityClassificationReason.Sector).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityClassificationReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityClassificationReason.Fee,
            SecurityClassificationReason.CreditControls,
            SecurityClassificationReason.Margin,
            SecurityClassificationReason.EntitlementOrEligibility,
            SecurityClassificationReason.MarketData,
            SecurityClassificationReason.AccountSelection,
            SecurityClassificationReason.DeliveryProcess,
            SecurityClassificationReason.Sector,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityClassificationReason type', () => {
        const validate = (value: SecurityClassificationReason) => {
            expect(Object.values(SecurityClassificationReason)).toContain(value);
        };

        validate(SecurityClassificationReason.Fee);
        validate(SecurityClassificationReason.CreditControls);
        validate(SecurityClassificationReason.Margin);
        validate(SecurityClassificationReason.EntitlementOrEligibility);
        validate(SecurityClassificationReason.MarketData);
        validate(SecurityClassificationReason.AccountSelection);
        validate(SecurityClassificationReason.DeliveryProcess);
        validate(SecurityClassificationReason.Sector);
    });

    test('should be immutable', () => {
        const ref = SecurityClassificationReason;
        expect(() => {
            ref.Fee = 10;
        }).toThrowError();
    });
});
