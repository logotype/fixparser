import { DiscretionOffsetType } from '../../src/fieldtypes/DiscretionOffsetType';

describe('DiscretionOffsetType', () => {
    test('should have the correct values', () => {
        expect(DiscretionOffsetType.Price).toBe(0);
        expect(DiscretionOffsetType.BasisPoints).toBe(1);
        expect(DiscretionOffsetType.Ticks).toBe(2);
        expect(DiscretionOffsetType.PriceTier).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionOffsetType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DiscretionOffsetType.Price,
            DiscretionOffsetType.BasisPoints,
            DiscretionOffsetType.Ticks,
            DiscretionOffsetType.PriceTier,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DiscretionOffsetType type', () => {
        const validate = (value: DiscretionOffsetType) => {
            expect(Object.values(DiscretionOffsetType)).toContain(value);
        };

        validate(DiscretionOffsetType.Price);
        validate(DiscretionOffsetType.BasisPoints);
        validate(DiscretionOffsetType.Ticks);
        validate(DiscretionOffsetType.PriceTier);
    });

    test('should be immutable', () => {
        const ref = DiscretionOffsetType;
        expect(() => {
            ref.Price = 10;
        }).toThrowError();
    });
});
