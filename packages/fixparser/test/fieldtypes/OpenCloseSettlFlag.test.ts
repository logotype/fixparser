import { OpenCloseSettlFlag } from '../../src/fieldtypes/OpenCloseSettlFlag';

describe('OpenCloseSettlFlag', () => {
    test('should have the correct values', () => {
        expect(OpenCloseSettlFlag.DailyOpen).toBe('0');
        expect(OpenCloseSettlFlag.SessionOpen).toBe('1');
        expect(OpenCloseSettlFlag.DeliverySettlementEntry).toBe('2');
        expect(OpenCloseSettlFlag.ExpectedEntry).toBe('3');
        expect(OpenCloseSettlFlag.EntryFromPreviousBusinessDay).toBe('4');
        expect(OpenCloseSettlFlag.TheoreticalPriceValue).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OpenCloseSettlFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OpenCloseSettlFlag.DailyOpen,
            OpenCloseSettlFlag.SessionOpen,
            OpenCloseSettlFlag.DeliverySettlementEntry,
            OpenCloseSettlFlag.ExpectedEntry,
            OpenCloseSettlFlag.EntryFromPreviousBusinessDay,
            OpenCloseSettlFlag.TheoreticalPriceValue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OpenCloseSettlFlag type', () => {
        const validate = (value: OpenCloseSettlFlag) => {
            expect(Object.values(OpenCloseSettlFlag)).toContain(value);
        };

        validate(OpenCloseSettlFlag.DailyOpen);
        validate(OpenCloseSettlFlag.SessionOpen);
        validate(OpenCloseSettlFlag.DeliverySettlementEntry);
        validate(OpenCloseSettlFlag.ExpectedEntry);
        validate(OpenCloseSettlFlag.EntryFromPreviousBusinessDay);
        validate(OpenCloseSettlFlag.TheoreticalPriceValue);
    });

    test('should be immutable', () => {
        const ref = OpenCloseSettlFlag;
        expect(() => {
            ref.DailyOpen = 10;
        }).toThrowError();
    });
});
