import { TradeAllocStatus } from '../../src/fieldtypes/TradeAllocStatus';

describe('TradeAllocStatus', () => {
    test('should have the correct values', () => {
        expect(TradeAllocStatus.PendingClear).toBe(0);
        expect(TradeAllocStatus.Claimed).toBe(1);
        expect(TradeAllocStatus.Cleared).toBe(2);
        expect(TradeAllocStatus.Rejected).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAllocStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeAllocStatus.PendingClear,
            TradeAllocStatus.Claimed,
            TradeAllocStatus.Cleared,
            TradeAllocStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAllocStatus type', () => {
        const validate = (value: TradeAllocStatus) => {
            expect(Object.values(TradeAllocStatus)).toContain(value);
        };

        validate(TradeAllocStatus.PendingClear);
        validate(TradeAllocStatus.Claimed);
        validate(TradeAllocStatus.Cleared);
        validate(TradeAllocStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TradeAllocStatus;
        expect(() => {
            ref.PendingClear = 10;
        }).toThrowError();
    });
});
