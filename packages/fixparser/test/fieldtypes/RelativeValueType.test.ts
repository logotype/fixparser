import { RelativeValueType } from '../../src/fieldtypes/RelativeValueType';

describe('RelativeValueType', () => {
    test('should have the correct values', () => {
        expect(RelativeValueType.ASWSpread).toBe(1);
        expect(RelativeValueType.OIS).toBe(2);
        expect(RelativeValueType.ZSpread).toBe(3);
        expect(RelativeValueType.DiscountMargin).toBe(4);
        expect(RelativeValueType.ISpread).toBe(5);
        expect(RelativeValueType.OAS).toBe(6);
        expect(RelativeValueType.GSpread).toBe(7);
        expect(RelativeValueType.CDSBasis).toBe(8);
        expect(RelativeValueType.CDSInterpolatedBasis).toBe(9);
        expect(RelativeValueType.DV01).toBe(10);
        expect(RelativeValueType.PV01).toBe(11);
        expect(RelativeValueType.CS01).toBe(12);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelativeValueType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RelativeValueType.ASWSpread,
            RelativeValueType.OIS,
            RelativeValueType.ZSpread,
            RelativeValueType.DiscountMargin,
            RelativeValueType.ISpread,
            RelativeValueType.OAS,
            RelativeValueType.GSpread,
            RelativeValueType.CDSBasis,
            RelativeValueType.CDSInterpolatedBasis,
            RelativeValueType.DV01,
            RelativeValueType.PV01,
            RelativeValueType.CS01,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelativeValueType type', () => {
        const validate = (value: RelativeValueType) => {
            expect(Object.values(RelativeValueType)).toContain(value);
        };

        validate(RelativeValueType.ASWSpread);
        validate(RelativeValueType.OIS);
        validate(RelativeValueType.ZSpread);
        validate(RelativeValueType.DiscountMargin);
        validate(RelativeValueType.ISpread);
        validate(RelativeValueType.OAS);
        validate(RelativeValueType.GSpread);
        validate(RelativeValueType.CDSBasis);
        validate(RelativeValueType.CDSInterpolatedBasis);
        validate(RelativeValueType.DV01);
        validate(RelativeValueType.PV01);
        validate(RelativeValueType.CS01);
    });

    test('should be immutable', () => {
        const ref = RelativeValueType;
        expect(() => {
            ref.ASWSpread = 10;
        }).toThrowError();
    });
});
