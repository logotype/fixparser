import { AlgoCertificateReportTransType } from '../../src/fieldtypes/AlgoCertificateReportTransType';

describe('AlgoCertificateReportTransType', () => {
    test('should have the correct values', () => {
        expect(AlgoCertificateReportTransType.New).toBe(0);
        expect(AlgoCertificateReportTransType.Cancel).toBe(1);
        expect(AlgoCertificateReportTransType.Replace).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgoCertificateReportTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AlgoCertificateReportTransType.New,
            AlgoCertificateReportTransType.Cancel,
            AlgoCertificateReportTransType.Replace,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgoCertificateReportTransType type', () => {
        const validate = (value: AlgoCertificateReportTransType) => {
            expect(Object.values(AlgoCertificateReportTransType)).toContain(value);
        };

        validate(AlgoCertificateReportTransType.New);
        validate(AlgoCertificateReportTransType.Cancel);
        validate(AlgoCertificateReportTransType.Replace);
    });

    test('should be immutable', () => {
        const ref = AlgoCertificateReportTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
