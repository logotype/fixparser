import { TradSesStatusRejReason } from '../../src/fieldtypes/TradSesStatusRejReason';

describe('TradSesStatusRejReason', () => {
    test('should have the correct values', () => {
        expect(TradSesStatusRejReason.UnknownOrInvalidTradingSessionID).toBe(1);
        expect(TradSesStatusRejReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesStatusRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradSesStatusRejReason.UnknownOrInvalidTradingSessionID,
            TradSesStatusRejReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesStatusRejReason type', () => {
        const validate = (value: TradSesStatusRejReason) => {
            expect(Object.values(TradSesStatusRejReason)).toContain(value);
        };

        validate(TradSesStatusRejReason.UnknownOrInvalidTradingSessionID);
        validate(TradSesStatusRejReason.Other);
    });

    test('should be immutable', () => {
        const ref = TradSesStatusRejReason;
        expect(() => {
            ref.UnknownOrInvalidTradingSessionID = 10;
        }).toThrowError();
    });
});
