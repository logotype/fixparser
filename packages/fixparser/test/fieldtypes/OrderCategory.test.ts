import { OrderCategory } from '../../src/fieldtypes/OrderCategory';

describe('OrderCategory', () => {
    test('should have the correct values', () => {
        expect(OrderCategory.Order).toBe('1');
        expect(OrderCategory.Quote).toBe('2');
        expect(OrderCategory.PrivatelyNegotiatedTrade).toBe('3');
        expect(OrderCategory.MultilegOrder).toBe('4');
        expect(OrderCategory.LinkedOrder).toBe('5');
        expect(OrderCategory.QuoteRequest).toBe('6');
        expect(OrderCategory.ImpliedOrder).toBe('7');
        expect(OrderCategory.CrossOrder).toBe('8');
        expect(OrderCategory.StreamingPrice).toBe('9');
        expect(OrderCategory.InternalCrossOrder).toBe('A');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderCategory.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderCategory.Order,
            OrderCategory.Quote,
            OrderCategory.PrivatelyNegotiatedTrade,
            OrderCategory.MultilegOrder,
            OrderCategory.LinkedOrder,
            OrderCategory.QuoteRequest,
            OrderCategory.ImpliedOrder,
            OrderCategory.CrossOrder,
            OrderCategory.StreamingPrice,
            OrderCategory.InternalCrossOrder,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrderCategory type', () => {
        const validate = (value: OrderCategory) => {
            expect(Object.values(OrderCategory)).toContain(value);
        };

        validate(OrderCategory.Order);
        validate(OrderCategory.Quote);
        validate(OrderCategory.PrivatelyNegotiatedTrade);
        validate(OrderCategory.MultilegOrder);
        validate(OrderCategory.LinkedOrder);
        validate(OrderCategory.QuoteRequest);
        validate(OrderCategory.ImpliedOrder);
        validate(OrderCategory.CrossOrder);
        validate(OrderCategory.StreamingPrice);
        validate(OrderCategory.InternalCrossOrder);
    });

    test('should be immutable', () => {
        const ref = OrderCategory;
        expect(() => {
            ref.Order = 10;
        }).toThrowError();
    });
});
