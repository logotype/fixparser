import { DeliveryStreamDeliveryPointSource } from '../../src/fieldtypes/DeliveryStreamDeliveryPointSource';

describe('DeliveryStreamDeliveryPointSource', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamDeliveryPointSource.Proprietary).toBe(0);
        expect(DeliveryStreamDeliveryPointSource.EIC).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamDeliveryPointSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryStreamDeliveryPointSource.Proprietary, DeliveryStreamDeliveryPointSource.EIC];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamDeliveryPointSource type', () => {
        const validate = (value: DeliveryStreamDeliveryPointSource) => {
            expect(Object.values(DeliveryStreamDeliveryPointSource)).toContain(value);
        };

        validate(DeliveryStreamDeliveryPointSource.Proprietary);
        validate(DeliveryStreamDeliveryPointSource.EIC);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamDeliveryPointSource;
        expect(() => {
            ref.Proprietary = 10;
        }).toThrowError();
    });
});
