import { PartyActionRejectReason } from '../../src/fieldtypes/PartyActionRejectReason';

describe('PartyActionRejectReason', () => {
    test('should have the correct values', () => {
        expect(PartyActionRejectReason.InvalidParty).toBe(0);
        expect(PartyActionRejectReason.UnkReqParty).toBe(1);
        expect(PartyActionRejectReason.NotAuthorized).toBe(98);
        expect(PartyActionRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyActionRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyActionRejectReason.InvalidParty,
            PartyActionRejectReason.UnkReqParty,
            PartyActionRejectReason.NotAuthorized,
            PartyActionRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyActionRejectReason type', () => {
        const validate = (value: PartyActionRejectReason) => {
            expect(Object.values(PartyActionRejectReason)).toContain(value);
        };

        validate(PartyActionRejectReason.InvalidParty);
        validate(PartyActionRejectReason.UnkReqParty);
        validate(PartyActionRejectReason.NotAuthorized);
        validate(PartyActionRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = PartyActionRejectReason;
        expect(() => {
            ref.InvalidParty = 10;
        }).toThrowError();
    });
});
