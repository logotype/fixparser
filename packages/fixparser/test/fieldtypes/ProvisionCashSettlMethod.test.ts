import { ProvisionCashSettlMethod } from '../../src/fieldtypes/ProvisionCashSettlMethod';

describe('ProvisionCashSettlMethod', () => {
    test('should have the correct values', () => {
        expect(ProvisionCashSettlMethod.CashPrice).toBe(0);
        expect(ProvisionCashSettlMethod.CashPriceAlternate).toBe(1);
        expect(ProvisionCashSettlMethod.ParYieldCurveAdjusted).toBe(2);
        expect(ProvisionCashSettlMethod.ZeroCouponYieldCurveAdjusted).toBe(3);
        expect(ProvisionCashSettlMethod.ParYieldCurveUnadjusted).toBe(4);
        expect(ProvisionCashSettlMethod.CrossCurrency).toBe(5);
        expect(ProvisionCashSettlMethod.CollateralizedPrice).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionCashSettlMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionCashSettlMethod.CashPrice,
            ProvisionCashSettlMethod.CashPriceAlternate,
            ProvisionCashSettlMethod.ParYieldCurveAdjusted,
            ProvisionCashSettlMethod.ZeroCouponYieldCurveAdjusted,
            ProvisionCashSettlMethod.ParYieldCurveUnadjusted,
            ProvisionCashSettlMethod.CrossCurrency,
            ProvisionCashSettlMethod.CollateralizedPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionCashSettlMethod type', () => {
        const validate = (value: ProvisionCashSettlMethod) => {
            expect(Object.values(ProvisionCashSettlMethod)).toContain(value);
        };

        validate(ProvisionCashSettlMethod.CashPrice);
        validate(ProvisionCashSettlMethod.CashPriceAlternate);
        validate(ProvisionCashSettlMethod.ParYieldCurveAdjusted);
        validate(ProvisionCashSettlMethod.ZeroCouponYieldCurveAdjusted);
        validate(ProvisionCashSettlMethod.ParYieldCurveUnadjusted);
        validate(ProvisionCashSettlMethod.CrossCurrency);
        validate(ProvisionCashSettlMethod.CollateralizedPrice);
    });

    test('should be immutable', () => {
        const ref = ProvisionCashSettlMethod;
        expect(() => {
            ref.CashPrice = 10;
        }).toThrowError();
    });
});
