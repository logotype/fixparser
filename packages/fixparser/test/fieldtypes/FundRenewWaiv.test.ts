import { FundRenewWaiv } from '../../src/fieldtypes/FundRenewWaiv';

describe('FundRenewWaiv', () => {
    test('should have the correct values', () => {
        expect(FundRenewWaiv.No).toBe('N');
        expect(FundRenewWaiv.Yes).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            FundRenewWaiv.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [FundRenewWaiv.No, FundRenewWaiv.Yes];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for FundRenewWaiv type', () => {
        const validate = (value: FundRenewWaiv) => {
            expect(Object.values(FundRenewWaiv)).toContain(value);
        };

        validate(FundRenewWaiv.No);
        validate(FundRenewWaiv.Yes);
    });

    test('should be immutable', () => {
        const ref = FundRenewWaiv;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
