import { PosQtyStatus } from '../../src/fieldtypes/PosQtyStatus';

describe('PosQtyStatus', () => {
    test('should have the correct values', () => {
        expect(PosQtyStatus.Submitted).toBe(0);
        expect(PosQtyStatus.Accepted).toBe(1);
        expect(PosQtyStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosQtyStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PosQtyStatus.Submitted, PosQtyStatus.Accepted, PosQtyStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosQtyStatus type', () => {
        const validate = (value: PosQtyStatus) => {
            expect(Object.values(PosQtyStatus)).toContain(value);
        };

        validate(PosQtyStatus.Submitted);
        validate(PosQtyStatus.Accepted);
        validate(PosQtyStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = PosQtyStatus;
        expect(() => {
            ref.Submitted = 10;
        }).toThrowError();
    });
});
