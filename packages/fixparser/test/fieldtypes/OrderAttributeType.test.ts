import { OrderAttributeType } from '../../src/fieldtypes/OrderAttributeType';

describe('OrderAttributeType', () => {
    test('should have the correct values', () => {
        expect(OrderAttributeType.AggregatedOrder).toBe(0);
        expect(OrderAttributeType.PendingAllocation).toBe(1);
        expect(OrderAttributeType.LiquidityProvisionActivityOrder).toBe(2);
        expect(OrderAttributeType.RiskReductionOrder).toBe(3);
        expect(OrderAttributeType.AlgorithmicOrder).toBe(4);
        expect(OrderAttributeType.SystematicInternaliserOrder).toBe(5);
        expect(OrderAttributeType.AllExecutionsSubmittedToAPA).toBe(6);
        expect(OrderAttributeType.OrderExecutionInstructedByClient).toBe(7);
        expect(OrderAttributeType.LargeInScale).toBe(8);
        expect(OrderAttributeType.Hidden).toBe(9);
        expect(OrderAttributeType.SubjectToEUSTO).toBe(10);
        expect(OrderAttributeType.SubjectToUKSTO).toBe(11);
        expect(OrderAttributeType.RepresentativeOrder).toBe(12);
        expect(OrderAttributeType.LinkageType).toBe(13);
        expect(OrderAttributeType.ExemptFromSTO).toBe(14);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderAttributeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderAttributeType.AggregatedOrder,
            OrderAttributeType.PendingAllocation,
            OrderAttributeType.LiquidityProvisionActivityOrder,
            OrderAttributeType.RiskReductionOrder,
            OrderAttributeType.AlgorithmicOrder,
            OrderAttributeType.SystematicInternaliserOrder,
            OrderAttributeType.AllExecutionsSubmittedToAPA,
            OrderAttributeType.OrderExecutionInstructedByClient,
            OrderAttributeType.LargeInScale,
            OrderAttributeType.Hidden,
            OrderAttributeType.SubjectToEUSTO,
            OrderAttributeType.SubjectToUKSTO,
            OrderAttributeType.RepresentativeOrder,
            OrderAttributeType.LinkageType,
            OrderAttributeType.ExemptFromSTO,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderAttributeType type', () => {
        const validate = (value: OrderAttributeType) => {
            expect(Object.values(OrderAttributeType)).toContain(value);
        };

        validate(OrderAttributeType.AggregatedOrder);
        validate(OrderAttributeType.PendingAllocation);
        validate(OrderAttributeType.LiquidityProvisionActivityOrder);
        validate(OrderAttributeType.RiskReductionOrder);
        validate(OrderAttributeType.AlgorithmicOrder);
        validate(OrderAttributeType.SystematicInternaliserOrder);
        validate(OrderAttributeType.AllExecutionsSubmittedToAPA);
        validate(OrderAttributeType.OrderExecutionInstructedByClient);
        validate(OrderAttributeType.LargeInScale);
        validate(OrderAttributeType.Hidden);
        validate(OrderAttributeType.SubjectToEUSTO);
        validate(OrderAttributeType.SubjectToUKSTO);
        validate(OrderAttributeType.RepresentativeOrder);
        validate(OrderAttributeType.LinkageType);
        validate(OrderAttributeType.ExemptFromSTO);
    });

    test('should be immutable', () => {
        const ref = OrderAttributeType;
        expect(() => {
            ref.AggregatedOrder = 10;
        }).toThrowError();
    });
});
