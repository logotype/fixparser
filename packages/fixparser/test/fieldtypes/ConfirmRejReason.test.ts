import { ConfirmRejReason } from '../../src/fieldtypes/ConfirmRejReason';

describe('ConfirmRejReason', () => {
    test('should have the correct values', () => {
        expect(ConfirmRejReason.MismatchedAccount).toBe(1);
        expect(ConfirmRejReason.MissingSettlementInstructions).toBe(2);
        expect(ConfirmRejReason.UnknownOrMissingIndividualAllocId).toBe(3);
        expect(ConfirmRejReason.TransactionNotRecognized).toBe(4);
        expect(ConfirmRejReason.DuplicateTransaction).toBe(5);
        expect(ConfirmRejReason.IncorrectOrMissingInstrument).toBe(6);
        expect(ConfirmRejReason.IncorrectOrMissingPrice).toBe(7);
        expect(ConfirmRejReason.IncorrectOrMissingCommission).toBe(8);
        expect(ConfirmRejReason.IncorrectOrMissingSettlDate).toBe(9);
        expect(ConfirmRejReason.IncorrectOrMissingFundIDOrFundName).toBe(10);
        expect(ConfirmRejReason.IncorrectOrMissingQuantity).toBe(11);
        expect(ConfirmRejReason.IncorrectOrMissingFees).toBe(12);
        expect(ConfirmRejReason.IncorrectOrMissingTax).toBe(13);
        expect(ConfirmRejReason.IncorrectOrMissingParty).toBe(14);
        expect(ConfirmRejReason.IncorrectOrMissingSide).toBe(15);
        expect(ConfirmRejReason.IncorrectOrMissingNetMoney).toBe(16);
        expect(ConfirmRejReason.IncorrectOrMissingTradeDate).toBe(17);
        expect(ConfirmRejReason.IncorrectOrMissingSettlCcyInstructions).toBe(18);
        expect(ConfirmRejReason.IncorrectOrMissingCapacity).toBe(19);
        expect(ConfirmRejReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ConfirmRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ConfirmRejReason.MismatchedAccount,
            ConfirmRejReason.MissingSettlementInstructions,
            ConfirmRejReason.UnknownOrMissingIndividualAllocId,
            ConfirmRejReason.TransactionNotRecognized,
            ConfirmRejReason.DuplicateTransaction,
            ConfirmRejReason.IncorrectOrMissingInstrument,
            ConfirmRejReason.IncorrectOrMissingPrice,
            ConfirmRejReason.IncorrectOrMissingCommission,
            ConfirmRejReason.IncorrectOrMissingSettlDate,
            ConfirmRejReason.IncorrectOrMissingFundIDOrFundName,
            ConfirmRejReason.IncorrectOrMissingQuantity,
            ConfirmRejReason.IncorrectOrMissingFees,
            ConfirmRejReason.IncorrectOrMissingTax,
            ConfirmRejReason.IncorrectOrMissingParty,
            ConfirmRejReason.IncorrectOrMissingSide,
            ConfirmRejReason.IncorrectOrMissingNetMoney,
            ConfirmRejReason.IncorrectOrMissingTradeDate,
            ConfirmRejReason.IncorrectOrMissingSettlCcyInstructions,
            ConfirmRejReason.IncorrectOrMissingCapacity,
            ConfirmRejReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ConfirmRejReason type', () => {
        const validate = (value: ConfirmRejReason) => {
            expect(Object.values(ConfirmRejReason)).toContain(value);
        };

        validate(ConfirmRejReason.MismatchedAccount);
        validate(ConfirmRejReason.MissingSettlementInstructions);
        validate(ConfirmRejReason.UnknownOrMissingIndividualAllocId);
        validate(ConfirmRejReason.TransactionNotRecognized);
        validate(ConfirmRejReason.DuplicateTransaction);
        validate(ConfirmRejReason.IncorrectOrMissingInstrument);
        validate(ConfirmRejReason.IncorrectOrMissingPrice);
        validate(ConfirmRejReason.IncorrectOrMissingCommission);
        validate(ConfirmRejReason.IncorrectOrMissingSettlDate);
        validate(ConfirmRejReason.IncorrectOrMissingFundIDOrFundName);
        validate(ConfirmRejReason.IncorrectOrMissingQuantity);
        validate(ConfirmRejReason.IncorrectOrMissingFees);
        validate(ConfirmRejReason.IncorrectOrMissingTax);
        validate(ConfirmRejReason.IncorrectOrMissingParty);
        validate(ConfirmRejReason.IncorrectOrMissingSide);
        validate(ConfirmRejReason.IncorrectOrMissingNetMoney);
        validate(ConfirmRejReason.IncorrectOrMissingTradeDate);
        validate(ConfirmRejReason.IncorrectOrMissingSettlCcyInstructions);
        validate(ConfirmRejReason.IncorrectOrMissingCapacity);
        validate(ConfirmRejReason.Other);
    });

    test('should be immutable', () => {
        const ref = ConfirmRejReason;
        expect(() => {
            ref.MismatchedAccount = 10;
        }).toThrowError();
    });
});
