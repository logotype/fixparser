import { AlgoCertificateRequestTransType } from '../../src/fieldtypes/AlgoCertificateRequestTransType';

describe('AlgoCertificateRequestTransType', () => {
    test('should have the correct values', () => {
        expect(AlgoCertificateRequestTransType.New).toBe(0);
        expect(AlgoCertificateRequestTransType.Cancel).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgoCertificateRequestTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AlgoCertificateRequestTransType.New, AlgoCertificateRequestTransType.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgoCertificateRequestTransType type', () => {
        const validate = (value: AlgoCertificateRequestTransType) => {
            expect(Object.values(AlgoCertificateRequestTransType)).toContain(value);
        };

        validate(AlgoCertificateRequestTransType.New);
        validate(AlgoCertificateRequestTransType.Cancel);
    });

    test('should be immutable', () => {
        const ref = AlgoCertificateRequestTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
