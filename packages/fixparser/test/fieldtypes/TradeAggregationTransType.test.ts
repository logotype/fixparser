import { TradeAggregationTransType } from '../../src/fieldtypes/TradeAggregationTransType';

describe('TradeAggregationTransType', () => {
    test('should have the correct values', () => {
        expect(TradeAggregationTransType.New).toBe(0);
        expect(TradeAggregationTransType.Cancel).toBe(1);
        expect(TradeAggregationTransType.Replace).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAggregationTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeAggregationTransType.New,
            TradeAggregationTransType.Cancel,
            TradeAggregationTransType.Replace,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAggregationTransType type', () => {
        const validate = (value: TradeAggregationTransType) => {
            expect(Object.values(TradeAggregationTransType)).toContain(value);
        };

        validate(TradeAggregationTransType.New);
        validate(TradeAggregationTransType.Cancel);
        validate(TradeAggregationTransType.Replace);
    });

    test('should be immutable', () => {
        const ref = TradeAggregationTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
