import { SettlDeliveryType } from '../../src/fieldtypes/SettlDeliveryType';

describe('SettlDeliveryType', () => {
    test('should have the correct values', () => {
        expect(SettlDeliveryType.Versus).toBe(0);
        expect(SettlDeliveryType.Free).toBe(1);
        expect(SettlDeliveryType.TriParty).toBe(2);
        expect(SettlDeliveryType.HoldInCustody).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlDeliveryType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlDeliveryType.Versus,
            SettlDeliveryType.Free,
            SettlDeliveryType.TriParty,
            SettlDeliveryType.HoldInCustody,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlDeliveryType type', () => {
        const validate = (value: SettlDeliveryType) => {
            expect(Object.values(SettlDeliveryType)).toContain(value);
        };

        validate(SettlDeliveryType.Versus);
        validate(SettlDeliveryType.Free);
        validate(SettlDeliveryType.TriParty);
        validate(SettlDeliveryType.HoldInCustody);
    });

    test('should be immutable', () => {
        const ref = SettlDeliveryType;
        expect(() => {
            ref.Versus = 10;
        }).toThrowError();
    });
});
