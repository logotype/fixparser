import { PaymentStreamRateTreatment } from '../../src/fieldtypes/PaymentStreamRateTreatment';

describe('PaymentStreamRateTreatment', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRateTreatment.BondEquivalentYield).toBe(0);
        expect(PaymentStreamRateTreatment.MoneyMarketYield).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRateTreatment.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamRateTreatment.BondEquivalentYield,
            PaymentStreamRateTreatment.MoneyMarketYield,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamRateTreatment type', () => {
        const validate = (value: PaymentStreamRateTreatment) => {
            expect(Object.values(PaymentStreamRateTreatment)).toContain(value);
        };

        validate(PaymentStreamRateTreatment.BondEquivalentYield);
        validate(PaymentStreamRateTreatment.MoneyMarketYield);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRateTreatment;
        expect(() => {
            ref.BondEquivalentYield = 10;
        }).toThrowError();
    });
});
