import { MatchStatus } from '../../src/fieldtypes/MatchStatus';

describe('MatchStatus', () => {
    test('should have the correct values', () => {
        expect(MatchStatus.Compared).toBe('0');
        expect(MatchStatus.Uncompared).toBe('1');
        expect(MatchStatus.AdvisoryOrAlert).toBe('2');
        expect(MatchStatus.Mismatched).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MatchStatus.Compared,
            MatchStatus.Uncompared,
            MatchStatus.AdvisoryOrAlert,
            MatchStatus.Mismatched,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MatchStatus type', () => {
        const validate = (value: MatchStatus) => {
            expect(Object.values(MatchStatus)).toContain(value);
        };

        validate(MatchStatus.Compared);
        validate(MatchStatus.Uncompared);
        validate(MatchStatus.AdvisoryOrAlert);
        validate(MatchStatus.Mismatched);
    });

    test('should be immutable', () => {
        const ref = MatchStatus;
        expect(() => {
            ref.Compared = 10;
        }).toThrowError();
    });
});
