import { BookingUnit } from '../../src/fieldtypes/BookingUnit';

describe('BookingUnit', () => {
    test('should have the correct values', () => {
        expect(BookingUnit.EachPartialExecutionIsABookableUnit).toBe('0');
        expect(BookingUnit.AggregatePartialExecutionsOnThisOrder).toBe('1');
        expect(BookingUnit.AggregateExecutionsForThisSymbol).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BookingUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BookingUnit.EachPartialExecutionIsABookableUnit,
            BookingUnit.AggregatePartialExecutionsOnThisOrder,
            BookingUnit.AggregateExecutionsForThisSymbol,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BookingUnit type', () => {
        const validate = (value: BookingUnit) => {
            expect(Object.values(BookingUnit)).toContain(value);
        };

        validate(BookingUnit.EachPartialExecutionIsABookableUnit);
        validate(BookingUnit.AggregatePartialExecutionsOnThisOrder);
        validate(BookingUnit.AggregateExecutionsForThisSymbol);
    });

    test('should be immutable', () => {
        const ref = BookingUnit;
        expect(() => {
            ref.EachPartialExecutionIsABookableUnit = 10;
        }).toThrowError();
    });
});
