import { SettlObligTransType } from '../../src/fieldtypes/SettlObligTransType';

describe('SettlObligTransType', () => {
    test('should have the correct values', () => {
        expect(SettlObligTransType.Cancel).toBe('C');
        expect(SettlObligTransType.New).toBe('N');
        expect(SettlObligTransType.Replace).toBe('R');
        expect(SettlObligTransType.Restate).toBe('T');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlObligTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlObligTransType.Cancel,
            SettlObligTransType.New,
            SettlObligTransType.Replace,
            SettlObligTransType.Restate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlObligTransType type', () => {
        const validate = (value: SettlObligTransType) => {
            expect(Object.values(SettlObligTransType)).toContain(value);
        };

        validate(SettlObligTransType.Cancel);
        validate(SettlObligTransType.New);
        validate(SettlObligTransType.Replace);
        validate(SettlObligTransType.Restate);
    });

    test('should be immutable', () => {
        const ref = SettlObligTransType;
        expect(() => {
            ref.Cancel = 10;
        }).toThrowError();
    });
});
