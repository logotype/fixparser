import { TriggerScope } from '../../src/fieldtypes/TriggerScope';

describe('TriggerScope', () => {
    test('should have the correct values', () => {
        expect(TriggerScope.ThisOrder).toBe(0);
        expect(TriggerScope.OtherOrder).toBe(1);
        expect(TriggerScope.AllOtherOrdersForGivenSecurity).toBe(2);
        expect(TriggerScope.AllOtherOrdersForGivenSecurityAndPrice).toBe(3);
        expect(TriggerScope.AllOtherOrdersForGivenSecurityAndSide).toBe(4);
        expect(TriggerScope.AllOtherOrdersForGivenSecurityPriceAndSide).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TriggerScope.ThisOrder,
            TriggerScope.OtherOrder,
            TriggerScope.AllOtherOrdersForGivenSecurity,
            TriggerScope.AllOtherOrdersForGivenSecurityAndPrice,
            TriggerScope.AllOtherOrdersForGivenSecurityAndSide,
            TriggerScope.AllOtherOrdersForGivenSecurityPriceAndSide,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TriggerScope type', () => {
        const validate = (value: TriggerScope) => {
            expect(Object.values(TriggerScope)).toContain(value);
        };

        validate(TriggerScope.ThisOrder);
        validate(TriggerScope.OtherOrder);
        validate(TriggerScope.AllOtherOrdersForGivenSecurity);
        validate(TriggerScope.AllOtherOrdersForGivenSecurityAndPrice);
        validate(TriggerScope.AllOtherOrdersForGivenSecurityAndSide);
        validate(TriggerScope.AllOtherOrdersForGivenSecurityPriceAndSide);
    });

    test('should be immutable', () => {
        const ref = TriggerScope;
        expect(() => {
            ref.ThisOrder = 10;
        }).toThrowError();
    });
});
