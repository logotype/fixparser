import { RateSource } from '../../src/fieldtypes/RateSource';

describe('RateSource', () => {
    test('should have the correct values', () => {
        expect(RateSource.Bloomberg).toBe(0);
        expect(RateSource.Reuters).toBe(1);
        expect(RateSource.Telerate).toBe(2);
        expect(RateSource.ISDARateOption).toBe(3);
        expect(RateSource.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RateSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RateSource.Bloomberg,
            RateSource.Reuters,
            RateSource.Telerate,
            RateSource.ISDARateOption,
            RateSource.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RateSource type', () => {
        const validate = (value: RateSource) => {
            expect(Object.values(RateSource)).toContain(value);
        };

        validate(RateSource.Bloomberg);
        validate(RateSource.Reuters);
        validate(RateSource.Telerate);
        validate(RateSource.ISDARateOption);
        validate(RateSource.Other);
    });

    test('should be immutable', () => {
        const ref = RateSource;
        expect(() => {
            ref.Bloomberg = 10;
        }).toThrowError();
    });
});
