import { PriceQuoteMethod } from '../../src/fieldtypes/PriceQuoteMethod';

describe('PriceQuoteMethod', () => {
    test('should have the correct values', () => {
        expect(PriceQuoteMethod.Standard).toBe('STD');
        expect(PriceQuoteMethod.Index).toBe('INX');
        expect(PriceQuoteMethod.InterestRateIndex).toBe('INT');
        expect(PriceQuoteMethod.PercentOfPar).toBe('PCTPAR');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceQuoteMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PriceQuoteMethod.Standard,
            PriceQuoteMethod.Index,
            PriceQuoteMethod.InterestRateIndex,
            PriceQuoteMethod.PercentOfPar,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PriceQuoteMethod type', () => {
        const validate = (value: PriceQuoteMethod) => {
            expect(Object.values(PriceQuoteMethod)).toContain(value);
        };

        validate(PriceQuoteMethod.Standard);
        validate(PriceQuoteMethod.Index);
        validate(PriceQuoteMethod.InterestRateIndex);
        validate(PriceQuoteMethod.PercentOfPar);
    });

    test('should be immutable', () => {
        const ref = PriceQuoteMethod;
        expect(() => {
            ref.Standard = 10;
        }).toThrowError();
    });
});
