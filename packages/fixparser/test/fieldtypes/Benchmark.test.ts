import { Benchmark } from '../../src/fieldtypes/Benchmark';

describe('Benchmark', () => {
    test('should have the correct values', () => {
        expect(Benchmark.CURVE).toBe('1');
        expect(Benchmark.FiveYR).toBe('2');
        expect(Benchmark.OLD5).toBe('3');
        expect(Benchmark.TenYR).toBe('4');
        expect(Benchmark.OLD10).toBe('5');
        expect(Benchmark.ThirtyYR).toBe('6');
        expect(Benchmark.OLD30).toBe('7');
        expect(Benchmark.ThreeMOLIBOR).toBe('8');
        expect(Benchmark.SixMOLIBOR).toBe('9');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Benchmark.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Benchmark.CURVE,
            Benchmark.FiveYR,
            Benchmark.OLD5,
            Benchmark.TenYR,
            Benchmark.OLD10,
            Benchmark.ThirtyYR,
            Benchmark.OLD30,
            Benchmark.ThreeMOLIBOR,
            Benchmark.SixMOLIBOR,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Benchmark type', () => {
        const validate = (value: Benchmark) => {
            expect(Object.values(Benchmark)).toContain(value);
        };

        validate(Benchmark.CURVE);
        validate(Benchmark.FiveYR);
        validate(Benchmark.OLD5);
        validate(Benchmark.TenYR);
        validate(Benchmark.OLD10);
        validate(Benchmark.ThirtyYR);
        validate(Benchmark.OLD30);
        validate(Benchmark.ThreeMOLIBOR);
        validate(Benchmark.SixMOLIBOR);
    });

    test('should be immutable', () => {
        const ref = Benchmark;
        expect(() => {
            ref.CURVE = 10;
        }).toThrowError();
    });
});
