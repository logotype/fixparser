import { PosType } from '../../src/fieldtypes/PosType';

describe('PosType', () => {
    test('should have the correct values', () => {
        expect(PosType.AllocationTradeQty).toBe('ALC');
        expect(PosType.OptionAssignment).toBe('AS');
        expect(PosType.AsOfTradeQty).toBe('ASF');
        expect(PosType.DeliveryQty).toBe('DLV');
        expect(PosType.ElectronicTradeQty).toBe('ETR');
        expect(PosType.OptionExerciseQty).toBe('EX');
        expect(PosType.EndOfDayQty).toBe('FIN');
        expect(PosType.IntraSpreadQty).toBe('IAS');
        expect(PosType.InterSpreadQty).toBe('IES');
        expect(PosType.AdjustmentQty).toBe('PA');
        expect(PosType.PitTradeQty).toBe('PIT');
        expect(PosType.StartOfDayQty).toBe('SOD');
        expect(PosType.IntegralSplit).toBe('SPL');
        expect(PosType.TransactionFromAssignment).toBe('TA');
        expect(PosType.TotalTransactionQty).toBe('TOT');
        expect(PosType.TransactionQuantity).toBe('TQ');
        expect(PosType.TransferTradeQty).toBe('TRF');
        expect(PosType.TransactionFromExercise).toBe('TX');
        expect(PosType.CrossMarginQty).toBe('XM');
        expect(PosType.ReceiveQuantity).toBe('RCV');
        expect(PosType.CorporateActionAdjustment).toBe('CAA');
        expect(PosType.DeliveryNoticeQty).toBe('DN');
        expect(PosType.ExchangeForPhysicalQty).toBe('EP');
        expect(PosType.PrivatelyNegotiatedTradeQty).toBe('PNTN');
        expect(PosType.NetDeltaQty).toBe('DLT');
        expect(PosType.CreditEventAdjustment).toBe('CEA');
        expect(PosType.SuccessionEventAdjustment).toBe('SEA');
        expect(PosType.NetQty).toBe('NET');
        expect(PosType.GrossQty).toBe('GRS');
        expect(PosType.IntradayQty).toBe('ITD');
        expect(PosType.GrossLongNonDeltaAdjustedSwaptionPosition).toBe('NDAS');
        expect(PosType.LongDeltaAdjustedPairedSwaptionPosition).toBe('DAS');
        expect(PosType.ExpiringQuantity).toBe('EXP');
        expect(PosType.QuantityNotExercised).toBe('UNEX');
        expect(PosType.RequestedExerciseQuantity).toBe('REQ');
        expect(PosType.CashFuturesEquivalentQuantity).toBe('CFE');
        expect(PosType.LoanOrBorrowedQuantity).toBe('SECLN');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosType.AllocationTradeQty,
            PosType.OptionAssignment,
            PosType.AsOfTradeQty,
            PosType.DeliveryQty,
            PosType.ElectronicTradeQty,
            PosType.OptionExerciseQty,
            PosType.EndOfDayQty,
            PosType.IntraSpreadQty,
            PosType.InterSpreadQty,
            PosType.AdjustmentQty,
            PosType.PitTradeQty,
            PosType.StartOfDayQty,
            PosType.IntegralSplit,
            PosType.TransactionFromAssignment,
            PosType.TotalTransactionQty,
            PosType.TransactionQuantity,
            PosType.TransferTradeQty,
            PosType.TransactionFromExercise,
            PosType.CrossMarginQty,
            PosType.ReceiveQuantity,
            PosType.CorporateActionAdjustment,
            PosType.DeliveryNoticeQty,
            PosType.ExchangeForPhysicalQty,
            PosType.PrivatelyNegotiatedTradeQty,
            PosType.NetDeltaQty,
            PosType.CreditEventAdjustment,
            PosType.SuccessionEventAdjustment,
            PosType.NetQty,
            PosType.GrossQty,
            PosType.IntradayQty,
            PosType.GrossLongNonDeltaAdjustedSwaptionPosition,
            PosType.LongDeltaAdjustedPairedSwaptionPosition,
            PosType.ExpiringQuantity,
            PosType.QuantityNotExercised,
            PosType.RequestedExerciseQuantity,
            PosType.CashFuturesEquivalentQuantity,
            PosType.LoanOrBorrowedQuantity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PosType type', () => {
        const validate = (value: PosType) => {
            expect(Object.values(PosType)).toContain(value);
        };

        validate(PosType.AllocationTradeQty);
        validate(PosType.OptionAssignment);
        validate(PosType.AsOfTradeQty);
        validate(PosType.DeliveryQty);
        validate(PosType.ElectronicTradeQty);
        validate(PosType.OptionExerciseQty);
        validate(PosType.EndOfDayQty);
        validate(PosType.IntraSpreadQty);
        validate(PosType.InterSpreadQty);
        validate(PosType.AdjustmentQty);
        validate(PosType.PitTradeQty);
        validate(PosType.StartOfDayQty);
        validate(PosType.IntegralSplit);
        validate(PosType.TransactionFromAssignment);
        validate(PosType.TotalTransactionQty);
        validate(PosType.TransactionQuantity);
        validate(PosType.TransferTradeQty);
        validate(PosType.TransactionFromExercise);
        validate(PosType.CrossMarginQty);
        validate(PosType.ReceiveQuantity);
        validate(PosType.CorporateActionAdjustment);
        validate(PosType.DeliveryNoticeQty);
        validate(PosType.ExchangeForPhysicalQty);
        validate(PosType.PrivatelyNegotiatedTradeQty);
        validate(PosType.NetDeltaQty);
        validate(PosType.CreditEventAdjustment);
        validate(PosType.SuccessionEventAdjustment);
        validate(PosType.NetQty);
        validate(PosType.GrossQty);
        validate(PosType.IntradayQty);
        validate(PosType.GrossLongNonDeltaAdjustedSwaptionPosition);
        validate(PosType.LongDeltaAdjustedPairedSwaptionPosition);
        validate(PosType.ExpiringQuantity);
        validate(PosType.QuantityNotExercised);
        validate(PosType.RequestedExerciseQuantity);
        validate(PosType.CashFuturesEquivalentQuantity);
        validate(PosType.LoanOrBorrowedQuantity);
    });

    test('should be immutable', () => {
        const ref = PosType;
        expect(() => {
            ref.AllocationTradeQty = 10;
        }).toThrowError();
    });
});
