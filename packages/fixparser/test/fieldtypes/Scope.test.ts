import { Scope } from '../../src/fieldtypes/Scope';

describe('Scope', () => {
    test('should have the correct values', () => {
        expect(Scope.LocalMarket).toBe('1');
        expect(Scope.National).toBe('2');
        expect(Scope.Global).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Scope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [Scope.LocalMarket, Scope.National, Scope.Global];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Scope type', () => {
        const validate = (value: Scope) => {
            expect(Object.values(Scope)).toContain(value);
        };

        validate(Scope.LocalMarket);
        validate(Scope.National);
        validate(Scope.Global);
    });

    test('should be immutable', () => {
        const ref = Scope;
        expect(() => {
            ref.LocalMarket = 10;
        }).toThrowError();
    });
});
