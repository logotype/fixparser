import { TradeContinuation } from '../../src/fieldtypes/TradeContinuation';

describe('TradeContinuation', () => {
    test('should have the correct values', () => {
        expect(TradeContinuation.Novation).toBe(0);
        expect(TradeContinuation.PartialNovation).toBe(1);
        expect(TradeContinuation.TradeUnwind).toBe(2);
        expect(TradeContinuation.PartialTradeUnwind).toBe(3);
        expect(TradeContinuation.Exercise).toBe(4);
        expect(TradeContinuation.Netting).toBe(5);
        expect(TradeContinuation.FullNetting).toBe(6);
        expect(TradeContinuation.PartialNetting).toBe(7);
        expect(TradeContinuation.Amendment).toBe(8);
        expect(TradeContinuation.Increase).toBe(9);
        expect(TradeContinuation.CreditEvent).toBe(10);
        expect(TradeContinuation.StrategicRestructuring).toBe(11);
        expect(TradeContinuation.SuccessionEventReorganization).toBe(12);
        expect(TradeContinuation.SuccessionEventRenaming).toBe(13);
        expect(TradeContinuation.Porting).toBe(14);
        expect(TradeContinuation.Withdrawl).toBe(15);
        expect(TradeContinuation.Void).toBe(16);
        expect(TradeContinuation.AccountTransfer).toBe(17);
        expect(TradeContinuation.GiveUp).toBe(18);
        expect(TradeContinuation.TakeUp).toBe(19);
        expect(TradeContinuation.AveragePricing).toBe(20);
        expect(TradeContinuation.Reversal).toBe(21);
        expect(TradeContinuation.AllocTrdPosting).toBe(22);
        expect(TradeContinuation.Cascade).toBe(23);
        expect(TradeContinuation.Delivery).toBe(24);
        expect(TradeContinuation.OptionAsgn).toBe(25);
        expect(TradeContinuation.Expiration).toBe(26);
        expect(TradeContinuation.Maturity).toBe(27);
        expect(TradeContinuation.EqualPosAdj).toBe(28);
        expect(TradeContinuation.UnequalPosAdj).toBe(29);
        expect(TradeContinuation.Correction).toBe(30);
        expect(TradeContinuation.EarlyTermination).toBe(31);
        expect(TradeContinuation.Rerate).toBe(32);
        expect(TradeContinuation.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeContinuation.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeContinuation.Novation,
            TradeContinuation.PartialNovation,
            TradeContinuation.TradeUnwind,
            TradeContinuation.PartialTradeUnwind,
            TradeContinuation.Exercise,
            TradeContinuation.Netting,
            TradeContinuation.FullNetting,
            TradeContinuation.PartialNetting,
            TradeContinuation.Amendment,
            TradeContinuation.Increase,
            TradeContinuation.CreditEvent,
            TradeContinuation.StrategicRestructuring,
            TradeContinuation.SuccessionEventReorganization,
            TradeContinuation.SuccessionEventRenaming,
            TradeContinuation.Porting,
            TradeContinuation.Withdrawl,
            TradeContinuation.Void,
            TradeContinuation.AccountTransfer,
            TradeContinuation.GiveUp,
            TradeContinuation.TakeUp,
            TradeContinuation.AveragePricing,
            TradeContinuation.Reversal,
            TradeContinuation.AllocTrdPosting,
            TradeContinuation.Cascade,
            TradeContinuation.Delivery,
            TradeContinuation.OptionAsgn,
            TradeContinuation.Expiration,
            TradeContinuation.Maturity,
            TradeContinuation.EqualPosAdj,
            TradeContinuation.UnequalPosAdj,
            TradeContinuation.Correction,
            TradeContinuation.EarlyTermination,
            TradeContinuation.Rerate,
            TradeContinuation.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeContinuation type', () => {
        const validate = (value: TradeContinuation) => {
            expect(Object.values(TradeContinuation)).toContain(value);
        };

        validate(TradeContinuation.Novation);
        validate(TradeContinuation.PartialNovation);
        validate(TradeContinuation.TradeUnwind);
        validate(TradeContinuation.PartialTradeUnwind);
        validate(TradeContinuation.Exercise);
        validate(TradeContinuation.Netting);
        validate(TradeContinuation.FullNetting);
        validate(TradeContinuation.PartialNetting);
        validate(TradeContinuation.Amendment);
        validate(TradeContinuation.Increase);
        validate(TradeContinuation.CreditEvent);
        validate(TradeContinuation.StrategicRestructuring);
        validate(TradeContinuation.SuccessionEventReorganization);
        validate(TradeContinuation.SuccessionEventRenaming);
        validate(TradeContinuation.Porting);
        validate(TradeContinuation.Withdrawl);
        validate(TradeContinuation.Void);
        validate(TradeContinuation.AccountTransfer);
        validate(TradeContinuation.GiveUp);
        validate(TradeContinuation.TakeUp);
        validate(TradeContinuation.AveragePricing);
        validate(TradeContinuation.Reversal);
        validate(TradeContinuation.AllocTrdPosting);
        validate(TradeContinuation.Cascade);
        validate(TradeContinuation.Delivery);
        validate(TradeContinuation.OptionAsgn);
        validate(TradeContinuation.Expiration);
        validate(TradeContinuation.Maturity);
        validate(TradeContinuation.EqualPosAdj);
        validate(TradeContinuation.UnequalPosAdj);
        validate(TradeContinuation.Correction);
        validate(TradeContinuation.EarlyTermination);
        validate(TradeContinuation.Rerate);
        validate(TradeContinuation.Other);
    });

    test('should be immutable', () => {
        const ref = TradeContinuation;
        expect(() => {
            ref.Novation = 10;
        }).toThrowError();
    });
});
