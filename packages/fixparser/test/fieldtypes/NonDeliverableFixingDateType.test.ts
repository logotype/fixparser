import { NonDeliverableFixingDateType } from '../../src/fieldtypes/NonDeliverableFixingDateType';

describe('NonDeliverableFixingDateType', () => {
    test('should have the correct values', () => {
        expect(NonDeliverableFixingDateType.Unadjusted).toBe(0);
        expect(NonDeliverableFixingDateType.Adjusted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NonDeliverableFixingDateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NonDeliverableFixingDateType.Unadjusted, NonDeliverableFixingDateType.Adjusted];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NonDeliverableFixingDateType type', () => {
        const validate = (value: NonDeliverableFixingDateType) => {
            expect(Object.values(NonDeliverableFixingDateType)).toContain(value);
        };

        validate(NonDeliverableFixingDateType.Unadjusted);
        validate(NonDeliverableFixingDateType.Adjusted);
    });

    test('should be immutable', () => {
        const ref = NonDeliverableFixingDateType;
        expect(() => {
            ref.Unadjusted = 10;
        }).toThrowError();
    });
});
