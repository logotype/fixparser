import { PaymentStreamRateIndexCurveUnit } from '../../src/fieldtypes/PaymentStreamRateIndexCurveUnit';

describe('PaymentStreamRateIndexCurveUnit', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRateIndexCurveUnit.Day).toBe('D');
        expect(PaymentStreamRateIndexCurveUnit.Week).toBe('Wk');
        expect(PaymentStreamRateIndexCurveUnit.Month).toBe('Mo');
        expect(PaymentStreamRateIndexCurveUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRateIndexCurveUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamRateIndexCurveUnit.Day,
            PaymentStreamRateIndexCurveUnit.Week,
            PaymentStreamRateIndexCurveUnit.Month,
            PaymentStreamRateIndexCurveUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PaymentStreamRateIndexCurveUnit type', () => {
        const validate = (value: PaymentStreamRateIndexCurveUnit) => {
            expect(Object.values(PaymentStreamRateIndexCurveUnit)).toContain(value);
        };

        validate(PaymentStreamRateIndexCurveUnit.Day);
        validate(PaymentStreamRateIndexCurveUnit.Week);
        validate(PaymentStreamRateIndexCurveUnit.Month);
        validate(PaymentStreamRateIndexCurveUnit.Year);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRateIndexCurveUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
