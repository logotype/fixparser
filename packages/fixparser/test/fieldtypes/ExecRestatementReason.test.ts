import { ExecRestatementReason } from '../../src/fieldtypes/ExecRestatementReason';

describe('ExecRestatementReason', () => {
    test('should have the correct values', () => {
        expect(ExecRestatementReason.GTCorporateAction).toBe(0);
        expect(ExecRestatementReason.GTRenewal).toBe(1);
        expect(ExecRestatementReason.VerbalChange).toBe(2);
        expect(ExecRestatementReason.RepricingOfOrder).toBe(3);
        expect(ExecRestatementReason.BrokerOption).toBe(4);
        expect(ExecRestatementReason.PartialDeclineOfOrderQty).toBe(5);
        expect(ExecRestatementReason.CancelOnTradingHalt).toBe(6);
        expect(ExecRestatementReason.CancelOnSystemFailure).toBe(7);
        expect(ExecRestatementReason.Market).toBe(8);
        expect(ExecRestatementReason.Canceled).toBe(9);
        expect(ExecRestatementReason.WarehouseRecap).toBe(10);
        expect(ExecRestatementReason.PegRefresh).toBe(11);
        expect(ExecRestatementReason.CancelOnConnectionLoss).toBe(12);
        expect(ExecRestatementReason.CancelOnLogout).toBe(13);
        expect(ExecRestatementReason.AssignTimePriority).toBe(14);
        expect(ExecRestatementReason.CancelledForTradePriceViolation).toBe(15);
        expect(ExecRestatementReason.CancelledForCrossImbalance).toBe(16);
        expect(ExecRestatementReason.CxldSMP).toBe(17);
        expect(ExecRestatementReason.CxldSMPAggressive).toBe(18);
        expect(ExecRestatementReason.CxldSMPPassive).toBe(19);
        expect(ExecRestatementReason.CxldSMPAggressivePassive).toBe(20);
        expect(ExecRestatementReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecRestatementReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecRestatementReason.GTCorporateAction,
            ExecRestatementReason.GTRenewal,
            ExecRestatementReason.VerbalChange,
            ExecRestatementReason.RepricingOfOrder,
            ExecRestatementReason.BrokerOption,
            ExecRestatementReason.PartialDeclineOfOrderQty,
            ExecRestatementReason.CancelOnTradingHalt,
            ExecRestatementReason.CancelOnSystemFailure,
            ExecRestatementReason.Market,
            ExecRestatementReason.Canceled,
            ExecRestatementReason.WarehouseRecap,
            ExecRestatementReason.PegRefresh,
            ExecRestatementReason.CancelOnConnectionLoss,
            ExecRestatementReason.CancelOnLogout,
            ExecRestatementReason.AssignTimePriority,
            ExecRestatementReason.CancelledForTradePriceViolation,
            ExecRestatementReason.CancelledForCrossImbalance,
            ExecRestatementReason.CxldSMP,
            ExecRestatementReason.CxldSMPAggressive,
            ExecRestatementReason.CxldSMPPassive,
            ExecRestatementReason.CxldSMPAggressivePassive,
            ExecRestatementReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExecRestatementReason type', () => {
        const validate = (value: ExecRestatementReason) => {
            expect(Object.values(ExecRestatementReason)).toContain(value);
        };

        validate(ExecRestatementReason.GTCorporateAction);
        validate(ExecRestatementReason.GTRenewal);
        validate(ExecRestatementReason.VerbalChange);
        validate(ExecRestatementReason.RepricingOfOrder);
        validate(ExecRestatementReason.BrokerOption);
        validate(ExecRestatementReason.PartialDeclineOfOrderQty);
        validate(ExecRestatementReason.CancelOnTradingHalt);
        validate(ExecRestatementReason.CancelOnSystemFailure);
        validate(ExecRestatementReason.Market);
        validate(ExecRestatementReason.Canceled);
        validate(ExecRestatementReason.WarehouseRecap);
        validate(ExecRestatementReason.PegRefresh);
        validate(ExecRestatementReason.CancelOnConnectionLoss);
        validate(ExecRestatementReason.CancelOnLogout);
        validate(ExecRestatementReason.AssignTimePriority);
        validate(ExecRestatementReason.CancelledForTradePriceViolation);
        validate(ExecRestatementReason.CancelledForCrossImbalance);
        validate(ExecRestatementReason.CxldSMP);
        validate(ExecRestatementReason.CxldSMPAggressive);
        validate(ExecRestatementReason.CxldSMPPassive);
        validate(ExecRestatementReason.CxldSMPAggressivePassive);
        validate(ExecRestatementReason.Other);
    });

    test('should be immutable', () => {
        const ref = ExecRestatementReason;
        expect(() => {
            ref.GTCorporateAction = 10;
        }).toThrowError();
    });
});
