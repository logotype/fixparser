import { MsgDirection } from '../../src/fieldtypes/MsgDirection';

describe('MsgDirection', () => {
    test('should have the correct values', () => {
        expect(MsgDirection.Receive).toBe('R');
        expect(MsgDirection.Send).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MsgDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MsgDirection.Receive, MsgDirection.Send];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MsgDirection type', () => {
        const validate = (value: MsgDirection) => {
            expect(Object.values(MsgDirection)).toContain(value);
        };

        validate(MsgDirection.Receive);
        validate(MsgDirection.Send);
    });

    test('should be immutable', () => {
        const ref = MsgDirection;
        expect(() => {
            ref.Receive = 10;
        }).toThrowError();
    });
});
