import { MDOriginType } from '../../src/fieldtypes/MDOriginType';

describe('MDOriginType', () => {
    test('should have the correct values', () => {
        expect(MDOriginType.Book).toBe(0);
        expect(MDOriginType.OffBook).toBe(1);
        expect(MDOriginType.Cross).toBe(2);
        expect(MDOriginType.QuoteDrivenMarket).toBe(3);
        expect(MDOriginType.DarkOrderBook).toBe(4);
        expect(MDOriginType.AuctionDrivenMarket).toBe(5);
        expect(MDOriginType.QuoteNegotiation).toBe(6);
        expect(MDOriginType.VoiceNegotiation).toBe(7);
        expect(MDOriginType.HybridMarket).toBe(8);
        expect(MDOriginType.OtherMarket).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDOriginType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDOriginType.Book,
            MDOriginType.OffBook,
            MDOriginType.Cross,
            MDOriginType.QuoteDrivenMarket,
            MDOriginType.DarkOrderBook,
            MDOriginType.AuctionDrivenMarket,
            MDOriginType.QuoteNegotiation,
            MDOriginType.VoiceNegotiation,
            MDOriginType.HybridMarket,
            MDOriginType.OtherMarket,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDOriginType type', () => {
        const validate = (value: MDOriginType) => {
            expect(Object.values(MDOriginType)).toContain(value);
        };

        validate(MDOriginType.Book);
        validate(MDOriginType.OffBook);
        validate(MDOriginType.Cross);
        validate(MDOriginType.QuoteDrivenMarket);
        validate(MDOriginType.DarkOrderBook);
        validate(MDOriginType.AuctionDrivenMarket);
        validate(MDOriginType.QuoteNegotiation);
        validate(MDOriginType.VoiceNegotiation);
        validate(MDOriginType.HybridMarket);
        validate(MDOriginType.OtherMarket);
    });

    test('should be immutable', () => {
        const ref = MDOriginType;
        expect(() => {
            ref.Book = 10;
        }).toThrowError();
    });
});
