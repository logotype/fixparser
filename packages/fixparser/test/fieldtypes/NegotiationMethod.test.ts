import { NegotiationMethod } from '../../src/fieldtypes/NegotiationMethod';

describe('NegotiationMethod', () => {
    test('should have the correct values', () => {
        expect(NegotiationMethod.AutoSpot).toBe(0);
        expect(NegotiationMethod.NegotiatedSpot).toBe(1);
        expect(NegotiationMethod.PhoneSpot).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NegotiationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NegotiationMethod.AutoSpot,
            NegotiationMethod.NegotiatedSpot,
            NegotiationMethod.PhoneSpot,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NegotiationMethod type', () => {
        const validate = (value: NegotiationMethod) => {
            expect(Object.values(NegotiationMethod)).toContain(value);
        };

        validate(NegotiationMethod.AutoSpot);
        validate(NegotiationMethod.NegotiatedSpot);
        validate(NegotiationMethod.PhoneSpot);
    });

    test('should be immutable', () => {
        const ref = NegotiationMethod;
        expect(() => {
            ref.AutoSpot = 10;
        }).toThrowError();
    });
});
