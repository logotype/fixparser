import { PaymentStreamFRADiscounting } from '../../src/fieldtypes/PaymentStreamFRADiscounting';

describe('PaymentStreamFRADiscounting', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamFRADiscounting.None).toBe(0);
        expect(PaymentStreamFRADiscounting.ISDA).toBe(1);
        expect(PaymentStreamFRADiscounting.AFMA).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamFRADiscounting.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamFRADiscounting.None,
            PaymentStreamFRADiscounting.ISDA,
            PaymentStreamFRADiscounting.AFMA,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamFRADiscounting type', () => {
        const validate = (value: PaymentStreamFRADiscounting) => {
            expect(Object.values(PaymentStreamFRADiscounting)).toContain(value);
        };

        validate(PaymentStreamFRADiscounting.None);
        validate(PaymentStreamFRADiscounting.ISDA);
        validate(PaymentStreamFRADiscounting.AFMA);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamFRADiscounting;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
