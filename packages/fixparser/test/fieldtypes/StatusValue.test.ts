import { StatusValue } from '../../src/fieldtypes/StatusValue';

describe('StatusValue', () => {
    test('should have the correct values', () => {
        expect(StatusValue.Connected).toBe(1);
        expect(StatusValue.NotConnectedUnexpected).toBe(2);
        expect(StatusValue.NotConnectedExpected).toBe(3);
        expect(StatusValue.InProcess).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StatusValue.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StatusValue.Connected,
            StatusValue.NotConnectedUnexpected,
            StatusValue.NotConnectedExpected,
            StatusValue.InProcess,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StatusValue type', () => {
        const validate = (value: StatusValue) => {
            expect(Object.values(StatusValue)).toContain(value);
        };

        validate(StatusValue.Connected);
        validate(StatusValue.NotConnectedUnexpected);
        validate(StatusValue.NotConnectedExpected);
        validate(StatusValue.InProcess);
    });

    test('should be immutable', () => {
        const ref = StatusValue;
        expect(() => {
            ref.Connected = 10;
        }).toThrowError();
    });
});
