import { TaxAdvantageType } from '../../src/fieldtypes/TaxAdvantageType';

describe('TaxAdvantageType', () => {
    test('should have the correct values', () => {
        expect(TaxAdvantageType.None).toBe(0);
        expect(TaxAdvantageType.MaxiISA).toBe(1);
        expect(TaxAdvantageType.TESSA).toBe(2);
        expect(TaxAdvantageType.MiniCashISA).toBe(3);
        expect(TaxAdvantageType.MiniStocksAndSharesISA).toBe(4);
        expect(TaxAdvantageType.MiniInsuranceISA).toBe(5);
        expect(TaxAdvantageType.CurrentYearPayment).toBe(6);
        expect(TaxAdvantageType.PriorYearPayment).toBe(7);
        expect(TaxAdvantageType.AssetTransfer).toBe(8);
        expect(TaxAdvantageType.EmployeePriorYear).toBe(9);
        expect(TaxAdvantageType.EmployeeCurrentYear).toBe(10);
        expect(TaxAdvantageType.EmployerPriorYear).toBe(11);
        expect(TaxAdvantageType.EmployerCurrentYear).toBe(12);
        expect(TaxAdvantageType.NonFundPrototypeIRA).toBe(13);
        expect(TaxAdvantageType.NonFundQualifiedPlan).toBe(14);
        expect(TaxAdvantageType.DefinedContributionPlan).toBe(15);
        expect(TaxAdvantageType.IRA).toBe(16);
        expect(TaxAdvantageType.IRARollover).toBe(17);
        expect(TaxAdvantageType.KEOGH).toBe(18);
        expect(TaxAdvantageType.ProfitSharingPlan).toBe(19);
        expect(TaxAdvantageType.US401K).toBe(20);
        expect(TaxAdvantageType.SelfDirectedIRA).toBe(21);
        expect(TaxAdvantageType.US403b).toBe(22);
        expect(TaxAdvantageType.US457).toBe(23);
        expect(TaxAdvantageType.RothIRAPrototype).toBe(24);
        expect(TaxAdvantageType.RothIRANonPrototype).toBe(25);
        expect(TaxAdvantageType.RothConversionIRAPrototype).toBe(26);
        expect(TaxAdvantageType.RothConversionIRANonPrototype).toBe(27);
        expect(TaxAdvantageType.EducationIRAPrototype).toBe(28);
        expect(TaxAdvantageType.EducationIRANonPrototype).toBe(29);
        expect(TaxAdvantageType.Other).toBe(999);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TaxAdvantageType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TaxAdvantageType.None,
            TaxAdvantageType.MaxiISA,
            TaxAdvantageType.TESSA,
            TaxAdvantageType.MiniCashISA,
            TaxAdvantageType.MiniStocksAndSharesISA,
            TaxAdvantageType.MiniInsuranceISA,
            TaxAdvantageType.CurrentYearPayment,
            TaxAdvantageType.PriorYearPayment,
            TaxAdvantageType.AssetTransfer,
            TaxAdvantageType.EmployeePriorYear,
            TaxAdvantageType.EmployeeCurrentYear,
            TaxAdvantageType.EmployerPriorYear,
            TaxAdvantageType.EmployerCurrentYear,
            TaxAdvantageType.NonFundPrototypeIRA,
            TaxAdvantageType.NonFundQualifiedPlan,
            TaxAdvantageType.DefinedContributionPlan,
            TaxAdvantageType.IRA,
            TaxAdvantageType.IRARollover,
            TaxAdvantageType.KEOGH,
            TaxAdvantageType.ProfitSharingPlan,
            TaxAdvantageType.US401K,
            TaxAdvantageType.SelfDirectedIRA,
            TaxAdvantageType.US403b,
            TaxAdvantageType.US457,
            TaxAdvantageType.RothIRAPrototype,
            TaxAdvantageType.RothIRANonPrototype,
            TaxAdvantageType.RothConversionIRAPrototype,
            TaxAdvantageType.RothConversionIRANonPrototype,
            TaxAdvantageType.EducationIRAPrototype,
            TaxAdvantageType.EducationIRANonPrototype,
            TaxAdvantageType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TaxAdvantageType type', () => {
        const validate = (value: TaxAdvantageType) => {
            expect(Object.values(TaxAdvantageType)).toContain(value);
        };

        validate(TaxAdvantageType.None);
        validate(TaxAdvantageType.MaxiISA);
        validate(TaxAdvantageType.TESSA);
        validate(TaxAdvantageType.MiniCashISA);
        validate(TaxAdvantageType.MiniStocksAndSharesISA);
        validate(TaxAdvantageType.MiniInsuranceISA);
        validate(TaxAdvantageType.CurrentYearPayment);
        validate(TaxAdvantageType.PriorYearPayment);
        validate(TaxAdvantageType.AssetTransfer);
        validate(TaxAdvantageType.EmployeePriorYear);
        validate(TaxAdvantageType.EmployeeCurrentYear);
        validate(TaxAdvantageType.EmployerPriorYear);
        validate(TaxAdvantageType.EmployerCurrentYear);
        validate(TaxAdvantageType.NonFundPrototypeIRA);
        validate(TaxAdvantageType.NonFundQualifiedPlan);
        validate(TaxAdvantageType.DefinedContributionPlan);
        validate(TaxAdvantageType.IRA);
        validate(TaxAdvantageType.IRARollover);
        validate(TaxAdvantageType.KEOGH);
        validate(TaxAdvantageType.ProfitSharingPlan);
        validate(TaxAdvantageType.US401K);
        validate(TaxAdvantageType.SelfDirectedIRA);
        validate(TaxAdvantageType.US403b);
        validate(TaxAdvantageType.US457);
        validate(TaxAdvantageType.RothIRAPrototype);
        validate(TaxAdvantageType.RothIRANonPrototype);
        validate(TaxAdvantageType.RothConversionIRAPrototype);
        validate(TaxAdvantageType.RothConversionIRANonPrototype);
        validate(TaxAdvantageType.EducationIRAPrototype);
        validate(TaxAdvantageType.EducationIRANonPrototype);
        validate(TaxAdvantageType.Other);
    });

    test('should be immutable', () => {
        const ref = TaxAdvantageType;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
