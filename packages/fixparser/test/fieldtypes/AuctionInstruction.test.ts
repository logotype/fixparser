import { AuctionInstruction } from '../../src/fieldtypes/AuctionInstruction';

describe('AuctionInstruction', () => {
    test('should have the correct values', () => {
        expect(AuctionInstruction.AutomatedAuctionPermitted).toBe(0);
        expect(AuctionInstruction.AutomatedAuctionNotPermitted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AuctionInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AuctionInstruction.AutomatedAuctionPermitted,
            AuctionInstruction.AutomatedAuctionNotPermitted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AuctionInstruction type', () => {
        const validate = (value: AuctionInstruction) => {
            expect(Object.values(AuctionInstruction)).toContain(value);
        };

        validate(AuctionInstruction.AutomatedAuctionPermitted);
        validate(AuctionInstruction.AutomatedAuctionNotPermitted);
    });

    test('should be immutable', () => {
        const ref = AuctionInstruction;
        expect(() => {
            ref.AutomatedAuctionPermitted = 10;
        }).toThrowError();
    });
});
