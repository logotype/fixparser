import { AdvTransType } from '../../src/fieldtypes/AdvTransType';

describe('AdvTransType', () => {
    test('should have the correct values', () => {
        expect(AdvTransType.New).toBe('N');
        expect(AdvTransType.Cancel).toBe('C');
        expect(AdvTransType.Replace).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AdvTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AdvTransType.New, AdvTransType.Cancel, AdvTransType.Replace];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AdvTransType type', () => {
        const validate = (value: AdvTransType) => {
            expect(Object.values(AdvTransType)).toContain(value);
        };

        validate(AdvTransType.New);
        validate(AdvTransType.Cancel);
        validate(AdvTransType.Replace);
    });

    test('should be immutable', () => {
        const ref = AdvTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
