import { NetworkRequestType } from '../../src/fieldtypes/NetworkRequestType';

describe('NetworkRequestType', () => {
    test('should have the correct values', () => {
        expect(NetworkRequestType.Snapshot).toBe(1);
        expect(NetworkRequestType.Subscribe).toBe(2);
        expect(NetworkRequestType.StopSubscribing).toBe(4);
        expect(NetworkRequestType.LevelOfDetail).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NetworkRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NetworkRequestType.Snapshot,
            NetworkRequestType.Subscribe,
            NetworkRequestType.StopSubscribing,
            NetworkRequestType.LevelOfDetail,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NetworkRequestType type', () => {
        const validate = (value: NetworkRequestType) => {
            expect(Object.values(NetworkRequestType)).toContain(value);
        };

        validate(NetworkRequestType.Snapshot);
        validate(NetworkRequestType.Subscribe);
        validate(NetworkRequestType.StopSubscribing);
        validate(NetworkRequestType.LevelOfDetail);
    });

    test('should be immutable', () => {
        const ref = NetworkRequestType;
        expect(() => {
            ref.Snapshot = 10;
        }).toThrowError();
    });
});
