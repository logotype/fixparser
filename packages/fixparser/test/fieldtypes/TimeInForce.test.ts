import { TimeInForce } from '../../src/fieldtypes/TimeInForce';

describe('TimeInForce', () => {
    test('should have the correct values', () => {
        expect(TimeInForce.Day).toBe('0');
        expect(TimeInForce.GoodTillCancel).toBe('1');
        expect(TimeInForce.AtTheOpening).toBe('2');
        expect(TimeInForce.ImmediateOrCancel).toBe('3');
        expect(TimeInForce.FillOrKill).toBe('4');
        expect(TimeInForce.GoodTillCrossing).toBe('5');
        expect(TimeInForce.GoodTillDate).toBe('6');
        expect(TimeInForce.AtTheClose).toBe('7');
        expect(TimeInForce.GoodThroughCrossing).toBe('8');
        expect(TimeInForce.AtCrossing).toBe('9');
        expect(TimeInForce.GoodForTime).toBe('A');
        expect(TimeInForce.GoodForAuction).toBe('B');
        expect(TimeInForce.GoodForMonth).toBe('C');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TimeInForce.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TimeInForce.Day,
            TimeInForce.GoodTillCancel,
            TimeInForce.AtTheOpening,
            TimeInForce.ImmediateOrCancel,
            TimeInForce.FillOrKill,
            TimeInForce.GoodTillCrossing,
            TimeInForce.GoodTillDate,
            TimeInForce.AtTheClose,
            TimeInForce.GoodThroughCrossing,
            TimeInForce.AtCrossing,
            TimeInForce.GoodForTime,
            TimeInForce.GoodForAuction,
            TimeInForce.GoodForMonth,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TimeInForce type', () => {
        const validate = (value: TimeInForce) => {
            expect(Object.values(TimeInForce)).toContain(value);
        };

        validate(TimeInForce.Day);
        validate(TimeInForce.GoodTillCancel);
        validate(TimeInForce.AtTheOpening);
        validate(TimeInForce.ImmediateOrCancel);
        validate(TimeInForce.FillOrKill);
        validate(TimeInForce.GoodTillCrossing);
        validate(TimeInForce.GoodTillDate);
        validate(TimeInForce.AtTheClose);
        validate(TimeInForce.GoodThroughCrossing);
        validate(TimeInForce.AtCrossing);
        validate(TimeInForce.GoodForTime);
        validate(TimeInForce.GoodForAuction);
        validate(TimeInForce.GoodForMonth);
    });

    test('should be immutable', () => {
        const ref = TimeInForce;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
