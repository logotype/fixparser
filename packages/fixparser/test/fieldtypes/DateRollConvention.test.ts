import { DateRollConvention } from '../../src/fieldtypes/DateRollConvention';

describe('DateRollConvention', () => {
    test('should have the correct values', () => {
        expect(DateRollConvention.FirstDay).toBe('1');
        expect(DateRollConvention.SecondDay).toBe('2');
        expect(DateRollConvention.ThirdDay).toBe('3');
        expect(DateRollConvention.FourthDay).toBe('4');
        expect(DateRollConvention.FifthDay).toBe('5');
        expect(DateRollConvention.SixthDay).toBe('6');
        expect(DateRollConvention.SeventhDay).toBe('7');
        expect(DateRollConvention.EighthDay).toBe('8');
        expect(DateRollConvention.NinthDay).toBe('9');
        expect(DateRollConvention.TenthDay).toBe('10');
        expect(DateRollConvention.EleventhDay).toBe('11');
        expect(DateRollConvention.TwelvthDay).toBe('12');
        expect(DateRollConvention.ThirteenthDay).toBe('13');
        expect(DateRollConvention.ForteenthDay).toBe('14');
        expect(DateRollConvention.FifteenthDay).toBe('15');
        expect(DateRollConvention.SixteenthDay).toBe('16');
        expect(DateRollConvention.SeventeenthDay).toBe('17');
        expect(DateRollConvention.EighteenthDay).toBe('18');
        expect(DateRollConvention.NineteenthDay).toBe('19');
        expect(DateRollConvention.TwentiethDay).toBe('20');
        expect(DateRollConvention.TwentyFirstDay).toBe('21');
        expect(DateRollConvention.TwentySecondDay).toBe('22');
        expect(DateRollConvention.TwentyThirdDay).toBe('23');
        expect(DateRollConvention.TwentyFourthDay).toBe('24');
        expect(DateRollConvention.TwentyFifthDay).toBe('25');
        expect(DateRollConvention.TwentySixthDay).toBe('26');
        expect(DateRollConvention.TwentySeventhDay).toBe('27');
        expect(DateRollConvention.TwentyEigthDa28y).toBe('28');
        expect(DateRollConvention.TwentyNinthDay).toBe('29');
        expect(DateRollConvention.ThirtiethDay).toBe('30');
        expect(DateRollConvention.EOM).toBe('EOM');
        expect(DateRollConvention.FRN).toBe('FRN');
        expect(DateRollConvention.IMM).toBe('IMM');
        expect(DateRollConvention.IMMCAD).toBe('IMMCAD');
        expect(DateRollConvention.IMMAUD).toBe('IMMAUD');
        expect(DateRollConvention.IMMNZD).toBe('IMMNZD');
        expect(DateRollConvention.SFE).toBe('SFE');
        expect(DateRollConvention.NONE).toBe('NONE');
        expect(DateRollConvention.TBILL).toBe('TBILL');
        expect(DateRollConvention.MON).toBe('MON');
        expect(DateRollConvention.TUE).toBe('TUE');
        expect(DateRollConvention.WED).toBe('WED');
        expect(DateRollConvention.THU).toBe('THU');
        expect(DateRollConvention.FRI).toBe('FRI');
        expect(DateRollConvention.SAT).toBe('SAT');
        expect(DateRollConvention.SUN).toBe('SUN');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DateRollConvention.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DateRollConvention.FirstDay,
            DateRollConvention.SecondDay,
            DateRollConvention.ThirdDay,
            DateRollConvention.FourthDay,
            DateRollConvention.FifthDay,
            DateRollConvention.SixthDay,
            DateRollConvention.SeventhDay,
            DateRollConvention.EighthDay,
            DateRollConvention.NinthDay,
            DateRollConvention.TenthDay,
            DateRollConvention.EleventhDay,
            DateRollConvention.TwelvthDay,
            DateRollConvention.ThirteenthDay,
            DateRollConvention.ForteenthDay,
            DateRollConvention.FifteenthDay,
            DateRollConvention.SixteenthDay,
            DateRollConvention.SeventeenthDay,
            DateRollConvention.EighteenthDay,
            DateRollConvention.NineteenthDay,
            DateRollConvention.TwentiethDay,
            DateRollConvention.TwentyFirstDay,
            DateRollConvention.TwentySecondDay,
            DateRollConvention.TwentyThirdDay,
            DateRollConvention.TwentyFourthDay,
            DateRollConvention.TwentyFifthDay,
            DateRollConvention.TwentySixthDay,
            DateRollConvention.TwentySeventhDay,
            DateRollConvention.TwentyEigthDa28y,
            DateRollConvention.TwentyNinthDay,
            DateRollConvention.ThirtiethDay,
            DateRollConvention.EOM,
            DateRollConvention.FRN,
            DateRollConvention.IMM,
            DateRollConvention.IMMCAD,
            DateRollConvention.IMMAUD,
            DateRollConvention.IMMNZD,
            DateRollConvention.SFE,
            DateRollConvention.NONE,
            DateRollConvention.TBILL,
            DateRollConvention.MON,
            DateRollConvention.TUE,
            DateRollConvention.WED,
            DateRollConvention.THU,
            DateRollConvention.FRI,
            DateRollConvention.SAT,
            DateRollConvention.SUN,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DateRollConvention type', () => {
        const validate = (value: DateRollConvention) => {
            expect(Object.values(DateRollConvention)).toContain(value);
        };

        validate(DateRollConvention.FirstDay);
        validate(DateRollConvention.SecondDay);
        validate(DateRollConvention.ThirdDay);
        validate(DateRollConvention.FourthDay);
        validate(DateRollConvention.FifthDay);
        validate(DateRollConvention.SixthDay);
        validate(DateRollConvention.SeventhDay);
        validate(DateRollConvention.EighthDay);
        validate(DateRollConvention.NinthDay);
        validate(DateRollConvention.TenthDay);
        validate(DateRollConvention.EleventhDay);
        validate(DateRollConvention.TwelvthDay);
        validate(DateRollConvention.ThirteenthDay);
        validate(DateRollConvention.ForteenthDay);
        validate(DateRollConvention.FifteenthDay);
        validate(DateRollConvention.SixteenthDay);
        validate(DateRollConvention.SeventeenthDay);
        validate(DateRollConvention.EighteenthDay);
        validate(DateRollConvention.NineteenthDay);
        validate(DateRollConvention.TwentiethDay);
        validate(DateRollConvention.TwentyFirstDay);
        validate(DateRollConvention.TwentySecondDay);
        validate(DateRollConvention.TwentyThirdDay);
        validate(DateRollConvention.TwentyFourthDay);
        validate(DateRollConvention.TwentyFifthDay);
        validate(DateRollConvention.TwentySixthDay);
        validate(DateRollConvention.TwentySeventhDay);
        validate(DateRollConvention.TwentyEigthDa28y);
        validate(DateRollConvention.TwentyNinthDay);
        validate(DateRollConvention.ThirtiethDay);
        validate(DateRollConvention.EOM);
        validate(DateRollConvention.FRN);
        validate(DateRollConvention.IMM);
        validate(DateRollConvention.IMMCAD);
        validate(DateRollConvention.IMMAUD);
        validate(DateRollConvention.IMMNZD);
        validate(DateRollConvention.SFE);
        validate(DateRollConvention.NONE);
        validate(DateRollConvention.TBILL);
        validate(DateRollConvention.MON);
        validate(DateRollConvention.TUE);
        validate(DateRollConvention.WED);
        validate(DateRollConvention.THU);
        validate(DateRollConvention.FRI);
        validate(DateRollConvention.SAT);
        validate(DateRollConvention.SUN);
    });

    test('should be immutable', () => {
        const ref = DateRollConvention;
        expect(() => {
            ref.FirstDay = 10;
        }).toThrowError();
    });
});
