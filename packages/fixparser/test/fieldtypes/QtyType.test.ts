import { QtyType } from '../../src/fieldtypes/QtyType';

describe('QtyType', () => {
    test('should have the correct values', () => {
        expect(QtyType.Units).toBe(0);
        expect(QtyType.Contracts).toBe(1);
        expect(QtyType.UnitsOfMeasurePerTimeUnit).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QtyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [QtyType.Units, QtyType.Contracts, QtyType.UnitsOfMeasurePerTimeUnit];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QtyType type', () => {
        const validate = (value: QtyType) => {
            expect(Object.values(QtyType)).toContain(value);
        };

        validate(QtyType.Units);
        validate(QtyType.Contracts);
        validate(QtyType.UnitsOfMeasurePerTimeUnit);
    });

    test('should be immutable', () => {
        const ref = QtyType;
        expect(() => {
            ref.Units = 10;
        }).toThrowError();
    });
});
