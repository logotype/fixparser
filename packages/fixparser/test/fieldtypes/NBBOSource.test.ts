import { NBBOSource } from '../../src/fieldtypes/NBBOSource';

describe('NBBOSource', () => {
    test('should have the correct values', () => {
        expect(NBBOSource.NotApplicable).toBe(0);
        expect(NBBOSource.Direct).toBe(1);
        expect(NBBOSource.SIP).toBe(2);
        expect(NBBOSource.Hybrid).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NBBOSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NBBOSource.NotApplicable, NBBOSource.Direct, NBBOSource.SIP, NBBOSource.Hybrid];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NBBOSource type', () => {
        const validate = (value: NBBOSource) => {
            expect(Object.values(NBBOSource)).toContain(value);
        };

        validate(NBBOSource.NotApplicable);
        validate(NBBOSource.Direct);
        validate(NBBOSource.SIP);
        validate(NBBOSource.Hybrid);
    });

    test('should be immutable', () => {
        const ref = NBBOSource;
        expect(() => {
            ref.NotApplicable = 10;
        }).toThrowError();
    });
});
