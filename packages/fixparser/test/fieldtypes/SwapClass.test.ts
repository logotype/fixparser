import { SwapClass } from '../../src/fieldtypes/SwapClass';

describe('SwapClass', () => {
    test('should have the correct values', () => {
        expect(SwapClass.BasisSwap).toBe('BS');
        expect(SwapClass.IndexSwap).toBe('IX');
        expect(SwapClass.BroadBasedSecuritySwap).toBe('BB');
        expect(SwapClass.BasketSwap).toBe('SK');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SwapClass.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SwapClass.BasisSwap,
            SwapClass.IndexSwap,
            SwapClass.BroadBasedSecuritySwap,
            SwapClass.BasketSwap,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SwapClass type', () => {
        const validate = (value: SwapClass) => {
            expect(Object.values(SwapClass)).toContain(value);
        };

        validate(SwapClass.BasisSwap);
        validate(SwapClass.IndexSwap);
        validate(SwapClass.BroadBasedSecuritySwap);
        validate(SwapClass.BasketSwap);
    });

    test('should be immutable', () => {
        const ref = SwapClass;
        expect(() => {
            ref.BasisSwap = 10;
        }).toThrowError();
    });
});
