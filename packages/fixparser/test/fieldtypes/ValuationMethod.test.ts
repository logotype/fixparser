import { ValuationMethod } from '../../src/fieldtypes/ValuationMethod';

describe('ValuationMethod', () => {
    test('should have the correct values', () => {
        expect(ValuationMethod.PremiumStyle).toBe('EQTY');
        expect(ValuationMethod.FuturesStyleMarkToMarket).toBe('FUT');
        expect(ValuationMethod.FuturesStyleWithAnAttachedCashAdjustment).toBe('FUTDA');
        expect(ValuationMethod.CDSStyleCollateralization).toBe('CDS');
        expect(ValuationMethod.CDSInDeliveryUseRecoveryRateToCalculate).toBe('CDSD');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ValuationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ValuationMethod.PremiumStyle,
            ValuationMethod.FuturesStyleMarkToMarket,
            ValuationMethod.FuturesStyleWithAnAttachedCashAdjustment,
            ValuationMethod.CDSStyleCollateralization,
            ValuationMethod.CDSInDeliveryUseRecoveryRateToCalculate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ValuationMethod type', () => {
        const validate = (value: ValuationMethod) => {
            expect(Object.values(ValuationMethod)).toContain(value);
        };

        validate(ValuationMethod.PremiumStyle);
        validate(ValuationMethod.FuturesStyleMarkToMarket);
        validate(ValuationMethod.FuturesStyleWithAnAttachedCashAdjustment);
        validate(ValuationMethod.CDSStyleCollateralization);
        validate(ValuationMethod.CDSInDeliveryUseRecoveryRateToCalculate);
    });

    test('should be immutable', () => {
        const ref = ValuationMethod;
        expect(() => {
            ref.PremiumStyle = 10;
        }).toThrowError();
    });
});
