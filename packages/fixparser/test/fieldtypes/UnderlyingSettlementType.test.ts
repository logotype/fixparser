import { UnderlyingSettlementType } from '../../src/fieldtypes/UnderlyingSettlementType';

describe('UnderlyingSettlementType', () => {
    test('should have the correct values', () => {
        expect(UnderlyingSettlementType.TPlus1).toBe(2);
        expect(UnderlyingSettlementType.TPlus3).toBe(4);
        expect(UnderlyingSettlementType.TPlus4).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingSettlementType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnderlyingSettlementType.TPlus1,
            UnderlyingSettlementType.TPlus3,
            UnderlyingSettlementType.TPlus4,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UnderlyingSettlementType type', () => {
        const validate = (value: UnderlyingSettlementType) => {
            expect(Object.values(UnderlyingSettlementType)).toContain(value);
        };

        validate(UnderlyingSettlementType.TPlus1);
        validate(UnderlyingSettlementType.TPlus3);
        validate(UnderlyingSettlementType.TPlus4);
    });

    test('should be immutable', () => {
        const ref = UnderlyingSettlementType;
        expect(() => {
            ref.TPlus1 = 10;
        }).toThrowError();
    });
});
