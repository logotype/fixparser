import { TradSesMode } from '../../src/fieldtypes/TradSesMode';

describe('TradSesMode', () => {
    test('should have the correct values', () => {
        expect(TradSesMode.Testing).toBe(1);
        expect(TradSesMode.Simulated).toBe(2);
        expect(TradSesMode.Production).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesMode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradSesMode.Testing, TradSesMode.Simulated, TradSesMode.Production];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesMode type', () => {
        const validate = (value: TradSesMode) => {
            expect(Object.values(TradSesMode)).toContain(value);
        };

        validate(TradSesMode.Testing);
        validate(TradSesMode.Simulated);
        validate(TradSesMode.Production);
    });

    test('should be immutable', () => {
        const ref = TradSesMode;
        expect(() => {
            ref.Testing = 10;
        }).toThrowError();
    });
});
