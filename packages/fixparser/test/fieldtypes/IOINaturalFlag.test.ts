import { IOINaturalFlag } from '../../src/fieldtypes/IOINaturalFlag';

describe('IOINaturalFlag', () => {
    test('should have the correct values', () => {
        expect(IOINaturalFlag.NotNatural).toBe('N');
        expect(IOINaturalFlag.Natural).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOINaturalFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IOINaturalFlag.NotNatural, IOINaturalFlag.Natural];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOINaturalFlag type', () => {
        const validate = (value: IOINaturalFlag) => {
            expect(Object.values(IOINaturalFlag)).toContain(value);
        };

        validate(IOINaturalFlag.NotNatural);
        validate(IOINaturalFlag.Natural);
    });

    test('should be immutable', () => {
        const ref = IOINaturalFlag;
        expect(() => {
            ref.NotNatural = 10;
        }).toThrowError();
    });
});
