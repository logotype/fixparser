import { CollRptRejectReason } from '../../src/fieldtypes/CollRptRejectReason';

describe('CollRptRejectReason', () => {
    test('should have the correct values', () => {
        expect(CollRptRejectReason.UnknownTrade).toBe(0);
        expect(CollRptRejectReason.UnknownInstrument).toBe(1);
        expect(CollRptRejectReason.UnknownCounterparty).toBe(2);
        expect(CollRptRejectReason.UnknownPosition).toBe(3);
        expect(CollRptRejectReason.UnacceptableCollateral).toBe(4);
        expect(CollRptRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollRptRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollRptRejectReason.UnknownTrade,
            CollRptRejectReason.UnknownInstrument,
            CollRptRejectReason.UnknownCounterparty,
            CollRptRejectReason.UnknownPosition,
            CollRptRejectReason.UnacceptableCollateral,
            CollRptRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollRptRejectReason type', () => {
        const validate = (value: CollRptRejectReason) => {
            expect(Object.values(CollRptRejectReason)).toContain(value);
        };

        validate(CollRptRejectReason.UnknownTrade);
        validate(CollRptRejectReason.UnknownInstrument);
        validate(CollRptRejectReason.UnknownCounterparty);
        validate(CollRptRejectReason.UnknownPosition);
        validate(CollRptRejectReason.UnacceptableCollateral);
        validate(CollRptRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = CollRptRejectReason;
        expect(() => {
            ref.UnknownTrade = 10;
        }).toThrowError();
    });
});
