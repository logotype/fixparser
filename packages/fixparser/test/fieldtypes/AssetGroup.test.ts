import { AssetGroup } from '../../src/fieldtypes/AssetGroup';

describe('AssetGroup', () => {
    test('should have the correct values', () => {
        expect(AssetGroup.Financials).toBe(1);
        expect(AssetGroup.Commodities).toBe(2);
        expect(AssetGroup.AlternativeInvestments).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AssetGroup.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AssetGroup.Financials, AssetGroup.Commodities, AssetGroup.AlternativeInvestments];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AssetGroup type', () => {
        const validate = (value: AssetGroup) => {
            expect(Object.values(AssetGroup)).toContain(value);
        };

        validate(AssetGroup.Financials);
        validate(AssetGroup.Commodities);
        validate(AssetGroup.AlternativeInvestments);
    });

    test('should be immutable', () => {
        const ref = AssetGroup;
        expect(() => {
            ref.Financials = 10;
        }).toThrowError();
    });
});
