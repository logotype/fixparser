import { ReturnRateValuationPriceOption } from '../../src/fieldtypes/ReturnRateValuationPriceOption';

describe('ReturnRateValuationPriceOption', () => {
    test('should have the correct values', () => {
        expect(ReturnRateValuationPriceOption.None).toBe(0);
        expect(ReturnRateValuationPriceOption.FuturesPrice).toBe(1);
        expect(ReturnRateValuationPriceOption.OptionsPrice).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRateValuationPriceOption.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReturnRateValuationPriceOption.None,
            ReturnRateValuationPriceOption.FuturesPrice,
            ReturnRateValuationPriceOption.OptionsPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRateValuationPriceOption type', () => {
        const validate = (value: ReturnRateValuationPriceOption) => {
            expect(Object.values(ReturnRateValuationPriceOption)).toContain(value);
        };

        validate(ReturnRateValuationPriceOption.None);
        validate(ReturnRateValuationPriceOption.FuturesPrice);
        validate(ReturnRateValuationPriceOption.OptionsPrice);
    });

    test('should be immutable', () => {
        const ref = ReturnRateValuationPriceOption;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
