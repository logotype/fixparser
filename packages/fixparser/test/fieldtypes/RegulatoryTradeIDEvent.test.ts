import { RegulatoryTradeIDEvent } from '../../src/fieldtypes/RegulatoryTradeIDEvent';

describe('RegulatoryTradeIDEvent', () => {
    test('should have the correct values', () => {
        expect(RegulatoryTradeIDEvent.InitialBlockTrade).toBe(0);
        expect(RegulatoryTradeIDEvent.Allocation).toBe(1);
        expect(RegulatoryTradeIDEvent.Clearing).toBe(2);
        expect(RegulatoryTradeIDEvent.Compression).toBe(3);
        expect(RegulatoryTradeIDEvent.Novation).toBe(4);
        expect(RegulatoryTradeIDEvent.Termination).toBe(5);
        expect(RegulatoryTradeIDEvent.PostTrdVal).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegulatoryTradeIDEvent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegulatoryTradeIDEvent.InitialBlockTrade,
            RegulatoryTradeIDEvent.Allocation,
            RegulatoryTradeIDEvent.Clearing,
            RegulatoryTradeIDEvent.Compression,
            RegulatoryTradeIDEvent.Novation,
            RegulatoryTradeIDEvent.Termination,
            RegulatoryTradeIDEvent.PostTrdVal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegulatoryTradeIDEvent type', () => {
        const validate = (value: RegulatoryTradeIDEvent) => {
            expect(Object.values(RegulatoryTradeIDEvent)).toContain(value);
        };

        validate(RegulatoryTradeIDEvent.InitialBlockTrade);
        validate(RegulatoryTradeIDEvent.Allocation);
        validate(RegulatoryTradeIDEvent.Clearing);
        validate(RegulatoryTradeIDEvent.Compression);
        validate(RegulatoryTradeIDEvent.Novation);
        validate(RegulatoryTradeIDEvent.Termination);
        validate(RegulatoryTradeIDEvent.PostTrdVal);
    });

    test('should be immutable', () => {
        const ref = RegulatoryTradeIDEvent;
        expect(() => {
            ref.InitialBlockTrade = 10;
        }).toThrowError();
    });
});
