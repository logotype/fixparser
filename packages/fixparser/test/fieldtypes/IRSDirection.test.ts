import { IRSDirection } from '../../src/fieldtypes/IRSDirection';

describe('IRSDirection', () => {
    test('should have the correct values', () => {
        expect(IRSDirection.Pay).toBe('PAY');
        expect(IRSDirection.Rcv).toBe('RCV');
        expect(IRSDirection.NA).toBe('NA');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IRSDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IRSDirection.Pay, IRSDirection.Rcv, IRSDirection.NA];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IRSDirection type', () => {
        const validate = (value: IRSDirection) => {
            expect(Object.values(IRSDirection)).toContain(value);
        };

        validate(IRSDirection.Pay);
        validate(IRSDirection.Rcv);
        validate(IRSDirection.NA);
    });

    test('should be immutable', () => {
        const ref = IRSDirection;
        expect(() => {
            ref.Pay = 10;
        }).toThrowError();
    });
});
