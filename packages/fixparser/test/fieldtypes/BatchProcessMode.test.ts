import { BatchProcessMode } from '../../src/fieldtypes/BatchProcessMode';

describe('BatchProcessMode', () => {
    test('should have the correct values', () => {
        expect(BatchProcessMode.Update).toBe(0);
        expect(BatchProcessMode.Snapshot).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BatchProcessMode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BatchProcessMode.Update, BatchProcessMode.Snapshot];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BatchProcessMode type', () => {
        const validate = (value: BatchProcessMode) => {
            expect(Object.values(BatchProcessMode)).toContain(value);
        };

        validate(BatchProcessMode.Update);
        validate(BatchProcessMode.Snapshot);
    });

    test('should be immutable', () => {
        const ref = BatchProcessMode;
        expect(() => {
            ref.Update = 10;
        }).toThrowError();
    });
});
