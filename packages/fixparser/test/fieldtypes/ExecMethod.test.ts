import { ExecMethod } from '../../src/fieldtypes/ExecMethod';

describe('ExecMethod', () => {
    test('should have the correct values', () => {
        expect(ExecMethod.Unspecified).toBe(0);
        expect(ExecMethod.Manual).toBe(1);
        expect(ExecMethod.Automated).toBe(2);
        expect(ExecMethod.VoiceBrokered).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecMethod.Unspecified,
            ExecMethod.Manual,
            ExecMethod.Automated,
            ExecMethod.VoiceBrokered,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExecMethod type', () => {
        const validate = (value: ExecMethod) => {
            expect(Object.values(ExecMethod)).toContain(value);
        };

        validate(ExecMethod.Unspecified);
        validate(ExecMethod.Manual);
        validate(ExecMethod.Automated);
        validate(ExecMethod.VoiceBrokered);
    });

    test('should be immutable', () => {
        const ref = ExecMethod;
        expect(() => {
            ref.Unspecified = 10;
        }).toThrowError();
    });
});
