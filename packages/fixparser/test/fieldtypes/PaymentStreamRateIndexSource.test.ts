import { PaymentStreamRateIndexSource } from '../../src/fieldtypes/PaymentStreamRateIndexSource';

describe('PaymentStreamRateIndexSource', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRateIndexSource.Bloomberg).toBe(0);
        expect(PaymentStreamRateIndexSource.Reuters).toBe(1);
        expect(PaymentStreamRateIndexSource.Telerate).toBe(2);
        expect(PaymentStreamRateIndexSource.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRateIndexSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamRateIndexSource.Bloomberg,
            PaymentStreamRateIndexSource.Reuters,
            PaymentStreamRateIndexSource.Telerate,
            PaymentStreamRateIndexSource.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamRateIndexSource type', () => {
        const validate = (value: PaymentStreamRateIndexSource) => {
            expect(Object.values(PaymentStreamRateIndexSource)).toContain(value);
        };

        validate(PaymentStreamRateIndexSource.Bloomberg);
        validate(PaymentStreamRateIndexSource.Reuters);
        validate(PaymentStreamRateIndexSource.Telerate);
        validate(PaymentStreamRateIndexSource.Other);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRateIndexSource;
        expect(() => {
            ref.Bloomberg = 10;
        }).toThrowError();
    });
});
