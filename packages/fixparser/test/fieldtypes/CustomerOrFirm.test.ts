import { CustomerOrFirm } from '../../src/fieldtypes/CustomerOrFirm';

describe('CustomerOrFirm', () => {
    test('should have the correct values', () => {
        expect(CustomerOrFirm.Customer).toBe(0);
        expect(CustomerOrFirm.Firm).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CustomerOrFirm.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CustomerOrFirm.Customer, CustomerOrFirm.Firm];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CustomerOrFirm type', () => {
        const validate = (value: CustomerOrFirm) => {
            expect(Object.values(CustomerOrFirm)).toContain(value);
        };

        validate(CustomerOrFirm.Customer);
        validate(CustomerOrFirm.Firm);
    });

    test('should be immutable', () => {
        const ref = CustomerOrFirm;
        expect(() => {
            ref.Customer = 10;
        }).toThrowError();
    });
});
