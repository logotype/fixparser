import { AllocSettlInstType } from '../../src/fieldtypes/AllocSettlInstType';

describe('AllocSettlInstType', () => {
    test('should have the correct values', () => {
        expect(AllocSettlInstType.UseDefaultInstructions).toBe(0);
        expect(AllocSettlInstType.DeriveFromParametersProvided).toBe(1);
        expect(AllocSettlInstType.FullDetailsProvided).toBe(2);
        expect(AllocSettlInstType.SSIDBIDsProvided).toBe(3);
        expect(AllocSettlInstType.PhoneForInstructions).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocSettlInstType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocSettlInstType.UseDefaultInstructions,
            AllocSettlInstType.DeriveFromParametersProvided,
            AllocSettlInstType.FullDetailsProvided,
            AllocSettlInstType.SSIDBIDsProvided,
            AllocSettlInstType.PhoneForInstructions,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocSettlInstType type', () => {
        const validate = (value: AllocSettlInstType) => {
            expect(Object.values(AllocSettlInstType)).toContain(value);
        };

        validate(AllocSettlInstType.UseDefaultInstructions);
        validate(AllocSettlInstType.DeriveFromParametersProvided);
        validate(AllocSettlInstType.FullDetailsProvided);
        validate(AllocSettlInstType.SSIDBIDsProvided);
        validate(AllocSettlInstType.PhoneForInstructions);
    });

    test('should be immutable', () => {
        const ref = AllocSettlInstType;
        expect(() => {
            ref.UseDefaultInstructions = 10;
        }).toThrowError();
    });
});
