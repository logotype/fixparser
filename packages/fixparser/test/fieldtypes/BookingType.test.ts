import { BookingType } from '../../src/fieldtypes/BookingType';

describe('BookingType', () => {
    test('should have the correct values', () => {
        expect(BookingType.RegularBooking).toBe(0);
        expect(BookingType.CFD).toBe(1);
        expect(BookingType.TotalReturnSwap).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BookingType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BookingType.RegularBooking, BookingType.CFD, BookingType.TotalReturnSwap];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BookingType type', () => {
        const validate = (value: BookingType) => {
            expect(Object.values(BookingType)).toContain(value);
        };

        validate(BookingType.RegularBooking);
        validate(BookingType.CFD);
        validate(BookingType.TotalReturnSwap);
    });

    test('should be immutable', () => {
        const ref = BookingType;
        expect(() => {
            ref.RegularBooking = 10;
        }).toThrowError();
    });
});
