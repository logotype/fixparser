import { ContingencyType } from '../../src/fieldtypes/ContingencyType';

describe('ContingencyType', () => {
    test('should have the correct values', () => {
        expect(ContingencyType.OneCancelsTheOther).toBe(1);
        expect(ContingencyType.OneTriggersTheOther).toBe(2);
        expect(ContingencyType.OneUpdatesTheOtherAbsolute).toBe(3);
        expect(ContingencyType.OneUpdatesTheOtherProportional).toBe(4);
        expect(ContingencyType.BidAndOffer).toBe(5);
        expect(ContingencyType.BidAndOfferOCO).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ContingencyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ContingencyType.OneCancelsTheOther,
            ContingencyType.OneTriggersTheOther,
            ContingencyType.OneUpdatesTheOtherAbsolute,
            ContingencyType.OneUpdatesTheOtherProportional,
            ContingencyType.BidAndOffer,
            ContingencyType.BidAndOfferOCO,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ContingencyType type', () => {
        const validate = (value: ContingencyType) => {
            expect(Object.values(ContingencyType)).toContain(value);
        };

        validate(ContingencyType.OneCancelsTheOther);
        validate(ContingencyType.OneTriggersTheOther);
        validate(ContingencyType.OneUpdatesTheOtherAbsolute);
        validate(ContingencyType.OneUpdatesTheOtherProportional);
        validate(ContingencyType.BidAndOffer);
        validate(ContingencyType.BidAndOfferOCO);
    });

    test('should be immutable', () => {
        const ref = ContingencyType;
        expect(() => {
            ref.OneCancelsTheOther = 10;
        }).toThrowError();
    });
});
