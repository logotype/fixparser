import { ClearingFeeIndicator } from '../../src/fieldtypes/ClearingFeeIndicator';

describe('ClearingFeeIndicator', () => {
    test('should have the correct values', () => {
        expect(ClearingFeeIndicator.FirstYearDelegate).toBe('1');
        expect(ClearingFeeIndicator.SecondYearDelegate).toBe('2');
        expect(ClearingFeeIndicator.ThirdYearDelegate).toBe('3');
        expect(ClearingFeeIndicator.FourthYearDelegate).toBe('4');
        expect(ClearingFeeIndicator.FifthYearDelegate).toBe('5');
        expect(ClearingFeeIndicator.SixthYearDelegate).toBe('9');
        expect(ClearingFeeIndicator.CBOEMember).toBe('B');
        expect(ClearingFeeIndicator.NonMemberAndCustomer).toBe('C');
        expect(ClearingFeeIndicator.EquityMemberAndClearingMember).toBe('E');
        expect(ClearingFeeIndicator.FullAndAssociateMember).toBe('F');
        expect(ClearingFeeIndicator.Firms106HAnd106J).toBe('H');
        expect(ClearingFeeIndicator.GIM).toBe('I');
        expect(ClearingFeeIndicator.Lessee106FEmployees).toBe('L');
        expect(ClearingFeeIndicator.AllOtherOwnershipTypes).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearingFeeIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ClearingFeeIndicator.FirstYearDelegate,
            ClearingFeeIndicator.SecondYearDelegate,
            ClearingFeeIndicator.ThirdYearDelegate,
            ClearingFeeIndicator.FourthYearDelegate,
            ClearingFeeIndicator.FifthYearDelegate,
            ClearingFeeIndicator.SixthYearDelegate,
            ClearingFeeIndicator.CBOEMember,
            ClearingFeeIndicator.NonMemberAndCustomer,
            ClearingFeeIndicator.EquityMemberAndClearingMember,
            ClearingFeeIndicator.FullAndAssociateMember,
            ClearingFeeIndicator.Firms106HAnd106J,
            ClearingFeeIndicator.GIM,
            ClearingFeeIndicator.Lessee106FEmployees,
            ClearingFeeIndicator.AllOtherOwnershipTypes,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ClearingFeeIndicator type', () => {
        const validate = (value: ClearingFeeIndicator) => {
            expect(Object.values(ClearingFeeIndicator)).toContain(value);
        };

        validate(ClearingFeeIndicator.FirstYearDelegate);
        validate(ClearingFeeIndicator.SecondYearDelegate);
        validate(ClearingFeeIndicator.ThirdYearDelegate);
        validate(ClearingFeeIndicator.FourthYearDelegate);
        validate(ClearingFeeIndicator.FifthYearDelegate);
        validate(ClearingFeeIndicator.SixthYearDelegate);
        validate(ClearingFeeIndicator.CBOEMember);
        validate(ClearingFeeIndicator.NonMemberAndCustomer);
        validate(ClearingFeeIndicator.EquityMemberAndClearingMember);
        validate(ClearingFeeIndicator.FullAndAssociateMember);
        validate(ClearingFeeIndicator.Firms106HAnd106J);
        validate(ClearingFeeIndicator.GIM);
        validate(ClearingFeeIndicator.Lessee106FEmployees);
        validate(ClearingFeeIndicator.AllOtherOwnershipTypes);
    });

    test('should be immutable', () => {
        const ref = ClearingFeeIndicator;
        expect(() => {
            ref.FirstYearDelegate = 10;
        }).toThrowError();
    });
});
