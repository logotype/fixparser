import { AllocPositionEffect } from '../../src/fieldtypes/AllocPositionEffect';

describe('AllocPositionEffect', () => {
    test('should have the correct values', () => {
        expect(AllocPositionEffect.Open).toBe('O');
        expect(AllocPositionEffect.Close).toBe('C');
        expect(AllocPositionEffect.Rolled).toBe('R');
        expect(AllocPositionEffect.FIFO).toBe('F');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocPositionEffect.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocPositionEffect.Open,
            AllocPositionEffect.Close,
            AllocPositionEffect.Rolled,
            AllocPositionEffect.FIFO,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AllocPositionEffect type', () => {
        const validate = (value: AllocPositionEffect) => {
            expect(Object.values(AllocPositionEffect)).toContain(value);
        };

        validate(AllocPositionEffect.Open);
        validate(AllocPositionEffect.Close);
        validate(AllocPositionEffect.Rolled);
        validate(AllocPositionEffect.FIFO);
    });

    test('should be immutable', () => {
        const ref = AllocPositionEffect;
        expect(() => {
            ref.Open = 10;
        }).toThrowError();
    });
});
