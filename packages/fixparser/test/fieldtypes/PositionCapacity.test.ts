import { PositionCapacity } from '../../src/fieldtypes/PositionCapacity';

describe('PositionCapacity', () => {
    test('should have the correct values', () => {
        expect(PositionCapacity.Principal).toBe(0);
        expect(PositionCapacity.Agent).toBe(1);
        expect(PositionCapacity.Customer).toBe(2);
        expect(PositionCapacity.Counterparty).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PositionCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PositionCapacity.Principal,
            PositionCapacity.Agent,
            PositionCapacity.Customer,
            PositionCapacity.Counterparty,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PositionCapacity type', () => {
        const validate = (value: PositionCapacity) => {
            expect(Object.values(PositionCapacity)).toContain(value);
        };

        validate(PositionCapacity.Principal);
        validate(PositionCapacity.Agent);
        validate(PositionCapacity.Customer);
        validate(PositionCapacity.Counterparty);
    });

    test('should be immutable', () => {
        const ref = PositionCapacity;
        expect(() => {
            ref.Principal = 10;
        }).toThrowError();
    });
});
