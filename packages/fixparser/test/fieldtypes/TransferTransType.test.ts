import { TransferTransType } from '../../src/fieldtypes/TransferTransType';

describe('TransferTransType', () => {
    test('should have the correct values', () => {
        expect(TransferTransType.New).toBe(0);
        expect(TransferTransType.Replace).toBe(1);
        expect(TransferTransType.Cancel).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TransferTransType.New, TransferTransType.Replace, TransferTransType.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferTransType type', () => {
        const validate = (value: TransferTransType) => {
            expect(Object.values(TransferTransType)).toContain(value);
        };

        validate(TransferTransType.New);
        validate(TransferTransType.Replace);
        validate(TransferTransType.Cancel);
    });

    test('should be immutable', () => {
        const ref = TransferTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
