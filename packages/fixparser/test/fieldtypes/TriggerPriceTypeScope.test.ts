import { TriggerPriceTypeScope } from '../../src/fieldtypes/TriggerPriceTypeScope';

describe('TriggerPriceTypeScope', () => {
    test('should have the correct values', () => {
        expect(TriggerPriceTypeScope.None).toBe('0');
        expect(TriggerPriceTypeScope.Local).toBe('1');
        expect(TriggerPriceTypeScope.National).toBe('2');
        expect(TriggerPriceTypeScope.Global).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerPriceTypeScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TriggerPriceTypeScope.None,
            TriggerPriceTypeScope.Local,
            TriggerPriceTypeScope.National,
            TriggerPriceTypeScope.Global,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerPriceTypeScope type', () => {
        const validate = (value: TriggerPriceTypeScope) => {
            expect(Object.values(TriggerPriceTypeScope)).toContain(value);
        };

        validate(TriggerPriceTypeScope.None);
        validate(TriggerPriceTypeScope.Local);
        validate(TriggerPriceTypeScope.National);
        validate(TriggerPriceTypeScope.Global);
    });

    test('should be immutable', () => {
        const ref = TriggerPriceTypeScope;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
