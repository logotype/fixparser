import { ListStatusType } from '../../src/fieldtypes/ListStatusType';

describe('ListStatusType', () => {
    test('should have the correct values', () => {
        expect(ListStatusType.Ack).toBe(1);
        expect(ListStatusType.Response).toBe(2);
        expect(ListStatusType.Timed).toBe(3);
        expect(ListStatusType.ExecStarted).toBe(4);
        expect(ListStatusType.AllDone).toBe(5);
        expect(ListStatusType.Alert).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListStatusType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ListStatusType.Ack,
            ListStatusType.Response,
            ListStatusType.Timed,
            ListStatusType.ExecStarted,
            ListStatusType.AllDone,
            ListStatusType.Alert,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ListStatusType type', () => {
        const validate = (value: ListStatusType) => {
            expect(Object.values(ListStatusType)).toContain(value);
        };

        validate(ListStatusType.Ack);
        validate(ListStatusType.Response);
        validate(ListStatusType.Timed);
        validate(ListStatusType.ExecStarted);
        validate(ListStatusType.AllDone);
        validate(ListStatusType.Alert);
    });

    test('should be immutable', () => {
        const ref = ListStatusType;
        expect(() => {
            ref.Ack = 10;
        }).toThrowError();
    });
});
