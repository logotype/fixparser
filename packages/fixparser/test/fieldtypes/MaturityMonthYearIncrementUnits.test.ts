import { MaturityMonthYearIncrementUnits } from '../../src/fieldtypes/MaturityMonthYearIncrementUnits';

describe('MaturityMonthYearIncrementUnits', () => {
    test('should have the correct values', () => {
        expect(MaturityMonthYearIncrementUnits.Months).toBe(0);
        expect(MaturityMonthYearIncrementUnits.Days).toBe(1);
        expect(MaturityMonthYearIncrementUnits.Weeks).toBe(2);
        expect(MaturityMonthYearIncrementUnits.Years).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MaturityMonthYearIncrementUnits.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MaturityMonthYearIncrementUnits.Months,
            MaturityMonthYearIncrementUnits.Days,
            MaturityMonthYearIncrementUnits.Weeks,
            MaturityMonthYearIncrementUnits.Years,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MaturityMonthYearIncrementUnits type', () => {
        const validate = (value: MaturityMonthYearIncrementUnits) => {
            expect(Object.values(MaturityMonthYearIncrementUnits)).toContain(value);
        };

        validate(MaturityMonthYearIncrementUnits.Months);
        validate(MaturityMonthYearIncrementUnits.Days);
        validate(MaturityMonthYearIncrementUnits.Weeks);
        validate(MaturityMonthYearIncrementUnits.Years);
    });

    test('should be immutable', () => {
        const ref = MaturityMonthYearIncrementUnits;
        expect(() => {
            ref.Months = 10;
        }).toThrowError();
    });
});
