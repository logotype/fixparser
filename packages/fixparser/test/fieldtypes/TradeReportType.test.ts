import { TradeReportType } from '../../src/fieldtypes/TradeReportType';

describe('TradeReportType', () => {
    test('should have the correct values', () => {
        expect(TradeReportType.Submit).toBe(0);
        expect(TradeReportType.Alleged).toBe(1);
        expect(TradeReportType.Accept).toBe(2);
        expect(TradeReportType.Decline).toBe(3);
        expect(TradeReportType.Addendum).toBe(4);
        expect(TradeReportType.No).toBe(5);
        expect(TradeReportType.TradeReportCancel).toBe(6);
        expect(TradeReportType.LockedIn).toBe(7);
        expect(TradeReportType.Defaulted).toBe(8);
        expect(TradeReportType.InvalidCMTA).toBe(9);
        expect(TradeReportType.Pended).toBe(10);
        expect(TradeReportType.AllegedNew).toBe(11);
        expect(TradeReportType.AllegedAddendum).toBe(12);
        expect(TradeReportType.AllegedNo).toBe(13);
        expect(TradeReportType.AllegedTradeReportCancel).toBe(14);
        expect(TradeReportType.AllegedTradeBreak).toBe(15);
        expect(TradeReportType.Verify).toBe(16);
        expect(TradeReportType.Dispute).toBe(17);
        expect(TradeReportType.NonMaterialUpdate).toBe(18);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeReportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeReportType.Submit,
            TradeReportType.Alleged,
            TradeReportType.Accept,
            TradeReportType.Decline,
            TradeReportType.Addendum,
            TradeReportType.No,
            TradeReportType.TradeReportCancel,
            TradeReportType.LockedIn,
            TradeReportType.Defaulted,
            TradeReportType.InvalidCMTA,
            TradeReportType.Pended,
            TradeReportType.AllegedNew,
            TradeReportType.AllegedAddendum,
            TradeReportType.AllegedNo,
            TradeReportType.AllegedTradeReportCancel,
            TradeReportType.AllegedTradeBreak,
            TradeReportType.Verify,
            TradeReportType.Dispute,
            TradeReportType.NonMaterialUpdate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeReportType type', () => {
        const validate = (value: TradeReportType) => {
            expect(Object.values(TradeReportType)).toContain(value);
        };

        validate(TradeReportType.Submit);
        validate(TradeReportType.Alleged);
        validate(TradeReportType.Accept);
        validate(TradeReportType.Decline);
        validate(TradeReportType.Addendum);
        validate(TradeReportType.No);
        validate(TradeReportType.TradeReportCancel);
        validate(TradeReportType.LockedIn);
        validate(TradeReportType.Defaulted);
        validate(TradeReportType.InvalidCMTA);
        validate(TradeReportType.Pended);
        validate(TradeReportType.AllegedNew);
        validate(TradeReportType.AllegedAddendum);
        validate(TradeReportType.AllegedNo);
        validate(TradeReportType.AllegedTradeReportCancel);
        validate(TradeReportType.AllegedTradeBreak);
        validate(TradeReportType.Verify);
        validate(TradeReportType.Dispute);
        validate(TradeReportType.NonMaterialUpdate);
    });

    test('should be immutable', () => {
        const ref = TradeReportType;
        expect(() => {
            ref.Submit = 10;
        }).toThrowError();
    });
});
