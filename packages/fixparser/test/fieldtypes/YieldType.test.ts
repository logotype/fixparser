import { YieldType } from '../../src/fieldtypes/YieldType';

describe('YieldType', () => {
    test('should have the correct values', () => {
        expect(YieldType.AfterTaxYield).toBe('AFTERTAX');
        expect(YieldType.AnnualYield).toBe('ANNUAL');
        expect(YieldType.YieldAtIssue).toBe('ATISSUE');
        expect(YieldType.YieldToAverageMaturity).toBe('AVGMATURITY');
        expect(YieldType.BookYield).toBe('BOOK');
        expect(YieldType.YieldToNextCall).toBe('CALL');
        expect(YieldType.YieldChangeSinceClose).toBe('CHANGE');
        expect(YieldType.ClosingYield).toBe('CLOSE');
        expect(YieldType.CompoundYield).toBe('COMPOUND');
        expect(YieldType.CurrentYield).toBe('CURRENT');
        expect(YieldType.GvntEquivalentYield).toBe('GOVTEQUIV');
        expect(YieldType.TrueGrossYield).toBe('GROSS');
        expect(YieldType.YieldWithInflationAssumption).toBe('INFLATION');
        expect(YieldType.InverseFloaterBondYield).toBe('INVERSEFLOATER');
        expect(YieldType.MostRecentClosingYield).toBe('LASTCLOSE');
        expect(YieldType.ClosingYieldMostRecentMonth).toBe('LASTMONTH');
        expect(YieldType.ClosingYieldMostRecentQuarter).toBe('LASTQUARTER');
        expect(YieldType.ClosingYieldMostRecentYear).toBe('LASTYEAR');
        expect(YieldType.YieldToLongestAverageLife).toBe('LONGAVGLIFE');
        expect(YieldType.MarkToMarketYield).toBe('MARK');
        expect(YieldType.YieldToMaturity).toBe('MATURITY');
        expect(YieldType.YieldToNextRefund).toBe('NEXTREFUND');
        expect(YieldType.OpenAverageYield).toBe('OPENAVG');
        expect(YieldType.PreviousCloseYield).toBe('PREVCLOSE');
        expect(YieldType.ProceedsYield).toBe('PROCEEDS');
        expect(YieldType.YieldToNextPut).toBe('PUT');
        expect(YieldType.SemiAnnualYield).toBe('SEMIANNUAL');
        expect(YieldType.YieldToShortestAverageLife).toBe('SHORTAVGLIFE');
        expect(YieldType.SimpleYield).toBe('SIMPLE');
        expect(YieldType.TaxEquivalentYield).toBe('TAXEQUIV');
        expect(YieldType.YieldToTenderDate).toBe('TENDER');
        expect(YieldType.TrueYield).toBe('TRUE');
        expect(YieldType.YieldValueOf32nds).toBe('VALUE1_32');
        expect(YieldType.YieldToWorst).toBe('WORST');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            YieldType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            YieldType.AfterTaxYield,
            YieldType.AnnualYield,
            YieldType.YieldAtIssue,
            YieldType.YieldToAverageMaturity,
            YieldType.BookYield,
            YieldType.YieldToNextCall,
            YieldType.YieldChangeSinceClose,
            YieldType.ClosingYield,
            YieldType.CompoundYield,
            YieldType.CurrentYield,
            YieldType.GvntEquivalentYield,
            YieldType.TrueGrossYield,
            YieldType.YieldWithInflationAssumption,
            YieldType.InverseFloaterBondYield,
            YieldType.MostRecentClosingYield,
            YieldType.ClosingYieldMostRecentMonth,
            YieldType.ClosingYieldMostRecentQuarter,
            YieldType.ClosingYieldMostRecentYear,
            YieldType.YieldToLongestAverageLife,
            YieldType.MarkToMarketYield,
            YieldType.YieldToMaturity,
            YieldType.YieldToNextRefund,
            YieldType.OpenAverageYield,
            YieldType.PreviousCloseYield,
            YieldType.ProceedsYield,
            YieldType.YieldToNextPut,
            YieldType.SemiAnnualYield,
            YieldType.YieldToShortestAverageLife,
            YieldType.SimpleYield,
            YieldType.TaxEquivalentYield,
            YieldType.YieldToTenderDate,
            YieldType.TrueYield,
            YieldType.YieldValueOf32nds,
            YieldType.YieldToWorst,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for YieldType type', () => {
        const validate = (value: YieldType) => {
            expect(Object.values(YieldType)).toContain(value);
        };

        validate(YieldType.AfterTaxYield);
        validate(YieldType.AnnualYield);
        validate(YieldType.YieldAtIssue);
        validate(YieldType.YieldToAverageMaturity);
        validate(YieldType.BookYield);
        validate(YieldType.YieldToNextCall);
        validate(YieldType.YieldChangeSinceClose);
        validate(YieldType.ClosingYield);
        validate(YieldType.CompoundYield);
        validate(YieldType.CurrentYield);
        validate(YieldType.GvntEquivalentYield);
        validate(YieldType.TrueGrossYield);
        validate(YieldType.YieldWithInflationAssumption);
        validate(YieldType.InverseFloaterBondYield);
        validate(YieldType.MostRecentClosingYield);
        validate(YieldType.ClosingYieldMostRecentMonth);
        validate(YieldType.ClosingYieldMostRecentQuarter);
        validate(YieldType.ClosingYieldMostRecentYear);
        validate(YieldType.YieldToLongestAverageLife);
        validate(YieldType.MarkToMarketYield);
        validate(YieldType.YieldToMaturity);
        validate(YieldType.YieldToNextRefund);
        validate(YieldType.OpenAverageYield);
        validate(YieldType.PreviousCloseYield);
        validate(YieldType.ProceedsYield);
        validate(YieldType.YieldToNextPut);
        validate(YieldType.SemiAnnualYield);
        validate(YieldType.YieldToShortestAverageLife);
        validate(YieldType.SimpleYield);
        validate(YieldType.TaxEquivalentYield);
        validate(YieldType.YieldToTenderDate);
        validate(YieldType.TrueYield);
        validate(YieldType.YieldValueOf32nds);
        validate(YieldType.YieldToWorst);
    });

    test('should be immutable', () => {
        const ref = YieldType;
        expect(() => {
            ref.AfterTaxYield = 10;
        }).toThrowError();
    });
});
