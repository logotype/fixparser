import { ForexReq } from '../../src/fieldtypes/ForexReq';

describe('ForexReq', () => {
    test('should have the correct values', () => {
        expect(ForexReq.DoNotExecuteForexAfterSecurityTrade).toBe('N');
        expect(ForexReq.ExecuteForexAfterSecurityTrade).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ForexReq.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ForexReq.DoNotExecuteForexAfterSecurityTrade,
            ForexReq.ExecuteForexAfterSecurityTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ForexReq type', () => {
        const validate = (value: ForexReq) => {
            expect(Object.values(ForexReq)).toContain(value);
        };

        validate(ForexReq.DoNotExecuteForexAfterSecurityTrade);
        validate(ForexReq.ExecuteForexAfterSecurityTrade);
    });

    test('should be immutable', () => {
        const ref = ForexReq;
        expect(() => {
            ref.DoNotExecuteForexAfterSecurityTrade = 10;
        }).toThrowError();
    });
});
