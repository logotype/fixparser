import { SideMultiLegReportingType } from '../../src/fieldtypes/SideMultiLegReportingType';

describe('SideMultiLegReportingType', () => {
    test('should have the correct values', () => {
        expect(SideMultiLegReportingType.SingleSecurity).toBe(1);
        expect(SideMultiLegReportingType.IndividualLegOfAMultilegSecurity).toBe(2);
        expect(SideMultiLegReportingType.MultilegSecurity).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SideMultiLegReportingType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SideMultiLegReportingType.SingleSecurity,
            SideMultiLegReportingType.IndividualLegOfAMultilegSecurity,
            SideMultiLegReportingType.MultilegSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SideMultiLegReportingType type', () => {
        const validate = (value: SideMultiLegReportingType) => {
            expect(Object.values(SideMultiLegReportingType)).toContain(value);
        };

        validate(SideMultiLegReportingType.SingleSecurity);
        validate(SideMultiLegReportingType.IndividualLegOfAMultilegSecurity);
        validate(SideMultiLegReportingType.MultilegSecurity);
    });

    test('should be immutable', () => {
        const ref = SideMultiLegReportingType;
        expect(() => {
            ref.SingleSecurity = 10;
        }).toThrowError();
    });
});
