import { RefRiskLimitCheckIDType } from '../../src/fieldtypes/RefRiskLimitCheckIDType';

describe('RefRiskLimitCheckIDType', () => {
    test('should have the correct values', () => {
        expect(RefRiskLimitCheckIDType.RiskLimitRequestID).toBe(0);
        expect(RefRiskLimitCheckIDType.RiskLimitCheckID).toBe(1);
        expect(RefRiskLimitCheckIDType.OutOfBandID).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RefRiskLimitCheckIDType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RefRiskLimitCheckIDType.RiskLimitRequestID,
            RefRiskLimitCheckIDType.RiskLimitCheckID,
            RefRiskLimitCheckIDType.OutOfBandID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RefRiskLimitCheckIDType type', () => {
        const validate = (value: RefRiskLimitCheckIDType) => {
            expect(Object.values(RefRiskLimitCheckIDType)).toContain(value);
        };

        validate(RefRiskLimitCheckIDType.RiskLimitRequestID);
        validate(RefRiskLimitCheckIDType.RiskLimitCheckID);
        validate(RefRiskLimitCheckIDType.OutOfBandID);
    });

    test('should be immutable', () => {
        const ref = RefRiskLimitCheckIDType;
        expect(() => {
            ref.RiskLimitRequestID = 10;
        }).toThrowError();
    });
});
