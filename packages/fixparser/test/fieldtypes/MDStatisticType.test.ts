import { MDStatisticType } from '../../src/fieldtypes/MDStatisticType';

describe('MDStatisticType', () => {
    test('should have the correct values', () => {
        expect(MDStatisticType.Count).toBe(1);
        expect(MDStatisticType.AverageVolume).toBe(2);
        expect(MDStatisticType.TotalVolume).toBe(3);
        expect(MDStatisticType.Distribution).toBe(4);
        expect(MDStatisticType.Ratio).toBe(5);
        expect(MDStatisticType.Liquidity).toBe(6);
        expect(MDStatisticType.VWAP).toBe(7);
        expect(MDStatisticType.Volatility).toBe(8);
        expect(MDStatisticType.Duration).toBe(9);
        expect(MDStatisticType.Tick).toBe(10);
        expect(MDStatisticType.AverageValue).toBe(11);
        expect(MDStatisticType.TotalValue).toBe(12);
        expect(MDStatisticType.High).toBe(13);
        expect(MDStatisticType.Low).toBe(14);
        expect(MDStatisticType.Midpoint).toBe(15);
        expect(MDStatisticType.First).toBe(16);
        expect(MDStatisticType.Last).toBe(17);
        expect(MDStatisticType.Final).toBe(18);
        expect(MDStatisticType.ExchangeBest).toBe(19);
        expect(MDStatisticType.ExchangeBestWithVolume).toBe(20);
        expect(MDStatisticType.ConsolidatedBest).toBe(21);
        expect(MDStatisticType.ConsolidatedBestWithVolume).toBe(22);
        expect(MDStatisticType.TWAP).toBe(23);
        expect(MDStatisticType.AverageDuration).toBe(24);
        expect(MDStatisticType.AveragePrice).toBe(25);
        expect(MDStatisticType.TotalFees).toBe(26);
        expect(MDStatisticType.TotalBenefits).toBe(27);
        expect(MDStatisticType.MedianValue).toBe(28);
        expect(MDStatisticType.AverageLiquidity).toBe(29);
        expect(MDStatisticType.MedianDuration).toBe(30);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticType.Count,
            MDStatisticType.AverageVolume,
            MDStatisticType.TotalVolume,
            MDStatisticType.Distribution,
            MDStatisticType.Ratio,
            MDStatisticType.Liquidity,
            MDStatisticType.VWAP,
            MDStatisticType.Volatility,
            MDStatisticType.Duration,
            MDStatisticType.Tick,
            MDStatisticType.AverageValue,
            MDStatisticType.TotalValue,
            MDStatisticType.High,
            MDStatisticType.Low,
            MDStatisticType.Midpoint,
            MDStatisticType.First,
            MDStatisticType.Last,
            MDStatisticType.Final,
            MDStatisticType.ExchangeBest,
            MDStatisticType.ExchangeBestWithVolume,
            MDStatisticType.ConsolidatedBest,
            MDStatisticType.ConsolidatedBestWithVolume,
            MDStatisticType.TWAP,
            MDStatisticType.AverageDuration,
            MDStatisticType.AveragePrice,
            MDStatisticType.TotalFees,
            MDStatisticType.TotalBenefits,
            MDStatisticType.MedianValue,
            MDStatisticType.AverageLiquidity,
            MDStatisticType.MedianDuration,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticType type', () => {
        const validate = (value: MDStatisticType) => {
            expect(Object.values(MDStatisticType)).toContain(value);
        };

        validate(MDStatisticType.Count);
        validate(MDStatisticType.AverageVolume);
        validate(MDStatisticType.TotalVolume);
        validate(MDStatisticType.Distribution);
        validate(MDStatisticType.Ratio);
        validate(MDStatisticType.Liquidity);
        validate(MDStatisticType.VWAP);
        validate(MDStatisticType.Volatility);
        validate(MDStatisticType.Duration);
        validate(MDStatisticType.Tick);
        validate(MDStatisticType.AverageValue);
        validate(MDStatisticType.TotalValue);
        validate(MDStatisticType.High);
        validate(MDStatisticType.Low);
        validate(MDStatisticType.Midpoint);
        validate(MDStatisticType.First);
        validate(MDStatisticType.Last);
        validate(MDStatisticType.Final);
        validate(MDStatisticType.ExchangeBest);
        validate(MDStatisticType.ExchangeBestWithVolume);
        validate(MDStatisticType.ConsolidatedBest);
        validate(MDStatisticType.ConsolidatedBestWithVolume);
        validate(MDStatisticType.TWAP);
        validate(MDStatisticType.AverageDuration);
        validate(MDStatisticType.AveragePrice);
        validate(MDStatisticType.TotalFees);
        validate(MDStatisticType.TotalBenefits);
        validate(MDStatisticType.MedianValue);
        validate(MDStatisticType.AverageLiquidity);
        validate(MDStatisticType.MedianDuration);
    });

    test('should be immutable', () => {
        const ref = MDStatisticType;
        expect(() => {
            ref.Count = 10;
        }).toThrowError();
    });
});
