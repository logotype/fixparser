import { MinQtyMethod } from '../../src/fieldtypes/MinQtyMethod';

describe('MinQtyMethod', () => {
    test('should have the correct values', () => {
        expect(MinQtyMethod.Once).toBe(1);
        expect(MinQtyMethod.Multiple).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MinQtyMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MinQtyMethod.Once, MinQtyMethod.Multiple];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MinQtyMethod type', () => {
        const validate = (value: MinQtyMethod) => {
            expect(Object.values(MinQtyMethod)).toContain(value);
        };

        validate(MinQtyMethod.Once);
        validate(MinQtyMethod.Multiple);
    });

    test('should be immutable', () => {
        const ref = MinQtyMethod;
        expect(() => {
            ref.Once = 10;
        }).toThrowError();
    });
});
