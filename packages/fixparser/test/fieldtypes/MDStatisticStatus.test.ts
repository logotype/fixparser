import { MDStatisticStatus } from '../../src/fieldtypes/MDStatisticStatus';

describe('MDStatisticStatus', () => {
    test('should have the correct values', () => {
        expect(MDStatisticStatus.Active).toBe(1);
        expect(MDStatisticStatus.Inactive).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDStatisticStatus.Active, MDStatisticStatus.Inactive];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticStatus type', () => {
        const validate = (value: MDStatisticStatus) => {
            expect(Object.values(MDStatisticStatus)).toContain(value);
        };

        validate(MDStatisticStatus.Active);
        validate(MDStatisticStatus.Inactive);
    });

    test('should be immutable', () => {
        const ref = MDStatisticStatus;
        expect(() => {
            ref.Active = 10;
        }).toThrowError();
    });
});
