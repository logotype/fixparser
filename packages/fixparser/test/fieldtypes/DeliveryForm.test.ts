import { DeliveryForm } from '../../src/fieldtypes/DeliveryForm';

describe('DeliveryForm', () => {
    test('should have the correct values', () => {
        expect(DeliveryForm.BookEntry).toBe(1);
        expect(DeliveryForm.Bearer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryForm.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryForm.BookEntry, DeliveryForm.Bearer];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryForm type', () => {
        const validate = (value: DeliveryForm) => {
            expect(Object.values(DeliveryForm)).toContain(value);
        };

        validate(DeliveryForm.BookEntry);
        validate(DeliveryForm.Bearer);
    });

    test('should be immutable', () => {
        const ref = DeliveryForm;
        expect(() => {
            ref.BookEntry = 10;
        }).toThrowError();
    });
});
