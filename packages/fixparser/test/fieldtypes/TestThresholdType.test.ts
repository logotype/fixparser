import { TestThresholdType } from '../../src/fieldtypes/TestThresholdType';

describe('TestThresholdType', () => {
    test('should have the correct values', () => {
        expect(TestThresholdType.Under).toBe(0);
        expect(TestThresholdType.Over).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestThresholdType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TestThresholdType.Under, TestThresholdType.Over];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestThresholdType type', () => {
        const validate = (value: TestThresholdType) => {
            expect(Object.values(TestThresholdType)).toContain(value);
        };

        validate(TestThresholdType.Under);
        validate(TestThresholdType.Over);
    });

    test('should be immutable', () => {
        const ref = TestThresholdType;
        expect(() => {
            ref.Under = 10;
        }).toThrowError();
    });
});
