import { QuoteStatus } from '../../src/fieldtypes/QuoteStatus';

describe('QuoteStatus', () => {
    test('should have the correct values', () => {
        expect(QuoteStatus.Accepted).toBe(0);
        expect(QuoteStatus.CancelForSymbol).toBe(1);
        expect(QuoteStatus.CanceledForSecurityType).toBe(2);
        expect(QuoteStatus.CanceledForUnderlying).toBe(3);
        expect(QuoteStatus.CanceledAll).toBe(4);
        expect(QuoteStatus.Rejected).toBe(5);
        expect(QuoteStatus.RemovedFromMarket).toBe(6);
        expect(QuoteStatus.Expired).toBe(7);
        expect(QuoteStatus.Query).toBe(8);
        expect(QuoteStatus.QuoteNotFound).toBe(9);
        expect(QuoteStatus.Pending).toBe(10);
        expect(QuoteStatus.Pass).toBe(11);
        expect(QuoteStatus.LockedMarketWarning).toBe(12);
        expect(QuoteStatus.CrossMarketWarning).toBe(13);
        expect(QuoteStatus.CanceledDueToLockMarket).toBe(14);
        expect(QuoteStatus.CanceledDueToCrossMarket).toBe(15);
        expect(QuoteStatus.Active).toBe(16);
        expect(QuoteStatus.Canceled).toBe(17);
        expect(QuoteStatus.UnsolicitedQuoteReplenishment).toBe(18);
        expect(QuoteStatus.PendingEndTrade).toBe(19);
        expect(QuoteStatus.TooLateToEnd).toBe(20);
        expect(QuoteStatus.Traded).toBe(21);
        expect(QuoteStatus.TradedAndRemoved).toBe(22);
        expect(QuoteStatus.ContractTerminates).toBe(23);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteStatus.Accepted,
            QuoteStatus.CancelForSymbol,
            QuoteStatus.CanceledForSecurityType,
            QuoteStatus.CanceledForUnderlying,
            QuoteStatus.CanceledAll,
            QuoteStatus.Rejected,
            QuoteStatus.RemovedFromMarket,
            QuoteStatus.Expired,
            QuoteStatus.Query,
            QuoteStatus.QuoteNotFound,
            QuoteStatus.Pending,
            QuoteStatus.Pass,
            QuoteStatus.LockedMarketWarning,
            QuoteStatus.CrossMarketWarning,
            QuoteStatus.CanceledDueToLockMarket,
            QuoteStatus.CanceledDueToCrossMarket,
            QuoteStatus.Active,
            QuoteStatus.Canceled,
            QuoteStatus.UnsolicitedQuoteReplenishment,
            QuoteStatus.PendingEndTrade,
            QuoteStatus.TooLateToEnd,
            QuoteStatus.Traded,
            QuoteStatus.TradedAndRemoved,
            QuoteStatus.ContractTerminates,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteStatus type', () => {
        const validate = (value: QuoteStatus) => {
            expect(Object.values(QuoteStatus)).toContain(value);
        };

        validate(QuoteStatus.Accepted);
        validate(QuoteStatus.CancelForSymbol);
        validate(QuoteStatus.CanceledForSecurityType);
        validate(QuoteStatus.CanceledForUnderlying);
        validate(QuoteStatus.CanceledAll);
        validate(QuoteStatus.Rejected);
        validate(QuoteStatus.RemovedFromMarket);
        validate(QuoteStatus.Expired);
        validate(QuoteStatus.Query);
        validate(QuoteStatus.QuoteNotFound);
        validate(QuoteStatus.Pending);
        validate(QuoteStatus.Pass);
        validate(QuoteStatus.LockedMarketWarning);
        validate(QuoteStatus.CrossMarketWarning);
        validate(QuoteStatus.CanceledDueToLockMarket);
        validate(QuoteStatus.CanceledDueToCrossMarket);
        validate(QuoteStatus.Active);
        validate(QuoteStatus.Canceled);
        validate(QuoteStatus.UnsolicitedQuoteReplenishment);
        validate(QuoteStatus.PendingEndTrade);
        validate(QuoteStatus.TooLateToEnd);
        validate(QuoteStatus.Traded);
        validate(QuoteStatus.TradedAndRemoved);
        validate(QuoteStatus.ContractTerminates);
    });

    test('should be immutable', () => {
        const ref = QuoteStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
