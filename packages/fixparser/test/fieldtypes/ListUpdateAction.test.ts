import { ListUpdateAction } from '../../src/fieldtypes/ListUpdateAction';

describe('ListUpdateAction', () => {
    test('should have the correct values', () => {
        expect(ListUpdateAction.Add).toBe('A');
        expect(ListUpdateAction.Delete).toBe('D');
        expect(ListUpdateAction.Modify).toBe('M');
        expect(ListUpdateAction.Snapshot).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListUpdateAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ListUpdateAction.Add,
            ListUpdateAction.Delete,
            ListUpdateAction.Modify,
            ListUpdateAction.Snapshot,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ListUpdateAction type', () => {
        const validate = (value: ListUpdateAction) => {
            expect(Object.values(ListUpdateAction)).toContain(value);
        };

        validate(ListUpdateAction.Add);
        validate(ListUpdateAction.Delete);
        validate(ListUpdateAction.Modify);
        validate(ListUpdateAction.Snapshot);
    });

    test('should be immutable', () => {
        const ref = ListUpdateAction;
        expect(() => {
            ref.Add = 10;
        }).toThrowError();
    });
});
