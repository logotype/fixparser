import { ThrottleInst } from '../../src/fieldtypes/ThrottleInst';

describe('ThrottleInst', () => {
    test('should have the correct values', () => {
        expect(ThrottleInst.RejectIfThrottleLimitExceeded).toBe(0);
        expect(ThrottleInst.QueueIfThrottleLimitExceeded).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ThrottleInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ThrottleInst.RejectIfThrottleLimitExceeded,
            ThrottleInst.QueueIfThrottleLimitExceeded,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ThrottleInst type', () => {
        const validate = (value: ThrottleInst) => {
            expect(Object.values(ThrottleInst)).toContain(value);
        };

        validate(ThrottleInst.RejectIfThrottleLimitExceeded);
        validate(ThrottleInst.QueueIfThrottleLimitExceeded);
    });

    test('should be immutable', () => {
        const ref = ThrottleInst;
        expect(() => {
            ref.RejectIfThrottleLimitExceeded = 10;
        }).toThrowError();
    });
});
