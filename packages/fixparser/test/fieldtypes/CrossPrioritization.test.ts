import { CrossPrioritization } from '../../src/fieldtypes/CrossPrioritization';

describe('CrossPrioritization', () => {
    test('should have the correct values', () => {
        expect(CrossPrioritization.None).toBe(0);
        expect(CrossPrioritization.BuySideIsPrioritized).toBe(1);
        expect(CrossPrioritization.SellSideIsPrioritized).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CrossPrioritization.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CrossPrioritization.None,
            CrossPrioritization.BuySideIsPrioritized,
            CrossPrioritization.SellSideIsPrioritized,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CrossPrioritization type', () => {
        const validate = (value: CrossPrioritization) => {
            expect(Object.values(CrossPrioritization)).toContain(value);
        };

        validate(CrossPrioritization.None);
        validate(CrossPrioritization.BuySideIsPrioritized);
        validate(CrossPrioritization.SellSideIsPrioritized);
    });

    test('should be immutable', () => {
        const ref = CrossPrioritization;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
