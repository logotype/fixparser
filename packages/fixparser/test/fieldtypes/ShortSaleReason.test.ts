import { ShortSaleReason } from '../../src/fieldtypes/ShortSaleReason';

describe('ShortSaleReason', () => {
    test('should have the correct values', () => {
        expect(ShortSaleReason.DealerSoldShort).toBe(0);
        expect(ShortSaleReason.DealerSoldShortExempt).toBe(1);
        expect(ShortSaleReason.SellingCustomerSoldShort).toBe(2);
        expect(ShortSaleReason.SellingCustomerSoldShortExempt).toBe(3);
        expect(ShortSaleReason.QualifiedServiceRepresentative).toBe(4);
        expect(ShortSaleReason.QSROrAGUContraSideSoldShortExempt).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ShortSaleReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ShortSaleReason.DealerSoldShort,
            ShortSaleReason.DealerSoldShortExempt,
            ShortSaleReason.SellingCustomerSoldShort,
            ShortSaleReason.SellingCustomerSoldShortExempt,
            ShortSaleReason.QualifiedServiceRepresentative,
            ShortSaleReason.QSROrAGUContraSideSoldShortExempt,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ShortSaleReason type', () => {
        const validate = (value: ShortSaleReason) => {
            expect(Object.values(ShortSaleReason)).toContain(value);
        };

        validate(ShortSaleReason.DealerSoldShort);
        validate(ShortSaleReason.DealerSoldShortExempt);
        validate(ShortSaleReason.SellingCustomerSoldShort);
        validate(ShortSaleReason.SellingCustomerSoldShortExempt);
        validate(ShortSaleReason.QualifiedServiceRepresentative);
        validate(ShortSaleReason.QSROrAGUContraSideSoldShortExempt);
    });

    test('should be immutable', () => {
        const ref = ShortSaleReason;
        expect(() => {
            ref.DealerSoldShort = 10;
        }).toThrowError();
    });
});
