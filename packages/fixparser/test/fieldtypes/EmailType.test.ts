import { EmailType } from '../../src/fieldtypes/EmailType';

describe('EmailType', () => {
    test('should have the correct values', () => {
        expect(EmailType.New).toBe('0');
        expect(EmailType.Reply).toBe('1');
        expect(EmailType.AdminReply).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EmailType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [EmailType.New, EmailType.Reply, EmailType.AdminReply];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for EmailType type', () => {
        const validate = (value: EmailType) => {
            expect(Object.values(EmailType)).toContain(value);
        };

        validate(EmailType.New);
        validate(EmailType.Reply);
        validate(EmailType.AdminReply);
    });

    test('should be immutable', () => {
        const ref = EmailType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
