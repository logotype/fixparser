import { TradingSessionSubID } from '../../src/fieldtypes/TradingSessionSubID';

describe('TradingSessionSubID', () => {
    test('should have the correct values', () => {
        expect(TradingSessionSubID.PreTrading).toBe('1');
        expect(TradingSessionSubID.OpeningOrOpeningAuction).toBe('2');
        expect(TradingSessionSubID.Continuous).toBe('3');
        expect(TradingSessionSubID.ClosingOrClosingAuction).toBe('4');
        expect(TradingSessionSubID.PostTrading).toBe('5');
        expect(TradingSessionSubID.ScheduledIntradayAuction).toBe('6');
        expect(TradingSessionSubID.Quiescent).toBe('7');
        expect(TradingSessionSubID.AnyAuction).toBe('8');
        expect(TradingSessionSubID.UnscheduledIntradayAuction).toBe('9');
        expect(TradingSessionSubID.OutOfMainSessionTrading).toBe('10');
        expect(TradingSessionSubID.PrivateAuction).toBe('11');
        expect(TradingSessionSubID.PublicAuction).toBe('12');
        expect(TradingSessionSubID.GroupAuction).toBe('13');
        expect(TradingSessionSubID.OrderInitiatedAuction).toBe('14');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradingSessionSubID.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradingSessionSubID.PreTrading,
            TradingSessionSubID.OpeningOrOpeningAuction,
            TradingSessionSubID.Continuous,
            TradingSessionSubID.ClosingOrClosingAuction,
            TradingSessionSubID.PostTrading,
            TradingSessionSubID.ScheduledIntradayAuction,
            TradingSessionSubID.Quiescent,
            TradingSessionSubID.AnyAuction,
            TradingSessionSubID.UnscheduledIntradayAuction,
            TradingSessionSubID.OutOfMainSessionTrading,
            TradingSessionSubID.PrivateAuction,
            TradingSessionSubID.PublicAuction,
            TradingSessionSubID.GroupAuction,
            TradingSessionSubID.OrderInitiatedAuction,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TradingSessionSubID type', () => {
        const validate = (value: TradingSessionSubID) => {
            expect(Object.values(TradingSessionSubID)).toContain(value);
        };

        validate(TradingSessionSubID.PreTrading);
        validate(TradingSessionSubID.OpeningOrOpeningAuction);
        validate(TradingSessionSubID.Continuous);
        validate(TradingSessionSubID.ClosingOrClosingAuction);
        validate(TradingSessionSubID.PostTrading);
        validate(TradingSessionSubID.ScheduledIntradayAuction);
        validate(TradingSessionSubID.Quiescent);
        validate(TradingSessionSubID.AnyAuction);
        validate(TradingSessionSubID.UnscheduledIntradayAuction);
        validate(TradingSessionSubID.OutOfMainSessionTrading);
        validate(TradingSessionSubID.PrivateAuction);
        validate(TradingSessionSubID.PublicAuction);
        validate(TradingSessionSubID.GroupAuction);
        validate(TradingSessionSubID.OrderInitiatedAuction);
    });

    test('should be immutable', () => {
        const ref = TradingSessionSubID;
        expect(() => {
            ref.PreTrading = 10;
        }).toThrowError();
    });
});
