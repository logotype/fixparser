import { CustOrderCapacity } from '../../src/fieldtypes/CustOrderCapacity';

describe('CustOrderCapacity', () => {
    test('should have the correct values', () => {
        expect(CustOrderCapacity.MemberTradingForTheirOwnAccount).toBe(1);
        expect(CustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount).toBe(2);
        expect(CustOrderCapacity.MemberTradingForAnotherMember).toBe(3);
        expect(CustOrderCapacity.AllOther).toBe(4);
        expect(CustOrderCapacity.RetailCustomer).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CustOrderCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CustOrderCapacity.MemberTradingForTheirOwnAccount,
            CustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount,
            CustOrderCapacity.MemberTradingForAnotherMember,
            CustOrderCapacity.AllOther,
            CustOrderCapacity.RetailCustomer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CustOrderCapacity type', () => {
        const validate = (value: CustOrderCapacity) => {
            expect(Object.values(CustOrderCapacity)).toContain(value);
        };

        validate(CustOrderCapacity.MemberTradingForTheirOwnAccount);
        validate(CustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount);
        validate(CustOrderCapacity.MemberTradingForAnotherMember);
        validate(CustOrderCapacity.AllOther);
        validate(CustOrderCapacity.RetailCustomer);
    });

    test('should be immutable', () => {
        const ref = CustOrderCapacity;
        expect(() => {
            ref.MemberTradingForTheirOwnAccount = 10;
        }).toThrowError();
    });
});
