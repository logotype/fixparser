import { CommissionAmountSubType } from '../../src/fieldtypes/CommissionAmountSubType';

describe('CommissionAmountSubType', () => {
    test('should have the correct values', () => {
        expect(CommissionAmountSubType.ResearchPaymentAccount).toBe(0);
        expect(CommissionAmountSubType.CommissionSharingAgreement).toBe(1);
        expect(CommissionAmountSubType.OtherTypeResearchPayment).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CommissionAmountSubType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CommissionAmountSubType.ResearchPaymentAccount,
            CommissionAmountSubType.CommissionSharingAgreement,
            CommissionAmountSubType.OtherTypeResearchPayment,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CommissionAmountSubType type', () => {
        const validate = (value: CommissionAmountSubType) => {
            expect(Object.values(CommissionAmountSubType)).toContain(value);
        };

        validate(CommissionAmountSubType.ResearchPaymentAccount);
        validate(CommissionAmountSubType.CommissionSharingAgreement);
        validate(CommissionAmountSubType.OtherTypeResearchPayment);
    });

    test('should be immutable', () => {
        const ref = CommissionAmountSubType;
        expect(() => {
            ref.ResearchPaymentAccount = 10;
        }).toThrowError();
    });
});
