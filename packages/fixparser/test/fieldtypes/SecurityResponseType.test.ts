import { SecurityResponseType } from '../../src/fieldtypes/SecurityResponseType';

describe('SecurityResponseType', () => {
    test('should have the correct values', () => {
        expect(SecurityResponseType.AcceptAsIs).toBe(1);
        expect(SecurityResponseType.AcceptWithRevisions).toBe(2);
        expect(SecurityResponseType.ListOfSecurityTypesReturnedPerRequest).toBe(3);
        expect(SecurityResponseType.ListOfSecuritiesReturnedPerRequest).toBe(4);
        expect(SecurityResponseType.RejectSecurityProposal).toBe(5);
        expect(SecurityResponseType.CannotMatchSelectionCriteria).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityResponseType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityResponseType.AcceptAsIs,
            SecurityResponseType.AcceptWithRevisions,
            SecurityResponseType.ListOfSecurityTypesReturnedPerRequest,
            SecurityResponseType.ListOfSecuritiesReturnedPerRequest,
            SecurityResponseType.RejectSecurityProposal,
            SecurityResponseType.CannotMatchSelectionCriteria,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityResponseType type', () => {
        const validate = (value: SecurityResponseType) => {
            expect(Object.values(SecurityResponseType)).toContain(value);
        };

        validate(SecurityResponseType.AcceptAsIs);
        validate(SecurityResponseType.AcceptWithRevisions);
        validate(SecurityResponseType.ListOfSecurityTypesReturnedPerRequest);
        validate(SecurityResponseType.ListOfSecuritiesReturnedPerRequest);
        validate(SecurityResponseType.RejectSecurityProposal);
        validate(SecurityResponseType.CannotMatchSelectionCriteria);
    });

    test('should be immutable', () => {
        const ref = SecurityResponseType;
        expect(() => {
            ref.AcceptAsIs = 10;
        }).toThrowError();
    });
});
