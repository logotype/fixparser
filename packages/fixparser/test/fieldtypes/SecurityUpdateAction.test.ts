import { SecurityUpdateAction } from '../../src/fieldtypes/SecurityUpdateAction';

describe('SecurityUpdateAction', () => {
    test('should have the correct values', () => {
        expect(SecurityUpdateAction.Add).toBe('A');
        expect(SecurityUpdateAction.Delete).toBe('D');
        expect(SecurityUpdateAction.Modify).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityUpdateAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SecurityUpdateAction.Add, SecurityUpdateAction.Delete, SecurityUpdateAction.Modify];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SecurityUpdateAction type', () => {
        const validate = (value: SecurityUpdateAction) => {
            expect(Object.values(SecurityUpdateAction)).toContain(value);
        };

        validate(SecurityUpdateAction.Add);
        validate(SecurityUpdateAction.Delete);
        validate(SecurityUpdateAction.Modify);
    });

    test('should be immutable', () => {
        const ref = SecurityUpdateAction;
        expect(() => {
            ref.Add = 10;
        }).toThrowError();
    });
});
