import { SideClearingTradePriceType } from '../../src/fieldtypes/SideClearingTradePriceType';

describe('SideClearingTradePriceType', () => {
    test('should have the correct values', () => {
        expect(SideClearingTradePriceType.TradeClearingAtExecutionPrice).toBe(0);
        expect(SideClearingTradePriceType.TradeClearingAtAlternateClearingPrice).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SideClearingTradePriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SideClearingTradePriceType.TradeClearingAtExecutionPrice,
            SideClearingTradePriceType.TradeClearingAtAlternateClearingPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SideClearingTradePriceType type', () => {
        const validate = (value: SideClearingTradePriceType) => {
            expect(Object.values(SideClearingTradePriceType)).toContain(value);
        };

        validate(SideClearingTradePriceType.TradeClearingAtExecutionPrice);
        validate(SideClearingTradePriceType.TradeClearingAtAlternateClearingPrice);
    });

    test('should be immutable', () => {
        const ref = SideClearingTradePriceType;
        expect(() => {
            ref.TradeClearingAtExecutionPrice = 10;
        }).toThrowError();
    });
});
