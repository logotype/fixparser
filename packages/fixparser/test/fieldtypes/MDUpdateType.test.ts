import { MDUpdateType } from '../../src/fieldtypes/MDUpdateType';

describe('MDUpdateType', () => {
    test('should have the correct values', () => {
        expect(MDUpdateType.FullRefresh).toBe(0);
        expect(MDUpdateType.IncrementalRefresh).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDUpdateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDUpdateType.FullRefresh, MDUpdateType.IncrementalRefresh];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDUpdateType type', () => {
        const validate = (value: MDUpdateType) => {
            expect(Object.values(MDUpdateType)).toContain(value);
        };

        validate(MDUpdateType.FullRefresh);
        validate(MDUpdateType.IncrementalRefresh);
    });

    test('should be immutable', () => {
        const ref = MDUpdateType;
        expect(() => {
            ref.FullRefresh = 10;
        }).toThrowError();
    });
});
