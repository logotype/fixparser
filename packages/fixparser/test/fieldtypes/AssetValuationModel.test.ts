import { AssetValuationModel } from '../../src/fieldtypes/AssetValuationModel';

describe('AssetValuationModel', () => {
    test('should have the correct values', () => {
        expect(AssetValuationModel.BlackScholes).toBe(1);
        expect(AssetValuationModel.Whaley).toBe(2);
        expect(AssetValuationModel.Bachelier).toBe(3);
        expect(AssetValuationModel.Kirk).toBe(4);
        expect(AssetValuationModel.Curran).toBe(5);
        expect(AssetValuationModel.Black76).toBe(6);
        expect(AssetValuationModel.Binomial).toBe(7);
        expect(AssetValuationModel.OtherModel).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AssetValuationModel.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AssetValuationModel.BlackScholes,
            AssetValuationModel.Whaley,
            AssetValuationModel.Bachelier,
            AssetValuationModel.Kirk,
            AssetValuationModel.Curran,
            AssetValuationModel.Black76,
            AssetValuationModel.Binomial,
            AssetValuationModel.OtherModel,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AssetValuationModel type', () => {
        const validate = (value: AssetValuationModel) => {
            expect(Object.values(AssetValuationModel)).toContain(value);
        };

        validate(AssetValuationModel.BlackScholes);
        validate(AssetValuationModel.Whaley);
        validate(AssetValuationModel.Bachelier);
        validate(AssetValuationModel.Kirk);
        validate(AssetValuationModel.Curran);
        validate(AssetValuationModel.Black76);
        validate(AssetValuationModel.Binomial);
        validate(AssetValuationModel.OtherModel);
    });

    test('should be immutable', () => {
        const ref = AssetValuationModel;
        expect(() => {
            ref.BlackScholes = 10;
        }).toThrowError();
    });
});
