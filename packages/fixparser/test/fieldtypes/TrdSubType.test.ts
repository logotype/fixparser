import { TrdSubType } from '../../src/fieldtypes/TrdSubType';

describe('TrdSubType', () => {
    test('should have the correct values', () => {
        expect(TrdSubType.CMTA).toBe(0);
        expect(TrdSubType.InternalTransferOrAdjustment).toBe(1);
        expect(TrdSubType.ExternalTransferOrTransferOfAccount).toBe(2);
        expect(TrdSubType.RejectForSubmittingSide).toBe(3);
        expect(TrdSubType.AdvisoryForContraSide).toBe(4);
        expect(TrdSubType.OffsetDueToAnAllocation).toBe(5);
        expect(TrdSubType.OnsetDueToAnAllocation).toBe(6);
        expect(TrdSubType.DifferentialSpread).toBe(7);
        expect(TrdSubType.ImpliedSpreadLegExecutedAgainstAnOutright).toBe(8);
        expect(TrdSubType.TransactionFromExercise).toBe(9);
        expect(TrdSubType.TransactionFromAssignment).toBe(10);
        expect(TrdSubType.ACATS).toBe(11);
        expect(TrdSubType.AI).toBe(14);
        expect(TrdSubType.B).toBe(15);
        expect(TrdSubType.K).toBe(16);
        expect(TrdSubType.LC).toBe(17);
        expect(TrdSubType.M).toBe(18);
        expect(TrdSubType.N).toBe(19);
        expect(TrdSubType.NM).toBe(20);
        expect(TrdSubType.NR).toBe(21);
        expect(TrdSubType.P).toBe(22);
        expect(TrdSubType.PA).toBe(23);
        expect(TrdSubType.PC).toBe(24);
        expect(TrdSubType.PN).toBe(25);
        expect(TrdSubType.R).toBe(26);
        expect(TrdSubType.RO).toBe(27);
        expect(TrdSubType.RT).toBe(28);
        expect(TrdSubType.SW).toBe(29);
        expect(TrdSubType.T).toBe(30);
        expect(TrdSubType.WN).toBe(31);
        expect(TrdSubType.WT).toBe(32);
        expect(TrdSubType.OffHoursTrade).toBe(33);
        expect(TrdSubType.OnHoursTrade).toBe(34);
        expect(TrdSubType.OTCQuote).toBe(35);
        expect(TrdSubType.ConvertedSWAP).toBe(36);
        expect(TrdSubType.CrossedTrade).toBe(37);
        expect(TrdSubType.InterimProtectedTrade).toBe(38);
        expect(TrdSubType.LargeInScale).toBe(39);
        expect(TrdSubType.WashTrade).toBe(40);
        expect(TrdSubType.TradeAtSettlement).toBe(41);
        expect(TrdSubType.AuctionTrade).toBe(42);
        expect(TrdSubType.TradeAtMarker).toBe(43);
        expect(TrdSubType.CreditDefault).toBe(44);
        expect(TrdSubType.CreditRestructuring).toBe(45);
        expect(TrdSubType.Merger).toBe(46);
        expect(TrdSubType.SpinOff).toBe(47);
        expect(TrdSubType.MultilateralCompression).toBe(48);
        expect(TrdSubType.Balancing).toBe(50);
        expect(TrdSubType.BasisTradeIndexClose).toBe(51);
        expect(TrdSubType.TradeAtCashOpen).toBe(52);
        expect(TrdSubType.TrdSubmitVenueClrSettl).toBe(53);
        expect(TrdSubType.BilateralCompression).toBe(54);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdSubType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdSubType.CMTA,
            TrdSubType.InternalTransferOrAdjustment,
            TrdSubType.ExternalTransferOrTransferOfAccount,
            TrdSubType.RejectForSubmittingSide,
            TrdSubType.AdvisoryForContraSide,
            TrdSubType.OffsetDueToAnAllocation,
            TrdSubType.OnsetDueToAnAllocation,
            TrdSubType.DifferentialSpread,
            TrdSubType.ImpliedSpreadLegExecutedAgainstAnOutright,
            TrdSubType.TransactionFromExercise,
            TrdSubType.TransactionFromAssignment,
            TrdSubType.ACATS,
            TrdSubType.AI,
            TrdSubType.B,
            TrdSubType.K,
            TrdSubType.LC,
            TrdSubType.M,
            TrdSubType.N,
            TrdSubType.NM,
            TrdSubType.NR,
            TrdSubType.P,
            TrdSubType.PA,
            TrdSubType.PC,
            TrdSubType.PN,
            TrdSubType.R,
            TrdSubType.RO,
            TrdSubType.RT,
            TrdSubType.SW,
            TrdSubType.T,
            TrdSubType.WN,
            TrdSubType.WT,
            TrdSubType.OffHoursTrade,
            TrdSubType.OnHoursTrade,
            TrdSubType.OTCQuote,
            TrdSubType.ConvertedSWAP,
            TrdSubType.CrossedTrade,
            TrdSubType.InterimProtectedTrade,
            TrdSubType.LargeInScale,
            TrdSubType.WashTrade,
            TrdSubType.TradeAtSettlement,
            TrdSubType.AuctionTrade,
            TrdSubType.TradeAtMarker,
            TrdSubType.CreditDefault,
            TrdSubType.CreditRestructuring,
            TrdSubType.Merger,
            TrdSubType.SpinOff,
            TrdSubType.MultilateralCompression,
            TrdSubType.Balancing,
            TrdSubType.BasisTradeIndexClose,
            TrdSubType.TradeAtCashOpen,
            TrdSubType.TrdSubmitVenueClrSettl,
            TrdSubType.BilateralCompression,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdSubType type', () => {
        const validate = (value: TrdSubType) => {
            expect(Object.values(TrdSubType)).toContain(value);
        };

        validate(TrdSubType.CMTA);
        validate(TrdSubType.InternalTransferOrAdjustment);
        validate(TrdSubType.ExternalTransferOrTransferOfAccount);
        validate(TrdSubType.RejectForSubmittingSide);
        validate(TrdSubType.AdvisoryForContraSide);
        validate(TrdSubType.OffsetDueToAnAllocation);
        validate(TrdSubType.OnsetDueToAnAllocation);
        validate(TrdSubType.DifferentialSpread);
        validate(TrdSubType.ImpliedSpreadLegExecutedAgainstAnOutright);
        validate(TrdSubType.TransactionFromExercise);
        validate(TrdSubType.TransactionFromAssignment);
        validate(TrdSubType.ACATS);
        validate(TrdSubType.AI);
        validate(TrdSubType.B);
        validate(TrdSubType.K);
        validate(TrdSubType.LC);
        validate(TrdSubType.M);
        validate(TrdSubType.N);
        validate(TrdSubType.NM);
        validate(TrdSubType.NR);
        validate(TrdSubType.P);
        validate(TrdSubType.PA);
        validate(TrdSubType.PC);
        validate(TrdSubType.PN);
        validate(TrdSubType.R);
        validate(TrdSubType.RO);
        validate(TrdSubType.RT);
        validate(TrdSubType.SW);
        validate(TrdSubType.T);
        validate(TrdSubType.WN);
        validate(TrdSubType.WT);
        validate(TrdSubType.OffHoursTrade);
        validate(TrdSubType.OnHoursTrade);
        validate(TrdSubType.OTCQuote);
        validate(TrdSubType.ConvertedSWAP);
        validate(TrdSubType.CrossedTrade);
        validate(TrdSubType.InterimProtectedTrade);
        validate(TrdSubType.LargeInScale);
        validate(TrdSubType.WashTrade);
        validate(TrdSubType.TradeAtSettlement);
        validate(TrdSubType.AuctionTrade);
        validate(TrdSubType.TradeAtMarker);
        validate(TrdSubType.CreditDefault);
        validate(TrdSubType.CreditRestructuring);
        validate(TrdSubType.Merger);
        validate(TrdSubType.SpinOff);
        validate(TrdSubType.MultilateralCompression);
        validate(TrdSubType.Balancing);
        validate(TrdSubType.BasisTradeIndexClose);
        validate(TrdSubType.TradeAtCashOpen);
        validate(TrdSubType.TrdSubmitVenueClrSettl);
        validate(TrdSubType.BilateralCompression);
    });

    test('should be immutable', () => {
        const ref = TrdSubType;
        expect(() => {
            ref.CMTA = 10;
        }).toThrowError();
    });
});
