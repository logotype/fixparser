import { BasisPxType } from '../../src/fieldtypes/BasisPxType';

describe('BasisPxType', () => {
    test('should have the correct values', () => {
        expect(BasisPxType.ClosingPriceAtMorningSession).toBe('2');
        expect(BasisPxType.ClosingPrice).toBe('3');
        expect(BasisPxType.CurrentPrice).toBe('4');
        expect(BasisPxType.SQ).toBe('5');
        expect(BasisPxType.VWAPThroughADay).toBe('6');
        expect(BasisPxType.VWAPThroughAMorningSession).toBe('7');
        expect(BasisPxType.VWAPThroughAnAfternoonSession).toBe('8');
        expect(BasisPxType.VWAPThroughADayExcept).toBe('9');
        expect(BasisPxType.VWAPThroughAMorningSessionExcept).toBe('A');
        expect(BasisPxType.VWAPThroughAnAfternoonSessionExcept).toBe('B');
        expect(BasisPxType.Strike).toBe('C');
        expect(BasisPxType.Open).toBe('D');
        expect(BasisPxType.Others).toBe('Z');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BasisPxType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BasisPxType.ClosingPriceAtMorningSession,
            BasisPxType.ClosingPrice,
            BasisPxType.CurrentPrice,
            BasisPxType.SQ,
            BasisPxType.VWAPThroughADay,
            BasisPxType.VWAPThroughAMorningSession,
            BasisPxType.VWAPThroughAnAfternoonSession,
            BasisPxType.VWAPThroughADayExcept,
            BasisPxType.VWAPThroughAMorningSessionExcept,
            BasisPxType.VWAPThroughAnAfternoonSessionExcept,
            BasisPxType.Strike,
            BasisPxType.Open,
            BasisPxType.Others,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BasisPxType type', () => {
        const validate = (value: BasisPxType) => {
            expect(Object.values(BasisPxType)).toContain(value);
        };

        validate(BasisPxType.ClosingPriceAtMorningSession);
        validate(BasisPxType.ClosingPrice);
        validate(BasisPxType.CurrentPrice);
        validate(BasisPxType.SQ);
        validate(BasisPxType.VWAPThroughADay);
        validate(BasisPxType.VWAPThroughAMorningSession);
        validate(BasisPxType.VWAPThroughAnAfternoonSession);
        validate(BasisPxType.VWAPThroughADayExcept);
        validate(BasisPxType.VWAPThroughAMorningSessionExcept);
        validate(BasisPxType.VWAPThroughAnAfternoonSessionExcept);
        validate(BasisPxType.Strike);
        validate(BasisPxType.Open);
        validate(BasisPxType.Others);
    });

    test('should be immutable', () => {
        const ref = BasisPxType;
        expect(() => {
            ref.ClosingPriceAtMorningSession = 10;
        }).toThrowError();
    });
});
