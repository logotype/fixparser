import { UnderlyingNotionalAdjustments } from '../../src/fieldtypes/UnderlyingNotionalAdjustments';

describe('UnderlyingNotionalAdjustments', () => {
    test('should have the correct values', () => {
        expect(UnderlyingNotionalAdjustments.Execution).toBe(0);
        expect(UnderlyingNotionalAdjustments.PortfolioRebalancing).toBe(1);
        expect(UnderlyingNotionalAdjustments.Standard).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingNotionalAdjustments.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnderlyingNotionalAdjustments.Execution,
            UnderlyingNotionalAdjustments.PortfolioRebalancing,
            UnderlyingNotionalAdjustments.Standard,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for UnderlyingNotionalAdjustments type', () => {
        const validate = (value: UnderlyingNotionalAdjustments) => {
            expect(Object.values(UnderlyingNotionalAdjustments)).toContain(value);
        };

        validate(UnderlyingNotionalAdjustments.Execution);
        validate(UnderlyingNotionalAdjustments.PortfolioRebalancing);
        validate(UnderlyingNotionalAdjustments.Standard);
    });

    test('should be immutable', () => {
        const ref = UnderlyingNotionalAdjustments;
        expect(() => {
            ref.Execution = 10;
        }).toThrowError();
    });
});
