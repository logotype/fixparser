import { AssetClass } from '../../src/fieldtypes/AssetClass';

describe('AssetClass', () => {
    test('should have the correct values', () => {
        expect(AssetClass.InterestRate).toBe(1);
        expect(AssetClass.Currency).toBe(2);
        expect(AssetClass.Credit).toBe(3);
        expect(AssetClass.Equity).toBe(4);
        expect(AssetClass.Commodity).toBe(5);
        expect(AssetClass.Other).toBe(6);
        expect(AssetClass.Cash).toBe(7);
        expect(AssetClass.Debt).toBe(8);
        expect(AssetClass.Fund).toBe(9);
        expect(AssetClass.LoanFacility).toBe(10);
        expect(AssetClass.Index).toBe(11);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AssetClass.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AssetClass.InterestRate,
            AssetClass.Currency,
            AssetClass.Credit,
            AssetClass.Equity,
            AssetClass.Commodity,
            AssetClass.Other,
            AssetClass.Cash,
            AssetClass.Debt,
            AssetClass.Fund,
            AssetClass.LoanFacility,
            AssetClass.Index,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AssetClass type', () => {
        const validate = (value: AssetClass) => {
            expect(Object.values(AssetClass)).toContain(value);
        };

        validate(AssetClass.InterestRate);
        validate(AssetClass.Currency);
        validate(AssetClass.Credit);
        validate(AssetClass.Equity);
        validate(AssetClass.Commodity);
        validate(AssetClass.Other);
        validate(AssetClass.Cash);
        validate(AssetClass.Debt);
        validate(AssetClass.Fund);
        validate(AssetClass.LoanFacility);
        validate(AssetClass.Index);
    });

    test('should be immutable', () => {
        const ref = AssetClass;
        expect(() => {
            ref.InterestRate = 10;
        }).toThrowError();
    });
});
