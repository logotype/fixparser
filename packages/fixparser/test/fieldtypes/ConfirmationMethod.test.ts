import { ConfirmationMethod } from '../../src/fieldtypes/ConfirmationMethod';

describe('ConfirmationMethod', () => {
    test('should have the correct values', () => {
        expect(ConfirmationMethod.NonElectronic).toBe(0);
        expect(ConfirmationMethod.Electronic).toBe(1);
        expect(ConfirmationMethod.Unconfirmed).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ConfirmationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ConfirmationMethod.NonElectronic,
            ConfirmationMethod.Electronic,
            ConfirmationMethod.Unconfirmed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ConfirmationMethod type', () => {
        const validate = (value: ConfirmationMethod) => {
            expect(Object.values(ConfirmationMethod)).toContain(value);
        };

        validate(ConfirmationMethod.NonElectronic);
        validate(ConfirmationMethod.Electronic);
        validate(ConfirmationMethod.Unconfirmed);
    });

    test('should be immutable', () => {
        const ref = ConfirmationMethod;
        expect(() => {
            ref.NonElectronic = 10;
        }).toThrowError();
    });
});
