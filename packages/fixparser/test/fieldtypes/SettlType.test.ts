import { SettlType } from '../../src/fieldtypes/SettlType';

describe('SettlType', () => {
    test('should have the correct values', () => {
        expect(SettlType.Regular).toBe('0');
        expect(SettlType.Cash).toBe('1');
        expect(SettlType.NextDay).toBe('2');
        expect(SettlType.TPlus2).toBe('3');
        expect(SettlType.TPlus3).toBe('4');
        expect(SettlType.TPlus4).toBe('5');
        expect(SettlType.Future).toBe('6');
        expect(SettlType.WhenAndIfIssued).toBe('7');
        expect(SettlType.SellersOption).toBe('8');
        expect(SettlType.TPlus5).toBe('9');
        expect(SettlType.BrokenDate).toBe('B');
        expect(SettlType.FXSpotNextSettlement).toBe('C');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlType.Regular,
            SettlType.Cash,
            SettlType.NextDay,
            SettlType.TPlus2,
            SettlType.TPlus3,
            SettlType.TPlus4,
            SettlType.Future,
            SettlType.WhenAndIfIssued,
            SettlType.SellersOption,
            SettlType.TPlus5,
            SettlType.BrokenDate,
            SettlType.FXSpotNextSettlement,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlType type', () => {
        const validate = (value: SettlType) => {
            expect(Object.values(SettlType)).toContain(value);
        };

        validate(SettlType.Regular);
        validate(SettlType.Cash);
        validate(SettlType.NextDay);
        validate(SettlType.TPlus2);
        validate(SettlType.TPlus3);
        validate(SettlType.TPlus4);
        validate(SettlType.Future);
        validate(SettlType.WhenAndIfIssued);
        validate(SettlType.SellersOption);
        validate(SettlType.TPlus5);
        validate(SettlType.BrokenDate);
        validate(SettlType.FXSpotNextSettlement);
    });

    test('should be immutable', () => {
        const ref = SettlType;
        expect(() => {
            ref.Regular = 10;
        }).toThrowError();
    });
});
