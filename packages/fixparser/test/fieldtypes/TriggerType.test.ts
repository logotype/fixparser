import { TriggerType } from '../../src/fieldtypes/TriggerType';

describe('TriggerType', () => {
    test('should have the correct values', () => {
        expect(TriggerType.PartialExecution).toBe('1');
        expect(TriggerType.SpecifiedTradingSession).toBe('2');
        expect(TriggerType.NextAuction).toBe('3');
        expect(TriggerType.PriceMovement).toBe('4');
        expect(TriggerType.OnOrderEntryOrModification).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TriggerType.PartialExecution,
            TriggerType.SpecifiedTradingSession,
            TriggerType.NextAuction,
            TriggerType.PriceMovement,
            TriggerType.OnOrderEntryOrModification,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerType type', () => {
        const validate = (value: TriggerType) => {
            expect(Object.values(TriggerType)).toContain(value);
        };

        validate(TriggerType.PartialExecution);
        validate(TriggerType.SpecifiedTradingSession);
        validate(TriggerType.NextAuction);
        validate(TriggerType.PriceMovement);
        validate(TriggerType.OnOrderEntryOrModification);
    });

    test('should be immutable', () => {
        const ref = TriggerType;
        expect(() => {
            ref.PartialExecution = 10;
        }).toThrowError();
    });
});
