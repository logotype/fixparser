import { ProtectionTermEventQualifier } from '../../src/fieldtypes/ProtectionTermEventQualifier';

describe('ProtectionTermEventQualifier', () => {
    test('should have the correct values', () => {
        expect(ProtectionTermEventQualifier.RestructuringMultipleHoldingObligations).toBe('H');
        expect(ProtectionTermEventQualifier.RestructuringMultipleCreditEventNotices).toBe('E');
        expect(ProtectionTermEventQualifier.FloatingRateInterestShortfall).toBe('C');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProtectionTermEventQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProtectionTermEventQualifier.RestructuringMultipleHoldingObligations,
            ProtectionTermEventQualifier.RestructuringMultipleCreditEventNotices,
            ProtectionTermEventQualifier.FloatingRateInterestShortfall,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ProtectionTermEventQualifier type', () => {
        const validate = (value: ProtectionTermEventQualifier) => {
            expect(Object.values(ProtectionTermEventQualifier)).toContain(value);
        };

        validate(ProtectionTermEventQualifier.RestructuringMultipleHoldingObligations);
        validate(ProtectionTermEventQualifier.RestructuringMultipleCreditEventNotices);
        validate(ProtectionTermEventQualifier.FloatingRateInterestShortfall);
    });

    test('should be immutable', () => {
        const ref = ProtectionTermEventQualifier;
        expect(() => {
            ref.RestructuringMultipleHoldingObligations = 10;
        }).toThrowError();
    });
});
