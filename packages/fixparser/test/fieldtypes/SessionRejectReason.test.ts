import { SessionRejectReason } from '../../src/fieldtypes/SessionRejectReason';

describe('SessionRejectReason', () => {
    test('should have the correct values', () => {
        expect(SessionRejectReason.InvalidTagNumber).toBe(0);
        expect(SessionRejectReason.RequiredTagMissing).toBe(1);
        expect(SessionRejectReason.TagNotDefinedForThisMessageType).toBe(2);
        expect(SessionRejectReason.UndefinedTag).toBe(3);
        expect(SessionRejectReason.TagSpecifiedWithoutAValue).toBe(4);
        expect(SessionRejectReason.ValueIsIncorrect).toBe(5);
        expect(SessionRejectReason.IncorrectDataFormatForValue).toBe(6);
        expect(SessionRejectReason.DecryptionProblem).toBe(7);
        expect(SessionRejectReason.SignatureProblem).toBe(8);
        expect(SessionRejectReason.CompIDProblem).toBe(9);
        expect(SessionRejectReason.SendingTimeAccuracyProblem).toBe(10);
        expect(SessionRejectReason.InvalidMsgType).toBe(11);
        expect(SessionRejectReason.XMLValidationError).toBe(12);
        expect(SessionRejectReason.TagAppearsMoreThanOnce).toBe(13);
        expect(SessionRejectReason.TagSpecifiedOutOfRequiredOrder).toBe(14);
        expect(SessionRejectReason.RepeatingGroupFieldsOutOfOrder).toBe(15);
        expect(SessionRejectReason.IncorrectNumInGroupCountForRepeatingGroup).toBe(16);
        expect(SessionRejectReason.NonDataValueIncludesFieldDelimiter).toBe(17);
        expect(SessionRejectReason.InvalidUnsupportedApplVer).toBe(18);
        expect(SessionRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SessionRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SessionRejectReason.InvalidTagNumber,
            SessionRejectReason.RequiredTagMissing,
            SessionRejectReason.TagNotDefinedForThisMessageType,
            SessionRejectReason.UndefinedTag,
            SessionRejectReason.TagSpecifiedWithoutAValue,
            SessionRejectReason.ValueIsIncorrect,
            SessionRejectReason.IncorrectDataFormatForValue,
            SessionRejectReason.DecryptionProblem,
            SessionRejectReason.SignatureProblem,
            SessionRejectReason.CompIDProblem,
            SessionRejectReason.SendingTimeAccuracyProblem,
            SessionRejectReason.InvalidMsgType,
            SessionRejectReason.XMLValidationError,
            SessionRejectReason.TagAppearsMoreThanOnce,
            SessionRejectReason.TagSpecifiedOutOfRequiredOrder,
            SessionRejectReason.RepeatingGroupFieldsOutOfOrder,
            SessionRejectReason.IncorrectNumInGroupCountForRepeatingGroup,
            SessionRejectReason.NonDataValueIncludesFieldDelimiter,
            SessionRejectReason.InvalidUnsupportedApplVer,
            SessionRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SessionRejectReason type', () => {
        const validate = (value: SessionRejectReason) => {
            expect(Object.values(SessionRejectReason)).toContain(value);
        };

        validate(SessionRejectReason.InvalidTagNumber);
        validate(SessionRejectReason.RequiredTagMissing);
        validate(SessionRejectReason.TagNotDefinedForThisMessageType);
        validate(SessionRejectReason.UndefinedTag);
        validate(SessionRejectReason.TagSpecifiedWithoutAValue);
        validate(SessionRejectReason.ValueIsIncorrect);
        validate(SessionRejectReason.IncorrectDataFormatForValue);
        validate(SessionRejectReason.DecryptionProblem);
        validate(SessionRejectReason.SignatureProblem);
        validate(SessionRejectReason.CompIDProblem);
        validate(SessionRejectReason.SendingTimeAccuracyProblem);
        validate(SessionRejectReason.InvalidMsgType);
        validate(SessionRejectReason.XMLValidationError);
        validate(SessionRejectReason.TagAppearsMoreThanOnce);
        validate(SessionRejectReason.TagSpecifiedOutOfRequiredOrder);
        validate(SessionRejectReason.RepeatingGroupFieldsOutOfOrder);
        validate(SessionRejectReason.IncorrectNumInGroupCountForRepeatingGroup);
        validate(SessionRejectReason.NonDataValueIncludesFieldDelimiter);
        validate(SessionRejectReason.InvalidUnsupportedApplVer);
        validate(SessionRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = SessionRejectReason;
        expect(() => {
            ref.InvalidTagNumber = 10;
        }).toThrowError();
    });
});
