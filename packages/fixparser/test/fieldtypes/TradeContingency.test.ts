import { TradeContingency } from '../../src/fieldtypes/TradeContingency';

describe('TradeContingency', () => {
    test('should have the correct values', () => {
        expect(TradeContingency.DoesNotApply).toBe(0);
        expect(TradeContingency.ContingentTrade).toBe(1);
        expect(TradeContingency.NonContingentTrade).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeContingency.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeContingency.DoesNotApply,
            TradeContingency.ContingentTrade,
            TradeContingency.NonContingentTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeContingency type', () => {
        const validate = (value: TradeContingency) => {
            expect(Object.values(TradeContingency)).toContain(value);
        };

        validate(TradeContingency.DoesNotApply);
        validate(TradeContingency.ContingentTrade);
        validate(TradeContingency.NonContingentTrade);
    });

    test('should be immutable', () => {
        const ref = TradeContingency;
        expect(() => {
            ref.DoesNotApply = 10;
        }).toThrowError();
    });
});
