import { LastCapacity } from '../../src/fieldtypes/LastCapacity';

describe('LastCapacity', () => {
    test('should have the correct values', () => {
        expect(LastCapacity.Agent).toBe('1');
        expect(LastCapacity.CrossAsAgent).toBe('2');
        expect(LastCapacity.CrossAsPrincipal).toBe('3');
        expect(LastCapacity.Principal).toBe('4');
        expect(LastCapacity.RisklessPrincipal).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LastCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LastCapacity.Agent,
            LastCapacity.CrossAsAgent,
            LastCapacity.CrossAsPrincipal,
            LastCapacity.Principal,
            LastCapacity.RisklessPrincipal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LastCapacity type', () => {
        const validate = (value: LastCapacity) => {
            expect(Object.values(LastCapacity)).toContain(value);
        };

        validate(LastCapacity.Agent);
        validate(LastCapacity.CrossAsAgent);
        validate(LastCapacity.CrossAsPrincipal);
        validate(LastCapacity.Principal);
        validate(LastCapacity.RisklessPrincipal);
    });

    test('should be immutable', () => {
        const ref = LastCapacity;
        expect(() => {
            ref.Agent = 10;
        }).toThrowError();
    });
});
