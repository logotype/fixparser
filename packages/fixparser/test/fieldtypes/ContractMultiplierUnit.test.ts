import { ContractMultiplierUnit } from '../../src/fieldtypes/ContractMultiplierUnit';

describe('ContractMultiplierUnit', () => {
    test('should have the correct values', () => {
        expect(ContractMultiplierUnit.Shares).toBe(0);
        expect(ContractMultiplierUnit.Hours).toBe(1);
        expect(ContractMultiplierUnit.Days).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ContractMultiplierUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ContractMultiplierUnit.Shares,
            ContractMultiplierUnit.Hours,
            ContractMultiplierUnit.Days,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ContractMultiplierUnit type', () => {
        const validate = (value: ContractMultiplierUnit) => {
            expect(Object.values(ContractMultiplierUnit)).toContain(value);
        };

        validate(ContractMultiplierUnit.Shares);
        validate(ContractMultiplierUnit.Hours);
        validate(ContractMultiplierUnit.Days);
    });

    test('should be immutable', () => {
        const ref = ContractMultiplierUnit;
        expect(() => {
            ref.Shares = 10;
        }).toThrowError();
    });
});
