import { ReturnRatePriceBasis } from '../../src/fieldtypes/ReturnRatePriceBasis';

describe('ReturnRatePriceBasis', () => {
    test('should have the correct values', () => {
        expect(ReturnRatePriceBasis.Gross).toBe(0);
        expect(ReturnRatePriceBasis.Net).toBe(1);
        expect(ReturnRatePriceBasis.Accrued).toBe(2);
        expect(ReturnRatePriceBasis.CleanNet).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRatePriceBasis.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReturnRatePriceBasis.Gross,
            ReturnRatePriceBasis.Net,
            ReturnRatePriceBasis.Accrued,
            ReturnRatePriceBasis.CleanNet,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRatePriceBasis type', () => {
        const validate = (value: ReturnRatePriceBasis) => {
            expect(Object.values(ReturnRatePriceBasis)).toContain(value);
        };

        validate(ReturnRatePriceBasis.Gross);
        validate(ReturnRatePriceBasis.Net);
        validate(ReturnRatePriceBasis.Accrued);
        validate(ReturnRatePriceBasis.CleanNet);
    });

    test('should be immutable', () => {
        const ref = ReturnRatePriceBasis;
        expect(() => {
            ref.Gross = 10;
        }).toThrowError();
    });
});
