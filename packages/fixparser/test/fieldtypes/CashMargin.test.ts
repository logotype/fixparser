import { CashMargin } from '../../src/fieldtypes/CashMargin';

describe('CashMargin', () => {
    test('should have the correct values', () => {
        expect(CashMargin.Cash).toBe('1');
        expect(CashMargin.MarginOpen).toBe('2');
        expect(CashMargin.MarginClose).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CashMargin.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CashMargin.Cash, CashMargin.MarginOpen, CashMargin.MarginClose];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CashMargin type', () => {
        const validate = (value: CashMargin) => {
            expect(Object.values(CashMargin)).toContain(value);
        };

        validate(CashMargin.Cash);
        validate(CashMargin.MarginOpen);
        validate(CashMargin.MarginClose);
    });

    test('should be immutable', () => {
        const ref = CashMargin;
        expect(() => {
            ref.Cash = 10;
        }).toThrowError();
    });
});
