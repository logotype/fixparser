import { PaymentDateOffsetDayType } from '../../src/fieldtypes/PaymentDateOffsetDayType';

describe('PaymentDateOffsetDayType', () => {
    test('should have the correct values', () => {
        expect(PaymentDateOffsetDayType.Business).toBe(0);
        expect(PaymentDateOffsetDayType.Calendar).toBe(1);
        expect(PaymentDateOffsetDayType.Commodity).toBe(2);
        expect(PaymentDateOffsetDayType.Currency).toBe(3);
        expect(PaymentDateOffsetDayType.Exchange).toBe(4);
        expect(PaymentDateOffsetDayType.Scheduled).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentDateOffsetDayType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentDateOffsetDayType.Business,
            PaymentDateOffsetDayType.Calendar,
            PaymentDateOffsetDayType.Commodity,
            PaymentDateOffsetDayType.Currency,
            PaymentDateOffsetDayType.Exchange,
            PaymentDateOffsetDayType.Scheduled,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentDateOffsetDayType type', () => {
        const validate = (value: PaymentDateOffsetDayType) => {
            expect(Object.values(PaymentDateOffsetDayType)).toContain(value);
        };

        validate(PaymentDateOffsetDayType.Business);
        validate(PaymentDateOffsetDayType.Calendar);
        validate(PaymentDateOffsetDayType.Commodity);
        validate(PaymentDateOffsetDayType.Currency);
        validate(PaymentDateOffsetDayType.Exchange);
        validate(PaymentDateOffsetDayType.Scheduled);
    });

    test('should be immutable', () => {
        const ref = PaymentDateOffsetDayType;
        expect(() => {
            ref.Business = 10;
        }).toThrowError();
    });
});
