import { MassStatusReqType } from '../../src/fieldtypes/MassStatusReqType';

describe('MassStatusReqType', () => {
    test('should have the correct values', () => {
        expect(MassStatusReqType.StatusForOrdersForASecurity).toBe(1);
        expect(MassStatusReqType.StatusForOrdersForAnUnderlyingSecurity).toBe(2);
        expect(MassStatusReqType.StatusForOrdersForAProduct).toBe(3);
        expect(MassStatusReqType.StatusForOrdersForACFICode).toBe(4);
        expect(MassStatusReqType.StatusForOrdersForASecurityType).toBe(5);
        expect(MassStatusReqType.StatusForOrdersForATradingSession).toBe(6);
        expect(MassStatusReqType.StatusForAllOrders).toBe(7);
        expect(MassStatusReqType.StatusForOrdersForAPartyID).toBe(8);
        expect(MassStatusReqType.StatusForSecurityIssuer).toBe(9);
        expect(MassStatusReqType.StatusForIssuerOfUnderlyingSecurity).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassStatusReqType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassStatusReqType.StatusForOrdersForASecurity,
            MassStatusReqType.StatusForOrdersForAnUnderlyingSecurity,
            MassStatusReqType.StatusForOrdersForAProduct,
            MassStatusReqType.StatusForOrdersForACFICode,
            MassStatusReqType.StatusForOrdersForASecurityType,
            MassStatusReqType.StatusForOrdersForATradingSession,
            MassStatusReqType.StatusForAllOrders,
            MassStatusReqType.StatusForOrdersForAPartyID,
            MassStatusReqType.StatusForSecurityIssuer,
            MassStatusReqType.StatusForIssuerOfUnderlyingSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassStatusReqType type', () => {
        const validate = (value: MassStatusReqType) => {
            expect(Object.values(MassStatusReqType)).toContain(value);
        };

        validate(MassStatusReqType.StatusForOrdersForASecurity);
        validate(MassStatusReqType.StatusForOrdersForAnUnderlyingSecurity);
        validate(MassStatusReqType.StatusForOrdersForAProduct);
        validate(MassStatusReqType.StatusForOrdersForACFICode);
        validate(MassStatusReqType.StatusForOrdersForASecurityType);
        validate(MassStatusReqType.StatusForOrdersForATradingSession);
        validate(MassStatusReqType.StatusForAllOrders);
        validate(MassStatusReqType.StatusForOrdersForAPartyID);
        validate(MassStatusReqType.StatusForSecurityIssuer);
        validate(MassStatusReqType.StatusForIssuerOfUnderlyingSecurity);
    });

    test('should be immutable', () => {
        const ref = MassStatusReqType;
        expect(() => {
            ref.StatusForOrdersForASecurity = 10;
        }).toThrowError();
    });
});
