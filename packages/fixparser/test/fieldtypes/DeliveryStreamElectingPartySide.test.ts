import { DeliveryStreamElectingPartySide } from '../../src/fieldtypes/DeliveryStreamElectingPartySide';

describe('DeliveryStreamElectingPartySide', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamElectingPartySide.Buyer).toBe(0);
        expect(DeliveryStreamElectingPartySide.Seller).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamElectingPartySide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryStreamElectingPartySide.Buyer, DeliveryStreamElectingPartySide.Seller];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamElectingPartySide type', () => {
        const validate = (value: DeliveryStreamElectingPartySide) => {
            expect(Object.values(DeliveryStreamElectingPartySide)).toContain(value);
        };

        validate(DeliveryStreamElectingPartySide.Buyer);
        validate(DeliveryStreamElectingPartySide.Seller);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamElectingPartySide;
        expect(() => {
            ref.Buyer = 10;
        }).toThrowError();
    });
});
