import { ComplexEventPriceBoundaryMethod } from '../../src/fieldtypes/ComplexEventPriceBoundaryMethod';

describe('ComplexEventPriceBoundaryMethod', () => {
    test('should have the correct values', () => {
        expect(ComplexEventPriceBoundaryMethod.LessThanComplexEventPrice).toBe(1);
        expect(ComplexEventPriceBoundaryMethod.LessThanOrEqualToComplexEventPrice).toBe(2);
        expect(ComplexEventPriceBoundaryMethod.EqualToComplexEventPrice).toBe(3);
        expect(ComplexEventPriceBoundaryMethod.GreaterThanOrEqualToComplexEventPrice).toBe(4);
        expect(ComplexEventPriceBoundaryMethod.GreaterThanComplexEventPrice).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventPriceBoundaryMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventPriceBoundaryMethod.LessThanComplexEventPrice,
            ComplexEventPriceBoundaryMethod.LessThanOrEqualToComplexEventPrice,
            ComplexEventPriceBoundaryMethod.EqualToComplexEventPrice,
            ComplexEventPriceBoundaryMethod.GreaterThanOrEqualToComplexEventPrice,
            ComplexEventPriceBoundaryMethod.GreaterThanComplexEventPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventPriceBoundaryMethod type', () => {
        const validate = (value: ComplexEventPriceBoundaryMethod) => {
            expect(Object.values(ComplexEventPriceBoundaryMethod)).toContain(value);
        };

        validate(ComplexEventPriceBoundaryMethod.LessThanComplexEventPrice);
        validate(ComplexEventPriceBoundaryMethod.LessThanOrEqualToComplexEventPrice);
        validate(ComplexEventPriceBoundaryMethod.EqualToComplexEventPrice);
        validate(ComplexEventPriceBoundaryMethod.GreaterThanOrEqualToComplexEventPrice);
        validate(ComplexEventPriceBoundaryMethod.GreaterThanComplexEventPrice);
    });

    test('should be immutable', () => {
        const ref = ComplexEventPriceBoundaryMethod;
        expect(() => {
            ref.LessThanComplexEventPrice = 10;
        }).toThrowError();
    });
});
