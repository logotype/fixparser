import { TriggerAction } from '../../src/fieldtypes/TriggerAction';

describe('TriggerAction', () => {
    test('should have the correct values', () => {
        expect(TriggerAction.Activate).toBe('1');
        expect(TriggerAction.Modify).toBe('2');
        expect(TriggerAction.Cancel).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TriggerAction.Activate, TriggerAction.Modify, TriggerAction.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerAction type', () => {
        const validate = (value: TriggerAction) => {
            expect(Object.values(TriggerAction)).toContain(value);
        };

        validate(TriggerAction.Activate);
        validate(TriggerAction.Modify);
        validate(TriggerAction.Cancel);
    });

    test('should be immutable', () => {
        const ref = TriggerAction;
        expect(() => {
            ref.Activate = 10;
        }).toThrowError();
    });
});
