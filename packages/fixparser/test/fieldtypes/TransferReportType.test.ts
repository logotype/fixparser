import { TransferReportType } from '../../src/fieldtypes/TransferReportType';

describe('TransferReportType', () => {
    test('should have the correct values', () => {
        expect(TransferReportType.Submit).toBe(0);
        expect(TransferReportType.Alleged).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferReportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TransferReportType.Submit, TransferReportType.Alleged];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferReportType type', () => {
        const validate = (value: TransferReportType) => {
            expect(Object.values(TransferReportType)).toContain(value);
        };

        validate(TransferReportType.Submit);
        validate(TransferReportType.Alleged);
    });

    test('should be immutable', () => {
        const ref = TransferReportType;
        expect(() => {
            ref.Submit = 10;
        }).toThrowError();
    });
});
