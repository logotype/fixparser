import { PaymentStreamPricingDayOfWeek } from '../../src/fieldtypes/PaymentStreamPricingDayOfWeek';

describe('PaymentStreamPricingDayOfWeek', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamPricingDayOfWeek.EveryDay).toBe(0);
        expect(PaymentStreamPricingDayOfWeek.Monday).toBe(1);
        expect(PaymentStreamPricingDayOfWeek.Tuesday).toBe(2);
        expect(PaymentStreamPricingDayOfWeek.Wednesday).toBe(3);
        expect(PaymentStreamPricingDayOfWeek.Thursday).toBe(4);
        expect(PaymentStreamPricingDayOfWeek.Friday).toBe(5);
        expect(PaymentStreamPricingDayOfWeek.Saturday).toBe(6);
        expect(PaymentStreamPricingDayOfWeek.Sunday).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamPricingDayOfWeek.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamPricingDayOfWeek.EveryDay,
            PaymentStreamPricingDayOfWeek.Monday,
            PaymentStreamPricingDayOfWeek.Tuesday,
            PaymentStreamPricingDayOfWeek.Wednesday,
            PaymentStreamPricingDayOfWeek.Thursday,
            PaymentStreamPricingDayOfWeek.Friday,
            PaymentStreamPricingDayOfWeek.Saturday,
            PaymentStreamPricingDayOfWeek.Sunday,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamPricingDayOfWeek type', () => {
        const validate = (value: PaymentStreamPricingDayOfWeek) => {
            expect(Object.values(PaymentStreamPricingDayOfWeek)).toContain(value);
        };

        validate(PaymentStreamPricingDayOfWeek.EveryDay);
        validate(PaymentStreamPricingDayOfWeek.Monday);
        validate(PaymentStreamPricingDayOfWeek.Tuesday);
        validate(PaymentStreamPricingDayOfWeek.Wednesday);
        validate(PaymentStreamPricingDayOfWeek.Thursday);
        validate(PaymentStreamPricingDayOfWeek.Friday);
        validate(PaymentStreamPricingDayOfWeek.Saturday);
        validate(PaymentStreamPricingDayOfWeek.Sunday);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamPricingDayOfWeek;
        expect(() => {
            ref.EveryDay = 10;
        }).toThrowError();
    });
});
