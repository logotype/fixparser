import { MultiLegRptTypeReq } from '../../src/fieldtypes/MultiLegRptTypeReq';

describe('MultiLegRptTypeReq', () => {
    test('should have the correct values', () => {
        expect(MultiLegRptTypeReq.ReportByMulitlegSecurityOnly).toBe(0);
        expect(MultiLegRptTypeReq.ReportByMultilegSecurityAndInstrumentLegs).toBe(1);
        expect(MultiLegRptTypeReq.ReportByInstrumentLegsOnly).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MultiLegRptTypeReq.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MultiLegRptTypeReq.ReportByMulitlegSecurityOnly,
            MultiLegRptTypeReq.ReportByMultilegSecurityAndInstrumentLegs,
            MultiLegRptTypeReq.ReportByInstrumentLegsOnly,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MultiLegRptTypeReq type', () => {
        const validate = (value: MultiLegRptTypeReq) => {
            expect(Object.values(MultiLegRptTypeReq)).toContain(value);
        };

        validate(MultiLegRptTypeReq.ReportByMulitlegSecurityOnly);
        validate(MultiLegRptTypeReq.ReportByMultilegSecurityAndInstrumentLegs);
        validate(MultiLegRptTypeReq.ReportByInstrumentLegsOnly);
    });

    test('should be immutable', () => {
        const ref = MultiLegRptTypeReq;
        expect(() => {
            ref.ReportByMulitlegSecurityOnly = 10;
        }).toThrowError();
    });
});
