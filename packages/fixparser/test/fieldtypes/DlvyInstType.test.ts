import { DlvyInstType } from '../../src/fieldtypes/DlvyInstType';

describe('DlvyInstType', () => {
    test('should have the correct values', () => {
        expect(DlvyInstType.Cash).toBe('C');
        expect(DlvyInstType.Securities).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DlvyInstType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DlvyInstType.Cash, DlvyInstType.Securities];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DlvyInstType type', () => {
        const validate = (value: DlvyInstType) => {
            expect(Object.values(DlvyInstType)).toContain(value);
        };

        validate(DlvyInstType.Cash);
        validate(DlvyInstType.Securities);
    });

    test('should be immutable', () => {
        const ref = DlvyInstType;
        expect(() => {
            ref.Cash = 10;
        }).toThrowError();
    });
});
