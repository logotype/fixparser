import { TrdRegPublicationType } from '../../src/fieldtypes/TrdRegPublicationType';

describe('TrdRegPublicationType', () => {
    test('should have the correct values', () => {
        expect(TrdRegPublicationType.PreTradeTransparencyWaiver).toBe(0);
        expect(TrdRegPublicationType.PostTradeDeferral).toBe(1);
        expect(TrdRegPublicationType.ExemptFromPublication).toBe(2);
        expect(TrdRegPublicationType.OrderLevelPublicationToSubscribers).toBe(3);
        expect(TrdRegPublicationType.PriceLevelPublicationToSubscribers).toBe(4);
        expect(TrdRegPublicationType.OrderLevelPublicationToThePublic).toBe(5);
        expect(TrdRegPublicationType.PublicationInternalToExecutionVenue).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdRegPublicationType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdRegPublicationType.PreTradeTransparencyWaiver,
            TrdRegPublicationType.PostTradeDeferral,
            TrdRegPublicationType.ExemptFromPublication,
            TrdRegPublicationType.OrderLevelPublicationToSubscribers,
            TrdRegPublicationType.PriceLevelPublicationToSubscribers,
            TrdRegPublicationType.OrderLevelPublicationToThePublic,
            TrdRegPublicationType.PublicationInternalToExecutionVenue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdRegPublicationType type', () => {
        const validate = (value: TrdRegPublicationType) => {
            expect(Object.values(TrdRegPublicationType)).toContain(value);
        };

        validate(TrdRegPublicationType.PreTradeTransparencyWaiver);
        validate(TrdRegPublicationType.PostTradeDeferral);
        validate(TrdRegPublicationType.ExemptFromPublication);
        validate(TrdRegPublicationType.OrderLevelPublicationToSubscribers);
        validate(TrdRegPublicationType.PriceLevelPublicationToSubscribers);
        validate(TrdRegPublicationType.OrderLevelPublicationToThePublic);
        validate(TrdRegPublicationType.PublicationInternalToExecutionVenue);
    });

    test('should be immutable', () => {
        const ref = TrdRegPublicationType;
        expect(() => {
            ref.PreTradeTransparencyWaiver = 10;
        }).toThrowError();
    });
});
