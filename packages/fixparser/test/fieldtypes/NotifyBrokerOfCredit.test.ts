import { NotifyBrokerOfCredit } from '../../src/fieldtypes/NotifyBrokerOfCredit';

describe('NotifyBrokerOfCredit', () => {
    test('should have the correct values', () => {
        expect(NotifyBrokerOfCredit.DetailsShouldNotBeCommunicated).toBe('N');
        expect(NotifyBrokerOfCredit.DetailsShouldBeCommunicated).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NotifyBrokerOfCredit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NotifyBrokerOfCredit.DetailsShouldNotBeCommunicated,
            NotifyBrokerOfCredit.DetailsShouldBeCommunicated,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for NotifyBrokerOfCredit type', () => {
        const validate = (value: NotifyBrokerOfCredit) => {
            expect(Object.values(NotifyBrokerOfCredit)).toContain(value);
        };

        validate(NotifyBrokerOfCredit.DetailsShouldNotBeCommunicated);
        validate(NotifyBrokerOfCredit.DetailsShouldBeCommunicated);
    });

    test('should be immutable', () => {
        const ref = NotifyBrokerOfCredit;
        expect(() => {
            ref.DetailsShouldNotBeCommunicated = 10;
        }).toThrowError();
    });
});
