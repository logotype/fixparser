import { ReferenceDataDateType } from '../../src/fieldtypes/ReferenceDataDateType';

describe('ReferenceDataDateType', () => {
    test('should have the correct values', () => {
        expect(ReferenceDataDateType.AdmitToTradeRequestDate).toBe(0);
        expect(ReferenceDataDateType.AdmitToTradeApprovalDate).toBe(1);
        expect(ReferenceDataDateType.AdmitToTradeOrFirstTradeDate).toBe(2);
        expect(ReferenceDataDateType.TerminationDate).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReferenceDataDateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReferenceDataDateType.AdmitToTradeRequestDate,
            ReferenceDataDateType.AdmitToTradeApprovalDate,
            ReferenceDataDateType.AdmitToTradeOrFirstTradeDate,
            ReferenceDataDateType.TerminationDate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReferenceDataDateType type', () => {
        const validate = (value: ReferenceDataDateType) => {
            expect(Object.values(ReferenceDataDateType)).toContain(value);
        };

        validate(ReferenceDataDateType.AdmitToTradeRequestDate);
        validate(ReferenceDataDateType.AdmitToTradeApprovalDate);
        validate(ReferenceDataDateType.AdmitToTradeOrFirstTradeDate);
        validate(ReferenceDataDateType.TerminationDate);
    });

    test('should be immutable', () => {
        const ref = ReferenceDataDateType;
        expect(() => {
            ref.AdmitToTradeRequestDate = 10;
        }).toThrowError();
    });
});
