import { ReturnRateDateMode } from '../../src/fieldtypes/ReturnRateDateMode';

describe('ReturnRateDateMode', () => {
    test('should have the correct values', () => {
        expect(ReturnRateDateMode.PriceValuation).toBe(0);
        expect(ReturnRateDateMode.DividendValuation).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRateDateMode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ReturnRateDateMode.PriceValuation, ReturnRateDateMode.DividendValuation];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRateDateMode type', () => {
        const validate = (value: ReturnRateDateMode) => {
            expect(Object.values(ReturnRateDateMode)).toContain(value);
        };

        validate(ReturnRateDateMode.PriceValuation);
        validate(ReturnRateDateMode.DividendValuation);
    });

    test('should be immutable', () => {
        const ref = ReturnRateDateMode;
        expect(() => {
            ref.PriceValuation = 10;
        }).toThrowError();
    });
});
