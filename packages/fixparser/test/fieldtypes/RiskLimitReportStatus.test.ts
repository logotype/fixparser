import { RiskLimitReportStatus } from '../../src/fieldtypes/RiskLimitReportStatus';

describe('RiskLimitReportStatus', () => {
    test('should have the correct values', () => {
        expect(RiskLimitReportStatus.Accepted).toBe(0);
        expect(RiskLimitReportStatus.Rejected).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitReportStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RiskLimitReportStatus.Accepted, RiskLimitReportStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitReportStatus type', () => {
        const validate = (value: RiskLimitReportStatus) => {
            expect(Object.values(RiskLimitReportStatus)).toContain(value);
        };

        validate(RiskLimitReportStatus.Accepted);
        validate(RiskLimitReportStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = RiskLimitReportStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
