import { PegMoveType } from '../../src/fieldtypes/PegMoveType';

describe('PegMoveType', () => {
    test('should have the correct values', () => {
        expect(PegMoveType.Floating).toBe(0);
        expect(PegMoveType.Fixed).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegMoveType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PegMoveType.Floating, PegMoveType.Fixed];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegMoveType type', () => {
        const validate = (value: PegMoveType) => {
            expect(Object.values(PegMoveType)).toContain(value);
        };

        validate(PegMoveType.Floating);
        validate(PegMoveType.Fixed);
    });

    test('should be immutable', () => {
        const ref = PegMoveType;
        expect(() => {
            ref.Floating = 10;
        }).toThrowError();
    });
});
