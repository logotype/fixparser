import { PaymentStreamCapRateBuySide } from '../../src/fieldtypes/PaymentStreamCapRateBuySide';

describe('PaymentStreamCapRateBuySide', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamCapRateBuySide.Buyer).toBe(1);
        expect(PaymentStreamCapRateBuySide.Seller).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamCapRateBuySide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamCapRateBuySide.Buyer, PaymentStreamCapRateBuySide.Seller];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamCapRateBuySide type', () => {
        const validate = (value: PaymentStreamCapRateBuySide) => {
            expect(Object.values(PaymentStreamCapRateBuySide)).toContain(value);
        };

        validate(PaymentStreamCapRateBuySide.Buyer);
        validate(PaymentStreamCapRateBuySide.Seller);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamCapRateBuySide;
        expect(() => {
            ref.Buyer = 10;
        }).toThrowError();
    });
});
