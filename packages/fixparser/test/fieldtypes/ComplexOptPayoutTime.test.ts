import { ComplexOptPayoutTime } from '../../src/fieldtypes/ComplexOptPayoutTime';

describe('ComplexOptPayoutTime', () => {
    test('should have the correct values', () => {
        expect(ComplexOptPayoutTime.Close).toBe(0);
        expect(ComplexOptPayoutTime.Open).toBe(1);
        expect(ComplexOptPayoutTime.OfficialSettl).toBe(2);
        expect(ComplexOptPayoutTime.ValuationTime).toBe(3);
        expect(ComplexOptPayoutTime.ExcahgneSettlTime).toBe(4);
        expect(ComplexOptPayoutTime.DerivativesClose).toBe(5);
        expect(ComplexOptPayoutTime.AsSpecified).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexOptPayoutTime.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexOptPayoutTime.Close,
            ComplexOptPayoutTime.Open,
            ComplexOptPayoutTime.OfficialSettl,
            ComplexOptPayoutTime.ValuationTime,
            ComplexOptPayoutTime.ExcahgneSettlTime,
            ComplexOptPayoutTime.DerivativesClose,
            ComplexOptPayoutTime.AsSpecified,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexOptPayoutTime type', () => {
        const validate = (value: ComplexOptPayoutTime) => {
            expect(Object.values(ComplexOptPayoutTime)).toContain(value);
        };

        validate(ComplexOptPayoutTime.Close);
        validate(ComplexOptPayoutTime.Open);
        validate(ComplexOptPayoutTime.OfficialSettl);
        validate(ComplexOptPayoutTime.ValuationTime);
        validate(ComplexOptPayoutTime.ExcahgneSettlTime);
        validate(ComplexOptPayoutTime.DerivativesClose);
        validate(ComplexOptPayoutTime.AsSpecified);
    });

    test('should be immutable', () => {
        const ref = ComplexOptPayoutTime;
        expect(() => {
            ref.Close = 10;
        }).toThrowError();
    });
});
