import { ComplexEventPeriodType } from '../../src/fieldtypes/ComplexEventPeriodType';

describe('ComplexEventPeriodType', () => {
    test('should have the correct values', () => {
        expect(ComplexEventPeriodType.AsianOut).toBe(0);
        expect(ComplexEventPeriodType.AsianIn).toBe(1);
        expect(ComplexEventPeriodType.BarrierCap).toBe(2);
        expect(ComplexEventPeriodType.BarrierFloor).toBe(3);
        expect(ComplexEventPeriodType.KnockOut).toBe(4);
        expect(ComplexEventPeriodType.KnockIn).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventPeriodType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventPeriodType.AsianOut,
            ComplexEventPeriodType.AsianIn,
            ComplexEventPeriodType.BarrierCap,
            ComplexEventPeriodType.BarrierFloor,
            ComplexEventPeriodType.KnockOut,
            ComplexEventPeriodType.KnockIn,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventPeriodType type', () => {
        const validate = (value: ComplexEventPeriodType) => {
            expect(Object.values(ComplexEventPeriodType)).toContain(value);
        };

        validate(ComplexEventPeriodType.AsianOut);
        validate(ComplexEventPeriodType.AsianIn);
        validate(ComplexEventPeriodType.BarrierCap);
        validate(ComplexEventPeriodType.BarrierFloor);
        validate(ComplexEventPeriodType.KnockOut);
        validate(ComplexEventPeriodType.KnockIn);
    });

    test('should be immutable', () => {
        const ref = ComplexEventPeriodType;
        expect(() => {
            ref.AsianOut = 10;
        }).toThrowError();
    });
});
