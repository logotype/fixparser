import { ExecAckStatus } from '../../src/fieldtypes/ExecAckStatus';

describe('ExecAckStatus', () => {
    test('should have the correct values', () => {
        expect(ExecAckStatus.Received).toBe('0');
        expect(ExecAckStatus.Accepted).toBe('1');
        expect(ExecAckStatus.DontKnow).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecAckStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ExecAckStatus.Received, ExecAckStatus.Accepted, ExecAckStatus.DontKnow];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExecAckStatus type', () => {
        const validate = (value: ExecAckStatus) => {
            expect(Object.values(ExecAckStatus)).toContain(value);
        };

        validate(ExecAckStatus.Received);
        validate(ExecAckStatus.Accepted);
        validate(ExecAckStatus.DontKnow);
    });

    test('should be immutable', () => {
        const ref = ExecAckStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
