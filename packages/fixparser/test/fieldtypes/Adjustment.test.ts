import { Adjustment } from '../../src/fieldtypes/Adjustment';

describe('Adjustment', () => {
    test('should have the correct values', () => {
        expect(Adjustment.Cancel).toBe(1);
        expect(Adjustment.Error).toBe(2);
        expect(Adjustment.Correction).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Adjustment.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [Adjustment.Cancel, Adjustment.Error, Adjustment.Correction];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for Adjustment type', () => {
        const validate = (value: Adjustment) => {
            expect(Object.values(Adjustment)).toContain(value);
        };

        validate(Adjustment.Cancel);
        validate(Adjustment.Error);
        validate(Adjustment.Correction);
    });

    test('should be immutable', () => {
        const ref = Adjustment;
        expect(() => {
            ref.Cancel = 10;
        }).toThrowError();
    });
});
