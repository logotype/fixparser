import { OrderRelationship } from '../../src/fieldtypes/OrderRelationship';

describe('OrderRelationship', () => {
    test('should have the correct values', () => {
        expect(OrderRelationship.NotSpecified).toBe(0);
        expect(OrderRelationship.OrderAggregation).toBe(1);
        expect(OrderRelationship.OrderSplit).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderRelationship.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderRelationship.NotSpecified,
            OrderRelationship.OrderAggregation,
            OrderRelationship.OrderSplit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderRelationship type', () => {
        const validate = (value: OrderRelationship) => {
            expect(Object.values(OrderRelationship)).toContain(value);
        };

        validate(OrderRelationship.NotSpecified);
        validate(OrderRelationship.OrderAggregation);
        validate(OrderRelationship.OrderSplit);
    });

    test('should be immutable', () => {
        const ref = OrderRelationship;
        expect(() => {
            ref.NotSpecified = 10;
        }).toThrowError();
    });
});
