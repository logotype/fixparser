import { EventTimeUnit } from '../../src/fieldtypes/EventTimeUnit';

describe('EventTimeUnit', () => {
    test('should have the correct values', () => {
        expect(EventTimeUnit.Hour).toBe('H');
        expect(EventTimeUnit.Minute).toBe('Min');
        expect(EventTimeUnit.Second).toBe('S');
        expect(EventTimeUnit.Day).toBe('D');
        expect(EventTimeUnit.Week).toBe('Wk');
        expect(EventTimeUnit.Month).toBe('Mo');
        expect(EventTimeUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EventTimeUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EventTimeUnit.Hour,
            EventTimeUnit.Minute,
            EventTimeUnit.Second,
            EventTimeUnit.Day,
            EventTimeUnit.Week,
            EventTimeUnit.Month,
            EventTimeUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for EventTimeUnit type', () => {
        const validate = (value: EventTimeUnit) => {
            expect(Object.values(EventTimeUnit)).toContain(value);
        };

        validate(EventTimeUnit.Hour);
        validate(EventTimeUnit.Minute);
        validate(EventTimeUnit.Second);
        validate(EventTimeUnit.Day);
        validate(EventTimeUnit.Week);
        validate(EventTimeUnit.Month);
        validate(EventTimeUnit.Year);
    });

    test('should be immutable', () => {
        const ref = EventTimeUnit;
        expect(() => {
            ref.Hour = 10;
        }).toThrowError();
    });
});
