import { ExpirationCycle } from '../../src/fieldtypes/ExpirationCycle';

describe('ExpirationCycle', () => {
    test('should have the correct values', () => {
        expect(ExpirationCycle.ExpireOnTradingSessionClose).toBe(0);
        expect(ExpirationCycle.ExpireOnTradingSessionOpen).toBe(1);
        expect(ExpirationCycle.SpecifiedExpiration).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExpirationCycle.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExpirationCycle.ExpireOnTradingSessionClose,
            ExpirationCycle.ExpireOnTradingSessionOpen,
            ExpirationCycle.SpecifiedExpiration,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExpirationCycle type', () => {
        const validate = (value: ExpirationCycle) => {
            expect(Object.values(ExpirationCycle)).toContain(value);
        };

        validate(ExpirationCycle.ExpireOnTradingSessionClose);
        validate(ExpirationCycle.ExpireOnTradingSessionOpen);
        validate(ExpirationCycle.SpecifiedExpiration);
    });

    test('should be immutable', () => {
        const ref = ExpirationCycle;
        expect(() => {
            ref.ExpireOnTradingSessionClose = 10;
        }).toThrowError();
    });
});
