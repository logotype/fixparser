import { PaymentStreamLinkStrikePriceType } from '../../src/fieldtypes/PaymentStreamLinkStrikePriceType';

describe('PaymentStreamLinkStrikePriceType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamLinkStrikePriceType.Volatility).toBe(0);
        expect(PaymentStreamLinkStrikePriceType.Variance).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamLinkStrikePriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamLinkStrikePriceType.Volatility,
            PaymentStreamLinkStrikePriceType.Variance,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamLinkStrikePriceType type', () => {
        const validate = (value: PaymentStreamLinkStrikePriceType) => {
            expect(Object.values(PaymentStreamLinkStrikePriceType)).toContain(value);
        };

        validate(PaymentStreamLinkStrikePriceType.Volatility);
        validate(PaymentStreamLinkStrikePriceType.Variance);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamLinkStrikePriceType;
        expect(() => {
            ref.Volatility = 10;
        }).toThrowError();
    });
});
