import { NewsRefType } from '../../src/fieldtypes/NewsRefType';

describe('NewsRefType', () => {
    test('should have the correct values', () => {
        expect(NewsRefType.Replacement).toBe(0);
        expect(NewsRefType.OtherLanguage).toBe(1);
        expect(NewsRefType.Complimentary).toBe(2);
        expect(NewsRefType.Withdrawal).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NewsRefType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NewsRefType.Replacement,
            NewsRefType.OtherLanguage,
            NewsRefType.Complimentary,
            NewsRefType.Withdrawal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NewsRefType type', () => {
        const validate = (value: NewsRefType) => {
            expect(Object.values(NewsRefType)).toContain(value);
        };

        validate(NewsRefType.Replacement);
        validate(NewsRefType.OtherLanguage);
        validate(NewsRefType.Complimentary);
        validate(NewsRefType.Withdrawal);
    });

    test('should be immutable', () => {
        const ref = NewsRefType;
        expect(() => {
            ref.Replacement = 10;
        }).toThrowError();
    });
});
