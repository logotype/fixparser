import { StrikePriceBoundaryMethod } from '../../src/fieldtypes/StrikePriceBoundaryMethod';

describe('StrikePriceBoundaryMethod', () => {
    test('should have the correct values', () => {
        expect(StrikePriceBoundaryMethod.LessThan).toBe(1);
        expect(StrikePriceBoundaryMethod.LessThanOrEqual).toBe(2);
        expect(StrikePriceBoundaryMethod.Equal).toBe(3);
        expect(StrikePriceBoundaryMethod.GreaterThanOrEqual).toBe(4);
        expect(StrikePriceBoundaryMethod.GreaterThan).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StrikePriceBoundaryMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StrikePriceBoundaryMethod.LessThan,
            StrikePriceBoundaryMethod.LessThanOrEqual,
            StrikePriceBoundaryMethod.Equal,
            StrikePriceBoundaryMethod.GreaterThanOrEqual,
            StrikePriceBoundaryMethod.GreaterThan,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StrikePriceBoundaryMethod type', () => {
        const validate = (value: StrikePriceBoundaryMethod) => {
            expect(Object.values(StrikePriceBoundaryMethod)).toContain(value);
        };

        validate(StrikePriceBoundaryMethod.LessThan);
        validate(StrikePriceBoundaryMethod.LessThanOrEqual);
        validate(StrikePriceBoundaryMethod.Equal);
        validate(StrikePriceBoundaryMethod.GreaterThanOrEqual);
        validate(StrikePriceBoundaryMethod.GreaterThan);
    });

    test('should be immutable', () => {
        const ref = StrikePriceBoundaryMethod;
        expect(() => {
            ref.LessThan = 10;
        }).toThrowError();
    });
});
