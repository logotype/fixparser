import { SubscriptionRequestType } from '../../src/fieldtypes/SubscriptionRequestType';

describe('SubscriptionRequestType', () => {
    test('should have the correct values', () => {
        expect(SubscriptionRequestType.Snapshot).toBe('0');
        expect(SubscriptionRequestType.SnapshotAndUpdates).toBe('1');
        expect(SubscriptionRequestType.DisablePreviousSnapshot).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SubscriptionRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SubscriptionRequestType.Snapshot,
            SubscriptionRequestType.SnapshotAndUpdates,
            SubscriptionRequestType.DisablePreviousSnapshot,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SubscriptionRequestType type', () => {
        const validate = (value: SubscriptionRequestType) => {
            expect(Object.values(SubscriptionRequestType)).toContain(value);
        };

        validate(SubscriptionRequestType.Snapshot);
        validate(SubscriptionRequestType.SnapshotAndUpdates);
        validate(SubscriptionRequestType.DisablePreviousSnapshot);
    });

    test('should be immutable', () => {
        const ref = SubscriptionRequestType;
        expect(() => {
            ref.Snapshot = 10;
        }).toThrowError();
    });
});
