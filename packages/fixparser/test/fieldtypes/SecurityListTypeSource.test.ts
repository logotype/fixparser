import { SecurityListTypeSource } from '../../src/fieldtypes/SecurityListTypeSource';

describe('SecurityListTypeSource', () => {
    test('should have the correct values', () => {
        expect(SecurityListTypeSource.ICB).toBe(1);
        expect(SecurityListTypeSource.NAICS).toBe(2);
        expect(SecurityListTypeSource.GICS).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityListTypeSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityListTypeSource.ICB,
            SecurityListTypeSource.NAICS,
            SecurityListTypeSource.GICS,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityListTypeSource type', () => {
        const validate = (value: SecurityListTypeSource) => {
            expect(Object.values(SecurityListTypeSource)).toContain(value);
        };

        validate(SecurityListTypeSource.ICB);
        validate(SecurityListTypeSource.NAICS);
        validate(SecurityListTypeSource.GICS);
    });

    test('should be immutable', () => {
        const ref = SecurityListTypeSource;
        expect(() => {
            ref.ICB = 10;
        }).toThrowError();
    });
});
