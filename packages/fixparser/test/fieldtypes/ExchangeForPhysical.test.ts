import { ExchangeForPhysical } from '../../src/fieldtypes/ExchangeForPhysical';

describe('ExchangeForPhysical', () => {
    test('should have the correct values', () => {
        expect(ExchangeForPhysical.False).toBe('N');
        expect(ExchangeForPhysical.True).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExchangeForPhysical.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ExchangeForPhysical.False, ExchangeForPhysical.True];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExchangeForPhysical type', () => {
        const validate = (value: ExchangeForPhysical) => {
            expect(Object.values(ExchangeForPhysical)).toContain(value);
        };

        validate(ExchangeForPhysical.False);
        validate(ExchangeForPhysical.True);
    });

    test('should be immutable', () => {
        const ref = ExchangeForPhysical;
        expect(() => {
            ref.False = 10;
        }).toThrowError();
    });
});
