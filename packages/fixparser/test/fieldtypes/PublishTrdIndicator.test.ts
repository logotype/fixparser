import { PublishTrdIndicator } from '../../src/fieldtypes/PublishTrdIndicator';

describe('PublishTrdIndicator', () => {
    test('should have the correct values', () => {
        expect(PublishTrdIndicator.DoNotReportTrade).toBe('N');
        expect(PublishTrdIndicator.ReportTrade).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PublishTrdIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PublishTrdIndicator.DoNotReportTrade, PublishTrdIndicator.ReportTrade];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PublishTrdIndicator type', () => {
        const validate = (value: PublishTrdIndicator) => {
            expect(Object.values(PublishTrdIndicator)).toContain(value);
        };

        validate(PublishTrdIndicator.DoNotReportTrade);
        validate(PublishTrdIndicator.ReportTrade);
    });

    test('should be immutable', () => {
        const ref = PublishTrdIndicator;
        expect(() => {
            ref.DoNotReportTrade = 10;
        }).toThrowError();
    });
});
