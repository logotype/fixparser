import { ThrottleStatus } from '../../src/fieldtypes/ThrottleStatus';

describe('ThrottleStatus', () => {
    test('should have the correct values', () => {
        expect(ThrottleStatus.ThrottleLimitNotExceededNotQueued).toBe(0);
        expect(ThrottleStatus.QueuedDueToThrottleLimitExceeded).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ThrottleStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ThrottleStatus.ThrottleLimitNotExceededNotQueued,
            ThrottleStatus.QueuedDueToThrottleLimitExceeded,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ThrottleStatus type', () => {
        const validate = (value: ThrottleStatus) => {
            expect(Object.values(ThrottleStatus)).toContain(value);
        };

        validate(ThrottleStatus.ThrottleLimitNotExceededNotQueued);
        validate(ThrottleStatus.QueuedDueToThrottleLimitExceeded);
    });

    test('should be immutable', () => {
        const ref = ThrottleStatus;
        expect(() => {
            ref.ThrottleLimitNotExceededNotQueued = 10;
        }).toThrowError();
    });
});
