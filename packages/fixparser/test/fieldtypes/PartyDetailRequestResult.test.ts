import { PartyDetailRequestResult } from '../../src/fieldtypes/PartyDetailRequestResult';

describe('PartyDetailRequestResult', () => {
    test('should have the correct values', () => {
        expect(PartyDetailRequestResult.Successful).toBe(0);
        expect(PartyDetailRequestResult.InvalidParty).toBe(1);
        expect(PartyDetailRequestResult.InvalidRelatedParty).toBe(2);
        expect(PartyDetailRequestResult.InvalidPartyStatus).toBe(3);
        expect(PartyDetailRequestResult.NotAuthorized).toBe(98);
        expect(PartyDetailRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyDetailRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyDetailRequestResult.Successful,
            PartyDetailRequestResult.InvalidParty,
            PartyDetailRequestResult.InvalidRelatedParty,
            PartyDetailRequestResult.InvalidPartyStatus,
            PartyDetailRequestResult.NotAuthorized,
            PartyDetailRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyDetailRequestResult type', () => {
        const validate = (value: PartyDetailRequestResult) => {
            expect(Object.values(PartyDetailRequestResult)).toContain(value);
        };

        validate(PartyDetailRequestResult.Successful);
        validate(PartyDetailRequestResult.InvalidParty);
        validate(PartyDetailRequestResult.InvalidRelatedParty);
        validate(PartyDetailRequestResult.InvalidPartyStatus);
        validate(PartyDetailRequestResult.NotAuthorized);
        validate(PartyDetailRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = PartyDetailRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
