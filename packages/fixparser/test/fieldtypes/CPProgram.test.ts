import { CPProgram } from '../../src/fieldtypes/CPProgram';

describe('CPProgram', () => {
    test('should have the correct values', () => {
        expect(CPProgram.Program3a3).toBe(1);
        expect(CPProgram.Program42).toBe(2);
        expect(CPProgram.Program3a2).toBe(3);
        expect(CPProgram.Program3a3And3c7).toBe(4);
        expect(CPProgram.Program3a4).toBe(5);
        expect(CPProgram.Program3a5).toBe(6);
        expect(CPProgram.Program3a7).toBe(7);
        expect(CPProgram.Program3c7).toBe(8);
        expect(CPProgram.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CPProgram.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CPProgram.Program3a3,
            CPProgram.Program42,
            CPProgram.Program3a2,
            CPProgram.Program3a3And3c7,
            CPProgram.Program3a4,
            CPProgram.Program3a5,
            CPProgram.Program3a7,
            CPProgram.Program3c7,
            CPProgram.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CPProgram type', () => {
        const validate = (value: CPProgram) => {
            expect(Object.values(CPProgram)).toContain(value);
        };

        validate(CPProgram.Program3a3);
        validate(CPProgram.Program42);
        validate(CPProgram.Program3a2);
        validate(CPProgram.Program3a3And3c7);
        validate(CPProgram.Program3a4);
        validate(CPProgram.Program3a5);
        validate(CPProgram.Program3a7);
        validate(CPProgram.Program3c7);
        validate(CPProgram.Other);
    });

    test('should be immutable', () => {
        const ref = CPProgram;
        expect(() => {
            ref.Program3a3 = 10;
        }).toThrowError();
    });
});
