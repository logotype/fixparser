import { CrossedIndicator } from '../../src/fieldtypes/CrossedIndicator';

describe('CrossedIndicator', () => {
    test('should have the correct values', () => {
        expect(CrossedIndicator.NoCross).toBe(0);
        expect(CrossedIndicator.CrossRejected).toBe(1);
        expect(CrossedIndicator.CrossAccepted).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CrossedIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CrossedIndicator.NoCross,
            CrossedIndicator.CrossRejected,
            CrossedIndicator.CrossAccepted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CrossedIndicator type', () => {
        const validate = (value: CrossedIndicator) => {
            expect(Object.values(CrossedIndicator)).toContain(value);
        };

        validate(CrossedIndicator.NoCross);
        validate(CrossedIndicator.CrossRejected);
        validate(CrossedIndicator.CrossAccepted);
    });

    test('should be immutable', () => {
        const ref = CrossedIndicator;
        expect(() => {
            ref.NoCross = 10;
        }).toThrowError();
    });
});
