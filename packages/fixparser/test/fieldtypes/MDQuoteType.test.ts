import { MDQuoteType } from '../../src/fieldtypes/MDQuoteType';

describe('MDQuoteType', () => {
    test('should have the correct values', () => {
        expect(MDQuoteType.Indicative).toBe(0);
        expect(MDQuoteType.Tradeable).toBe(1);
        expect(MDQuoteType.RestrictedTradeable).toBe(2);
        expect(MDQuoteType.Counter).toBe(3);
        expect(MDQuoteType.IndicativeAndTradeable).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDQuoteType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDQuoteType.Indicative,
            MDQuoteType.Tradeable,
            MDQuoteType.RestrictedTradeable,
            MDQuoteType.Counter,
            MDQuoteType.IndicativeAndTradeable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDQuoteType type', () => {
        const validate = (value: MDQuoteType) => {
            expect(Object.values(MDQuoteType)).toContain(value);
        };

        validate(MDQuoteType.Indicative);
        validate(MDQuoteType.Tradeable);
        validate(MDQuoteType.RestrictedTradeable);
        validate(MDQuoteType.Counter);
        validate(MDQuoteType.IndicativeAndTradeable);
    });

    test('should be immutable', () => {
        const ref = MDQuoteType;
        expect(() => {
            ref.Indicative = 10;
        }).toThrowError();
    });
});
