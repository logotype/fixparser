import { DeliveryType } from '../../src/fieldtypes/DeliveryType';

describe('DeliveryType', () => {
    test('should have the correct values', () => {
        expect(DeliveryType.VersusPayment).toBe(0);
        expect(DeliveryType.Free).toBe(1);
        expect(DeliveryType.TriParty).toBe(2);
        expect(DeliveryType.HoldInCustody).toBe(3);
        expect(DeliveryType.DeliverByValue).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryType.VersusPayment,
            DeliveryType.Free,
            DeliveryType.TriParty,
            DeliveryType.HoldInCustody,
            DeliveryType.DeliverByValue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryType type', () => {
        const validate = (value: DeliveryType) => {
            expect(Object.values(DeliveryType)).toContain(value);
        };

        validate(DeliveryType.VersusPayment);
        validate(DeliveryType.Free);
        validate(DeliveryType.TriParty);
        validate(DeliveryType.HoldInCustody);
        validate(DeliveryType.DeliverByValue);
    });

    test('should be immutable', () => {
        const ref = DeliveryType;
        expect(() => {
            ref.VersusPayment = 10;
        }).toThrowError();
    });
});
