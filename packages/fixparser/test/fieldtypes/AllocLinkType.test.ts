import { AllocLinkType } from '../../src/fieldtypes/AllocLinkType';

describe('AllocLinkType', () => {
    test('should have the correct values', () => {
        expect(AllocLinkType.FXNetting).toBe(0);
        expect(AllocLinkType.FXSwap).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocLinkType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AllocLinkType.FXNetting, AllocLinkType.FXSwap];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocLinkType type', () => {
        const validate = (value: AllocLinkType) => {
            expect(Object.values(AllocLinkType)).toContain(value);
        };

        validate(AllocLinkType.FXNetting);
        validate(AllocLinkType.FXSwap);
    });

    test('should be immutable', () => {
        const ref = AllocLinkType;
        expect(() => {
            ref.FXNetting = 10;
        }).toThrowError();
    });
});
