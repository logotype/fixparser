import { MassCancelResponse } from '../../src/fieldtypes/MassCancelResponse';

describe('MassCancelResponse', () => {
    test('should have the correct values', () => {
        expect(MassCancelResponse.CancelRequestRejected).toBe('0');
        expect(MassCancelResponse.CancelOrdersForASecurity).toBe('1');
        expect(MassCancelResponse.CancelOrdersForAnUnderlyingSecurity).toBe('2');
        expect(MassCancelResponse.CancelOrdersForAProduct).toBe('3');
        expect(MassCancelResponse.CancelOrdersForACFICode).toBe('4');
        expect(MassCancelResponse.CancelOrdersForASecurityType).toBe('5');
        expect(MassCancelResponse.CancelOrdersForATradingSession).toBe('6');
        expect(MassCancelResponse.CancelAllOrders).toBe('7');
        expect(MassCancelResponse.CancelOrdersForAMarket).toBe('8');
        expect(MassCancelResponse.CancelOrdersForAMarketSegment).toBe('9');
        expect(MassCancelResponse.CancelOrdersForASecurityGroup).toBe('A');
        expect(MassCancelResponse.CancelOrdersForASecuritiesIssuer).toBe('B');
        expect(MassCancelResponse.CancelOrdersForIssuerOfUnderlyingSecurity).toBe('C');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassCancelResponse.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassCancelResponse.CancelRequestRejected,
            MassCancelResponse.CancelOrdersForASecurity,
            MassCancelResponse.CancelOrdersForAnUnderlyingSecurity,
            MassCancelResponse.CancelOrdersForAProduct,
            MassCancelResponse.CancelOrdersForACFICode,
            MassCancelResponse.CancelOrdersForASecurityType,
            MassCancelResponse.CancelOrdersForATradingSession,
            MassCancelResponse.CancelAllOrders,
            MassCancelResponse.CancelOrdersForAMarket,
            MassCancelResponse.CancelOrdersForAMarketSegment,
            MassCancelResponse.CancelOrdersForASecurityGroup,
            MassCancelResponse.CancelOrdersForASecuritiesIssuer,
            MassCancelResponse.CancelOrdersForIssuerOfUnderlyingSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MassCancelResponse type', () => {
        const validate = (value: MassCancelResponse) => {
            expect(Object.values(MassCancelResponse)).toContain(value);
        };

        validate(MassCancelResponse.CancelRequestRejected);
        validate(MassCancelResponse.CancelOrdersForASecurity);
        validate(MassCancelResponse.CancelOrdersForAnUnderlyingSecurity);
        validate(MassCancelResponse.CancelOrdersForAProduct);
        validate(MassCancelResponse.CancelOrdersForACFICode);
        validate(MassCancelResponse.CancelOrdersForASecurityType);
        validate(MassCancelResponse.CancelOrdersForATradingSession);
        validate(MassCancelResponse.CancelAllOrders);
        validate(MassCancelResponse.CancelOrdersForAMarket);
        validate(MassCancelResponse.CancelOrdersForAMarketSegment);
        validate(MassCancelResponse.CancelOrdersForASecurityGroup);
        validate(MassCancelResponse.CancelOrdersForASecuritiesIssuer);
        validate(MassCancelResponse.CancelOrdersForIssuerOfUnderlyingSecurity);
    });

    test('should be immutable', () => {
        const ref = MassCancelResponse;
        expect(() => {
            ref.CancelRequestRejected = 10;
        }).toThrowError();
    });
});
