import { PosTransType } from '../../src/fieldtypes/PosTransType';

describe('PosTransType', () => {
    test('should have the correct values', () => {
        expect(PosTransType.Exercise).toBe(1);
        expect(PosTransType.DoNotExercise).toBe(2);
        expect(PosTransType.PositionAdjustment).toBe(3);
        expect(PosTransType.PositionChangeSubmission).toBe(4);
        expect(PosTransType.Pledge).toBe(5);
        expect(PosTransType.LargeTraderSubmission).toBe(6);
        expect(PosTransType.LargePositionsReportingSubmission).toBe(7);
        expect(PosTransType.LongHoldings).toBe(8);
        expect(PosTransType.InternalTransfer).toBe(9);
        expect(PosTransType.TransferOfFirm).toBe(10);
        expect(PosTransType.ExternalTransfer).toBe(11);
        expect(PosTransType.CorporateAction).toBe(12);
        expect(PosTransType.Notification).toBe(13);
        expect(PosTransType.PositionCreation).toBe(14);
        expect(PosTransType.Closeout).toBe(15);
        expect(PosTransType.Reopen).toBe(16);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosTransType.Exercise,
            PosTransType.DoNotExercise,
            PosTransType.PositionAdjustment,
            PosTransType.PositionChangeSubmission,
            PosTransType.Pledge,
            PosTransType.LargeTraderSubmission,
            PosTransType.LargePositionsReportingSubmission,
            PosTransType.LongHoldings,
            PosTransType.InternalTransfer,
            PosTransType.TransferOfFirm,
            PosTransType.ExternalTransfer,
            PosTransType.CorporateAction,
            PosTransType.Notification,
            PosTransType.PositionCreation,
            PosTransType.Closeout,
            PosTransType.Reopen,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosTransType type', () => {
        const validate = (value: PosTransType) => {
            expect(Object.values(PosTransType)).toContain(value);
        };

        validate(PosTransType.Exercise);
        validate(PosTransType.DoNotExercise);
        validate(PosTransType.PositionAdjustment);
        validate(PosTransType.PositionChangeSubmission);
        validate(PosTransType.Pledge);
        validate(PosTransType.LargeTraderSubmission);
        validate(PosTransType.LargePositionsReportingSubmission);
        validate(PosTransType.LongHoldings);
        validate(PosTransType.InternalTransfer);
        validate(PosTransType.TransferOfFirm);
        validate(PosTransType.ExternalTransfer);
        validate(PosTransType.CorporateAction);
        validate(PosTransType.Notification);
        validate(PosTransType.PositionCreation);
        validate(PosTransType.Closeout);
        validate(PosTransType.Reopen);
    });

    test('should be immutable', () => {
        const ref = PosTransType;
        expect(() => {
            ref.Exercise = 10;
        }).toThrowError();
    });
});
