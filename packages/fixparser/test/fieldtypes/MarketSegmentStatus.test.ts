import { MarketSegmentStatus } from '../../src/fieldtypes/MarketSegmentStatus';

describe('MarketSegmentStatus', () => {
    test('should have the correct values', () => {
        expect(MarketSegmentStatus.Active).toBe(1);
        expect(MarketSegmentStatus.Inactive).toBe(2);
        expect(MarketSegmentStatus.Published).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketSegmentStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketSegmentStatus.Active,
            MarketSegmentStatus.Inactive,
            MarketSegmentStatus.Published,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketSegmentStatus type', () => {
        const validate = (value: MarketSegmentStatus) => {
            expect(Object.values(MarketSegmentStatus)).toContain(value);
        };

        validate(MarketSegmentStatus.Active);
        validate(MarketSegmentStatus.Inactive);
        validate(MarketSegmentStatus.Published);
    });

    test('should be immutable', () => {
        const ref = MarketSegmentStatus;
        expect(() => {
            ref.Active = 10;
        }).toThrowError();
    });
});
