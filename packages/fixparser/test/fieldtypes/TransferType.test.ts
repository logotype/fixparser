import { TransferType } from '../../src/fieldtypes/TransferType';

describe('TransferType', () => {
    test('should have the correct values', () => {
        expect(TransferType.RequestTransfer).toBe(0);
        expect(TransferType.AcceptTransfer).toBe(1);
        expect(TransferType.DeclineTransfer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TransferType.RequestTransfer,
            TransferType.AcceptTransfer,
            TransferType.DeclineTransfer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferType type', () => {
        const validate = (value: TransferType) => {
            expect(Object.values(TransferType)).toContain(value);
        };

        validate(TransferType.RequestTransfer);
        validate(TransferType.AcceptTransfer);
        validate(TransferType.DeclineTransfer);
    });

    test('should be immutable', () => {
        const ref = TransferType;
        expect(() => {
            ref.RequestTransfer = 10;
        }).toThrowError();
    });
});
