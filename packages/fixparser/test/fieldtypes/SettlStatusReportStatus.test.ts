import { SettlStatusReportStatus } from '../../src/fieldtypes/SettlStatusReportStatus';

describe('SettlStatusReportStatus', () => {
    test('should have the correct values', () => {
        expect(SettlStatusReportStatus.Received).toBe(0);
        expect(SettlStatusReportStatus.Accepted).toBe(1);
        expect(SettlStatusReportStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlStatusReportStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlStatusReportStatus.Received,
            SettlStatusReportStatus.Accepted,
            SettlStatusReportStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlStatusReportStatus type', () => {
        const validate = (value: SettlStatusReportStatus) => {
            expect(Object.values(SettlStatusReportStatus)).toContain(value);
        };

        validate(SettlStatusReportStatus.Received);
        validate(SettlStatusReportStatus.Accepted);
        validate(SettlStatusReportStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = SettlStatusReportStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
