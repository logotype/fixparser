import { IOIQltyInd } from '../../src/fieldtypes/IOIQltyInd';

describe('IOIQltyInd', () => {
    test('should have the correct values', () => {
        expect(IOIQltyInd.High).toBe('H');
        expect(IOIQltyInd.Low).toBe('L');
        expect(IOIQltyInd.Medium).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOIQltyInd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IOIQltyInd.High, IOIQltyInd.Low, IOIQltyInd.Medium];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOIQltyInd type', () => {
        const validate = (value: IOIQltyInd) => {
            expect(Object.values(IOIQltyInd)).toContain(value);
        };

        validate(IOIQltyInd.High);
        validate(IOIQltyInd.Low);
        validate(IOIQltyInd.Medium);
    });

    test('should be immutable', () => {
        const ref = IOIQltyInd;
        expect(() => {
            ref.High = 10;
        }).toThrowError();
    });
});
