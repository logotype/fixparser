import { SettlSessID } from '../../src/fieldtypes/SettlSessID';

describe('SettlSessID', () => {
    test('should have the correct values', () => {
        expect(SettlSessID.Intraday).toBe('ITD');
        expect(SettlSessID.RegularTradingHours).toBe('RTH');
        expect(SettlSessID.ElectronicTradingHours).toBe('ETH');
        expect(SettlSessID.EndOfDay).toBe('EOD');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlSessID.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlSessID.Intraday,
            SettlSessID.RegularTradingHours,
            SettlSessID.ElectronicTradingHours,
            SettlSessID.EndOfDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlSessID type', () => {
        const validate = (value: SettlSessID) => {
            expect(Object.values(SettlSessID)).toContain(value);
        };

        validate(SettlSessID.Intraday);
        validate(SettlSessID.RegularTradingHours);
        validate(SettlSessID.ElectronicTradingHours);
        validate(SettlSessID.EndOfDay);
    });

    test('should be immutable', () => {
        const ref = SettlSessID;
        expect(() => {
            ref.Intraday = 10;
        }).toThrowError();
    });
});
