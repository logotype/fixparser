import { NonCashDividendTreatment } from '../../src/fieldtypes/NonCashDividendTreatment';

describe('NonCashDividendTreatment', () => {
    test('should have the correct values', () => {
        expect(NonCashDividendTreatment.PotentialAdjustment).toBe(0);
        expect(NonCashDividendTreatment.CashEquivalent).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NonCashDividendTreatment.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            NonCashDividendTreatment.PotentialAdjustment,
            NonCashDividendTreatment.CashEquivalent,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NonCashDividendTreatment type', () => {
        const validate = (value: NonCashDividendTreatment) => {
            expect(Object.values(NonCashDividendTreatment)).toContain(value);
        };

        validate(NonCashDividendTreatment.PotentialAdjustment);
        validate(NonCashDividendTreatment.CashEquivalent);
    });

    test('should be immutable', () => {
        const ref = NonCashDividendTreatment;
        expect(() => {
            ref.PotentialAdjustment = 10;
        }).toThrowError();
    });
});
