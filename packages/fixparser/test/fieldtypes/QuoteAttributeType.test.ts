import { QuoteAttributeType } from '../../src/fieldtypes/QuoteAttributeType';

describe('QuoteAttributeType', () => {
    test('should have the correct values', () => {
        expect(QuoteAttributeType.QuoteAboveStandardMarketSize).toBe(0);
        expect(QuoteAttributeType.QuoteAboveSpecificInstrumentSize).toBe(1);
        expect(QuoteAttributeType.QuoteApplicableForLiquidtyProvisionActivity).toBe(2);
        expect(QuoteAttributeType.QuoteIssuerStatus).toBe(3);
        expect(QuoteAttributeType.BidOrAskRequest).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteAttributeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteAttributeType.QuoteAboveStandardMarketSize,
            QuoteAttributeType.QuoteAboveSpecificInstrumentSize,
            QuoteAttributeType.QuoteApplicableForLiquidtyProvisionActivity,
            QuoteAttributeType.QuoteIssuerStatus,
            QuoteAttributeType.BidOrAskRequest,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteAttributeType type', () => {
        const validate = (value: QuoteAttributeType) => {
            expect(Object.values(QuoteAttributeType)).toContain(value);
        };

        validate(QuoteAttributeType.QuoteAboveStandardMarketSize);
        validate(QuoteAttributeType.QuoteAboveSpecificInstrumentSize);
        validate(QuoteAttributeType.QuoteApplicableForLiquidtyProvisionActivity);
        validate(QuoteAttributeType.QuoteIssuerStatus);
        validate(QuoteAttributeType.BidOrAskRequest);
    });

    test('should be immutable', () => {
        const ref = QuoteAttributeType;
        expect(() => {
            ref.QuoteAboveStandardMarketSize = 10;
        }).toThrowError();
    });
});
