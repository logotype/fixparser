import { PaymentType } from '../../src/fieldtypes/PaymentType';

describe('PaymentType', () => {
    test('should have the correct values', () => {
        expect(PaymentType.Brokerage).toBe(0);
        expect(PaymentType.UpfrontFee).toBe(1);
        expect(PaymentType.IndependentAmountCollateral).toBe(2);
        expect(PaymentType.PrincipalExchange).toBe(3);
        expect(PaymentType.NovationTermination).toBe(4);
        expect(PaymentType.EarlyTerminationProvision).toBe(5);
        expect(PaymentType.CancelableProvision).toBe(6);
        expect(PaymentType.ExtendibleProvision).toBe(7);
        expect(PaymentType.CapRateProvision).toBe(8);
        expect(PaymentType.FloorRateProvision).toBe(9);
        expect(PaymentType.OptionPremium).toBe(10);
        expect(PaymentType.SettlementPayment).toBe(11);
        expect(PaymentType.CashSettl).toBe(12);
        expect(PaymentType.SecurityLending).toBe(13);
        expect(PaymentType.Rebate).toBe(14);
        expect(PaymentType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentType.Brokerage,
            PaymentType.UpfrontFee,
            PaymentType.IndependentAmountCollateral,
            PaymentType.PrincipalExchange,
            PaymentType.NovationTermination,
            PaymentType.EarlyTerminationProvision,
            PaymentType.CancelableProvision,
            PaymentType.ExtendibleProvision,
            PaymentType.CapRateProvision,
            PaymentType.FloorRateProvision,
            PaymentType.OptionPremium,
            PaymentType.SettlementPayment,
            PaymentType.CashSettl,
            PaymentType.SecurityLending,
            PaymentType.Rebate,
            PaymentType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentType type', () => {
        const validate = (value: PaymentType) => {
            expect(Object.values(PaymentType)).toContain(value);
        };

        validate(PaymentType.Brokerage);
        validate(PaymentType.UpfrontFee);
        validate(PaymentType.IndependentAmountCollateral);
        validate(PaymentType.PrincipalExchange);
        validate(PaymentType.NovationTermination);
        validate(PaymentType.EarlyTerminationProvision);
        validate(PaymentType.CancelableProvision);
        validate(PaymentType.ExtendibleProvision);
        validate(PaymentType.CapRateProvision);
        validate(PaymentType.FloorRateProvision);
        validate(PaymentType.OptionPremium);
        validate(PaymentType.SettlementPayment);
        validate(PaymentType.CashSettl);
        validate(PaymentType.SecurityLending);
        validate(PaymentType.Rebate);
        validate(PaymentType.Other);
    });

    test('should be immutable', () => {
        const ref = PaymentType;
        expect(() => {
            ref.Brokerage = 10;
        }).toThrowError();
    });
});
