import { UnsolicitedIndicator } from '../../src/fieldtypes/UnsolicitedIndicator';

describe('UnsolicitedIndicator', () => {
    test('should have the correct values', () => {
        expect(UnsolicitedIndicator.MessageIsBeingSentAsAResultOfAPriorRequest).toBe('N');
        expect(UnsolicitedIndicator.MessageIsBeingSentUnsolicited).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnsolicitedIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            UnsolicitedIndicator.MessageIsBeingSentAsAResultOfAPriorRequest,
            UnsolicitedIndicator.MessageIsBeingSentUnsolicited,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for UnsolicitedIndicator type', () => {
        const validate = (value: UnsolicitedIndicator) => {
            expect(Object.values(UnsolicitedIndicator)).toContain(value);
        };

        validate(UnsolicitedIndicator.MessageIsBeingSentAsAResultOfAPriorRequest);
        validate(UnsolicitedIndicator.MessageIsBeingSentUnsolicited);
    });

    test('should be immutable', () => {
        const ref = UnsolicitedIndicator;
        expect(() => {
            ref.MessageIsBeingSentAsAResultOfAPriorRequest = 10;
        }).toThrowError();
    });
});
