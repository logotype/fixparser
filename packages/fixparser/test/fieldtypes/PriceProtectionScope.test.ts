import { PriceProtectionScope } from '../../src/fieldtypes/PriceProtectionScope';

describe('PriceProtectionScope', () => {
    test('should have the correct values', () => {
        expect(PriceProtectionScope.None).toBe('0');
        expect(PriceProtectionScope.Local).toBe('1');
        expect(PriceProtectionScope.National).toBe('2');
        expect(PriceProtectionScope.Global).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceProtectionScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PriceProtectionScope.None,
            PriceProtectionScope.Local,
            PriceProtectionScope.National,
            PriceProtectionScope.Global,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PriceProtectionScope type', () => {
        const validate = (value: PriceProtectionScope) => {
            expect(Object.values(PriceProtectionScope)).toContain(value);
        };

        validate(PriceProtectionScope.None);
        validate(PriceProtectionScope.Local);
        validate(PriceProtectionScope.National);
        validate(PriceProtectionScope.Global);
    });

    test('should be immutable', () => {
        const ref = PriceProtectionScope;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
