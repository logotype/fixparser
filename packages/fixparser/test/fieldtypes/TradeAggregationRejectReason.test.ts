import { TradeAggregationRejectReason } from '../../src/fieldtypes/TradeAggregationRejectReason';

describe('TradeAggregationRejectReason', () => {
    test('should have the correct values', () => {
        expect(TradeAggregationRejectReason.UnknownOrders).toBe(0);
        expect(TradeAggregationRejectReason.UnknownExecutionFills).toBe(1);
        expect(TradeAggregationRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeAggregationRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeAggregationRejectReason.UnknownOrders,
            TradeAggregationRejectReason.UnknownExecutionFills,
            TradeAggregationRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeAggregationRejectReason type', () => {
        const validate = (value: TradeAggregationRejectReason) => {
            expect(Object.values(TradeAggregationRejectReason)).toContain(value);
        };

        validate(TradeAggregationRejectReason.UnknownOrders);
        validate(TradeAggregationRejectReason.UnknownExecutionFills);
        validate(TradeAggregationRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = TradeAggregationRejectReason;
        expect(() => {
            ref.UnknownOrders = 10;
        }).toThrowError();
    });
});
