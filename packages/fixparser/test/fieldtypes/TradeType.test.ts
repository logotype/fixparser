import { TradeType } from '../../src/fieldtypes/TradeType';

describe('TradeType', () => {
    test('should have the correct values', () => {
        expect(TradeType.Agency).toBe('A');
        expect(TradeType.VWAPGuarantee).toBe('G');
        expect(TradeType.GuaranteedClose).toBe('J');
        expect(TradeType.RiskTrade).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeType.Agency,
            TradeType.VWAPGuarantee,
            TradeType.GuaranteedClose,
            TradeType.RiskTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TradeType type', () => {
        const validate = (value: TradeType) => {
            expect(Object.values(TradeType)).toContain(value);
        };

        validate(TradeType.Agency);
        validate(TradeType.VWAPGuarantee);
        validate(TradeType.GuaranteedClose);
        validate(TradeType.RiskTrade);
    });

    test('should be immutable', () => {
        const ref = TradeType;
        expect(() => {
            ref.Agency = 10;
        }).toThrowError();
    });
});
