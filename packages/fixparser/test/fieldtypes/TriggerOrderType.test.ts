import { TriggerOrderType } from '../../src/fieldtypes/TriggerOrderType';

describe('TriggerOrderType', () => {
    test('should have the correct values', () => {
        expect(TriggerOrderType.Market).toBe('1');
        expect(TriggerOrderType.Limit).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerOrderType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TriggerOrderType.Market, TriggerOrderType.Limit];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerOrderType type', () => {
        const validate = (value: TriggerOrderType) => {
            expect(Object.values(TriggerOrderType)).toContain(value);
        };

        validate(TriggerOrderType.Market);
        validate(TriggerOrderType.Limit);
    });

    test('should be immutable', () => {
        const ref = TriggerOrderType;
        expect(() => {
            ref.Market = 10;
        }).toThrowError();
    });
});
