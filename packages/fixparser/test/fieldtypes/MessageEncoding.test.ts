import { MessageEncoding } from '../../src/fieldtypes/MessageEncoding';

describe('MessageEncoding', () => {
    test('should have the correct values', () => {
        expect(MessageEncoding.ISO2022JP).toBe('ISO-2022-JP');
        expect(MessageEncoding.EUCJP).toBe('EUC-JP');
        expect(MessageEncoding.ShiftJIS).toBe('Shift_JIS');
        expect(MessageEncoding.UTF8).toBe('UTF-8');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MessageEncoding.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MessageEncoding.ISO2022JP,
            MessageEncoding.EUCJP,
            MessageEncoding.ShiftJIS,
            MessageEncoding.UTF8,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MessageEncoding type', () => {
        const validate = (value: MessageEncoding) => {
            expect(Object.values(MessageEncoding)).toContain(value);
        };

        validate(MessageEncoding.ISO2022JP);
        validate(MessageEncoding.EUCJP);
        validate(MessageEncoding.ShiftJIS);
        validate(MessageEncoding.UTF8);
    });

    test('should be immutable', () => {
        const ref = MessageEncoding;
        expect(() => {
            ref.ISO2022JP = 10;
        }).toThrowError();
    });
});
