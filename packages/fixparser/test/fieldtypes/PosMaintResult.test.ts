import { PosMaintResult } from '../../src/fieldtypes/PosMaintResult';

describe('PosMaintResult', () => {
    test('should have the correct values', () => {
        expect(PosMaintResult.SuccessfulCompletion).toBe(0);
        expect(PosMaintResult.Rejected).toBe(1);
        expect(PosMaintResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosMaintResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PosMaintResult.SuccessfulCompletion, PosMaintResult.Rejected, PosMaintResult.Other];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosMaintResult type', () => {
        const validate = (value: PosMaintResult) => {
            expect(Object.values(PosMaintResult)).toContain(value);
        };

        validate(PosMaintResult.SuccessfulCompletion);
        validate(PosMaintResult.Rejected);
        validate(PosMaintResult.Other);
    });

    test('should be immutable', () => {
        const ref = PosMaintResult;
        expect(() => {
            ref.SuccessfulCompletion = 10;
        }).toThrowError();
    });
});
