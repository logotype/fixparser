import { MassActionType } from '../../src/fieldtypes/MassActionType';

describe('MassActionType', () => {
    test('should have the correct values', () => {
        expect(MassActionType.SuspendOrders).toBe(1);
        expect(MassActionType.ReleaseOrdersFromSuspension).toBe(2);
        expect(MassActionType.CancelOrders).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassActionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassActionType.SuspendOrders,
            MassActionType.ReleaseOrdersFromSuspension,
            MassActionType.CancelOrders,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassActionType type', () => {
        const validate = (value: MassActionType) => {
            expect(Object.values(MassActionType)).toContain(value);
        };

        validate(MassActionType.SuspendOrders);
        validate(MassActionType.ReleaseOrdersFromSuspension);
        validate(MassActionType.CancelOrders);
    });

    test('should be immutable', () => {
        const ref = MassActionType;
        expect(() => {
            ref.SuspendOrders = 10;
        }).toThrowError();
    });
});
