import { PaymentMethod } from '../../src/fieldtypes/PaymentMethod';

describe('PaymentMethod', () => {
    test('should have the correct values', () => {
        expect(PaymentMethod.CREST).toBe(1);
        expect(PaymentMethod.NSCC).toBe(2);
        expect(PaymentMethod.Euroclear).toBe(3);
        expect(PaymentMethod.Clearstream).toBe(4);
        expect(PaymentMethod.Cheque).toBe(5);
        expect(PaymentMethod.TelegraphicTransfer).toBe(6);
        expect(PaymentMethod.FedWire).toBe(7);
        expect(PaymentMethod.DebitCard).toBe(8);
        expect(PaymentMethod.DirectDebit).toBe(9);
        expect(PaymentMethod.DirectCredit).toBe(10);
        expect(PaymentMethod.CreditCard).toBe(11);
        expect(PaymentMethod.ACHDebit).toBe(12);
        expect(PaymentMethod.ACHCredit).toBe(13);
        expect(PaymentMethod.BPAY).toBe(14);
        expect(PaymentMethod.HighValueClearingSystem).toBe(15);
        expect(PaymentMethod.CHIPS).toBe(16);
        expect(PaymentMethod.SWIFT).toBe(17);
        expect(PaymentMethod.CHAPS).toBe(18);
        expect(PaymentMethod.SIC).toBe(19);
        expect(PaymentMethod.EuroSIC).toBe(20);
        expect(PaymentMethod.Other).toBe(999);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentMethod.CREST,
            PaymentMethod.NSCC,
            PaymentMethod.Euroclear,
            PaymentMethod.Clearstream,
            PaymentMethod.Cheque,
            PaymentMethod.TelegraphicTransfer,
            PaymentMethod.FedWire,
            PaymentMethod.DebitCard,
            PaymentMethod.DirectDebit,
            PaymentMethod.DirectCredit,
            PaymentMethod.CreditCard,
            PaymentMethod.ACHDebit,
            PaymentMethod.ACHCredit,
            PaymentMethod.BPAY,
            PaymentMethod.HighValueClearingSystem,
            PaymentMethod.CHIPS,
            PaymentMethod.SWIFT,
            PaymentMethod.CHAPS,
            PaymentMethod.SIC,
            PaymentMethod.EuroSIC,
            PaymentMethod.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentMethod type', () => {
        const validate = (value: PaymentMethod) => {
            expect(Object.values(PaymentMethod)).toContain(value);
        };

        validate(PaymentMethod.CREST);
        validate(PaymentMethod.NSCC);
        validate(PaymentMethod.Euroclear);
        validate(PaymentMethod.Clearstream);
        validate(PaymentMethod.Cheque);
        validate(PaymentMethod.TelegraphicTransfer);
        validate(PaymentMethod.FedWire);
        validate(PaymentMethod.DebitCard);
        validate(PaymentMethod.DirectDebit);
        validate(PaymentMethod.DirectCredit);
        validate(PaymentMethod.CreditCard);
        validate(PaymentMethod.ACHDebit);
        validate(PaymentMethod.ACHCredit);
        validate(PaymentMethod.BPAY);
        validate(PaymentMethod.HighValueClearingSystem);
        validate(PaymentMethod.CHIPS);
        validate(PaymentMethod.SWIFT);
        validate(PaymentMethod.CHAPS);
        validate(PaymentMethod.SIC);
        validate(PaymentMethod.EuroSIC);
        validate(PaymentMethod.Other);
    });

    test('should be immutable', () => {
        const ref = PaymentMethod;
        expect(() => {
            ref.CREST = 10;
        }).toThrowError();
    });
});
