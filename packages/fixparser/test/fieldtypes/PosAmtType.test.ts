import { PosAmtType } from '../../src/fieldtypes/PosAmtType';

describe('PosAmtType', () => {
    test('should have the correct values', () => {
        expect(PosAmtType.CashAmount).toBe('CASH');
        expect(PosAmtType.CashResidualAmount).toBe('CRES');
        expect(PosAmtType.FinalMarkToMarketAmount).toBe('FMTM');
        expect(PosAmtType.IncrementalMarkToMarketAmount).toBe('IMTM');
        expect(PosAmtType.PremiumAmount).toBe('PREM');
        expect(PosAmtType.StartOfDayMarkToMarketAmount).toBe('SMTM');
        expect(PosAmtType.TradeVariationAmount).toBe('TVAR');
        expect(PosAmtType.ValueAdjustedAmount).toBe('VADJ');
        expect(PosAmtType.SettlementValue).toBe('SETL');
        expect(PosAmtType.InitialTradeCouponAmount).toBe('ICPN');
        expect(PosAmtType.AccruedCouponAmount).toBe('ACPN');
        expect(PosAmtType.CouponAmount).toBe('CPN');
        expect(PosAmtType.IncrementalAccruedCoupon).toBe('IACPN');
        expect(PosAmtType.CollateralizedMarkToMarket).toBe('CMTM');
        expect(PosAmtType.IncrementalCollateralizedMarkToMarket).toBe('ICMTM');
        expect(PosAmtType.CompensationAmount).toBe('DLV');
        expect(PosAmtType.TotalBankedAmount).toBe('BANK');
        expect(PosAmtType.TotalCollateralizedAmount).toBe('COLAT');
        expect(PosAmtType.LongPairedSwapNotionalValue).toBe('LSNV');
        expect(PosAmtType.ShortPairedSwapNotionalValue).toBe('SSNV');
        expect(PosAmtType.StartOfDayAccruedCoupon).toBe('SACPN');
        expect(PosAmtType.NetPresentValue).toBe('NPV');
        expect(PosAmtType.StartOfDayNetPresentValue).toBe('SNPV');
        expect(PosAmtType.NetCashFlow).toBe('NCF');
        expect(PosAmtType.PresentValueOfFees).toBe('PVFEES');
        expect(PosAmtType.PresentValueOneBasisPoints).toBe('PV01');
        expect(PosAmtType.FiveYearEquivalentNotional).toBe('5YREN');
        expect(PosAmtType.UndiscountedMarkToMarket).toBe('UMTM');
        expect(PosAmtType.MarkToModel).toBe('MTD');
        expect(PosAmtType.MarkToMarketVariance).toBe('VMTM');
        expect(PosAmtType.MarkToModelVariance).toBe('VMTD');
        expect(PosAmtType.UpfrontPayment).toBe('UPFRNT');
        expect(PosAmtType.EndVale).toBe('ENDV');
        expect(PosAmtType.OutstandingMarginLoan).toBe('MGNLN');
        expect(PosAmtType.LoanValue).toBe('LNVL');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosAmtType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosAmtType.CashAmount,
            PosAmtType.CashResidualAmount,
            PosAmtType.FinalMarkToMarketAmount,
            PosAmtType.IncrementalMarkToMarketAmount,
            PosAmtType.PremiumAmount,
            PosAmtType.StartOfDayMarkToMarketAmount,
            PosAmtType.TradeVariationAmount,
            PosAmtType.ValueAdjustedAmount,
            PosAmtType.SettlementValue,
            PosAmtType.InitialTradeCouponAmount,
            PosAmtType.AccruedCouponAmount,
            PosAmtType.CouponAmount,
            PosAmtType.IncrementalAccruedCoupon,
            PosAmtType.CollateralizedMarkToMarket,
            PosAmtType.IncrementalCollateralizedMarkToMarket,
            PosAmtType.CompensationAmount,
            PosAmtType.TotalBankedAmount,
            PosAmtType.TotalCollateralizedAmount,
            PosAmtType.LongPairedSwapNotionalValue,
            PosAmtType.ShortPairedSwapNotionalValue,
            PosAmtType.StartOfDayAccruedCoupon,
            PosAmtType.NetPresentValue,
            PosAmtType.StartOfDayNetPresentValue,
            PosAmtType.NetCashFlow,
            PosAmtType.PresentValueOfFees,
            PosAmtType.PresentValueOneBasisPoints,
            PosAmtType.FiveYearEquivalentNotional,
            PosAmtType.UndiscountedMarkToMarket,
            PosAmtType.MarkToModel,
            PosAmtType.MarkToMarketVariance,
            PosAmtType.MarkToModelVariance,
            PosAmtType.UpfrontPayment,
            PosAmtType.EndVale,
            PosAmtType.OutstandingMarginLoan,
            PosAmtType.LoanValue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PosAmtType type', () => {
        const validate = (value: PosAmtType) => {
            expect(Object.values(PosAmtType)).toContain(value);
        };

        validate(PosAmtType.CashAmount);
        validate(PosAmtType.CashResidualAmount);
        validate(PosAmtType.FinalMarkToMarketAmount);
        validate(PosAmtType.IncrementalMarkToMarketAmount);
        validate(PosAmtType.PremiumAmount);
        validate(PosAmtType.StartOfDayMarkToMarketAmount);
        validate(PosAmtType.TradeVariationAmount);
        validate(PosAmtType.ValueAdjustedAmount);
        validate(PosAmtType.SettlementValue);
        validate(PosAmtType.InitialTradeCouponAmount);
        validate(PosAmtType.AccruedCouponAmount);
        validate(PosAmtType.CouponAmount);
        validate(PosAmtType.IncrementalAccruedCoupon);
        validate(PosAmtType.CollateralizedMarkToMarket);
        validate(PosAmtType.IncrementalCollateralizedMarkToMarket);
        validate(PosAmtType.CompensationAmount);
        validate(PosAmtType.TotalBankedAmount);
        validate(PosAmtType.TotalCollateralizedAmount);
        validate(PosAmtType.LongPairedSwapNotionalValue);
        validate(PosAmtType.ShortPairedSwapNotionalValue);
        validate(PosAmtType.StartOfDayAccruedCoupon);
        validate(PosAmtType.NetPresentValue);
        validate(PosAmtType.StartOfDayNetPresentValue);
        validate(PosAmtType.NetCashFlow);
        validate(PosAmtType.PresentValueOfFees);
        validate(PosAmtType.PresentValueOneBasisPoints);
        validate(PosAmtType.FiveYearEquivalentNotional);
        validate(PosAmtType.UndiscountedMarkToMarket);
        validate(PosAmtType.MarkToModel);
        validate(PosAmtType.MarkToMarketVariance);
        validate(PosAmtType.MarkToModelVariance);
        validate(PosAmtType.UpfrontPayment);
        validate(PosAmtType.EndVale);
        validate(PosAmtType.OutstandingMarginLoan);
        validate(PosAmtType.LoanValue);
    });

    test('should be immutable', () => {
        const ref = PosAmtType;
        expect(() => {
            ref.CashAmount = 10;
        }).toThrowError();
    });
});
