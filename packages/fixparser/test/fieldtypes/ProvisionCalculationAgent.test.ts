import { ProvisionCalculationAgent } from '../../src/fieldtypes/ProvisionCalculationAgent';

describe('ProvisionCalculationAgent', () => {
    test('should have the correct values', () => {
        expect(ProvisionCalculationAgent.ExercisingParty).toBe(0);
        expect(ProvisionCalculationAgent.NonExercisingParty).toBe(1);
        expect(ProvisionCalculationAgent.MasterAgreeent).toBe(2);
        expect(ProvisionCalculationAgent.Supplement).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionCalculationAgent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionCalculationAgent.ExercisingParty,
            ProvisionCalculationAgent.NonExercisingParty,
            ProvisionCalculationAgent.MasterAgreeent,
            ProvisionCalculationAgent.Supplement,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionCalculationAgent type', () => {
        const validate = (value: ProvisionCalculationAgent) => {
            expect(Object.values(ProvisionCalculationAgent)).toContain(value);
        };

        validate(ProvisionCalculationAgent.ExercisingParty);
        validate(ProvisionCalculationAgent.NonExercisingParty);
        validate(ProvisionCalculationAgent.MasterAgreeent);
        validate(ProvisionCalculationAgent.Supplement);
    });

    test('should be immutable', () => {
        const ref = ProvisionCalculationAgent;
        expect(() => {
            ref.ExercisingParty = 10;
        }).toThrowError();
    });
});
