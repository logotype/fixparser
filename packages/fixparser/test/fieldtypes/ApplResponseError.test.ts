import { ApplResponseError } from '../../src/fieldtypes/ApplResponseError';

describe('ApplResponseError', () => {
    test('should have the correct values', () => {
        expect(ApplResponseError.ApplicationDoesNotExist).toBe(0);
        expect(ApplResponseError.MessagesRequestedAreNotAvailable).toBe(1);
        expect(ApplResponseError.UserNotAuthorizedForApplication).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplResponseError.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplResponseError.ApplicationDoesNotExist,
            ApplResponseError.MessagesRequestedAreNotAvailable,
            ApplResponseError.UserNotAuthorizedForApplication,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplResponseError type', () => {
        const validate = (value: ApplResponseError) => {
            expect(Object.values(ApplResponseError)).toContain(value);
        };

        validate(ApplResponseError.ApplicationDoesNotExist);
        validate(ApplResponseError.MessagesRequestedAreNotAvailable);
        validate(ApplResponseError.UserNotAuthorizedForApplication);
    });

    test('should be immutable', () => {
        const ref = ApplResponseError;
        expect(() => {
            ref.ApplicationDoesNotExist = 10;
        }).toThrowError();
    });
});
