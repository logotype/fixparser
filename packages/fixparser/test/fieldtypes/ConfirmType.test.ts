import { ConfirmType } from '../../src/fieldtypes/ConfirmType';

describe('ConfirmType', () => {
    test('should have the correct values', () => {
        expect(ConfirmType.Status).toBe(1);
        expect(ConfirmType.Confirmation).toBe(2);
        expect(ConfirmType.ConfirmationRequestRejected).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ConfirmType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ConfirmType.Status,
            ConfirmType.Confirmation,
            ConfirmType.ConfirmationRequestRejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ConfirmType type', () => {
        const validate = (value: ConfirmType) => {
            expect(Object.values(ConfirmType)).toContain(value);
        };

        validate(ConfirmType.Status);
        validate(ConfirmType.Confirmation);
        validate(ConfirmType.ConfirmationRequestRejected);
    });

    test('should be immutable', () => {
        const ref = ConfirmType;
        expect(() => {
            ref.Status = 10;
        }).toThrowError();
    });
});
