import { EntitlementType } from '../../src/fieldtypes/EntitlementType';

describe('EntitlementType', () => {
    test('should have the correct values', () => {
        expect(EntitlementType.Trade).toBe(0);
        expect(EntitlementType.MakeMarkets).toBe(1);
        expect(EntitlementType.HoldPositions).toBe(2);
        expect(EntitlementType.PerformGiveUps).toBe(3);
        expect(EntitlementType.SubmitIOIs).toBe(4);
        expect(EntitlementType.SubscribeMarketData).toBe(5);
        expect(EntitlementType.ShortWithPreBorrow).toBe(6);
        expect(EntitlementType.SubmitQuoteRequests).toBe(7);
        expect(EntitlementType.RespondToQuoteRequests).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EntitlementType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EntitlementType.Trade,
            EntitlementType.MakeMarkets,
            EntitlementType.HoldPositions,
            EntitlementType.PerformGiveUps,
            EntitlementType.SubmitIOIs,
            EntitlementType.SubscribeMarketData,
            EntitlementType.ShortWithPreBorrow,
            EntitlementType.SubmitQuoteRequests,
            EntitlementType.RespondToQuoteRequests,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EntitlementType type', () => {
        const validate = (value: EntitlementType) => {
            expect(Object.values(EntitlementType)).toContain(value);
        };

        validate(EntitlementType.Trade);
        validate(EntitlementType.MakeMarkets);
        validate(EntitlementType.HoldPositions);
        validate(EntitlementType.PerformGiveUps);
        validate(EntitlementType.SubmitIOIs);
        validate(EntitlementType.SubscribeMarketData);
        validate(EntitlementType.ShortWithPreBorrow);
        validate(EntitlementType.SubmitQuoteRequests);
        validate(EntitlementType.RespondToQuoteRequests);
    });

    test('should be immutable', () => {
        const ref = EntitlementType;
        expect(() => {
            ref.Trade = 10;
        }).toThrowError();
    });
});
