import { NetGrossInd } from '../../src/fieldtypes/NetGrossInd';

describe('NetGrossInd', () => {
    test('should have the correct values', () => {
        expect(NetGrossInd.Net).toBe(1);
        expect(NetGrossInd.Gross).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NetGrossInd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NetGrossInd.Net, NetGrossInd.Gross];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NetGrossInd type', () => {
        const validate = (value: NetGrossInd) => {
            expect(Object.values(NetGrossInd)).toContain(value);
        };

        validate(NetGrossInd.Net);
        validate(NetGrossInd.Gross);
    });

    test('should be immutable', () => {
        const ref = NetGrossInd;
        expect(() => {
            ref.Net = 10;
        }).toThrowError();
    });
});
