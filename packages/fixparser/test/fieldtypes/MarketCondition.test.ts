import { MarketCondition } from '../../src/fieldtypes/MarketCondition';

describe('MarketCondition', () => {
    test('should have the correct values', () => {
        expect(MarketCondition.Normal).toBe(0);
        expect(MarketCondition.Stressed).toBe(1);
        expect(MarketCondition.Exceptional).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MarketCondition.Normal, MarketCondition.Stressed, MarketCondition.Exceptional];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketCondition type', () => {
        const validate = (value: MarketCondition) => {
            expect(Object.values(MarketCondition)).toContain(value);
        };

        validate(MarketCondition.Normal);
        validate(MarketCondition.Stressed);
        validate(MarketCondition.Exceptional);
    });

    test('should be immutable', () => {
        const ref = MarketCondition;
        expect(() => {
            ref.Normal = 10;
        }).toThrowError();
    });
});
