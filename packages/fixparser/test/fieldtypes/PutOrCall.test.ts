import { PutOrCall } from '../../src/fieldtypes/PutOrCall';

describe('PutOrCall', () => {
    test('should have the correct values', () => {
        expect(PutOrCall.Put).toBe(0);
        expect(PutOrCall.Call).toBe(1);
        expect(PutOrCall.Other).toBe(2);
        expect(PutOrCall.Chooser).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PutOrCall.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PutOrCall.Put, PutOrCall.Call, PutOrCall.Other, PutOrCall.Chooser];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PutOrCall type', () => {
        const validate = (value: PutOrCall) => {
            expect(Object.values(PutOrCall)).toContain(value);
        };

        validate(PutOrCall.Put);
        validate(PutOrCall.Call);
        validate(PutOrCall.Other);
        validate(PutOrCall.Chooser);
    });

    test('should be immutable', () => {
        const ref = PutOrCall;
        expect(() => {
            ref.Put = 10;
        }).toThrowError();
    });
});
