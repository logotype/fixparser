import { ImpliedMarketIndicator } from '../../src/fieldtypes/ImpliedMarketIndicator';

describe('ImpliedMarketIndicator', () => {
    test('should have the correct values', () => {
        expect(ImpliedMarketIndicator.NotImplied).toBe(0);
        expect(ImpliedMarketIndicator.ImpliedIn).toBe(1);
        expect(ImpliedMarketIndicator.ImpliedOut).toBe(2);
        expect(ImpliedMarketIndicator.BothImpliedInAndImpliedOut).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ImpliedMarketIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ImpliedMarketIndicator.NotImplied,
            ImpliedMarketIndicator.ImpliedIn,
            ImpliedMarketIndicator.ImpliedOut,
            ImpliedMarketIndicator.BothImpliedInAndImpliedOut,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ImpliedMarketIndicator type', () => {
        const validate = (value: ImpliedMarketIndicator) => {
            expect(Object.values(ImpliedMarketIndicator)).toContain(value);
        };

        validate(ImpliedMarketIndicator.NotImplied);
        validate(ImpliedMarketIndicator.ImpliedIn);
        validate(ImpliedMarketIndicator.ImpliedOut);
        validate(ImpliedMarketIndicator.BothImpliedInAndImpliedOut);
    });

    test('should be immutable', () => {
        const ref = ImpliedMarketIndicator;
        expect(() => {
            ref.NotImplied = 10;
        }).toThrowError();
    });
});
