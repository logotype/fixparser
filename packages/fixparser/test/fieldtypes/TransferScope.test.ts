import { TransferScope } from '../../src/fieldtypes/TransferScope';

describe('TransferScope', () => {
    test('should have the correct values', () => {
        expect(TransferScope.InterFirmTransfer).toBe(0);
        expect(TransferScope.IntraFirmTransfer).toBe(1);
        expect(TransferScope.CMTA).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TransferScope.InterFirmTransfer, TransferScope.IntraFirmTransfer, TransferScope.CMTA];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferScope type', () => {
        const validate = (value: TransferScope) => {
            expect(Object.values(TransferScope)).toContain(value);
        };

        validate(TransferScope.InterFirmTransfer);
        validate(TransferScope.IntraFirmTransfer);
        validate(TransferScope.CMTA);
    });

    test('should be immutable', () => {
        const ref = TransferScope;
        expect(() => {
            ref.InterFirmTransfer = 10;
        }).toThrowError();
    });
});
