import { TradedFlatSwitch } from '../../src/fieldtypes/TradedFlatSwitch';

describe('TradedFlatSwitch', () => {
    test('should have the correct values', () => {
        expect(TradedFlatSwitch.NotTradedFlat).toBe('N');
        expect(TradedFlatSwitch.TradedFlat).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradedFlatSwitch.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradedFlatSwitch.NotTradedFlat, TradedFlatSwitch.TradedFlat];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TradedFlatSwitch type', () => {
        const validate = (value: TradedFlatSwitch) => {
            expect(Object.values(TradedFlatSwitch)).toContain(value);
        };

        validate(TradedFlatSwitch.NotTradedFlat);
        validate(TradedFlatSwitch.TradedFlat);
    });

    test('should be immutable', () => {
        const ref = TradedFlatSwitch;
        expect(() => {
            ref.NotTradedFlat = 10;
        }).toThrowError();
    });
});
