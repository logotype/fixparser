import { PaymentStreamSettlLevel } from '../../src/fieldtypes/PaymentStreamSettlLevel';

describe('PaymentStreamSettlLevel', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamSettlLevel.Average).toBe(0);
        expect(PaymentStreamSettlLevel.Maximum).toBe(1);
        expect(PaymentStreamSettlLevel.Minimum).toBe(2);
        expect(PaymentStreamSettlLevel.Cumulative).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamSettlLevel.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamSettlLevel.Average,
            PaymentStreamSettlLevel.Maximum,
            PaymentStreamSettlLevel.Minimum,
            PaymentStreamSettlLevel.Cumulative,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamSettlLevel type', () => {
        const validate = (value: PaymentStreamSettlLevel) => {
            expect(Object.values(PaymentStreamSettlLevel)).toContain(value);
        };

        validate(PaymentStreamSettlLevel.Average);
        validate(PaymentStreamSettlLevel.Maximum);
        validate(PaymentStreamSettlLevel.Minimum);
        validate(PaymentStreamSettlLevel.Cumulative);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamSettlLevel;
        expect(() => {
            ref.Average = 10;
        }).toThrowError();
    });
});
