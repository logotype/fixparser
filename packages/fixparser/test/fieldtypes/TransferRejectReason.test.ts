import { TransferRejectReason } from '../../src/fieldtypes/TransferRejectReason';

describe('TransferRejectReason', () => {
    test('should have the correct values', () => {
        expect(TransferRejectReason.Success).toBe(0);
        expect(TransferRejectReason.InvalidParty).toBe(1);
        expect(TransferRejectReason.UnknownInstrument).toBe(2);
        expect(TransferRejectReason.UnauthorizedToSubmitXfer).toBe(3);
        expect(TransferRejectReason.UnknownPosition).toBe(4);
        expect(TransferRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TransferRejectReason.Success,
            TransferRejectReason.InvalidParty,
            TransferRejectReason.UnknownInstrument,
            TransferRejectReason.UnauthorizedToSubmitXfer,
            TransferRejectReason.UnknownPosition,
            TransferRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferRejectReason type', () => {
        const validate = (value: TransferRejectReason) => {
            expect(Object.values(TransferRejectReason)).toContain(value);
        };

        validate(TransferRejectReason.Success);
        validate(TransferRejectReason.InvalidParty);
        validate(TransferRejectReason.UnknownInstrument);
        validate(TransferRejectReason.UnauthorizedToSubmitXfer);
        validate(TransferRejectReason.UnknownPosition);
        validate(TransferRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = TransferRejectReason;
        expect(() => {
            ref.Success = 10;
        }).toThrowError();
    });
});
