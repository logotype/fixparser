import { ConfirmStatus } from '../../src/fieldtypes/ConfirmStatus';

describe('ConfirmStatus', () => {
    test('should have the correct values', () => {
        expect(ConfirmStatus.Received).toBe(1);
        expect(ConfirmStatus.MismatchedAccount).toBe(2);
        expect(ConfirmStatus.MissingSettlementInstructions).toBe(3);
        expect(ConfirmStatus.Confirmed).toBe(4);
        expect(ConfirmStatus.RequestRejected).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ConfirmStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ConfirmStatus.Received,
            ConfirmStatus.MismatchedAccount,
            ConfirmStatus.MissingSettlementInstructions,
            ConfirmStatus.Confirmed,
            ConfirmStatus.RequestRejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ConfirmStatus type', () => {
        const validate = (value: ConfirmStatus) => {
            expect(Object.values(ConfirmStatus)).toContain(value);
        };

        validate(ConfirmStatus.Received);
        validate(ConfirmStatus.MismatchedAccount);
        validate(ConfirmStatus.MissingSettlementInstructions);
        validate(ConfirmStatus.Confirmed);
        validate(ConfirmStatus.RequestRejected);
    });

    test('should be immutable', () => {
        const ref = ConfirmStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
