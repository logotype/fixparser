import { PartyActionResponse } from '../../src/fieldtypes/PartyActionResponse';

describe('PartyActionResponse', () => {
    test('should have the correct values', () => {
        expect(PartyActionResponse.Accepted).toBe(0);
        expect(PartyActionResponse.Completed).toBe(1);
        expect(PartyActionResponse.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyActionResponse.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyActionResponse.Accepted,
            PartyActionResponse.Completed,
            PartyActionResponse.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyActionResponse type', () => {
        const validate = (value: PartyActionResponse) => {
            expect(Object.values(PartyActionResponse)).toContain(value);
        };

        validate(PartyActionResponse.Accepted);
        validate(PartyActionResponse.Completed);
        validate(PartyActionResponse.Rejected);
    });

    test('should be immutable', () => {
        const ref = PartyActionResponse;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
