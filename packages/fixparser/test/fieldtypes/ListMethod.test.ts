import { ListMethod } from '../../src/fieldtypes/ListMethod';

describe('ListMethod', () => {
    test('should have the correct values', () => {
        expect(ListMethod.PreListedOnly).toBe(0);
        expect(ListMethod.UserRequested).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ListMethod.PreListedOnly, ListMethod.UserRequested];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ListMethod type', () => {
        const validate = (value: ListMethod) => {
            expect(Object.values(ListMethod)).toContain(value);
        };

        validate(ListMethod.PreListedOnly);
        validate(ListMethod.UserRequested);
    });

    test('should be immutable', () => {
        const ref = ListMethod;
        expect(() => {
            ref.PreListedOnly = 10;
        }).toThrowError();
    });
});
