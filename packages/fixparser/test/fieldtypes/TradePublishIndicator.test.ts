import { TradePublishIndicator } from '../../src/fieldtypes/TradePublishIndicator';

describe('TradePublishIndicator', () => {
    test('should have the correct values', () => {
        expect(TradePublishIndicator.DoNotPublishTrade).toBe(0);
        expect(TradePublishIndicator.PublishTrade).toBe(1);
        expect(TradePublishIndicator.DeferredPublication).toBe(2);
        expect(TradePublishIndicator.Published).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradePublishIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradePublishIndicator.DoNotPublishTrade,
            TradePublishIndicator.PublishTrade,
            TradePublishIndicator.DeferredPublication,
            TradePublishIndicator.Published,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradePublishIndicator type', () => {
        const validate = (value: TradePublishIndicator) => {
            expect(Object.values(TradePublishIndicator)).toContain(value);
        };

        validate(TradePublishIndicator.DoNotPublishTrade);
        validate(TradePublishIndicator.PublishTrade);
        validate(TradePublishIndicator.DeferredPublication);
        validate(TradePublishIndicator.Published);
    });

    test('should be immutable', () => {
        const ref = TradePublishIndicator;
        expect(() => {
            ref.DoNotPublishTrade = 10;
        }).toThrowError();
    });
});
