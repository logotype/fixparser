import { MarginAmtType } from '../../src/fieldtypes/MarginAmtType';

describe('MarginAmtType', () => {
    test('should have the correct values', () => {
        expect(MarginAmtType.AdditionalMargin).toBe(1);
        expect(MarginAmtType.AdjustedMargin).toBe(2);
        expect(MarginAmtType.UnadjustedMargin).toBe(3);
        expect(MarginAmtType.BinaryAddOnAmount).toBe(4);
        expect(MarginAmtType.CashBalanceAmount).toBe(5);
        expect(MarginAmtType.ConcentrationMargin).toBe(6);
        expect(MarginAmtType.CoreMargin).toBe(7);
        expect(MarginAmtType.DeliveryMargin).toBe(8);
        expect(MarginAmtType.DiscretionaryMargin).toBe(9);
        expect(MarginAmtType.FuturesSpreadMargin).toBe(10);
        expect(MarginAmtType.InitialMargin).toBe(11);
        expect(MarginAmtType.LiquidatingMargin).toBe(12);
        expect(MarginAmtType.MarginCallAmount).toBe(13);
        expect(MarginAmtType.MarginDeficitAmount).toBe(14);
        expect(MarginAmtType.MarginExcessAmount).toBe(15);
        expect(MarginAmtType.OptionPremiumAmount).toBe(16);
        expect(MarginAmtType.PremiumMargin).toBe(17);
        expect(MarginAmtType.ReserveMargin).toBe(18);
        expect(MarginAmtType.SecurityCollateralAmount).toBe(19);
        expect(MarginAmtType.StressTestAddOnAmount).toBe(20);
        expect(MarginAmtType.SuperMargin).toBe(21);
        expect(MarginAmtType.TotalMargin).toBe(22);
        expect(MarginAmtType.VariationMargin).toBe(23);
        expect(MarginAmtType.SecondaryVariationMargin).toBe(24);
        expect(MarginAmtType.RolledUpMarginDeficit).toBe(25);
        expect(MarginAmtType.SpreadResponseMargin).toBe(26);
        expect(MarginAmtType.SystemicRiskMargin).toBe(27);
        expect(MarginAmtType.CurveRiskMargin).toBe(28);
        expect(MarginAmtType.IndexSpreadRiskMargin).toBe(29);
        expect(MarginAmtType.SectorRiskMargin).toBe(30);
        expect(MarginAmtType.JumpToDefaultRiskMargin).toBe(31);
        expect(MarginAmtType.BasisRiskMargin).toBe(32);
        expect(MarginAmtType.InterestRateRiskMargin).toBe(33);
        expect(MarginAmtType.JumpToHealthRiskMargin).toBe(34);
        expect(MarginAmtType.OtherRiskMargin).toBe(35);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarginAmtType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarginAmtType.AdditionalMargin,
            MarginAmtType.AdjustedMargin,
            MarginAmtType.UnadjustedMargin,
            MarginAmtType.BinaryAddOnAmount,
            MarginAmtType.CashBalanceAmount,
            MarginAmtType.ConcentrationMargin,
            MarginAmtType.CoreMargin,
            MarginAmtType.DeliveryMargin,
            MarginAmtType.DiscretionaryMargin,
            MarginAmtType.FuturesSpreadMargin,
            MarginAmtType.InitialMargin,
            MarginAmtType.LiquidatingMargin,
            MarginAmtType.MarginCallAmount,
            MarginAmtType.MarginDeficitAmount,
            MarginAmtType.MarginExcessAmount,
            MarginAmtType.OptionPremiumAmount,
            MarginAmtType.PremiumMargin,
            MarginAmtType.ReserveMargin,
            MarginAmtType.SecurityCollateralAmount,
            MarginAmtType.StressTestAddOnAmount,
            MarginAmtType.SuperMargin,
            MarginAmtType.TotalMargin,
            MarginAmtType.VariationMargin,
            MarginAmtType.SecondaryVariationMargin,
            MarginAmtType.RolledUpMarginDeficit,
            MarginAmtType.SpreadResponseMargin,
            MarginAmtType.SystemicRiskMargin,
            MarginAmtType.CurveRiskMargin,
            MarginAmtType.IndexSpreadRiskMargin,
            MarginAmtType.SectorRiskMargin,
            MarginAmtType.JumpToDefaultRiskMargin,
            MarginAmtType.BasisRiskMargin,
            MarginAmtType.InterestRateRiskMargin,
            MarginAmtType.JumpToHealthRiskMargin,
            MarginAmtType.OtherRiskMargin,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarginAmtType type', () => {
        const validate = (value: MarginAmtType) => {
            expect(Object.values(MarginAmtType)).toContain(value);
        };

        validate(MarginAmtType.AdditionalMargin);
        validate(MarginAmtType.AdjustedMargin);
        validate(MarginAmtType.UnadjustedMargin);
        validate(MarginAmtType.BinaryAddOnAmount);
        validate(MarginAmtType.CashBalanceAmount);
        validate(MarginAmtType.ConcentrationMargin);
        validate(MarginAmtType.CoreMargin);
        validate(MarginAmtType.DeliveryMargin);
        validate(MarginAmtType.DiscretionaryMargin);
        validate(MarginAmtType.FuturesSpreadMargin);
        validate(MarginAmtType.InitialMargin);
        validate(MarginAmtType.LiquidatingMargin);
        validate(MarginAmtType.MarginCallAmount);
        validate(MarginAmtType.MarginDeficitAmount);
        validate(MarginAmtType.MarginExcessAmount);
        validate(MarginAmtType.OptionPremiumAmount);
        validate(MarginAmtType.PremiumMargin);
        validate(MarginAmtType.ReserveMargin);
        validate(MarginAmtType.SecurityCollateralAmount);
        validate(MarginAmtType.StressTestAddOnAmount);
        validate(MarginAmtType.SuperMargin);
        validate(MarginAmtType.TotalMargin);
        validate(MarginAmtType.VariationMargin);
        validate(MarginAmtType.SecondaryVariationMargin);
        validate(MarginAmtType.RolledUpMarginDeficit);
        validate(MarginAmtType.SpreadResponseMargin);
        validate(MarginAmtType.SystemicRiskMargin);
        validate(MarginAmtType.CurveRiskMargin);
        validate(MarginAmtType.IndexSpreadRiskMargin);
        validate(MarginAmtType.SectorRiskMargin);
        validate(MarginAmtType.JumpToDefaultRiskMargin);
        validate(MarginAmtType.BasisRiskMargin);
        validate(MarginAmtType.InterestRateRiskMargin);
        validate(MarginAmtType.JumpToHealthRiskMargin);
        validate(MarginAmtType.OtherRiskMargin);
    });

    test('should be immutable', () => {
        const ref = MarginAmtType;
        expect(() => {
            ref.AdditionalMargin = 10;
        }).toThrowError();
    });
});
