import { MDStatisticScope } from '../../src/fieldtypes/MDStatisticScope';

describe('MDStatisticScope', () => {
    test('should have the correct values', () => {
        expect(MDStatisticScope.BidPrices).toBe(1);
        expect(MDStatisticScope.OfferPrices).toBe(2);
        expect(MDStatisticScope.BidDepth).toBe(3);
        expect(MDStatisticScope.OfferDepth).toBe(4);
        expect(MDStatisticScope.Orders).toBe(5);
        expect(MDStatisticScope.Quotes).toBe(6);
        expect(MDStatisticScope.OrdersAndQuotes).toBe(7);
        expect(MDStatisticScope.Trades).toBe(8);
        expect(MDStatisticScope.TradePrices).toBe(9);
        expect(MDStatisticScope.AuctionPrices).toBe(10);
        expect(MDStatisticScope.OpeningPrices).toBe(11);
        expect(MDStatisticScope.ClosingPrices).toBe(12);
        expect(MDStatisticScope.SettlementPrices).toBe(13);
        expect(MDStatisticScope.UnderlyingPrices).toBe(14);
        expect(MDStatisticScope.OpenInterest).toBe(15);
        expect(MDStatisticScope.IndexValues).toBe(16);
        expect(MDStatisticScope.MarginRates).toBe(17);
        expect(MDStatisticScope.Outages).toBe(18);
        expect(MDStatisticScope.ScheduledAuctions).toBe(19);
        expect(MDStatisticScope.ReferencePrices).toBe(20);
        expect(MDStatisticScope.TradeValue).toBe(21);
        expect(MDStatisticScope.MarketDataFeeItems).toBe(22);
        expect(MDStatisticScope.Rebates).toBe(23);
        expect(MDStatisticScope.Discounts).toBe(24);
        expect(MDStatisticScope.Payments).toBe(25);
        expect(MDStatisticScope.Taxes).toBe(26);
        expect(MDStatisticScope.Levies).toBe(27);
        expect(MDStatisticScope.Benefits).toBe(28);
        expect(MDStatisticScope.Fees).toBe(29);
        expect(MDStatisticScope.OrdersRFQs).toBe(30);
        expect(MDStatisticScope.MarketMakers).toBe(31);
        expect(MDStatisticScope.TradingInterruptions).toBe(32);
        expect(MDStatisticScope.TradingSuspensions).toBe(33);
        expect(MDStatisticScope.NoQuotes).toBe(34);
        expect(MDStatisticScope.RequestForQuotes).toBe(35);
        expect(MDStatisticScope.TradeVolume).toBe(36);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticScope.BidPrices,
            MDStatisticScope.OfferPrices,
            MDStatisticScope.BidDepth,
            MDStatisticScope.OfferDepth,
            MDStatisticScope.Orders,
            MDStatisticScope.Quotes,
            MDStatisticScope.OrdersAndQuotes,
            MDStatisticScope.Trades,
            MDStatisticScope.TradePrices,
            MDStatisticScope.AuctionPrices,
            MDStatisticScope.OpeningPrices,
            MDStatisticScope.ClosingPrices,
            MDStatisticScope.SettlementPrices,
            MDStatisticScope.UnderlyingPrices,
            MDStatisticScope.OpenInterest,
            MDStatisticScope.IndexValues,
            MDStatisticScope.MarginRates,
            MDStatisticScope.Outages,
            MDStatisticScope.ScheduledAuctions,
            MDStatisticScope.ReferencePrices,
            MDStatisticScope.TradeValue,
            MDStatisticScope.MarketDataFeeItems,
            MDStatisticScope.Rebates,
            MDStatisticScope.Discounts,
            MDStatisticScope.Payments,
            MDStatisticScope.Taxes,
            MDStatisticScope.Levies,
            MDStatisticScope.Benefits,
            MDStatisticScope.Fees,
            MDStatisticScope.OrdersRFQs,
            MDStatisticScope.MarketMakers,
            MDStatisticScope.TradingInterruptions,
            MDStatisticScope.TradingSuspensions,
            MDStatisticScope.NoQuotes,
            MDStatisticScope.RequestForQuotes,
            MDStatisticScope.TradeVolume,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticScope type', () => {
        const validate = (value: MDStatisticScope) => {
            expect(Object.values(MDStatisticScope)).toContain(value);
        };

        validate(MDStatisticScope.BidPrices);
        validate(MDStatisticScope.OfferPrices);
        validate(MDStatisticScope.BidDepth);
        validate(MDStatisticScope.OfferDepth);
        validate(MDStatisticScope.Orders);
        validate(MDStatisticScope.Quotes);
        validate(MDStatisticScope.OrdersAndQuotes);
        validate(MDStatisticScope.Trades);
        validate(MDStatisticScope.TradePrices);
        validate(MDStatisticScope.AuctionPrices);
        validate(MDStatisticScope.OpeningPrices);
        validate(MDStatisticScope.ClosingPrices);
        validate(MDStatisticScope.SettlementPrices);
        validate(MDStatisticScope.UnderlyingPrices);
        validate(MDStatisticScope.OpenInterest);
        validate(MDStatisticScope.IndexValues);
        validate(MDStatisticScope.MarginRates);
        validate(MDStatisticScope.Outages);
        validate(MDStatisticScope.ScheduledAuctions);
        validate(MDStatisticScope.ReferencePrices);
        validate(MDStatisticScope.TradeValue);
        validate(MDStatisticScope.MarketDataFeeItems);
        validate(MDStatisticScope.Rebates);
        validate(MDStatisticScope.Discounts);
        validate(MDStatisticScope.Payments);
        validate(MDStatisticScope.Taxes);
        validate(MDStatisticScope.Levies);
        validate(MDStatisticScope.Benefits);
        validate(MDStatisticScope.Fees);
        validate(MDStatisticScope.OrdersRFQs);
        validate(MDStatisticScope.MarketMakers);
        validate(MDStatisticScope.TradingInterruptions);
        validate(MDStatisticScope.TradingSuspensions);
        validate(MDStatisticScope.NoQuotes);
        validate(MDStatisticScope.RequestForQuotes);
        validate(MDStatisticScope.TradeVolume);
    });

    test('should be immutable', () => {
        const ref = MDStatisticScope;
        expect(() => {
            ref.BidPrices = 10;
        }).toThrowError();
    });
});
