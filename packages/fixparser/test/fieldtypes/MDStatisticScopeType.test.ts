import { MDStatisticScopeType } from '../../src/fieldtypes/MDStatisticScopeType';

describe('MDStatisticScopeType', () => {
    test('should have the correct values', () => {
        expect(MDStatisticScopeType.EntryRate).toBe(1);
        expect(MDStatisticScopeType.ModificationRate).toBe(2);
        expect(MDStatisticScopeType.CancelRate).toBe(3);
        expect(MDStatisticScopeType.DownwardMove).toBe(4);
        expect(MDStatisticScopeType.UpwardMove).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticScopeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticScopeType.EntryRate,
            MDStatisticScopeType.ModificationRate,
            MDStatisticScopeType.CancelRate,
            MDStatisticScopeType.DownwardMove,
            MDStatisticScopeType.UpwardMove,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticScopeType type', () => {
        const validate = (value: MDStatisticScopeType) => {
            expect(Object.values(MDStatisticScopeType)).toContain(value);
        };

        validate(MDStatisticScopeType.EntryRate);
        validate(MDStatisticScopeType.ModificationRate);
        validate(MDStatisticScopeType.CancelRate);
        validate(MDStatisticScopeType.DownwardMove);
        validate(MDStatisticScopeType.UpwardMove);
    });

    test('should be immutable', () => {
        const ref = MDStatisticScopeType;
        expect(() => {
            ref.EntryRate = 10;
        }).toThrowError();
    });
});
