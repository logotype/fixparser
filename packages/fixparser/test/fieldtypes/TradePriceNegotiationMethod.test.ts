import { TradePriceNegotiationMethod } from '../../src/fieldtypes/TradePriceNegotiationMethod';

describe('TradePriceNegotiationMethod', () => {
    test('should have the correct values', () => {
        expect(TradePriceNegotiationMethod.PercentPar).toBe(0);
        expect(TradePriceNegotiationMethod.DealSpread).toBe(1);
        expect(TradePriceNegotiationMethod.UpfrontPnts).toBe(2);
        expect(TradePriceNegotiationMethod.UpfrontAmt).toBe(3);
        expect(TradePriceNegotiationMethod.ParUpfrontAmt).toBe(4);
        expect(TradePriceNegotiationMethod.SpreadUpfrontAmt).toBe(5);
        expect(TradePriceNegotiationMethod.UpfrontPntsAmt).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradePriceNegotiationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradePriceNegotiationMethod.PercentPar,
            TradePriceNegotiationMethod.DealSpread,
            TradePriceNegotiationMethod.UpfrontPnts,
            TradePriceNegotiationMethod.UpfrontAmt,
            TradePriceNegotiationMethod.ParUpfrontAmt,
            TradePriceNegotiationMethod.SpreadUpfrontAmt,
            TradePriceNegotiationMethod.UpfrontPntsAmt,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradePriceNegotiationMethod type', () => {
        const validate = (value: TradePriceNegotiationMethod) => {
            expect(Object.values(TradePriceNegotiationMethod)).toContain(value);
        };

        validate(TradePriceNegotiationMethod.PercentPar);
        validate(TradePriceNegotiationMethod.DealSpread);
        validate(TradePriceNegotiationMethod.UpfrontPnts);
        validate(TradePriceNegotiationMethod.UpfrontAmt);
        validate(TradePriceNegotiationMethod.ParUpfrontAmt);
        validate(TradePriceNegotiationMethod.SpreadUpfrontAmt);
        validate(TradePriceNegotiationMethod.UpfrontPntsAmt);
    });

    test('should be immutable', () => {
        const ref = TradePriceNegotiationMethod;
        expect(() => {
            ref.PercentPar = 10;
        }).toThrowError();
    });
});
