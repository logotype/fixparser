import { PaymentStreamNegativeRateTreatment } from '../../src/fieldtypes/PaymentStreamNegativeRateTreatment';

describe('PaymentStreamNegativeRateTreatment', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamNegativeRateTreatment.ZeroInterestRateMethod).toBe(0);
        expect(PaymentStreamNegativeRateTreatment.NegativeInterestRateMethod).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamNegativeRateTreatment.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamNegativeRateTreatment.ZeroInterestRateMethod,
            PaymentStreamNegativeRateTreatment.NegativeInterestRateMethod,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamNegativeRateTreatment type', () => {
        const validate = (value: PaymentStreamNegativeRateTreatment) => {
            expect(Object.values(PaymentStreamNegativeRateTreatment)).toContain(value);
        };

        validate(PaymentStreamNegativeRateTreatment.ZeroInterestRateMethod);
        validate(PaymentStreamNegativeRateTreatment.NegativeInterestRateMethod);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamNegativeRateTreatment;
        expect(() => {
            ref.ZeroInterestRateMethod = 10;
        }).toThrowError();
    });
});
