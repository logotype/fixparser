import { TrdRegPublicationReason } from '../../src/fieldtypes/TrdRegPublicationReason';

describe('TrdRegPublicationReason', () => {
    test('should have the correct values', () => {
        expect(TrdRegPublicationReason.NoBookOrderDueToAverageSpreadPrice).toBe(0);
        expect(TrdRegPublicationReason.NoBookOrderDueToRefPrice).toBe(1);
        expect(TrdRegPublicationReason.NoBookOrderDueToOtherConditions).toBe(2);
        expect(TrdRegPublicationReason.NoPublicPriceDueToRefPrice).toBe(3);
        expect(TrdRegPublicationReason.NoPublicPriceDueToIlliquid).toBe(4);
        expect(TrdRegPublicationReason.NoPublicPriceDueToOrderSize).toBe(5);
        expect(TrdRegPublicationReason.DeferralDueToLargeInScale).toBe(6);
        expect(TrdRegPublicationReason.DeferralDueToIlliquid).toBe(7);
        expect(TrdRegPublicationReason.DeferralDueToSizeSpecific).toBe(8);
        expect(TrdRegPublicationReason.NoPublicPriceDueToLargeInScale).toBe(9);
        expect(TrdRegPublicationReason.NoPublicPriceSizeDueToOrderHidden).toBe(10);
        expect(TrdRegPublicationReason.ExemptedDueToSecuritiesFinancingTransaction).toBe(11);
        expect(TrdRegPublicationReason.ExemptedDueToESCBPolicyTransaction).toBe(12);
        expect(TrdRegPublicationReason.ExceptionDueToReportByPaper).toBe(13);
        expect(TrdRegPublicationReason.ExceptionDueToTradeExecutedWithNonReportingParty).toBe(14);
        expect(TrdRegPublicationReason.ExceptionDueToIntraFirmOrder).toBe(15);
        expect(TrdRegPublicationReason.ReportedOutsideReportingHours).toBe(16);
        expect(TrdRegPublicationReason.NoPublicPxDueToPreTradeWaiver).toBe(17);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdRegPublicationReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdRegPublicationReason.NoBookOrderDueToAverageSpreadPrice,
            TrdRegPublicationReason.NoBookOrderDueToRefPrice,
            TrdRegPublicationReason.NoBookOrderDueToOtherConditions,
            TrdRegPublicationReason.NoPublicPriceDueToRefPrice,
            TrdRegPublicationReason.NoPublicPriceDueToIlliquid,
            TrdRegPublicationReason.NoPublicPriceDueToOrderSize,
            TrdRegPublicationReason.DeferralDueToLargeInScale,
            TrdRegPublicationReason.DeferralDueToIlliquid,
            TrdRegPublicationReason.DeferralDueToSizeSpecific,
            TrdRegPublicationReason.NoPublicPriceDueToLargeInScale,
            TrdRegPublicationReason.NoPublicPriceSizeDueToOrderHidden,
            TrdRegPublicationReason.ExemptedDueToSecuritiesFinancingTransaction,
            TrdRegPublicationReason.ExemptedDueToESCBPolicyTransaction,
            TrdRegPublicationReason.ExceptionDueToReportByPaper,
            TrdRegPublicationReason.ExceptionDueToTradeExecutedWithNonReportingParty,
            TrdRegPublicationReason.ExceptionDueToIntraFirmOrder,
            TrdRegPublicationReason.ReportedOutsideReportingHours,
            TrdRegPublicationReason.NoPublicPxDueToPreTradeWaiver,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdRegPublicationReason type', () => {
        const validate = (value: TrdRegPublicationReason) => {
            expect(Object.values(TrdRegPublicationReason)).toContain(value);
        };

        validate(TrdRegPublicationReason.NoBookOrderDueToAverageSpreadPrice);
        validate(TrdRegPublicationReason.NoBookOrderDueToRefPrice);
        validate(TrdRegPublicationReason.NoBookOrderDueToOtherConditions);
        validate(TrdRegPublicationReason.NoPublicPriceDueToRefPrice);
        validate(TrdRegPublicationReason.NoPublicPriceDueToIlliquid);
        validate(TrdRegPublicationReason.NoPublicPriceDueToOrderSize);
        validate(TrdRegPublicationReason.DeferralDueToLargeInScale);
        validate(TrdRegPublicationReason.DeferralDueToIlliquid);
        validate(TrdRegPublicationReason.DeferralDueToSizeSpecific);
        validate(TrdRegPublicationReason.NoPublicPriceDueToLargeInScale);
        validate(TrdRegPublicationReason.NoPublicPriceSizeDueToOrderHidden);
        validate(TrdRegPublicationReason.ExemptedDueToSecuritiesFinancingTransaction);
        validate(TrdRegPublicationReason.ExemptedDueToESCBPolicyTransaction);
        validate(TrdRegPublicationReason.ExceptionDueToReportByPaper);
        validate(TrdRegPublicationReason.ExceptionDueToTradeExecutedWithNonReportingParty);
        validate(TrdRegPublicationReason.ExceptionDueToIntraFirmOrder);
        validate(TrdRegPublicationReason.ReportedOutsideReportingHours);
        validate(TrdRegPublicationReason.NoPublicPxDueToPreTradeWaiver);
    });

    test('should be immutable', () => {
        const ref = TrdRegPublicationReason;
        expect(() => {
            ref.NoBookOrderDueToAverageSpreadPrice = 10;
        }).toThrowError();
    });
});
