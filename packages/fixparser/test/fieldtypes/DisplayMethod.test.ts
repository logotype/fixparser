import { DisplayMethod } from '../../src/fieldtypes/DisplayMethod';

describe('DisplayMethod', () => {
    test('should have the correct values', () => {
        expect(DisplayMethod.Initial).toBe('1');
        expect(DisplayMethod.New).toBe('2');
        expect(DisplayMethod.Random).toBe('3');
        expect(DisplayMethod.Undisclosed).toBe('4');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DisplayMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DisplayMethod.Initial,
            DisplayMethod.New,
            DisplayMethod.Random,
            DisplayMethod.Undisclosed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DisplayMethod type', () => {
        const validate = (value: DisplayMethod) => {
            expect(Object.values(DisplayMethod)).toContain(value);
        };

        validate(DisplayMethod.Initial);
        validate(DisplayMethod.New);
        validate(DisplayMethod.Random);
        validate(DisplayMethod.Undisclosed);
    });

    test('should be immutable', () => {
        const ref = DisplayMethod;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
