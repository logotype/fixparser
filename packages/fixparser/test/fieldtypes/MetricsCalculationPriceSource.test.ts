import { MetricsCalculationPriceSource } from '../../src/fieldtypes/MetricsCalculationPriceSource';

describe('MetricsCalculationPriceSource', () => {
    test('should have the correct values', () => {
        expect(MetricsCalculationPriceSource.Realtime).toBe(1);
        expect(MetricsCalculationPriceSource.EndOfDay).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MetricsCalculationPriceSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MetricsCalculationPriceSource.Realtime, MetricsCalculationPriceSource.EndOfDay];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MetricsCalculationPriceSource type', () => {
        const validate = (value: MetricsCalculationPriceSource) => {
            expect(Object.values(MetricsCalculationPriceSource)).toContain(value);
        };

        validate(MetricsCalculationPriceSource.Realtime);
        validate(MetricsCalculationPriceSource.EndOfDay);
    });

    test('should be immutable', () => {
        const ref = MetricsCalculationPriceSource;
        expect(() => {
            ref.Realtime = 10;
        }).toThrowError();
    });
});
