import { ProvisionCashSettlPaymentDateType } from '../../src/fieldtypes/ProvisionCashSettlPaymentDateType';

describe('ProvisionCashSettlPaymentDateType', () => {
    test('should have the correct values', () => {
        expect(ProvisionCashSettlPaymentDateType.Unadjusted).toBe(0);
        expect(ProvisionCashSettlPaymentDateType.Adjusted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionCashSettlPaymentDateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionCashSettlPaymentDateType.Unadjusted,
            ProvisionCashSettlPaymentDateType.Adjusted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionCashSettlPaymentDateType type', () => {
        const validate = (value: ProvisionCashSettlPaymentDateType) => {
            expect(Object.values(ProvisionCashSettlPaymentDateType)).toContain(value);
        };

        validate(ProvisionCashSettlPaymentDateType.Unadjusted);
        validate(ProvisionCashSettlPaymentDateType.Adjusted);
    });

    test('should be immutable', () => {
        const ref = ProvisionCashSettlPaymentDateType;
        expect(() => {
            ref.Unadjusted = 10;
        }).toThrowError();
    });
});
