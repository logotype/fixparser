import { ExDestinationIDSource } from '../../src/fieldtypes/ExDestinationIDSource';

describe('ExDestinationIDSource', () => {
    test('should have the correct values', () => {
        expect(ExDestinationIDSource.BIC).toBe('B');
        expect(ExDestinationIDSource.GeneralIdentifier).toBe('C');
        expect(ExDestinationIDSource.Proprietary).toBe('D');
        expect(ExDestinationIDSource.ISOCountryCode).toBe('E');
        expect(ExDestinationIDSource.MIC).toBe('G');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExDestinationIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExDestinationIDSource.BIC,
            ExDestinationIDSource.GeneralIdentifier,
            ExDestinationIDSource.Proprietary,
            ExDestinationIDSource.ISOCountryCode,
            ExDestinationIDSource.MIC,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExDestinationIDSource type', () => {
        const validate = (value: ExDestinationIDSource) => {
            expect(Object.values(ExDestinationIDSource)).toContain(value);
        };

        validate(ExDestinationIDSource.BIC);
        validate(ExDestinationIDSource.GeneralIdentifier);
        validate(ExDestinationIDSource.Proprietary);
        validate(ExDestinationIDSource.ISOCountryCode);
        validate(ExDestinationIDSource.MIC);
    });

    test('should be immutable', () => {
        const ref = ExDestinationIDSource;
        expect(() => {
            ref.BIC = 10;
        }).toThrowError();
    });
});
