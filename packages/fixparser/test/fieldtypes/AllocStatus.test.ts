import { AllocStatus } from '../../src/fieldtypes/AllocStatus';

describe('AllocStatus', () => {
    test('should have the correct values', () => {
        expect(AllocStatus.Accepted).toBe(0);
        expect(AllocStatus.BlockLevelReject).toBe(1);
        expect(AllocStatus.AccountLevelReject).toBe(2);
        expect(AllocStatus.Received).toBe(3);
        expect(AllocStatus.Incomplete).toBe(4);
        expect(AllocStatus.RejectedByIntermediary).toBe(5);
        expect(AllocStatus.AllocationPending).toBe(6);
        expect(AllocStatus.Reversed).toBe(7);
        expect(AllocStatus.CancelledByIntermediary).toBe(8);
        expect(AllocStatus.Claimed).toBe(9);
        expect(AllocStatus.Refused).toBe(10);
        expect(AllocStatus.PendingGiveUpApproval).toBe(11);
        expect(AllocStatus.Cancelled).toBe(12);
        expect(AllocStatus.PendingTakeUpApproval).toBe(13);
        expect(AllocStatus.ReversalPending).toBe(14);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocStatus.Accepted,
            AllocStatus.BlockLevelReject,
            AllocStatus.AccountLevelReject,
            AllocStatus.Received,
            AllocStatus.Incomplete,
            AllocStatus.RejectedByIntermediary,
            AllocStatus.AllocationPending,
            AllocStatus.Reversed,
            AllocStatus.CancelledByIntermediary,
            AllocStatus.Claimed,
            AllocStatus.Refused,
            AllocStatus.PendingGiveUpApproval,
            AllocStatus.Cancelled,
            AllocStatus.PendingTakeUpApproval,
            AllocStatus.ReversalPending,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocStatus type', () => {
        const validate = (value: AllocStatus) => {
            expect(Object.values(AllocStatus)).toContain(value);
        };

        validate(AllocStatus.Accepted);
        validate(AllocStatus.BlockLevelReject);
        validate(AllocStatus.AccountLevelReject);
        validate(AllocStatus.Received);
        validate(AllocStatus.Incomplete);
        validate(AllocStatus.RejectedByIntermediary);
        validate(AllocStatus.AllocationPending);
        validate(AllocStatus.Reversed);
        validate(AllocStatus.CancelledByIntermediary);
        validate(AllocStatus.Claimed);
        validate(AllocStatus.Refused);
        validate(AllocStatus.PendingGiveUpApproval);
        validate(AllocStatus.Cancelled);
        validate(AllocStatus.PendingTakeUpApproval);
        validate(AllocStatus.ReversalPending);
    });

    test('should be immutable', () => {
        const ref = AllocStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
