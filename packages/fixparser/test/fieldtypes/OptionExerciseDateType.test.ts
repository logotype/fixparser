import { OptionExerciseDateType } from '../../src/fieldtypes/OptionExerciseDateType';

describe('OptionExerciseDateType', () => {
    test('should have the correct values', () => {
        expect(OptionExerciseDateType.Unadjusted).toBe(0);
        expect(OptionExerciseDateType.Adjusted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OptionExerciseDateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OptionExerciseDateType.Unadjusted, OptionExerciseDateType.Adjusted];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OptionExerciseDateType type', () => {
        const validate = (value: OptionExerciseDateType) => {
            expect(Object.values(OptionExerciseDateType)).toContain(value);
        };

        validate(OptionExerciseDateType.Unadjusted);
        validate(OptionExerciseDateType.Adjusted);
    });

    test('should be immutable', () => {
        const ref = OptionExerciseDateType;
        expect(() => {
            ref.Unadjusted = 10;
        }).toThrowError();
    });
});
