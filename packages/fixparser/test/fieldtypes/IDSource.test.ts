import { IDSource } from '../../src/fieldtypes/IDSource';

describe('IDSource', () => {
    test('should have the correct values', () => {
        expect(IDSource.CUSIP).toBe('1');
        expect(IDSource.SEDOL).toBe('2');
        expect(IDSource.QUIK).toBe('3');
        expect(IDSource.ISINNumber).toBe('4');
        expect(IDSource.RICCode).toBe('5');
        expect(IDSource.ISOCurrencyCode).toBe('6');
        expect(IDSource.ISOCountryCode).toBe('7');
        expect(IDSource.ExchangeSymbol).toBe('8');
        expect(IDSource.ConsolidatedTapeAssociation).toBe('9');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            IDSource.CUSIP,
            IDSource.SEDOL,
            IDSource.QUIK,
            IDSource.ISINNumber,
            IDSource.RICCode,
            IDSource.ISOCurrencyCode,
            IDSource.ISOCountryCode,
            IDSource.ExchangeSymbol,
            IDSource.ConsolidatedTapeAssociation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IDSource type', () => {
        const validate = (value: IDSource) => {
            expect(Object.values(IDSource)).toContain(value);
        };

        validate(IDSource.CUSIP);
        validate(IDSource.SEDOL);
        validate(IDSource.QUIK);
        validate(IDSource.ISINNumber);
        validate(IDSource.RICCode);
        validate(IDSource.ISOCurrencyCode);
        validate(IDSource.ISOCountryCode);
        validate(IDSource.ExchangeSymbol);
        validate(IDSource.ConsolidatedTapeAssociation);
    });

    test('should be immutable', () => {
        const ref = IDSource;
        expect(() => {
            ref.CUSIP = 10;
        }).toThrowError();
    });
});
