import { StrategyParameterType } from '../../src/fieldtypes/StrategyParameterType';

describe('StrategyParameterType', () => {
    test('should have the correct values', () => {
        expect(StrategyParameterType.Int).toBe(1);
        expect(StrategyParameterType.Length).toBe(2);
        expect(StrategyParameterType.NumInGroup).toBe(3);
        expect(StrategyParameterType.SeqNum).toBe(4);
        expect(StrategyParameterType.TagNum).toBe(5);
        expect(StrategyParameterType.Float).toBe(6);
        expect(StrategyParameterType.Qty).toBe(7);
        expect(StrategyParameterType.Price).toBe(8);
        expect(StrategyParameterType.PriceOffset).toBe(9);
        expect(StrategyParameterType.Amt).toBe(10);
        expect(StrategyParameterType.Percentage).toBe(11);
        expect(StrategyParameterType.Char).toBe(12);
        expect(StrategyParameterType.Boolean).toBe(13);
        expect(StrategyParameterType.String).toBe(14);
        expect(StrategyParameterType.MultipleCharValue).toBe(15);
        expect(StrategyParameterType.Currency).toBe(16);
        expect(StrategyParameterType.Exchange).toBe(17);
        expect(StrategyParameterType.MonthYear).toBe(18);
        expect(StrategyParameterType.UTCTimestamp).toBe(19);
        expect(StrategyParameterType.UTCTimeOnly).toBe(20);
        expect(StrategyParameterType.LocalMktDate).toBe(21);
        expect(StrategyParameterType.UTCDateOnly).toBe(22);
        expect(StrategyParameterType.Data).toBe(23);
        expect(StrategyParameterType.MultipleStringValue).toBe(24);
        expect(StrategyParameterType.Country).toBe(25);
        expect(StrategyParameterType.Language).toBe(26);
        expect(StrategyParameterType.TZTimeOnly).toBe(27);
        expect(StrategyParameterType.TZTimestamp).toBe(28);
        expect(StrategyParameterType.Tenor).toBe(29);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StrategyParameterType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StrategyParameterType.Int,
            StrategyParameterType.Length,
            StrategyParameterType.NumInGroup,
            StrategyParameterType.SeqNum,
            StrategyParameterType.TagNum,
            StrategyParameterType.Float,
            StrategyParameterType.Qty,
            StrategyParameterType.Price,
            StrategyParameterType.PriceOffset,
            StrategyParameterType.Amt,
            StrategyParameterType.Percentage,
            StrategyParameterType.Char,
            StrategyParameterType.Boolean,
            StrategyParameterType.String,
            StrategyParameterType.MultipleCharValue,
            StrategyParameterType.Currency,
            StrategyParameterType.Exchange,
            StrategyParameterType.MonthYear,
            StrategyParameterType.UTCTimestamp,
            StrategyParameterType.UTCTimeOnly,
            StrategyParameterType.LocalMktDate,
            StrategyParameterType.UTCDateOnly,
            StrategyParameterType.Data,
            StrategyParameterType.MultipleStringValue,
            StrategyParameterType.Country,
            StrategyParameterType.Language,
            StrategyParameterType.TZTimeOnly,
            StrategyParameterType.TZTimestamp,
            StrategyParameterType.Tenor,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StrategyParameterType type', () => {
        const validate = (value: StrategyParameterType) => {
            expect(Object.values(StrategyParameterType)).toContain(value);
        };

        validate(StrategyParameterType.Int);
        validate(StrategyParameterType.Length);
        validate(StrategyParameterType.NumInGroup);
        validate(StrategyParameterType.SeqNum);
        validate(StrategyParameterType.TagNum);
        validate(StrategyParameterType.Float);
        validate(StrategyParameterType.Qty);
        validate(StrategyParameterType.Price);
        validate(StrategyParameterType.PriceOffset);
        validate(StrategyParameterType.Amt);
        validate(StrategyParameterType.Percentage);
        validate(StrategyParameterType.Char);
        validate(StrategyParameterType.Boolean);
        validate(StrategyParameterType.String);
        validate(StrategyParameterType.MultipleCharValue);
        validate(StrategyParameterType.Currency);
        validate(StrategyParameterType.Exchange);
        validate(StrategyParameterType.MonthYear);
        validate(StrategyParameterType.UTCTimestamp);
        validate(StrategyParameterType.UTCTimeOnly);
        validate(StrategyParameterType.LocalMktDate);
        validate(StrategyParameterType.UTCDateOnly);
        validate(StrategyParameterType.Data);
        validate(StrategyParameterType.MultipleStringValue);
        validate(StrategyParameterType.Country);
        validate(StrategyParameterType.Language);
        validate(StrategyParameterType.TZTimeOnly);
        validate(StrategyParameterType.TZTimestamp);
        validate(StrategyParameterType.Tenor);
    });

    test('should be immutable', () => {
        const ref = StrategyParameterType;
        expect(() => {
            ref.Int = 10;
        }).toThrowError();
    });
});
