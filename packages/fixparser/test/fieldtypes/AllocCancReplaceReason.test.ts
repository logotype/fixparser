import { AllocCancReplaceReason } from '../../src/fieldtypes/AllocCancReplaceReason';

describe('AllocCancReplaceReason', () => {
    test('should have the correct values', () => {
        expect(AllocCancReplaceReason.OriginalDetailsIncomplete).toBe(1);
        expect(AllocCancReplaceReason.ChangeInUnderlyingOrderDetails).toBe(2);
        expect(AllocCancReplaceReason.CancelledByGiveupFirm).toBe(3);
        expect(AllocCancReplaceReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocCancReplaceReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocCancReplaceReason.OriginalDetailsIncomplete,
            AllocCancReplaceReason.ChangeInUnderlyingOrderDetails,
            AllocCancReplaceReason.CancelledByGiveupFirm,
            AllocCancReplaceReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocCancReplaceReason type', () => {
        const validate = (value: AllocCancReplaceReason) => {
            expect(Object.values(AllocCancReplaceReason)).toContain(value);
        };

        validate(AllocCancReplaceReason.OriginalDetailsIncomplete);
        validate(AllocCancReplaceReason.ChangeInUnderlyingOrderDetails);
        validate(AllocCancReplaceReason.CancelledByGiveupFirm);
        validate(AllocCancReplaceReason.Other);
    });

    test('should be immutable', () => {
        const ref = AllocCancReplaceReason;
        expect(() => {
            ref.OriginalDetailsIncomplete = 10;
        }).toThrowError();
    });
});
