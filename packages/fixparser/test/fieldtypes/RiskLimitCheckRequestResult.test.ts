import { RiskLimitCheckRequestResult } from '../../src/fieldtypes/RiskLimitCheckRequestResult';

describe('RiskLimitCheckRequestResult', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckRequestResult.Successful).toBe(0);
        expect(RiskLimitCheckRequestResult.InvalidParty).toBe(1);
        expect(RiskLimitCheckRequestResult.ReqExceedsCreditLimit).toBe(2);
        expect(RiskLimitCheckRequestResult.ReqExceedsClipSizeLimit).toBe(3);
        expect(RiskLimitCheckRequestResult.ReqExceedsMaxNotional).toBe(4);
        expect(RiskLimitCheckRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitCheckRequestResult.Successful,
            RiskLimitCheckRequestResult.InvalidParty,
            RiskLimitCheckRequestResult.ReqExceedsCreditLimit,
            RiskLimitCheckRequestResult.ReqExceedsClipSizeLimit,
            RiskLimitCheckRequestResult.ReqExceedsMaxNotional,
            RiskLimitCheckRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckRequestResult type', () => {
        const validate = (value: RiskLimitCheckRequestResult) => {
            expect(Object.values(RiskLimitCheckRequestResult)).toContain(value);
        };

        validate(RiskLimitCheckRequestResult.Successful);
        validate(RiskLimitCheckRequestResult.InvalidParty);
        validate(RiskLimitCheckRequestResult.ReqExceedsCreditLimit);
        validate(RiskLimitCheckRequestResult.ReqExceedsClipSizeLimit);
        validate(RiskLimitCheckRequestResult.ReqExceedsMaxNotional);
        validate(RiskLimitCheckRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
