import { MDSecSizeType } from '../../src/fieldtypes/MDSecSizeType';

describe('MDSecSizeType', () => {
    test('should have the correct values', () => {
        expect(MDSecSizeType.Customer).toBe(1);
        expect(MDSecSizeType.CustomerProfessional).toBe(2);
        expect(MDSecSizeType.DoNotTradeThrough).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDSecSizeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDSecSizeType.Customer,
            MDSecSizeType.CustomerProfessional,
            MDSecSizeType.DoNotTradeThrough,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDSecSizeType type', () => {
        const validate = (value: MDSecSizeType) => {
            expect(Object.values(MDSecSizeType)).toContain(value);
        };

        validate(MDSecSizeType.Customer);
        validate(MDSecSizeType.CustomerProfessional);
        validate(MDSecSizeType.DoNotTradeThrough);
    });

    test('should be immutable', () => {
        const ref = MDSecSizeType;
        expect(() => {
            ref.Customer = 10;
        }).toThrowError();
    });
});
