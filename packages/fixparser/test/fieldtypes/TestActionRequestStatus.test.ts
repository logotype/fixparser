import { TestActionRequestStatus } from '../../src/fieldtypes/TestActionRequestStatus';

describe('TestActionRequestStatus', () => {
    test('should have the correct values', () => {
        expect(TestActionRequestStatus.Received).toBe(0);
        expect(TestActionRequestStatus.Accepted).toBe(1);
        expect(TestActionRequestStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestActionRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TestActionRequestStatus.Received,
            TestActionRequestStatus.Accepted,
            TestActionRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestActionRequestStatus type', () => {
        const validate = (value: TestActionRequestStatus) => {
            expect(Object.values(TestActionRequestStatus)).toContain(value);
        };

        validate(TestActionRequestStatus.Received);
        validate(TestActionRequestStatus.Accepted);
        validate(TestActionRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TestActionRequestStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
