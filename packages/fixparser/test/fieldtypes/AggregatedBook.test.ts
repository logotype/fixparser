import { AggregatedBook } from '../../src/fieldtypes/AggregatedBook';

describe('AggregatedBook', () => {
    test('should have the correct values', () => {
        expect(AggregatedBook.BookEntriesToBeAggregated).toBe('Y');
        expect(AggregatedBook.BookEntriesShouldNotBeAggregated).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AggregatedBook.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AggregatedBook.BookEntriesToBeAggregated,
            AggregatedBook.BookEntriesShouldNotBeAggregated,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AggregatedBook type', () => {
        const validate = (value: AggregatedBook) => {
            expect(Object.values(AggregatedBook)).toContain(value);
        };

        validate(AggregatedBook.BookEntriesToBeAggregated);
        validate(AggregatedBook.BookEntriesShouldNotBeAggregated);
    });

    test('should be immutable', () => {
        const ref = AggregatedBook;
        expect(() => {
            ref.BookEntriesToBeAggregated = 10;
        }).toThrowError();
    });
});
