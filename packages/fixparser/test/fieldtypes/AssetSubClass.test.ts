import { AssetSubClass } from '../../src/fieldtypes/AssetSubClass';

describe('AssetSubClass', () => {
    test('should have the correct values', () => {
        expect(AssetSubClass.SingleCurrency).toBe(1);
        expect(AssetSubClass.CrossCurrency).toBe(2);
        expect(AssetSubClass.Basket).toBe(3);
        expect(AssetSubClass.SingleName).toBe(4);
        expect(AssetSubClass.CreditIndex).toBe(5);
        expect(AssetSubClass.IndexTranche).toBe(6);
        expect(AssetSubClass.CreditBasket).toBe(7);
        expect(AssetSubClass.Exotic).toBe(8);
        expect(AssetSubClass.Common).toBe(9);
        expect(AssetSubClass.Preferred).toBe(10);
        expect(AssetSubClass.EquityIndex).toBe(11);
        expect(AssetSubClass.EquityBasket).toBe(12);
        expect(AssetSubClass.Metals).toBe(13);
        expect(AssetSubClass.Bullion).toBe(14);
        expect(AssetSubClass.Energy).toBe(15);
        expect(AssetSubClass.CommodityIndex).toBe(16);
        expect(AssetSubClass.Agricultural).toBe(17);
        expect(AssetSubClass.Environmental).toBe(18);
        expect(AssetSubClass.Freight).toBe(19);
        expect(AssetSubClass.Government).toBe(20);
        expect(AssetSubClass.Agency).toBe(21);
        expect(AssetSubClass.Corporate).toBe(22);
        expect(AssetSubClass.Financing).toBe(23);
        expect(AssetSubClass.MoneyMarket).toBe(24);
        expect(AssetSubClass.Mortgage).toBe(25);
        expect(AssetSubClass.Municipal).toBe(26);
        expect(AssetSubClass.MutualFund).toBe(27);
        expect(AssetSubClass.CollectiveInvestmentVehicle).toBe(28);
        expect(AssetSubClass.InvestmentProgram).toBe(29);
        expect(AssetSubClass.SpecializedAccountProgram).toBe(30);
        expect(AssetSubClass.TermLoan).toBe(31);
        expect(AssetSubClass.BridgeLoan).toBe(32);
        expect(AssetSubClass.LetterOfCredit).toBe(33);
        expect(AssetSubClass.DividendIndex).toBe(34);
        expect(AssetSubClass.StockDividend).toBe(35);
        expect(AssetSubClass.ExchangeTradedFund).toBe(36);
        expect(AssetSubClass.VolatilityIndex).toBe(37);
        expect(AssetSubClass.FXCrossRates).toBe(38);
        expect(AssetSubClass.FXEmergingMarkets).toBe(39);
        expect(AssetSubClass.FXMajors).toBe(40);
        expect(AssetSubClass.Fertilizer).toBe(41);
        expect(AssetSubClass.IndustrialProduct).toBe(42);
        expect(AssetSubClass.Inflation).toBe(43);
        expect(AssetSubClass.Paper).toBe(44);
        expect(AssetSubClass.Polypropylene).toBe(45);
        expect(AssetSubClass.OfficialEconomicStatistics).toBe(46);
        expect(AssetSubClass.OtherC10).toBe(47);
        expect(AssetSubClass.Other).toBe(48);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AssetSubClass.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AssetSubClass.SingleCurrency,
            AssetSubClass.CrossCurrency,
            AssetSubClass.Basket,
            AssetSubClass.SingleName,
            AssetSubClass.CreditIndex,
            AssetSubClass.IndexTranche,
            AssetSubClass.CreditBasket,
            AssetSubClass.Exotic,
            AssetSubClass.Common,
            AssetSubClass.Preferred,
            AssetSubClass.EquityIndex,
            AssetSubClass.EquityBasket,
            AssetSubClass.Metals,
            AssetSubClass.Bullion,
            AssetSubClass.Energy,
            AssetSubClass.CommodityIndex,
            AssetSubClass.Agricultural,
            AssetSubClass.Environmental,
            AssetSubClass.Freight,
            AssetSubClass.Government,
            AssetSubClass.Agency,
            AssetSubClass.Corporate,
            AssetSubClass.Financing,
            AssetSubClass.MoneyMarket,
            AssetSubClass.Mortgage,
            AssetSubClass.Municipal,
            AssetSubClass.MutualFund,
            AssetSubClass.CollectiveInvestmentVehicle,
            AssetSubClass.InvestmentProgram,
            AssetSubClass.SpecializedAccountProgram,
            AssetSubClass.TermLoan,
            AssetSubClass.BridgeLoan,
            AssetSubClass.LetterOfCredit,
            AssetSubClass.DividendIndex,
            AssetSubClass.StockDividend,
            AssetSubClass.ExchangeTradedFund,
            AssetSubClass.VolatilityIndex,
            AssetSubClass.FXCrossRates,
            AssetSubClass.FXEmergingMarkets,
            AssetSubClass.FXMajors,
            AssetSubClass.Fertilizer,
            AssetSubClass.IndustrialProduct,
            AssetSubClass.Inflation,
            AssetSubClass.Paper,
            AssetSubClass.Polypropylene,
            AssetSubClass.OfficialEconomicStatistics,
            AssetSubClass.OtherC10,
            AssetSubClass.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AssetSubClass type', () => {
        const validate = (value: AssetSubClass) => {
            expect(Object.values(AssetSubClass)).toContain(value);
        };

        validate(AssetSubClass.SingleCurrency);
        validate(AssetSubClass.CrossCurrency);
        validate(AssetSubClass.Basket);
        validate(AssetSubClass.SingleName);
        validate(AssetSubClass.CreditIndex);
        validate(AssetSubClass.IndexTranche);
        validate(AssetSubClass.CreditBasket);
        validate(AssetSubClass.Exotic);
        validate(AssetSubClass.Common);
        validate(AssetSubClass.Preferred);
        validate(AssetSubClass.EquityIndex);
        validate(AssetSubClass.EquityBasket);
        validate(AssetSubClass.Metals);
        validate(AssetSubClass.Bullion);
        validate(AssetSubClass.Energy);
        validate(AssetSubClass.CommodityIndex);
        validate(AssetSubClass.Agricultural);
        validate(AssetSubClass.Environmental);
        validate(AssetSubClass.Freight);
        validate(AssetSubClass.Government);
        validate(AssetSubClass.Agency);
        validate(AssetSubClass.Corporate);
        validate(AssetSubClass.Financing);
        validate(AssetSubClass.MoneyMarket);
        validate(AssetSubClass.Mortgage);
        validate(AssetSubClass.Municipal);
        validate(AssetSubClass.MutualFund);
        validate(AssetSubClass.CollectiveInvestmentVehicle);
        validate(AssetSubClass.InvestmentProgram);
        validate(AssetSubClass.SpecializedAccountProgram);
        validate(AssetSubClass.TermLoan);
        validate(AssetSubClass.BridgeLoan);
        validate(AssetSubClass.LetterOfCredit);
        validate(AssetSubClass.DividendIndex);
        validate(AssetSubClass.StockDividend);
        validate(AssetSubClass.ExchangeTradedFund);
        validate(AssetSubClass.VolatilityIndex);
        validate(AssetSubClass.FXCrossRates);
        validate(AssetSubClass.FXEmergingMarkets);
        validate(AssetSubClass.FXMajors);
        validate(AssetSubClass.Fertilizer);
        validate(AssetSubClass.IndustrialProduct);
        validate(AssetSubClass.Inflation);
        validate(AssetSubClass.Paper);
        validate(AssetSubClass.Polypropylene);
        validate(AssetSubClass.OfficialEconomicStatistics);
        validate(AssetSubClass.OtherC10);
        validate(AssetSubClass.Other);
    });

    test('should be immutable', () => {
        const ref = AssetSubClass;
        expect(() => {
            ref.SingleCurrency = 10;
        }).toThrowError();
    });
});
