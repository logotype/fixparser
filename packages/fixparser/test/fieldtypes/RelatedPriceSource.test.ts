import { RelatedPriceSource } from '../../src/fieldtypes/RelatedPriceSource';

describe('RelatedPriceSource', () => {
    test('should have the correct values', () => {
        expect(RelatedPriceSource.NBBid).toBe(1);
        expect(RelatedPriceSource.NBOffer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelatedPriceSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RelatedPriceSource.NBBid, RelatedPriceSource.NBOffer];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelatedPriceSource type', () => {
        const validate = (value: RelatedPriceSource) => {
            expect(Object.values(RelatedPriceSource)).toContain(value);
        };

        validate(RelatedPriceSource.NBBid);
        validate(RelatedPriceSource.NBOffer);
    });

    test('should be immutable', () => {
        const ref = RelatedPriceSource;
        expect(() => {
            ref.NBBid = 10;
        }).toThrowError();
    });
});
