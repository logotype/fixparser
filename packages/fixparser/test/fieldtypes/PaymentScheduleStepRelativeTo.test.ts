import { PaymentScheduleStepRelativeTo } from '../../src/fieldtypes/PaymentScheduleStepRelativeTo';

describe('PaymentScheduleStepRelativeTo', () => {
    test('should have the correct values', () => {
        expect(PaymentScheduleStepRelativeTo.Initial).toBe(0);
        expect(PaymentScheduleStepRelativeTo.Previous).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentScheduleStepRelativeTo.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentScheduleStepRelativeTo.Initial, PaymentScheduleStepRelativeTo.Previous];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentScheduleStepRelativeTo type', () => {
        const validate = (value: PaymentScheduleStepRelativeTo) => {
            expect(Object.values(PaymentScheduleStepRelativeTo)).toContain(value);
        };

        validate(PaymentScheduleStepRelativeTo.Initial);
        validate(PaymentScheduleStepRelativeTo.Previous);
    });

    test('should be immutable', () => {
        const ref = PaymentScheduleStepRelativeTo;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
