import { UnderlyingCashType } from '../../src/fieldtypes/UnderlyingCashType';

describe('UnderlyingCashType', () => {
    test('should have the correct values', () => {
        expect(UnderlyingCashType.FIXED).toBe('FIXED');
        expect(UnderlyingCashType.DIFF).toBe('DIFF');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            UnderlyingCashType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [UnderlyingCashType.FIXED, UnderlyingCashType.DIFF];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for UnderlyingCashType type', () => {
        const validate = (value: UnderlyingCashType) => {
            expect(Object.values(UnderlyingCashType)).toContain(value);
        };

        validate(UnderlyingCashType.FIXED);
        validate(UnderlyingCashType.DIFF);
    });

    test('should be immutable', () => {
        const ref = UnderlyingCashType;
        expect(() => {
            ref.FIXED = 10;
        }).toThrowError();
    });
});
