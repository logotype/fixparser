import { CashSettlQuoteMethod } from '../../src/fieldtypes/CashSettlQuoteMethod';

describe('CashSettlQuoteMethod', () => {
    test('should have the correct values', () => {
        expect(CashSettlQuoteMethod.Bid).toBe(0);
        expect(CashSettlQuoteMethod.Mid).toBe(1);
        expect(CashSettlQuoteMethod.Offer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CashSettlQuoteMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CashSettlQuoteMethod.Bid, CashSettlQuoteMethod.Mid, CashSettlQuoteMethod.Offer];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CashSettlQuoteMethod type', () => {
        const validate = (value: CashSettlQuoteMethod) => {
            expect(Object.values(CashSettlQuoteMethod)).toContain(value);
        };

        validate(CashSettlQuoteMethod.Bid);
        validate(CashSettlQuoteMethod.Mid);
        validate(CashSettlQuoteMethod.Offer);
    });

    test('should be immutable', () => {
        const ref = CashSettlQuoteMethod;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
