import { ReturnRateQuoteTimeType } from '../../src/fieldtypes/ReturnRateQuoteTimeType';

describe('ReturnRateQuoteTimeType', () => {
    test('should have the correct values', () => {
        expect(ReturnRateQuoteTimeType.Open).toBe(0);
        expect(ReturnRateQuoteTimeType.OfficialSettlPx).toBe(1);
        expect(ReturnRateQuoteTimeType.Xetra).toBe(2);
        expect(ReturnRateQuoteTimeType.Close).toBe(3);
        expect(ReturnRateQuoteTimeType.DerivativesClose).toBe(4);
        expect(ReturnRateQuoteTimeType.High).toBe(5);
        expect(ReturnRateQuoteTimeType.Low).toBe(6);
        expect(ReturnRateQuoteTimeType.AsSpecifiedInMasterConfirmation).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRateQuoteTimeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReturnRateQuoteTimeType.Open,
            ReturnRateQuoteTimeType.OfficialSettlPx,
            ReturnRateQuoteTimeType.Xetra,
            ReturnRateQuoteTimeType.Close,
            ReturnRateQuoteTimeType.DerivativesClose,
            ReturnRateQuoteTimeType.High,
            ReturnRateQuoteTimeType.Low,
            ReturnRateQuoteTimeType.AsSpecifiedInMasterConfirmation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRateQuoteTimeType type', () => {
        const validate = (value: ReturnRateQuoteTimeType) => {
            expect(Object.values(ReturnRateQuoteTimeType)).toContain(value);
        };

        validate(ReturnRateQuoteTimeType.Open);
        validate(ReturnRateQuoteTimeType.OfficialSettlPx);
        validate(ReturnRateQuoteTimeType.Xetra);
        validate(ReturnRateQuoteTimeType.Close);
        validate(ReturnRateQuoteTimeType.DerivativesClose);
        validate(ReturnRateQuoteTimeType.High);
        validate(ReturnRateQuoteTimeType.Low);
        validate(ReturnRateQuoteTimeType.AsSpecifiedInMasterConfirmation);
    });

    test('should be immutable', () => {
        const ref = ReturnRateQuoteTimeType;
        expect(() => {
            ref.Open = 10;
        }).toThrowError();
    });
});
