import { ApplVerID } from '../../src/fieldtypes/ApplVerID';

describe('ApplVerID', () => {
    test('should have the correct values', () => {
        expect(ApplVerID.FIX27).toBe('0');
        expect(ApplVerID.FIX30).toBe('1');
        expect(ApplVerID.FIX40).toBe('2');
        expect(ApplVerID.FIX41).toBe('3');
        expect(ApplVerID.FIX42).toBe('4');
        expect(ApplVerID.FIX43).toBe('5');
        expect(ApplVerID.FIX44).toBe('6');
        expect(ApplVerID.FIX50).toBe('7');
        expect(ApplVerID.FIX50SP1).toBe('8');
        expect(ApplVerID.FIX50SP2).toBe('9');
        expect(ApplVerID.FIXLatest).toBe('10');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplVerID.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplVerID.FIX27,
            ApplVerID.FIX30,
            ApplVerID.FIX40,
            ApplVerID.FIX41,
            ApplVerID.FIX42,
            ApplVerID.FIX43,
            ApplVerID.FIX44,
            ApplVerID.FIX50,
            ApplVerID.FIX50SP1,
            ApplVerID.FIX50SP2,
            ApplVerID.FIXLatest,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ApplVerID type', () => {
        const validate = (value: ApplVerID) => {
            expect(Object.values(ApplVerID)).toContain(value);
        };

        validate(ApplVerID.FIX27);
        validate(ApplVerID.FIX30);
        validate(ApplVerID.FIX40);
        validate(ApplVerID.FIX41);
        validate(ApplVerID.FIX42);
        validate(ApplVerID.FIX43);
        validate(ApplVerID.FIX44);
        validate(ApplVerID.FIX50);
        validate(ApplVerID.FIX50SP1);
        validate(ApplVerID.FIX50SP2);
        validate(ApplVerID.FIXLatest);
    });

    test('should be immutable', () => {
        const ref = ApplVerID;
        expect(() => {
            ref.FIX27 = 10;
        }).toThrowError();
    });
});
