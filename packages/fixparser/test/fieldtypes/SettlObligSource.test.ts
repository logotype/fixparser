import { SettlObligSource } from '../../src/fieldtypes/SettlObligSource';

describe('SettlObligSource', () => {
    test('should have the correct values', () => {
        expect(SettlObligSource.InstructionsOfBroker).toBe('1');
        expect(SettlObligSource.InstructionsForInstitution).toBe('2');
        expect(SettlObligSource.Investor).toBe('3');
        expect(SettlObligSource.BuyersSettlementInstructions).toBe('4');
        expect(SettlObligSource.SellersSettlementInstructions).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlObligSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlObligSource.InstructionsOfBroker,
            SettlObligSource.InstructionsForInstitution,
            SettlObligSource.Investor,
            SettlObligSource.BuyersSettlementInstructions,
            SettlObligSource.SellersSettlementInstructions,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlObligSource type', () => {
        const validate = (value: SettlObligSource) => {
            expect(Object.values(SettlObligSource)).toContain(value);
        };

        validate(SettlObligSource.InstructionsOfBroker);
        validate(SettlObligSource.InstructionsForInstitution);
        validate(SettlObligSource.Investor);
        validate(SettlObligSource.BuyersSettlementInstructions);
        validate(SettlObligSource.SellersSettlementInstructions);
    });

    test('should be immutable', () => {
        const ref = SettlObligSource;
        expect(() => {
            ref.InstructionsOfBroker = 10;
        }).toThrowError();
    });
});
