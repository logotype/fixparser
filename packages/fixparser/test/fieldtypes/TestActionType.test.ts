import { TestActionType } from '../../src/fieldtypes/TestActionType';

describe('TestActionType', () => {
    test('should have the correct values', () => {
        expect(TestActionType.Start).toBe(0);
        expect(TestActionType.Stop).toBe(1);
        expect(TestActionType.State).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestActionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TestActionType.Start, TestActionType.Stop, TestActionType.State];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestActionType type', () => {
        const validate = (value: TestActionType) => {
            expect(Object.values(TestActionType)).toContain(value);
        };

        validate(TestActionType.Start);
        validate(TestActionType.Stop);
        validate(TestActionType.State);
    });

    test('should be immutable', () => {
        const ref = TestActionType;
        expect(() => {
            ref.Start = 10;
        }).toThrowError();
    });
});
