import { PegLimitType } from '../../src/fieldtypes/PegLimitType';

describe('PegLimitType', () => {
    test('should have the correct values', () => {
        expect(PegLimitType.OrBetter).toBe(0);
        expect(PegLimitType.Strict).toBe(1);
        expect(PegLimitType.OrWorse).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegLimitType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PegLimitType.OrBetter, PegLimitType.Strict, PegLimitType.OrWorse];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegLimitType type', () => {
        const validate = (value: PegLimitType) => {
            expect(Object.values(PegLimitType)).toContain(value);
        };

        validate(PegLimitType.OrBetter);
        validate(PegLimitType.Strict);
        validate(PegLimitType.OrWorse);
    });

    test('should be immutable', () => {
        const ref = PegLimitType;
        expect(() => {
            ref.OrBetter = 10;
        }).toThrowError();
    });
});
