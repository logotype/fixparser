import { SettlPriceType } from '../../src/fieldtypes/SettlPriceType';

describe('SettlPriceType', () => {
    test('should have the correct values', () => {
        expect(SettlPriceType.Final).toBe(1);
        expect(SettlPriceType.Theoretical).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SettlPriceType.Final, SettlPriceType.Theoretical];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlPriceType type', () => {
        const validate = (value: SettlPriceType) => {
            expect(Object.values(SettlPriceType)).toContain(value);
        };

        validate(SettlPriceType.Final);
        validate(SettlPriceType.Theoretical);
    });

    test('should be immutable', () => {
        const ref = SettlPriceType;
        expect(() => {
            ref.Final = 10;
        }).toThrowError();
    });
});
