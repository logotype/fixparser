import { MassOrderRequestResult } from '../../src/fieldtypes/MassOrderRequestResult';

describe('MassOrderRequestResult', () => {
    test('should have the correct values', () => {
        expect(MassOrderRequestResult.Successful).toBe(0);
        expect(MassOrderRequestResult.ResponseLevelNotSupported).toBe(1);
        expect(MassOrderRequestResult.InvalidMarket).toBe(2);
        expect(MassOrderRequestResult.InvalidMarketSegment).toBe(3);
        expect(MassOrderRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassOrderRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassOrderRequestResult.Successful,
            MassOrderRequestResult.ResponseLevelNotSupported,
            MassOrderRequestResult.InvalidMarket,
            MassOrderRequestResult.InvalidMarketSegment,
            MassOrderRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassOrderRequestResult type', () => {
        const validate = (value: MassOrderRequestResult) => {
            expect(Object.values(MassOrderRequestResult)).toContain(value);
        };

        validate(MassOrderRequestResult.Successful);
        validate(MassOrderRequestResult.ResponseLevelNotSupported);
        validate(MassOrderRequestResult.InvalidMarket);
        validate(MassOrderRequestResult.InvalidMarketSegment);
        validate(MassOrderRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = MassOrderRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
