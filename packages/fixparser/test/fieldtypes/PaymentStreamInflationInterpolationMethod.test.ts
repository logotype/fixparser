import { PaymentStreamInflationInterpolationMethod } from '../../src/fieldtypes/PaymentStreamInflationInterpolationMethod';

describe('PaymentStreamInflationInterpolationMethod', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamInflationInterpolationMethod.None).toBe(0);
        expect(PaymentStreamInflationInterpolationMethod.LinearZeroYield).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamInflationInterpolationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamInflationInterpolationMethod.None,
            PaymentStreamInflationInterpolationMethod.LinearZeroYield,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamInflationInterpolationMethod type', () => {
        const validate = (value: PaymentStreamInflationInterpolationMethod) => {
            expect(Object.values(PaymentStreamInflationInterpolationMethod)).toContain(value);
        };

        validate(PaymentStreamInflationInterpolationMethod.None);
        validate(PaymentStreamInflationInterpolationMethod.LinearZeroYield);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamInflationInterpolationMethod;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
