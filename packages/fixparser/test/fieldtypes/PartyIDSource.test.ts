import { PartyIDSource } from '../../src/fieldtypes/PartyIDSource';

describe('PartyIDSource', () => {
    test('should have the correct values', () => {
        expect(PartyIDSource.UKNationalInsuranceOrPensionNumber).toBe('6');
        expect(PartyIDSource.KoreanInvestorID).toBe('1');
        expect(PartyIDSource.ISITCAcronym).toBe('I');
        expect(PartyIDSource.BIC).toBe('B');
        expect(PartyIDSource.USSocialSecurityNumber).toBe('7');
        expect(PartyIDSource.TaiwaneseForeignInvestorID).toBe('2');
        expect(PartyIDSource.GeneralIdentifier).toBe('C');
        expect(PartyIDSource.USEmployerOrTaxIDNumber).toBe('8');
        expect(PartyIDSource.TaiwaneseTradingAcct).toBe('3');
        expect(PartyIDSource.Proprietary).toBe('D');
        expect(PartyIDSource.AustralianBusinessNumber).toBe('9');
        expect(PartyIDSource.MalaysianCentralDepository).toBe('4');
        expect(PartyIDSource.ISOCountryCode).toBe('E');
        expect(PartyIDSource.AustralianTaxFileNumber).toBe('A');
        expect(PartyIDSource.ChineseInvestorID).toBe('5');
        expect(PartyIDSource.SettlementEntityLocation).toBe('F');
        expect(PartyIDSource.TaxID).toBe('J');
        expect(PartyIDSource.MIC).toBe('G');
        expect(PartyIDSource.CSDParticipant).toBe('H');
        expect(PartyIDSource.AustralianCompanyNumber).toBe('K');
        expect(PartyIDSource.AustralianRegisteredBodyNumber).toBe('L');
        expect(PartyIDSource.CFTCReportingFirmIdentifier).toBe('M');
        expect(PartyIDSource.LegalEntityIdentifier).toBe('N');
        expect(PartyIDSource.InterimIdentifier).toBe('O');
        expect(PartyIDSource.ShortCodeIdentifier).toBe('P');
        expect(PartyIDSource.NationalIDNaturalPerson).toBe('Q');
        expect(PartyIDSource.IndiaPermanentAccountNumber).toBe('R');
        expect(PartyIDSource.FDID).toBe('S');
        expect(PartyIDSource.SPSAID).toBe('T');
        expect(PartyIDSource.MasterSPSAID).toBe('U');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyIDSource.UKNationalInsuranceOrPensionNumber,
            PartyIDSource.KoreanInvestorID,
            PartyIDSource.ISITCAcronym,
            PartyIDSource.BIC,
            PartyIDSource.USSocialSecurityNumber,
            PartyIDSource.TaiwaneseForeignInvestorID,
            PartyIDSource.GeneralIdentifier,
            PartyIDSource.USEmployerOrTaxIDNumber,
            PartyIDSource.TaiwaneseTradingAcct,
            PartyIDSource.Proprietary,
            PartyIDSource.AustralianBusinessNumber,
            PartyIDSource.MalaysianCentralDepository,
            PartyIDSource.ISOCountryCode,
            PartyIDSource.AustralianTaxFileNumber,
            PartyIDSource.ChineseInvestorID,
            PartyIDSource.SettlementEntityLocation,
            PartyIDSource.TaxID,
            PartyIDSource.MIC,
            PartyIDSource.CSDParticipant,
            PartyIDSource.AustralianCompanyNumber,
            PartyIDSource.AustralianRegisteredBodyNumber,
            PartyIDSource.CFTCReportingFirmIdentifier,
            PartyIDSource.LegalEntityIdentifier,
            PartyIDSource.InterimIdentifier,
            PartyIDSource.ShortCodeIdentifier,
            PartyIDSource.NationalIDNaturalPerson,
            PartyIDSource.IndiaPermanentAccountNumber,
            PartyIDSource.FDID,
            PartyIDSource.SPSAID,
            PartyIDSource.MasterSPSAID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PartyIDSource type', () => {
        const validate = (value: PartyIDSource) => {
            expect(Object.values(PartyIDSource)).toContain(value);
        };

        validate(PartyIDSource.UKNationalInsuranceOrPensionNumber);
        validate(PartyIDSource.KoreanInvestorID);
        validate(PartyIDSource.ISITCAcronym);
        validate(PartyIDSource.BIC);
        validate(PartyIDSource.USSocialSecurityNumber);
        validate(PartyIDSource.TaiwaneseForeignInvestorID);
        validate(PartyIDSource.GeneralIdentifier);
        validate(PartyIDSource.USEmployerOrTaxIDNumber);
        validate(PartyIDSource.TaiwaneseTradingAcct);
        validate(PartyIDSource.Proprietary);
        validate(PartyIDSource.AustralianBusinessNumber);
        validate(PartyIDSource.MalaysianCentralDepository);
        validate(PartyIDSource.ISOCountryCode);
        validate(PartyIDSource.AustralianTaxFileNumber);
        validate(PartyIDSource.ChineseInvestorID);
        validate(PartyIDSource.SettlementEntityLocation);
        validate(PartyIDSource.TaxID);
        validate(PartyIDSource.MIC);
        validate(PartyIDSource.CSDParticipant);
        validate(PartyIDSource.AustralianCompanyNumber);
        validate(PartyIDSource.AustralianRegisteredBodyNumber);
        validate(PartyIDSource.CFTCReportingFirmIdentifier);
        validate(PartyIDSource.LegalEntityIdentifier);
        validate(PartyIDSource.InterimIdentifier);
        validate(PartyIDSource.ShortCodeIdentifier);
        validate(PartyIDSource.NationalIDNaturalPerson);
        validate(PartyIDSource.IndiaPermanentAccountNumber);
        validate(PartyIDSource.FDID);
        validate(PartyIDSource.SPSAID);
        validate(PartyIDSource.MasterSPSAID);
    });

    test('should be immutable', () => {
        const ref = PartyIDSource;
        expect(() => {
            ref.UKNationalInsuranceOrPensionNumber = 10;
        }).toThrowError();
    });
});
