import { PartyActionType } from '../../src/fieldtypes/PartyActionType';

describe('PartyActionType', () => {
    test('should have the correct values', () => {
        expect(PartyActionType.Suspend).toBe(0);
        expect(PartyActionType.HaltTrading).toBe(1);
        expect(PartyActionType.Reinstate).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyActionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PartyActionType.Suspend, PartyActionType.HaltTrading, PartyActionType.Reinstate];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyActionType type', () => {
        const validate = (value: PartyActionType) => {
            expect(Object.values(PartyActionType)).toContain(value);
        };

        validate(PartyActionType.Suspend);
        validate(PartyActionType.HaltTrading);
        validate(PartyActionType.Reinstate);
    });

    test('should be immutable', () => {
        const ref = PartyActionType;
        expect(() => {
            ref.Suspend = 10;
        }).toThrowError();
    });
});
