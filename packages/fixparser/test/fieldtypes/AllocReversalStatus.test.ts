import { AllocReversalStatus } from '../../src/fieldtypes/AllocReversalStatus';

describe('AllocReversalStatus', () => {
    test('should have the correct values', () => {
        expect(AllocReversalStatus.Completed).toBe(0);
        expect(AllocReversalStatus.Refused).toBe(1);
        expect(AllocReversalStatus.Cancelled).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocReversalStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocReversalStatus.Completed,
            AllocReversalStatus.Refused,
            AllocReversalStatus.Cancelled,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocReversalStatus type', () => {
        const validate = (value: AllocReversalStatus) => {
            expect(Object.values(AllocReversalStatus)).toContain(value);
        };

        validate(AllocReversalStatus.Completed);
        validate(AllocReversalStatus.Refused);
        validate(AllocReversalStatus.Cancelled);
    });

    test('should be immutable', () => {
        const ref = AllocReversalStatus;
        expect(() => {
            ref.Completed = 10;
        }).toThrowError();
    });
});
