import { CxlRejReason } from '../../src/fieldtypes/CxlRejReason';

describe('CxlRejReason', () => {
    test('should have the correct values', () => {
        expect(CxlRejReason.TooLateToCancel).toBe(0);
        expect(CxlRejReason.UnknownOrder).toBe(1);
        expect(CxlRejReason.BrokerCredit).toBe(2);
        expect(CxlRejReason.OrderAlreadyInPendingStatus).toBe(3);
        expect(CxlRejReason.UnableToProcessOrderMassCancelRequest).toBe(4);
        expect(CxlRejReason.OrigOrdModTime).toBe(5);
        expect(CxlRejReason.DuplicateClOrdID).toBe(6);
        expect(CxlRejReason.PriceExceedsCurrentPrice).toBe(7);
        expect(CxlRejReason.PriceExceedsCurrentPriceBand).toBe(8);
        expect(CxlRejReason.InvalidPriceIncrement).toBe(18);
        expect(CxlRejReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CxlRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CxlRejReason.TooLateToCancel,
            CxlRejReason.UnknownOrder,
            CxlRejReason.BrokerCredit,
            CxlRejReason.OrderAlreadyInPendingStatus,
            CxlRejReason.UnableToProcessOrderMassCancelRequest,
            CxlRejReason.OrigOrdModTime,
            CxlRejReason.DuplicateClOrdID,
            CxlRejReason.PriceExceedsCurrentPrice,
            CxlRejReason.PriceExceedsCurrentPriceBand,
            CxlRejReason.InvalidPriceIncrement,
            CxlRejReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CxlRejReason type', () => {
        const validate = (value: CxlRejReason) => {
            expect(Object.values(CxlRejReason)).toContain(value);
        };

        validate(CxlRejReason.TooLateToCancel);
        validate(CxlRejReason.UnknownOrder);
        validate(CxlRejReason.BrokerCredit);
        validate(CxlRejReason.OrderAlreadyInPendingStatus);
        validate(CxlRejReason.UnableToProcessOrderMassCancelRequest);
        validate(CxlRejReason.OrigOrdModTime);
        validate(CxlRejReason.DuplicateClOrdID);
        validate(CxlRejReason.PriceExceedsCurrentPrice);
        validate(CxlRejReason.PriceExceedsCurrentPriceBand);
        validate(CxlRejReason.InvalidPriceIncrement);
        validate(CxlRejReason.Other);
    });

    test('should be immutable', () => {
        const ref = CxlRejReason;
        expect(() => {
            ref.TooLateToCancel = 10;
        }).toThrowError();
    });
});
