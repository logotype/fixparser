import { SettlObligMode } from '../../src/fieldtypes/SettlObligMode';

describe('SettlObligMode', () => {
    test('should have the correct values', () => {
        expect(SettlObligMode.Preliminary).toBe(1);
        expect(SettlObligMode.Final).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlObligMode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SettlObligMode.Preliminary, SettlObligMode.Final];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlObligMode type', () => {
        const validate = (value: SettlObligMode) => {
            expect(Object.values(SettlObligMode)).toContain(value);
        };

        validate(SettlObligMode.Preliminary);
        validate(SettlObligMode.Final);
    });

    test('should be immutable', () => {
        const ref = SettlObligMode;
        expect(() => {
            ref.Preliminary = 10;
        }).toThrowError();
    });
});
