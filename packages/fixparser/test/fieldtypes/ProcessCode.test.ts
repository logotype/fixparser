import { ProcessCode } from '../../src/fieldtypes/ProcessCode';

describe('ProcessCode', () => {
    test('should have the correct values', () => {
        expect(ProcessCode.Regular).toBe('0');
        expect(ProcessCode.SoftDollar).toBe('1');
        expect(ProcessCode.StepIn).toBe('2');
        expect(ProcessCode.StepOut).toBe('3');
        expect(ProcessCode.SoftDollarStepIn).toBe('4');
        expect(ProcessCode.SoftDollarStepOut).toBe('5');
        expect(ProcessCode.PlanSponsor).toBe('6');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProcessCode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProcessCode.Regular,
            ProcessCode.SoftDollar,
            ProcessCode.StepIn,
            ProcessCode.StepOut,
            ProcessCode.SoftDollarStepIn,
            ProcessCode.SoftDollarStepOut,
            ProcessCode.PlanSponsor,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ProcessCode type', () => {
        const validate = (value: ProcessCode) => {
            expect(Object.values(ProcessCode)).toContain(value);
        };

        validate(ProcessCode.Regular);
        validate(ProcessCode.SoftDollar);
        validate(ProcessCode.StepIn);
        validate(ProcessCode.StepOut);
        validate(ProcessCode.SoftDollarStepIn);
        validate(ProcessCode.SoftDollarStepOut);
        validate(ProcessCode.PlanSponsor);
    });

    test('should be immutable', () => {
        const ref = ProcessCode;
        expect(() => {
            ref.Regular = 10;
        }).toThrowError();
    });
});
