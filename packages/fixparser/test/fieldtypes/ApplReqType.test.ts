import { ApplReqType } from '../../src/fieldtypes/ApplReqType';

describe('ApplReqType', () => {
    test('should have the correct values', () => {
        expect(ApplReqType.Retransmission).toBe(0);
        expect(ApplReqType.Subscription).toBe(1);
        expect(ApplReqType.RequestLastSeqNum).toBe(2);
        expect(ApplReqType.RequestApplications).toBe(3);
        expect(ApplReqType.Unsubscribe).toBe(4);
        expect(ApplReqType.CancelRetransmission).toBe(5);
        expect(ApplReqType.CancelRetransmissionUnsubscribe).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplReqType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplReqType.Retransmission,
            ApplReqType.Subscription,
            ApplReqType.RequestLastSeqNum,
            ApplReqType.RequestApplications,
            ApplReqType.Unsubscribe,
            ApplReqType.CancelRetransmission,
            ApplReqType.CancelRetransmissionUnsubscribe,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplReqType type', () => {
        const validate = (value: ApplReqType) => {
            expect(Object.values(ApplReqType)).toContain(value);
        };

        validate(ApplReqType.Retransmission);
        validate(ApplReqType.Subscription);
        validate(ApplReqType.RequestLastSeqNum);
        validate(ApplReqType.RequestApplications);
        validate(ApplReqType.Unsubscribe);
        validate(ApplReqType.CancelRetransmission);
        validate(ApplReqType.CancelRetransmissionUnsubscribe);
    });

    test('should be immutable', () => {
        const ref = ApplReqType;
        expect(() => {
            ref.Retransmission = 10;
        }).toThrowError();
    });
});
