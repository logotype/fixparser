import { StreamAsgnRejReason } from '../../src/fieldtypes/StreamAsgnRejReason';

describe('StreamAsgnRejReason', () => {
    test('should have the correct values', () => {
        expect(StreamAsgnRejReason.UnknownClient).toBe(0);
        expect(StreamAsgnRejReason.ExceedsMaximumSize).toBe(1);
        expect(StreamAsgnRejReason.UnknownOrInvalidCurrencyPair).toBe(2);
        expect(StreamAsgnRejReason.NoAvailableStream).toBe(3);
        expect(StreamAsgnRejReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamAsgnRejReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StreamAsgnRejReason.UnknownClient,
            StreamAsgnRejReason.ExceedsMaximumSize,
            StreamAsgnRejReason.UnknownOrInvalidCurrencyPair,
            StreamAsgnRejReason.NoAvailableStream,
            StreamAsgnRejReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamAsgnRejReason type', () => {
        const validate = (value: StreamAsgnRejReason) => {
            expect(Object.values(StreamAsgnRejReason)).toContain(value);
        };

        validate(StreamAsgnRejReason.UnknownClient);
        validate(StreamAsgnRejReason.ExceedsMaximumSize);
        validate(StreamAsgnRejReason.UnknownOrInvalidCurrencyPair);
        validate(StreamAsgnRejReason.NoAvailableStream);
        validate(StreamAsgnRejReason.Other);
    });

    test('should be immutable', () => {
        const ref = StreamAsgnRejReason;
        expect(() => {
            ref.UnknownClient = 10;
        }).toThrowError();
    });
});
