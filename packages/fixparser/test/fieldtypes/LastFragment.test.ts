import { LastFragment } from '../../src/fieldtypes/LastFragment';

describe('LastFragment', () => {
    test('should have the correct values', () => {
        expect(LastFragment.NotLastMessage).toBe('N');
        expect(LastFragment.LastMessage).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LastFragment.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [LastFragment.NotLastMessage, LastFragment.LastMessage];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LastFragment type', () => {
        const validate = (value: LastFragment) => {
            expect(Object.values(LastFragment)).toContain(value);
        };

        validate(LastFragment.NotLastMessage);
        validate(LastFragment.LastMessage);
    });

    test('should be immutable', () => {
        const ref = LastFragment;
        expect(() => {
            ref.NotLastMessage = 10;
        }).toThrowError();
    });
});
