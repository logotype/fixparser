import { HandlInst } from '../../src/fieldtypes/HandlInst';

describe('HandlInst', () => {
    test('should have the correct values', () => {
        expect(HandlInst.AutomatedExecutionNoIntervention).toBe('1');
        expect(HandlInst.AutomatedExecutionInterventionOK).toBe('2');
        expect(HandlInst.ManualOrder).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            HandlInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            HandlInst.AutomatedExecutionNoIntervention,
            HandlInst.AutomatedExecutionInterventionOK,
            HandlInst.ManualOrder,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for HandlInst type', () => {
        const validate = (value: HandlInst) => {
            expect(Object.values(HandlInst)).toContain(value);
        };

        validate(HandlInst.AutomatedExecutionNoIntervention);
        validate(HandlInst.AutomatedExecutionInterventionOK);
        validate(HandlInst.ManualOrder);
    });

    test('should be immutable', () => {
        const ref = HandlInst;
        expect(() => {
            ref.AutomatedExecutionNoIntervention = 10;
        }).toThrowError();
    });
});
