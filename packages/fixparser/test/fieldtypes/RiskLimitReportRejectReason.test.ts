import { RiskLimitReportRejectReason } from '../../src/fieldtypes/RiskLimitReportRejectReason';

describe('RiskLimitReportRejectReason', () => {
    test('should have the correct values', () => {
        expect(RiskLimitReportRejectReason.UnkRiskLmtRprtID).toBe(0);
        expect(RiskLimitReportRejectReason.UnkPty).toBe(1);
        expect(RiskLimitReportRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitReportRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitReportRejectReason.UnkRiskLmtRprtID,
            RiskLimitReportRejectReason.UnkPty,
            RiskLimitReportRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitReportRejectReason type', () => {
        const validate = (value: RiskLimitReportRejectReason) => {
            expect(Object.values(RiskLimitReportRejectReason)).toContain(value);
        };

        validate(RiskLimitReportRejectReason.UnkRiskLmtRprtID);
        validate(RiskLimitReportRejectReason.UnkPty);
        validate(RiskLimitReportRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = RiskLimitReportRejectReason;
        expect(() => {
            ref.UnkRiskLmtRprtID = 10;
        }).toThrowError();
    });
});
