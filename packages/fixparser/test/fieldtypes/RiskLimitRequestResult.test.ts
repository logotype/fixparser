import { RiskLimitRequestResult } from '../../src/fieldtypes/RiskLimitRequestResult';

describe('RiskLimitRequestResult', () => {
    test('should have the correct values', () => {
        expect(RiskLimitRequestResult.Successful).toBe(0);
        expect(RiskLimitRequestResult.InvalidParty).toBe(1);
        expect(RiskLimitRequestResult.InvalidRelatedParty).toBe(2);
        expect(RiskLimitRequestResult.InvalidRiskLimitType).toBe(3);
        expect(RiskLimitRequestResult.InvalidRiskLimitID).toBe(4);
        expect(RiskLimitRequestResult.InvalidRiskLimitAmount).toBe(5);
        expect(RiskLimitRequestResult.InvalidRiskWarningLevelAction).toBe(6);
        expect(RiskLimitRequestResult.InvalidRiskInstrumentScope).toBe(7);
        expect(RiskLimitRequestResult.RiskLimitActionsNotSupported).toBe(8);
        expect(RiskLimitRequestResult.WarningLevelsNotSupported).toBe(9);
        expect(RiskLimitRequestResult.WarningLevelActionsNotSupported).toBe(10);
        expect(RiskLimitRequestResult.RiskInstrumentScopeNotSupported).toBe(11);
        expect(RiskLimitRequestResult.RiskLimitNotApprovedForParty).toBe(12);
        expect(RiskLimitRequestResult.RiskLimitAlreadyDefinedForParty).toBe(13);
        expect(RiskLimitRequestResult.InstrumentNotApprovedForParty).toBe(14);
        expect(RiskLimitRequestResult.NotAuthorized).toBe(98);
        expect(RiskLimitRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitRequestResult.Successful,
            RiskLimitRequestResult.InvalidParty,
            RiskLimitRequestResult.InvalidRelatedParty,
            RiskLimitRequestResult.InvalidRiskLimitType,
            RiskLimitRequestResult.InvalidRiskLimitID,
            RiskLimitRequestResult.InvalidRiskLimitAmount,
            RiskLimitRequestResult.InvalidRiskWarningLevelAction,
            RiskLimitRequestResult.InvalidRiskInstrumentScope,
            RiskLimitRequestResult.RiskLimitActionsNotSupported,
            RiskLimitRequestResult.WarningLevelsNotSupported,
            RiskLimitRequestResult.WarningLevelActionsNotSupported,
            RiskLimitRequestResult.RiskInstrumentScopeNotSupported,
            RiskLimitRequestResult.RiskLimitNotApprovedForParty,
            RiskLimitRequestResult.RiskLimitAlreadyDefinedForParty,
            RiskLimitRequestResult.InstrumentNotApprovedForParty,
            RiskLimitRequestResult.NotAuthorized,
            RiskLimitRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitRequestResult type', () => {
        const validate = (value: RiskLimitRequestResult) => {
            expect(Object.values(RiskLimitRequestResult)).toContain(value);
        };

        validate(RiskLimitRequestResult.Successful);
        validate(RiskLimitRequestResult.InvalidParty);
        validate(RiskLimitRequestResult.InvalidRelatedParty);
        validate(RiskLimitRequestResult.InvalidRiskLimitType);
        validate(RiskLimitRequestResult.InvalidRiskLimitID);
        validate(RiskLimitRequestResult.InvalidRiskLimitAmount);
        validate(RiskLimitRequestResult.InvalidRiskWarningLevelAction);
        validate(RiskLimitRequestResult.InvalidRiskInstrumentScope);
        validate(RiskLimitRequestResult.RiskLimitActionsNotSupported);
        validate(RiskLimitRequestResult.WarningLevelsNotSupported);
        validate(RiskLimitRequestResult.WarningLevelActionsNotSupported);
        validate(RiskLimitRequestResult.RiskInstrumentScopeNotSupported);
        validate(RiskLimitRequestResult.RiskLimitNotApprovedForParty);
        validate(RiskLimitRequestResult.RiskLimitAlreadyDefinedForParty);
        validate(RiskLimitRequestResult.InstrumentNotApprovedForParty);
        validate(RiskLimitRequestResult.NotAuthorized);
        validate(RiskLimitRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = RiskLimitRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
