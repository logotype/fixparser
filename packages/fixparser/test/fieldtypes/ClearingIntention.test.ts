import { ClearingIntention } from '../../src/fieldtypes/ClearingIntention';

describe('ClearingIntention', () => {
    test('should have the correct values', () => {
        expect(ClearingIntention.DoNotIntendToClear).toBe(0);
        expect(ClearingIntention.IntendToClear).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearingIntention.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ClearingIntention.DoNotIntendToClear, ClearingIntention.IntendToClear];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ClearingIntention type', () => {
        const validate = (value: ClearingIntention) => {
            expect(Object.values(ClearingIntention)).toContain(value);
        };

        validate(ClearingIntention.DoNotIntendToClear);
        validate(ClearingIntention.IntendToClear);
    });

    test('should be immutable', () => {
        const ref = ClearingIntention;
        expect(() => {
            ref.DoNotIntendToClear = 10;
        }).toThrowError();
    });
});
