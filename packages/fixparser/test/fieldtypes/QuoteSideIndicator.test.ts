import { QuoteSideIndicator } from '../../src/fieldtypes/QuoteSideIndicator';

describe('QuoteSideIndicator', () => {
    test('should have the correct values', () => {
        expect(QuoteSideIndicator.No).toBe('N');
        expect(QuoteSideIndicator.Yes).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteSideIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [QuoteSideIndicator.No, QuoteSideIndicator.Yes];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for QuoteSideIndicator type', () => {
        const validate = (value: QuoteSideIndicator) => {
            expect(Object.values(QuoteSideIndicator)).toContain(value);
        };

        validate(QuoteSideIndicator.No);
        validate(QuoteSideIndicator.Yes);
    });

    test('should be immutable', () => {
        const ref = QuoteSideIndicator;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
