import { TrdRptStatus } from '../../src/fieldtypes/TrdRptStatus';

describe('TrdRptStatus', () => {
    test('should have the correct values', () => {
        expect(TrdRptStatus.Accepted).toBe(0);
        expect(TrdRptStatus.Rejected).toBe(1);
        expect(TrdRptStatus.Cancelled).toBe(2);
        expect(TrdRptStatus.AcceptedWithErrors).toBe(3);
        expect(TrdRptStatus.PendingNew).toBe(4);
        expect(TrdRptStatus.PendingCancel).toBe(5);
        expect(TrdRptStatus.PendingReplace).toBe(6);
        expect(TrdRptStatus.Terminated).toBe(7);
        expect(TrdRptStatus.PendingVerification).toBe(8);
        expect(TrdRptStatus.DeemedVerified).toBe(9);
        expect(TrdRptStatus.Verified).toBe(10);
        expect(TrdRptStatus.Disputed).toBe(11);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdRptStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdRptStatus.Accepted,
            TrdRptStatus.Rejected,
            TrdRptStatus.Cancelled,
            TrdRptStatus.AcceptedWithErrors,
            TrdRptStatus.PendingNew,
            TrdRptStatus.PendingCancel,
            TrdRptStatus.PendingReplace,
            TrdRptStatus.Terminated,
            TrdRptStatus.PendingVerification,
            TrdRptStatus.DeemedVerified,
            TrdRptStatus.Verified,
            TrdRptStatus.Disputed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdRptStatus type', () => {
        const validate = (value: TrdRptStatus) => {
            expect(Object.values(TrdRptStatus)).toContain(value);
        };

        validate(TrdRptStatus.Accepted);
        validate(TrdRptStatus.Rejected);
        validate(TrdRptStatus.Cancelled);
        validate(TrdRptStatus.AcceptedWithErrors);
        validate(TrdRptStatus.PendingNew);
        validate(TrdRptStatus.PendingCancel);
        validate(TrdRptStatus.PendingReplace);
        validate(TrdRptStatus.Terminated);
        validate(TrdRptStatus.PendingVerification);
        validate(TrdRptStatus.DeemedVerified);
        validate(TrdRptStatus.Verified);
        validate(TrdRptStatus.Disputed);
    });

    test('should be immutable', () => {
        const ref = TrdRptStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
