import { ShortSaleExemptionReason } from '../../src/fieldtypes/ShortSaleExemptionReason';

describe('ShortSaleExemptionReason', () => {
    test('should have the correct values', () => {
        expect(ShortSaleExemptionReason.ExemptionReasonUnknown).toBe(0);
        expect(ShortSaleExemptionReason.IncomingSSE).toBe(1);
        expect(ShortSaleExemptionReason.AboveNationalBestBid).toBe(2);
        expect(ShortSaleExemptionReason.DelayedDelivery).toBe(3);
        expect(ShortSaleExemptionReason.OddLot).toBe(4);
        expect(ShortSaleExemptionReason.DomesticArbitrage).toBe(5);
        expect(ShortSaleExemptionReason.InternationalArbitrage).toBe(6);
        expect(ShortSaleExemptionReason.UnderwriterOrSyndicateDistribution).toBe(7);
        expect(ShortSaleExemptionReason.RisklessPrincipal).toBe(8);
        expect(ShortSaleExemptionReason.VWAP).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ShortSaleExemptionReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ShortSaleExemptionReason.ExemptionReasonUnknown,
            ShortSaleExemptionReason.IncomingSSE,
            ShortSaleExemptionReason.AboveNationalBestBid,
            ShortSaleExemptionReason.DelayedDelivery,
            ShortSaleExemptionReason.OddLot,
            ShortSaleExemptionReason.DomesticArbitrage,
            ShortSaleExemptionReason.InternationalArbitrage,
            ShortSaleExemptionReason.UnderwriterOrSyndicateDistribution,
            ShortSaleExemptionReason.RisklessPrincipal,
            ShortSaleExemptionReason.VWAP,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ShortSaleExemptionReason type', () => {
        const validate = (value: ShortSaleExemptionReason) => {
            expect(Object.values(ShortSaleExemptionReason)).toContain(value);
        };

        validate(ShortSaleExemptionReason.ExemptionReasonUnknown);
        validate(ShortSaleExemptionReason.IncomingSSE);
        validate(ShortSaleExemptionReason.AboveNationalBestBid);
        validate(ShortSaleExemptionReason.DelayedDelivery);
        validate(ShortSaleExemptionReason.OddLot);
        validate(ShortSaleExemptionReason.DomesticArbitrage);
        validate(ShortSaleExemptionReason.InternationalArbitrage);
        validate(ShortSaleExemptionReason.UnderwriterOrSyndicateDistribution);
        validate(ShortSaleExemptionReason.RisklessPrincipal);
        validate(ShortSaleExemptionReason.VWAP);
    });

    test('should be immutable', () => {
        const ref = ShortSaleExemptionReason;
        expect(() => {
            ref.ExemptionReasonUnknown = 10;
        }).toThrowError();
    });
});
