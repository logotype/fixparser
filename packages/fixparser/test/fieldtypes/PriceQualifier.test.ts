import { PriceQualifier } from '../../src/fieldtypes/PriceQualifier';

describe('PriceQualifier', () => {
    test('should have the correct values', () => {
        expect(PriceQualifier.AccruedInterestIsFactored).toBe(0);
        expect(PriceQualifier.TaxIsFactored).toBe(1);
        expect(PriceQualifier.BondAmortizationIsFactored).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PriceQualifier.AccruedInterestIsFactored,
            PriceQualifier.TaxIsFactored,
            PriceQualifier.BondAmortizationIsFactored,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PriceQualifier type', () => {
        const validate = (value: PriceQualifier) => {
            expect(Object.values(PriceQualifier)).toContain(value);
        };

        validate(PriceQualifier.AccruedInterestIsFactored);
        validate(PriceQualifier.TaxIsFactored);
        validate(PriceQualifier.BondAmortizationIsFactored);
    });

    test('should be immutable', () => {
        const ref = PriceQualifier;
        expect(() => {
            ref.AccruedInterestIsFactored = 10;
        }).toThrowError();
    });
});
