import { IOIQty } from '../../src/fieldtypes/IOIQty';

describe('IOIQty', () => {
    test('should have the correct values', () => {
        expect(IOIQty.Small).toBe('S');
        expect(IOIQty.Medium).toBe('M');
        expect(IOIQty.Large).toBe('L');
        expect(IOIQty.UndisclosedQuantity).toBe('U');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOIQty.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IOIQty.Small, IOIQty.Medium, IOIQty.Large, IOIQty.UndisclosedQuantity];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOIQty type', () => {
        const validate = (value: IOIQty) => {
            expect(Object.values(IOIQty)).toContain(value);
        };

        validate(IOIQty.Small);
        validate(IOIQty.Medium);
        validate(IOIQty.Large);
        validate(IOIQty.UndisclosedQuantity);
    });

    test('should be immutable', () => {
        const ref = IOIQty;
        expect(() => {
            ref.Small = 10;
        }).toThrowError();
    });
});
