import { SettlMethod } from '../../src/fieldtypes/SettlMethod';

describe('SettlMethod', () => {
    test('should have the correct values', () => {
        expect(SettlMethod.CashSettlementRequired).toBe('C');
        expect(SettlMethod.PhysicalSettlementRequired).toBe('P');
        expect(SettlMethod.Election).toBe('E');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlMethod.CashSettlementRequired,
            SettlMethod.PhysicalSettlementRequired,
            SettlMethod.Election,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlMethod type', () => {
        const validate = (value: SettlMethod) => {
            expect(Object.values(SettlMethod)).toContain(value);
        };

        validate(SettlMethod.CashSettlementRequired);
        validate(SettlMethod.PhysicalSettlementRequired);
        validate(SettlMethod.Election);
    });

    test('should be immutable', () => {
        const ref = SettlMethod;
        expect(() => {
            ref.CashSettlementRequired = 10;
        }).toThrowError();
    });
});
