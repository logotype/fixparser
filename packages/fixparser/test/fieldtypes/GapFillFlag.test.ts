import { GapFillFlag } from '../../src/fieldtypes/GapFillFlag';

describe('GapFillFlag', () => {
    test('should have the correct values', () => {
        expect(GapFillFlag.SequenceReset).toBe('N');
        expect(GapFillFlag.GapFillMessage).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            GapFillFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [GapFillFlag.SequenceReset, GapFillFlag.GapFillMessage];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for GapFillFlag type', () => {
        const validate = (value: GapFillFlag) => {
            expect(Object.values(GapFillFlag)).toContain(value);
        };

        validate(GapFillFlag.SequenceReset);
        validate(GapFillFlag.GapFillMessage);
    });

    test('should be immutable', () => {
        const ref = GapFillFlag;
        expect(() => {
            ref.SequenceReset = 10;
        }).toThrowError();
    });
});
