import { BusinessDayConvention } from '../../src/fieldtypes/BusinessDayConvention';

describe('BusinessDayConvention', () => {
    test('should have the correct values', () => {
        expect(BusinessDayConvention.NotApplicable).toBe(0);
        expect(BusinessDayConvention.None).toBe(1);
        expect(BusinessDayConvention.FollowingDay).toBe(2);
        expect(BusinessDayConvention.FloatingRateNote).toBe(3);
        expect(BusinessDayConvention.ModifiedFollowingDay).toBe(4);
        expect(BusinessDayConvention.PrecedingDay).toBe(5);
        expect(BusinessDayConvention.ModifiedPrecedingDay).toBe(6);
        expect(BusinessDayConvention.NearestDay).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BusinessDayConvention.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BusinessDayConvention.NotApplicable,
            BusinessDayConvention.None,
            BusinessDayConvention.FollowingDay,
            BusinessDayConvention.FloatingRateNote,
            BusinessDayConvention.ModifiedFollowingDay,
            BusinessDayConvention.PrecedingDay,
            BusinessDayConvention.ModifiedPrecedingDay,
            BusinessDayConvention.NearestDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BusinessDayConvention type', () => {
        const validate = (value: BusinessDayConvention) => {
            expect(Object.values(BusinessDayConvention)).toContain(value);
        };

        validate(BusinessDayConvention.NotApplicable);
        validate(BusinessDayConvention.None);
        validate(BusinessDayConvention.FollowingDay);
        validate(BusinessDayConvention.FloatingRateNote);
        validate(BusinessDayConvention.ModifiedFollowingDay);
        validate(BusinessDayConvention.PrecedingDay);
        validate(BusinessDayConvention.ModifiedPrecedingDay);
        validate(BusinessDayConvention.NearestDay);
    });

    test('should be immutable', () => {
        const ref = BusinessDayConvention;
        expect(() => {
            ref.NotApplicable = 10;
        }).toThrowError();
    });
});
