import { MiscFeeType } from '../../src/fieldtypes/MiscFeeType';

describe('MiscFeeType', () => {
    test('should have the correct values', () => {
        expect(MiscFeeType.Regulatory).toBe('1');
        expect(MiscFeeType.Tax).toBe('2');
        expect(MiscFeeType.LocalCommission).toBe('3');
        expect(MiscFeeType.ExchangeFees).toBe('4');
        expect(MiscFeeType.Stamp).toBe('5');
        expect(MiscFeeType.Levy).toBe('6');
        expect(MiscFeeType.Other).toBe('7');
        expect(MiscFeeType.Markup).toBe('8');
        expect(MiscFeeType.ConsumptionTax).toBe('9');
        expect(MiscFeeType.PerTransaction).toBe('10');
        expect(MiscFeeType.Conversion).toBe('11');
        expect(MiscFeeType.Agent).toBe('12');
        expect(MiscFeeType.TransferFee).toBe('13');
        expect(MiscFeeType.SecurityLending).toBe('14');
        expect(MiscFeeType.TradeReporting).toBe('15');
        expect(MiscFeeType.TaxOnPrincipalAmount).toBe('16');
        expect(MiscFeeType.TaxOnAccruedInterestAmount).toBe('17');
        expect(MiscFeeType.NewIssuanceFee).toBe('18');
        expect(MiscFeeType.ServiceFee).toBe('19');
        expect(MiscFeeType.OddLotFee).toBe('20');
        expect(MiscFeeType.AuctionFee).toBe('21');
        expect(MiscFeeType.ValueAddedTax).toBe('22');
        expect(MiscFeeType.SalesTax).toBe('23');
        expect(MiscFeeType.ExecutionFee).toBe('24');
        expect(MiscFeeType.OrderEntryFee).toBe('25');
        expect(MiscFeeType.OrderModificationFee).toBe('26');
        expect(MiscFeeType.OrdersCancellationFee).toBe('27');
        expect(MiscFeeType.MarketDataAccessFee).toBe('28');
        expect(MiscFeeType.MarketDataTerminalFee).toBe('29');
        expect(MiscFeeType.MarketDataVolumeFee).toBe('30');
        expect(MiscFeeType.ClearingFee).toBe('31');
        expect(MiscFeeType.SettlementFee).toBe('32');
        expect(MiscFeeType.Rebates).toBe('33');
        expect(MiscFeeType.Discounts).toBe('34');
        expect(MiscFeeType.Payments).toBe('35');
        expect(MiscFeeType.NonMonetaryPayments).toBe('36');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MiscFeeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MiscFeeType.Regulatory,
            MiscFeeType.Tax,
            MiscFeeType.LocalCommission,
            MiscFeeType.ExchangeFees,
            MiscFeeType.Stamp,
            MiscFeeType.Levy,
            MiscFeeType.Other,
            MiscFeeType.Markup,
            MiscFeeType.ConsumptionTax,
            MiscFeeType.PerTransaction,
            MiscFeeType.Conversion,
            MiscFeeType.Agent,
            MiscFeeType.TransferFee,
            MiscFeeType.SecurityLending,
            MiscFeeType.TradeReporting,
            MiscFeeType.TaxOnPrincipalAmount,
            MiscFeeType.TaxOnAccruedInterestAmount,
            MiscFeeType.NewIssuanceFee,
            MiscFeeType.ServiceFee,
            MiscFeeType.OddLotFee,
            MiscFeeType.AuctionFee,
            MiscFeeType.ValueAddedTax,
            MiscFeeType.SalesTax,
            MiscFeeType.ExecutionFee,
            MiscFeeType.OrderEntryFee,
            MiscFeeType.OrderModificationFee,
            MiscFeeType.OrdersCancellationFee,
            MiscFeeType.MarketDataAccessFee,
            MiscFeeType.MarketDataTerminalFee,
            MiscFeeType.MarketDataVolumeFee,
            MiscFeeType.ClearingFee,
            MiscFeeType.SettlementFee,
            MiscFeeType.Rebates,
            MiscFeeType.Discounts,
            MiscFeeType.Payments,
            MiscFeeType.NonMonetaryPayments,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MiscFeeType type', () => {
        const validate = (value: MiscFeeType) => {
            expect(Object.values(MiscFeeType)).toContain(value);
        };

        validate(MiscFeeType.Regulatory);
        validate(MiscFeeType.Tax);
        validate(MiscFeeType.LocalCommission);
        validate(MiscFeeType.ExchangeFees);
        validate(MiscFeeType.Stamp);
        validate(MiscFeeType.Levy);
        validate(MiscFeeType.Other);
        validate(MiscFeeType.Markup);
        validate(MiscFeeType.ConsumptionTax);
        validate(MiscFeeType.PerTransaction);
        validate(MiscFeeType.Conversion);
        validate(MiscFeeType.Agent);
        validate(MiscFeeType.TransferFee);
        validate(MiscFeeType.SecurityLending);
        validate(MiscFeeType.TradeReporting);
        validate(MiscFeeType.TaxOnPrincipalAmount);
        validate(MiscFeeType.TaxOnAccruedInterestAmount);
        validate(MiscFeeType.NewIssuanceFee);
        validate(MiscFeeType.ServiceFee);
        validate(MiscFeeType.OddLotFee);
        validate(MiscFeeType.AuctionFee);
        validate(MiscFeeType.ValueAddedTax);
        validate(MiscFeeType.SalesTax);
        validate(MiscFeeType.ExecutionFee);
        validate(MiscFeeType.OrderEntryFee);
        validate(MiscFeeType.OrderModificationFee);
        validate(MiscFeeType.OrdersCancellationFee);
        validate(MiscFeeType.MarketDataAccessFee);
        validate(MiscFeeType.MarketDataTerminalFee);
        validate(MiscFeeType.MarketDataVolumeFee);
        validate(MiscFeeType.ClearingFee);
        validate(MiscFeeType.SettlementFee);
        validate(MiscFeeType.Rebates);
        validate(MiscFeeType.Discounts);
        validate(MiscFeeType.Payments);
        validate(MiscFeeType.NonMonetaryPayments);
    });

    test('should be immutable', () => {
        const ref = MiscFeeType;
        expect(() => {
            ref.Regulatory = 10;
        }).toThrowError();
    });
});
