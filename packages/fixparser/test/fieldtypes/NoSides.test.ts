import { NoSides } from '../../src/fieldtypes/NoSides';

describe('NoSides', () => {
    test('should have the correct values', () => {
        expect(NoSides.OneSide).toBe(1);
        expect(NoSides.BothSides).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NoSides.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NoSides.OneSide, NoSides.BothSides];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NoSides type', () => {
        const validate = (value: NoSides) => {
            expect(Object.values(NoSides)).toContain(value);
        };

        validate(NoSides.OneSide);
        validate(NoSides.BothSides);
    });

    test('should be immutable', () => {
        const ref = NoSides;
        expect(() => {
            ref.OneSide = 10;
        }).toThrowError();
    });
});
