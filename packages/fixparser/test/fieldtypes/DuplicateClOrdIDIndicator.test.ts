import { DuplicateClOrdIDIndicator } from '../../src/fieldtypes/DuplicateClOrdIDIndicator';

describe('DuplicateClOrdIDIndicator', () => {
    test('should have the correct values', () => {
        expect(DuplicateClOrdIDIndicator.UniqueClOrdID).toBe('N');
        expect(DuplicateClOrdIDIndicator.DuplicateClOrdID).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DuplicateClOrdIDIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DuplicateClOrdIDIndicator.UniqueClOrdID, DuplicateClOrdIDIndicator.DuplicateClOrdID];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DuplicateClOrdIDIndicator type', () => {
        const validate = (value: DuplicateClOrdIDIndicator) => {
            expect(Object.values(DuplicateClOrdIDIndicator)).toContain(value);
        };

        validate(DuplicateClOrdIDIndicator.UniqueClOrdID);
        validate(DuplicateClOrdIDIndicator.DuplicateClOrdID);
    });

    test('should be immutable', () => {
        const ref = DuplicateClOrdIDIndicator;
        expect(() => {
            ref.UniqueClOrdID = 10;
        }).toThrowError();
    });
});
