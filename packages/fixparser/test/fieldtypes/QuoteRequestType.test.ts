import { QuoteRequestType } from '../../src/fieldtypes/QuoteRequestType';

describe('QuoteRequestType', () => {
    test('should have the correct values', () => {
        expect(QuoteRequestType.Manual).toBe(1);
        expect(QuoteRequestType.Automatic).toBe(2);
        expect(QuoteRequestType.ConfirmQuote).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [QuoteRequestType.Manual, QuoteRequestType.Automatic, QuoteRequestType.ConfirmQuote];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteRequestType type', () => {
        const validate = (value: QuoteRequestType) => {
            expect(Object.values(QuoteRequestType)).toContain(value);
        };

        validate(QuoteRequestType.Manual);
        validate(QuoteRequestType.Automatic);
        validate(QuoteRequestType.ConfirmQuote);
    });

    test('should be immutable', () => {
        const ref = QuoteRequestType;
        expect(() => {
            ref.Manual = 10;
        }).toThrowError();
    });
});
