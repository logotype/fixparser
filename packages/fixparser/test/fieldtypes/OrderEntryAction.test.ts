import { OrderEntryAction } from '../../src/fieldtypes/OrderEntryAction';

describe('OrderEntryAction', () => {
    test('should have the correct values', () => {
        expect(OrderEntryAction.Add).toBe('1');
        expect(OrderEntryAction.Modify).toBe('2');
        expect(OrderEntryAction.Delete).toBe('3');
        expect(OrderEntryAction.Suspend).toBe('4');
        expect(OrderEntryAction.Release).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderEntryAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderEntryAction.Add,
            OrderEntryAction.Modify,
            OrderEntryAction.Delete,
            OrderEntryAction.Suspend,
            OrderEntryAction.Release,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrderEntryAction type', () => {
        const validate = (value: OrderEntryAction) => {
            expect(Object.values(OrderEntryAction)).toContain(value);
        };

        validate(OrderEntryAction.Add);
        validate(OrderEntryAction.Modify);
        validate(OrderEntryAction.Delete);
        validate(OrderEntryAction.Suspend);
        validate(OrderEntryAction.Release);
    });

    test('should be immutable', () => {
        const ref = OrderEntryAction;
        expect(() => {
            ref.Add = 10;
        }).toThrowError();
    });
});
