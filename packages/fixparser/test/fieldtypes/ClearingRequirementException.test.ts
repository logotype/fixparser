import { ClearingRequirementException } from '../../src/fieldtypes/ClearingRequirementException';

describe('ClearingRequirementException', () => {
    test('should have the correct values', () => {
        expect(ClearingRequirementException.NoException).toBe(0);
        expect(ClearingRequirementException.Exception).toBe(1);
        expect(ClearingRequirementException.EndUserException).toBe(2);
        expect(ClearingRequirementException.InterAffiliateException).toBe(3);
        expect(ClearingRequirementException.TreasuryAffiliateException).toBe(4);
        expect(ClearingRequirementException.CooperativeException).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearingRequirementException.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ClearingRequirementException.NoException,
            ClearingRequirementException.Exception,
            ClearingRequirementException.EndUserException,
            ClearingRequirementException.InterAffiliateException,
            ClearingRequirementException.TreasuryAffiliateException,
            ClearingRequirementException.CooperativeException,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ClearingRequirementException type', () => {
        const validate = (value: ClearingRequirementException) => {
            expect(Object.values(ClearingRequirementException)).toContain(value);
        };

        validate(ClearingRequirementException.NoException);
        validate(ClearingRequirementException.Exception);
        validate(ClearingRequirementException.EndUserException);
        validate(ClearingRequirementException.InterAffiliateException);
        validate(ClearingRequirementException.TreasuryAffiliateException);
        validate(ClearingRequirementException.CooperativeException);
    });

    test('should be immutable', () => {
        const ref = ClearingRequirementException;
        expect(() => {
            ref.NoException = 10;
        }).toThrowError();
    });
});
