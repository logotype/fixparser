import { CouponType } from '../../src/fieldtypes/CouponType';

describe('CouponType', () => {
    test('should have the correct values', () => {
        expect(CouponType.Zero).toBe(0);
        expect(CouponType.FixedRate).toBe(1);
        expect(CouponType.FloatingRate).toBe(2);
        expect(CouponType.Structured).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CouponType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CouponType.Zero,
            CouponType.FixedRate,
            CouponType.FloatingRate,
            CouponType.Structured,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CouponType type', () => {
        const validate = (value: CouponType) => {
            expect(Object.values(CouponType)).toContain(value);
        };

        validate(CouponType.Zero);
        validate(CouponType.FixedRate);
        validate(CouponType.FloatingRate);
        validate(CouponType.Structured);
    });

    test('should be immutable', () => {
        const ref = CouponType;
        expect(() => {
            ref.Zero = 10;
        }).toThrowError();
    });
});
