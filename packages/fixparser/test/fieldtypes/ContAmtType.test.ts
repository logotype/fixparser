import { ContAmtType } from '../../src/fieldtypes/ContAmtType';

describe('ContAmtType', () => {
    test('should have the correct values', () => {
        expect(ContAmtType.CommissionAmount).toBe(1);
        expect(ContAmtType.CommissionPercent).toBe(2);
        expect(ContAmtType.InitialChargeAmount).toBe(3);
        expect(ContAmtType.InitialChargePercent).toBe(4);
        expect(ContAmtType.DiscountAmount).toBe(5);
        expect(ContAmtType.DiscountPercent).toBe(6);
        expect(ContAmtType.DilutionLevyAmount).toBe(7);
        expect(ContAmtType.DilutionLevyPercent).toBe(8);
        expect(ContAmtType.ExitChargeAmount).toBe(9);
        expect(ContAmtType.ExitChargePercent).toBe(10);
        expect(ContAmtType.FundBasedRenewalCommissionPercent).toBe(11);
        expect(ContAmtType.ProjectedFundValue).toBe(12);
        expect(ContAmtType.FundBasedRenewalCommissionOnOrder).toBe(13);
        expect(ContAmtType.FundBasedRenewalCommissionOnFund).toBe(14);
        expect(ContAmtType.NetSettlementAmount).toBe(15);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ContAmtType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ContAmtType.CommissionAmount,
            ContAmtType.CommissionPercent,
            ContAmtType.InitialChargeAmount,
            ContAmtType.InitialChargePercent,
            ContAmtType.DiscountAmount,
            ContAmtType.DiscountPercent,
            ContAmtType.DilutionLevyAmount,
            ContAmtType.DilutionLevyPercent,
            ContAmtType.ExitChargeAmount,
            ContAmtType.ExitChargePercent,
            ContAmtType.FundBasedRenewalCommissionPercent,
            ContAmtType.ProjectedFundValue,
            ContAmtType.FundBasedRenewalCommissionOnOrder,
            ContAmtType.FundBasedRenewalCommissionOnFund,
            ContAmtType.NetSettlementAmount,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ContAmtType type', () => {
        const validate = (value: ContAmtType) => {
            expect(Object.values(ContAmtType)).toContain(value);
        };

        validate(ContAmtType.CommissionAmount);
        validate(ContAmtType.CommissionPercent);
        validate(ContAmtType.InitialChargeAmount);
        validate(ContAmtType.InitialChargePercent);
        validate(ContAmtType.DiscountAmount);
        validate(ContAmtType.DiscountPercent);
        validate(ContAmtType.DilutionLevyAmount);
        validate(ContAmtType.DilutionLevyPercent);
        validate(ContAmtType.ExitChargeAmount);
        validate(ContAmtType.ExitChargePercent);
        validate(ContAmtType.FundBasedRenewalCommissionPercent);
        validate(ContAmtType.ProjectedFundValue);
        validate(ContAmtType.FundBasedRenewalCommissionOnOrder);
        validate(ContAmtType.FundBasedRenewalCommissionOnFund);
        validate(ContAmtType.NetSettlementAmount);
    });

    test('should be immutable', () => {
        const ref = ContAmtType;
        expect(() => {
            ref.CommissionAmount = 10;
        }).toThrowError();
    });
});
