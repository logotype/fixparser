import { ProtectionTermEventDayType } from '../../src/fieldtypes/ProtectionTermEventDayType';

describe('ProtectionTermEventDayType', () => {
    test('should have the correct values', () => {
        expect(ProtectionTermEventDayType.Business).toBe(0);
        expect(ProtectionTermEventDayType.Calendar).toBe(1);
        expect(ProtectionTermEventDayType.CommodityBusiness).toBe(2);
        expect(ProtectionTermEventDayType.CurrencyBusiness).toBe(3);
        expect(ProtectionTermEventDayType.ExchangeBusiness).toBe(4);
        expect(ProtectionTermEventDayType.ScheduledTradingDay).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProtectionTermEventDayType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProtectionTermEventDayType.Business,
            ProtectionTermEventDayType.Calendar,
            ProtectionTermEventDayType.CommodityBusiness,
            ProtectionTermEventDayType.CurrencyBusiness,
            ProtectionTermEventDayType.ExchangeBusiness,
            ProtectionTermEventDayType.ScheduledTradingDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProtectionTermEventDayType type', () => {
        const validate = (value: ProtectionTermEventDayType) => {
            expect(Object.values(ProtectionTermEventDayType)).toContain(value);
        };

        validate(ProtectionTermEventDayType.Business);
        validate(ProtectionTermEventDayType.Calendar);
        validate(ProtectionTermEventDayType.CommodityBusiness);
        validate(ProtectionTermEventDayType.CurrencyBusiness);
        validate(ProtectionTermEventDayType.ExchangeBusiness);
        validate(ProtectionTermEventDayType.ScheduledTradingDay);
    });

    test('should be immutable', () => {
        const ref = ProtectionTermEventDayType;
        expect(() => {
            ref.Business = 10;
        }).toThrowError();
    });
});
