import { LegSwapType } from '../../src/fieldtypes/LegSwapType';

describe('LegSwapType', () => {
    test('should have the correct values', () => {
        expect(LegSwapType.ParForPar).toBe(1);
        expect(LegSwapType.ModifiedDuration).toBe(2);
        expect(LegSwapType.Risk).toBe(4);
        expect(LegSwapType.Proceeds).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LegSwapType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LegSwapType.ParForPar,
            LegSwapType.ModifiedDuration,
            LegSwapType.Risk,
            LegSwapType.Proceeds,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LegSwapType type', () => {
        const validate = (value: LegSwapType) => {
            expect(Object.values(LegSwapType)).toContain(value);
        };

        validate(LegSwapType.ParForPar);
        validate(LegSwapType.ModifiedDuration);
        validate(LegSwapType.Risk);
        validate(LegSwapType.Proceeds);
    });

    test('should be immutable', () => {
        const ref = LegSwapType;
        expect(() => {
            ref.ParForPar = 10;
        }).toThrowError();
    });
});
