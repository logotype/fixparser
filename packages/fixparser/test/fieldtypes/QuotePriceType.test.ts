import { QuotePriceType } from '../../src/fieldtypes/QuotePriceType';

describe('QuotePriceType', () => {
    test('should have the correct values', () => {
        expect(QuotePriceType.Percent).toBe(1);
        expect(QuotePriceType.PerShare).toBe(2);
        expect(QuotePriceType.FixedAmount).toBe(3);
        expect(QuotePriceType.Discount).toBe(4);
        expect(QuotePriceType.Premium).toBe(5);
        expect(QuotePriceType.Spread).toBe(6);
        expect(QuotePriceType.TEDPrice).toBe(7);
        expect(QuotePriceType.TEDYield).toBe(8);
        expect(QuotePriceType.YieldSpread).toBe(9);
        expect(QuotePriceType.Yield).toBe(10);
        expect(QuotePriceType.PriceSpread).toBe(12);
        expect(QuotePriceType.ProductTicksInHalves).toBe(13);
        expect(QuotePriceType.ProductTicksInFourths).toBe(14);
        expect(QuotePriceType.ProductTicksInEighths).toBe(15);
        expect(QuotePriceType.ProductTicksInSixteenths).toBe(16);
        expect(QuotePriceType.ProductTicksInThirtySeconds).toBe(17);
        expect(QuotePriceType.ProductTicksInSixtyFourths).toBe(18);
        expect(QuotePriceType.ProductTicksInOneTwentyEighths).toBe(19);
        expect(QuotePriceType.NormalRateRepresentation).toBe(20);
        expect(QuotePriceType.InverseRateRepresentation).toBe(21);
        expect(QuotePriceType.BasisPoints).toBe(22);
        expect(QuotePriceType.UpFrontPoints).toBe(23);
        expect(QuotePriceType.InterestRate).toBe(24);
        expect(QuotePriceType.PercentageOfNotional).toBe(25);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuotePriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuotePriceType.Percent,
            QuotePriceType.PerShare,
            QuotePriceType.FixedAmount,
            QuotePriceType.Discount,
            QuotePriceType.Premium,
            QuotePriceType.Spread,
            QuotePriceType.TEDPrice,
            QuotePriceType.TEDYield,
            QuotePriceType.YieldSpread,
            QuotePriceType.Yield,
            QuotePriceType.PriceSpread,
            QuotePriceType.ProductTicksInHalves,
            QuotePriceType.ProductTicksInFourths,
            QuotePriceType.ProductTicksInEighths,
            QuotePriceType.ProductTicksInSixteenths,
            QuotePriceType.ProductTicksInThirtySeconds,
            QuotePriceType.ProductTicksInSixtyFourths,
            QuotePriceType.ProductTicksInOneTwentyEighths,
            QuotePriceType.NormalRateRepresentation,
            QuotePriceType.InverseRateRepresentation,
            QuotePriceType.BasisPoints,
            QuotePriceType.UpFrontPoints,
            QuotePriceType.InterestRate,
            QuotePriceType.PercentageOfNotional,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuotePriceType type', () => {
        const validate = (value: QuotePriceType) => {
            expect(Object.values(QuotePriceType)).toContain(value);
        };

        validate(QuotePriceType.Percent);
        validate(QuotePriceType.PerShare);
        validate(QuotePriceType.FixedAmount);
        validate(QuotePriceType.Discount);
        validate(QuotePriceType.Premium);
        validate(QuotePriceType.Spread);
        validate(QuotePriceType.TEDPrice);
        validate(QuotePriceType.TEDYield);
        validate(QuotePriceType.YieldSpread);
        validate(QuotePriceType.Yield);
        validate(QuotePriceType.PriceSpread);
        validate(QuotePriceType.ProductTicksInHalves);
        validate(QuotePriceType.ProductTicksInFourths);
        validate(QuotePriceType.ProductTicksInEighths);
        validate(QuotePriceType.ProductTicksInSixteenths);
        validate(QuotePriceType.ProductTicksInThirtySeconds);
        validate(QuotePriceType.ProductTicksInSixtyFourths);
        validate(QuotePriceType.ProductTicksInOneTwentyEighths);
        validate(QuotePriceType.NormalRateRepresentation);
        validate(QuotePriceType.InverseRateRepresentation);
        validate(QuotePriceType.BasisPoints);
        validate(QuotePriceType.UpFrontPoints);
        validate(QuotePriceType.InterestRate);
        validate(QuotePriceType.PercentageOfNotional);
    });

    test('should be immutable', () => {
        const ref = QuotePriceType;
        expect(() => {
            ref.Percent = 10;
        }).toThrowError();
    });
});
