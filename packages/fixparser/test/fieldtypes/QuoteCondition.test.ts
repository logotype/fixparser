import { QuoteCondition } from '../../src/fieldtypes/QuoteCondition';

describe('QuoteCondition', () => {
    test('should have the correct values', () => {
        expect(QuoteCondition.Open).toBe('A');
        expect(QuoteCondition.Closed).toBe('B');
        expect(QuoteCondition.ExchangeBest).toBe('C');
        expect(QuoteCondition.ConsolidatedBest).toBe('D');
        expect(QuoteCondition.Locked).toBe('E');
        expect(QuoteCondition.Crossed).toBe('F');
        expect(QuoteCondition.Depth).toBe('G');
        expect(QuoteCondition.FastTrading).toBe('H');
        expect(QuoteCondition.NonFirm).toBe('I');
        expect(QuoteCondition.Manual).toBe('L');
        expect(QuoteCondition.OutrightPrice).toBe('J');
        expect(QuoteCondition.ImpliedPrice).toBe('K');
        expect(QuoteCondition.DepthOnOffer).toBe('M');
        expect(QuoteCondition.DepthOnBid).toBe('N');
        expect(QuoteCondition.Closing).toBe('O');
        expect(QuoteCondition.NewsDissemination).toBe('P');
        expect(QuoteCondition.TradingRange).toBe('Q');
        expect(QuoteCondition.OrderInflux).toBe('R');
        expect(QuoteCondition.DueToRelated).toBe('S');
        expect(QuoteCondition.NewsPending).toBe('T');
        expect(QuoteCondition.AdditionalInfo).toBe('U');
        expect(QuoteCondition.AdditionalInfoDueToRelated).toBe('V');
        expect(QuoteCondition.Resume).toBe('W');
        expect(QuoteCondition.ViewOfCommon).toBe('X');
        expect(QuoteCondition.VolumeAlert).toBe('Y');
        expect(QuoteCondition.OrderImbalance).toBe('Z');
        expect(QuoteCondition.EquipmentChangeover).toBe('a');
        expect(QuoteCondition.NoOpen).toBe('b');
        expect(QuoteCondition.RegularETH).toBe('c');
        expect(QuoteCondition.AutomaticExecution).toBe('d');
        expect(QuoteCondition.AutomaticExecutionETH).toBe('e');
        expect(QuoteCondition.FastMarketETH).toBe('f');
        expect(QuoteCondition.InactiveETH).toBe('g');
        expect(QuoteCondition.Rotation).toBe('h');
        expect(QuoteCondition.RotationETH).toBe('i');
        expect(QuoteCondition.Halt).toBe('j');
        expect(QuoteCondition.HaltETH).toBe('k');
        expect(QuoteCondition.DueToNewsDissemination).toBe('l');
        expect(QuoteCondition.DueToNewsPending).toBe('m');
        expect(QuoteCondition.TradingResume).toBe('n');
        expect(QuoteCondition.OutOfSequence).toBe('o');
        expect(QuoteCondition.BidSpecialist).toBe('p');
        expect(QuoteCondition.OfferSpecialist).toBe('q');
        expect(QuoteCondition.BidOfferSpecialist).toBe('r');
        expect(QuoteCondition.EndOfDaySAM).toBe('s');
        expect(QuoteCondition.ForbiddenSAM).toBe('t');
        expect(QuoteCondition.FrozenSAM).toBe('u');
        expect(QuoteCondition.PreOpeningSAM).toBe('v');
        expect(QuoteCondition.OpeningSAM).toBe('w');
        expect(QuoteCondition.OpenSAM).toBe('x');
        expect(QuoteCondition.SurveillanceSAM).toBe('y');
        expect(QuoteCondition.SuspendedSAM).toBe('z');
        expect(QuoteCondition.ReservedSAM).toBe('0');
        expect(QuoteCondition.NoActiveSAM).toBe('1');
        expect(QuoteCondition.Restricted).toBe('2');
        expect(QuoteCondition.RestOfBookVWAP).toBe('3');
        expect(QuoteCondition.BetterPricesInConditionalOrders).toBe('4');
        expect(QuoteCondition.MedianPrice).toBe('5');
        expect(QuoteCondition.FullCurve).toBe('6');
        expect(QuoteCondition.FlatCurve).toBe('7');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteCondition.Open,
            QuoteCondition.Closed,
            QuoteCondition.ExchangeBest,
            QuoteCondition.ConsolidatedBest,
            QuoteCondition.Locked,
            QuoteCondition.Crossed,
            QuoteCondition.Depth,
            QuoteCondition.FastTrading,
            QuoteCondition.NonFirm,
            QuoteCondition.Manual,
            QuoteCondition.OutrightPrice,
            QuoteCondition.ImpliedPrice,
            QuoteCondition.DepthOnOffer,
            QuoteCondition.DepthOnBid,
            QuoteCondition.Closing,
            QuoteCondition.NewsDissemination,
            QuoteCondition.TradingRange,
            QuoteCondition.OrderInflux,
            QuoteCondition.DueToRelated,
            QuoteCondition.NewsPending,
            QuoteCondition.AdditionalInfo,
            QuoteCondition.AdditionalInfoDueToRelated,
            QuoteCondition.Resume,
            QuoteCondition.ViewOfCommon,
            QuoteCondition.VolumeAlert,
            QuoteCondition.OrderImbalance,
            QuoteCondition.EquipmentChangeover,
            QuoteCondition.NoOpen,
            QuoteCondition.RegularETH,
            QuoteCondition.AutomaticExecution,
            QuoteCondition.AutomaticExecutionETH,
            QuoteCondition.FastMarketETH,
            QuoteCondition.InactiveETH,
            QuoteCondition.Rotation,
            QuoteCondition.RotationETH,
            QuoteCondition.Halt,
            QuoteCondition.HaltETH,
            QuoteCondition.DueToNewsDissemination,
            QuoteCondition.DueToNewsPending,
            QuoteCondition.TradingResume,
            QuoteCondition.OutOfSequence,
            QuoteCondition.BidSpecialist,
            QuoteCondition.OfferSpecialist,
            QuoteCondition.BidOfferSpecialist,
            QuoteCondition.EndOfDaySAM,
            QuoteCondition.ForbiddenSAM,
            QuoteCondition.FrozenSAM,
            QuoteCondition.PreOpeningSAM,
            QuoteCondition.OpeningSAM,
            QuoteCondition.OpenSAM,
            QuoteCondition.SurveillanceSAM,
            QuoteCondition.SuspendedSAM,
            QuoteCondition.ReservedSAM,
            QuoteCondition.NoActiveSAM,
            QuoteCondition.Restricted,
            QuoteCondition.RestOfBookVWAP,
            QuoteCondition.BetterPricesInConditionalOrders,
            QuoteCondition.MedianPrice,
            QuoteCondition.FullCurve,
            QuoteCondition.FlatCurve,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for QuoteCondition type', () => {
        const validate = (value: QuoteCondition) => {
            expect(Object.values(QuoteCondition)).toContain(value);
        };

        validate(QuoteCondition.Open);
        validate(QuoteCondition.Closed);
        validate(QuoteCondition.ExchangeBest);
        validate(QuoteCondition.ConsolidatedBest);
        validate(QuoteCondition.Locked);
        validate(QuoteCondition.Crossed);
        validate(QuoteCondition.Depth);
        validate(QuoteCondition.FastTrading);
        validate(QuoteCondition.NonFirm);
        validate(QuoteCondition.Manual);
        validate(QuoteCondition.OutrightPrice);
        validate(QuoteCondition.ImpliedPrice);
        validate(QuoteCondition.DepthOnOffer);
        validate(QuoteCondition.DepthOnBid);
        validate(QuoteCondition.Closing);
        validate(QuoteCondition.NewsDissemination);
        validate(QuoteCondition.TradingRange);
        validate(QuoteCondition.OrderInflux);
        validate(QuoteCondition.DueToRelated);
        validate(QuoteCondition.NewsPending);
        validate(QuoteCondition.AdditionalInfo);
        validate(QuoteCondition.AdditionalInfoDueToRelated);
        validate(QuoteCondition.Resume);
        validate(QuoteCondition.ViewOfCommon);
        validate(QuoteCondition.VolumeAlert);
        validate(QuoteCondition.OrderImbalance);
        validate(QuoteCondition.EquipmentChangeover);
        validate(QuoteCondition.NoOpen);
        validate(QuoteCondition.RegularETH);
        validate(QuoteCondition.AutomaticExecution);
        validate(QuoteCondition.AutomaticExecutionETH);
        validate(QuoteCondition.FastMarketETH);
        validate(QuoteCondition.InactiveETH);
        validate(QuoteCondition.Rotation);
        validate(QuoteCondition.RotationETH);
        validate(QuoteCondition.Halt);
        validate(QuoteCondition.HaltETH);
        validate(QuoteCondition.DueToNewsDissemination);
        validate(QuoteCondition.DueToNewsPending);
        validate(QuoteCondition.TradingResume);
        validate(QuoteCondition.OutOfSequence);
        validate(QuoteCondition.BidSpecialist);
        validate(QuoteCondition.OfferSpecialist);
        validate(QuoteCondition.BidOfferSpecialist);
        validate(QuoteCondition.EndOfDaySAM);
        validate(QuoteCondition.ForbiddenSAM);
        validate(QuoteCondition.FrozenSAM);
        validate(QuoteCondition.PreOpeningSAM);
        validate(QuoteCondition.OpeningSAM);
        validate(QuoteCondition.OpenSAM);
        validate(QuoteCondition.SurveillanceSAM);
        validate(QuoteCondition.SuspendedSAM);
        validate(QuoteCondition.ReservedSAM);
        validate(QuoteCondition.NoActiveSAM);
        validate(QuoteCondition.Restricted);
        validate(QuoteCondition.RestOfBookVWAP);
        validate(QuoteCondition.BetterPricesInConditionalOrders);
        validate(QuoteCondition.MedianPrice);
        validate(QuoteCondition.FullCurve);
        validate(QuoteCondition.FlatCurve);
    });

    test('should be immutable', () => {
        const ref = QuoteCondition;
        expect(() => {
            ref.Open = 10;
        }).toThrowError();
    });
});
