import { MDEntryType } from '../../src/fieldtypes/MDEntryType';

describe('MDEntryType', () => {
    test('should have the correct values', () => {
        expect(MDEntryType.Bid).toBe('0');
        expect(MDEntryType.Offer).toBe('1');
        expect(MDEntryType.Trade).toBe('2');
        expect(MDEntryType.IndexValue).toBe('3');
        expect(MDEntryType.OpeningPrice).toBe('4');
        expect(MDEntryType.ClosingPrice).toBe('5');
        expect(MDEntryType.SettlementPrice).toBe('6');
        expect(MDEntryType.TradingSessionHighPrice).toBe('7');
        expect(MDEntryType.TradingSessionLowPrice).toBe('8');
        expect(MDEntryType.VWAP).toBe('9');
        expect(MDEntryType.Imbalance).toBe('A');
        expect(MDEntryType.TradeVolume).toBe('B');
        expect(MDEntryType.OpenInterest).toBe('C');
        expect(MDEntryType.CompositeUnderlyingPrice).toBe('D');
        expect(MDEntryType.SimulatedSellPrice).toBe('E');
        expect(MDEntryType.SimulatedBuyPrice).toBe('F');
        expect(MDEntryType.MarginRate).toBe('G');
        expect(MDEntryType.MidPrice).toBe('H');
        expect(MDEntryType.EmptyBook).toBe('J');
        expect(MDEntryType.SettleHighPrice).toBe('K');
        expect(MDEntryType.SettleLowPrice).toBe('L');
        expect(MDEntryType.PriorSettlePrice).toBe('M');
        expect(MDEntryType.SessionHighBid).toBe('N');
        expect(MDEntryType.SessionLowOffer).toBe('O');
        expect(MDEntryType.EarlyPrices).toBe('P');
        expect(MDEntryType.AuctionClearingPrice).toBe('Q');
        expect(MDEntryType.SwapValueFactor).toBe('S');
        expect(MDEntryType.DailyValueAdjustmentForLongPositions).toBe('R');
        expect(MDEntryType.CumulativeValueAdjustmentForLongPositions).toBe('T');
        expect(MDEntryType.DailyValueAdjustmentForShortPositions).toBe('U');
        expect(MDEntryType.CumulativeValueAdjustmentForShortPositions).toBe('V');
        expect(MDEntryType.FixingPrice).toBe('W');
        expect(MDEntryType.CashRate).toBe('X');
        expect(MDEntryType.RecoveryRate).toBe('Y');
        expect(MDEntryType.RecoveryRateForLong).toBe('Z');
        expect(MDEntryType.RecoveryRateForShort).toBe('a');
        expect(MDEntryType.MarketBid).toBe('b');
        expect(MDEntryType.MarketOffer).toBe('c');
        expect(MDEntryType.ShortSaleMinPrice).toBe('d');
        expect(MDEntryType.PreviousClosingPrice).toBe('e');
        expect(MDEntryType.ThresholdLimitPriceBanding).toBe('g');
        expect(MDEntryType.DailyFinancingValue).toBe('h');
        expect(MDEntryType.AccruedFinancingValue).toBe('i');
        expect(MDEntryType.TWAP).toBe('t');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDEntryType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDEntryType.Bid,
            MDEntryType.Offer,
            MDEntryType.Trade,
            MDEntryType.IndexValue,
            MDEntryType.OpeningPrice,
            MDEntryType.ClosingPrice,
            MDEntryType.SettlementPrice,
            MDEntryType.TradingSessionHighPrice,
            MDEntryType.TradingSessionLowPrice,
            MDEntryType.VWAP,
            MDEntryType.Imbalance,
            MDEntryType.TradeVolume,
            MDEntryType.OpenInterest,
            MDEntryType.CompositeUnderlyingPrice,
            MDEntryType.SimulatedSellPrice,
            MDEntryType.SimulatedBuyPrice,
            MDEntryType.MarginRate,
            MDEntryType.MidPrice,
            MDEntryType.EmptyBook,
            MDEntryType.SettleHighPrice,
            MDEntryType.SettleLowPrice,
            MDEntryType.PriorSettlePrice,
            MDEntryType.SessionHighBid,
            MDEntryType.SessionLowOffer,
            MDEntryType.EarlyPrices,
            MDEntryType.AuctionClearingPrice,
            MDEntryType.SwapValueFactor,
            MDEntryType.DailyValueAdjustmentForLongPositions,
            MDEntryType.CumulativeValueAdjustmentForLongPositions,
            MDEntryType.DailyValueAdjustmentForShortPositions,
            MDEntryType.CumulativeValueAdjustmentForShortPositions,
            MDEntryType.FixingPrice,
            MDEntryType.CashRate,
            MDEntryType.RecoveryRate,
            MDEntryType.RecoveryRateForLong,
            MDEntryType.RecoveryRateForShort,
            MDEntryType.MarketBid,
            MDEntryType.MarketOffer,
            MDEntryType.ShortSaleMinPrice,
            MDEntryType.PreviousClosingPrice,
            MDEntryType.ThresholdLimitPriceBanding,
            MDEntryType.DailyFinancingValue,
            MDEntryType.AccruedFinancingValue,
            MDEntryType.TWAP,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MDEntryType type', () => {
        const validate = (value: MDEntryType) => {
            expect(Object.values(MDEntryType)).toContain(value);
        };

        validate(MDEntryType.Bid);
        validate(MDEntryType.Offer);
        validate(MDEntryType.Trade);
        validate(MDEntryType.IndexValue);
        validate(MDEntryType.OpeningPrice);
        validate(MDEntryType.ClosingPrice);
        validate(MDEntryType.SettlementPrice);
        validate(MDEntryType.TradingSessionHighPrice);
        validate(MDEntryType.TradingSessionLowPrice);
        validate(MDEntryType.VWAP);
        validate(MDEntryType.Imbalance);
        validate(MDEntryType.TradeVolume);
        validate(MDEntryType.OpenInterest);
        validate(MDEntryType.CompositeUnderlyingPrice);
        validate(MDEntryType.SimulatedSellPrice);
        validate(MDEntryType.SimulatedBuyPrice);
        validate(MDEntryType.MarginRate);
        validate(MDEntryType.MidPrice);
        validate(MDEntryType.EmptyBook);
        validate(MDEntryType.SettleHighPrice);
        validate(MDEntryType.SettleLowPrice);
        validate(MDEntryType.PriorSettlePrice);
        validate(MDEntryType.SessionHighBid);
        validate(MDEntryType.SessionLowOffer);
        validate(MDEntryType.EarlyPrices);
        validate(MDEntryType.AuctionClearingPrice);
        validate(MDEntryType.SwapValueFactor);
        validate(MDEntryType.DailyValueAdjustmentForLongPositions);
        validate(MDEntryType.CumulativeValueAdjustmentForLongPositions);
        validate(MDEntryType.DailyValueAdjustmentForShortPositions);
        validate(MDEntryType.CumulativeValueAdjustmentForShortPositions);
        validate(MDEntryType.FixingPrice);
        validate(MDEntryType.CashRate);
        validate(MDEntryType.RecoveryRate);
        validate(MDEntryType.RecoveryRateForLong);
        validate(MDEntryType.RecoveryRateForShort);
        validate(MDEntryType.MarketBid);
        validate(MDEntryType.MarketOffer);
        validate(MDEntryType.ShortSaleMinPrice);
        validate(MDEntryType.PreviousClosingPrice);
        validate(MDEntryType.ThresholdLimitPriceBanding);
        validate(MDEntryType.DailyFinancingValue);
        validate(MDEntryType.AccruedFinancingValue);
        validate(MDEntryType.TWAP);
    });

    test('should be immutable', () => {
        const ref = MDEntryType;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
