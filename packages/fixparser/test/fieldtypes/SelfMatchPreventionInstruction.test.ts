import { SelfMatchPreventionInstruction } from '../../src/fieldtypes/SelfMatchPreventionInstruction';

describe('SelfMatchPreventionInstruction', () => {
    test('should have the correct values', () => {
        expect(SelfMatchPreventionInstruction.CancelAggressive).toBe(1);
        expect(SelfMatchPreventionInstruction.CancelPassive).toBe(2);
        expect(SelfMatchPreventionInstruction.CancelAggressivePassive).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SelfMatchPreventionInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SelfMatchPreventionInstruction.CancelAggressive,
            SelfMatchPreventionInstruction.CancelPassive,
            SelfMatchPreventionInstruction.CancelAggressivePassive,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SelfMatchPreventionInstruction type', () => {
        const validate = (value: SelfMatchPreventionInstruction) => {
            expect(Object.values(SelfMatchPreventionInstruction)).toContain(value);
        };

        validate(SelfMatchPreventionInstruction.CancelAggressive);
        validate(SelfMatchPreventionInstruction.CancelPassive);
        validate(SelfMatchPreventionInstruction.CancelAggressivePassive);
    });

    test('should be immutable', () => {
        const ref = SelfMatchPreventionInstruction;
        expect(() => {
            ref.CancelAggressive = 10;
        }).toThrowError();
    });
});
