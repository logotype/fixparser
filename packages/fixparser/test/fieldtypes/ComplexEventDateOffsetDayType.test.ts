import { ComplexEventDateOffsetDayType } from '../../src/fieldtypes/ComplexEventDateOffsetDayType';

describe('ComplexEventDateOffsetDayType', () => {
    test('should have the correct values', () => {
        expect(ComplexEventDateOffsetDayType.Business).toBe(0);
        expect(ComplexEventDateOffsetDayType.Calendar).toBe(1);
        expect(ComplexEventDateOffsetDayType.CommodityBusiness).toBe(2);
        expect(ComplexEventDateOffsetDayType.CurrencyBusiness).toBe(3);
        expect(ComplexEventDateOffsetDayType.ExchangeBusiness).toBe(4);
        expect(ComplexEventDateOffsetDayType.ScheduledTradingDay).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventDateOffsetDayType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventDateOffsetDayType.Business,
            ComplexEventDateOffsetDayType.Calendar,
            ComplexEventDateOffsetDayType.CommodityBusiness,
            ComplexEventDateOffsetDayType.CurrencyBusiness,
            ComplexEventDateOffsetDayType.ExchangeBusiness,
            ComplexEventDateOffsetDayType.ScheduledTradingDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventDateOffsetDayType type', () => {
        const validate = (value: ComplexEventDateOffsetDayType) => {
            expect(Object.values(ComplexEventDateOffsetDayType)).toContain(value);
        };

        validate(ComplexEventDateOffsetDayType.Business);
        validate(ComplexEventDateOffsetDayType.Calendar);
        validate(ComplexEventDateOffsetDayType.CommodityBusiness);
        validate(ComplexEventDateOffsetDayType.CurrencyBusiness);
        validate(ComplexEventDateOffsetDayType.ExchangeBusiness);
        validate(ComplexEventDateOffsetDayType.ScheduledTradingDay);
    });

    test('should be immutable', () => {
        const ref = ComplexEventDateOffsetDayType;
        expect(() => {
            ref.Business = 10;
        }).toThrowError();
    });
});
