import { SettlInstTransType } from '../../src/fieldtypes/SettlInstTransType';

describe('SettlInstTransType', () => {
    test('should have the correct values', () => {
        expect(SettlInstTransType.New).toBe('N');
        expect(SettlInstTransType.Cancel).toBe('C');
        expect(SettlInstTransType.Replace).toBe('R');
        expect(SettlInstTransType.Restate).toBe('T');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlInstTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlInstTransType.New,
            SettlInstTransType.Cancel,
            SettlInstTransType.Replace,
            SettlInstTransType.Restate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlInstTransType type', () => {
        const validate = (value: SettlInstTransType) => {
            expect(Object.values(SettlInstTransType)).toContain(value);
        };

        validate(SettlInstTransType.New);
        validate(SettlInstTransType.Cancel);
        validate(SettlInstTransType.Replace);
        validate(SettlInstTransType.Restate);
    });

    test('should be immutable', () => {
        const ref = SettlInstTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
