import { RegulatoryTradeIDScope } from '../../src/fieldtypes/RegulatoryTradeIDScope';

describe('RegulatoryTradeIDScope', () => {
    test('should have the correct values', () => {
        expect(RegulatoryTradeIDScope.ClearingMember).toBe(1);
        expect(RegulatoryTradeIDScope.Client).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegulatoryTradeIDScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RegulatoryTradeIDScope.ClearingMember, RegulatoryTradeIDScope.Client];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegulatoryTradeIDScope type', () => {
        const validate = (value: RegulatoryTradeIDScope) => {
            expect(Object.values(RegulatoryTradeIDScope)).toContain(value);
        };

        validate(RegulatoryTradeIDScope.ClearingMember);
        validate(RegulatoryTradeIDScope.Client);
    });

    test('should be immutable', () => {
        const ref = RegulatoryTradeIDScope;
        expect(() => {
            ref.ClearingMember = 10;
        }).toThrowError();
    });
});
