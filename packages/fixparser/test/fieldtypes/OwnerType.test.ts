import { OwnerType } from '../../src/fieldtypes/OwnerType';

describe('OwnerType', () => {
    test('should have the correct values', () => {
        expect(OwnerType.IndividualInvestor).toBe(1);
        expect(OwnerType.PublicCompany).toBe(2);
        expect(OwnerType.PrivateCompany).toBe(3);
        expect(OwnerType.IndividualTrustee).toBe(4);
        expect(OwnerType.CompanyTrustee).toBe(5);
        expect(OwnerType.PensionPlan).toBe(6);
        expect(OwnerType.CustodianUnderGiftsToMinorsAct).toBe(7);
        expect(OwnerType.Trusts).toBe(8);
        expect(OwnerType.Fiduciaries).toBe(9);
        expect(OwnerType.NetworkingSubAccount).toBe(10);
        expect(OwnerType.NonProfitOrganization).toBe(11);
        expect(OwnerType.CorporateBody).toBe(12);
        expect(OwnerType.Nominee).toBe(13);
        expect(OwnerType.InstitutionalCustomer).toBe(14);
        expect(OwnerType.Combined).toBe(15);
        expect(OwnerType.MemberFirmEmployee).toBe(16);
        expect(OwnerType.MarketMakingAccount).toBe(17);
        expect(OwnerType.ProprietaryAccount).toBe(18);
        expect(OwnerType.NonbrokerDealer).toBe(19);
        expect(OwnerType.UnknownBeneficialOwnerType).toBe(20);
        expect(OwnerType.FirmsErrorAccount).toBe(21);
        expect(OwnerType.FirmAgencyAveragePriceAccount).toBe(22);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OwnerType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OwnerType.IndividualInvestor,
            OwnerType.PublicCompany,
            OwnerType.PrivateCompany,
            OwnerType.IndividualTrustee,
            OwnerType.CompanyTrustee,
            OwnerType.PensionPlan,
            OwnerType.CustodianUnderGiftsToMinorsAct,
            OwnerType.Trusts,
            OwnerType.Fiduciaries,
            OwnerType.NetworkingSubAccount,
            OwnerType.NonProfitOrganization,
            OwnerType.CorporateBody,
            OwnerType.Nominee,
            OwnerType.InstitutionalCustomer,
            OwnerType.Combined,
            OwnerType.MemberFirmEmployee,
            OwnerType.MarketMakingAccount,
            OwnerType.ProprietaryAccount,
            OwnerType.NonbrokerDealer,
            OwnerType.UnknownBeneficialOwnerType,
            OwnerType.FirmsErrorAccount,
            OwnerType.FirmAgencyAveragePriceAccount,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OwnerType type', () => {
        const validate = (value: OwnerType) => {
            expect(Object.values(OwnerType)).toContain(value);
        };

        validate(OwnerType.IndividualInvestor);
        validate(OwnerType.PublicCompany);
        validate(OwnerType.PrivateCompany);
        validate(OwnerType.IndividualTrustee);
        validate(OwnerType.CompanyTrustee);
        validate(OwnerType.PensionPlan);
        validate(OwnerType.CustodianUnderGiftsToMinorsAct);
        validate(OwnerType.Trusts);
        validate(OwnerType.Fiduciaries);
        validate(OwnerType.NetworkingSubAccount);
        validate(OwnerType.NonProfitOrganization);
        validate(OwnerType.CorporateBody);
        validate(OwnerType.Nominee);
        validate(OwnerType.InstitutionalCustomer);
        validate(OwnerType.Combined);
        validate(OwnerType.MemberFirmEmployee);
        validate(OwnerType.MarketMakingAccount);
        validate(OwnerType.ProprietaryAccount);
        validate(OwnerType.NonbrokerDealer);
        validate(OwnerType.UnknownBeneficialOwnerType);
        validate(OwnerType.FirmsErrorAccount);
        validate(OwnerType.FirmAgencyAveragePriceAccount);
    });

    test('should be immutable', () => {
        const ref = OwnerType;
        expect(() => {
            ref.IndividualInvestor = 10;
        }).toThrowError();
    });
});
