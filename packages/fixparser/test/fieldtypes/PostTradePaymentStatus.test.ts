import { PostTradePaymentStatus } from '../../src/fieldtypes/PostTradePaymentStatus';

describe('PostTradePaymentStatus', () => {
    test('should have the correct values', () => {
        expect(PostTradePaymentStatus.New).toBe(0);
        expect(PostTradePaymentStatus.Initiated).toBe(1);
        expect(PostTradePaymentStatus.Pending).toBe(2);
        expect(PostTradePaymentStatus.Confirmed).toBe(3);
        expect(PostTradePaymentStatus.Rejected).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PostTradePaymentStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PostTradePaymentStatus.New,
            PostTradePaymentStatus.Initiated,
            PostTradePaymentStatus.Pending,
            PostTradePaymentStatus.Confirmed,
            PostTradePaymentStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PostTradePaymentStatus type', () => {
        const validate = (value: PostTradePaymentStatus) => {
            expect(Object.values(PostTradePaymentStatus)).toContain(value);
        };

        validate(PostTradePaymentStatus.New);
        validate(PostTradePaymentStatus.Initiated);
        validate(PostTradePaymentStatus.Pending);
        validate(PostTradePaymentStatus.Confirmed);
        validate(PostTradePaymentStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = PostTradePaymentStatus;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
