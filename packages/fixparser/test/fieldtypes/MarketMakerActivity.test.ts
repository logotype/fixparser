import { MarketMakerActivity } from '../../src/fieldtypes/MarketMakerActivity';

describe('MarketMakerActivity', () => {
    test('should have the correct values', () => {
        expect(MarketMakerActivity.NoParticipation).toBe(0);
        expect(MarketMakerActivity.BuyParticipation).toBe(1);
        expect(MarketMakerActivity.SellParticipation).toBe(2);
        expect(MarketMakerActivity.BothBuyAndSellParticipation).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketMakerActivity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketMakerActivity.NoParticipation,
            MarketMakerActivity.BuyParticipation,
            MarketMakerActivity.SellParticipation,
            MarketMakerActivity.BothBuyAndSellParticipation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketMakerActivity type', () => {
        const validate = (value: MarketMakerActivity) => {
            expect(Object.values(MarketMakerActivity)).toContain(value);
        };

        validate(MarketMakerActivity.NoParticipation);
        validate(MarketMakerActivity.BuyParticipation);
        validate(MarketMakerActivity.SellParticipation);
        validate(MarketMakerActivity.BothBuyAndSellParticipation);
    });

    test('should be immutable', () => {
        const ref = MarketMakerActivity;
        expect(() => {
            ref.NoParticipation = 10;
        }).toThrowError();
    });
});
