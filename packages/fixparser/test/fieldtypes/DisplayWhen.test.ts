import { DisplayWhen } from '../../src/fieldtypes/DisplayWhen';

describe('DisplayWhen', () => {
    test('should have the correct values', () => {
        expect(DisplayWhen.Immediate).toBe('1');
        expect(DisplayWhen.Exhaust).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DisplayWhen.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DisplayWhen.Immediate, DisplayWhen.Exhaust];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DisplayWhen type', () => {
        const validate = (value: DisplayWhen) => {
            expect(Object.values(DisplayWhen)).toContain(value);
        };

        validate(DisplayWhen.Immediate);
        validate(DisplayWhen.Exhaust);
    });

    test('should be immutable', () => {
        const ref = DisplayWhen;
        expect(() => {
            ref.Immediate = 10;
        }).toThrowError();
    });
});
