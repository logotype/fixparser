import { AllocationRollupInstruction } from '../../src/fieldtypes/AllocationRollupInstruction';

describe('AllocationRollupInstruction', () => {
    test('should have the correct values', () => {
        expect(AllocationRollupInstruction.Rollup).toBe(0);
        expect(AllocationRollupInstruction.DoNotRollUp).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocationRollupInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AllocationRollupInstruction.Rollup, AllocationRollupInstruction.DoNotRollUp];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocationRollupInstruction type', () => {
        const validate = (value: AllocationRollupInstruction) => {
            expect(Object.values(AllocationRollupInstruction)).toContain(value);
        };

        validate(AllocationRollupInstruction.Rollup);
        validate(AllocationRollupInstruction.DoNotRollUp);
    });

    test('should be immutable', () => {
        const ref = AllocationRollupInstruction;
        expect(() => {
            ref.Rollup = 10;
        }).toThrowError();
    });
});
