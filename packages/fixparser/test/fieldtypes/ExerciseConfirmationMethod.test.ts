import { ExerciseConfirmationMethod } from '../../src/fieldtypes/ExerciseConfirmationMethod';

describe('ExerciseConfirmationMethod', () => {
    test('should have the correct values', () => {
        expect(ExerciseConfirmationMethod.NotRequired).toBe(0);
        expect(ExerciseConfirmationMethod.NonElectronic).toBe(1);
        expect(ExerciseConfirmationMethod.Electronic).toBe(2);
        expect(ExerciseConfirmationMethod.Unknown).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExerciseConfirmationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExerciseConfirmationMethod.NotRequired,
            ExerciseConfirmationMethod.NonElectronic,
            ExerciseConfirmationMethod.Electronic,
            ExerciseConfirmationMethod.Unknown,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExerciseConfirmationMethod type', () => {
        const validate = (value: ExerciseConfirmationMethod) => {
            expect(Object.values(ExerciseConfirmationMethod)).toContain(value);
        };

        validate(ExerciseConfirmationMethod.NotRequired);
        validate(ExerciseConfirmationMethod.NonElectronic);
        validate(ExerciseConfirmationMethod.Electronic);
        validate(ExerciseConfirmationMethod.Unknown);
    });

    test('should be immutable', () => {
        const ref = ExerciseConfirmationMethod;
        expect(() => {
            ref.NotRequired = 10;
        }).toThrowError();
    });
});
