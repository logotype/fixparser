import { DKReason } from '../../src/fieldtypes/DKReason';

describe('DKReason', () => {
    test('should have the correct values', () => {
        expect(DKReason.UnknownSymbol).toBe('A');
        expect(DKReason.WrongSide).toBe('B');
        expect(DKReason.QuantityExceedsOrder).toBe('C');
        expect(DKReason.NoMatchingOrder).toBe('D');
        expect(DKReason.PriceExceedsLimit).toBe('E');
        expect(DKReason.CalculationDifference).toBe('F');
        expect(DKReason.NoMatchingExecutionReport).toBe('G');
        expect(DKReason.Other).toBe('Z');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DKReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DKReason.UnknownSymbol,
            DKReason.WrongSide,
            DKReason.QuantityExceedsOrder,
            DKReason.NoMatchingOrder,
            DKReason.PriceExceedsLimit,
            DKReason.CalculationDifference,
            DKReason.NoMatchingExecutionReport,
            DKReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DKReason type', () => {
        const validate = (value: DKReason) => {
            expect(Object.values(DKReason)).toContain(value);
        };

        validate(DKReason.UnknownSymbol);
        validate(DKReason.WrongSide);
        validate(DKReason.QuantityExceedsOrder);
        validate(DKReason.NoMatchingOrder);
        validate(DKReason.PriceExceedsLimit);
        validate(DKReason.CalculationDifference);
        validate(DKReason.NoMatchingExecutionReport);
        validate(DKReason.Other);
    });

    test('should be immutable', () => {
        const ref = DKReason;
        expect(() => {
            ref.UnknownSymbol = 10;
        }).toThrowError();
    });
});
