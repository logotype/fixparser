import { MatchExceptionToleranceValueType } from '../../src/fieldtypes/MatchExceptionToleranceValueType';

describe('MatchExceptionToleranceValueType', () => {
    test('should have the correct values', () => {
        expect(MatchExceptionToleranceValueType.FixedAmount).toBe(1);
        expect(MatchExceptionToleranceValueType.Percentage).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchExceptionToleranceValueType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MatchExceptionToleranceValueType.FixedAmount,
            MatchExceptionToleranceValueType.Percentage,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MatchExceptionToleranceValueType type', () => {
        const validate = (value: MatchExceptionToleranceValueType) => {
            expect(Object.values(MatchExceptionToleranceValueType)).toContain(value);
        };

        validate(MatchExceptionToleranceValueType.FixedAmount);
        validate(MatchExceptionToleranceValueType.Percentage);
    });

    test('should be immutable', () => {
        const ref = MatchExceptionToleranceValueType;
        expect(() => {
            ref.FixedAmount = 10;
        }).toThrowError();
    });
});
