import { RelatedPositionIDSource } from '../../src/fieldtypes/RelatedPositionIDSource';

describe('RelatedPositionIDSource', () => {
    test('should have the correct values', () => {
        expect(RelatedPositionIDSource.PosMaintRptID).toBe(1);
        expect(RelatedPositionIDSource.TransferID).toBe(2);
        expect(RelatedPositionIDSource.PositionEntityID).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelatedPositionIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RelatedPositionIDSource.PosMaintRptID,
            RelatedPositionIDSource.TransferID,
            RelatedPositionIDSource.PositionEntityID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelatedPositionIDSource type', () => {
        const validate = (value: RelatedPositionIDSource) => {
            expect(Object.values(RelatedPositionIDSource)).toContain(value);
        };

        validate(RelatedPositionIDSource.PosMaintRptID);
        validate(RelatedPositionIDSource.TransferID);
        validate(RelatedPositionIDSource.PositionEntityID);
    });

    test('should be immutable', () => {
        const ref = RelatedPositionIDSource;
        expect(() => {
            ref.PosMaintRptID = 10;
        }).toThrowError();
    });
});
