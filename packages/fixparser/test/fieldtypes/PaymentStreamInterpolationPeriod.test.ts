import { PaymentStreamInterpolationPeriod } from '../../src/fieldtypes/PaymentStreamInterpolationPeriod';

describe('PaymentStreamInterpolationPeriod', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamInterpolationPeriod.Initial).toBe(0);
        expect(PaymentStreamInterpolationPeriod.InitialAndFinal).toBe(1);
        expect(PaymentStreamInterpolationPeriod.Final).toBe(2);
        expect(PaymentStreamInterpolationPeriod.AnyPeriod).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamInterpolationPeriod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamInterpolationPeriod.Initial,
            PaymentStreamInterpolationPeriod.InitialAndFinal,
            PaymentStreamInterpolationPeriod.Final,
            PaymentStreamInterpolationPeriod.AnyPeriod,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamInterpolationPeriod type', () => {
        const validate = (value: PaymentStreamInterpolationPeriod) => {
            expect(Object.values(PaymentStreamInterpolationPeriod)).toContain(value);
        };

        validate(PaymentStreamInterpolationPeriod.Initial);
        validate(PaymentStreamInterpolationPeriod.InitialAndFinal);
        validate(PaymentStreamInterpolationPeriod.Final);
        validate(PaymentStreamInterpolationPeriod.AnyPeriod);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamInterpolationPeriod;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
