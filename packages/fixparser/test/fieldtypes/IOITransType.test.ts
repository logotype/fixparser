import { IOITransType } from '../../src/fieldtypes/IOITransType';

describe('IOITransType', () => {
    test('should have the correct values', () => {
        expect(IOITransType.New).toBe('N');
        expect(IOITransType.Cancel).toBe('C');
        expect(IOITransType.Replace).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IOITransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IOITransType.New, IOITransType.Cancel, IOITransType.Replace];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for IOITransType type', () => {
        const validate = (value: IOITransType) => {
            expect(Object.values(IOITransType)).toContain(value);
        };

        validate(IOITransType.New);
        validate(IOITransType.Cancel);
        validate(IOITransType.Replace);
    });

    test('should be immutable', () => {
        const ref = IOITransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
