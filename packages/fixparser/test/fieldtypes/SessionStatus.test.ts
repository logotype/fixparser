import { SessionStatus } from '../../src/fieldtypes/SessionStatus';

describe('SessionStatus', () => {
    test('should have the correct values', () => {
        expect(SessionStatus.SessionActive).toBe(0);
        expect(SessionStatus.SessionPasswordChanged).toBe(1);
        expect(SessionStatus.SessionPasswordDueToExpire).toBe(2);
        expect(SessionStatus.NewSessionPasswordDoesNotComplyWithPolicy).toBe(3);
        expect(SessionStatus.SessionLogoutComplete).toBe(4);
        expect(SessionStatus.InvalidUsernameOrPassword).toBe(5);
        expect(SessionStatus.AccountLocked).toBe(6);
        expect(SessionStatus.LogonsAreNotAllowedAtThisTime).toBe(7);
        expect(SessionStatus.PasswordExpired).toBe(8);
        expect(SessionStatus.ReceivedMsgSeqNumTooLow).toBe(9);
        expect(SessionStatus.ReceivedNextExpectedMsgSeqNumTooHigh).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SessionStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SessionStatus.SessionActive,
            SessionStatus.SessionPasswordChanged,
            SessionStatus.SessionPasswordDueToExpire,
            SessionStatus.NewSessionPasswordDoesNotComplyWithPolicy,
            SessionStatus.SessionLogoutComplete,
            SessionStatus.InvalidUsernameOrPassword,
            SessionStatus.AccountLocked,
            SessionStatus.LogonsAreNotAllowedAtThisTime,
            SessionStatus.PasswordExpired,
            SessionStatus.ReceivedMsgSeqNumTooLow,
            SessionStatus.ReceivedNextExpectedMsgSeqNumTooHigh,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SessionStatus type', () => {
        const validate = (value: SessionStatus) => {
            expect(Object.values(SessionStatus)).toContain(value);
        };

        validate(SessionStatus.SessionActive);
        validate(SessionStatus.SessionPasswordChanged);
        validate(SessionStatus.SessionPasswordDueToExpire);
        validate(SessionStatus.NewSessionPasswordDoesNotComplyWithPolicy);
        validate(SessionStatus.SessionLogoutComplete);
        validate(SessionStatus.InvalidUsernameOrPassword);
        validate(SessionStatus.AccountLocked);
        validate(SessionStatus.LogonsAreNotAllowedAtThisTime);
        validate(SessionStatus.PasswordExpired);
        validate(SessionStatus.ReceivedMsgSeqNumTooLow);
        validate(SessionStatus.ReceivedNextExpectedMsgSeqNumTooHigh);
    });

    test('should be immutable', () => {
        const ref = SessionStatus;
        expect(() => {
            ref.SessionActive = 10;
        }).toThrowError();
    });
});
