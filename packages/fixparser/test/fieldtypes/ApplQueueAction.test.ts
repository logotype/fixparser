import { ApplQueueAction } from '../../src/fieldtypes/ApplQueueAction';

describe('ApplQueueAction', () => {
    test('should have the correct values', () => {
        expect(ApplQueueAction.NoActionTaken).toBe(0);
        expect(ApplQueueAction.QueueFlushed).toBe(1);
        expect(ApplQueueAction.OverlayLast).toBe(2);
        expect(ApplQueueAction.EndSession).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplQueueAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplQueueAction.NoActionTaken,
            ApplQueueAction.QueueFlushed,
            ApplQueueAction.OverlayLast,
            ApplQueueAction.EndSession,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplQueueAction type', () => {
        const validate = (value: ApplQueueAction) => {
            expect(Object.values(ApplQueueAction)).toContain(value);
        };

        validate(ApplQueueAction.NoActionTaken);
        validate(ApplQueueAction.QueueFlushed);
        validate(ApplQueueAction.OverlayLast);
        validate(ApplQueueAction.EndSession);
    });

    test('should be immutable', () => {
        const ref = ApplQueueAction;
        expect(() => {
            ref.NoActionTaken = 10;
        }).toThrowError();
    });
});
