import { OffsetInstruction } from '../../src/fieldtypes/OffsetInstruction';

describe('OffsetInstruction', () => {
    test('should have the correct values', () => {
        expect(OffsetInstruction.Offset).toBe(0);
        expect(OffsetInstruction.Onset).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OffsetInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OffsetInstruction.Offset, OffsetInstruction.Onset];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OffsetInstruction type', () => {
        const validate = (value: OffsetInstruction) => {
            expect(Object.values(OffsetInstruction)).toContain(value);
        };

        validate(OffsetInstruction.Offset);
        validate(OffsetInstruction.Onset);
    });

    test('should be immutable', () => {
        const ref = OffsetInstruction;
        expect(() => {
            ref.Offset = 10;
        }).toThrowError();
    });
});
