import { PayRequestStatus } from '../../src/fieldtypes/PayRequestStatus';

describe('PayRequestStatus', () => {
    test('should have the correct values', () => {
        expect(PayRequestStatus.Received).toBe(0);
        expect(PayRequestStatus.Accepted).toBe(1);
        expect(PayRequestStatus.Rejected).toBe(2);
        expect(PayRequestStatus.Disputed).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PayRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PayRequestStatus.Received,
            PayRequestStatus.Accepted,
            PayRequestStatus.Rejected,
            PayRequestStatus.Disputed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PayRequestStatus type', () => {
        const validate = (value: PayRequestStatus) => {
            expect(Object.values(PayRequestStatus)).toContain(value);
        };

        validate(PayRequestStatus.Received);
        validate(PayRequestStatus.Accepted);
        validate(PayRequestStatus.Rejected);
        validate(PayRequestStatus.Disputed);
    });

    test('should be immutable', () => {
        const ref = PayRequestStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
