import { PaymentPaySide } from '../../src/fieldtypes/PaymentPaySide';

describe('PaymentPaySide', () => {
    test('should have the correct values', () => {
        expect(PaymentPaySide.Buy).toBe(1);
        expect(PaymentPaySide.Sell).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentPaySide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentPaySide.Buy, PaymentPaySide.Sell];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentPaySide type', () => {
        const validate = (value: PaymentPaySide) => {
            expect(Object.values(PaymentPaySide)).toContain(value);
        };

        validate(PaymentPaySide.Buy);
        validate(PaymentPaySide.Sell);
    });

    test('should be immutable', () => {
        const ref = PaymentPaySide;
        expect(() => {
            ref.Buy = 10;
        }).toThrowError();
    });
});
