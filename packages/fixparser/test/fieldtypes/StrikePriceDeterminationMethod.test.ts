import { StrikePriceDeterminationMethod } from '../../src/fieldtypes/StrikePriceDeterminationMethod';

describe('StrikePriceDeterminationMethod', () => {
    test('should have the correct values', () => {
        expect(StrikePriceDeterminationMethod.FixedStrike).toBe(1);
        expect(StrikePriceDeterminationMethod.StrikeSetAtExpiration).toBe(2);
        expect(StrikePriceDeterminationMethod.StrikeSetToAverageAcrossLife).toBe(3);
        expect(StrikePriceDeterminationMethod.StrikeSetToOptimalValue).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StrikePriceDeterminationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StrikePriceDeterminationMethod.FixedStrike,
            StrikePriceDeterminationMethod.StrikeSetAtExpiration,
            StrikePriceDeterminationMethod.StrikeSetToAverageAcrossLife,
            StrikePriceDeterminationMethod.StrikeSetToOptimalValue,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StrikePriceDeterminationMethod type', () => {
        const validate = (value: StrikePriceDeterminationMethod) => {
            expect(Object.values(StrikePriceDeterminationMethod)).toContain(value);
        };

        validate(StrikePriceDeterminationMethod.FixedStrike);
        validate(StrikePriceDeterminationMethod.StrikeSetAtExpiration);
        validate(StrikePriceDeterminationMethod.StrikeSetToAverageAcrossLife);
        validate(StrikePriceDeterminationMethod.StrikeSetToOptimalValue);
    });

    test('should be immutable', () => {
        const ref = StrikePriceDeterminationMethod;
        expect(() => {
            ref.FixedStrike = 10;
        }).toThrowError();
    });
});
