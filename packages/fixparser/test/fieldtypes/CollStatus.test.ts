import { CollStatus } from '../../src/fieldtypes/CollStatus';

describe('CollStatus', () => {
    test('should have the correct values', () => {
        expect(CollStatus.Unassigned).toBe(0);
        expect(CollStatus.PartiallyAssigned).toBe(1);
        expect(CollStatus.AssignmentProposed).toBe(2);
        expect(CollStatus.Assigned).toBe(3);
        expect(CollStatus.Challenged).toBe(4);
        expect(CollStatus.Reused).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollStatus.Unassigned,
            CollStatus.PartiallyAssigned,
            CollStatus.AssignmentProposed,
            CollStatus.Assigned,
            CollStatus.Challenged,
            CollStatus.Reused,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollStatus type', () => {
        const validate = (value: CollStatus) => {
            expect(Object.values(CollStatus)).toContain(value);
        };

        validate(CollStatus.Unassigned);
        validate(CollStatus.PartiallyAssigned);
        validate(CollStatus.AssignmentProposed);
        validate(CollStatus.Assigned);
        validate(CollStatus.Challenged);
        validate(CollStatus.Reused);
    });

    test('should be immutable', () => {
        const ref = CollStatus;
        expect(() => {
            ref.Unassigned = 10;
        }).toThrowError();
    });
});
