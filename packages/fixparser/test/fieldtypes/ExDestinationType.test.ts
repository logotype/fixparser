import { ExDestinationType } from '../../src/fieldtypes/ExDestinationType';

describe('ExDestinationType', () => {
    test('should have the correct values', () => {
        expect(ExDestinationType.NoRestriction).toBe(0);
        expect(ExDestinationType.TradedOnlyOnTradingVenue).toBe(1);
        expect(ExDestinationType.TradedOnlyOnSI).toBe(2);
        expect(ExDestinationType.TradedOnTradingVenueOrSI).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExDestinationType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExDestinationType.NoRestriction,
            ExDestinationType.TradedOnlyOnTradingVenue,
            ExDestinationType.TradedOnlyOnSI,
            ExDestinationType.TradedOnTradingVenueOrSI,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExDestinationType type', () => {
        const validate = (value: ExDestinationType) => {
            expect(Object.values(ExDestinationType)).toContain(value);
        };

        validate(ExDestinationType.NoRestriction);
        validate(ExDestinationType.TradedOnlyOnTradingVenue);
        validate(ExDestinationType.TradedOnlyOnSI);
        validate(ExDestinationType.TradedOnTradingVenueOrSI);
    });

    test('should be immutable', () => {
        const ref = ExDestinationType;
        expect(() => {
            ref.NoRestriction = 10;
        }).toThrowError();
    });
});
