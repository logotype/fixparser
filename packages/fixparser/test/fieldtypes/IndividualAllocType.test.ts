import { IndividualAllocType } from '../../src/fieldtypes/IndividualAllocType';

describe('IndividualAllocType', () => {
    test('should have the correct values', () => {
        expect(IndividualAllocType.SubAllocate).toBe(1);
        expect(IndividualAllocType.ThirdPartyAllocation).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IndividualAllocType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IndividualAllocType.SubAllocate, IndividualAllocType.ThirdPartyAllocation];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for IndividualAllocType type', () => {
        const validate = (value: IndividualAllocType) => {
            expect(Object.values(IndividualAllocType)).toContain(value);
        };

        validate(IndividualAllocType.SubAllocate);
        validate(IndividualAllocType.ThirdPartyAllocation);
    });

    test('should be immutable', () => {
        const ref = IndividualAllocType;
        expect(() => {
            ref.SubAllocate = 10;
        }).toThrowError();
    });
});
