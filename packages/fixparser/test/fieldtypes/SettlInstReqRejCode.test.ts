import { SettlInstReqRejCode } from '../../src/fieldtypes/SettlInstReqRejCode';

describe('SettlInstReqRejCode', () => {
    test('should have the correct values', () => {
        expect(SettlInstReqRejCode.UnableToProcessRequest).toBe(0);
        expect(SettlInstReqRejCode.UnknownAccount).toBe(1);
        expect(SettlInstReqRejCode.NoMatchingSettlementInstructionsFound).toBe(2);
        expect(SettlInstReqRejCode.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlInstReqRejCode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlInstReqRejCode.UnableToProcessRequest,
            SettlInstReqRejCode.UnknownAccount,
            SettlInstReqRejCode.NoMatchingSettlementInstructionsFound,
            SettlInstReqRejCode.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlInstReqRejCode type', () => {
        const validate = (value: SettlInstReqRejCode) => {
            expect(Object.values(SettlInstReqRejCode)).toContain(value);
        };

        validate(SettlInstReqRejCode.UnableToProcessRequest);
        validate(SettlInstReqRejCode.UnknownAccount);
        validate(SettlInstReqRejCode.NoMatchingSettlementInstructionsFound);
        validate(SettlInstReqRejCode.Other);
    });

    test('should be immutable', () => {
        const ref = SettlInstReqRejCode;
        expect(() => {
            ref.UnableToProcessRequest = 10;
        }).toThrowError();
    });
});
