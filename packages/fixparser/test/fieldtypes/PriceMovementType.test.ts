import { PriceMovementType } from '../../src/fieldtypes/PriceMovementType';

describe('PriceMovementType', () => {
    test('should have the correct values', () => {
        expect(PriceMovementType.Amount).toBe(0);
        expect(PriceMovementType.Percentage).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceMovementType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PriceMovementType.Amount, PriceMovementType.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PriceMovementType type', () => {
        const validate = (value: PriceMovementType) => {
            expect(Object.values(PriceMovementType)).toContain(value);
        };

        validate(PriceMovementType.Amount);
        validate(PriceMovementType.Percentage);
    });

    test('should be immutable', () => {
        const ref = PriceMovementType;
        expect(() => {
            ref.Amount = 10;
        }).toThrowError();
    });
});
