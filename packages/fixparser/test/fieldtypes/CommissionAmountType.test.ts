import { CommissionAmountType } from '../../src/fieldtypes/CommissionAmountType';

describe('CommissionAmountType', () => {
    test('should have the correct values', () => {
        expect(CommissionAmountType.Unspecified).toBe(0);
        expect(CommissionAmountType.Acceptance).toBe(1);
        expect(CommissionAmountType.Broker).toBe(2);
        expect(CommissionAmountType.ClearingBroker).toBe(3);
        expect(CommissionAmountType.Retail).toBe(4);
        expect(CommissionAmountType.SalesCommission).toBe(5);
        expect(CommissionAmountType.LocalCommission).toBe(6);
        expect(CommissionAmountType.ResearchPayment).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CommissionAmountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CommissionAmountType.Unspecified,
            CommissionAmountType.Acceptance,
            CommissionAmountType.Broker,
            CommissionAmountType.ClearingBroker,
            CommissionAmountType.Retail,
            CommissionAmountType.SalesCommission,
            CommissionAmountType.LocalCommission,
            CommissionAmountType.ResearchPayment,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CommissionAmountType type', () => {
        const validate = (value: CommissionAmountType) => {
            expect(Object.values(CommissionAmountType)).toContain(value);
        };

        validate(CommissionAmountType.Unspecified);
        validate(CommissionAmountType.Acceptance);
        validate(CommissionAmountType.Broker);
        validate(CommissionAmountType.ClearingBroker);
        validate(CommissionAmountType.Retail);
        validate(CommissionAmountType.SalesCommission);
        validate(CommissionAmountType.LocalCommission);
        validate(CommissionAmountType.ResearchPayment);
    });

    test('should be immutable', () => {
        const ref = CommissionAmountType;
        expect(() => {
            ref.Unspecified = 10;
        }).toThrowError();
    });
});
