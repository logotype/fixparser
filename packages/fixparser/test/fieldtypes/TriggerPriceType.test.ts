import { TriggerPriceType } from '../../src/fieldtypes/TriggerPriceType';

describe('TriggerPriceType', () => {
    test('should have the correct values', () => {
        expect(TriggerPriceType.BestOffer).toBe('1');
        expect(TriggerPriceType.LastTrade).toBe('2');
        expect(TriggerPriceType.BestBid).toBe('3');
        expect(TriggerPriceType.BestBidOrLastTrade).toBe('4');
        expect(TriggerPriceType.BestOfferOrLastTrade).toBe('5');
        expect(TriggerPriceType.BestMid).toBe('6');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TriggerPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TriggerPriceType.BestOffer,
            TriggerPriceType.LastTrade,
            TriggerPriceType.BestBid,
            TriggerPriceType.BestBidOrLastTrade,
            TriggerPriceType.BestOfferOrLastTrade,
            TriggerPriceType.BestMid,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TriggerPriceType type', () => {
        const validate = (value: TriggerPriceType) => {
            expect(Object.values(TriggerPriceType)).toContain(value);
        };

        validate(TriggerPriceType.BestOffer);
        validate(TriggerPriceType.LastTrade);
        validate(TriggerPriceType.BestBid);
        validate(TriggerPriceType.BestBidOrLastTrade);
        validate(TriggerPriceType.BestOfferOrLastTrade);
        validate(TriggerPriceType.BestMid);
    });

    test('should be immutable', () => {
        const ref = TriggerPriceType;
        expect(() => {
            ref.BestOffer = 10;
        }).toThrowError();
    });
});
