import { PosReqType } from '../../src/fieldtypes/PosReqType';

describe('PosReqType', () => {
    test('should have the correct values', () => {
        expect(PosReqType.Positions).toBe(0);
        expect(PosReqType.Trades).toBe(1);
        expect(PosReqType.Exercises).toBe(2);
        expect(PosReqType.Assignments).toBe(3);
        expect(PosReqType.SettlementActivity).toBe(4);
        expect(PosReqType.BackoutMessage).toBe(5);
        expect(PosReqType.DeltaPositions).toBe(6);
        expect(PosReqType.NetPosition).toBe(7);
        expect(PosReqType.LargePositionsReporting).toBe(8);
        expect(PosReqType.ExercisePositionReportingSubmission).toBe(9);
        expect(PosReqType.PositionLimitReportingSubmissing).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosReqType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosReqType.Positions,
            PosReqType.Trades,
            PosReqType.Exercises,
            PosReqType.Assignments,
            PosReqType.SettlementActivity,
            PosReqType.BackoutMessage,
            PosReqType.DeltaPositions,
            PosReqType.NetPosition,
            PosReqType.LargePositionsReporting,
            PosReqType.ExercisePositionReportingSubmission,
            PosReqType.PositionLimitReportingSubmissing,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosReqType type', () => {
        const validate = (value: PosReqType) => {
            expect(Object.values(PosReqType)).toContain(value);
        };

        validate(PosReqType.Positions);
        validate(PosReqType.Trades);
        validate(PosReqType.Exercises);
        validate(PosReqType.Assignments);
        validate(PosReqType.SettlementActivity);
        validate(PosReqType.BackoutMessage);
        validate(PosReqType.DeltaPositions);
        validate(PosReqType.NetPosition);
        validate(PosReqType.LargePositionsReporting);
        validate(PosReqType.ExercisePositionReportingSubmission);
        validate(PosReqType.PositionLimitReportingSubmissing);
    });

    test('should be immutable', () => {
        const ref = PosReqType;
        expect(() => {
            ref.Positions = 10;
        }).toThrowError();
    });
});
