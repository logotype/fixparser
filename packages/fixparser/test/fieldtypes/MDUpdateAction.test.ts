import { MDUpdateAction } from '../../src/fieldtypes/MDUpdateAction';

describe('MDUpdateAction', () => {
    test('should have the correct values', () => {
        expect(MDUpdateAction.New).toBe('0');
        expect(MDUpdateAction.Change).toBe('1');
        expect(MDUpdateAction.Delete).toBe('2');
        expect(MDUpdateAction.DeleteThru).toBe('3');
        expect(MDUpdateAction.DeleteFrom).toBe('4');
        expect(MDUpdateAction.Overlay).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDUpdateAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDUpdateAction.New,
            MDUpdateAction.Change,
            MDUpdateAction.Delete,
            MDUpdateAction.DeleteThru,
            MDUpdateAction.DeleteFrom,
            MDUpdateAction.Overlay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MDUpdateAction type', () => {
        const validate = (value: MDUpdateAction) => {
            expect(Object.values(MDUpdateAction)).toContain(value);
        };

        validate(MDUpdateAction.New);
        validate(MDUpdateAction.Change);
        validate(MDUpdateAction.Delete);
        validate(MDUpdateAction.DeleteThru);
        validate(MDUpdateAction.DeleteFrom);
        validate(MDUpdateAction.Overlay);
    });

    test('should be immutable', () => {
        const ref = MDUpdateAction;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
