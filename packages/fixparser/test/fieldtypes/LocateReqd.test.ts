import { LocateReqd } from '../../src/fieldtypes/LocateReqd';

describe('LocateReqd', () => {
    test('should have the correct values', () => {
        expect(LocateReqd.No).toBe('N');
        expect(LocateReqd.Yes).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LocateReqd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [LocateReqd.No, LocateReqd.Yes];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for LocateReqd type', () => {
        const validate = (value: LocateReqd) => {
            expect(Object.values(LocateReqd)).toContain(value);
        };

        validate(LocateReqd.No);
        validate(LocateReqd.Yes);
    });

    test('should be immutable', () => {
        const ref = LocateReqd;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
