import { ComplexEventCondition } from '../../src/fieldtypes/ComplexEventCondition';

describe('ComplexEventCondition', () => {
    test('should have the correct values', () => {
        expect(ComplexEventCondition.And).toBe(1);
        expect(ComplexEventCondition.Or).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ComplexEventCondition.And, ComplexEventCondition.Or];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventCondition type', () => {
        const validate = (value: ComplexEventCondition) => {
            expect(Object.values(ComplexEventCondition)).toContain(value);
        };

        validate(ComplexEventCondition.And);
        validate(ComplexEventCondition.Or);
    });

    test('should be immutable', () => {
        const ref = ComplexEventCondition;
        expect(() => {
            ref.And = 10;
        }).toThrowError();
    });
});
