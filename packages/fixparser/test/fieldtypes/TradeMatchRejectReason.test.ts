import { TradeMatchRejectReason } from '../../src/fieldtypes/TradeMatchRejectReason';

describe('TradeMatchRejectReason', () => {
    test('should have the correct values', () => {
        expect(TradeMatchRejectReason.Successful).toBe(0);
        expect(TradeMatchRejectReason.InvalidPartyInformation).toBe(1);
        expect(TradeMatchRejectReason.UnknownInstrument).toBe(2);
        expect(TradeMatchRejectReason.Unauthorized).toBe(3);
        expect(TradeMatchRejectReason.InvalidTradeType).toBe(4);
        expect(TradeMatchRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeMatchRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeMatchRejectReason.Successful,
            TradeMatchRejectReason.InvalidPartyInformation,
            TradeMatchRejectReason.UnknownInstrument,
            TradeMatchRejectReason.Unauthorized,
            TradeMatchRejectReason.InvalidTradeType,
            TradeMatchRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeMatchRejectReason type', () => {
        const validate = (value: TradeMatchRejectReason) => {
            expect(Object.values(TradeMatchRejectReason)).toContain(value);
        };

        validate(TradeMatchRejectReason.Successful);
        validate(TradeMatchRejectReason.InvalidPartyInformation);
        validate(TradeMatchRejectReason.UnknownInstrument);
        validate(TradeMatchRejectReason.Unauthorized);
        validate(TradeMatchRejectReason.InvalidTradeType);
        validate(TradeMatchRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = TradeMatchRejectReason;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
