import { PaymentStreamRateSpreadType } from '../../src/fieldtypes/PaymentStreamRateSpreadType';

describe('PaymentStreamRateSpreadType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRateSpreadType.Absolute).toBe(0);
        expect(PaymentStreamRateSpreadType.Percentage).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRateSpreadType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamRateSpreadType.Absolute, PaymentStreamRateSpreadType.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamRateSpreadType type', () => {
        const validate = (value: PaymentStreamRateSpreadType) => {
            expect(Object.values(PaymentStreamRateSpreadType)).toContain(value);
        };

        validate(PaymentStreamRateSpreadType.Absolute);
        validate(PaymentStreamRateSpreadType.Percentage);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRateSpreadType;
        expect(() => {
            ref.Absolute = 10;
        }).toThrowError();
    });
});
