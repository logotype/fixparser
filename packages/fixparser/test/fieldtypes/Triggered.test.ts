import { Triggered } from '../../src/fieldtypes/Triggered';

describe('Triggered', () => {
    test('should have the correct values', () => {
        expect(Triggered.NotTriggered).toBe(0);
        expect(Triggered.Triggered).toBe(1);
        expect(Triggered.StopOrderTriggered).toBe(2);
        expect(Triggered.OCOOrderTriggered).toBe(3);
        expect(Triggered.OTOOrderTriggered).toBe(4);
        expect(Triggered.OUOOrderTriggered).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Triggered.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Triggered.NotTriggered,
            Triggered.Triggered,
            Triggered.StopOrderTriggered,
            Triggered.OCOOrderTriggered,
            Triggered.OTOOrderTriggered,
            Triggered.OUOOrderTriggered,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for Triggered type', () => {
        const validate = (value: Triggered) => {
            expect(Object.values(Triggered)).toContain(value);
        };

        validate(Triggered.NotTriggered);
        validate(Triggered.Triggered);
        validate(Triggered.StopOrderTriggered);
        validate(Triggered.OCOOrderTriggered);
        validate(Triggered.OTOOrderTriggered);
        validate(Triggered.OUOOrderTriggered);
    });

    test('should be immutable', () => {
        const ref = Triggered;
        expect(() => {
            ref.NotTriggered = 10;
        }).toThrowError();
    });
});
