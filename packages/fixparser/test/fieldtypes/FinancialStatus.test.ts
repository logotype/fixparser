import { FinancialStatus } from '../../src/fieldtypes/FinancialStatus';

describe('FinancialStatus', () => {
    test('should have the correct values', () => {
        expect(FinancialStatus.Bankrupt).toBe('1');
        expect(FinancialStatus.PendingDelisting).toBe('2');
        expect(FinancialStatus.Restricted).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            FinancialStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            FinancialStatus.Bankrupt,
            FinancialStatus.PendingDelisting,
            FinancialStatus.Restricted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for FinancialStatus type', () => {
        const validate = (value: FinancialStatus) => {
            expect(Object.values(FinancialStatus)).toContain(value);
        };

        validate(FinancialStatus.Bankrupt);
        validate(FinancialStatus.PendingDelisting);
        validate(FinancialStatus.Restricted);
    });

    test('should be immutable', () => {
        const ref = FinancialStatus;
        expect(() => {
            ref.Bankrupt = 10;
        }).toThrowError();
    });
});
