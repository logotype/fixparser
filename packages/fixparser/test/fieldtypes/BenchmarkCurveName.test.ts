import { BenchmarkCurveName } from '../../src/fieldtypes/BenchmarkCurveName';

describe('BenchmarkCurveName', () => {
    test('should have the correct values', () => {
        expect(BenchmarkCurveName.EONIA).toBe('EONIA');
        expect(BenchmarkCurveName.EUREPO).toBe('EUREPO');
        expect(BenchmarkCurveName.Euribor).toBe('Euribor');
        expect(BenchmarkCurveName.FutureSWAP).toBe('FutureSWAP');
        expect(BenchmarkCurveName.LIBID).toBe('LIBID');
        expect(BenchmarkCurveName.LIBOR).toBe('LIBOR');
        expect(BenchmarkCurveName.MuniAAA).toBe('MuniAAA');
        expect(BenchmarkCurveName.OTHER).toBe('OTHER');
        expect(BenchmarkCurveName.Pfandbriefe).toBe('Pfandbriefe');
        expect(BenchmarkCurveName.SONIA).toBe('SONIA');
        expect(BenchmarkCurveName.SWAP).toBe('SWAP');
        expect(BenchmarkCurveName.Treasury).toBe('Treasury');
        expect(BenchmarkCurveName.FedFundRateEffective).toBe('FEDEFF');
        expect(BenchmarkCurveName.FedOpen).toBe('FEDOPEN');
        expect(BenchmarkCurveName.EURIBOR).toBe('EURIBOR');
        expect(BenchmarkCurveName.AUBSW).toBe('AUBSW');
        expect(BenchmarkCurveName.BUBOR).toBe('BUBOR');
        expect(BenchmarkCurveName.CDOR).toBe('CDOR');
        expect(BenchmarkCurveName.CIBOR).toBe('CIBOR');
        expect(BenchmarkCurveName.EONIASWAP).toBe('EONIASWAP');
        expect(BenchmarkCurveName.ESTR).toBe('ESTR');
        expect(BenchmarkCurveName.EURODOLLAR).toBe('EURODOLLAR');
        expect(BenchmarkCurveName.EUROSWISS).toBe('EUROSWISS');
        expect(BenchmarkCurveName.GCFREPO).toBe('GCFREPO');
        expect(BenchmarkCurveName.ISDAFIX).toBe('ISDAFIX');
        expect(BenchmarkCurveName.JIBAR).toBe('JIBAR');
        expect(BenchmarkCurveName.MOSPRIM).toBe('MOSPRIM');
        expect(BenchmarkCurveName.NIBOR).toBe('NIBOR');
        expect(BenchmarkCurveName.PRIBOR).toBe('PRIBOR');
        expect(BenchmarkCurveName.SOFR).toBe('SOFR');
        expect(BenchmarkCurveName.STIBOR).toBe('STIBOR');
        expect(BenchmarkCurveName.TELBOR).toBe('TELBOR');
        expect(BenchmarkCurveName.TIBOR).toBe('TIBOR');
        expect(BenchmarkCurveName.WIBOR).toBe('WIBOR');
        expect(BenchmarkCurveName.AONIA).toBe('AONIA');
        expect(BenchmarkCurveName.AONIAR).toBe('AONIA-R');
        expect(BenchmarkCurveName.BKBM).toBe('BKBM');
        expect(BenchmarkCurveName.CD19D).toBe('CD91D');
        expect(BenchmarkCurveName.CORRA).toBe('CORRA');
        expect(BenchmarkCurveName.DIRRTN).toBe('DIRR-TN');
        expect(BenchmarkCurveName.EIBOR).toBe('EIBOR');
        expect(BenchmarkCurveName.FixingRepoRate).toBe('FixingRepoRate');
        expect(BenchmarkCurveName.HIBOR).toBe('HIBOR');
        expect(BenchmarkCurveName.IBR).toBe('IBR');
        expect(BenchmarkCurveName.KLIBOR).toBe('KLIBOR');
        expect(BenchmarkCurveName.MIBOR).toBe('MIBOR');
        expect(BenchmarkCurveName.NZONIA).toBe('NZONIA');
        expect(BenchmarkCurveName.PHIREF).toBe('PHIREF');
        expect(BenchmarkCurveName.REIBOR).toBe('REIBOR');
        expect(BenchmarkCurveName.SAIBOR).toBe('SAIBOR');
        expect(BenchmarkCurveName.SARON).toBe('SARON');
        expect(BenchmarkCurveName.SORA).toBe('SORA');
        expect(BenchmarkCurveName.TLREF).toBe('TLREF');
        expect(BenchmarkCurveName.TIIE).toBe('TIIE');
        expect(BenchmarkCurveName.THBFIX).toBe('THBFIX');
        expect(BenchmarkCurveName.TONAR).toBe('TONAR');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BenchmarkCurveName.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            BenchmarkCurveName.EONIA,
            BenchmarkCurveName.EUREPO,
            BenchmarkCurveName.Euribor,
            BenchmarkCurveName.FutureSWAP,
            BenchmarkCurveName.LIBID,
            BenchmarkCurveName.LIBOR,
            BenchmarkCurveName.MuniAAA,
            BenchmarkCurveName.OTHER,
            BenchmarkCurveName.Pfandbriefe,
            BenchmarkCurveName.SONIA,
            BenchmarkCurveName.SWAP,
            BenchmarkCurveName.Treasury,
            BenchmarkCurveName.FedFundRateEffective,
            BenchmarkCurveName.FedOpen,
            BenchmarkCurveName.EURIBOR,
            BenchmarkCurveName.AUBSW,
            BenchmarkCurveName.BUBOR,
            BenchmarkCurveName.CDOR,
            BenchmarkCurveName.CIBOR,
            BenchmarkCurveName.EONIASWAP,
            BenchmarkCurveName.ESTR,
            BenchmarkCurveName.EURODOLLAR,
            BenchmarkCurveName.EUROSWISS,
            BenchmarkCurveName.GCFREPO,
            BenchmarkCurveName.ISDAFIX,
            BenchmarkCurveName.JIBAR,
            BenchmarkCurveName.MOSPRIM,
            BenchmarkCurveName.NIBOR,
            BenchmarkCurveName.PRIBOR,
            BenchmarkCurveName.SOFR,
            BenchmarkCurveName.STIBOR,
            BenchmarkCurveName.TELBOR,
            BenchmarkCurveName.TIBOR,
            BenchmarkCurveName.WIBOR,
            BenchmarkCurveName.AONIA,
            BenchmarkCurveName.AONIAR,
            BenchmarkCurveName.BKBM,
            BenchmarkCurveName.CD19D,
            BenchmarkCurveName.CORRA,
            BenchmarkCurveName.DIRRTN,
            BenchmarkCurveName.EIBOR,
            BenchmarkCurveName.FixingRepoRate,
            BenchmarkCurveName.HIBOR,
            BenchmarkCurveName.IBR,
            BenchmarkCurveName.KLIBOR,
            BenchmarkCurveName.MIBOR,
            BenchmarkCurveName.NZONIA,
            BenchmarkCurveName.PHIREF,
            BenchmarkCurveName.REIBOR,
            BenchmarkCurveName.SAIBOR,
            BenchmarkCurveName.SARON,
            BenchmarkCurveName.SORA,
            BenchmarkCurveName.TLREF,
            BenchmarkCurveName.TIIE,
            BenchmarkCurveName.THBFIX,
            BenchmarkCurveName.TONAR,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for BenchmarkCurveName type', () => {
        const validate = (value: BenchmarkCurveName) => {
            expect(Object.values(BenchmarkCurveName)).toContain(value);
        };

        validate(BenchmarkCurveName.EONIA);
        validate(BenchmarkCurveName.EUREPO);
        validate(BenchmarkCurveName.Euribor);
        validate(BenchmarkCurveName.FutureSWAP);
        validate(BenchmarkCurveName.LIBID);
        validate(BenchmarkCurveName.LIBOR);
        validate(BenchmarkCurveName.MuniAAA);
        validate(BenchmarkCurveName.OTHER);
        validate(BenchmarkCurveName.Pfandbriefe);
        validate(BenchmarkCurveName.SONIA);
        validate(BenchmarkCurveName.SWAP);
        validate(BenchmarkCurveName.Treasury);
        validate(BenchmarkCurveName.FedFundRateEffective);
        validate(BenchmarkCurveName.FedOpen);
        validate(BenchmarkCurveName.EURIBOR);
        validate(BenchmarkCurveName.AUBSW);
        validate(BenchmarkCurveName.BUBOR);
        validate(BenchmarkCurveName.CDOR);
        validate(BenchmarkCurveName.CIBOR);
        validate(BenchmarkCurveName.EONIASWAP);
        validate(BenchmarkCurveName.ESTR);
        validate(BenchmarkCurveName.EURODOLLAR);
        validate(BenchmarkCurveName.EUROSWISS);
        validate(BenchmarkCurveName.GCFREPO);
        validate(BenchmarkCurveName.ISDAFIX);
        validate(BenchmarkCurveName.JIBAR);
        validate(BenchmarkCurveName.MOSPRIM);
        validate(BenchmarkCurveName.NIBOR);
        validate(BenchmarkCurveName.PRIBOR);
        validate(BenchmarkCurveName.SOFR);
        validate(BenchmarkCurveName.STIBOR);
        validate(BenchmarkCurveName.TELBOR);
        validate(BenchmarkCurveName.TIBOR);
        validate(BenchmarkCurveName.WIBOR);
        validate(BenchmarkCurveName.AONIA);
        validate(BenchmarkCurveName.AONIAR);
        validate(BenchmarkCurveName.BKBM);
        validate(BenchmarkCurveName.CD19D);
        validate(BenchmarkCurveName.CORRA);
        validate(BenchmarkCurveName.DIRRTN);
        validate(BenchmarkCurveName.EIBOR);
        validate(BenchmarkCurveName.FixingRepoRate);
        validate(BenchmarkCurveName.HIBOR);
        validate(BenchmarkCurveName.IBR);
        validate(BenchmarkCurveName.KLIBOR);
        validate(BenchmarkCurveName.MIBOR);
        validate(BenchmarkCurveName.NZONIA);
        validate(BenchmarkCurveName.PHIREF);
        validate(BenchmarkCurveName.REIBOR);
        validate(BenchmarkCurveName.SAIBOR);
        validate(BenchmarkCurveName.SARON);
        validate(BenchmarkCurveName.SORA);
        validate(BenchmarkCurveName.TLREF);
        validate(BenchmarkCurveName.TIIE);
        validate(BenchmarkCurveName.THBFIX);
        validate(BenchmarkCurveName.TONAR);
    });

    test('should be immutable', () => {
        const ref = BenchmarkCurveName;
        expect(() => {
            ref.EONIA = 10;
        }).toThrowError();
    });
});
