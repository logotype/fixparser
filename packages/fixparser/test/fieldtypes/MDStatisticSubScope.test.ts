import { MDStatisticSubScope } from '../../src/fieldtypes/MDStatisticSubScope';

describe('MDStatisticSubScope', () => {
    test('should have the correct values', () => {
        expect(MDStatisticSubScope.Visible).toBe(1);
        expect(MDStatisticSubScope.Hidden).toBe(2);
        expect(MDStatisticSubScope.Indicative).toBe(3);
        expect(MDStatisticSubScope.Tradeable).toBe(4);
        expect(MDStatisticSubScope.Passive).toBe(5);
        expect(MDStatisticSubScope.MarketConsensus).toBe(6);
        expect(MDStatisticSubScope.Power).toBe(7);
        expect(MDStatisticSubScope.HardwareError).toBe(8);
        expect(MDStatisticSubScope.SoftwareError).toBe(9);
        expect(MDStatisticSubScope.NetworkError).toBe(10);
        expect(MDStatisticSubScope.Failed).toBe(11);
        expect(MDStatisticSubScope.Executed).toBe(12);
        expect(MDStatisticSubScope.Entered).toBe(13);
        expect(MDStatisticSubScope.Modified).toBe(14);
        expect(MDStatisticSubScope.Cancelled).toBe(15);
        expect(MDStatisticSubScope.MarketDataAccess).toBe(16);
        expect(MDStatisticSubScope.TerminalAccess).toBe(17);
        expect(MDStatisticSubScope.Volume).toBe(18);
        expect(MDStatisticSubScope.Cleared).toBe(19);
        expect(MDStatisticSubScope.Settled).toBe(20);
        expect(MDStatisticSubScope.Other).toBe(21);
        expect(MDStatisticSubScope.Monetary).toBe(22);
        expect(MDStatisticSubScope.NonMonetary).toBe(23);
        expect(MDStatisticSubScope.Gross).toBe(24);
        expect(MDStatisticSubScope.LargeInScale).toBe(25);
        expect(MDStatisticSubScope.NeitherHiddenNorLargeInScale).toBe(26);
        expect(MDStatisticSubScope.CorporateAction).toBe(27);
        expect(MDStatisticSubScope.VenueDecision).toBe(28);
        expect(MDStatisticSubScope.MinimumTimePeriod).toBe(29);
        expect(MDStatisticSubScope.Open).toBe(30);
        expect(MDStatisticSubScope.NotExecuted).toBe(31);
        expect(MDStatisticSubScope.Aggressive).toBe(32);
        expect(MDStatisticSubScope.Directed).toBe(33);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticSubScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MDStatisticSubScope.Visible,
            MDStatisticSubScope.Hidden,
            MDStatisticSubScope.Indicative,
            MDStatisticSubScope.Tradeable,
            MDStatisticSubScope.Passive,
            MDStatisticSubScope.MarketConsensus,
            MDStatisticSubScope.Power,
            MDStatisticSubScope.HardwareError,
            MDStatisticSubScope.SoftwareError,
            MDStatisticSubScope.NetworkError,
            MDStatisticSubScope.Failed,
            MDStatisticSubScope.Executed,
            MDStatisticSubScope.Entered,
            MDStatisticSubScope.Modified,
            MDStatisticSubScope.Cancelled,
            MDStatisticSubScope.MarketDataAccess,
            MDStatisticSubScope.TerminalAccess,
            MDStatisticSubScope.Volume,
            MDStatisticSubScope.Cleared,
            MDStatisticSubScope.Settled,
            MDStatisticSubScope.Other,
            MDStatisticSubScope.Monetary,
            MDStatisticSubScope.NonMonetary,
            MDStatisticSubScope.Gross,
            MDStatisticSubScope.LargeInScale,
            MDStatisticSubScope.NeitherHiddenNorLargeInScale,
            MDStatisticSubScope.CorporateAction,
            MDStatisticSubScope.VenueDecision,
            MDStatisticSubScope.MinimumTimePeriod,
            MDStatisticSubScope.Open,
            MDStatisticSubScope.NotExecuted,
            MDStatisticSubScope.Aggressive,
            MDStatisticSubScope.Directed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticSubScope type', () => {
        const validate = (value: MDStatisticSubScope) => {
            expect(Object.values(MDStatisticSubScope)).toContain(value);
        };

        validate(MDStatisticSubScope.Visible);
        validate(MDStatisticSubScope.Hidden);
        validate(MDStatisticSubScope.Indicative);
        validate(MDStatisticSubScope.Tradeable);
        validate(MDStatisticSubScope.Passive);
        validate(MDStatisticSubScope.MarketConsensus);
        validate(MDStatisticSubScope.Power);
        validate(MDStatisticSubScope.HardwareError);
        validate(MDStatisticSubScope.SoftwareError);
        validate(MDStatisticSubScope.NetworkError);
        validate(MDStatisticSubScope.Failed);
        validate(MDStatisticSubScope.Executed);
        validate(MDStatisticSubScope.Entered);
        validate(MDStatisticSubScope.Modified);
        validate(MDStatisticSubScope.Cancelled);
        validate(MDStatisticSubScope.MarketDataAccess);
        validate(MDStatisticSubScope.TerminalAccess);
        validate(MDStatisticSubScope.Volume);
        validate(MDStatisticSubScope.Cleared);
        validate(MDStatisticSubScope.Settled);
        validate(MDStatisticSubScope.Other);
        validate(MDStatisticSubScope.Monetary);
        validate(MDStatisticSubScope.NonMonetary);
        validate(MDStatisticSubScope.Gross);
        validate(MDStatisticSubScope.LargeInScale);
        validate(MDStatisticSubScope.NeitherHiddenNorLargeInScale);
        validate(MDStatisticSubScope.CorporateAction);
        validate(MDStatisticSubScope.VenueDecision);
        validate(MDStatisticSubScope.MinimumTimePeriod);
        validate(MDStatisticSubScope.Open);
        validate(MDStatisticSubScope.NotExecuted);
        validate(MDStatisticSubScope.Aggressive);
        validate(MDStatisticSubScope.Directed);
    });

    test('should be immutable', () => {
        const ref = MDStatisticSubScope;
        expect(() => {
            ref.Visible = 10;
        }).toThrowError();
    });
});
