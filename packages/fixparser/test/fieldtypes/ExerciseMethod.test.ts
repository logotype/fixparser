import { ExerciseMethod } from '../../src/fieldtypes/ExerciseMethod';

describe('ExerciseMethod', () => {
    test('should have the correct values', () => {
        expect(ExerciseMethod.Automatic).toBe('A');
        expect(ExerciseMethod.Manual).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExerciseMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ExerciseMethod.Automatic, ExerciseMethod.Manual];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExerciseMethod type', () => {
        const validate = (value: ExerciseMethod) => {
            expect(Object.values(ExerciseMethod)).toContain(value);
        };

        validate(ExerciseMethod.Automatic);
        validate(ExerciseMethod.Manual);
    });

    test('should be immutable', () => {
        const ref = ExerciseMethod;
        expect(() => {
            ref.Automatic = 10;
        }).toThrowError();
    });
});
