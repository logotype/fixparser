import { DeleteReason } from '../../src/fieldtypes/DeleteReason';

describe('DeleteReason', () => {
    test('should have the correct values', () => {
        expect(DeleteReason.Cancellation).toBe('0');
        expect(DeleteReason.Error).toBe('1');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeleteReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeleteReason.Cancellation, DeleteReason.Error];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DeleteReason type', () => {
        const validate = (value: DeleteReason) => {
            expect(Object.values(DeleteReason)).toContain(value);
        };

        validate(DeleteReason.Cancellation);
        validate(DeleteReason.Error);
    });

    test('should be immutable', () => {
        const ref = DeleteReason;
        expect(() => {
            ref.Cancellation = 10;
        }).toThrowError();
    });
});
