import { AsOfIndicator } from '../../src/fieldtypes/AsOfIndicator';

describe('AsOfIndicator', () => {
    test('should have the correct values', () => {
        expect(AsOfIndicator.False).toBe('0');
        expect(AsOfIndicator.True).toBe('1');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AsOfIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AsOfIndicator.False, AsOfIndicator.True];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for AsOfIndicator type', () => {
        const validate = (value: AsOfIndicator) => {
            expect(Object.values(AsOfIndicator)).toContain(value);
        };

        validate(AsOfIndicator.False);
        validate(AsOfIndicator.True);
    });

    test('should be immutable', () => {
        const ref = AsOfIndicator;
        expect(() => {
            ref.False = 10;
        }).toThrowError();
    });
});
