import { PaymentStubLength } from '../../src/fieldtypes/PaymentStubLength';

describe('PaymentStubLength', () => {
    test('should have the correct values', () => {
        expect(PaymentStubLength.Short).toBe(0);
        expect(PaymentStubLength.Long).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStubLength.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStubLength.Short, PaymentStubLength.Long];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStubLength type', () => {
        const validate = (value: PaymentStubLength) => {
            expect(Object.values(PaymentStubLength)).toContain(value);
        };

        validate(PaymentStubLength.Short);
        validate(PaymentStubLength.Long);
    });

    test('should be immutable', () => {
        const ref = PaymentStubLength;
        expect(() => {
            ref.Short = 10;
        }).toThrowError();
    });
});
