import { ComplexEventCreditEventNotifyingParty } from '../../src/fieldtypes/ComplexEventCreditEventNotifyingParty';

describe('ComplexEventCreditEventNotifyingParty', () => {
    test('should have the correct values', () => {
        expect(ComplexEventCreditEventNotifyingParty.SellerNotifies).toBe(0);
        expect(ComplexEventCreditEventNotifyingParty.BuyerNotifies).toBe(1);
        expect(ComplexEventCreditEventNotifyingParty.SellerOrBuyerNotifies).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventCreditEventNotifyingParty.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventCreditEventNotifyingParty.SellerNotifies,
            ComplexEventCreditEventNotifyingParty.BuyerNotifies,
            ComplexEventCreditEventNotifyingParty.SellerOrBuyerNotifies,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventCreditEventNotifyingParty type', () => {
        const validate = (value: ComplexEventCreditEventNotifyingParty) => {
            expect(Object.values(ComplexEventCreditEventNotifyingParty)).toContain(value);
        };

        validate(ComplexEventCreditEventNotifyingParty.SellerNotifies);
        validate(ComplexEventCreditEventNotifyingParty.BuyerNotifies);
        validate(ComplexEventCreditEventNotifyingParty.SellerOrBuyerNotifies);
    });

    test('should be immutable', () => {
        const ref = ComplexEventCreditEventNotifyingParty;
        expect(() => {
            ref.SellerNotifies = 10;
        }).toThrowError();
    });
});
