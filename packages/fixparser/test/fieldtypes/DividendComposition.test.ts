import { DividendComposition } from '../../src/fieldtypes/DividendComposition';

describe('DividendComposition', () => {
    test('should have the correct values', () => {
        expect(DividendComposition.EquityAmountReceiver).toBe(0);
        expect(DividendComposition.CalculationAgent).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DividendComposition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DividendComposition.EquityAmountReceiver, DividendComposition.CalculationAgent];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DividendComposition type', () => {
        const validate = (value: DividendComposition) => {
            expect(Object.values(DividendComposition)).toContain(value);
        };

        validate(DividendComposition.EquityAmountReceiver);
        validate(DividendComposition.CalculationAgent);
    });

    test('should be immutable', () => {
        const ref = DividendComposition;
        expect(() => {
            ref.EquityAmountReceiver = 10;
        }).toThrowError();
    });
});
