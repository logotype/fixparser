import { RiskLimitCheckTransType } from '../../src/fieldtypes/RiskLimitCheckTransType';

describe('RiskLimitCheckTransType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckTransType.New).toBe(0);
        expect(RiskLimitCheckTransType.Cancel).toBe(1);
        expect(RiskLimitCheckTransType.Replace).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitCheckTransType.New,
            RiskLimitCheckTransType.Cancel,
            RiskLimitCheckTransType.Replace,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckTransType type', () => {
        const validate = (value: RiskLimitCheckTransType) => {
            expect(Object.values(RiskLimitCheckTransType)).toContain(value);
        };

        validate(RiskLimitCheckTransType.New);
        validate(RiskLimitCheckTransType.Cancel);
        validate(RiskLimitCheckTransType.Replace);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
