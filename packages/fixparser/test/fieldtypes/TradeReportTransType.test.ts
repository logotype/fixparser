import { TradeReportTransType } from '../../src/fieldtypes/TradeReportTransType';

describe('TradeReportTransType', () => {
    test('should have the correct values', () => {
        expect(TradeReportTransType.New).toBe(0);
        expect(TradeReportTransType.Cancel).toBe(1);
        expect(TradeReportTransType.Replace).toBe(2);
        expect(TradeReportTransType.Release).toBe(3);
        expect(TradeReportTransType.Reverse).toBe(4);
        expect(TradeReportTransType.CancelDueToBackOutOfTrade).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeReportTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeReportTransType.New,
            TradeReportTransType.Cancel,
            TradeReportTransType.Replace,
            TradeReportTransType.Release,
            TradeReportTransType.Reverse,
            TradeReportTransType.CancelDueToBackOutOfTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeReportTransType type', () => {
        const validate = (value: TradeReportTransType) => {
            expect(Object.values(TradeReportTransType)).toContain(value);
        };

        validate(TradeReportTransType.New);
        validate(TradeReportTransType.Cancel);
        validate(TradeReportTransType.Replace);
        validate(TradeReportTransType.Release);
        validate(TradeReportTransType.Reverse);
        validate(TradeReportTransType.CancelDueToBackOutOfTrade);
    });

    test('should be immutable', () => {
        const ref = TradeReportTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
