import { RiskLimitCheckType } from '../../src/fieldtypes/RiskLimitCheckType';

describe('RiskLimitCheckType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckType.Submit).toBe(0);
        expect(RiskLimitCheckType.LimitConsumed).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RiskLimitCheckType.Submit, RiskLimitCheckType.LimitConsumed];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckType type', () => {
        const validate = (value: RiskLimitCheckType) => {
            expect(Object.values(RiskLimitCheckType)).toContain(value);
        };

        validate(RiskLimitCheckType.Submit);
        validate(RiskLimitCheckType.LimitConsumed);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckType;
        expect(() => {
            ref.Submit = 10;
        }).toThrowError();
    });
});
