import { StreamAsgnType } from '../../src/fieldtypes/StreamAsgnType';

describe('StreamAsgnType', () => {
    test('should have the correct values', () => {
        expect(StreamAsgnType.Assignment).toBe(1);
        expect(StreamAsgnType.Rejected).toBe(2);
        expect(StreamAsgnType.Terminate).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamAsgnType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StreamAsgnType.Assignment, StreamAsgnType.Rejected, StreamAsgnType.Terminate];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamAsgnType type', () => {
        const validate = (value: StreamAsgnType) => {
            expect(Object.values(StreamAsgnType)).toContain(value);
        };

        validate(StreamAsgnType.Assignment);
        validate(StreamAsgnType.Rejected);
        validate(StreamAsgnType.Terminate);
    });

    test('should be immutable', () => {
        const ref = StreamAsgnType;
        expect(() => {
            ref.Assignment = 10;
        }).toThrowError();
    });
});
