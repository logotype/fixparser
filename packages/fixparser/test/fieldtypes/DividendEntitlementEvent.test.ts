import { DividendEntitlementEvent } from '../../src/fieldtypes/DividendEntitlementEvent';

describe('DividendEntitlementEvent', () => {
    test('should have the correct values', () => {
        expect(DividendEntitlementEvent.ExDate).toBe(0);
        expect(DividendEntitlementEvent.RecordDate).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DividendEntitlementEvent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DividendEntitlementEvent.ExDate, DividendEntitlementEvent.RecordDate];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DividendEntitlementEvent type', () => {
        const validate = (value: DividendEntitlementEvent) => {
            expect(Object.values(DividendEntitlementEvent)).toContain(value);
        };

        validate(DividendEntitlementEvent.ExDate);
        validate(DividendEntitlementEvent.RecordDate);
    });

    test('should be immutable', () => {
        const ref = DividendEntitlementEvent;
        expect(() => {
            ref.ExDate = 10;
        }).toThrowError();
    });
});
