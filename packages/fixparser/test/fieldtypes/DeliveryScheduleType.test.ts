import { DeliveryScheduleType } from '../../src/fieldtypes/DeliveryScheduleType';

describe('DeliveryScheduleType', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleType.Notional).toBe(0);
        expect(DeliveryScheduleType.Delivery).toBe(1);
        expect(DeliveryScheduleType.PhysicalSettlPeriods).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryScheduleType.Notional,
            DeliveryScheduleType.Delivery,
            DeliveryScheduleType.PhysicalSettlPeriods,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleType type', () => {
        const validate = (value: DeliveryScheduleType) => {
            expect(Object.values(DeliveryScheduleType)).toContain(value);
        };

        validate(DeliveryScheduleType.Notional);
        validate(DeliveryScheduleType.Delivery);
        validate(DeliveryScheduleType.PhysicalSettlPeriods);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleType;
        expect(() => {
            ref.Notional = 10;
        }).toThrowError();
    });
});
