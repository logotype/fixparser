import { DisclosureInstruction } from '../../src/fieldtypes/DisclosureInstruction';

describe('DisclosureInstruction', () => {
    test('should have the correct values', () => {
        expect(DisclosureInstruction.No).toBe(0);
        expect(DisclosureInstruction.Yes).toBe(1);
        expect(DisclosureInstruction.UseDefaultSetting).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DisclosureInstruction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DisclosureInstruction.No,
            DisclosureInstruction.Yes,
            DisclosureInstruction.UseDefaultSetting,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DisclosureInstruction type', () => {
        const validate = (value: DisclosureInstruction) => {
            expect(Object.values(DisclosureInstruction)).toContain(value);
        };

        validate(DisclosureInstruction.No);
        validate(DisclosureInstruction.Yes);
        validate(DisclosureInstruction.UseDefaultSetting);
    });

    test('should be immutable', () => {
        const ref = DisclosureInstruction;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
