import { PayReportStatus } from '../../src/fieldtypes/PayReportStatus';

describe('PayReportStatus', () => {
    test('should have the correct values', () => {
        expect(PayReportStatus.Received).toBe(0);
        expect(PayReportStatus.Accepted).toBe(1);
        expect(PayReportStatus.Rejected).toBe(2);
        expect(PayReportStatus.Disputed).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PayReportStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PayReportStatus.Received,
            PayReportStatus.Accepted,
            PayReportStatus.Rejected,
            PayReportStatus.Disputed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PayReportStatus type', () => {
        const validate = (value: PayReportStatus) => {
            expect(Object.values(PayReportStatus)).toContain(value);
        };

        validate(PayReportStatus.Received);
        validate(PayReportStatus.Accepted);
        validate(PayReportStatus.Rejected);
        validate(PayReportStatus.Disputed);
    });

    test('should be immutable', () => {
        const ref = PayReportStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
