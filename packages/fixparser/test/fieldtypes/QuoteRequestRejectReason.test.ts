import { QuoteRequestRejectReason } from '../../src/fieldtypes/QuoteRequestRejectReason';

describe('QuoteRequestRejectReason', () => {
    test('should have the correct values', () => {
        expect(QuoteRequestRejectReason.UnknownSymbol).toBe(1);
        expect(QuoteRequestRejectReason.Exchange).toBe(2);
        expect(QuoteRequestRejectReason.QuoteRequestExceedsLimit).toBe(3);
        expect(QuoteRequestRejectReason.TooLateToEnter).toBe(4);
        expect(QuoteRequestRejectReason.InvalidPrice).toBe(5);
        expect(QuoteRequestRejectReason.NotAuthorizedToRequestQuote).toBe(6);
        expect(QuoteRequestRejectReason.NoMatchForInquiry).toBe(7);
        expect(QuoteRequestRejectReason.NoMarketForInstrument).toBe(8);
        expect(QuoteRequestRejectReason.NoInventory).toBe(9);
        expect(QuoteRequestRejectReason.Pass).toBe(10);
        expect(QuoteRequestRejectReason.InsufficientCredit).toBe(11);
        expect(QuoteRequestRejectReason.ExceededClipSizeLimit).toBe(12);
        expect(QuoteRequestRejectReason.ExceededMaxNotionalOrderAmt).toBe(13);
        expect(QuoteRequestRejectReason.ExceededDV01PV01Limit).toBe(14);
        expect(QuoteRequestRejectReason.ExceededCS01Limit).toBe(15);
        expect(QuoteRequestRejectReason.UnavailablePriceLiquidity).toBe(16);
        expect(QuoteRequestRejectReason.UnmetRegulatoryRequirement).toBe(17);
        expect(QuoteRequestRejectReason.UnknownAccounts).toBe(18);
        expect(QuoteRequestRejectReason.InvalidMissingEntitlements).toBe(19);
        expect(QuoteRequestRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteRequestRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteRequestRejectReason.UnknownSymbol,
            QuoteRequestRejectReason.Exchange,
            QuoteRequestRejectReason.QuoteRequestExceedsLimit,
            QuoteRequestRejectReason.TooLateToEnter,
            QuoteRequestRejectReason.InvalidPrice,
            QuoteRequestRejectReason.NotAuthorizedToRequestQuote,
            QuoteRequestRejectReason.NoMatchForInquiry,
            QuoteRequestRejectReason.NoMarketForInstrument,
            QuoteRequestRejectReason.NoInventory,
            QuoteRequestRejectReason.Pass,
            QuoteRequestRejectReason.InsufficientCredit,
            QuoteRequestRejectReason.ExceededClipSizeLimit,
            QuoteRequestRejectReason.ExceededMaxNotionalOrderAmt,
            QuoteRequestRejectReason.ExceededDV01PV01Limit,
            QuoteRequestRejectReason.ExceededCS01Limit,
            QuoteRequestRejectReason.UnavailablePriceLiquidity,
            QuoteRequestRejectReason.UnmetRegulatoryRequirement,
            QuoteRequestRejectReason.UnknownAccounts,
            QuoteRequestRejectReason.InvalidMissingEntitlements,
            QuoteRequestRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteRequestRejectReason type', () => {
        const validate = (value: QuoteRequestRejectReason) => {
            expect(Object.values(QuoteRequestRejectReason)).toContain(value);
        };

        validate(QuoteRequestRejectReason.UnknownSymbol);
        validate(QuoteRequestRejectReason.Exchange);
        validate(QuoteRequestRejectReason.QuoteRequestExceedsLimit);
        validate(QuoteRequestRejectReason.TooLateToEnter);
        validate(QuoteRequestRejectReason.InvalidPrice);
        validate(QuoteRequestRejectReason.NotAuthorizedToRequestQuote);
        validate(QuoteRequestRejectReason.NoMatchForInquiry);
        validate(QuoteRequestRejectReason.NoMarketForInstrument);
        validate(QuoteRequestRejectReason.NoInventory);
        validate(QuoteRequestRejectReason.Pass);
        validate(QuoteRequestRejectReason.InsufficientCredit);
        validate(QuoteRequestRejectReason.ExceededClipSizeLimit);
        validate(QuoteRequestRejectReason.ExceededMaxNotionalOrderAmt);
        validate(QuoteRequestRejectReason.ExceededDV01PV01Limit);
        validate(QuoteRequestRejectReason.ExceededCS01Limit);
        validate(QuoteRequestRejectReason.UnavailablePriceLiquidity);
        validate(QuoteRequestRejectReason.UnmetRegulatoryRequirement);
        validate(QuoteRequestRejectReason.UnknownAccounts);
        validate(QuoteRequestRejectReason.InvalidMissingEntitlements);
        validate(QuoteRequestRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = QuoteRequestRejectReason;
        expect(() => {
            ref.UnknownSymbol = 10;
        }).toThrowError();
    });
});
