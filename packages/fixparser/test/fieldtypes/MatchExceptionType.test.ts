import { MatchExceptionType } from '../../src/fieldtypes/MatchExceptionType';

describe('MatchExceptionType', () => {
    test('should have the correct values', () => {
        expect(MatchExceptionType.NoMatchingConfirmation).toBe(0);
        expect(MatchExceptionType.NoMatchingAllocation).toBe(1);
        expect(MatchExceptionType.AllocationDataElementMissing).toBe(2);
        expect(MatchExceptionType.ConfirmationDataElementMissing).toBe(3);
        expect(MatchExceptionType.DataDifferenceNotWithinTolerance).toBe(4);
        expect(MatchExceptionType.MatchWithinTolerance).toBe(5);
        expect(MatchExceptionType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchExceptionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MatchExceptionType.NoMatchingConfirmation,
            MatchExceptionType.NoMatchingAllocation,
            MatchExceptionType.AllocationDataElementMissing,
            MatchExceptionType.ConfirmationDataElementMissing,
            MatchExceptionType.DataDifferenceNotWithinTolerance,
            MatchExceptionType.MatchWithinTolerance,
            MatchExceptionType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MatchExceptionType type', () => {
        const validate = (value: MatchExceptionType) => {
            expect(Object.values(MatchExceptionType)).toContain(value);
        };

        validate(MatchExceptionType.NoMatchingConfirmation);
        validate(MatchExceptionType.NoMatchingAllocation);
        validate(MatchExceptionType.AllocationDataElementMissing);
        validate(MatchExceptionType.ConfirmationDataElementMissing);
        validate(MatchExceptionType.DataDifferenceNotWithinTolerance);
        validate(MatchExceptionType.MatchWithinTolerance);
        validate(MatchExceptionType.Other);
    });

    test('should be immutable', () => {
        const ref = MatchExceptionType;
        expect(() => {
            ref.NoMatchingConfirmation = 10;
        }).toThrowError();
    });
});
