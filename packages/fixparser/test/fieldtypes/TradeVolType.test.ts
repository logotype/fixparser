import { TradeVolType } from '../../src/fieldtypes/TradeVolType';

describe('TradeVolType', () => {
    test('should have the correct values', () => {
        expect(TradeVolType.NumberOfUnits).toBe(0);
        expect(TradeVolType.NumberOfRoundLots).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeVolType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradeVolType.NumberOfUnits, TradeVolType.NumberOfRoundLots];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeVolType type', () => {
        const validate = (value: TradeVolType) => {
            expect(Object.values(TradeVolType)).toContain(value);
        };

        validate(TradeVolType.NumberOfUnits);
        validate(TradeVolType.NumberOfRoundLots);
    });

    test('should be immutable', () => {
        const ref = TradeVolType;
        expect(() => {
            ref.NumberOfUnits = 10;
        }).toThrowError();
    });
});
