import { OrigCustOrderCapacity } from '../../src/fieldtypes/OrigCustOrderCapacity';

describe('OrigCustOrderCapacity', () => {
    test('should have the correct values', () => {
        expect(OrigCustOrderCapacity.MemberTradingForTheirOwnAccount).toBe(1);
        expect(OrigCustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount).toBe(2);
        expect(OrigCustOrderCapacity.MemberTradingForAnotherMember).toBe(3);
        expect(OrigCustOrderCapacity.AllOther).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrigCustOrderCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrigCustOrderCapacity.MemberTradingForTheirOwnAccount,
            OrigCustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount,
            OrigCustOrderCapacity.MemberTradingForAnotherMember,
            OrigCustOrderCapacity.AllOther,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrigCustOrderCapacity type', () => {
        const validate = (value: OrigCustOrderCapacity) => {
            expect(Object.values(OrigCustOrderCapacity)).toContain(value);
        };

        validate(OrigCustOrderCapacity.MemberTradingForTheirOwnAccount);
        validate(OrigCustOrderCapacity.ClearingFirmTradingForItsProprietaryAccount);
        validate(OrigCustOrderCapacity.MemberTradingForAnotherMember);
        validate(OrigCustOrderCapacity.AllOther);
    });

    test('should be immutable', () => {
        const ref = OrigCustOrderCapacity;
        expect(() => {
            ref.MemberTradingForTheirOwnAccount = 10;
        }).toThrowError();
    });
});
