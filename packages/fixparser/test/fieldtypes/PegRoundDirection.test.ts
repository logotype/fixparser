import { PegRoundDirection } from '../../src/fieldtypes/PegRoundDirection';

describe('PegRoundDirection', () => {
    test('should have the correct values', () => {
        expect(PegRoundDirection.MoreAggressive).toBe(1);
        expect(PegRoundDirection.MorePassive).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegRoundDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PegRoundDirection.MoreAggressive, PegRoundDirection.MorePassive];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegRoundDirection type', () => {
        const validate = (value: PegRoundDirection) => {
            expect(Object.values(PegRoundDirection)).toContain(value);
        };

        validate(PegRoundDirection.MoreAggressive);
        validate(PegRoundDirection.MorePassive);
    });

    test('should be immutable', () => {
        const ref = PegRoundDirection;
        expect(() => {
            ref.MoreAggressive = 10;
        }).toThrowError();
    });
});
