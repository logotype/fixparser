import { EventInitiatorType } from '../../src/fieldtypes/EventInitiatorType';

describe('EventInitiatorType', () => {
    test('should have the correct values', () => {
        expect(EventInitiatorType.CustomerOrClient).toBe('C');
        expect(EventInitiatorType.ExchangeOrExecutionVenue).toBe('E');
        expect(EventInitiatorType.FirmOrBroker).toBe('F');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EventInitiatorType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EventInitiatorType.CustomerOrClient,
            EventInitiatorType.ExchangeOrExecutionVenue,
            EventInitiatorType.FirmOrBroker,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for EventInitiatorType type', () => {
        const validate = (value: EventInitiatorType) => {
            expect(Object.values(EventInitiatorType)).toContain(value);
        };

        validate(EventInitiatorType.CustomerOrClient);
        validate(EventInitiatorType.ExchangeOrExecutionVenue);
        validate(EventInitiatorType.FirmOrBroker);
    });

    test('should be immutable', () => {
        const ref = EventInitiatorType;
        expect(() => {
            ref.CustomerOrClient = 10;
        }).toThrowError();
    });
});
