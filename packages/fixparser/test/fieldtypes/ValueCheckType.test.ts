import { ValueCheckType } from '../../src/fieldtypes/ValueCheckType';

describe('ValueCheckType', () => {
    test('should have the correct values', () => {
        expect(ValueCheckType.PriceCheck).toBe(1);
        expect(ValueCheckType.NotionalValueCheck).toBe(2);
        expect(ValueCheckType.QuantityCheck).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ValueCheckType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ValueCheckType.PriceCheck,
            ValueCheckType.NotionalValueCheck,
            ValueCheckType.QuantityCheck,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ValueCheckType type', () => {
        const validate = (value: ValueCheckType) => {
            expect(Object.values(ValueCheckType)).toContain(value);
        };

        validate(ValueCheckType.PriceCheck);
        validate(ValueCheckType.NotionalValueCheck);
        validate(ValueCheckType.QuantityCheck);
    });

    test('should be immutable', () => {
        const ref = ValueCheckType;
        expect(() => {
            ref.PriceCheck = 10;
        }).toThrowError();
    });
});
