import { SettlInstMode } from '../../src/fieldtypes/SettlInstMode';

describe('SettlInstMode', () => {
    test('should have the correct values', () => {
        expect(SettlInstMode.Default).toBe('0');
        expect(SettlInstMode.StandingInstructionsProvided).toBe('1');
        expect(SettlInstMode.SpecificAllocationAccountOverriding).toBe('2');
        expect(SettlInstMode.SpecificAllocationAccountStanding).toBe('3');
        expect(SettlInstMode.SpecificOrderForASingleAccount).toBe('4');
        expect(SettlInstMode.RequestReject).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlInstMode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlInstMode.Default,
            SettlInstMode.StandingInstructionsProvided,
            SettlInstMode.SpecificAllocationAccountOverriding,
            SettlInstMode.SpecificAllocationAccountStanding,
            SettlInstMode.SpecificOrderForASingleAccount,
            SettlInstMode.RequestReject,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlInstMode type', () => {
        const validate = (value: SettlInstMode) => {
            expect(Object.values(SettlInstMode)).toContain(value);
        };

        validate(SettlInstMode.Default);
        validate(SettlInstMode.StandingInstructionsProvided);
        validate(SettlInstMode.SpecificAllocationAccountOverriding);
        validate(SettlInstMode.SpecificAllocationAccountStanding);
        validate(SettlInstMode.SpecificOrderForASingleAccount);
        validate(SettlInstMode.RequestReject);
    });

    test('should be immutable', () => {
        const ref = SettlInstMode;
        expect(() => {
            ref.Default = 10;
        }).toThrowError();
    });
});
