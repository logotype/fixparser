import { MultilegPriceMethod } from '../../src/fieldtypes/MultilegPriceMethod';

describe('MultilegPriceMethod', () => {
    test('should have the correct values', () => {
        expect(MultilegPriceMethod.NetPrice).toBe(0);
        expect(MultilegPriceMethod.ReversedNetPrice).toBe(1);
        expect(MultilegPriceMethod.YieldDifference).toBe(2);
        expect(MultilegPriceMethod.Individual).toBe(3);
        expect(MultilegPriceMethod.ContractWeightedAveragePrice).toBe(4);
        expect(MultilegPriceMethod.MultipliedPrice).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MultilegPriceMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MultilegPriceMethod.NetPrice,
            MultilegPriceMethod.ReversedNetPrice,
            MultilegPriceMethod.YieldDifference,
            MultilegPriceMethod.Individual,
            MultilegPriceMethod.ContractWeightedAveragePrice,
            MultilegPriceMethod.MultipliedPrice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MultilegPriceMethod type', () => {
        const validate = (value: MultilegPriceMethod) => {
            expect(Object.values(MultilegPriceMethod)).toContain(value);
        };

        validate(MultilegPriceMethod.NetPrice);
        validate(MultilegPriceMethod.ReversedNetPrice);
        validate(MultilegPriceMethod.YieldDifference);
        validate(MultilegPriceMethod.Individual);
        validate(MultilegPriceMethod.ContractWeightedAveragePrice);
        validate(MultilegPriceMethod.MultipliedPrice);
    });

    test('should be immutable', () => {
        const ref = MultilegPriceMethod;
        expect(() => {
            ref.NetPrice = 10;
        }).toThrowError();
    });
});
