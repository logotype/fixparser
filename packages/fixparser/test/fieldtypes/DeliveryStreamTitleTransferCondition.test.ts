import { DeliveryStreamTitleTransferCondition } from '../../src/fieldtypes/DeliveryStreamTitleTransferCondition';

describe('DeliveryStreamTitleTransferCondition', () => {
    test('should have the correct values', () => {
        expect(DeliveryStreamTitleTransferCondition.Transfers).toBe(0);
        expect(DeliveryStreamTitleTransferCondition.DoesNotTransfer).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryStreamTitleTransferCondition.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DeliveryStreamTitleTransferCondition.Transfers,
            DeliveryStreamTitleTransferCondition.DoesNotTransfer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryStreamTitleTransferCondition type', () => {
        const validate = (value: DeliveryStreamTitleTransferCondition) => {
            expect(Object.values(DeliveryStreamTitleTransferCondition)).toContain(value);
        };

        validate(DeliveryStreamTitleTransferCondition.Transfers);
        validate(DeliveryStreamTitleTransferCondition.DoesNotTransfer);
    });

    test('should be immutable', () => {
        const ref = DeliveryStreamTitleTransferCondition;
        expect(() => {
            ref.Transfers = 10;
        }).toThrowError();
    });
});
