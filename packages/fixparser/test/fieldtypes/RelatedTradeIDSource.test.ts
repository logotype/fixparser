import { RelatedTradeIDSource } from '../../src/fieldtypes/RelatedTradeIDSource';

describe('RelatedTradeIDSource', () => {
    test('should have the correct values', () => {
        expect(RelatedTradeIDSource.NonFIXSource).toBe(0);
        expect(RelatedTradeIDSource.TradeID).toBe(1);
        expect(RelatedTradeIDSource.SecondaryTradeID).toBe(2);
        expect(RelatedTradeIDSource.TradeReportID).toBe(3);
        expect(RelatedTradeIDSource.FirmTradeID).toBe(4);
        expect(RelatedTradeIDSource.SecondaryFirmTradeID).toBe(5);
        expect(RelatedTradeIDSource.RegulatoryTradeID).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelatedTradeIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RelatedTradeIDSource.NonFIXSource,
            RelatedTradeIDSource.TradeID,
            RelatedTradeIDSource.SecondaryTradeID,
            RelatedTradeIDSource.TradeReportID,
            RelatedTradeIDSource.FirmTradeID,
            RelatedTradeIDSource.SecondaryFirmTradeID,
            RelatedTradeIDSource.RegulatoryTradeID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelatedTradeIDSource type', () => {
        const validate = (value: RelatedTradeIDSource) => {
            expect(Object.values(RelatedTradeIDSource)).toContain(value);
        };

        validate(RelatedTradeIDSource.NonFIXSource);
        validate(RelatedTradeIDSource.TradeID);
        validate(RelatedTradeIDSource.SecondaryTradeID);
        validate(RelatedTradeIDSource.TradeReportID);
        validate(RelatedTradeIDSource.FirmTradeID);
        validate(RelatedTradeIDSource.SecondaryFirmTradeID);
        validate(RelatedTradeIDSource.RegulatoryTradeID);
    });

    test('should be immutable', () => {
        const ref = RelatedTradeIDSource;
        expect(() => {
            ref.NonFIXSource = 10;
        }).toThrowError();
    });
});
