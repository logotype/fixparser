import { TradeRequestResult } from '../../src/fieldtypes/TradeRequestResult';

describe('TradeRequestResult', () => {
    test('should have the correct values', () => {
        expect(TradeRequestResult.Successful).toBe(0);
        expect(TradeRequestResult.InvalidOrUnknownInstrument).toBe(1);
        expect(TradeRequestResult.InvalidTypeOfTradeRequested).toBe(2);
        expect(TradeRequestResult.InvalidParties).toBe(3);
        expect(TradeRequestResult.InvalidTransportTypeRequested).toBe(4);
        expect(TradeRequestResult.InvalidDestinationRequested).toBe(5);
        expect(TradeRequestResult.TradeRequestTypeNotSupported).toBe(8);
        expect(TradeRequestResult.NotAuthorized).toBe(9);
        expect(TradeRequestResult.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeRequestResult.Successful,
            TradeRequestResult.InvalidOrUnknownInstrument,
            TradeRequestResult.InvalidTypeOfTradeRequested,
            TradeRequestResult.InvalidParties,
            TradeRequestResult.InvalidTransportTypeRequested,
            TradeRequestResult.InvalidDestinationRequested,
            TradeRequestResult.TradeRequestTypeNotSupported,
            TradeRequestResult.NotAuthorized,
            TradeRequestResult.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeRequestResult type', () => {
        const validate = (value: TradeRequestResult) => {
            expect(Object.values(TradeRequestResult)).toContain(value);
        };

        validate(TradeRequestResult.Successful);
        validate(TradeRequestResult.InvalidOrUnknownInstrument);
        validate(TradeRequestResult.InvalidTypeOfTradeRequested);
        validate(TradeRequestResult.InvalidParties);
        validate(TradeRequestResult.InvalidTransportTypeRequested);
        validate(TradeRequestResult.InvalidDestinationRequested);
        validate(TradeRequestResult.TradeRequestTypeNotSupported);
        validate(TradeRequestResult.NotAuthorized);
        validate(TradeRequestResult.Other);
    });

    test('should be immutable', () => {
        const ref = TradeRequestResult;
        expect(() => {
            ref.Successful = 10;
        }).toThrowError();
    });
});
