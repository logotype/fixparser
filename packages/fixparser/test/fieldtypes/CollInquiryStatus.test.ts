import { CollInquiryStatus } from '../../src/fieldtypes/CollInquiryStatus';

describe('CollInquiryStatus', () => {
    test('should have the correct values', () => {
        expect(CollInquiryStatus.Accepted).toBe(0);
        expect(CollInquiryStatus.AcceptedWithWarnings).toBe(1);
        expect(CollInquiryStatus.Completed).toBe(2);
        expect(CollInquiryStatus.CompletedWithWarnings).toBe(3);
        expect(CollInquiryStatus.Rejected).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollInquiryStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollInquiryStatus.Accepted,
            CollInquiryStatus.AcceptedWithWarnings,
            CollInquiryStatus.Completed,
            CollInquiryStatus.CompletedWithWarnings,
            CollInquiryStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollInquiryStatus type', () => {
        const validate = (value: CollInquiryStatus) => {
            expect(Object.values(CollInquiryStatus)).toContain(value);
        };

        validate(CollInquiryStatus.Accepted);
        validate(CollInquiryStatus.AcceptedWithWarnings);
        validate(CollInquiryStatus.Completed);
        validate(CollInquiryStatus.CompletedWithWarnings);
        validate(CollInquiryStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = CollInquiryStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
