import { PaymentSettlStyle } from '../../src/fieldtypes/PaymentSettlStyle';

describe('PaymentSettlStyle', () => {
    test('should have the correct values', () => {
        expect(PaymentSettlStyle.Standard).toBe(0);
        expect(PaymentSettlStyle.Net).toBe(1);
        expect(PaymentSettlStyle.StandardfNet).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentSettlStyle.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentSettlStyle.Standard, PaymentSettlStyle.Net, PaymentSettlStyle.StandardfNet];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentSettlStyle type', () => {
        const validate = (value: PaymentSettlStyle) => {
            expect(Object.values(PaymentSettlStyle)).toContain(value);
        };

        validate(PaymentSettlStyle.Standard);
        validate(PaymentSettlStyle.Net);
        validate(PaymentSettlStyle.StandardfNet);
    });

    test('should be immutable', () => {
        const ref = PaymentSettlStyle;
        expect(() => {
            ref.Standard = 10;
        }).toThrowError();
    });
});
