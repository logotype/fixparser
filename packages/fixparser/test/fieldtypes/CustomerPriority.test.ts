import { CustomerPriority } from '../../src/fieldtypes/CustomerPriority';

describe('CustomerPriority', () => {
    test('should have the correct values', () => {
        expect(CustomerPriority.NoPriority).toBe(0);
        expect(CustomerPriority.UnconditionalPriority).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CustomerPriority.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CustomerPriority.NoPriority, CustomerPriority.UnconditionalPriority];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CustomerPriority type', () => {
        const validate = (value: CustomerPriority) => {
            expect(Object.values(CustomerPriority)).toContain(value);
        };

        validate(CustomerPriority.NoPriority);
        validate(CustomerPriority.UnconditionalPriority);
    });

    test('should be immutable', () => {
        const ref = CustomerPriority;
        expect(() => {
            ref.NoPriority = 10;
        }).toThrowError();
    });
});
