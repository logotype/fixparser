import { TradeMatchAckStatus } from '../../src/fieldtypes/TradeMatchAckStatus';

describe('TradeMatchAckStatus', () => {
    test('should have the correct values', () => {
        expect(TradeMatchAckStatus.ReceivedNotProcessed).toBe(0);
        expect(TradeMatchAckStatus.Accepted).toBe(1);
        expect(TradeMatchAckStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeMatchAckStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeMatchAckStatus.ReceivedNotProcessed,
            TradeMatchAckStatus.Accepted,
            TradeMatchAckStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeMatchAckStatus type', () => {
        const validate = (value: TradeMatchAckStatus) => {
            expect(Object.values(TradeMatchAckStatus)).toContain(value);
        };

        validate(TradeMatchAckStatus.ReceivedNotProcessed);
        validate(TradeMatchAckStatus.Accepted);
        validate(TradeMatchAckStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = TradeMatchAckStatus;
        expect(() => {
            ref.ReceivedNotProcessed = 10;
        }).toThrowError();
    });
});
