import { StreamNotionalCommodityFrequency } from '../../src/fieldtypes/StreamNotionalCommodityFrequency';

describe('StreamNotionalCommodityFrequency', () => {
    test('should have the correct values', () => {
        expect(StreamNotionalCommodityFrequency.Term).toBe(0);
        expect(StreamNotionalCommodityFrequency.PerBusinessDay).toBe(1);
        expect(StreamNotionalCommodityFrequency.PerCalculationPeriod).toBe(2);
        expect(StreamNotionalCommodityFrequency.PerSettlPeriod).toBe(3);
        expect(StreamNotionalCommodityFrequency.PerCalendarDay).toBe(4);
        expect(StreamNotionalCommodityFrequency.PerHour).toBe(5);
        expect(StreamNotionalCommodityFrequency.PerMonth).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamNotionalCommodityFrequency.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StreamNotionalCommodityFrequency.Term,
            StreamNotionalCommodityFrequency.PerBusinessDay,
            StreamNotionalCommodityFrequency.PerCalculationPeriod,
            StreamNotionalCommodityFrequency.PerSettlPeriod,
            StreamNotionalCommodityFrequency.PerCalendarDay,
            StreamNotionalCommodityFrequency.PerHour,
            StreamNotionalCommodityFrequency.PerMonth,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamNotionalCommodityFrequency type', () => {
        const validate = (value: StreamNotionalCommodityFrequency) => {
            expect(Object.values(StreamNotionalCommodityFrequency)).toContain(value);
        };

        validate(StreamNotionalCommodityFrequency.Term);
        validate(StreamNotionalCommodityFrequency.PerBusinessDay);
        validate(StreamNotionalCommodityFrequency.PerCalculationPeriod);
        validate(StreamNotionalCommodityFrequency.PerSettlPeriod);
        validate(StreamNotionalCommodityFrequency.PerCalendarDay);
        validate(StreamNotionalCommodityFrequency.PerHour);
        validate(StreamNotionalCommodityFrequency.PerMonth);
    });

    test('should be immutable', () => {
        const ref = StreamNotionalCommodityFrequency;
        expect(() => {
            ref.Term = 10;
        }).toThrowError();
    });
});
