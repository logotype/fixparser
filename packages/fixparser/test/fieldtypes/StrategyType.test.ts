import { StrategyType } from '../../src/fieldtypes/StrategyType';

describe('StrategyType', () => {
    test('should have the correct values', () => {
        expect(StrategyType.Straddle).toBe('STD');
        expect(StrategyType.Strangle).toBe('STG');
        expect(StrategyType.Butterfly).toBe('BF');
        expect(StrategyType.Condor).toBe('CNDR');
        expect(StrategyType.CallableInversibleSnowball).toBe('CISN');
        expect(StrategyType.Other).toBe('OTHER');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StrategyType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StrategyType.Straddle,
            StrategyType.Strangle,
            StrategyType.Butterfly,
            StrategyType.Condor,
            StrategyType.CallableInversibleSnowball,
            StrategyType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for StrategyType type', () => {
        const validate = (value: StrategyType) => {
            expect(Object.values(StrategyType)).toContain(value);
        };

        validate(StrategyType.Straddle);
        validate(StrategyType.Strangle);
        validate(StrategyType.Butterfly);
        validate(StrategyType.Condor);
        validate(StrategyType.CallableInversibleSnowball);
        validate(StrategyType.Other);
    });

    test('should be immutable', () => {
        const ref = StrategyType;
        expect(() => {
            ref.Straddle = 10;
        }).toThrowError();
    });
});
