import { StrikeIndexQuote } from '../../src/fieldtypes/StrikeIndexQuote';

describe('StrikeIndexQuote', () => {
    test('should have the correct values', () => {
        expect(StrikeIndexQuote.Bid).toBe(0);
        expect(StrikeIndexQuote.Mid).toBe(1);
        expect(StrikeIndexQuote.Offer).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StrikeIndexQuote.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StrikeIndexQuote.Bid, StrikeIndexQuote.Mid, StrikeIndexQuote.Offer];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StrikeIndexQuote type', () => {
        const validate = (value: StrikeIndexQuote) => {
            expect(Object.values(StrikeIndexQuote)).toContain(value);
        };

        validate(StrikeIndexQuote.Bid);
        validate(StrikeIndexQuote.Mid);
        validate(StrikeIndexQuote.Offer);
    });

    test('should be immutable', () => {
        const ref = StrikeIndexQuote;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
