import { QuoteType } from '../../src/fieldtypes/QuoteType';

describe('QuoteType', () => {
    test('should have the correct values', () => {
        expect(QuoteType.Indicative).toBe(0);
        expect(QuoteType.Tradeable).toBe(1);
        expect(QuoteType.RestrictedTradeable).toBe(2);
        expect(QuoteType.Counter).toBe(3);
        expect(QuoteType.InitiallyTradeable).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteType.Indicative,
            QuoteType.Tradeable,
            QuoteType.RestrictedTradeable,
            QuoteType.Counter,
            QuoteType.InitiallyTradeable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteType type', () => {
        const validate = (value: QuoteType) => {
            expect(Object.values(QuoteType)).toContain(value);
        };

        validate(QuoteType.Indicative);
        validate(QuoteType.Tradeable);
        validate(QuoteType.RestrictedTradeable);
        validate(QuoteType.Counter);
        validate(QuoteType.InitiallyTradeable);
    });

    test('should be immutable', () => {
        const ref = QuoteType;
        expect(() => {
            ref.Indicative = 10;
        }).toThrowError();
    });
});
