import { MarginDirection } from '../../src/fieldtypes/MarginDirection';

describe('MarginDirection', () => {
    test('should have the correct values', () => {
        expect(MarginDirection.Posted).toBe(0);
        expect(MarginDirection.Received).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarginDirection.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MarginDirection.Posted, MarginDirection.Received];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarginDirection type', () => {
        const validate = (value: MarginDirection) => {
            expect(Object.values(MarginDirection)).toContain(value);
        };

        validate(MarginDirection.Posted);
        validate(MarginDirection.Received);
    });

    test('should be immutable', () => {
        const ref = MarginDirection;
        expect(() => {
            ref.Posted = 10;
        }).toThrowError();
    });
});
