import { MassCancelRejectReason } from '../../src/fieldtypes/MassCancelRejectReason';

describe('MassCancelRejectReason', () => {
    test('should have the correct values', () => {
        expect(MassCancelRejectReason.MassCancelNotSupported).toBe(0);
        expect(MassCancelRejectReason.InvalidOrUnknownSecurity).toBe(1);
        expect(MassCancelRejectReason.InvalidOrUnknownUnderlyingSecurity).toBe(2);
        expect(MassCancelRejectReason.InvalidOrUnknownProduct).toBe(3);
        expect(MassCancelRejectReason.InvalidOrUnknownCFICode).toBe(4);
        expect(MassCancelRejectReason.InvalidOrUnknownSecurityType).toBe(5);
        expect(MassCancelRejectReason.InvalidOrUnknownTradingSession).toBe(6);
        expect(MassCancelRejectReason.InvalidOrUnknownMarket).toBe(7);
        expect(MassCancelRejectReason.InvalidOrUnkownMarketSegment).toBe(8);
        expect(MassCancelRejectReason.InvalidOrUnknownSecurityGroup).toBe(9);
        expect(MassCancelRejectReason.InvalidOrUnknownSecurityIssuer).toBe(10);
        expect(MassCancelRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity).toBe(11);
        expect(MassCancelRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassCancelRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassCancelRejectReason.MassCancelNotSupported,
            MassCancelRejectReason.InvalidOrUnknownSecurity,
            MassCancelRejectReason.InvalidOrUnknownUnderlyingSecurity,
            MassCancelRejectReason.InvalidOrUnknownProduct,
            MassCancelRejectReason.InvalidOrUnknownCFICode,
            MassCancelRejectReason.InvalidOrUnknownSecurityType,
            MassCancelRejectReason.InvalidOrUnknownTradingSession,
            MassCancelRejectReason.InvalidOrUnknownMarket,
            MassCancelRejectReason.InvalidOrUnkownMarketSegment,
            MassCancelRejectReason.InvalidOrUnknownSecurityGroup,
            MassCancelRejectReason.InvalidOrUnknownSecurityIssuer,
            MassCancelRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity,
            MassCancelRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MassCancelRejectReason type', () => {
        const validate = (value: MassCancelRejectReason) => {
            expect(Object.values(MassCancelRejectReason)).toContain(value);
        };

        validate(MassCancelRejectReason.MassCancelNotSupported);
        validate(MassCancelRejectReason.InvalidOrUnknownSecurity);
        validate(MassCancelRejectReason.InvalidOrUnknownUnderlyingSecurity);
        validate(MassCancelRejectReason.InvalidOrUnknownProduct);
        validate(MassCancelRejectReason.InvalidOrUnknownCFICode);
        validate(MassCancelRejectReason.InvalidOrUnknownSecurityType);
        validate(MassCancelRejectReason.InvalidOrUnknownTradingSession);
        validate(MassCancelRejectReason.InvalidOrUnknownMarket);
        validate(MassCancelRejectReason.InvalidOrUnkownMarketSegment);
        validate(MassCancelRejectReason.InvalidOrUnknownSecurityGroup);
        validate(MassCancelRejectReason.InvalidOrUnknownSecurityIssuer);
        validate(MassCancelRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity);
        validate(MassCancelRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = MassCancelRejectReason;
        expect(() => {
            ref.MassCancelNotSupported = 10;
        }).toThrowError();
    });
});
