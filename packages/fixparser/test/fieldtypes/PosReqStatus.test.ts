import { PosReqStatus } from '../../src/fieldtypes/PosReqStatus';

describe('PosReqStatus', () => {
    test('should have the correct values', () => {
        expect(PosReqStatus.Completed).toBe(0);
        expect(PosReqStatus.CompletedWithWarnings).toBe(1);
        expect(PosReqStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosReqStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PosReqStatus.Completed, PosReqStatus.CompletedWithWarnings, PosReqStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosReqStatus type', () => {
        const validate = (value: PosReqStatus) => {
            expect(Object.values(PosReqStatus)).toContain(value);
        };

        validate(PosReqStatus.Completed);
        validate(PosReqStatus.CompletedWithWarnings);
        validate(PosReqStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = PosReqStatus;
        expect(() => {
            ref.Completed = 10;
        }).toThrowError();
    });
});
