import { SolicitedFlag } from '../../src/fieldtypes/SolicitedFlag';

describe('SolicitedFlag', () => {
    test('should have the correct values', () => {
        expect(SolicitedFlag.WasNotSolicited).toBe('N');
        expect(SolicitedFlag.WasSolicited).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SolicitedFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SolicitedFlag.WasNotSolicited, SolicitedFlag.WasSolicited];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SolicitedFlag type', () => {
        const validate = (value: SolicitedFlag) => {
            expect(Object.values(SolicitedFlag)).toContain(value);
        };

        validate(SolicitedFlag.WasNotSolicited);
        validate(SolicitedFlag.WasSolicited);
    });

    test('should be immutable', () => {
        const ref = SolicitedFlag;
        expect(() => {
            ref.WasNotSolicited = 10;
        }).toThrowError();
    });
});
