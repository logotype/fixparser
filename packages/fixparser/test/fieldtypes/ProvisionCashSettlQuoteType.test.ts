import { ProvisionCashSettlQuoteType } from '../../src/fieldtypes/ProvisionCashSettlQuoteType';

describe('ProvisionCashSettlQuoteType', () => {
    test('should have the correct values', () => {
        expect(ProvisionCashSettlQuoteType.Bid).toBe(0);
        expect(ProvisionCashSettlQuoteType.Mid).toBe(1);
        expect(ProvisionCashSettlQuoteType.Offer).toBe(2);
        expect(ProvisionCashSettlQuoteType.ExercisingPartyPays).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionCashSettlQuoteType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionCashSettlQuoteType.Bid,
            ProvisionCashSettlQuoteType.Mid,
            ProvisionCashSettlQuoteType.Offer,
            ProvisionCashSettlQuoteType.ExercisingPartyPays,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionCashSettlQuoteType type', () => {
        const validate = (value: ProvisionCashSettlQuoteType) => {
            expect(Object.values(ProvisionCashSettlQuoteType)).toContain(value);
        };

        validate(ProvisionCashSettlQuoteType.Bid);
        validate(ProvisionCashSettlQuoteType.Mid);
        validate(ProvisionCashSettlQuoteType.Offer);
        validate(ProvisionCashSettlQuoteType.ExercisingPartyPays);
    });

    test('should be immutable', () => {
        const ref = ProvisionCashSettlQuoteType;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
