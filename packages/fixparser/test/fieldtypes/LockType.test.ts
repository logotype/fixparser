import { LockType } from '../../src/fieldtypes/LockType';

describe('LockType', () => {
    test('should have the correct values', () => {
        expect(LockType.NotLocked).toBe(0);
        expect(LockType.AwayMarketNetter).toBe(1);
        expect(LockType.ThreeTickLocked).toBe(2);
        expect(LockType.LockedByMarketMaker).toBe(3);
        expect(LockType.DirectedOrderLock).toBe(4);
        expect(LockType.MultilegLock).toBe(5);
        expect(LockType.MarketOrderLock).toBe(6);
        expect(LockType.PreAssignmentLock).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            LockType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            LockType.NotLocked,
            LockType.AwayMarketNetter,
            LockType.ThreeTickLocked,
            LockType.LockedByMarketMaker,
            LockType.DirectedOrderLock,
            LockType.MultilegLock,
            LockType.MarketOrderLock,
            LockType.PreAssignmentLock,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for LockType type', () => {
        const validate = (value: LockType) => {
            expect(Object.values(LockType)).toContain(value);
        };

        validate(LockType.NotLocked);
        validate(LockType.AwayMarketNetter);
        validate(LockType.ThreeTickLocked);
        validate(LockType.LockedByMarketMaker);
        validate(LockType.DirectedOrderLock);
        validate(LockType.MultilegLock);
        validate(LockType.MarketOrderLock);
        validate(LockType.PreAssignmentLock);
    });

    test('should be immutable', () => {
        const ref = LockType;
        expect(() => {
            ref.NotLocked = 10;
        }).toThrowError();
    });
});
