import { SecurityStatus } from '../../src/fieldtypes/SecurityStatus';

describe('SecurityStatus', () => {
    test('should have the correct values', () => {
        expect(SecurityStatus.Active).toBe('1');
        expect(SecurityStatus.Inactive).toBe('2');
        expect(SecurityStatus.ActiveClosingOrdersOnly).toBe('3');
        expect(SecurityStatus.Expired).toBe('4');
        expect(SecurityStatus.Delisted).toBe('5');
        expect(SecurityStatus.KnockedOut).toBe('6');
        expect(SecurityStatus.KnockOutRevoked).toBe('7');
        expect(SecurityStatus.PendingExpiry).toBe('8');
        expect(SecurityStatus.Suspended).toBe('9');
        expect(SecurityStatus.Published).toBe('10');
        expect(SecurityStatus.PendingDeletion).toBe('11');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityStatus.Active,
            SecurityStatus.Inactive,
            SecurityStatus.ActiveClosingOrdersOnly,
            SecurityStatus.Expired,
            SecurityStatus.Delisted,
            SecurityStatus.KnockedOut,
            SecurityStatus.KnockOutRevoked,
            SecurityStatus.PendingExpiry,
            SecurityStatus.Suspended,
            SecurityStatus.Published,
            SecurityStatus.PendingDeletion,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SecurityStatus type', () => {
        const validate = (value: SecurityStatus) => {
            expect(Object.values(SecurityStatus)).toContain(value);
        };

        validate(SecurityStatus.Active);
        validate(SecurityStatus.Inactive);
        validate(SecurityStatus.ActiveClosingOrdersOnly);
        validate(SecurityStatus.Expired);
        validate(SecurityStatus.Delisted);
        validate(SecurityStatus.KnockedOut);
        validate(SecurityStatus.KnockOutRevoked);
        validate(SecurityStatus.PendingExpiry);
        validate(SecurityStatus.Suspended);
        validate(SecurityStatus.Published);
        validate(SecurityStatus.PendingDeletion);
    });

    test('should be immutable', () => {
        const ref = SecurityStatus;
        expect(() => {
            ref.Active = 10;
        }).toThrowError();
    });
});
