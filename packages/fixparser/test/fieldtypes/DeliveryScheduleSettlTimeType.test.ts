import { DeliveryScheduleSettlTimeType } from '../../src/fieldtypes/DeliveryScheduleSettlTimeType';

describe('DeliveryScheduleSettlTimeType', () => {
    test('should have the correct values', () => {
        expect(DeliveryScheduleSettlTimeType.Hour).toBe(0);
        expect(DeliveryScheduleSettlTimeType.Timestamp).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DeliveryScheduleSettlTimeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [DeliveryScheduleSettlTimeType.Hour, DeliveryScheduleSettlTimeType.Timestamp];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DeliveryScheduleSettlTimeType type', () => {
        const validate = (value: DeliveryScheduleSettlTimeType) => {
            expect(Object.values(DeliveryScheduleSettlTimeType)).toContain(value);
        };

        validate(DeliveryScheduleSettlTimeType.Hour);
        validate(DeliveryScheduleSettlTimeType.Timestamp);
    });

    test('should be immutable', () => {
        const ref = DeliveryScheduleSettlTimeType;
        expect(() => {
            ref.Hour = 10;
        }).toThrowError();
    });
});
