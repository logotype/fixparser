import { TransferStatus } from '../../src/fieldtypes/TransferStatus';

describe('TransferStatus', () => {
    test('should have the correct values', () => {
        expect(TransferStatus.Received).toBe(0);
        expect(TransferStatus.RejectedByIntermediary).toBe(1);
        expect(TransferStatus.AcceptPending).toBe(2);
        expect(TransferStatus.Accepted).toBe(3);
        expect(TransferStatus.Declined).toBe(4);
        expect(TransferStatus.Cancelled).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TransferStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TransferStatus.Received,
            TransferStatus.RejectedByIntermediary,
            TransferStatus.AcceptPending,
            TransferStatus.Accepted,
            TransferStatus.Declined,
            TransferStatus.Cancelled,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TransferStatus type', () => {
        const validate = (value: TransferStatus) => {
            expect(Object.values(TransferStatus)).toContain(value);
        };

        validate(TransferStatus.Received);
        validate(TransferStatus.RejectedByIntermediary);
        validate(TransferStatus.AcceptPending);
        validate(TransferStatus.Accepted);
        validate(TransferStatus.Declined);
        validate(TransferStatus.Cancelled);
    });

    test('should be immutable', () => {
        const ref = TransferStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
