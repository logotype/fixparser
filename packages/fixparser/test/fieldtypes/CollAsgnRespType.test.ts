import { CollAsgnRespType } from '../../src/fieldtypes/CollAsgnRespType';

describe('CollAsgnRespType', () => {
    test('should have the correct values', () => {
        expect(CollAsgnRespType.Received).toBe(0);
        expect(CollAsgnRespType.Accepted).toBe(1);
        expect(CollAsgnRespType.Declined).toBe(2);
        expect(CollAsgnRespType.Rejected).toBe(3);
        expect(CollAsgnRespType.TransactionPending).toBe(4);
        expect(CollAsgnRespType.TransactionCompletedWithWarning).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollAsgnRespType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollAsgnRespType.Received,
            CollAsgnRespType.Accepted,
            CollAsgnRespType.Declined,
            CollAsgnRespType.Rejected,
            CollAsgnRespType.TransactionPending,
            CollAsgnRespType.TransactionCompletedWithWarning,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollAsgnRespType type', () => {
        const validate = (value: CollAsgnRespType) => {
            expect(Object.values(CollAsgnRespType)).toContain(value);
        };

        validate(CollAsgnRespType.Received);
        validate(CollAsgnRespType.Accepted);
        validate(CollAsgnRespType.Declined);
        validate(CollAsgnRespType.Rejected);
        validate(CollAsgnRespType.TransactionPending);
        validate(CollAsgnRespType.TransactionCompletedWithWarning);
    });

    test('should be immutable', () => {
        const ref = CollAsgnRespType;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
