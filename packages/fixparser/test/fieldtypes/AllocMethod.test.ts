import { AllocMethod } from '../../src/fieldtypes/AllocMethod';

describe('AllocMethod', () => {
    test('should have the correct values', () => {
        expect(AllocMethod.Automatic).toBe(1);
        expect(AllocMethod.Guarantor).toBe(2);
        expect(AllocMethod.Manual).toBe(3);
        expect(AllocMethod.BrokerAssigned).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocMethod.Automatic,
            AllocMethod.Guarantor,
            AllocMethod.Manual,
            AllocMethod.BrokerAssigned,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocMethod type', () => {
        const validate = (value: AllocMethod) => {
            expect(Object.values(AllocMethod)).toContain(value);
        };

        validate(AllocMethod.Automatic);
        validate(AllocMethod.Guarantor);
        validate(AllocMethod.Manual);
        validate(AllocMethod.BrokerAssigned);
    });

    test('should be immutable', () => {
        const ref = AllocMethod;
        expect(() => {
            ref.Automatic = 10;
        }).toThrowError();
    });
});
