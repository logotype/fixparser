import { BidDescriptorType } from '../../src/fieldtypes/BidDescriptorType';

describe('BidDescriptorType', () => {
    test('should have the correct values', () => {
        expect(BidDescriptorType.Sector).toBe(1);
        expect(BidDescriptorType.Country).toBe(2);
        expect(BidDescriptorType.Index).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            BidDescriptorType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [BidDescriptorType.Sector, BidDescriptorType.Country, BidDescriptorType.Index];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for BidDescriptorType type', () => {
        const validate = (value: BidDescriptorType) => {
            expect(Object.values(BidDescriptorType)).toContain(value);
        };

        validate(BidDescriptorType.Sector);
        validate(BidDescriptorType.Country);
        validate(BidDescriptorType.Index);
    });

    test('should be immutable', () => {
        const ref = BidDescriptorType;
        expect(() => {
            ref.Sector = 10;
        }).toThrowError();
    });
});
