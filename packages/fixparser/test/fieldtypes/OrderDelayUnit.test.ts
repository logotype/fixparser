import { OrderDelayUnit } from '../../src/fieldtypes/OrderDelayUnit';

describe('OrderDelayUnit', () => {
    test('should have the correct values', () => {
        expect(OrderDelayUnit.Seconds).toBe(0);
        expect(OrderDelayUnit.TenthsOfASecond).toBe(1);
        expect(OrderDelayUnit.HundredthsOfASecond).toBe(2);
        expect(OrderDelayUnit.Milliseconds).toBe(3);
        expect(OrderDelayUnit.Microseconds).toBe(4);
        expect(OrderDelayUnit.Nanoseconds).toBe(5);
        expect(OrderDelayUnit.Minutes).toBe(10);
        expect(OrderDelayUnit.Hours).toBe(11);
        expect(OrderDelayUnit.Days).toBe(12);
        expect(OrderDelayUnit.Weeks).toBe(13);
        expect(OrderDelayUnit.Months).toBe(14);
        expect(OrderDelayUnit.Years).toBe(15);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderDelayUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderDelayUnit.Seconds,
            OrderDelayUnit.TenthsOfASecond,
            OrderDelayUnit.HundredthsOfASecond,
            OrderDelayUnit.Milliseconds,
            OrderDelayUnit.Microseconds,
            OrderDelayUnit.Nanoseconds,
            OrderDelayUnit.Minutes,
            OrderDelayUnit.Hours,
            OrderDelayUnit.Days,
            OrderDelayUnit.Weeks,
            OrderDelayUnit.Months,
            OrderDelayUnit.Years,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderDelayUnit type', () => {
        const validate = (value: OrderDelayUnit) => {
            expect(Object.values(OrderDelayUnit)).toContain(value);
        };

        validate(OrderDelayUnit.Seconds);
        validate(OrderDelayUnit.TenthsOfASecond);
        validate(OrderDelayUnit.HundredthsOfASecond);
        validate(OrderDelayUnit.Milliseconds);
        validate(OrderDelayUnit.Microseconds);
        validate(OrderDelayUnit.Nanoseconds);
        validate(OrderDelayUnit.Minutes);
        validate(OrderDelayUnit.Hours);
        validate(OrderDelayUnit.Days);
        validate(OrderDelayUnit.Weeks);
        validate(OrderDelayUnit.Months);
        validate(OrderDelayUnit.Years);
    });

    test('should be immutable', () => {
        const ref = OrderDelayUnit;
        expect(() => {
            ref.Seconds = 10;
        }).toThrowError();
    });
});
