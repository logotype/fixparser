import { PaymentStreamRealizedVarianceMethod } from '../../src/fieldtypes/PaymentStreamRealizedVarianceMethod';

describe('PaymentStreamRealizedVarianceMethod', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamRealizedVarianceMethod.Previous).toBe(0);
        expect(PaymentStreamRealizedVarianceMethod.Last).toBe(1);
        expect(PaymentStreamRealizedVarianceMethod.Both).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamRealizedVarianceMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamRealizedVarianceMethod.Previous,
            PaymentStreamRealizedVarianceMethod.Last,
            PaymentStreamRealizedVarianceMethod.Both,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamRealizedVarianceMethod type', () => {
        const validate = (value: PaymentStreamRealizedVarianceMethod) => {
            expect(Object.values(PaymentStreamRealizedVarianceMethod)).toContain(value);
        };

        validate(PaymentStreamRealizedVarianceMethod.Previous);
        validate(PaymentStreamRealizedVarianceMethod.Last);
        validate(PaymentStreamRealizedVarianceMethod.Both);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamRealizedVarianceMethod;
        expect(() => {
            ref.Previous = 10;
        }).toThrowError();
    });
});
