import { CollAsgnReason } from '../../src/fieldtypes/CollAsgnReason';

describe('CollAsgnReason', () => {
    test('should have the correct values', () => {
        expect(CollAsgnReason.Initial).toBe(0);
        expect(CollAsgnReason.Scheduled).toBe(1);
        expect(CollAsgnReason.TimeWarning).toBe(2);
        expect(CollAsgnReason.MarginDeficiency).toBe(3);
        expect(CollAsgnReason.MarginExcess).toBe(4);
        expect(CollAsgnReason.ForwardCollateralDemand).toBe(5);
        expect(CollAsgnReason.EventOfDefault).toBe(6);
        expect(CollAsgnReason.AdverseTaxEvent).toBe(7);
        expect(CollAsgnReason.TransferDeposit).toBe(8);
        expect(CollAsgnReason.TransferWithdrawal).toBe(9);
        expect(CollAsgnReason.Pledge).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollAsgnReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollAsgnReason.Initial,
            CollAsgnReason.Scheduled,
            CollAsgnReason.TimeWarning,
            CollAsgnReason.MarginDeficiency,
            CollAsgnReason.MarginExcess,
            CollAsgnReason.ForwardCollateralDemand,
            CollAsgnReason.EventOfDefault,
            CollAsgnReason.AdverseTaxEvent,
            CollAsgnReason.TransferDeposit,
            CollAsgnReason.TransferWithdrawal,
            CollAsgnReason.Pledge,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollAsgnReason type', () => {
        const validate = (value: CollAsgnReason) => {
            expect(Object.values(CollAsgnReason)).toContain(value);
        };

        validate(CollAsgnReason.Initial);
        validate(CollAsgnReason.Scheduled);
        validate(CollAsgnReason.TimeWarning);
        validate(CollAsgnReason.MarginDeficiency);
        validate(CollAsgnReason.MarginExcess);
        validate(CollAsgnReason.ForwardCollateralDemand);
        validate(CollAsgnReason.EventOfDefault);
        validate(CollAsgnReason.AdverseTaxEvent);
        validate(CollAsgnReason.TransferDeposit);
        validate(CollAsgnReason.TransferWithdrawal);
        validate(CollAsgnReason.Pledge);
    });

    test('should be immutable', () => {
        const ref = CollAsgnReason;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
