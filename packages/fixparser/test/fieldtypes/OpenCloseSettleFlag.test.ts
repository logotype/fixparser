import { OpenCloseSettleFlag } from '../../src/fieldtypes/OpenCloseSettleFlag';

describe('OpenCloseSettleFlag', () => {
    test('should have the correct values', () => {
        expect(OpenCloseSettleFlag.DailyOpen).toBe('0');
        expect(OpenCloseSettleFlag.SessionOpen).toBe('1');
        expect(OpenCloseSettleFlag.DeliverySettlementEntry).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OpenCloseSettleFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OpenCloseSettleFlag.DailyOpen,
            OpenCloseSettleFlag.SessionOpen,
            OpenCloseSettleFlag.DeliverySettlementEntry,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OpenCloseSettleFlag type', () => {
        const validate = (value: OpenCloseSettleFlag) => {
            expect(Object.values(OpenCloseSettleFlag)).toContain(value);
        };

        validate(OpenCloseSettleFlag.DailyOpen);
        validate(OpenCloseSettleFlag.SessionOpen);
        validate(OpenCloseSettleFlag.DeliverySettlementEntry);
    });

    test('should be immutable', () => {
        const ref = OpenCloseSettleFlag;
        expect(() => {
            ref.DailyOpen = 10;
        }).toThrowError();
    });
});
