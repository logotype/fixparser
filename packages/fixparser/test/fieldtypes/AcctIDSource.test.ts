import { AcctIDSource } from '../../src/fieldtypes/AcctIDSource';

describe('AcctIDSource', () => {
    test('should have the correct values', () => {
        expect(AcctIDSource.BIC).toBe(1);
        expect(AcctIDSource.SIDCode).toBe(2);
        expect(AcctIDSource.TFM).toBe(3);
        expect(AcctIDSource.OMGEO).toBe(4);
        expect(AcctIDSource.DTCCCode).toBe(5);
        expect(AcctIDSource.SPSAID).toBe(6);
        expect(AcctIDSource.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AcctIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AcctIDSource.BIC,
            AcctIDSource.SIDCode,
            AcctIDSource.TFM,
            AcctIDSource.OMGEO,
            AcctIDSource.DTCCCode,
            AcctIDSource.SPSAID,
            AcctIDSource.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AcctIDSource type', () => {
        const validate = (value: AcctIDSource) => {
            expect(Object.values(AcctIDSource)).toContain(value);
        };

        validate(AcctIDSource.BIC);
        validate(AcctIDSource.SIDCode);
        validate(AcctIDSource.TFM);
        validate(AcctIDSource.OMGEO);
        validate(AcctIDSource.DTCCCode);
        validate(AcctIDSource.SPSAID);
        validate(AcctIDSource.Other);
    });

    test('should be immutable', () => {
        const ref = AcctIDSource;
        expect(() => {
            ref.BIC = 10;
        }).toThrowError();
    });
});
