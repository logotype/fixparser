import { TargetStrategy } from '../../src/fieldtypes/TargetStrategy';

describe('TargetStrategy', () => {
    test('should have the correct values', () => {
        expect(TargetStrategy.VWAP).toBe(1);
        expect(TargetStrategy.Participate).toBe(2);
        expect(TargetStrategy.MininizeMarketImpact).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TargetStrategy.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TargetStrategy.VWAP, TargetStrategy.Participate, TargetStrategy.MininizeMarketImpact];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TargetStrategy type', () => {
        const validate = (value: TargetStrategy) => {
            expect(Object.values(TargetStrategy)).toContain(value);
        };

        validate(TargetStrategy.VWAP);
        validate(TargetStrategy.Participate);
        validate(TargetStrategy.MininizeMarketImpact);
    });

    test('should be immutable', () => {
        const ref = TargetStrategy;
        expect(() => {
            ref.VWAP = 10;
        }).toThrowError();
    });
});
