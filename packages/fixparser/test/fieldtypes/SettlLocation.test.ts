import { SettlLocation } from '../../src/fieldtypes/SettlLocation';

describe('SettlLocation', () => {
    test('should have the correct values', () => {
        expect(SettlLocation.CEDEL).toBe('CED');
        expect(SettlLocation.DepositoryTrustCompany).toBe('DTC');
        expect(SettlLocation.EuroClear).toBe('EUR');
        expect(SettlLocation.FederalBookEntry).toBe('FED');
        expect(SettlLocation.LocalMarketSettleLocation).toBe('ISO Country Code');
        expect(SettlLocation.Physical).toBe('PNY');
        expect(SettlLocation.ParticipantTrustCompany).toBe('PTC');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlLocation.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlLocation.CEDEL,
            SettlLocation.DepositoryTrustCompany,
            SettlLocation.EuroClear,
            SettlLocation.FederalBookEntry,
            SettlLocation.LocalMarketSettleLocation,
            SettlLocation.Physical,
            SettlLocation.ParticipantTrustCompany,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlLocation type', () => {
        const validate = (value: SettlLocation) => {
            expect(Object.values(SettlLocation)).toContain(value);
        };

        validate(SettlLocation.CEDEL);
        validate(SettlLocation.DepositoryTrustCompany);
        validate(SettlLocation.EuroClear);
        validate(SettlLocation.FederalBookEntry);
        validate(SettlLocation.LocalMarketSettleLocation);
        validate(SettlLocation.Physical);
        validate(SettlLocation.ParticipantTrustCompany);
    });

    test('should be immutable', () => {
        const ref = SettlLocation;
        expect(() => {
            ref.CEDEL = 10;
        }).toThrowError();
    });
});
