import { SettlCurrFxRateCalc } from '../../src/fieldtypes/SettlCurrFxRateCalc';

describe('SettlCurrFxRateCalc', () => {
    test('should have the correct values', () => {
        expect(SettlCurrFxRateCalc.Multiply).toBe('M');
        expect(SettlCurrFxRateCalc.Divide).toBe('D');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlCurrFxRateCalc.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SettlCurrFxRateCalc.Multiply, SettlCurrFxRateCalc.Divide];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SettlCurrFxRateCalc type', () => {
        const validate = (value: SettlCurrFxRateCalc) => {
            expect(Object.values(SettlCurrFxRateCalc)).toContain(value);
        };

        validate(SettlCurrFxRateCalc.Multiply);
        validate(SettlCurrFxRateCalc.Divide);
    });

    test('should be immutable', () => {
        const ref = SettlCurrFxRateCalc;
        expect(() => {
            ref.Multiply = 10;
        }).toThrowError();
    });
});
