import { AllocRequestStatus } from '../../src/fieldtypes/AllocRequestStatus';

describe('AllocRequestStatus', () => {
    test('should have the correct values', () => {
        expect(AllocRequestStatus.Accepted).toBe(0);
        expect(AllocRequestStatus.Rejected).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AllocRequestStatus.Accepted, AllocRequestStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocRequestStatus type', () => {
        const validate = (value: AllocRequestStatus) => {
            expect(Object.values(AllocRequestStatus)).toContain(value);
        };

        validate(AllocRequestStatus.Accepted);
        validate(AllocRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = AllocRequestStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
