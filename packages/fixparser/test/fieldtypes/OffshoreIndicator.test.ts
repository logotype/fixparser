import { OffshoreIndicator } from '../../src/fieldtypes/OffshoreIndicator';

describe('OffshoreIndicator', () => {
    test('should have the correct values', () => {
        expect(OffshoreIndicator.Regular).toBe(0);
        expect(OffshoreIndicator.Offshore).toBe(1);
        expect(OffshoreIndicator.Onshore).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OffshoreIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [OffshoreIndicator.Regular, OffshoreIndicator.Offshore, OffshoreIndicator.Onshore];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OffshoreIndicator type', () => {
        const validate = (value: OffshoreIndicator) => {
            expect(Object.values(OffshoreIndicator)).toContain(value);
        };

        validate(OffshoreIndicator.Regular);
        validate(OffshoreIndicator.Offshore);
        validate(OffshoreIndicator.Onshore);
    });

    test('should be immutable', () => {
        const ref = OffshoreIndicator;
        expect(() => {
            ref.Regular = 10;
        }).toThrowError();
    });
});
