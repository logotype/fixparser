import { ProvisionOptionSinglePartyBuyerSide } from '../../src/fieldtypes/ProvisionOptionSinglePartyBuyerSide';

describe('ProvisionOptionSinglePartyBuyerSide', () => {
    test('should have the correct values', () => {
        expect(ProvisionOptionSinglePartyBuyerSide.Buy).toBe(1);
        expect(ProvisionOptionSinglePartyBuyerSide.Sell).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionOptionSinglePartyBuyerSide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ProvisionOptionSinglePartyBuyerSide.Buy, ProvisionOptionSinglePartyBuyerSide.Sell];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionOptionSinglePartyBuyerSide type', () => {
        const validate = (value: ProvisionOptionSinglePartyBuyerSide) => {
            expect(Object.values(ProvisionOptionSinglePartyBuyerSide)).toContain(value);
        };

        validate(ProvisionOptionSinglePartyBuyerSide.Buy);
        validate(ProvisionOptionSinglePartyBuyerSide.Sell);
    });

    test('should be immutable', () => {
        const ref = ProvisionOptionSinglePartyBuyerSide;
        expect(() => {
            ref.Buy = 10;
        }).toThrowError();
    });
});
