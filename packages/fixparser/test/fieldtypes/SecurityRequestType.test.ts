import { SecurityRequestType } from '../../src/fieldtypes/SecurityRequestType';

describe('SecurityRequestType', () => {
    test('should have the correct values', () => {
        expect(SecurityRequestType.RequestSecurityIdentityAndSpecifications).toBe(0);
        expect(SecurityRequestType.RequestSecurityIdentityForSpecifications).toBe(1);
        expect(SecurityRequestType.RequestListSecurityTypes).toBe(2);
        expect(SecurityRequestType.RequestListSecurities).toBe(3);
        expect(SecurityRequestType.Symbol).toBe(4);
        expect(SecurityRequestType.SecurityTypeAndOrCFICode).toBe(5);
        expect(SecurityRequestType.Product).toBe(6);
        expect(SecurityRequestType.TradingSessionID).toBe(7);
        expect(SecurityRequestType.AllSecurities).toBe(8);
        expect(SecurityRequestType.MarketIDOrMarketID).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityRequestType.RequestSecurityIdentityAndSpecifications,
            SecurityRequestType.RequestSecurityIdentityForSpecifications,
            SecurityRequestType.RequestListSecurityTypes,
            SecurityRequestType.RequestListSecurities,
            SecurityRequestType.Symbol,
            SecurityRequestType.SecurityTypeAndOrCFICode,
            SecurityRequestType.Product,
            SecurityRequestType.TradingSessionID,
            SecurityRequestType.AllSecurities,
            SecurityRequestType.MarketIDOrMarketID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityRequestType type', () => {
        const validate = (value: SecurityRequestType) => {
            expect(Object.values(SecurityRequestType)).toContain(value);
        };

        validate(SecurityRequestType.RequestSecurityIdentityAndSpecifications);
        validate(SecurityRequestType.RequestSecurityIdentityForSpecifications);
        validate(SecurityRequestType.RequestListSecurityTypes);
        validate(SecurityRequestType.RequestListSecurities);
        validate(SecurityRequestType.Symbol);
        validate(SecurityRequestType.SecurityTypeAndOrCFICode);
        validate(SecurityRequestType.Product);
        validate(SecurityRequestType.TradingSessionID);
        validate(SecurityRequestType.AllSecurities);
        validate(SecurityRequestType.MarketIDOrMarketID);
    });

    test('should be immutable', () => {
        const ref = SecurityRequestType;
        expect(() => {
            ref.RequestSecurityIdentityAndSpecifications = 10;
        }).toThrowError();
    });
});
