import { NetworkStatusResponseType } from '../../src/fieldtypes/NetworkStatusResponseType';

describe('NetworkStatusResponseType', () => {
    test('should have the correct values', () => {
        expect(NetworkStatusResponseType.Full).toBe(1);
        expect(NetworkStatusResponseType.IncrementalUpdate).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NetworkStatusResponseType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NetworkStatusResponseType.Full, NetworkStatusResponseType.IncrementalUpdate];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NetworkStatusResponseType type', () => {
        const validate = (value: NetworkStatusResponseType) => {
            expect(Object.values(NetworkStatusResponseType)).toContain(value);
        };

        validate(NetworkStatusResponseType.Full);
        validate(NetworkStatusResponseType.IncrementalUpdate);
    });

    test('should be immutable', () => {
        const ref = NetworkStatusResponseType;
        expect(() => {
            ref.Full = 10;
        }).toThrowError();
    });
});
