import { RiskLimitRequestType } from '../../src/fieldtypes/RiskLimitRequestType';

describe('RiskLimitRequestType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitRequestType.Definitions).toBe(1);
        expect(RiskLimitRequestType.Utilization).toBe(2);
        expect(RiskLimitRequestType.DefinitionsAndUtilizations).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitRequestType.Definitions,
            RiskLimitRequestType.Utilization,
            RiskLimitRequestType.DefinitionsAndUtilizations,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitRequestType type', () => {
        const validate = (value: RiskLimitRequestType) => {
            expect(Object.values(RiskLimitRequestType)).toContain(value);
        };

        validate(RiskLimitRequestType.Definitions);
        validate(RiskLimitRequestType.Utilization);
        validate(RiskLimitRequestType.DefinitionsAndUtilizations);
    });

    test('should be immutable', () => {
        const ref = RiskLimitRequestType;
        expect(() => {
            ref.Definitions = 10;
        }).toThrowError();
    });
});
