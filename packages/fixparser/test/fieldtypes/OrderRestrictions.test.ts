import { OrderRestrictions } from '../../src/fieldtypes/OrderRestrictions';

describe('OrderRestrictions', () => {
    test('should have the correct values', () => {
        expect(OrderRestrictions.ProgramTrade).toBe('1');
        expect(OrderRestrictions.IndexArbitrage).toBe('2');
        expect(OrderRestrictions.NonIndexArbitrage).toBe('3');
        expect(OrderRestrictions.CompetingMarketMaker).toBe('4');
        expect(OrderRestrictions.ActingAsMarketMakerOrSpecialistInSecurity).toBe('5');
        expect(OrderRestrictions.ActingAsMarketMakerOrSpecialistInUnderlying).toBe('6');
        expect(OrderRestrictions.ForeignEntity).toBe('7');
        expect(OrderRestrictions.ExternalMarketParticipant).toBe('8');
        expect(OrderRestrictions.ExternalInterConnectedMarketLinkage).toBe('9');
        expect(OrderRestrictions.RisklessArbitrage).toBe('A');
        expect(OrderRestrictions.IssuerHolding).toBe('B');
        expect(OrderRestrictions.IssuePriceStabilization).toBe('C');
        expect(OrderRestrictions.NonAlgorithmic).toBe('D');
        expect(OrderRestrictions.Algorithmic).toBe('E');
        expect(OrderRestrictions.Cross).toBe('F');
        expect(OrderRestrictions.InsiderAccount).toBe('G');
        expect(OrderRestrictions.SignificantShareholder).toBe('H');
        expect(OrderRestrictions.NormalCourseIssuerBid).toBe('I');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderRestrictions.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderRestrictions.ProgramTrade,
            OrderRestrictions.IndexArbitrage,
            OrderRestrictions.NonIndexArbitrage,
            OrderRestrictions.CompetingMarketMaker,
            OrderRestrictions.ActingAsMarketMakerOrSpecialistInSecurity,
            OrderRestrictions.ActingAsMarketMakerOrSpecialistInUnderlying,
            OrderRestrictions.ForeignEntity,
            OrderRestrictions.ExternalMarketParticipant,
            OrderRestrictions.ExternalInterConnectedMarketLinkage,
            OrderRestrictions.RisklessArbitrage,
            OrderRestrictions.IssuerHolding,
            OrderRestrictions.IssuePriceStabilization,
            OrderRestrictions.NonAlgorithmic,
            OrderRestrictions.Algorithmic,
            OrderRestrictions.Cross,
            OrderRestrictions.InsiderAccount,
            OrderRestrictions.SignificantShareholder,
            OrderRestrictions.NormalCourseIssuerBid,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrderRestrictions type', () => {
        const validate = (value: OrderRestrictions) => {
            expect(Object.values(OrderRestrictions)).toContain(value);
        };

        validate(OrderRestrictions.ProgramTrade);
        validate(OrderRestrictions.IndexArbitrage);
        validate(OrderRestrictions.NonIndexArbitrage);
        validate(OrderRestrictions.CompetingMarketMaker);
        validate(OrderRestrictions.ActingAsMarketMakerOrSpecialistInSecurity);
        validate(OrderRestrictions.ActingAsMarketMakerOrSpecialistInUnderlying);
        validate(OrderRestrictions.ForeignEntity);
        validate(OrderRestrictions.ExternalMarketParticipant);
        validate(OrderRestrictions.ExternalInterConnectedMarketLinkage);
        validate(OrderRestrictions.RisklessArbitrage);
        validate(OrderRestrictions.IssuerHolding);
        validate(OrderRestrictions.IssuePriceStabilization);
        validate(OrderRestrictions.NonAlgorithmic);
        validate(OrderRestrictions.Algorithmic);
        validate(OrderRestrictions.Cross);
        validate(OrderRestrictions.InsiderAccount);
        validate(OrderRestrictions.SignificantShareholder);
        validate(OrderRestrictions.NormalCourseIssuerBid);
    });

    test('should be immutable', () => {
        const ref = OrderRestrictions;
        expect(() => {
            ref.ProgramTrade = 10;
        }).toThrowError();
    });
});
