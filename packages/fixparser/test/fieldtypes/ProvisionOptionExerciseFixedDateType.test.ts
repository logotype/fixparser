import { ProvisionOptionExerciseFixedDateType } from '../../src/fieldtypes/ProvisionOptionExerciseFixedDateType';

describe('ProvisionOptionExerciseFixedDateType', () => {
    test('should have the correct values', () => {
        expect(ProvisionOptionExerciseFixedDateType.Unadjusted).toBe(0);
        expect(ProvisionOptionExerciseFixedDateType.Adjusted).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionOptionExerciseFixedDateType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionOptionExerciseFixedDateType.Unadjusted,
            ProvisionOptionExerciseFixedDateType.Adjusted,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionOptionExerciseFixedDateType type', () => {
        const validate = (value: ProvisionOptionExerciseFixedDateType) => {
            expect(Object.values(ProvisionOptionExerciseFixedDateType)).toContain(value);
        };

        validate(ProvisionOptionExerciseFixedDateType.Unadjusted);
        validate(ProvisionOptionExerciseFixedDateType.Adjusted);
    });

    test('should be immutable', () => {
        const ref = ProvisionOptionExerciseFixedDateType;
        expect(() => {
            ref.Unadjusted = 10;
        }).toThrowError();
    });
});
