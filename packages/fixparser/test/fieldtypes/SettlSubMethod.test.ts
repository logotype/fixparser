import { SettlSubMethod } from '../../src/fieldtypes/SettlSubMethod';

describe('SettlSubMethod', () => {
    test('should have the correct values', () => {
        expect(SettlSubMethod.Shares).toBe(1);
        expect(SettlSubMethod.Derivatives).toBe(2);
        expect(SettlSubMethod.PaymentVsPayment).toBe(3);
        expect(SettlSubMethod.Notional).toBe(4);
        expect(SettlSubMethod.Cascade).toBe(5);
        expect(SettlSubMethod.Repurchase).toBe(6);
        expect(SettlSubMethod.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SettlSubMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SettlSubMethod.Shares,
            SettlSubMethod.Derivatives,
            SettlSubMethod.PaymentVsPayment,
            SettlSubMethod.Notional,
            SettlSubMethod.Cascade,
            SettlSubMethod.Repurchase,
            SettlSubMethod.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SettlSubMethod type', () => {
        const validate = (value: SettlSubMethod) => {
            expect(Object.values(SettlSubMethod)).toContain(value);
        };

        validate(SettlSubMethod.Shares);
        validate(SettlSubMethod.Derivatives);
        validate(SettlSubMethod.PaymentVsPayment);
        validate(SettlSubMethod.Notional);
        validate(SettlSubMethod.Cascade);
        validate(SettlSubMethod.Repurchase);
        validate(SettlSubMethod.Other);
    });

    test('should be immutable', () => {
        const ref = SettlSubMethod;
        expect(() => {
            ref.Shares = 10;
        }).toThrowError();
    });
});
