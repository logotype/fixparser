import { SingleQuoteIndicator } from '../../src/fieldtypes/SingleQuoteIndicator';

describe('SingleQuoteIndicator', () => {
    test('should have the correct values', () => {
        expect(SingleQuoteIndicator.MultipleQuotesAllowed).toBe('N');
        expect(SingleQuoteIndicator.OnlyOneQuoteAllowed).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SingleQuoteIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SingleQuoteIndicator.MultipleQuotesAllowed, SingleQuoteIndicator.OnlyOneQuoteAllowed];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SingleQuoteIndicator type', () => {
        const validate = (value: SingleQuoteIndicator) => {
            expect(Object.values(SingleQuoteIndicator)).toContain(value);
        };

        validate(SingleQuoteIndicator.MultipleQuotesAllowed);
        validate(SingleQuoteIndicator.OnlyOneQuoteAllowed);
    });

    test('should be immutable', () => {
        const ref = SingleQuoteIndicator;
        expect(() => {
            ref.MultipleQuotesAllowed = 10;
        }).toThrowError();
    });
});
