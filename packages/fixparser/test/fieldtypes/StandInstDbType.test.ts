import { StandInstDbType } from '../../src/fieldtypes/StandInstDbType';

describe('StandInstDbType', () => {
    test('should have the correct values', () => {
        expect(StandInstDbType.Other).toBe(0);
        expect(StandInstDbType.DTCSID).toBe(1);
        expect(StandInstDbType.ThomsonALERT).toBe(2);
        expect(StandInstDbType.AGlobalCustodian).toBe(3);
        expect(StandInstDbType.AccountNet).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StandInstDbType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            StandInstDbType.Other,
            StandInstDbType.DTCSID,
            StandInstDbType.ThomsonALERT,
            StandInstDbType.AGlobalCustodian,
            StandInstDbType.AccountNet,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StandInstDbType type', () => {
        const validate = (value: StandInstDbType) => {
            expect(Object.values(StandInstDbType)).toContain(value);
        };

        validate(StandInstDbType.Other);
        validate(StandInstDbType.DTCSID);
        validate(StandInstDbType.ThomsonALERT);
        validate(StandInstDbType.AGlobalCustodian);
        validate(StandInstDbType.AccountNet);
    });

    test('should be immutable', () => {
        const ref = StandInstDbType;
        expect(() => {
            ref.Other = 10;
        }).toThrowError();
    });
});
