import { OptPayoutType } from '../../src/fieldtypes/OptPayoutType';

describe('OptPayoutType', () => {
    test('should have the correct values', () => {
        expect(OptPayoutType.Vanilla).toBe(1);
        expect(OptPayoutType.Capped).toBe(2);
        expect(OptPayoutType.Binary).toBe(3);
        expect(OptPayoutType.Asian).toBe(4);
        expect(OptPayoutType.Barrier).toBe(5);
        expect(OptPayoutType.DigitalBarrier).toBe(6);
        expect(OptPayoutType.Lookback).toBe(7);
        expect(OptPayoutType.OtherPathDependent).toBe(8);
        expect(OptPayoutType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OptPayoutType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OptPayoutType.Vanilla,
            OptPayoutType.Capped,
            OptPayoutType.Binary,
            OptPayoutType.Asian,
            OptPayoutType.Barrier,
            OptPayoutType.DigitalBarrier,
            OptPayoutType.Lookback,
            OptPayoutType.OtherPathDependent,
            OptPayoutType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OptPayoutType type', () => {
        const validate = (value: OptPayoutType) => {
            expect(Object.values(OptPayoutType)).toContain(value);
        };

        validate(OptPayoutType.Vanilla);
        validate(OptPayoutType.Capped);
        validate(OptPayoutType.Binary);
        validate(OptPayoutType.Asian);
        validate(OptPayoutType.Barrier);
        validate(OptPayoutType.DigitalBarrier);
        validate(OptPayoutType.Lookback);
        validate(OptPayoutType.OtherPathDependent);
        validate(OptPayoutType.Other);
    });

    test('should be immutable', () => {
        const ref = OptPayoutType;
        expect(() => {
            ref.Vanilla = 10;
        }).toThrowError();
    });
});
