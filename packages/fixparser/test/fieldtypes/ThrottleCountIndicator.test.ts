import { ThrottleCountIndicator } from '../../src/fieldtypes/ThrottleCountIndicator';

describe('ThrottleCountIndicator', () => {
    test('should have the correct values', () => {
        expect(ThrottleCountIndicator.OutstandingRequestsUnchanged).toBe(0);
        expect(ThrottleCountIndicator.OutstandingRequestsDecreased).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ThrottleCountIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ThrottleCountIndicator.OutstandingRequestsUnchanged,
            ThrottleCountIndicator.OutstandingRequestsDecreased,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ThrottleCountIndicator type', () => {
        const validate = (value: ThrottleCountIndicator) => {
            expect(Object.values(ThrottleCountIndicator)).toContain(value);
        };

        validate(ThrottleCountIndicator.OutstandingRequestsUnchanged);
        validate(ThrottleCountIndicator.OutstandingRequestsDecreased);
    });

    test('should be immutable', () => {
        const ref = ThrottleCountIndicator;
        expect(() => {
            ref.OutstandingRequestsUnchanged = 10;
        }).toThrowError();
    });
});
