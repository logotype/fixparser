import { ReportToExch } from '../../src/fieldtypes/ReportToExch';

describe('ReportToExch', () => {
    test('should have the correct values', () => {
        expect(ReportToExch.SenderReports).toBe('N');
        expect(ReportToExch.ReceiverReports).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReportToExch.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ReportToExch.SenderReports, ReportToExch.ReceiverReports];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ReportToExch type', () => {
        const validate = (value: ReportToExch) => {
            expect(Object.values(ReportToExch)).toContain(value);
        };

        validate(ReportToExch.SenderReports);
        validate(ReportToExch.ReceiverReports);
    });

    test('should be immutable', () => {
        const ref = ReportToExch;
        expect(() => {
            ref.SenderReports = 10;
        }).toThrowError();
    });
});
