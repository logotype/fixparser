import { MarketDisruptionFallbackProvision } from '../../src/fieldtypes/MarketDisruptionFallbackProvision';

describe('MarketDisruptionFallbackProvision', () => {
    test('should have the correct values', () => {
        expect(MarketDisruptionFallbackProvision.MasterAgreement).toBe(0);
        expect(MarketDisruptionFallbackProvision.Confirmation).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketDisruptionFallbackProvision.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketDisruptionFallbackProvision.MasterAgreement,
            MarketDisruptionFallbackProvision.Confirmation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketDisruptionFallbackProvision type', () => {
        const validate = (value: MarketDisruptionFallbackProvision) => {
            expect(Object.values(MarketDisruptionFallbackProvision)).toContain(value);
        };

        validate(MarketDisruptionFallbackProvision.MasterAgreement);
        validate(MarketDisruptionFallbackProvision.Confirmation);
    });

    test('should be immutable', () => {
        const ref = MarketDisruptionFallbackProvision;
        expect(() => {
            ref.MasterAgreement = 10;
        }).toThrowError();
    });
});
