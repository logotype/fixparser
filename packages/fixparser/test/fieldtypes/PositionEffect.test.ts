import { PositionEffect } from '../../src/fieldtypes/PositionEffect';

describe('PositionEffect', () => {
    test('should have the correct values', () => {
        expect(PositionEffect.Close).toBe('C');
        expect(PositionEffect.FIFO).toBe('F');
        expect(PositionEffect.Open).toBe('O');
        expect(PositionEffect.Rolled).toBe('R');
        expect(PositionEffect.CloseButNotifyOnOpen).toBe('N');
        expect(PositionEffect.Default).toBe('D');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PositionEffect.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PositionEffect.Close,
            PositionEffect.FIFO,
            PositionEffect.Open,
            PositionEffect.Rolled,
            PositionEffect.CloseButNotifyOnOpen,
            PositionEffect.Default,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PositionEffect type', () => {
        const validate = (value: PositionEffect) => {
            expect(Object.values(PositionEffect)).toContain(value);
        };

        validate(PositionEffect.Close);
        validate(PositionEffect.FIFO);
        validate(PositionEffect.Open);
        validate(PositionEffect.Rolled);
        validate(PositionEffect.CloseButNotifyOnOpen);
        validate(PositionEffect.Default);
    });

    test('should be immutable', () => {
        const ref = PositionEffect;
        expect(() => {
            ref.Close = 10;
        }).toThrowError();
    });
});
