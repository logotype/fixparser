import { NBBOEntryType } from '../../src/fieldtypes/NBBOEntryType';

describe('NBBOEntryType', () => {
    test('should have the correct values', () => {
        expect(NBBOEntryType.Bid).toBe(0);
        expect(NBBOEntryType.Offer).toBe(1);
        expect(NBBOEntryType.MidPrice).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NBBOEntryType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NBBOEntryType.Bid, NBBOEntryType.Offer, NBBOEntryType.MidPrice];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NBBOEntryType type', () => {
        const validate = (value: NBBOEntryType) => {
            expect(Object.values(NBBOEntryType)).toContain(value);
        };

        validate(NBBOEntryType.Bid);
        validate(NBBOEntryType.Offer);
        validate(NBBOEntryType.MidPrice);
    });

    test('should be immutable', () => {
        const ref = NBBOEntryType;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
