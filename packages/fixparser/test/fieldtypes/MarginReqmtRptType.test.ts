import { MarginReqmtRptType } from '../../src/fieldtypes/MarginReqmtRptType';

describe('MarginReqmtRptType', () => {
    test('should have the correct values', () => {
        expect(MarginReqmtRptType.Summary).toBe(0);
        expect(MarginReqmtRptType.Detail).toBe(1);
        expect(MarginReqmtRptType.ExcessDeficit).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarginReqmtRptType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarginReqmtRptType.Summary,
            MarginReqmtRptType.Detail,
            MarginReqmtRptType.ExcessDeficit,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarginReqmtRptType type', () => {
        const validate = (value: MarginReqmtRptType) => {
            expect(Object.values(MarginReqmtRptType)).toContain(value);
        };

        validate(MarginReqmtRptType.Summary);
        validate(MarginReqmtRptType.Detail);
        validate(MarginReqmtRptType.ExcessDeficit);
    });

    test('should be immutable', () => {
        const ref = MarginReqmtRptType;
        expect(() => {
            ref.Summary = 10;
        }).toThrowError();
    });
});
