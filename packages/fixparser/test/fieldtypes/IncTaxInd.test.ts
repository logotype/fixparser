import { IncTaxInd } from '../../src/fieldtypes/IncTaxInd';

describe('IncTaxInd', () => {
    test('should have the correct values', () => {
        expect(IncTaxInd.Net).toBe(1);
        expect(IncTaxInd.Gross).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            IncTaxInd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [IncTaxInd.Net, IncTaxInd.Gross];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for IncTaxInd type', () => {
        const validate = (value: IncTaxInd) => {
            expect(Object.values(IncTaxInd)).toContain(value);
        };

        validate(IncTaxInd.Net);
        validate(IncTaxInd.Gross);
    });

    test('should be immutable', () => {
        const ref = IncTaxInd;
        expect(() => {
            ref.Net = 10;
        }).toThrowError();
    });
});
