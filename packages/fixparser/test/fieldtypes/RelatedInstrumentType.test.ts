import { RelatedInstrumentType } from '../../src/fieldtypes/RelatedInstrumentType';

describe('RelatedInstrumentType', () => {
    test('should have the correct values', () => {
        expect(RelatedInstrumentType.HedgesForInstrument).toBe(1);
        expect(RelatedInstrumentType.Underlier).toBe(2);
        expect(RelatedInstrumentType.EquityEquivalent).toBe(3);
        expect(RelatedInstrumentType.NearestExchangeTradedContract).toBe(4);
        expect(RelatedInstrumentType.RetailEquivalent).toBe(5);
        expect(RelatedInstrumentType.Leg).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelatedInstrumentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RelatedInstrumentType.HedgesForInstrument,
            RelatedInstrumentType.Underlier,
            RelatedInstrumentType.EquityEquivalent,
            RelatedInstrumentType.NearestExchangeTradedContract,
            RelatedInstrumentType.RetailEquivalent,
            RelatedInstrumentType.Leg,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelatedInstrumentType type', () => {
        const validate = (value: RelatedInstrumentType) => {
            expect(Object.values(RelatedInstrumentType)).toContain(value);
        };

        validate(RelatedInstrumentType.HedgesForInstrument);
        validate(RelatedInstrumentType.Underlier);
        validate(RelatedInstrumentType.EquityEquivalent);
        validate(RelatedInstrumentType.NearestExchangeTradedContract);
        validate(RelatedInstrumentType.RetailEquivalent);
        validate(RelatedInstrumentType.Leg);
    });

    test('should be immutable', () => {
        const ref = RelatedInstrumentType;
        expect(() => {
            ref.HedgesForInstrument = 10;
        }).toThrowError();
    });
});
