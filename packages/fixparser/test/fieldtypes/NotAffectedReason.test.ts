import { NotAffectedReason } from '../../src/fieldtypes/NotAffectedReason';

describe('NotAffectedReason', () => {
    test('should have the correct values', () => {
        expect(NotAffectedReason.OrderSuspended).toBe(0);
        expect(NotAffectedReason.InstrumentSuspended).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            NotAffectedReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [NotAffectedReason.OrderSuspended, NotAffectedReason.InstrumentSuspended];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for NotAffectedReason type', () => {
        const validate = (value: NotAffectedReason) => {
            expect(Object.values(NotAffectedReason)).toContain(value);
        };

        validate(NotAffectedReason.OrderSuspended);
        validate(NotAffectedReason.InstrumentSuspended);
    });

    test('should be immutable', () => {
        const ref = NotAffectedReason;
        expect(() => {
            ref.OrderSuspended = 10;
        }).toThrowError();
    });
});
