import { PegPriceType } from '../../src/fieldtypes/PegPriceType';

describe('PegPriceType', () => {
    test('should have the correct values', () => {
        expect(PegPriceType.LastPeg).toBe(1);
        expect(PegPriceType.MidPricePeg).toBe(2);
        expect(PegPriceType.OpeningPeg).toBe(3);
        expect(PegPriceType.MarketPeg).toBe(4);
        expect(PegPriceType.PrimaryPeg).toBe(5);
        expect(PegPriceType.PegToVWAP).toBe(7);
        expect(PegPriceType.TrailingStopPeg).toBe(8);
        expect(PegPriceType.PegToLimitPrice).toBe(9);
        expect(PegPriceType.ShortSaleMinPricePeg).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegPriceType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PegPriceType.LastPeg,
            PegPriceType.MidPricePeg,
            PegPriceType.OpeningPeg,
            PegPriceType.MarketPeg,
            PegPriceType.PrimaryPeg,
            PegPriceType.PegToVWAP,
            PegPriceType.TrailingStopPeg,
            PegPriceType.PegToLimitPrice,
            PegPriceType.ShortSaleMinPricePeg,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegPriceType type', () => {
        const validate = (value: PegPriceType) => {
            expect(Object.values(PegPriceType)).toContain(value);
        };

        validate(PegPriceType.LastPeg);
        validate(PegPriceType.MidPricePeg);
        validate(PegPriceType.OpeningPeg);
        validate(PegPriceType.MarketPeg);
        validate(PegPriceType.PrimaryPeg);
        validate(PegPriceType.PegToVWAP);
        validate(PegPriceType.TrailingStopPeg);
        validate(PegPriceType.PegToLimitPrice);
        validate(PegPriceType.ShortSaleMinPricePeg);
    });

    test('should be immutable', () => {
        const ref = PegPriceType;
        expect(() => {
            ref.LastPeg = 10;
        }).toThrowError();
    });
});
