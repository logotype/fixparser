import { PartyDetailRoleQualifier } from '../../src/fieldtypes/PartyDetailRoleQualifier';

describe('PartyDetailRoleQualifier', () => {
    test('should have the correct values', () => {
        expect(PartyDetailRoleQualifier.Agency).toBe(0);
        expect(PartyDetailRoleQualifier.Principal).toBe(1);
        expect(PartyDetailRoleQualifier.RisklessPrincipal).toBe(2);
        expect(PartyDetailRoleQualifier.ExchangeOrderSubmitter).toBe(30);
        expect(PartyDetailRoleQualifier.GeneralClearingMember).toBe(3);
        expect(PartyDetailRoleQualifier.IndividualClearingMember).toBe(4);
        expect(PartyDetailRoleQualifier.PreferredMarketMaker).toBe(5);
        expect(PartyDetailRoleQualifier.DirectedMarketMaker).toBe(6);
        expect(PartyDetailRoleQualifier.Bank).toBe(7);
        expect(PartyDetailRoleQualifier.Hub).toBe(8);
        expect(PartyDetailRoleQualifier.PrimaryTrdRepository).toBe(9);
        expect(PartyDetailRoleQualifier.OrigTrdRepository).toBe(10);
        expect(PartyDetailRoleQualifier.AddtnlIntlTrdRepository).toBe(11);
        expect(PartyDetailRoleQualifier.AddtnlDomesticTrdRepository).toBe(12);
        expect(PartyDetailRoleQualifier.RelatedExchange).toBe(13);
        expect(PartyDetailRoleQualifier.OptionsExchange).toBe(14);
        expect(PartyDetailRoleQualifier.SpecifiedExchange).toBe(15);
        expect(PartyDetailRoleQualifier.ConstituentExchange).toBe(16);
        expect(PartyDetailRoleQualifier.ExemptFromTradeReporting).toBe(17);
        expect(PartyDetailRoleQualifier.Current).toBe(18);
        expect(PartyDetailRoleQualifier.New).toBe(19);
        expect(PartyDetailRoleQualifier.DesignatedSponsor).toBe(20);
        expect(PartyDetailRoleQualifier.Specialist).toBe(21);
        expect(PartyDetailRoleQualifier.Algorithm).toBe(22);
        expect(PartyDetailRoleQualifier.FirmOrLegalEntity).toBe(23);
        expect(PartyDetailRoleQualifier.NaturalPerson).toBe(24);
        expect(PartyDetailRoleQualifier.RegularTrader).toBe(25);
        expect(PartyDetailRoleQualifier.HeadTrader).toBe(26);
        expect(PartyDetailRoleQualifier.Supervisor).toBe(27);
        expect(PartyDetailRoleQualifier.TriParty).toBe(28);
        expect(PartyDetailRoleQualifier.Lender).toBe(29);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyDetailRoleQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyDetailRoleQualifier.Agency,
            PartyDetailRoleQualifier.Principal,
            PartyDetailRoleQualifier.RisklessPrincipal,
            PartyDetailRoleQualifier.ExchangeOrderSubmitter,
            PartyDetailRoleQualifier.GeneralClearingMember,
            PartyDetailRoleQualifier.IndividualClearingMember,
            PartyDetailRoleQualifier.PreferredMarketMaker,
            PartyDetailRoleQualifier.DirectedMarketMaker,
            PartyDetailRoleQualifier.Bank,
            PartyDetailRoleQualifier.Hub,
            PartyDetailRoleQualifier.PrimaryTrdRepository,
            PartyDetailRoleQualifier.OrigTrdRepository,
            PartyDetailRoleQualifier.AddtnlIntlTrdRepository,
            PartyDetailRoleQualifier.AddtnlDomesticTrdRepository,
            PartyDetailRoleQualifier.RelatedExchange,
            PartyDetailRoleQualifier.OptionsExchange,
            PartyDetailRoleQualifier.SpecifiedExchange,
            PartyDetailRoleQualifier.ConstituentExchange,
            PartyDetailRoleQualifier.ExemptFromTradeReporting,
            PartyDetailRoleQualifier.Current,
            PartyDetailRoleQualifier.New,
            PartyDetailRoleQualifier.DesignatedSponsor,
            PartyDetailRoleQualifier.Specialist,
            PartyDetailRoleQualifier.Algorithm,
            PartyDetailRoleQualifier.FirmOrLegalEntity,
            PartyDetailRoleQualifier.NaturalPerson,
            PartyDetailRoleQualifier.RegularTrader,
            PartyDetailRoleQualifier.HeadTrader,
            PartyDetailRoleQualifier.Supervisor,
            PartyDetailRoleQualifier.TriParty,
            PartyDetailRoleQualifier.Lender,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyDetailRoleQualifier type', () => {
        const validate = (value: PartyDetailRoleQualifier) => {
            expect(Object.values(PartyDetailRoleQualifier)).toContain(value);
        };

        validate(PartyDetailRoleQualifier.Agency);
        validate(PartyDetailRoleQualifier.Principal);
        validate(PartyDetailRoleQualifier.RisklessPrincipal);
        validate(PartyDetailRoleQualifier.ExchangeOrderSubmitter);
        validate(PartyDetailRoleQualifier.GeneralClearingMember);
        validate(PartyDetailRoleQualifier.IndividualClearingMember);
        validate(PartyDetailRoleQualifier.PreferredMarketMaker);
        validate(PartyDetailRoleQualifier.DirectedMarketMaker);
        validate(PartyDetailRoleQualifier.Bank);
        validate(PartyDetailRoleQualifier.Hub);
        validate(PartyDetailRoleQualifier.PrimaryTrdRepository);
        validate(PartyDetailRoleQualifier.OrigTrdRepository);
        validate(PartyDetailRoleQualifier.AddtnlIntlTrdRepository);
        validate(PartyDetailRoleQualifier.AddtnlDomesticTrdRepository);
        validate(PartyDetailRoleQualifier.RelatedExchange);
        validate(PartyDetailRoleQualifier.OptionsExchange);
        validate(PartyDetailRoleQualifier.SpecifiedExchange);
        validate(PartyDetailRoleQualifier.ConstituentExchange);
        validate(PartyDetailRoleQualifier.ExemptFromTradeReporting);
        validate(PartyDetailRoleQualifier.Current);
        validate(PartyDetailRoleQualifier.New);
        validate(PartyDetailRoleQualifier.DesignatedSponsor);
        validate(PartyDetailRoleQualifier.Specialist);
        validate(PartyDetailRoleQualifier.Algorithm);
        validate(PartyDetailRoleQualifier.FirmOrLegalEntity);
        validate(PartyDetailRoleQualifier.NaturalPerson);
        validate(PartyDetailRoleQualifier.RegularTrader);
        validate(PartyDetailRoleQualifier.HeadTrader);
        validate(PartyDetailRoleQualifier.Supervisor);
        validate(PartyDetailRoleQualifier.TriParty);
        validate(PartyDetailRoleQualifier.Lender);
    });

    test('should be immutable', () => {
        const ref = PartyDetailRoleQualifier;
        expect(() => {
            ref.Agency = 10;
        }).toThrowError();
    });
});
