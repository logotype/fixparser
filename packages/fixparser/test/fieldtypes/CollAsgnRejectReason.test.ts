import { CollAsgnRejectReason } from '../../src/fieldtypes/CollAsgnRejectReason';

describe('CollAsgnRejectReason', () => {
    test('should have the correct values', () => {
        expect(CollAsgnRejectReason.UnknownDeal).toBe(0);
        expect(CollAsgnRejectReason.UnknownOrInvalidInstrument).toBe(1);
        expect(CollAsgnRejectReason.UnauthorizedTransaction).toBe(2);
        expect(CollAsgnRejectReason.InsufficientCollateral).toBe(3);
        expect(CollAsgnRejectReason.InvalidTypeOfCollateral).toBe(4);
        expect(CollAsgnRejectReason.ExcessiveSubstitution).toBe(5);
        expect(CollAsgnRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollAsgnRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollAsgnRejectReason.UnknownDeal,
            CollAsgnRejectReason.UnknownOrInvalidInstrument,
            CollAsgnRejectReason.UnauthorizedTransaction,
            CollAsgnRejectReason.InsufficientCollateral,
            CollAsgnRejectReason.InvalidTypeOfCollateral,
            CollAsgnRejectReason.ExcessiveSubstitution,
            CollAsgnRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollAsgnRejectReason type', () => {
        const validate = (value: CollAsgnRejectReason) => {
            expect(Object.values(CollAsgnRejectReason)).toContain(value);
        };

        validate(CollAsgnRejectReason.UnknownDeal);
        validate(CollAsgnRejectReason.UnknownOrInvalidInstrument);
        validate(CollAsgnRejectReason.UnauthorizedTransaction);
        validate(CollAsgnRejectReason.InsufficientCollateral);
        validate(CollAsgnRejectReason.InvalidTypeOfCollateral);
        validate(CollAsgnRejectReason.ExcessiveSubstitution);
        validate(CollAsgnRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = CollAsgnRejectReason;
        expect(() => {
            ref.UnknownDeal = 10;
        }).toThrowError();
    });
});
