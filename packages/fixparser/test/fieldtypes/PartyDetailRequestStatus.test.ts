import { PartyDetailRequestStatus } from '../../src/fieldtypes/PartyDetailRequestStatus';

describe('PartyDetailRequestStatus', () => {
    test('should have the correct values', () => {
        expect(PartyDetailRequestStatus.Accepted).toBe(0);
        expect(PartyDetailRequestStatus.AcceptedWithChanges).toBe(1);
        expect(PartyDetailRequestStatus.Rejected).toBe(2);
        expect(PartyDetailRequestStatus.AcceptancePending).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyDetailRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyDetailRequestStatus.Accepted,
            PartyDetailRequestStatus.AcceptedWithChanges,
            PartyDetailRequestStatus.Rejected,
            PartyDetailRequestStatus.AcceptancePending,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyDetailRequestStatus type', () => {
        const validate = (value: PartyDetailRequestStatus) => {
            expect(Object.values(PartyDetailRequestStatus)).toContain(value);
        };

        validate(PartyDetailRequestStatus.Accepted);
        validate(PartyDetailRequestStatus.AcceptedWithChanges);
        validate(PartyDetailRequestStatus.Rejected);
        validate(PartyDetailRequestStatus.AcceptancePending);
    });

    test('should be immutable', () => {
        const ref = PartyDetailRequestStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
