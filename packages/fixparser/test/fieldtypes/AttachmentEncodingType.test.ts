import { AttachmentEncodingType } from '../../src/fieldtypes/AttachmentEncodingType';

describe('AttachmentEncodingType', () => {
    test('should have the correct values', () => {
        expect(AttachmentEncodingType.Base64).toBe(0);
        expect(AttachmentEncodingType.RawBinary).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AttachmentEncodingType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AttachmentEncodingType.Base64, AttachmentEncodingType.RawBinary];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AttachmentEncodingType type', () => {
        const validate = (value: AttachmentEncodingType) => {
            expect(Object.values(AttachmentEncodingType)).toContain(value);
        };

        validate(AttachmentEncodingType.Base64);
        validate(AttachmentEncodingType.RawBinary);
    });

    test('should be immutable', () => {
        const ref = AttachmentEncodingType;
        expect(() => {
            ref.Base64 = 10;
        }).toThrowError();
    });
});
