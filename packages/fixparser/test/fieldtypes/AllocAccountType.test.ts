import { AllocAccountType } from '../../src/fieldtypes/AllocAccountType';

describe('AllocAccountType', () => {
    test('should have the correct values', () => {
        expect(AllocAccountType.CarriedCustomerSide).toBe(1);
        expect(AllocAccountType.CarriedNonCustomerSide).toBe(2);
        expect(AllocAccountType.HouseTrader).toBe(3);
        expect(AllocAccountType.FloorTrader).toBe(4);
        expect(AllocAccountType.CarriedNonCustomerSideCrossMargined).toBe(6);
        expect(AllocAccountType.HouseTraderCrossMargined).toBe(7);
        expect(AllocAccountType.JointBackOfficeAccount).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocAccountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocAccountType.CarriedCustomerSide,
            AllocAccountType.CarriedNonCustomerSide,
            AllocAccountType.HouseTrader,
            AllocAccountType.FloorTrader,
            AllocAccountType.CarriedNonCustomerSideCrossMargined,
            AllocAccountType.HouseTraderCrossMargined,
            AllocAccountType.JointBackOfficeAccount,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocAccountType type', () => {
        const validate = (value: AllocAccountType) => {
            expect(Object.values(AllocAccountType)).toContain(value);
        };

        validate(AllocAccountType.CarriedCustomerSide);
        validate(AllocAccountType.CarriedNonCustomerSide);
        validate(AllocAccountType.HouseTrader);
        validate(AllocAccountType.FloorTrader);
        validate(AllocAccountType.CarriedNonCustomerSideCrossMargined);
        validate(AllocAccountType.HouseTraderCrossMargined);
        validate(AllocAccountType.JointBackOfficeAccount);
    });

    test('should be immutable', () => {
        const ref = AllocAccountType;
        expect(() => {
            ref.CarriedCustomerSide = 10;
        }).toThrowError();
    });
});
