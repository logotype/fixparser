import { MDImplicitDelete } from '../../src/fieldtypes/MDImplicitDelete';

describe('MDImplicitDelete', () => {
    test('should have the correct values', () => {
        expect(MDImplicitDelete.No).toBe('N');
        expect(MDImplicitDelete.Yes).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDImplicitDelete.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDImplicitDelete.No, MDImplicitDelete.Yes];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MDImplicitDelete type', () => {
        const validate = (value: MDImplicitDelete) => {
            expect(Object.values(MDImplicitDelete)).toContain(value);
        };

        validate(MDImplicitDelete.No);
        validate(MDImplicitDelete.Yes);
    });

    test('should be immutable', () => {
        const ref = MDImplicitDelete;
        expect(() => {
            ref.No = 10;
        }).toThrowError();
    });
});
