import { DiscretionInst } from '../../src/fieldtypes/DiscretionInst';

describe('DiscretionInst', () => {
    test('should have the correct values', () => {
        expect(DiscretionInst.RelatedToDisplayedPrice).toBe('0');
        expect(DiscretionInst.RelatedToMarketPrice).toBe('1');
        expect(DiscretionInst.RelatedToPrimaryPrice).toBe('2');
        expect(DiscretionInst.RelatedToLocalPrimaryPrice).toBe('3');
        expect(DiscretionInst.RelatedToMidpointPrice).toBe('4');
        expect(DiscretionInst.RelatedToLastTradePrice).toBe('5');
        expect(DiscretionInst.RelatedToVWAP).toBe('6');
        expect(DiscretionInst.AveragePriceGuarantee).toBe('7');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DiscretionInst.RelatedToDisplayedPrice,
            DiscretionInst.RelatedToMarketPrice,
            DiscretionInst.RelatedToPrimaryPrice,
            DiscretionInst.RelatedToLocalPrimaryPrice,
            DiscretionInst.RelatedToMidpointPrice,
            DiscretionInst.RelatedToLastTradePrice,
            DiscretionInst.RelatedToVWAP,
            DiscretionInst.AveragePriceGuarantee,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DiscretionInst type', () => {
        const validate = (value: DiscretionInst) => {
            expect(Object.values(DiscretionInst)).toContain(value);
        };

        validate(DiscretionInst.RelatedToDisplayedPrice);
        validate(DiscretionInst.RelatedToMarketPrice);
        validate(DiscretionInst.RelatedToPrimaryPrice);
        validate(DiscretionInst.RelatedToLocalPrimaryPrice);
        validate(DiscretionInst.RelatedToMidpointPrice);
        validate(DiscretionInst.RelatedToLastTradePrice);
        validate(DiscretionInst.RelatedToVWAP);
        validate(DiscretionInst.AveragePriceGuarantee);
    });

    test('should be immutable', () => {
        const ref = DiscretionInst;
        expect(() => {
            ref.RelatedToDisplayedPrice = 10;
        }).toThrowError();
    });
});
