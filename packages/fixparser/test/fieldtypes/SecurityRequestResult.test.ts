import { SecurityRequestResult } from '../../src/fieldtypes/SecurityRequestResult';

describe('SecurityRequestResult', () => {
    test('should have the correct values', () => {
        expect(SecurityRequestResult.ValidRequest).toBe(0);
        expect(SecurityRequestResult.InvalidOrUnsupportedRequest).toBe(1);
        expect(SecurityRequestResult.NoInstrumentsFound).toBe(2);
        expect(SecurityRequestResult.NotAuthorizedToRetrieveInstrumentData).toBe(3);
        expect(SecurityRequestResult.InstrumentDataTemporarilyUnavailable).toBe(4);
        expect(SecurityRequestResult.RequestForInstrumentDataNotSupported).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityRequestResult.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityRequestResult.ValidRequest,
            SecurityRequestResult.InvalidOrUnsupportedRequest,
            SecurityRequestResult.NoInstrumentsFound,
            SecurityRequestResult.NotAuthorizedToRetrieveInstrumentData,
            SecurityRequestResult.InstrumentDataTemporarilyUnavailable,
            SecurityRequestResult.RequestForInstrumentDataNotSupported,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityRequestResult type', () => {
        const validate = (value: SecurityRequestResult) => {
            expect(Object.values(SecurityRequestResult)).toContain(value);
        };

        validate(SecurityRequestResult.ValidRequest);
        validate(SecurityRequestResult.InvalidOrUnsupportedRequest);
        validate(SecurityRequestResult.NoInstrumentsFound);
        validate(SecurityRequestResult.NotAuthorizedToRetrieveInstrumentData);
        validate(SecurityRequestResult.InstrumentDataTemporarilyUnavailable);
        validate(SecurityRequestResult.RequestForInstrumentDataNotSupported);
    });

    test('should be immutable', () => {
        const ref = SecurityRequestResult;
        expect(() => {
            ref.ValidRequest = 10;
        }).toThrowError();
    });
});
