import { AlgoCertificateRequestStatus } from '../../src/fieldtypes/AlgoCertificateRequestStatus';

describe('AlgoCertificateRequestStatus', () => {
    test('should have the correct values', () => {
        expect(AlgoCertificateRequestStatus.Received).toBe(0);
        expect(AlgoCertificateRequestStatus.Accepted).toBe(1);
        expect(AlgoCertificateRequestStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AlgoCertificateRequestStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AlgoCertificateRequestStatus.Received,
            AlgoCertificateRequestStatus.Accepted,
            AlgoCertificateRequestStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AlgoCertificateRequestStatus type', () => {
        const validate = (value: AlgoCertificateRequestStatus) => {
            expect(Object.values(AlgoCertificateRequestStatus)).toContain(value);
        };

        validate(AlgoCertificateRequestStatus.Received);
        validate(AlgoCertificateRequestStatus.Accepted);
        validate(AlgoCertificateRequestStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = AlgoCertificateRequestStatus;
        expect(() => {
            ref.Received = 10;
        }).toThrowError();
    });
});
