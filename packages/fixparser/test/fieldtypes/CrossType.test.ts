import { CrossType } from '../../src/fieldtypes/CrossType';

describe('CrossType', () => {
    test('should have the correct values', () => {
        expect(CrossType.CrossAON).toBe(1);
        expect(CrossType.CrossIOC).toBe(2);
        expect(CrossType.CrossOneSide).toBe(3);
        expect(CrossType.CrossSamePrice).toBe(4);
        expect(CrossType.BasisCross).toBe(5);
        expect(CrossType.ContingentCross).toBe(6);
        expect(CrossType.VWAPCross).toBe(7);
        expect(CrossType.STSCross).toBe(8);
        expect(CrossType.CustomerToCustomer).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CrossType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CrossType.CrossAON,
            CrossType.CrossIOC,
            CrossType.CrossOneSide,
            CrossType.CrossSamePrice,
            CrossType.BasisCross,
            CrossType.ContingentCross,
            CrossType.VWAPCross,
            CrossType.STSCross,
            CrossType.CustomerToCustomer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CrossType type', () => {
        const validate = (value: CrossType) => {
            expect(Object.values(CrossType)).toContain(value);
        };

        validate(CrossType.CrossAON);
        validate(CrossType.CrossIOC);
        validate(CrossType.CrossOneSide);
        validate(CrossType.CrossSamePrice);
        validate(CrossType.BasisCross);
        validate(CrossType.ContingentCross);
        validate(CrossType.VWAPCross);
        validate(CrossType.STSCross);
        validate(CrossType.CustomerToCustomer);
    });

    test('should be immutable', () => {
        const ref = CrossType;
        expect(() => {
            ref.CrossAON = 10;
        }).toThrowError();
    });
});
