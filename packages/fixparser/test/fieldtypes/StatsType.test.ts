import { StatsType } from '../../src/fieldtypes/StatsType';

describe('StatsType', () => {
    test('should have the correct values', () => {
        expect(StatsType.ExchangeLast).toBe(1);
        expect(StatsType.High).toBe(2);
        expect(StatsType.AveragePrice).toBe(3);
        expect(StatsType.Turnover).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StatsType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StatsType.ExchangeLast, StatsType.High, StatsType.AveragePrice, StatsType.Turnover];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StatsType type', () => {
        const validate = (value: StatsType) => {
            expect(Object.values(StatsType)).toContain(value);
        };

        validate(StatsType.ExchangeLast);
        validate(StatsType.High);
        validate(StatsType.AveragePrice);
        validate(StatsType.Turnover);
    });

    test('should be immutable', () => {
        const ref = StatsType;
        expect(() => {
            ref.ExchangeLast = 10;
        }).toThrowError();
    });
});
