import { TradSesMethod } from '../../src/fieldtypes/TradSesMethod';

describe('TradSesMethod', () => {
    test('should have the correct values', () => {
        expect(TradSesMethod.Electronic).toBe(1);
        expect(TradSesMethod.OpenOutcry).toBe(2);
        expect(TradSesMethod.TwoParty).toBe(3);
        expect(TradSesMethod.Voice).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradSesMethod.Electronic,
            TradSesMethod.OpenOutcry,
            TradSesMethod.TwoParty,
            TradSesMethod.Voice,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesMethod type', () => {
        const validate = (value: TradSesMethod) => {
            expect(Object.values(TradSesMethod)).toContain(value);
        };

        validate(TradSesMethod.Electronic);
        validate(TradSesMethod.OpenOutcry);
        validate(TradSesMethod.TwoParty);
        validate(TradSesMethod.Voice);
    });

    test('should be immutable', () => {
        const ref = TradSesMethod;
        expect(() => {
            ref.Electronic = 10;
        }).toThrowError();
    });
});
