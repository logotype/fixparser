import { MDBookType } from '../../src/fieldtypes/MDBookType';

describe('MDBookType', () => {
    test('should have the correct values', () => {
        expect(MDBookType.TopOfBook).toBe(1);
        expect(MDBookType.PriceDepth).toBe(2);
        expect(MDBookType.OrderDepth).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDBookType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDBookType.TopOfBook, MDBookType.PriceDepth, MDBookType.OrderDepth];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDBookType type', () => {
        const validate = (value: MDBookType) => {
            expect(Object.values(MDBookType)).toContain(value);
        };

        validate(MDBookType.TopOfBook);
        validate(MDBookType.PriceDepth);
        validate(MDBookType.OrderDepth);
    });

    test('should be immutable', () => {
        const ref = MDBookType;
        expect(() => {
            ref.TopOfBook = 10;
        }).toThrowError();
    });
});
