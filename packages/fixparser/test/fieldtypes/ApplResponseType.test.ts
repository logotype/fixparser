import { ApplResponseType } from '../../src/fieldtypes/ApplResponseType';

describe('ApplResponseType', () => {
    test('should have the correct values', () => {
        expect(ApplResponseType.RequestSuccessfullyProcessed).toBe(0);
        expect(ApplResponseType.ApplicationDoesNotExist).toBe(1);
        expect(ApplResponseType.MessagesNotAvailable).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplResponseType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplResponseType.RequestSuccessfullyProcessed,
            ApplResponseType.ApplicationDoesNotExist,
            ApplResponseType.MessagesNotAvailable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplResponseType type', () => {
        const validate = (value: ApplResponseType) => {
            expect(Object.values(ApplResponseType)).toContain(value);
        };

        validate(ApplResponseType.RequestSuccessfullyProcessed);
        validate(ApplResponseType.ApplicationDoesNotExist);
        validate(ApplResponseType.MessagesNotAvailable);
    });

    test('should be immutable', () => {
        const ref = ApplResponseType;
        expect(() => {
            ref.RequestSuccessfullyProcessed = 10;
        }).toThrowError();
    });
});
