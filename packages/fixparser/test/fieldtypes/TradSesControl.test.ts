import { TradSesControl } from '../../src/fieldtypes/TradSesControl';

describe('TradSesControl', () => {
    test('should have the correct values', () => {
        expect(TradSesControl.Automatic).toBe(0);
        expect(TradSesControl.Manual).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradSesControl.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TradSesControl.Automatic, TradSesControl.Manual];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradSesControl type', () => {
        const validate = (value: TradSesControl) => {
            expect(Object.values(TradSesControl)).toContain(value);
        };

        validate(TradSesControl.Automatic);
        validate(TradSesControl.Manual);
    });

    test('should be immutable', () => {
        const ref = TradSesControl;
        expect(() => {
            ref.Automatic = 10;
        }).toThrowError();
    });
});
