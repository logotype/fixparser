import { StreamCommodityNearbySettlDayUnit } from '../../src/fieldtypes/StreamCommodityNearbySettlDayUnit';

describe('StreamCommodityNearbySettlDayUnit', () => {
    test('should have the correct values', () => {
        expect(StreamCommodityNearbySettlDayUnit.Week).toBe('Wk');
        expect(StreamCommodityNearbySettlDayUnit.Month).toBe('Mo');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamCommodityNearbySettlDayUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StreamCommodityNearbySettlDayUnit.Week, StreamCommodityNearbySettlDayUnit.Month];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for StreamCommodityNearbySettlDayUnit type', () => {
        const validate = (value: StreamCommodityNearbySettlDayUnit) => {
            expect(Object.values(StreamCommodityNearbySettlDayUnit)).toContain(value);
        };

        validate(StreamCommodityNearbySettlDayUnit.Week);
        validate(StreamCommodityNearbySettlDayUnit.Month);
    });

    test('should be immutable', () => {
        const ref = StreamCommodityNearbySettlDayUnit;
        expect(() => {
            ref.Week = 10;
        }).toThrowError();
    });
});
