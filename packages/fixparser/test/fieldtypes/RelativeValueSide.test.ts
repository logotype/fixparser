import { RelativeValueSide } from '../../src/fieldtypes/RelativeValueSide';

describe('RelativeValueSide', () => {
    test('should have the correct values', () => {
        expect(RelativeValueSide.Bid).toBe(1);
        expect(RelativeValueSide.Mid).toBe(2);
        expect(RelativeValueSide.Offer).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelativeValueSide.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [RelativeValueSide.Bid, RelativeValueSide.Mid, RelativeValueSide.Offer];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelativeValueSide type', () => {
        const validate = (value: RelativeValueSide) => {
            expect(Object.values(RelativeValueSide)).toContain(value);
        };

        validate(RelativeValueSide.Bid);
        validate(RelativeValueSide.Mid);
        validate(RelativeValueSide.Offer);
    });

    test('should be immutable', () => {
        const ref = RelativeValueSide;
        expect(() => {
            ref.Bid = 10;
        }).toThrowError();
    });
});
