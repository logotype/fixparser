import { DayBookingInst } from '../../src/fieldtypes/DayBookingInst';

describe('DayBookingInst', () => {
    test('should have the correct values', () => {
        expect(DayBookingInst.Auto).toBe('0');
        expect(DayBookingInst.SpeakWithOrderInitiatorBeforeBooking).toBe('1');
        expect(DayBookingInst.Accumulate).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DayBookingInst.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DayBookingInst.Auto,
            DayBookingInst.SpeakWithOrderInitiatorBeforeBooking,
            DayBookingInst.Accumulate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for DayBookingInst type', () => {
        const validate = (value: DayBookingInst) => {
            expect(Object.values(DayBookingInst)).toContain(value);
        };

        validate(DayBookingInst.Auto);
        validate(DayBookingInst.SpeakWithOrderInitiatorBeforeBooking);
        validate(DayBookingInst.Accumulate);
    });

    test('should be immutable', () => {
        const ref = DayBookingInst;
        expect(() => {
            ref.Auto = 10;
        }).toThrowError();
    });
});
