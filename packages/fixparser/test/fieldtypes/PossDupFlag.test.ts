import { PossDupFlag } from '../../src/fieldtypes/PossDupFlag';

describe('PossDupFlag', () => {
    test('should have the correct values', () => {
        expect(PossDupFlag.OriginalTransmission).toBe('N');
        expect(PossDupFlag.PossibleDuplicate).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PossDupFlag.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PossDupFlag.OriginalTransmission, PossDupFlag.PossibleDuplicate];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PossDupFlag type', () => {
        const validate = (value: PossDupFlag) => {
            expect(Object.values(PossDupFlag)).toContain(value);
        };

        validate(PossDupFlag.OriginalTransmission);
        validate(PossDupFlag.PossibleDuplicate);
    });

    test('should be immutable', () => {
        const ref = PossDupFlag;
        expect(() => {
            ref.OriginalTransmission = 10;
        }).toThrowError();
    });
});
