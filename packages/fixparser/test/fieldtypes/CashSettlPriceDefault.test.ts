import { CashSettlPriceDefault } from '../../src/fieldtypes/CashSettlPriceDefault';

describe('CashSettlPriceDefault', () => {
    test('should have the correct values', () => {
        expect(CashSettlPriceDefault.Close).toBe(0);
        expect(CashSettlPriceDefault.Hedge).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CashSettlPriceDefault.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CashSettlPriceDefault.Close, CashSettlPriceDefault.Hedge];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CashSettlPriceDefault type', () => {
        const validate = (value: CashSettlPriceDefault) => {
            expect(Object.values(CashSettlPriceDefault)).toContain(value);
        };

        validate(CashSettlPriceDefault.Close);
        validate(CashSettlPriceDefault.Hedge);
    });

    test('should be immutable', () => {
        const ref = CashSettlPriceDefault;
        expect(() => {
            ref.Close = 10;
        }).toThrowError();
    });
});
