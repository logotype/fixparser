import { MiscFeeQualifier } from '../../src/fieldtypes/MiscFeeQualifier';

describe('MiscFeeQualifier', () => {
    test('should have the correct values', () => {
        expect(MiscFeeQualifier.Contributes).toBe(0);
        expect(MiscFeeQualifier.DoesNotContribute).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MiscFeeQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MiscFeeQualifier.Contributes, MiscFeeQualifier.DoesNotContribute];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MiscFeeQualifier type', () => {
        const validate = (value: MiscFeeQualifier) => {
            expect(Object.values(MiscFeeQualifier)).toContain(value);
        };

        validate(MiscFeeQualifier.Contributes);
        validate(MiscFeeQualifier.DoesNotContribute);
    });

    test('should be immutable', () => {
        const ref = MiscFeeQualifier;
        expect(() => {
            ref.Contributes = 10;
        }).toThrowError();
    });
});
