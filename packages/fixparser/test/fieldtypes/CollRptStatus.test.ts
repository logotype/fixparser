import { CollRptStatus } from '../../src/fieldtypes/CollRptStatus';

describe('CollRptStatus', () => {
    test('should have the correct values', () => {
        expect(CollRptStatus.Accepted).toBe(0);
        expect(CollRptStatus.Received).toBe(1);
        expect(CollRptStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollRptStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CollRptStatus.Accepted, CollRptStatus.Received, CollRptStatus.Rejected];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollRptStatus type', () => {
        const validate = (value: CollRptStatus) => {
            expect(Object.values(CollRptStatus)).toContain(value);
        };

        validate(CollRptStatus.Accepted);
        validate(CollRptStatus.Received);
        validate(CollRptStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = CollRptStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
