import { SecurityTradingEvent } from '../../src/fieldtypes/SecurityTradingEvent';

describe('SecurityTradingEvent', () => {
    test('should have the correct values', () => {
        expect(SecurityTradingEvent.OrderImbalance).toBe(1);
        expect(SecurityTradingEvent.TradingResumes).toBe(2);
        expect(SecurityTradingEvent.PriceVolatilityInterruption).toBe(3);
        expect(SecurityTradingEvent.ChangeOfTradingSession).toBe(4);
        expect(SecurityTradingEvent.ChangeOfTradingSubsession).toBe(5);
        expect(SecurityTradingEvent.ChangeOfSecurityTradingStatus).toBe(6);
        expect(SecurityTradingEvent.ChangeOfBookType).toBe(7);
        expect(SecurityTradingEvent.ChangeOfMarketDepth).toBe(8);
        expect(SecurityTradingEvent.CorporateAction).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityTradingEvent.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityTradingEvent.OrderImbalance,
            SecurityTradingEvent.TradingResumes,
            SecurityTradingEvent.PriceVolatilityInterruption,
            SecurityTradingEvent.ChangeOfTradingSession,
            SecurityTradingEvent.ChangeOfTradingSubsession,
            SecurityTradingEvent.ChangeOfSecurityTradingStatus,
            SecurityTradingEvent.ChangeOfBookType,
            SecurityTradingEvent.ChangeOfMarketDepth,
            SecurityTradingEvent.CorporateAction,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityTradingEvent type', () => {
        const validate = (value: SecurityTradingEvent) => {
            expect(Object.values(SecurityTradingEvent)).toContain(value);
        };

        validate(SecurityTradingEvent.OrderImbalance);
        validate(SecurityTradingEvent.TradingResumes);
        validate(SecurityTradingEvent.PriceVolatilityInterruption);
        validate(SecurityTradingEvent.ChangeOfTradingSession);
        validate(SecurityTradingEvent.ChangeOfTradingSubsession);
        validate(SecurityTradingEvent.ChangeOfSecurityTradingStatus);
        validate(SecurityTradingEvent.ChangeOfBookType);
        validate(SecurityTradingEvent.ChangeOfMarketDepth);
        validate(SecurityTradingEvent.CorporateAction);
    });

    test('should be immutable', () => {
        const ref = SecurityTradingEvent;
        expect(() => {
            ref.OrderImbalance = 10;
        }).toThrowError();
    });
});
