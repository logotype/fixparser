import { RegulatoryTradeIDType } from '../../src/fieldtypes/RegulatoryTradeIDType';

describe('RegulatoryTradeIDType', () => {
    test('should have the correct values', () => {
        expect(RegulatoryTradeIDType.Current).toBe(0);
        expect(RegulatoryTradeIDType.Previous).toBe(1);
        expect(RegulatoryTradeIDType.Block).toBe(2);
        expect(RegulatoryTradeIDType.Related).toBe(3);
        expect(RegulatoryTradeIDType.ClearedBlockTrade).toBe(4);
        expect(RegulatoryTradeIDType.TradingVenueTransactionIdentifier).toBe(5);
        expect(RegulatoryTradeIDType.ReportTrackingNumber).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegulatoryTradeIDType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegulatoryTradeIDType.Current,
            RegulatoryTradeIDType.Previous,
            RegulatoryTradeIDType.Block,
            RegulatoryTradeIDType.Related,
            RegulatoryTradeIDType.ClearedBlockTrade,
            RegulatoryTradeIDType.TradingVenueTransactionIdentifier,
            RegulatoryTradeIDType.ReportTrackingNumber,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegulatoryTradeIDType type', () => {
        const validate = (value: RegulatoryTradeIDType) => {
            expect(Object.values(RegulatoryTradeIDType)).toContain(value);
        };

        validate(RegulatoryTradeIDType.Current);
        validate(RegulatoryTradeIDType.Previous);
        validate(RegulatoryTradeIDType.Block);
        validate(RegulatoryTradeIDType.Related);
        validate(RegulatoryTradeIDType.ClearedBlockTrade);
        validate(RegulatoryTradeIDType.TradingVenueTransactionIdentifier);
        validate(RegulatoryTradeIDType.ReportTrackingNumber);
    });

    test('should be immutable', () => {
        const ref = RegulatoryTradeIDType;
        expect(() => {
            ref.Current = 10;
        }).toThrowError();
    });
});
