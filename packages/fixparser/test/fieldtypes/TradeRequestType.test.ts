import { TradeRequestType } from '../../src/fieldtypes/TradeRequestType';

describe('TradeRequestType', () => {
    test('should have the correct values', () => {
        expect(TradeRequestType.AllTrades).toBe(0);
        expect(TradeRequestType.MatchedTradesMatchingCriteria).toBe(1);
        expect(TradeRequestType.UnmatchedTradesThatMatchCriteria).toBe(2);
        expect(TradeRequestType.UnreportedTradesThatMatchCriteria).toBe(3);
        expect(TradeRequestType.AdvisoriesThatMatchCriteria).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradeRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradeRequestType.AllTrades,
            TradeRequestType.MatchedTradesMatchingCriteria,
            TradeRequestType.UnmatchedTradesThatMatchCriteria,
            TradeRequestType.UnreportedTradesThatMatchCriteria,
            TradeRequestType.AdvisoriesThatMatchCriteria,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradeRequestType type', () => {
        const validate = (value: TradeRequestType) => {
            expect(Object.values(TradeRequestType)).toContain(value);
        };

        validate(TradeRequestType.AllTrades);
        validate(TradeRequestType.MatchedTradesMatchingCriteria);
        validate(TradeRequestType.UnmatchedTradesThatMatchCriteria);
        validate(TradeRequestType.UnreportedTradesThatMatchCriteria);
        validate(TradeRequestType.AdvisoriesThatMatchCriteria);
    });

    test('should be immutable', () => {
        const ref = TradeRequestType;
        expect(() => {
            ref.AllTrades = 10;
        }).toThrowError();
    });
});
