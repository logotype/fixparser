import { EncryptMethod } from '../../src/fieldtypes/EncryptMethod';

describe('EncryptMethod', () => {
    test('should have the correct values', () => {
        expect(EncryptMethod.None).toBe(0);
        expect(EncryptMethod.PKCS).toBe(1);
        expect(EncryptMethod.DES).toBe(2);
        expect(EncryptMethod.PKCSDES).toBe(3);
        expect(EncryptMethod.PGPDES).toBe(4);
        expect(EncryptMethod.PGPDESMD5).toBe(5);
        expect(EncryptMethod.PEM).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            EncryptMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            EncryptMethod.None,
            EncryptMethod.PKCS,
            EncryptMethod.DES,
            EncryptMethod.PKCSDES,
            EncryptMethod.PGPDES,
            EncryptMethod.PGPDESMD5,
            EncryptMethod.PEM,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for EncryptMethod type', () => {
        const validate = (value: EncryptMethod) => {
            expect(Object.values(EncryptMethod)).toContain(value);
        };

        validate(EncryptMethod.None);
        validate(EncryptMethod.PKCS);
        validate(EncryptMethod.DES);
        validate(EncryptMethod.PKCSDES);
        validate(EncryptMethod.PGPDES);
        validate(EncryptMethod.PGPDESMD5);
        validate(EncryptMethod.PEM);
    });

    test('should be immutable', () => {
        const ref = EncryptMethod;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
