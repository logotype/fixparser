import { OrderOwnershipIndicator } from '../../src/fieldtypes/OrderOwnershipIndicator';

describe('OrderOwnershipIndicator', () => {
    test('should have the correct values', () => {
        expect(OrderOwnershipIndicator.NoChange).toBe(0);
        expect(OrderOwnershipIndicator.ExecutingPartyChange).toBe(1);
        expect(OrderOwnershipIndicator.EnteringPartyChange).toBe(2);
        expect(OrderOwnershipIndicator.SpecifiedPartyChange).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderOwnershipIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderOwnershipIndicator.NoChange,
            OrderOwnershipIndicator.ExecutingPartyChange,
            OrderOwnershipIndicator.EnteringPartyChange,
            OrderOwnershipIndicator.SpecifiedPartyChange,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderOwnershipIndicator type', () => {
        const validate = (value: OrderOwnershipIndicator) => {
            expect(Object.values(OrderOwnershipIndicator)).toContain(value);
        };

        validate(OrderOwnershipIndicator.NoChange);
        validate(OrderOwnershipIndicator.ExecutingPartyChange);
        validate(OrderOwnershipIndicator.EnteringPartyChange);
        validate(OrderOwnershipIndicator.SpecifiedPartyChange);
    });

    test('should be immutable', () => {
        const ref = OrderOwnershipIndicator;
        expect(() => {
            ref.NoChange = 10;
        }).toThrowError();
    });
});
