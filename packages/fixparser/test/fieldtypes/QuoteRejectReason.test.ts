import { QuoteRejectReason } from '../../src/fieldtypes/QuoteRejectReason';

describe('QuoteRejectReason', () => {
    test('should have the correct values', () => {
        expect(QuoteRejectReason.UnknownSymbol).toBe(1);
        expect(QuoteRejectReason.Exchange).toBe(2);
        expect(QuoteRejectReason.QuoteRequestExceedsLimit).toBe(3);
        expect(QuoteRejectReason.TooLateToEnter).toBe(4);
        expect(QuoteRejectReason.UnknownQuote).toBe(5);
        expect(QuoteRejectReason.DuplicateQuote).toBe(6);
        expect(QuoteRejectReason.InvalidBid).toBe(7);
        expect(QuoteRejectReason.InvalidPrice).toBe(8);
        expect(QuoteRejectReason.NotAuthorizedToQuoteSecurity).toBe(9);
        expect(QuoteRejectReason.PriceExceedsCurrentPriceBand).toBe(10);
        expect(QuoteRejectReason.QuoteLocked).toBe(11);
        expect(QuoteRejectReason.InvalidOrUnknownSecurityIssuer).toBe(12);
        expect(QuoteRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity).toBe(13);
        expect(QuoteRejectReason.NotionalValueExceedsThreshold).toBe(14);
        expect(QuoteRejectReason.PriceExceedsCurrentPriceBandDepr).toBe(15);
        expect(QuoteRejectReason.ReferencePriceNotAvailable).toBe(16);
        expect(QuoteRejectReason.InsufficientCreditLimit).toBe(17);
        expect(QuoteRejectReason.ExceededClipSizeLimit).toBe(18);
        expect(QuoteRejectReason.ExceededMaxNotionalOrderAmt).toBe(19);
        expect(QuoteRejectReason.ExceededDV01PV01Limit).toBe(20);
        expect(QuoteRejectReason.ExceededCS01Limit).toBe(21);
        expect(QuoteRejectReason.UnavailablePriceLiquidity).toBe(22);
        expect(QuoteRejectReason.InvalidMissingEntitlements).toBe(23);
        expect(QuoteRejectReason.UnknownAccounts).toBe(24);
        expect(QuoteRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteRejectReason.UnknownSymbol,
            QuoteRejectReason.Exchange,
            QuoteRejectReason.QuoteRequestExceedsLimit,
            QuoteRejectReason.TooLateToEnter,
            QuoteRejectReason.UnknownQuote,
            QuoteRejectReason.DuplicateQuote,
            QuoteRejectReason.InvalidBid,
            QuoteRejectReason.InvalidPrice,
            QuoteRejectReason.NotAuthorizedToQuoteSecurity,
            QuoteRejectReason.PriceExceedsCurrentPriceBand,
            QuoteRejectReason.QuoteLocked,
            QuoteRejectReason.InvalidOrUnknownSecurityIssuer,
            QuoteRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity,
            QuoteRejectReason.NotionalValueExceedsThreshold,
            QuoteRejectReason.PriceExceedsCurrentPriceBandDepr,
            QuoteRejectReason.ReferencePriceNotAvailable,
            QuoteRejectReason.InsufficientCreditLimit,
            QuoteRejectReason.ExceededClipSizeLimit,
            QuoteRejectReason.ExceededMaxNotionalOrderAmt,
            QuoteRejectReason.ExceededDV01PV01Limit,
            QuoteRejectReason.ExceededCS01Limit,
            QuoteRejectReason.UnavailablePriceLiquidity,
            QuoteRejectReason.InvalidMissingEntitlements,
            QuoteRejectReason.UnknownAccounts,
            QuoteRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteRejectReason type', () => {
        const validate = (value: QuoteRejectReason) => {
            expect(Object.values(QuoteRejectReason)).toContain(value);
        };

        validate(QuoteRejectReason.UnknownSymbol);
        validate(QuoteRejectReason.Exchange);
        validate(QuoteRejectReason.QuoteRequestExceedsLimit);
        validate(QuoteRejectReason.TooLateToEnter);
        validate(QuoteRejectReason.UnknownQuote);
        validate(QuoteRejectReason.DuplicateQuote);
        validate(QuoteRejectReason.InvalidBid);
        validate(QuoteRejectReason.InvalidPrice);
        validate(QuoteRejectReason.NotAuthorizedToQuoteSecurity);
        validate(QuoteRejectReason.PriceExceedsCurrentPriceBand);
        validate(QuoteRejectReason.QuoteLocked);
        validate(QuoteRejectReason.InvalidOrUnknownSecurityIssuer);
        validate(QuoteRejectReason.InvalidOrUnknownIssuerOfUnderlyingSecurity);
        validate(QuoteRejectReason.NotionalValueExceedsThreshold);
        validate(QuoteRejectReason.PriceExceedsCurrentPriceBandDepr);
        validate(QuoteRejectReason.ReferencePriceNotAvailable);
        validate(QuoteRejectReason.InsufficientCreditLimit);
        validate(QuoteRejectReason.ExceededClipSizeLimit);
        validate(QuoteRejectReason.ExceededMaxNotionalOrderAmt);
        validate(QuoteRejectReason.ExceededDV01PV01Limit);
        validate(QuoteRejectReason.ExceededCS01Limit);
        validate(QuoteRejectReason.UnavailablePriceLiquidity);
        validate(QuoteRejectReason.InvalidMissingEntitlements);
        validate(QuoteRejectReason.UnknownAccounts);
        validate(QuoteRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = QuoteRejectReason;
        expect(() => {
            ref.UnknownSymbol = 10;
        }).toThrowError();
    });
});
