import { ExecTransType } from '../../src/fieldtypes/ExecTransType';

describe('ExecTransType', () => {
    test('should have the correct values', () => {
        expect(ExecTransType.New).toBe('0');
        expect(ExecTransType.Cancel).toBe('1');
        expect(ExecTransType.Correct).toBe('2');
        expect(ExecTransType.Status).toBe('3');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ExecTransType.New, ExecTransType.Cancel, ExecTransType.Correct, ExecTransType.Status];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExecTransType type', () => {
        const validate = (value: ExecTransType) => {
            expect(Object.values(ExecTransType)).toContain(value);
        };

        validate(ExecTransType.New);
        validate(ExecTransType.Cancel);
        validate(ExecTransType.Correct);
        validate(ExecTransType.Status);
    });

    test('should be immutable', () => {
        const ref = ExecTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
