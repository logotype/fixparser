import { MultilegModel } from '../../src/fieldtypes/MultilegModel';

describe('MultilegModel', () => {
    test('should have the correct values', () => {
        expect(MultilegModel.PredefinedMultilegSecurity).toBe(0);
        expect(MultilegModel.UserDefinedMultilegSecurity).toBe(1);
        expect(MultilegModel.UserDefined).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MultilegModel.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MultilegModel.PredefinedMultilegSecurity,
            MultilegModel.UserDefinedMultilegSecurity,
            MultilegModel.UserDefined,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MultilegModel type', () => {
        const validate = (value: MultilegModel) => {
            expect(Object.values(MultilegModel)).toContain(value);
        };

        validate(MultilegModel.PredefinedMultilegSecurity);
        validate(MultilegModel.UserDefinedMultilegSecurity);
        validate(MultilegModel.UserDefined);
    });

    test('should be immutable', () => {
        const ref = MultilegModel;
        expect(() => {
            ref.PredefinedMultilegSecurity = 10;
        }).toThrowError();
    });
});
