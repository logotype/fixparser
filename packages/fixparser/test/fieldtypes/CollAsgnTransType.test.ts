import { CollAsgnTransType } from '../../src/fieldtypes/CollAsgnTransType';

describe('CollAsgnTransType', () => {
    test('should have the correct values', () => {
        expect(CollAsgnTransType.New).toBe(0);
        expect(CollAsgnTransType.Replace).toBe(1);
        expect(CollAsgnTransType.Cancel).toBe(2);
        expect(CollAsgnTransType.Release).toBe(3);
        expect(CollAsgnTransType.Reverse).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollAsgnTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollAsgnTransType.New,
            CollAsgnTransType.Replace,
            CollAsgnTransType.Cancel,
            CollAsgnTransType.Release,
            CollAsgnTransType.Reverse,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollAsgnTransType type', () => {
        const validate = (value: CollAsgnTransType) => {
            expect(Object.values(CollAsgnTransType)).toContain(value);
        };

        validate(CollAsgnTransType.New);
        validate(CollAsgnTransType.Replace);
        validate(CollAsgnTransType.Cancel);
        validate(CollAsgnTransType.Release);
        validate(CollAsgnTransType.Reverse);
    });

    test('should be immutable', () => {
        const ref = CollAsgnTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
