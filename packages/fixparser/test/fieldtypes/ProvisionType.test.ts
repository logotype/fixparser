import { ProvisionType } from '../../src/fieldtypes/ProvisionType';

describe('ProvisionType', () => {
    test('should have the correct values', () => {
        expect(ProvisionType.MandatoryEarlyTermination).toBe(0);
        expect(ProvisionType.OptionalEarlyTermination).toBe(1);
        expect(ProvisionType.Cancelable).toBe(2);
        expect(ProvisionType.Extendable).toBe(3);
        expect(ProvisionType.MutualEarlyTermination).toBe(4);
        expect(ProvisionType.Evergreen).toBe(5);
        expect(ProvisionType.Callable).toBe(6);
        expect(ProvisionType.Puttable).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProvisionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProvisionType.MandatoryEarlyTermination,
            ProvisionType.OptionalEarlyTermination,
            ProvisionType.Cancelable,
            ProvisionType.Extendable,
            ProvisionType.MutualEarlyTermination,
            ProvisionType.Evergreen,
            ProvisionType.Callable,
            ProvisionType.Puttable,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ProvisionType type', () => {
        const validate = (value: ProvisionType) => {
            expect(Object.values(ProvisionType)).toContain(value);
        };

        validate(ProvisionType.MandatoryEarlyTermination);
        validate(ProvisionType.OptionalEarlyTermination);
        validate(ProvisionType.Cancelable);
        validate(ProvisionType.Extendable);
        validate(ProvisionType.MutualEarlyTermination);
        validate(ProvisionType.Evergreen);
        validate(ProvisionType.Callable);
        validate(ProvisionType.Puttable);
    });

    test('should be immutable', () => {
        const ref = ProvisionType;
        expect(() => {
            ref.MandatoryEarlyTermination = 10;
        }).toThrowError();
    });
});
