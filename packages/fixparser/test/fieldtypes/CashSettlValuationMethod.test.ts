import { CashSettlValuationMethod } from '../../src/fieldtypes/CashSettlValuationMethod';

describe('CashSettlValuationMethod', () => {
    test('should have the correct values', () => {
        expect(CashSettlValuationMethod.Market).toBe(0);
        expect(CashSettlValuationMethod.Highest).toBe(1);
        expect(CashSettlValuationMethod.AverageMarket).toBe(2);
        expect(CashSettlValuationMethod.AverageHighest).toBe(3);
        expect(CashSettlValuationMethod.BlendedMarket).toBe(4);
        expect(CashSettlValuationMethod.BlendedHighest).toBe(5);
        expect(CashSettlValuationMethod.AverageBlendedMarket).toBe(6);
        expect(CashSettlValuationMethod.AverageBlendedHighest).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CashSettlValuationMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CashSettlValuationMethod.Market,
            CashSettlValuationMethod.Highest,
            CashSettlValuationMethod.AverageMarket,
            CashSettlValuationMethod.AverageHighest,
            CashSettlValuationMethod.BlendedMarket,
            CashSettlValuationMethod.BlendedHighest,
            CashSettlValuationMethod.AverageBlendedMarket,
            CashSettlValuationMethod.AverageBlendedHighest,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CashSettlValuationMethod type', () => {
        const validate = (value: CashSettlValuationMethod) => {
            expect(Object.values(CashSettlValuationMethod)).toContain(value);
        };

        validate(CashSettlValuationMethod.Market);
        validate(CashSettlValuationMethod.Highest);
        validate(CashSettlValuationMethod.AverageMarket);
        validate(CashSettlValuationMethod.AverageHighest);
        validate(CashSettlValuationMethod.BlendedMarket);
        validate(CashSettlValuationMethod.BlendedHighest);
        validate(CashSettlValuationMethod.AverageBlendedMarket);
        validate(CashSettlValuationMethod.AverageBlendedHighest);
    });

    test('should be immutable', () => {
        const ref = CashSettlValuationMethod;
        expect(() => {
            ref.Market = 10;
        }).toThrowError();
    });
});
