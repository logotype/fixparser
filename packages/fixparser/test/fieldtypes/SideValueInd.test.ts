import { SideValueInd } from '../../src/fieldtypes/SideValueInd';

describe('SideValueInd', () => {
    test('should have the correct values', () => {
        expect(SideValueInd.SideValue1).toBe(1);
        expect(SideValueInd.SideValue2).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SideValueInd.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [SideValueInd.SideValue1, SideValueInd.SideValue2];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SideValueInd type', () => {
        const validate = (value: SideValueInd) => {
            expect(Object.values(SideValueInd)).toContain(value);
        };

        validate(SideValueInd.SideValue1);
        validate(SideValueInd.SideValue2);
    });

    test('should be immutable', () => {
        const ref = SideValueInd;
        expect(() => {
            ref.SideValue1 = 10;
        }).toThrowError();
    });
});
