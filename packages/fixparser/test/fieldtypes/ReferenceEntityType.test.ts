import { ReferenceEntityType } from '../../src/fieldtypes/ReferenceEntityType';

describe('ReferenceEntityType', () => {
    test('should have the correct values', () => {
        expect(ReferenceEntityType.Asian).toBe(1);
        expect(ReferenceEntityType.AustralianNewZealand).toBe(2);
        expect(ReferenceEntityType.EuropeanEmergingMarkets).toBe(3);
        expect(ReferenceEntityType.Japanese).toBe(4);
        expect(ReferenceEntityType.NorthAmericanHighYield).toBe(5);
        expect(ReferenceEntityType.NorthAmericanInsurance).toBe(6);
        expect(ReferenceEntityType.NorthAmericanInvestmentGrade).toBe(7);
        expect(ReferenceEntityType.Singaporean).toBe(8);
        expect(ReferenceEntityType.WesternEuropean).toBe(9);
        expect(ReferenceEntityType.WesternEuropeanInsurance).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReferenceEntityType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReferenceEntityType.Asian,
            ReferenceEntityType.AustralianNewZealand,
            ReferenceEntityType.EuropeanEmergingMarkets,
            ReferenceEntityType.Japanese,
            ReferenceEntityType.NorthAmericanHighYield,
            ReferenceEntityType.NorthAmericanInsurance,
            ReferenceEntityType.NorthAmericanInvestmentGrade,
            ReferenceEntityType.Singaporean,
            ReferenceEntityType.WesternEuropean,
            ReferenceEntityType.WesternEuropeanInsurance,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReferenceEntityType type', () => {
        const validate = (value: ReferenceEntityType) => {
            expect(Object.values(ReferenceEntityType)).toContain(value);
        };

        validate(ReferenceEntityType.Asian);
        validate(ReferenceEntityType.AustralianNewZealand);
        validate(ReferenceEntityType.EuropeanEmergingMarkets);
        validate(ReferenceEntityType.Japanese);
        validate(ReferenceEntityType.NorthAmericanHighYield);
        validate(ReferenceEntityType.NorthAmericanInsurance);
        validate(ReferenceEntityType.NorthAmericanInvestmentGrade);
        validate(ReferenceEntityType.Singaporean);
        validate(ReferenceEntityType.WesternEuropean);
        validate(ReferenceEntityType.WesternEuropeanInsurance);
    });

    test('should be immutable', () => {
        const ref = ReferenceEntityType;
        expect(() => {
            ref.Asian = 10;
        }).toThrowError();
    });
});
