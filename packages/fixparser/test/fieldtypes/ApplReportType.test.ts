import { ApplReportType } from '../../src/fieldtypes/ApplReportType';

describe('ApplReportType', () => {
    test('should have the correct values', () => {
        expect(ApplReportType.ApplSeqNumReset).toBe(0);
        expect(ApplReportType.LastMessageSent).toBe(1);
        expect(ApplReportType.ApplicationAlive).toBe(2);
        expect(ApplReportType.ResendComplete).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ApplReportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ApplReportType.ApplSeqNumReset,
            ApplReportType.LastMessageSent,
            ApplReportType.ApplicationAlive,
            ApplReportType.ResendComplete,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ApplReportType type', () => {
        const validate = (value: ApplReportType) => {
            expect(Object.values(ApplReportType)).toContain(value);
        };

        validate(ApplReportType.ApplSeqNumReset);
        validate(ApplReportType.LastMessageSent);
        validate(ApplReportType.ApplicationAlive);
        validate(ApplReportType.ResendComplete);
    });

    test('should be immutable', () => {
        const ref = ApplReportType;
        expect(() => {
            ref.ApplSeqNumReset = 10;
        }).toThrowError();
    });
});
