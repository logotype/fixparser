import { PossResend } from '../../src/fieldtypes/PossResend';

describe('PossResend', () => {
    test('should have the correct values', () => {
        expect(PossResend.OriginalTransmission).toBe('N');
        expect(PossResend.PossibleResend).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PossResend.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PossResend.OriginalTransmission, PossResend.PossibleResend];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PossResend type', () => {
        const validate = (value: PossResend) => {
            expect(Object.values(PossResend)).toContain(value);
        };

        validate(PossResend.OriginalTransmission);
        validate(PossResend.PossibleResend);
    });

    test('should be immutable', () => {
        const ref = PossResend;
        expect(() => {
            ref.OriginalTransmission = 10;
        }).toThrowError();
    });
});
