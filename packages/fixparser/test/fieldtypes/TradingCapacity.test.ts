import { TradingCapacity } from '../../src/fieldtypes/TradingCapacity';

describe('TradingCapacity', () => {
    test('should have the correct values', () => {
        expect(TradingCapacity.Customer).toBe(1);
        expect(TradingCapacity.CustomerProfessional).toBe(2);
        expect(TradingCapacity.BrokerDealer).toBe(3);
        expect(TradingCapacity.CustomerBrokerDealer).toBe(4);
        expect(TradingCapacity.Principal).toBe(5);
        expect(TradingCapacity.MarketMaker).toBe(6);
        expect(TradingCapacity.AwayMarketMaker).toBe(7);
        expect(TradingCapacity.SystematicInternaliser).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TradingCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TradingCapacity.Customer,
            TradingCapacity.CustomerProfessional,
            TradingCapacity.BrokerDealer,
            TradingCapacity.CustomerBrokerDealer,
            TradingCapacity.Principal,
            TradingCapacity.MarketMaker,
            TradingCapacity.AwayMarketMaker,
            TradingCapacity.SystematicInternaliser,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TradingCapacity type', () => {
        const validate = (value: TradingCapacity) => {
            expect(Object.values(TradingCapacity)).toContain(value);
        };

        validate(TradingCapacity.Customer);
        validate(TradingCapacity.CustomerProfessional);
        validate(TradingCapacity.BrokerDealer);
        validate(TradingCapacity.CustomerBrokerDealer);
        validate(TradingCapacity.Principal);
        validate(TradingCapacity.MarketMaker);
        validate(TradingCapacity.AwayMarketMaker);
        validate(TradingCapacity.SystematicInternaliser);
    });

    test('should be immutable', () => {
        const ref = TradingCapacity;
        expect(() => {
            ref.Customer = 10;
        }).toThrowError();
    });
});
