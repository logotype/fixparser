import { AllocIntermedReqType } from '../../src/fieldtypes/AllocIntermedReqType';

describe('AllocIntermedReqType', () => {
    test('should have the correct values', () => {
        expect(AllocIntermedReqType.PendingAccept).toBe(1);
        expect(AllocIntermedReqType.PendingRelease).toBe(2);
        expect(AllocIntermedReqType.PendingReversal).toBe(3);
        expect(AllocIntermedReqType.Accept).toBe(4);
        expect(AllocIntermedReqType.BlockLevelReject).toBe(5);
        expect(AllocIntermedReqType.AccountLevelReject).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocIntermedReqType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocIntermedReqType.PendingAccept,
            AllocIntermedReqType.PendingRelease,
            AllocIntermedReqType.PendingReversal,
            AllocIntermedReqType.Accept,
            AllocIntermedReqType.BlockLevelReject,
            AllocIntermedReqType.AccountLevelReject,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocIntermedReqType type', () => {
        const validate = (value: AllocIntermedReqType) => {
            expect(Object.values(AllocIntermedReqType)).toContain(value);
        };

        validate(AllocIntermedReqType.PendingAccept);
        validate(AllocIntermedReqType.PendingRelease);
        validate(AllocIntermedReqType.PendingReversal);
        validate(AllocIntermedReqType.Accept);
        validate(AllocIntermedReqType.BlockLevelReject);
        validate(AllocIntermedReqType.AccountLevelReject);
    });

    test('should be immutable', () => {
        const ref = AllocIntermedReqType;
        expect(() => {
            ref.PendingAccept = 10;
        }).toThrowError();
    });
});
