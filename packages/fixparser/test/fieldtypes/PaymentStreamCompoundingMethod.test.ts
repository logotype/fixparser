import { PaymentStreamCompoundingMethod } from '../../src/fieldtypes/PaymentStreamCompoundingMethod';

describe('PaymentStreamCompoundingMethod', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamCompoundingMethod.None).toBe(0);
        expect(PaymentStreamCompoundingMethod.Flat).toBe(1);
        expect(PaymentStreamCompoundingMethod.Straight).toBe(2);
        expect(PaymentStreamCompoundingMethod.SpreadExclusive).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamCompoundingMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamCompoundingMethod.None,
            PaymentStreamCompoundingMethod.Flat,
            PaymentStreamCompoundingMethod.Straight,
            PaymentStreamCompoundingMethod.SpreadExclusive,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamCompoundingMethod type', () => {
        const validate = (value: PaymentStreamCompoundingMethod) => {
            expect(Object.values(PaymentStreamCompoundingMethod)).toContain(value);
        };

        validate(PaymentStreamCompoundingMethod.None);
        validate(PaymentStreamCompoundingMethod.Flat);
        validate(PaymentStreamCompoundingMethod.Straight);
        validate(PaymentStreamCompoundingMethod.SpreadExclusive);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamCompoundingMethod;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
