import { RegistStatus } from '../../src/fieldtypes/RegistStatus';

describe('RegistStatus', () => {
    test('should have the correct values', () => {
        expect(RegistStatus.Accepted).toBe('A');
        expect(RegistStatus.Rejected).toBe('R');
        expect(RegistStatus.Held).toBe('H');
        expect(RegistStatus.Reminder).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegistStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegistStatus.Accepted,
            RegistStatus.Rejected,
            RegistStatus.Held,
            RegistStatus.Reminder,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for RegistStatus type', () => {
        const validate = (value: RegistStatus) => {
            expect(Object.values(RegistStatus)).toContain(value);
        };

        validate(RegistStatus.Accepted);
        validate(RegistStatus.Rejected);
        validate(RegistStatus.Held);
        validate(RegistStatus.Reminder);
    });

    test('should be immutable', () => {
        const ref = RegistStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
