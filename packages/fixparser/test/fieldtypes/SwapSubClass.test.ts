import { SwapSubClass } from '../../src/fieldtypes/SwapSubClass';

describe('SwapSubClass', () => {
    test('should have the correct values', () => {
        expect(SwapSubClass.Amortizing).toBe('AMTZ');
        expect(SwapSubClass.Compounding).toBe('COMP');
        expect(SwapSubClass.ConstantNotionalSchedule).toBe('CNST');
        expect(SwapSubClass.AccretingNotionalSchedule).toBe('ACRT');
        expect(SwapSubClass.CustomNotionalSchedule).toBe('CUST');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SwapSubClass.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SwapSubClass.Amortizing,
            SwapSubClass.Compounding,
            SwapSubClass.ConstantNotionalSchedule,
            SwapSubClass.AccretingNotionalSchedule,
            SwapSubClass.CustomNotionalSchedule,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SwapSubClass type', () => {
        const validate = (value: SwapSubClass) => {
            expect(Object.values(SwapSubClass)).toContain(value);
        };

        validate(SwapSubClass.Amortizing);
        validate(SwapSubClass.Compounding);
        validate(SwapSubClass.ConstantNotionalSchedule);
        validate(SwapSubClass.AccretingNotionalSchedule);
        validate(SwapSubClass.CustomNotionalSchedule);
    });

    test('should be immutable', () => {
        const ref = SwapSubClass;
        expect(() => {
            ref.Amortizing = 10;
        }).toThrowError();
    });
});
