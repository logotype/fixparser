import { CollInquiryQualifier } from '../../src/fieldtypes/CollInquiryQualifier';

describe('CollInquiryQualifier', () => {
    test('should have the correct values', () => {
        expect(CollInquiryQualifier.TradeDate).toBe(0);
        expect(CollInquiryQualifier.GCInstrument).toBe(1);
        expect(CollInquiryQualifier.CollateralInstrument).toBe(2);
        expect(CollInquiryQualifier.SubstitutionEligible).toBe(3);
        expect(CollInquiryQualifier.NotAssigned).toBe(4);
        expect(CollInquiryQualifier.PartiallyAssigned).toBe(5);
        expect(CollInquiryQualifier.FullyAssigned).toBe(6);
        expect(CollInquiryQualifier.OutstandingTrades).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CollInquiryQualifier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            CollInquiryQualifier.TradeDate,
            CollInquiryQualifier.GCInstrument,
            CollInquiryQualifier.CollateralInstrument,
            CollInquiryQualifier.SubstitutionEligible,
            CollInquiryQualifier.NotAssigned,
            CollInquiryQualifier.PartiallyAssigned,
            CollInquiryQualifier.FullyAssigned,
            CollInquiryQualifier.OutstandingTrades,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for CollInquiryQualifier type', () => {
        const validate = (value: CollInquiryQualifier) => {
            expect(Object.values(CollInquiryQualifier)).toContain(value);
        };

        validate(CollInquiryQualifier.TradeDate);
        validate(CollInquiryQualifier.GCInstrument);
        validate(CollInquiryQualifier.CollateralInstrument);
        validate(CollInquiryQualifier.SubstitutionEligible);
        validate(CollInquiryQualifier.NotAssigned);
        validate(CollInquiryQualifier.PartiallyAssigned);
        validate(CollInquiryQualifier.FullyAssigned);
        validate(CollInquiryQualifier.OutstandingTrades);
    });

    test('should be immutable', () => {
        const ref = CollInquiryQualifier;
        expect(() => {
            ref.TradeDate = 10;
        }).toThrowError();
    });
});
