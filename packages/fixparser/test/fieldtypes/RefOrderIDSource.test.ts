import { RefOrderIDSource } from '../../src/fieldtypes/RefOrderIDSource';

describe('RefOrderIDSource', () => {
    test('should have the correct values', () => {
        expect(RefOrderIDSource.SecondaryOrderID).toBe('0');
        expect(RefOrderIDSource.OrderID).toBe('1');
        expect(RefOrderIDSource.MDEntryID).toBe('2');
        expect(RefOrderIDSource.QuoteEntryID).toBe('3');
        expect(RefOrderIDSource.OriginalOrderID).toBe('4');
        expect(RefOrderIDSource.QuoteID).toBe('5');
        expect(RefOrderIDSource.QuoteReqID).toBe('6');
        expect(RefOrderIDSource.PreviousOrderIdentifier).toBe('7');
        expect(RefOrderIDSource.PreviousQuoteIdentifier).toBe('8');
        expect(RefOrderIDSource.ParentOrderIdentifier).toBe('9');
        expect(RefOrderIDSource.ManualOrderIdentifier).toBe('A');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RefOrderIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RefOrderIDSource.SecondaryOrderID,
            RefOrderIDSource.OrderID,
            RefOrderIDSource.MDEntryID,
            RefOrderIDSource.QuoteEntryID,
            RefOrderIDSource.OriginalOrderID,
            RefOrderIDSource.QuoteID,
            RefOrderIDSource.QuoteReqID,
            RefOrderIDSource.PreviousOrderIdentifier,
            RefOrderIDSource.PreviousQuoteIdentifier,
            RefOrderIDSource.ParentOrderIdentifier,
            RefOrderIDSource.ManualOrderIdentifier,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for RefOrderIDSource type', () => {
        const validate = (value: RefOrderIDSource) => {
            expect(Object.values(RefOrderIDSource)).toContain(value);
        };

        validate(RefOrderIDSource.SecondaryOrderID);
        validate(RefOrderIDSource.OrderID);
        validate(RefOrderIDSource.MDEntryID);
        validate(RefOrderIDSource.QuoteEntryID);
        validate(RefOrderIDSource.OriginalOrderID);
        validate(RefOrderIDSource.QuoteID);
        validate(RefOrderIDSource.QuoteReqID);
        validate(RefOrderIDSource.PreviousOrderIdentifier);
        validate(RefOrderIDSource.PreviousQuoteIdentifier);
        validate(RefOrderIDSource.ParentOrderIdentifier);
        validate(RefOrderIDSource.ManualOrderIdentifier);
    });

    test('should be immutable', () => {
        const ref = RefOrderIDSource;
        expect(() => {
            ref.SecondaryOrderID = 10;
        }).toThrowError();
    });
});
