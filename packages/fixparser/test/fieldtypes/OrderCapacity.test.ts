import { OrderCapacity } from '../../src/fieldtypes/OrderCapacity';

describe('OrderCapacity', () => {
    test('should have the correct values', () => {
        expect(OrderCapacity.Agency).toBe('A');
        expect(OrderCapacity.Proprietary).toBe('G');
        expect(OrderCapacity.Individual).toBe('I');
        expect(OrderCapacity.Principal).toBe('P');
        expect(OrderCapacity.RisklessPrincipal).toBe('R');
        expect(OrderCapacity.AgentForOtherMember).toBe('W');
        expect(OrderCapacity.MixedCapacity).toBe('M');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderCapacity.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderCapacity.Agency,
            OrderCapacity.Proprietary,
            OrderCapacity.Individual,
            OrderCapacity.Principal,
            OrderCapacity.RisklessPrincipal,
            OrderCapacity.AgentForOtherMember,
            OrderCapacity.MixedCapacity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrderCapacity type', () => {
        const validate = (value: OrderCapacity) => {
            expect(Object.values(OrderCapacity)).toContain(value);
        };

        validate(OrderCapacity.Agency);
        validate(OrderCapacity.Proprietary);
        validate(OrderCapacity.Individual);
        validate(OrderCapacity.Principal);
        validate(OrderCapacity.RisklessPrincipal);
        validate(OrderCapacity.AgentForOtherMember);
        validate(OrderCapacity.MixedCapacity);
    });

    test('should be immutable', () => {
        const ref = OrderCapacity;
        expect(() => {
            ref.Agency = 10;
        }).toThrowError();
    });
});
