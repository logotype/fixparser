import { MatchExceptionElementType } from '../../src/fieldtypes/MatchExceptionElementType';

describe('MatchExceptionElementType', () => {
    test('should have the correct values', () => {
        expect(MatchExceptionElementType.AccruedInterest).toBe(1);
        expect(MatchExceptionElementType.DealPrice).toBe(2);
        expect(MatchExceptionElementType.TradeDate).toBe(3);
        expect(MatchExceptionElementType.SettlementDate).toBe(4);
        expect(MatchExceptionElementType.SideIndicator).toBe(5);
        expect(MatchExceptionElementType.TradedCurrency).toBe(6);
        expect(MatchExceptionElementType.AccountID).toBe(7);
        expect(MatchExceptionElementType.ExecutingBrokerID).toBe(8);
        expect(MatchExceptionElementType.SettlementCurrencyAndAmount).toBe(9);
        expect(MatchExceptionElementType.InvestmentManagerID).toBe(10);
        expect(MatchExceptionElementType.NetAmount).toBe(11);
        expect(MatchExceptionElementType.PlaceOfSettlement).toBe(12);
        expect(MatchExceptionElementType.Commissions).toBe(13);
        expect(MatchExceptionElementType.SecurityIdentifier).toBe(14);
        expect(MatchExceptionElementType.QualityAllocated).toBe(15);
        expect(MatchExceptionElementType.Principal).toBe(16);
        expect(MatchExceptionElementType.Fees).toBe(17);
        expect(MatchExceptionElementType.Tax).toBe(18);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MatchExceptionElementType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MatchExceptionElementType.AccruedInterest,
            MatchExceptionElementType.DealPrice,
            MatchExceptionElementType.TradeDate,
            MatchExceptionElementType.SettlementDate,
            MatchExceptionElementType.SideIndicator,
            MatchExceptionElementType.TradedCurrency,
            MatchExceptionElementType.AccountID,
            MatchExceptionElementType.ExecutingBrokerID,
            MatchExceptionElementType.SettlementCurrencyAndAmount,
            MatchExceptionElementType.InvestmentManagerID,
            MatchExceptionElementType.NetAmount,
            MatchExceptionElementType.PlaceOfSettlement,
            MatchExceptionElementType.Commissions,
            MatchExceptionElementType.SecurityIdentifier,
            MatchExceptionElementType.QualityAllocated,
            MatchExceptionElementType.Principal,
            MatchExceptionElementType.Fees,
            MatchExceptionElementType.Tax,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MatchExceptionElementType type', () => {
        const validate = (value: MatchExceptionElementType) => {
            expect(Object.values(MatchExceptionElementType)).toContain(value);
        };

        validate(MatchExceptionElementType.AccruedInterest);
        validate(MatchExceptionElementType.DealPrice);
        validate(MatchExceptionElementType.TradeDate);
        validate(MatchExceptionElementType.SettlementDate);
        validate(MatchExceptionElementType.SideIndicator);
        validate(MatchExceptionElementType.TradedCurrency);
        validate(MatchExceptionElementType.AccountID);
        validate(MatchExceptionElementType.ExecutingBrokerID);
        validate(MatchExceptionElementType.SettlementCurrencyAndAmount);
        validate(MatchExceptionElementType.InvestmentManagerID);
        validate(MatchExceptionElementType.NetAmount);
        validate(MatchExceptionElementType.PlaceOfSettlement);
        validate(MatchExceptionElementType.Commissions);
        validate(MatchExceptionElementType.SecurityIdentifier);
        validate(MatchExceptionElementType.QualityAllocated);
        validate(MatchExceptionElementType.Principal);
        validate(MatchExceptionElementType.Fees);
        validate(MatchExceptionElementType.Tax);
    });

    test('should be immutable', () => {
        const ref = MatchExceptionElementType;
        expect(() => {
            ref.AccruedInterest = 10;
        }).toThrowError();
    });
});
