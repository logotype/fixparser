import { PaymentSubType } from '../../src/fieldtypes/PaymentSubType';

describe('PaymentSubType', () => {
    test('should have the correct values', () => {
        expect(PaymentSubType.Initial).toBe(0);
        expect(PaymentSubType.Intermediate).toBe(1);
        expect(PaymentSubType.Final).toBe(2);
        expect(PaymentSubType.Prepaid).toBe(3);
        expect(PaymentSubType.Postpaid).toBe(4);
        expect(PaymentSubType.Variable).toBe(5);
        expect(PaymentSubType.Fixed).toBe(6);
        expect(PaymentSubType.Swap).toBe(7);
        expect(PaymentSubType.Conditional).toBe(8);
        expect(PaymentSubType.FixedRate).toBe(9);
        expect(PaymentSubType.FloatingRate).toBe(10);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentSubType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentSubType.Initial,
            PaymentSubType.Intermediate,
            PaymentSubType.Final,
            PaymentSubType.Prepaid,
            PaymentSubType.Postpaid,
            PaymentSubType.Variable,
            PaymentSubType.Fixed,
            PaymentSubType.Swap,
            PaymentSubType.Conditional,
            PaymentSubType.FixedRate,
            PaymentSubType.FloatingRate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentSubType type', () => {
        const validate = (value: PaymentSubType) => {
            expect(Object.values(PaymentSubType)).toContain(value);
        };

        validate(PaymentSubType.Initial);
        validate(PaymentSubType.Intermediate);
        validate(PaymentSubType.Final);
        validate(PaymentSubType.Prepaid);
        validate(PaymentSubType.Postpaid);
        validate(PaymentSubType.Variable);
        validate(PaymentSubType.Fixed);
        validate(PaymentSubType.Swap);
        validate(PaymentSubType.Conditional);
        validate(PaymentSubType.FixedRate);
        validate(PaymentSubType.FloatingRate);
    });

    test('should be immutable', () => {
        const ref = PaymentSubType;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
