import { SecurityListType } from '../../src/fieldtypes/SecurityListType';

describe('SecurityListType', () => {
    test('should have the correct values', () => {
        expect(SecurityListType.IndustryClassification).toBe(1);
        expect(SecurityListType.TradingList).toBe(2);
        expect(SecurityListType.Market).toBe(3);
        expect(SecurityListType.NewspaperList).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityListType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityListType.IndustryClassification,
            SecurityListType.TradingList,
            SecurityListType.Market,
            SecurityListType.NewspaperList,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityListType type', () => {
        const validate = (value: SecurityListType) => {
            expect(Object.values(SecurityListType)).toContain(value);
        };

        validate(SecurityListType.IndustryClassification);
        validate(SecurityListType.TradingList);
        validate(SecurityListType.Market);
        validate(SecurityListType.NewspaperList);
    });

    test('should be immutable', () => {
        const ref = SecurityListType;
        expect(() => {
            ref.IndustryClassification = 10;
        }).toThrowError();
    });
});
