import { RiskLimitType } from '../../src/fieldtypes/RiskLimitType';

describe('RiskLimitType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitType.CreditLimit).toBe(0);
        expect(RiskLimitType.GrossLimit).toBe(1);
        expect(RiskLimitType.NetLimit).toBe(2);
        expect(RiskLimitType.Exposure).toBe(3);
        expect(RiskLimitType.LongLimit).toBe(4);
        expect(RiskLimitType.ShortLimit).toBe(5);
        expect(RiskLimitType.CashMargin).toBe(6);
        expect(RiskLimitType.AdditionalMargin).toBe(7);
        expect(RiskLimitType.TotalMargin).toBe(8);
        expect(RiskLimitType.LimitConsumed).toBe(9);
        expect(RiskLimitType.ClipSize).toBe(10);
        expect(RiskLimitType.MaxNotionalOrderSize).toBe(11);
        expect(RiskLimitType.DV01PV01Limit).toBe(12);
        expect(RiskLimitType.CS01Limit).toBe(13);
        expect(RiskLimitType.VolumeLimitPerTimePeriod).toBe(14);
        expect(RiskLimitType.VolFilledPctOrdVolTmPeriod).toBe(15);
        expect(RiskLimitType.NotlFilledPctNotlTmPeriod).toBe(16);
        expect(RiskLimitType.TransactionExecutionLimitPerTimePeriod).toBe(17);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitType.CreditLimit,
            RiskLimitType.GrossLimit,
            RiskLimitType.NetLimit,
            RiskLimitType.Exposure,
            RiskLimitType.LongLimit,
            RiskLimitType.ShortLimit,
            RiskLimitType.CashMargin,
            RiskLimitType.AdditionalMargin,
            RiskLimitType.TotalMargin,
            RiskLimitType.LimitConsumed,
            RiskLimitType.ClipSize,
            RiskLimitType.MaxNotionalOrderSize,
            RiskLimitType.DV01PV01Limit,
            RiskLimitType.CS01Limit,
            RiskLimitType.VolumeLimitPerTimePeriod,
            RiskLimitType.VolFilledPctOrdVolTmPeriod,
            RiskLimitType.NotlFilledPctNotlTmPeriod,
            RiskLimitType.TransactionExecutionLimitPerTimePeriod,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitType type', () => {
        const validate = (value: RiskLimitType) => {
            expect(Object.values(RiskLimitType)).toContain(value);
        };

        validate(RiskLimitType.CreditLimit);
        validate(RiskLimitType.GrossLimit);
        validate(RiskLimitType.NetLimit);
        validate(RiskLimitType.Exposure);
        validate(RiskLimitType.LongLimit);
        validate(RiskLimitType.ShortLimit);
        validate(RiskLimitType.CashMargin);
        validate(RiskLimitType.AdditionalMargin);
        validate(RiskLimitType.TotalMargin);
        validate(RiskLimitType.LimitConsumed);
        validate(RiskLimitType.ClipSize);
        validate(RiskLimitType.MaxNotionalOrderSize);
        validate(RiskLimitType.DV01PV01Limit);
        validate(RiskLimitType.CS01Limit);
        validate(RiskLimitType.VolumeLimitPerTimePeriod);
        validate(RiskLimitType.VolFilledPctOrdVolTmPeriod);
        validate(RiskLimitType.NotlFilledPctNotlTmPeriod);
        validate(RiskLimitType.TransactionExecutionLimitPerTimePeriod);
    });

    test('should be immutable', () => {
        const ref = RiskLimitType;
        expect(() => {
            ref.CreditLimit = 10;
        }).toThrowError();
    });
});
