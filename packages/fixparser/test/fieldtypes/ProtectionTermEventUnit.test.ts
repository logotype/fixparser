import { ProtectionTermEventUnit } from '../../src/fieldtypes/ProtectionTermEventUnit';

describe('ProtectionTermEventUnit', () => {
    test('should have the correct values', () => {
        expect(ProtectionTermEventUnit.Day).toBe('D');
        expect(ProtectionTermEventUnit.Week).toBe('Wk');
        expect(ProtectionTermEventUnit.Month).toBe('Mo');
        expect(ProtectionTermEventUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ProtectionTermEventUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ProtectionTermEventUnit.Day,
            ProtectionTermEventUnit.Week,
            ProtectionTermEventUnit.Month,
            ProtectionTermEventUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ProtectionTermEventUnit type', () => {
        const validate = (value: ProtectionTermEventUnit) => {
            expect(Object.values(ProtectionTermEventUnit)).toContain(value);
        };

        validate(ProtectionTermEventUnit.Day);
        validate(ProtectionTermEventUnit.Week);
        validate(ProtectionTermEventUnit.Month);
        validate(ProtectionTermEventUnit.Year);
    });

    test('should be immutable', () => {
        const ref = ProtectionTermEventUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
