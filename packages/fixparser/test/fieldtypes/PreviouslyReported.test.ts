import { PreviouslyReported } from '../../src/fieldtypes/PreviouslyReported';

describe('PreviouslyReported', () => {
    test('should have the correct values', () => {
        expect(PreviouslyReported.NotReportedToCounterparty).toBe('N');
        expect(PreviouslyReported.PreviouslyReportedToCounterparty).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PreviouslyReported.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PreviouslyReported.NotReportedToCounterparty,
            PreviouslyReported.PreviouslyReportedToCounterparty,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PreviouslyReported type', () => {
        const validate = (value: PreviouslyReported) => {
            expect(Object.values(PreviouslyReported)).toContain(value);
        };

        validate(PreviouslyReported.NotReportedToCounterparty);
        validate(PreviouslyReported.PreviouslyReportedToCounterparty);
    });

    test('should be immutable', () => {
        const ref = PreviouslyReported;
        expect(() => {
            ref.NotReportedToCounterparty = 10;
        }).toThrowError();
    });
});
