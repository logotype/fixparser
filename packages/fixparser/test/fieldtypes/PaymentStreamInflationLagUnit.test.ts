import { PaymentStreamInflationLagUnit } from '../../src/fieldtypes/PaymentStreamInflationLagUnit';

describe('PaymentStreamInflationLagUnit', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamInflationLagUnit.Day).toBe('D');
        expect(PaymentStreamInflationLagUnit.Week).toBe('Wk');
        expect(PaymentStreamInflationLagUnit.Month).toBe('Mo');
        expect(PaymentStreamInflationLagUnit.Year).toBe('Yr');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamInflationLagUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamInflationLagUnit.Day,
            PaymentStreamInflationLagUnit.Week,
            PaymentStreamInflationLagUnit.Month,
            PaymentStreamInflationLagUnit.Year,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PaymentStreamInflationLagUnit type', () => {
        const validate = (value: PaymentStreamInflationLagUnit) => {
            expect(Object.values(PaymentStreamInflationLagUnit)).toContain(value);
        };

        validate(PaymentStreamInflationLagUnit.Day);
        validate(PaymentStreamInflationLagUnit.Week);
        validate(PaymentStreamInflationLagUnit.Month);
        validate(PaymentStreamInflationLagUnit.Year);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamInflationLagUnit;
        expect(() => {
            ref.Day = 10;
        }).toThrowError();
    });
});
