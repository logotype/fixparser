import { ListRejectReason } from '../../src/fieldtypes/ListRejectReason';

describe('ListRejectReason', () => {
    test('should have the correct values', () => {
        expect(ListRejectReason.BrokerCredit).toBe(0);
        expect(ListRejectReason.ExchangeClosed).toBe(2);
        expect(ListRejectReason.TooLateToEnter).toBe(4);
        expect(ListRejectReason.UnknownOrder).toBe(5);
        expect(ListRejectReason.DuplicateOrder).toBe(6);
        expect(ListRejectReason.UnsupportedOrderCharacteristic).toBe(11);
        expect(ListRejectReason.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ListRejectReason.BrokerCredit,
            ListRejectReason.ExchangeClosed,
            ListRejectReason.TooLateToEnter,
            ListRejectReason.UnknownOrder,
            ListRejectReason.DuplicateOrder,
            ListRejectReason.UnsupportedOrderCharacteristic,
            ListRejectReason.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ListRejectReason type', () => {
        const validate = (value: ListRejectReason) => {
            expect(Object.values(ListRejectReason)).toContain(value);
        };

        validate(ListRejectReason.BrokerCredit);
        validate(ListRejectReason.ExchangeClosed);
        validate(ListRejectReason.TooLateToEnter);
        validate(ListRejectReason.UnknownOrder);
        validate(ListRejectReason.DuplicateOrder);
        validate(ListRejectReason.UnsupportedOrderCharacteristic);
        validate(ListRejectReason.Other);
    });

    test('should be immutable', () => {
        const ref = ListRejectReason;
        expect(() => {
            ref.BrokerCredit = 10;
        }).toThrowError();
    });
});
