import { MDStatisticValueType } from '../../src/fieldtypes/MDStatisticValueType';

describe('MDStatisticValueType', () => {
    test('should have the correct values', () => {
        expect(MDStatisticValueType.Absolute).toBe(1);
        expect(MDStatisticValueType.Percentage).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDStatisticValueType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDStatisticValueType.Absolute, MDStatisticValueType.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDStatisticValueType type', () => {
        const validate = (value: MDStatisticValueType) => {
            expect(Object.values(MDStatisticValueType)).toContain(value);
        };

        validate(MDStatisticValueType.Absolute);
        validate(MDStatisticValueType.Percentage);
    });

    test('should be immutable', () => {
        const ref = MDStatisticValueType;
        expect(() => {
            ref.Absolute = 10;
        }).toThrowError();
    });
});
