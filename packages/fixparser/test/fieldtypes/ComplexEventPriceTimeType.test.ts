import { ComplexEventPriceTimeType } from '../../src/fieldtypes/ComplexEventPriceTimeType';

describe('ComplexEventPriceTimeType', () => {
    test('should have the correct values', () => {
        expect(ComplexEventPriceTimeType.Expiration).toBe(1);
        expect(ComplexEventPriceTimeType.Immediate).toBe(2);
        expect(ComplexEventPriceTimeType.SpecifiedDate).toBe(3);
        expect(ComplexEventPriceTimeType.Close).toBe(4);
        expect(ComplexEventPriceTimeType.Open).toBe(5);
        expect(ComplexEventPriceTimeType.OfficialSettlPrice).toBe(6);
        expect(ComplexEventPriceTimeType.DerivativesClose).toBe(7);
        expect(ComplexEventPriceTimeType.AsSpecifiedMasterConfirmation).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ComplexEventPriceTimeType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ComplexEventPriceTimeType.Expiration,
            ComplexEventPriceTimeType.Immediate,
            ComplexEventPriceTimeType.SpecifiedDate,
            ComplexEventPriceTimeType.Close,
            ComplexEventPriceTimeType.Open,
            ComplexEventPriceTimeType.OfficialSettlPrice,
            ComplexEventPriceTimeType.DerivativesClose,
            ComplexEventPriceTimeType.AsSpecifiedMasterConfirmation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ComplexEventPriceTimeType type', () => {
        const validate = (value: ComplexEventPriceTimeType) => {
            expect(Object.values(ComplexEventPriceTimeType)).toContain(value);
        };

        validate(ComplexEventPriceTimeType.Expiration);
        validate(ComplexEventPriceTimeType.Immediate);
        validate(ComplexEventPriceTimeType.SpecifiedDate);
        validate(ComplexEventPriceTimeType.Close);
        validate(ComplexEventPriceTimeType.Open);
        validate(ComplexEventPriceTimeType.OfficialSettlPrice);
        validate(ComplexEventPriceTimeType.DerivativesClose);
        validate(ComplexEventPriceTimeType.AsSpecifiedMasterConfirmation);
    });

    test('should be immutable', () => {
        const ref = ComplexEventPriceTimeType;
        expect(() => {
            ref.Expiration = 10;
        }).toThrowError();
    });
});
