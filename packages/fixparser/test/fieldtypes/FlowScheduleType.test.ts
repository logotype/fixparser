import { FlowScheduleType } from '../../src/fieldtypes/FlowScheduleType';

describe('FlowScheduleType', () => {
    test('should have the correct values', () => {
        expect(FlowScheduleType.NERCEasternOffPeak).toBe(0);
        expect(FlowScheduleType.NERCWesternOffPeak).toBe(1);
        expect(FlowScheduleType.NERCCalendarAllDaysInMonth).toBe(2);
        expect(FlowScheduleType.NERCEasternPeak).toBe(3);
        expect(FlowScheduleType.NERCWesternPeak).toBe(4);
        expect(FlowScheduleType.AllTimes).toBe(5);
        expect(FlowScheduleType.OnPeak).toBe(6);
        expect(FlowScheduleType.OffPeak).toBe(7);
        expect(FlowScheduleType.Base).toBe(8);
        expect(FlowScheduleType.Block).toBe(9);
        expect(FlowScheduleType.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            FlowScheduleType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            FlowScheduleType.NERCEasternOffPeak,
            FlowScheduleType.NERCWesternOffPeak,
            FlowScheduleType.NERCCalendarAllDaysInMonth,
            FlowScheduleType.NERCEasternPeak,
            FlowScheduleType.NERCWesternPeak,
            FlowScheduleType.AllTimes,
            FlowScheduleType.OnPeak,
            FlowScheduleType.OffPeak,
            FlowScheduleType.Base,
            FlowScheduleType.Block,
            FlowScheduleType.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for FlowScheduleType type', () => {
        const validate = (value: FlowScheduleType) => {
            expect(Object.values(FlowScheduleType)).toContain(value);
        };

        validate(FlowScheduleType.NERCEasternOffPeak);
        validate(FlowScheduleType.NERCWesternOffPeak);
        validate(FlowScheduleType.NERCCalendarAllDaysInMonth);
        validate(FlowScheduleType.NERCEasternPeak);
        validate(FlowScheduleType.NERCWesternPeak);
        validate(FlowScheduleType.AllTimes);
        validate(FlowScheduleType.OnPeak);
        validate(FlowScheduleType.OffPeak);
        validate(FlowScheduleType.Base);
        validate(FlowScheduleType.Block);
        validate(FlowScheduleType.Other);
    });

    test('should be immutable', () => {
        const ref = FlowScheduleType;
        expect(() => {
            ref.NERCEasternOffPeak = 10;
        }).toThrowError();
    });
});
