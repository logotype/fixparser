import { TestSuiteRequestTransType } from '../../src/fieldtypes/TestSuiteRequestTransType';

describe('TestSuiteRequestTransType', () => {
    test('should have the correct values', () => {
        expect(TestSuiteRequestTransType.New).toBe(0);
        expect(TestSuiteRequestTransType.Cancel).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestSuiteRequestTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TestSuiteRequestTransType.New, TestSuiteRequestTransType.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestSuiteRequestTransType type', () => {
        const validate = (value: TestSuiteRequestTransType) => {
            expect(Object.values(TestSuiteRequestTransType)).toContain(value);
        };

        validate(TestSuiteRequestTransType.New);
        validate(TestSuiteRequestTransType.Cancel);
    });

    test('should be immutable', () => {
        const ref = TestSuiteRequestTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
