import { DiscretionScope } from '../../src/fieldtypes/DiscretionScope';

describe('DiscretionScope', () => {
    test('should have the correct values', () => {
        expect(DiscretionScope.Local).toBe(1);
        expect(DiscretionScope.National).toBe(2);
        expect(DiscretionScope.Global).toBe(3);
        expect(DiscretionScope.NationalExcludingLocal).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            DiscretionScope.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            DiscretionScope.Local,
            DiscretionScope.National,
            DiscretionScope.Global,
            DiscretionScope.NationalExcludingLocal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for DiscretionScope type', () => {
        const validate = (value: DiscretionScope) => {
            expect(Object.values(DiscretionScope)).toContain(value);
        };

        validate(DiscretionScope.Local);
        validate(DiscretionScope.National);
        validate(DiscretionScope.Global);
        validate(DiscretionScope.NationalExcludingLocal);
    });

    test('should be immutable', () => {
        const ref = DiscretionScope;
        expect(() => {
            ref.Local = 10;
        }).toThrowError();
    });
});
