import { PriceLimitType } from '../../src/fieldtypes/PriceLimitType';

describe('PriceLimitType', () => {
    test('should have the correct values', () => {
        expect(PriceLimitType.Price).toBe(0);
        expect(PriceLimitType.Ticks).toBe(1);
        expect(PriceLimitType.Percentage).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PriceLimitType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PriceLimitType.Price, PriceLimitType.Ticks, PriceLimitType.Percentage];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PriceLimitType type', () => {
        const validate = (value: PriceLimitType) => {
            expect(Object.values(PriceLimitType)).toContain(value);
        };

        validate(PriceLimitType.Price);
        validate(PriceLimitType.Ticks);
        validate(PriceLimitType.Percentage);
    });

    test('should be immutable', () => {
        const ref = PriceLimitType;
        expect(() => {
            ref.Price = 10;
        }).toThrowError();
    });
});
