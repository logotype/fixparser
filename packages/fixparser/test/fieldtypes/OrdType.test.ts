import { OrdType } from '../../src/fieldtypes/OrdType';

describe('OrdType', () => {
    test('should have the correct values', () => {
        expect(OrdType.Market).toBe('1');
        expect(OrdType.Limit).toBe('2');
        expect(OrdType.Stop).toBe('3');
        expect(OrdType.StopLimit).toBe('4');
        expect(OrdType.MarketOnClose).toBe('5');
        expect(OrdType.WithOrWithout).toBe('6');
        expect(OrdType.LimitOrBetter).toBe('7');
        expect(OrdType.LimitWithOrWithout).toBe('8');
        expect(OrdType.OnBasis).toBe('9');
        expect(OrdType.OnClose).toBe('A');
        expect(OrdType.LimitOnClose).toBe('B');
        expect(OrdType.ForexMarket).toBe('C');
        expect(OrdType.PreviouslyQuoted).toBe('D');
        expect(OrdType.PreviouslyIndicated).toBe('E');
        expect(OrdType.ForexLimit).toBe('F');
        expect(OrdType.ForexSwap).toBe('G');
        expect(OrdType.ForexPreviouslyQuoted).toBe('H');
        expect(OrdType.Funari).toBe('I');
        expect(OrdType.MarketIfTouched).toBe('J');
        expect(OrdType.MarketWithLeftOverAsLimit).toBe('K');
        expect(OrdType.PreviousFundValuationPoint).toBe('L');
        expect(OrdType.NextFundValuationPoint).toBe('M');
        expect(OrdType.Pegged).toBe('P');
        expect(OrdType.CounterOrderSelection).toBe('Q');
        expect(OrdType.StopOnBidOrOffer).toBe('R');
        expect(OrdType.StopLimitOnBidOrOffer).toBe('S');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrdType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrdType.Market,
            OrdType.Limit,
            OrdType.Stop,
            OrdType.StopLimit,
            OrdType.MarketOnClose,
            OrdType.WithOrWithout,
            OrdType.LimitOrBetter,
            OrdType.LimitWithOrWithout,
            OrdType.OnBasis,
            OrdType.OnClose,
            OrdType.LimitOnClose,
            OrdType.ForexMarket,
            OrdType.PreviouslyQuoted,
            OrdType.PreviouslyIndicated,
            OrdType.ForexLimit,
            OrdType.ForexSwap,
            OrdType.ForexPreviouslyQuoted,
            OrdType.Funari,
            OrdType.MarketIfTouched,
            OrdType.MarketWithLeftOverAsLimit,
            OrdType.PreviousFundValuationPoint,
            OrdType.NextFundValuationPoint,
            OrdType.Pegged,
            OrdType.CounterOrderSelection,
            OrdType.StopOnBidOrOffer,
            OrdType.StopLimitOnBidOrOffer,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for OrdType type', () => {
        const validate = (value: OrdType) => {
            expect(Object.values(OrdType)).toContain(value);
        };

        validate(OrdType.Market);
        validate(OrdType.Limit);
        validate(OrdType.Stop);
        validate(OrdType.StopLimit);
        validate(OrdType.MarketOnClose);
        validate(OrdType.WithOrWithout);
        validate(OrdType.LimitOrBetter);
        validate(OrdType.LimitWithOrWithout);
        validate(OrdType.OnBasis);
        validate(OrdType.OnClose);
        validate(OrdType.LimitOnClose);
        validate(OrdType.ForexMarket);
        validate(OrdType.PreviouslyQuoted);
        validate(OrdType.PreviouslyIndicated);
        validate(OrdType.ForexLimit);
        validate(OrdType.ForexSwap);
        validate(OrdType.ForexPreviouslyQuoted);
        validate(OrdType.Funari);
        validate(OrdType.MarketIfTouched);
        validate(OrdType.MarketWithLeftOverAsLimit);
        validate(OrdType.PreviousFundValuationPoint);
        validate(OrdType.NextFundValuationPoint);
        validate(OrdType.Pegged);
        validate(OrdType.CounterOrderSelection);
        validate(OrdType.StopOnBidOrOffer);
        validate(OrdType.StopLimitOnBidOrOffer);
    });

    test('should be immutable', () => {
        const ref = OrdType;
        expect(() => {
            ref.Market = 10;
        }).toThrowError();
    });
});
