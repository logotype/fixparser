import { ValueCheckAction } from '../../src/fieldtypes/ValueCheckAction';

describe('ValueCheckAction', () => {
    test('should have the correct values', () => {
        expect(ValueCheckAction.DoNotCheck).toBe(0);
        expect(ValueCheckAction.Check).toBe(1);
        expect(ValueCheckAction.BestEffort).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ValueCheckAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ValueCheckAction.DoNotCheck, ValueCheckAction.Check, ValueCheckAction.BestEffort];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ValueCheckAction type', () => {
        const validate = (value: ValueCheckAction) => {
            expect(Object.values(ValueCheckAction)).toContain(value);
        };

        validate(ValueCheckAction.DoNotCheck);
        validate(ValueCheckAction.Check);
        validate(ValueCheckAction.BestEffort);
    });

    test('should be immutable', () => {
        const ref = ValueCheckAction;
        expect(() => {
            ref.DoNotCheck = 10;
        }).toThrowError();
    });
});
