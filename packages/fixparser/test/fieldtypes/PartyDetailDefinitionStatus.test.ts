import { PartyDetailDefinitionStatus } from '../../src/fieldtypes/PartyDetailDefinitionStatus';

describe('PartyDetailDefinitionStatus', () => {
    test('should have the correct values', () => {
        expect(PartyDetailDefinitionStatus.Accepted).toBe(0);
        expect(PartyDetailDefinitionStatus.AcceptedWithChanges).toBe(1);
        expect(PartyDetailDefinitionStatus.Rejected).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PartyDetailDefinitionStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PartyDetailDefinitionStatus.Accepted,
            PartyDetailDefinitionStatus.AcceptedWithChanges,
            PartyDetailDefinitionStatus.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PartyDetailDefinitionStatus type', () => {
        const validate = (value: PartyDetailDefinitionStatus) => {
            expect(Object.values(PartyDetailDefinitionStatus)).toContain(value);
        };

        validate(PartyDetailDefinitionStatus.Accepted);
        validate(PartyDetailDefinitionStatus.AcceptedWithChanges);
        validate(PartyDetailDefinitionStatus.Rejected);
    });

    test('should be immutable', () => {
        const ref = PartyDetailDefinitionStatus;
        expect(() => {
            ref.Accepted = 10;
        }).toThrowError();
    });
});
