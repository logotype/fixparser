import { PrivateQuote } from '../../src/fieldtypes/PrivateQuote';

describe('PrivateQuote', () => {
    test('should have the correct values', () => {
        expect(PrivateQuote.PrivateQuote).toBe('Y');
        expect(PrivateQuote.PublicQuote).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PrivateQuote.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PrivateQuote.PrivateQuote, PrivateQuote.PublicQuote];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PrivateQuote type', () => {
        const validate = (value: PrivateQuote) => {
            expect(Object.values(PrivateQuote)).toContain(value);
        };

        validate(PrivateQuote.PrivateQuote);
        validate(PrivateQuote.PublicQuote);
    });

    test('should be immutable', () => {
        const ref = PrivateQuote;
        expect(() => {
            ref.PrivateQuote = 10;
        }).toThrowError();
    });
});
