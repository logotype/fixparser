import { ExecTypeReason } from '../../src/fieldtypes/ExecTypeReason';

describe('ExecTypeReason', () => {
    test('should have the correct values', () => {
        expect(ExecTypeReason.OrdAddedOnRequest).toBe(1);
        expect(ExecTypeReason.OrdReplacedOnRequest).toBe(2);
        expect(ExecTypeReason.OrdCxldOnRequest).toBe(3);
        expect(ExecTypeReason.UnsolicitedOrdCxl).toBe(4);
        expect(ExecTypeReason.NonRestingOrdAddedOnRequest).toBe(5);
        expect(ExecTypeReason.OrdReplacedWithNonRestingOrdOnRequest).toBe(6);
        expect(ExecTypeReason.TriggerOrdReplacedOnRequest).toBe(7);
        expect(ExecTypeReason.SuspendedOrdReplacedOnRequest).toBe(8);
        expect(ExecTypeReason.SuspendedOrdCxldOnRequest).toBe(9);
        expect(ExecTypeReason.OrdCxlPending).toBe(10);
        expect(ExecTypeReason.PendingCxlExecuted).toBe(11);
        expect(ExecTypeReason.RestingOrdTriggered).toBe(12);
        expect(ExecTypeReason.SuspendedOrdActivated).toBe(13);
        expect(ExecTypeReason.ActiveOrdSuspended).toBe(14);
        expect(ExecTypeReason.OrdExpired).toBe(15);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecTypeReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecTypeReason.OrdAddedOnRequest,
            ExecTypeReason.OrdReplacedOnRequest,
            ExecTypeReason.OrdCxldOnRequest,
            ExecTypeReason.UnsolicitedOrdCxl,
            ExecTypeReason.NonRestingOrdAddedOnRequest,
            ExecTypeReason.OrdReplacedWithNonRestingOrdOnRequest,
            ExecTypeReason.TriggerOrdReplacedOnRequest,
            ExecTypeReason.SuspendedOrdReplacedOnRequest,
            ExecTypeReason.SuspendedOrdCxldOnRequest,
            ExecTypeReason.OrdCxlPending,
            ExecTypeReason.PendingCxlExecuted,
            ExecTypeReason.RestingOrdTriggered,
            ExecTypeReason.SuspendedOrdActivated,
            ExecTypeReason.ActiveOrdSuspended,
            ExecTypeReason.OrdExpired,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExecTypeReason type', () => {
        const validate = (value: ExecTypeReason) => {
            expect(Object.values(ExecTypeReason)).toContain(value);
        };

        validate(ExecTypeReason.OrdAddedOnRequest);
        validate(ExecTypeReason.OrdReplacedOnRequest);
        validate(ExecTypeReason.OrdCxldOnRequest);
        validate(ExecTypeReason.UnsolicitedOrdCxl);
        validate(ExecTypeReason.NonRestingOrdAddedOnRequest);
        validate(ExecTypeReason.OrdReplacedWithNonRestingOrdOnRequest);
        validate(ExecTypeReason.TriggerOrdReplacedOnRequest);
        validate(ExecTypeReason.SuspendedOrdReplacedOnRequest);
        validate(ExecTypeReason.SuspendedOrdCxldOnRequest);
        validate(ExecTypeReason.OrdCxlPending);
        validate(ExecTypeReason.PendingCxlExecuted);
        validate(ExecTypeReason.RestingOrdTriggered);
        validate(ExecTypeReason.SuspendedOrdActivated);
        validate(ExecTypeReason.ActiveOrdSuspended);
        validate(ExecTypeReason.OrdExpired);
    });

    test('should be immutable', () => {
        const ref = ExecTypeReason;
        expect(() => {
            ref.OrdAddedOnRequest = 10;
        }).toThrowError();
    });
});
