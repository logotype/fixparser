import { OrderEventReason } from '../../src/fieldtypes/OrderEventReason';

describe('OrderEventReason', () => {
    test('should have the correct values', () => {
        expect(OrderEventReason.AddOrderRequest).toBe(1);
        expect(OrderEventReason.ModifyOrderRequest).toBe(2);
        expect(OrderEventReason.DeleteOrderRequest).toBe(3);
        expect(OrderEventReason.OrderEnteredOOB).toBe(4);
        expect(OrderEventReason.OrderModifiedOOB).toBe(5);
        expect(OrderEventReason.OrderDeletedOOB).toBe(6);
        expect(OrderEventReason.OrderActivatedOrTriggered).toBe(7);
        expect(OrderEventReason.OrderExpired).toBe(8);
        expect(OrderEventReason.ReserveOrderRefreshed).toBe(9);
        expect(OrderEventReason.AwayMarketBetter).toBe(10);
        expect(OrderEventReason.CorporateAction).toBe(11);
        expect(OrderEventReason.StartOfDay).toBe(12);
        expect(OrderEventReason.EndOfDay).toBe(13);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderEventReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderEventReason.AddOrderRequest,
            OrderEventReason.ModifyOrderRequest,
            OrderEventReason.DeleteOrderRequest,
            OrderEventReason.OrderEnteredOOB,
            OrderEventReason.OrderModifiedOOB,
            OrderEventReason.OrderDeletedOOB,
            OrderEventReason.OrderActivatedOrTriggered,
            OrderEventReason.OrderExpired,
            OrderEventReason.ReserveOrderRefreshed,
            OrderEventReason.AwayMarketBetter,
            OrderEventReason.CorporateAction,
            OrderEventReason.StartOfDay,
            OrderEventReason.EndOfDay,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderEventReason type', () => {
        const validate = (value: OrderEventReason) => {
            expect(Object.values(OrderEventReason)).toContain(value);
        };

        validate(OrderEventReason.AddOrderRequest);
        validate(OrderEventReason.ModifyOrderRequest);
        validate(OrderEventReason.DeleteOrderRequest);
        validate(OrderEventReason.OrderEnteredOOB);
        validate(OrderEventReason.OrderModifiedOOB);
        validate(OrderEventReason.OrderDeletedOOB);
        validate(OrderEventReason.OrderActivatedOrTriggered);
        validate(OrderEventReason.OrderExpired);
        validate(OrderEventReason.ReserveOrderRefreshed);
        validate(OrderEventReason.AwayMarketBetter);
        validate(OrderEventReason.CorporateAction);
        validate(OrderEventReason.StartOfDay);
        validate(OrderEventReason.EndOfDay);
    });

    test('should be immutable', () => {
        const ref = OrderEventReason;
        expect(() => {
            ref.AddOrderRequest = 10;
        }).toThrowError();
    });
});
