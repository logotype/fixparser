import { PegOffsetType } from '../../src/fieldtypes/PegOffsetType';

describe('PegOffsetType', () => {
    test('should have the correct values', () => {
        expect(PegOffsetType.Price).toBe(0);
        expect(PegOffsetType.BasisPoints).toBe(1);
        expect(PegOffsetType.Ticks).toBe(2);
        expect(PegOffsetType.PriceTier).toBe(3);
        expect(PegOffsetType.Percentage).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PegOffsetType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PegOffsetType.Price,
            PegOffsetType.BasisPoints,
            PegOffsetType.Ticks,
            PegOffsetType.PriceTier,
            PegOffsetType.Percentage,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PegOffsetType type', () => {
        const validate = (value: PegOffsetType) => {
            expect(Object.values(PegOffsetType)).toContain(value);
        };

        validate(PegOffsetType.Price);
        validate(PegOffsetType.BasisPoints);
        validate(PegOffsetType.Ticks);
        validate(PegOffsetType.PriceTier);
        validate(PegOffsetType.Percentage);
    });

    test('should be immutable', () => {
        const ref = PegOffsetType;
        expect(() => {
            ref.Price = 10;
        }).toThrowError();
    });
});
