import { ExtraordinaryEventAdjustmentMethod } from '../../src/fieldtypes/ExtraordinaryEventAdjustmentMethod';

describe('ExtraordinaryEventAdjustmentMethod', () => {
    test('should have the correct values', () => {
        expect(ExtraordinaryEventAdjustmentMethod.CalculationAgent).toBe(0);
        expect(ExtraordinaryEventAdjustmentMethod.OptionsExchange).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExtraordinaryEventAdjustmentMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExtraordinaryEventAdjustmentMethod.CalculationAgent,
            ExtraordinaryEventAdjustmentMethod.OptionsExchange,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ExtraordinaryEventAdjustmentMethod type', () => {
        const validate = (value: ExtraordinaryEventAdjustmentMethod) => {
            expect(Object.values(ExtraordinaryEventAdjustmentMethod)).toContain(value);
        };

        validate(ExtraordinaryEventAdjustmentMethod.CalculationAgent);
        validate(ExtraordinaryEventAdjustmentMethod.OptionsExchange);
    });

    test('should be immutable', () => {
        const ref = ExtraordinaryEventAdjustmentMethod;
        expect(() => {
            ref.CalculationAgent = 10;
        }).toThrowError();
    });
});
