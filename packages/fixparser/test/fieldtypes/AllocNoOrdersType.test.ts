import { AllocNoOrdersType } from '../../src/fieldtypes/AllocNoOrdersType';

describe('AllocNoOrdersType', () => {
    test('should have the correct values', () => {
        expect(AllocNoOrdersType.NotSpecified).toBe(0);
        expect(AllocNoOrdersType.ExplicitListProvided).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocNoOrdersType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [AllocNoOrdersType.NotSpecified, AllocNoOrdersType.ExplicitListProvided];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocNoOrdersType type', () => {
        const validate = (value: AllocNoOrdersType) => {
            expect(Object.values(AllocNoOrdersType)).toContain(value);
        };

        validate(AllocNoOrdersType.NotSpecified);
        validate(AllocNoOrdersType.ExplicitListProvided);
    });

    test('should be immutable', () => {
        const ref = AllocNoOrdersType;
        expect(() => {
            ref.NotSpecified = 10;
        }).toThrowError();
    });
});
