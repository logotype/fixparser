import { TestSuiteStatus } from '../../src/fieldtypes/TestSuiteStatus';

describe('TestSuiteStatus', () => {
    test('should have the correct values', () => {
        expect(TestSuiteStatus.Undefined).toBe(0);
        expect(TestSuiteStatus.Pass).toBe(1);
        expect(TestSuiteStatus.Fail).toBe(2);
        expect(TestSuiteStatus.Warning).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestSuiteStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TestSuiteStatus.Undefined,
            TestSuiteStatus.Pass,
            TestSuiteStatus.Fail,
            TestSuiteStatus.Warning,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TestSuiteStatus type', () => {
        const validate = (value: TestSuiteStatus) => {
            expect(Object.values(TestSuiteStatus)).toContain(value);
        };

        validate(TestSuiteStatus.Undefined);
        validate(TestSuiteStatus.Pass);
        validate(TestSuiteStatus.Fail);
        validate(TestSuiteStatus.Warning);
    });

    test('should be immutable', () => {
        const ref = TestSuiteStatus;
        expect(() => {
            ref.Undefined = 10;
        }).toThrowError();
    });
});
