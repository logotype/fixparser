import { MarketSegmentType } from '../../src/fieldtypes/MarketSegmentType';

describe('MarketSegmentType', () => {
    test('should have the correct values', () => {
        expect(MarketSegmentType.Pool).toBe(1);
        expect(MarketSegmentType.Retail).toBe(2);
        expect(MarketSegmentType.Wholesale).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketSegmentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MarketSegmentType.Pool, MarketSegmentType.Retail, MarketSegmentType.Wholesale];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketSegmentType type', () => {
        const validate = (value: MarketSegmentType) => {
            expect(Object.values(MarketSegmentType)).toContain(value);
        };

        validate(MarketSegmentType.Pool);
        validate(MarketSegmentType.Retail);
        validate(MarketSegmentType.Wholesale);
    });

    test('should be immutable', () => {
        const ref = MarketSegmentType;
        expect(() => {
            ref.Pool = 10;
        }).toThrowError();
    });
});
