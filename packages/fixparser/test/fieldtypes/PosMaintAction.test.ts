import { PosMaintAction } from '../../src/fieldtypes/PosMaintAction';

describe('PosMaintAction', () => {
    test('should have the correct values', () => {
        expect(PosMaintAction.New).toBe(1);
        expect(PosMaintAction.Replace).toBe(2);
        expect(PosMaintAction.Cancel).toBe(3);
        expect(PosMaintAction.Reverse).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosMaintAction.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosMaintAction.New,
            PosMaintAction.Replace,
            PosMaintAction.Cancel,
            PosMaintAction.Reverse,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosMaintAction type', () => {
        const validate = (value: PosMaintAction) => {
            expect(Object.values(PosMaintAction)).toContain(value);
        };

        validate(PosMaintAction.New);
        validate(PosMaintAction.Replace);
        validate(PosMaintAction.Cancel);
        validate(PosMaintAction.Reverse);
    });

    test('should be immutable', () => {
        const ref = PosMaintAction;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
