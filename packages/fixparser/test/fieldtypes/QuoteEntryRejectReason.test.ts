import { QuoteEntryRejectReason } from '../../src/fieldtypes/QuoteEntryRejectReason';

describe('QuoteEntryRejectReason', () => {
    test('should have the correct values', () => {
        expect(QuoteEntryRejectReason.UnknownSymbol).toBe(1);
        expect(QuoteEntryRejectReason.Exchange).toBe(2);
        expect(QuoteEntryRejectReason.QuoteExceedsLimit).toBe(3);
        expect(QuoteEntryRejectReason.TooLateToEnter).toBe(4);
        expect(QuoteEntryRejectReason.UnknownQuote).toBe(5);
        expect(QuoteEntryRejectReason.DuplicateQuote).toBe(6);
        expect(QuoteEntryRejectReason.InvalidBidAskSpread).toBe(7);
        expect(QuoteEntryRejectReason.InvalidPrice).toBe(8);
        expect(QuoteEntryRejectReason.NotAuthorizedToQuoteSecurity).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteEntryRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteEntryRejectReason.UnknownSymbol,
            QuoteEntryRejectReason.Exchange,
            QuoteEntryRejectReason.QuoteExceedsLimit,
            QuoteEntryRejectReason.TooLateToEnter,
            QuoteEntryRejectReason.UnknownQuote,
            QuoteEntryRejectReason.DuplicateQuote,
            QuoteEntryRejectReason.InvalidBidAskSpread,
            QuoteEntryRejectReason.InvalidPrice,
            QuoteEntryRejectReason.NotAuthorizedToQuoteSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteEntryRejectReason type', () => {
        const validate = (value: QuoteEntryRejectReason) => {
            expect(Object.values(QuoteEntryRejectReason)).toContain(value);
        };

        validate(QuoteEntryRejectReason.UnknownSymbol);
        validate(QuoteEntryRejectReason.Exchange);
        validate(QuoteEntryRejectReason.QuoteExceedsLimit);
        validate(QuoteEntryRejectReason.TooLateToEnter);
        validate(QuoteEntryRejectReason.UnknownQuote);
        validate(QuoteEntryRejectReason.DuplicateQuote);
        validate(QuoteEntryRejectReason.InvalidBidAskSpread);
        validate(QuoteEntryRejectReason.InvalidPrice);
        validate(QuoteEntryRejectReason.NotAuthorizedToQuoteSecurity);
    });

    test('should be immutable', () => {
        const ref = QuoteEntryRejectReason;
        expect(() => {
            ref.UnknownSymbol = 10;
        }).toThrowError();
    });
});
