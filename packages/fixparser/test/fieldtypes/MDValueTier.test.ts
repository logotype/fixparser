import { MDValueTier } from '../../src/fieldtypes/MDValueTier';

describe('MDValueTier', () => {
    test('should have the correct values', () => {
        expect(MDValueTier.Range1).toBe(1);
        expect(MDValueTier.Range2).toBe(2);
        expect(MDValueTier.Range3).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MDValueTier.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [MDValueTier.Range1, MDValueTier.Range2, MDValueTier.Range3];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MDValueTier type', () => {
        const validate = (value: MDValueTier) => {
            expect(Object.values(MDValueTier)).toContain(value);
        };

        validate(MDValueTier.Range1);
        validate(MDValueTier.Range2);
        validate(MDValueTier.Range3);
    });

    test('should be immutable', () => {
        const ref = MDValueTier;
        expect(() => {
            ref.Range1 = 10;
        }).toThrowError();
    });
});
