import { Seniority } from '../../src/fieldtypes/Seniority';

describe('Seniority', () => {
    test('should have the correct values', () => {
        expect(Seniority.SeniorSecured).toBe('SD');
        expect(Seniority.Senior).toBe('SR');
        expect(Seniority.Subordinated).toBe('SB');
        expect(Seniority.Junior).toBe('JR');
        expect(Seniority.Mezzanine).toBe('MZ');
        expect(Seniority.SeniorNonPreferred).toBe('SN');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Seniority.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Seniority.SeniorSecured,
            Seniority.Senior,
            Seniority.Subordinated,
            Seniority.Junior,
            Seniority.Mezzanine,
            Seniority.SeniorNonPreferred,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Seniority type', () => {
        const validate = (value: Seniority) => {
            expect(Object.values(Seniority)).toContain(value);
        };

        validate(Seniority.SeniorSecured);
        validate(Seniority.Senior);
        validate(Seniority.Subordinated);
        validate(Seniority.Junior);
        validate(Seniority.Mezzanine);
        validate(Seniority.SeniorNonPreferred);
    });

    test('should be immutable', () => {
        const ref = Seniority;
        expect(() => {
            ref.SeniorSecured = 10;
        }).toThrowError();
    });
});
