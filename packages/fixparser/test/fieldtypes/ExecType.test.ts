import { ExecType } from '../../src/fieldtypes/ExecType';

describe('ExecType', () => {
    test('should have the correct values', () => {
        expect(ExecType.New).toBe('0');
        expect(ExecType.DoneForDay).toBe('3');
        expect(ExecType.Canceled).toBe('4');
        expect(ExecType.Replaced).toBe('5');
        expect(ExecType.PendingCancel).toBe('6');
        expect(ExecType.Stopped).toBe('7');
        expect(ExecType.Rejected).toBe('8');
        expect(ExecType.Suspended).toBe('9');
        expect(ExecType.PendingNew).toBe('A');
        expect(ExecType.Calculated).toBe('B');
        expect(ExecType.Expired).toBe('C');
        expect(ExecType.Restated).toBe('D');
        expect(ExecType.PendingReplace).toBe('E');
        expect(ExecType.Trade).toBe('F');
        expect(ExecType.TradeCorrect).toBe('G');
        expect(ExecType.TradeCancel).toBe('H');
        expect(ExecType.OrderStatus).toBe('I');
        expect(ExecType.TradeInAClearingHold).toBe('J');
        expect(ExecType.TradeHasBeenReleasedToClearing).toBe('K');
        expect(ExecType.TriggeredOrActivatedBySystem).toBe('L');
        expect(ExecType.Locked).toBe('M');
        expect(ExecType.Released).toBe('N');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ExecType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ExecType.New,
            ExecType.DoneForDay,
            ExecType.Canceled,
            ExecType.Replaced,
            ExecType.PendingCancel,
            ExecType.Stopped,
            ExecType.Rejected,
            ExecType.Suspended,
            ExecType.PendingNew,
            ExecType.Calculated,
            ExecType.Expired,
            ExecType.Restated,
            ExecType.PendingReplace,
            ExecType.Trade,
            ExecType.TradeCorrect,
            ExecType.TradeCancel,
            ExecType.OrderStatus,
            ExecType.TradeInAClearingHold,
            ExecType.TradeHasBeenReleasedToClearing,
            ExecType.TriggeredOrActivatedBySystem,
            ExecType.Locked,
            ExecType.Released,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ExecType type', () => {
        const validate = (value: ExecType) => {
            expect(Object.values(ExecType)).toContain(value);
        };

        validate(ExecType.New);
        validate(ExecType.DoneForDay);
        validate(ExecType.Canceled);
        validate(ExecType.Replaced);
        validate(ExecType.PendingCancel);
        validate(ExecType.Stopped);
        validate(ExecType.Rejected);
        validate(ExecType.Suspended);
        validate(ExecType.PendingNew);
        validate(ExecType.Calculated);
        validate(ExecType.Expired);
        validate(ExecType.Restated);
        validate(ExecType.PendingReplace);
        validate(ExecType.Trade);
        validate(ExecType.TradeCorrect);
        validate(ExecType.TradeCancel);
        validate(ExecType.OrderStatus);
        validate(ExecType.TradeInAClearingHold);
        validate(ExecType.TradeHasBeenReleasedToClearing);
        validate(ExecType.TriggeredOrActivatedBySystem);
        validate(ExecType.Locked);
        validate(ExecType.Released);
    });

    test('should be immutable', () => {
        const ref = ExecType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
