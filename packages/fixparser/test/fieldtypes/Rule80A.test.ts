import { Rule80A } from '../../src/fieldtypes/Rule80A';

describe('Rule80A', () => {
    test('should have the correct values', () => {
        expect(Rule80A.AgencySingleOrder).toBe('A');
        expect(Rule80A.ShortExemptTransactionAType).toBe('B');
        expect(Rule80A.ProprietaryNonAlgo).toBe('C');
        expect(Rule80A.ProgramOrderMember).toBe('D');
        expect(Rule80A.ShortExemptTransactionForPrincipal).toBe('E');
        expect(Rule80A.ShortExemptTransactionWType).toBe('F');
        expect(Rule80A.ShortExemptTransactionIType).toBe('H');
        expect(Rule80A.IndividualInvestor).toBe('I');
        expect(Rule80A.ProprietaryAlgo).toBe('J');
        expect(Rule80A.AgencyAlgo).toBe('K');
        expect(Rule80A.ShortExemptTransactionMemberAffliated).toBe('L');
        expect(Rule80A.ProgramOrderOtherMember).toBe('M');
        expect(Rule80A.AgentForOtherMember).toBe('N');
        expect(Rule80A.ProprietaryTransactionAffiliated).toBe('O');
        expect(Rule80A.Principal).toBe('P');
        expect(Rule80A.TransactionNonMember).toBe('R');
        expect(Rule80A.SpecialistTrades).toBe('S');
        expect(Rule80A.TransactionUnaffiliatedMember).toBe('T');
        expect(Rule80A.AgencyIndexArb).toBe('U');
        expect(Rule80A.AllOtherOrdersAsAgentForOtherMember).toBe('W');
        expect(Rule80A.ShortExemptTransactionMemberNotAffliated).toBe('X');
        expect(Rule80A.AgencyNonAlgo).toBe('Y');
        expect(Rule80A.ShortExemptTransactionNonMember).toBe('Z');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            Rule80A.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            Rule80A.AgencySingleOrder,
            Rule80A.ShortExemptTransactionAType,
            Rule80A.ProprietaryNonAlgo,
            Rule80A.ProgramOrderMember,
            Rule80A.ShortExemptTransactionForPrincipal,
            Rule80A.ShortExemptTransactionWType,
            Rule80A.ShortExemptTransactionIType,
            Rule80A.IndividualInvestor,
            Rule80A.ProprietaryAlgo,
            Rule80A.AgencyAlgo,
            Rule80A.ShortExemptTransactionMemberAffliated,
            Rule80A.ProgramOrderOtherMember,
            Rule80A.AgentForOtherMember,
            Rule80A.ProprietaryTransactionAffiliated,
            Rule80A.Principal,
            Rule80A.TransactionNonMember,
            Rule80A.SpecialistTrades,
            Rule80A.TransactionUnaffiliatedMember,
            Rule80A.AgencyIndexArb,
            Rule80A.AllOtherOrdersAsAgentForOtherMember,
            Rule80A.ShortExemptTransactionMemberNotAffliated,
            Rule80A.AgencyNonAlgo,
            Rule80A.ShortExemptTransactionNonMember,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Rule80A type', () => {
        const validate = (value: Rule80A) => {
            expect(Object.values(Rule80A)).toContain(value);
        };

        validate(Rule80A.AgencySingleOrder);
        validate(Rule80A.ShortExemptTransactionAType);
        validate(Rule80A.ProprietaryNonAlgo);
        validate(Rule80A.ProgramOrderMember);
        validate(Rule80A.ShortExemptTransactionForPrincipal);
        validate(Rule80A.ShortExemptTransactionWType);
        validate(Rule80A.ShortExemptTransactionIType);
        validate(Rule80A.IndividualInvestor);
        validate(Rule80A.ProprietaryAlgo);
        validate(Rule80A.AgencyAlgo);
        validate(Rule80A.ShortExemptTransactionMemberAffliated);
        validate(Rule80A.ProgramOrderOtherMember);
        validate(Rule80A.AgentForOtherMember);
        validate(Rule80A.ProprietaryTransactionAffiliated);
        validate(Rule80A.Principal);
        validate(Rule80A.TransactionNonMember);
        validate(Rule80A.SpecialistTrades);
        validate(Rule80A.TransactionUnaffiliatedMember);
        validate(Rule80A.AgencyIndexArb);
        validate(Rule80A.AllOtherOrdersAsAgentForOtherMember);
        validate(Rule80A.ShortExemptTransactionMemberNotAffliated);
        validate(Rule80A.AgencyNonAlgo);
        validate(Rule80A.ShortExemptTransactionNonMember);
    });

    test('should be immutable', () => {
        const ref = Rule80A;
        expect(() => {
            ref.AgencySingleOrder = 10;
        }).toThrowError();
    });
});
