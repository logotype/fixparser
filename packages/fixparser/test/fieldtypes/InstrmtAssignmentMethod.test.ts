import { InstrmtAssignmentMethod } from '../../src/fieldtypes/InstrmtAssignmentMethod';

describe('InstrmtAssignmentMethod', () => {
    test('should have the correct values', () => {
        expect(InstrmtAssignmentMethod.ProRata).toBe('P');
        expect(InstrmtAssignmentMethod.Random).toBe('R');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            InstrmtAssignmentMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [InstrmtAssignmentMethod.ProRata, InstrmtAssignmentMethod.Random];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for InstrmtAssignmentMethod type', () => {
        const validate = (value: InstrmtAssignmentMethod) => {
            expect(Object.values(InstrmtAssignmentMethod)).toContain(value);
        };

        validate(InstrmtAssignmentMethod.ProRata);
        validate(InstrmtAssignmentMethod.Random);
    });

    test('should be immutable', () => {
        const ref = InstrmtAssignmentMethod;
        expect(() => {
            ref.ProRata = 10;
        }).toThrowError();
    });
});
