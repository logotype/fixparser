import { SecurityRejectReason } from '../../src/fieldtypes/SecurityRejectReason';

describe('SecurityRejectReason', () => {
    test('should have the correct values', () => {
        expect(SecurityRejectReason.InvalidInstrumentRequested).toBe(1);
        expect(SecurityRejectReason.InstrumentAlreadyExists).toBe(2);
        expect(SecurityRejectReason.RequestTypeNotSupported).toBe(3);
        expect(SecurityRejectReason.SystemUnavailableForInstrumentCreation).toBe(4);
        expect(SecurityRejectReason.IneligibleInstrumentGroup).toBe(5);
        expect(SecurityRejectReason.InstrumentIDUnavailable).toBe(6);
        expect(SecurityRejectReason.InvalidOrMissingDataOnOptionLeg).toBe(7);
        expect(SecurityRejectReason.InvalidOrMissingDataOnFutureLeg).toBe(8);
        expect(SecurityRejectReason.InvalidOrMissingDataOnFXLeg).toBe(10);
        expect(SecurityRejectReason.InvalidLegPriceSpecified).toBe(11);
        expect(SecurityRejectReason.InvalidInstrumentStructureSpecified).toBe(12);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityRejectReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityRejectReason.InvalidInstrumentRequested,
            SecurityRejectReason.InstrumentAlreadyExists,
            SecurityRejectReason.RequestTypeNotSupported,
            SecurityRejectReason.SystemUnavailableForInstrumentCreation,
            SecurityRejectReason.IneligibleInstrumentGroup,
            SecurityRejectReason.InstrumentIDUnavailable,
            SecurityRejectReason.InvalidOrMissingDataOnOptionLeg,
            SecurityRejectReason.InvalidOrMissingDataOnFutureLeg,
            SecurityRejectReason.InvalidOrMissingDataOnFXLeg,
            SecurityRejectReason.InvalidLegPriceSpecified,
            SecurityRejectReason.InvalidInstrumentStructureSpecified,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityRejectReason type', () => {
        const validate = (value: SecurityRejectReason) => {
            expect(Object.values(SecurityRejectReason)).toContain(value);
        };

        validate(SecurityRejectReason.InvalidInstrumentRequested);
        validate(SecurityRejectReason.InstrumentAlreadyExists);
        validate(SecurityRejectReason.RequestTypeNotSupported);
        validate(SecurityRejectReason.SystemUnavailableForInstrumentCreation);
        validate(SecurityRejectReason.IneligibleInstrumentGroup);
        validate(SecurityRejectReason.InstrumentIDUnavailable);
        validate(SecurityRejectReason.InvalidOrMissingDataOnOptionLeg);
        validate(SecurityRejectReason.InvalidOrMissingDataOnFutureLeg);
        validate(SecurityRejectReason.InvalidOrMissingDataOnFXLeg);
        validate(SecurityRejectReason.InvalidLegPriceSpecified);
        validate(SecurityRejectReason.InvalidInstrumentStructureSpecified);
    });

    test('should be immutable', () => {
        const ref = SecurityRejectReason;
        expect(() => {
            ref.InvalidInstrumentRequested = 10;
        }).toThrowError();
    });
});
