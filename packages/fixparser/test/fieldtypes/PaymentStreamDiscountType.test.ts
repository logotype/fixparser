import { PaymentStreamDiscountType } from '../../src/fieldtypes/PaymentStreamDiscountType';

describe('PaymentStreamDiscountType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamDiscountType.Standard).toBe(0);
        expect(PaymentStreamDiscountType.FRA).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamDiscountType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PaymentStreamDiscountType.Standard, PaymentStreamDiscountType.FRA];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamDiscountType type', () => {
        const validate = (value: PaymentStreamDiscountType) => {
            expect(Object.values(PaymentStreamDiscountType)).toContain(value);
        };

        validate(PaymentStreamDiscountType.Standard);
        validate(PaymentStreamDiscountType.FRA);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamDiscountType;
        expect(() => {
            ref.Standard = 10;
        }).toThrowError();
    });
});
