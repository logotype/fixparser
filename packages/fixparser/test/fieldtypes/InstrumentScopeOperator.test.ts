import { InstrumentScopeOperator } from '../../src/fieldtypes/InstrumentScopeOperator';

describe('InstrumentScopeOperator', () => {
    test('should have the correct values', () => {
        expect(InstrumentScopeOperator.Include).toBe(1);
        expect(InstrumentScopeOperator.Exclude).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            InstrumentScopeOperator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [InstrumentScopeOperator.Include, InstrumentScopeOperator.Exclude];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for InstrumentScopeOperator type', () => {
        const validate = (value: InstrumentScopeOperator) => {
            expect(Object.values(InstrumentScopeOperator)).toContain(value);
        };

        validate(InstrumentScopeOperator.Include);
        validate(InstrumentScopeOperator.Exclude);
    });

    test('should be immutable', () => {
        const ref = InstrumentScopeOperator;
        expect(() => {
            ref.Include = 10;
        }).toThrowError();
    });
});
