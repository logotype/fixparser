import { PaymentStreamPricingDayDistribution } from '../../src/fieldtypes/PaymentStreamPricingDayDistribution';

describe('PaymentStreamPricingDayDistribution', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamPricingDayDistribution.All).toBe(0);
        expect(PaymentStreamPricingDayDistribution.First).toBe(1);
        expect(PaymentStreamPricingDayDistribution.Last).toBe(2);
        expect(PaymentStreamPricingDayDistribution.Penultimate).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamPricingDayDistribution.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamPricingDayDistribution.All,
            PaymentStreamPricingDayDistribution.First,
            PaymentStreamPricingDayDistribution.Last,
            PaymentStreamPricingDayDistribution.Penultimate,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamPricingDayDistribution type', () => {
        const validate = (value: PaymentStreamPricingDayDistribution) => {
            expect(Object.values(PaymentStreamPricingDayDistribution)).toContain(value);
        };

        validate(PaymentStreamPricingDayDistribution.All);
        validate(PaymentStreamPricingDayDistribution.First);
        validate(PaymentStreamPricingDayDistribution.Last);
        validate(PaymentStreamPricingDayDistribution.Penultimate);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamPricingDayDistribution;
        expect(() => {
            ref.All = 10;
        }).toThrowError();
    });
});
