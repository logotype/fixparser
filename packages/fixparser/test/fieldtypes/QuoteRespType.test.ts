import { QuoteRespType } from '../../src/fieldtypes/QuoteRespType';

describe('QuoteRespType', () => {
    test('should have the correct values', () => {
        expect(QuoteRespType.Hit).toBe(1);
        expect(QuoteRespType.Counter).toBe(2);
        expect(QuoteRespType.Expired).toBe(3);
        expect(QuoteRespType.Cover).toBe(4);
        expect(QuoteRespType.DoneAway).toBe(5);
        expect(QuoteRespType.Pass).toBe(6);
        expect(QuoteRespType.EndTrade).toBe(7);
        expect(QuoteRespType.TimedOut).toBe(8);
        expect(QuoteRespType.Tied).toBe(9);
        expect(QuoteRespType.TiedCover).toBe(10);
        expect(QuoteRespType.Accept).toBe(11);
        expect(QuoteRespType.TerminateContract).toBe(12);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            QuoteRespType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            QuoteRespType.Hit,
            QuoteRespType.Counter,
            QuoteRespType.Expired,
            QuoteRespType.Cover,
            QuoteRespType.DoneAway,
            QuoteRespType.Pass,
            QuoteRespType.EndTrade,
            QuoteRespType.TimedOut,
            QuoteRespType.Tied,
            QuoteRespType.TiedCover,
            QuoteRespType.Accept,
            QuoteRespType.TerminateContract,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for QuoteRespType type', () => {
        const validate = (value: QuoteRespType) => {
            expect(Object.values(QuoteRespType)).toContain(value);
        };

        validate(QuoteRespType.Hit);
        validate(QuoteRespType.Counter);
        validate(QuoteRespType.Expired);
        validate(QuoteRespType.Cover);
        validate(QuoteRespType.DoneAway);
        validate(QuoteRespType.Pass);
        validate(QuoteRespType.EndTrade);
        validate(QuoteRespType.TimedOut);
        validate(QuoteRespType.Tied);
        validate(QuoteRespType.TiedCover);
        validate(QuoteRespType.Accept);
        validate(QuoteRespType.TerminateContract);
    });

    test('should be immutable', () => {
        const ref = QuoteRespType;
        expect(() => {
            ref.Hit = 10;
        }).toThrowError();
    });
});
