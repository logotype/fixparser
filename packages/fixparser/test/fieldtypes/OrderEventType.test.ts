import { OrderEventType } from '../../src/fieldtypes/OrderEventType';

describe('OrderEventType', () => {
    test('should have the correct values', () => {
        expect(OrderEventType.Added).toBe(1);
        expect(OrderEventType.Modified).toBe(2);
        expect(OrderEventType.Deleted).toBe(3);
        expect(OrderEventType.PartiallyFilled).toBe(4);
        expect(OrderEventType.Filled).toBe(5);
        expect(OrderEventType.Suspended).toBe(6);
        expect(OrderEventType.Released).toBe(7);
        expect(OrderEventType.Restated).toBe(8);
        expect(OrderEventType.Locked).toBe(9);
        expect(OrderEventType.Triggered).toBe(10);
        expect(OrderEventType.Activated).toBe(11);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderEventType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderEventType.Added,
            OrderEventType.Modified,
            OrderEventType.Deleted,
            OrderEventType.PartiallyFilled,
            OrderEventType.Filled,
            OrderEventType.Suspended,
            OrderEventType.Released,
            OrderEventType.Restated,
            OrderEventType.Locked,
            OrderEventType.Triggered,
            OrderEventType.Activated,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderEventType type', () => {
        const validate = (value: OrderEventType) => {
            expect(Object.values(OrderEventType)).toContain(value);
        };

        validate(OrderEventType.Added);
        validate(OrderEventType.Modified);
        validate(OrderEventType.Deleted);
        validate(OrderEventType.PartiallyFilled);
        validate(OrderEventType.Filled);
        validate(OrderEventType.Suspended);
        validate(OrderEventType.Released);
        validate(OrderEventType.Restated);
        validate(OrderEventType.Locked);
        validate(OrderEventType.Triggered);
        validate(OrderEventType.Activated);
    });

    test('should be immutable', () => {
        const ref = OrderEventType;
        expect(() => {
            ref.Added = 10;
        }).toThrowError();
    });
});
