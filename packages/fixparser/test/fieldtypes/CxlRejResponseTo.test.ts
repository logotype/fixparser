import { CxlRejResponseTo } from '../../src/fieldtypes/CxlRejResponseTo';

describe('CxlRejResponseTo', () => {
    test('should have the correct values', () => {
        expect(CxlRejResponseTo.OrderCancelRequest).toBe('1');
        expect(CxlRejResponseTo.OrderCancelReplaceRequest).toBe('2');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            CxlRejResponseTo.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [CxlRejResponseTo.OrderCancelRequest, CxlRejResponseTo.OrderCancelReplaceRequest];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for CxlRejResponseTo type', () => {
        const validate = (value: CxlRejResponseTo) => {
            expect(Object.values(CxlRejResponseTo)).toContain(value);
        };

        validate(CxlRejResponseTo.OrderCancelRequest);
        validate(CxlRejResponseTo.OrderCancelReplaceRequest);
    });

    test('should be immutable', () => {
        const ref = CxlRejResponseTo;
        expect(() => {
            ref.OrderCancelRequest = 10;
        }).toThrowError();
    });
});
