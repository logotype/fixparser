import { TimeUnit } from '../../src/fieldtypes/TimeUnit';

describe('TimeUnit', () => {
    test('should have the correct values', () => {
        expect(TimeUnit.Hour).toBe('H');
        expect(TimeUnit.Minute).toBe('Min');
        expect(TimeUnit.Second).toBe('S');
        expect(TimeUnit.Day).toBe('D');
        expect(TimeUnit.Week).toBe('Wk');
        expect(TimeUnit.Month).toBe('Mo');
        expect(TimeUnit.Year).toBe('Yr');
        expect(TimeUnit.Quarter).toBe('Q');
        expect(TimeUnit.EndOfMonth).toBe('EOM');
        expect(TimeUnit.Flexible).toBe('F');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TimeUnit.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TimeUnit.Hour,
            TimeUnit.Minute,
            TimeUnit.Second,
            TimeUnit.Day,
            TimeUnit.Week,
            TimeUnit.Month,
            TimeUnit.Year,
            TimeUnit.Quarter,
            TimeUnit.EndOfMonth,
            TimeUnit.Flexible,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TimeUnit type', () => {
        const validate = (value: TimeUnit) => {
            expect(Object.values(TimeUnit)).toContain(value);
        };

        validate(TimeUnit.Hour);
        validate(TimeUnit.Minute);
        validate(TimeUnit.Second);
        validate(TimeUnit.Day);
        validate(TimeUnit.Week);
        validate(TimeUnit.Month);
        validate(TimeUnit.Year);
        validate(TimeUnit.Quarter);
        validate(TimeUnit.EndOfMonth);
        validate(TimeUnit.Flexible);
    });

    test('should be immutable', () => {
        const ref = TimeUnit;
        expect(() => {
            ref.Hour = 10;
        }).toThrowError();
    });
});
