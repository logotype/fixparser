import { RelatedOrderIDSource } from '../../src/fieldtypes/RelatedOrderIDSource';

describe('RelatedOrderIDSource', () => {
    test('should have the correct values', () => {
        expect(RelatedOrderIDSource.NonFIXSource).toBe(0);
        expect(RelatedOrderIDSource.SystemOrderIdentifier).toBe(1);
        expect(RelatedOrderIDSource.ClientOrderIdentifier).toBe(2);
        expect(RelatedOrderIDSource.SecondaryOrderIdentifier).toBe(3);
        expect(RelatedOrderIDSource.SecondaryClientOrderIdentifier).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RelatedOrderIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RelatedOrderIDSource.NonFIXSource,
            RelatedOrderIDSource.SystemOrderIdentifier,
            RelatedOrderIDSource.ClientOrderIdentifier,
            RelatedOrderIDSource.SecondaryOrderIdentifier,
            RelatedOrderIDSource.SecondaryClientOrderIdentifier,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RelatedOrderIDSource type', () => {
        const validate = (value: RelatedOrderIDSource) => {
            expect(Object.values(RelatedOrderIDSource)).toContain(value);
        };

        validate(RelatedOrderIDSource.NonFIXSource);
        validate(RelatedOrderIDSource.SystemOrderIdentifier);
        validate(RelatedOrderIDSource.ClientOrderIdentifier);
        validate(RelatedOrderIDSource.SecondaryOrderIdentifier);
        validate(RelatedOrderIDSource.SecondaryClientOrderIdentifier);
    });

    test('should be immutable', () => {
        const ref = RelatedOrderIDSource;
        expect(() => {
            ref.NonFIXSource = 10;
        }).toThrowError();
    });
});
