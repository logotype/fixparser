import { SecurityListRequestType } from '../../src/fieldtypes/SecurityListRequestType';

describe('SecurityListRequestType', () => {
    test('should have the correct values', () => {
        expect(SecurityListRequestType.Symbol).toBe(0);
        expect(SecurityListRequestType.SecurityTypeAnd).toBe(1);
        expect(SecurityListRequestType.Product).toBe(2);
        expect(SecurityListRequestType.TradingSessionID).toBe(3);
        expect(SecurityListRequestType.AllSecurities).toBe(4);
        expect(SecurityListRequestType.MarketIDOrMarketID).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityListRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityListRequestType.Symbol,
            SecurityListRequestType.SecurityTypeAnd,
            SecurityListRequestType.Product,
            SecurityListRequestType.TradingSessionID,
            SecurityListRequestType.AllSecurities,
            SecurityListRequestType.MarketIDOrMarketID,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityListRequestType type', () => {
        const validate = (value: SecurityListRequestType) => {
            expect(Object.values(SecurityListRequestType)).toContain(value);
        };

        validate(SecurityListRequestType.Symbol);
        validate(SecurityListRequestType.SecurityTypeAnd);
        validate(SecurityListRequestType.Product);
        validate(SecurityListRequestType.TradingSessionID);
        validate(SecurityListRequestType.AllSecurities);
        validate(SecurityListRequestType.MarketIDOrMarketID);
    });

    test('should be immutable', () => {
        const ref = SecurityListRequestType;
        expect(() => {
            ref.Symbol = 10;
        }).toThrowError();
    });
});
