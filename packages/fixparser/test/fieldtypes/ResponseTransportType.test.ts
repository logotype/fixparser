import { ResponseTransportType } from '../../src/fieldtypes/ResponseTransportType';

describe('ResponseTransportType', () => {
    test('should have the correct values', () => {
        expect(ResponseTransportType.Inband).toBe(0);
        expect(ResponseTransportType.OutOfBand).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ResponseTransportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [ResponseTransportType.Inband, ResponseTransportType.OutOfBand];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ResponseTransportType type', () => {
        const validate = (value: ResponseTransportType) => {
            expect(Object.values(ResponseTransportType)).toContain(value);
        };

        validate(ResponseTransportType.Inband);
        validate(ResponseTransportType.OutOfBand);
    });

    test('should be immutable', () => {
        const ref = ResponseTransportType;
        expect(() => {
            ref.Inband = 10;
        }).toThrowError();
    });
});
