import { ListExecInstType } from '../../src/fieldtypes/ListExecInstType';

describe('ListExecInstType', () => {
    test('should have the correct values', () => {
        expect(ListExecInstType.Immediate).toBe('1');
        expect(ListExecInstType.WaitForInstruction).toBe('2');
        expect(ListExecInstType.SellDriven).toBe('3');
        expect(ListExecInstType.BuyDrivenCashTopUp).toBe('4');
        expect(ListExecInstType.BuyDrivenCashWithdraw).toBe('5');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ListExecInstType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ListExecInstType.Immediate,
            ListExecInstType.WaitForInstruction,
            ListExecInstType.SellDriven,
            ListExecInstType.BuyDrivenCashTopUp,
            ListExecInstType.BuyDrivenCashWithdraw,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for ListExecInstType type', () => {
        const validate = (value: ListExecInstType) => {
            expect(Object.values(ListExecInstType)).toContain(value);
        };

        validate(ListExecInstType.Immediate);
        validate(ListExecInstType.WaitForInstruction);
        validate(ListExecInstType.SellDriven);
        validate(ListExecInstType.BuyDrivenCashTopUp);
        validate(ListExecInstType.BuyDrivenCashWithdraw);
    });

    test('should be immutable', () => {
        const ref = ListExecInstType;
        expect(() => {
            ref.Immediate = 10;
        }).toThrowError();
    });
});
