import { SecurityTradingStatus } from '../../src/fieldtypes/SecurityTradingStatus';

describe('SecurityTradingStatus', () => {
    test('should have the correct values', () => {
        expect(SecurityTradingStatus.OpeningDelay).toBe(1);
        expect(SecurityTradingStatus.TradingHalt).toBe(2);
        expect(SecurityTradingStatus.Resume).toBe(3);
        expect(SecurityTradingStatus.NoOpen).toBe(4);
        expect(SecurityTradingStatus.PriceIndication).toBe(5);
        expect(SecurityTradingStatus.TradingRangeIndication).toBe(6);
        expect(SecurityTradingStatus.MarketImbalanceBuy).toBe(7);
        expect(SecurityTradingStatus.MarketImbalanceSell).toBe(8);
        expect(SecurityTradingStatus.MarketOnCloseImbalanceBuy).toBe(9);
        expect(SecurityTradingStatus.MarketOnCloseImbalanceSell).toBe(10);
        expect(SecurityTradingStatus.NoMarketImbalance).toBe(12);
        expect(SecurityTradingStatus.NoMarketOnCloseImbalance).toBe(13);
        expect(SecurityTradingStatus.ITSPreOpening).toBe(14);
        expect(SecurityTradingStatus.NewPriceIndication).toBe(15);
        expect(SecurityTradingStatus.TradeDisseminationTime).toBe(16);
        expect(SecurityTradingStatus.ReadyToTrade).toBe(17);
        expect(SecurityTradingStatus.NotAvailableForTrading).toBe(18);
        expect(SecurityTradingStatus.NotTradedOnThisMarket).toBe(19);
        expect(SecurityTradingStatus.UnknownOrInvalid).toBe(20);
        expect(SecurityTradingStatus.PreOpen).toBe(21);
        expect(SecurityTradingStatus.OpeningRotation).toBe(22);
        expect(SecurityTradingStatus.FastMarket).toBe(23);
        expect(SecurityTradingStatus.PreCross).toBe(24);
        expect(SecurityTradingStatus.Cross).toBe(25);
        expect(SecurityTradingStatus.PostClose).toBe(26);
        expect(SecurityTradingStatus.NoCancel).toBe(27);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityTradingStatus.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityTradingStatus.OpeningDelay,
            SecurityTradingStatus.TradingHalt,
            SecurityTradingStatus.Resume,
            SecurityTradingStatus.NoOpen,
            SecurityTradingStatus.PriceIndication,
            SecurityTradingStatus.TradingRangeIndication,
            SecurityTradingStatus.MarketImbalanceBuy,
            SecurityTradingStatus.MarketImbalanceSell,
            SecurityTradingStatus.MarketOnCloseImbalanceBuy,
            SecurityTradingStatus.MarketOnCloseImbalanceSell,
            SecurityTradingStatus.NoMarketImbalance,
            SecurityTradingStatus.NoMarketOnCloseImbalance,
            SecurityTradingStatus.ITSPreOpening,
            SecurityTradingStatus.NewPriceIndication,
            SecurityTradingStatus.TradeDisseminationTime,
            SecurityTradingStatus.ReadyToTrade,
            SecurityTradingStatus.NotAvailableForTrading,
            SecurityTradingStatus.NotTradedOnThisMarket,
            SecurityTradingStatus.UnknownOrInvalid,
            SecurityTradingStatus.PreOpen,
            SecurityTradingStatus.OpeningRotation,
            SecurityTradingStatus.FastMarket,
            SecurityTradingStatus.PreCross,
            SecurityTradingStatus.Cross,
            SecurityTradingStatus.PostClose,
            SecurityTradingStatus.NoCancel,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for SecurityTradingStatus type', () => {
        const validate = (value: SecurityTradingStatus) => {
            expect(Object.values(SecurityTradingStatus)).toContain(value);
        };

        validate(SecurityTradingStatus.OpeningDelay);
        validate(SecurityTradingStatus.TradingHalt);
        validate(SecurityTradingStatus.Resume);
        validate(SecurityTradingStatus.NoOpen);
        validate(SecurityTradingStatus.PriceIndication);
        validate(SecurityTradingStatus.TradingRangeIndication);
        validate(SecurityTradingStatus.MarketImbalanceBuy);
        validate(SecurityTradingStatus.MarketImbalanceSell);
        validate(SecurityTradingStatus.MarketOnCloseImbalanceBuy);
        validate(SecurityTradingStatus.MarketOnCloseImbalanceSell);
        validate(SecurityTradingStatus.NoMarketImbalance);
        validate(SecurityTradingStatus.NoMarketOnCloseImbalance);
        validate(SecurityTradingStatus.ITSPreOpening);
        validate(SecurityTradingStatus.NewPriceIndication);
        validate(SecurityTradingStatus.TradeDisseminationTime);
        validate(SecurityTradingStatus.ReadyToTrade);
        validate(SecurityTradingStatus.NotAvailableForTrading);
        validate(SecurityTradingStatus.NotTradedOnThisMarket);
        validate(SecurityTradingStatus.UnknownOrInvalid);
        validate(SecurityTradingStatus.PreOpen);
        validate(SecurityTradingStatus.OpeningRotation);
        validate(SecurityTradingStatus.FastMarket);
        validate(SecurityTradingStatus.PreCross);
        validate(SecurityTradingStatus.Cross);
        validate(SecurityTradingStatus.PostClose);
        validate(SecurityTradingStatus.NoCancel);
    });

    test('should be immutable', () => {
        const ref = SecurityTradingStatus;
        expect(() => {
            ref.OpeningDelay = 10;
        }).toThrowError();
    });
});
