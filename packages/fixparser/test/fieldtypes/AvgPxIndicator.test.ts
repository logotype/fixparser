import { AvgPxIndicator } from '../../src/fieldtypes/AvgPxIndicator';

describe('AvgPxIndicator', () => {
    test('should have the correct values', () => {
        expect(AvgPxIndicator.NoAveragePricing).toBe(0);
        expect(AvgPxIndicator.Trade).toBe(1);
        expect(AvgPxIndicator.LastTrade).toBe(2);
        expect(AvgPxIndicator.NotionalValueAveragePxGroupTrade).toBe(3);
        expect(AvgPxIndicator.AveragePricedTrade).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AvgPxIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AvgPxIndicator.NoAveragePricing,
            AvgPxIndicator.Trade,
            AvgPxIndicator.LastTrade,
            AvgPxIndicator.NotionalValueAveragePxGroupTrade,
            AvgPxIndicator.AveragePricedTrade,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AvgPxIndicator type', () => {
        const validate = (value: AvgPxIndicator) => {
            expect(Object.values(AvgPxIndicator)).toContain(value);
        };

        validate(AvgPxIndicator.NoAveragePricing);
        validate(AvgPxIndicator.Trade);
        validate(AvgPxIndicator.LastTrade);
        validate(AvgPxIndicator.NotionalValueAveragePxGroupTrade);
        validate(AvgPxIndicator.AveragePricedTrade);
    });

    test('should be immutable', () => {
        const ref = AvgPxIndicator;
        expect(() => {
            ref.NoAveragePricing = 10;
        }).toThrowError();
    });
});
