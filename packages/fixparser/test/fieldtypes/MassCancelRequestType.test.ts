import { MassCancelRequestType } from '../../src/fieldtypes/MassCancelRequestType';

describe('MassCancelRequestType', () => {
    test('should have the correct values', () => {
        expect(MassCancelRequestType.CancelOrdersForASecurity).toBe('1');
        expect(MassCancelRequestType.CancelOrdersForAnUnderlyingSecurity).toBe('2');
        expect(MassCancelRequestType.CancelOrdersForAProduct).toBe('3');
        expect(MassCancelRequestType.CancelOrdersForACFICode).toBe('4');
        expect(MassCancelRequestType.CancelOrdersForASecurityType).toBe('5');
        expect(MassCancelRequestType.CancelOrdersForATradingSession).toBe('6');
        expect(MassCancelRequestType.CancelAllOrders).toBe('7');
        expect(MassCancelRequestType.CancelOrdersForAMarket).toBe('8');
        expect(MassCancelRequestType.CancelOrdersForAMarketSegment).toBe('9');
        expect(MassCancelRequestType.CancelOrdersForASecurityGroup).toBe('A');
        expect(MassCancelRequestType.CancelOrdersForSecurityIssuer).toBe('B');
        expect(MassCancelRequestType.CancelForIssuerOfUnderlyingSecurity).toBe('C');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MassCancelRequestType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MassCancelRequestType.CancelOrdersForASecurity,
            MassCancelRequestType.CancelOrdersForAnUnderlyingSecurity,
            MassCancelRequestType.CancelOrdersForAProduct,
            MassCancelRequestType.CancelOrdersForACFICode,
            MassCancelRequestType.CancelOrdersForASecurityType,
            MassCancelRequestType.CancelOrdersForATradingSession,
            MassCancelRequestType.CancelAllOrders,
            MassCancelRequestType.CancelOrdersForAMarket,
            MassCancelRequestType.CancelOrdersForAMarketSegment,
            MassCancelRequestType.CancelOrdersForASecurityGroup,
            MassCancelRequestType.CancelOrdersForSecurityIssuer,
            MassCancelRequestType.CancelForIssuerOfUnderlyingSecurity,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for MassCancelRequestType type', () => {
        const validate = (value: MassCancelRequestType) => {
            expect(Object.values(MassCancelRequestType)).toContain(value);
        };

        validate(MassCancelRequestType.CancelOrdersForASecurity);
        validate(MassCancelRequestType.CancelOrdersForAnUnderlyingSecurity);
        validate(MassCancelRequestType.CancelOrdersForAProduct);
        validate(MassCancelRequestType.CancelOrdersForACFICode);
        validate(MassCancelRequestType.CancelOrdersForASecurityType);
        validate(MassCancelRequestType.CancelOrdersForATradingSession);
        validate(MassCancelRequestType.CancelAllOrders);
        validate(MassCancelRequestType.CancelOrdersForAMarket);
        validate(MassCancelRequestType.CancelOrdersForAMarketSegment);
        validate(MassCancelRequestType.CancelOrdersForASecurityGroup);
        validate(MassCancelRequestType.CancelOrdersForSecurityIssuer);
        validate(MassCancelRequestType.CancelForIssuerOfUnderlyingSecurity);
    });

    test('should be immutable', () => {
        const ref = MassCancelRequestType;
        expect(() => {
            ref.CancelOrdersForASecurity = 10;
        }).toThrowError();
    });
});
