import { OrderOrigination } from '../../src/fieldtypes/OrderOrigination';

describe('OrderOrigination', () => {
    test('should have the correct values', () => {
        expect(OrderOrigination.OrderReceivedFromCustomer).toBe(1);
        expect(OrderOrigination.OrderReceivedFromWithinFirm).toBe(2);
        expect(OrderOrigination.OrderReceivedFromAnotherBrokerDealer).toBe(3);
        expect(OrderOrigination.OrderReceivedFromCustomerOrWithFirm).toBe(4);
        expect(OrderOrigination.OrderReceivedFromDirectAccessCustomer).toBe(5);
        expect(OrderOrigination.OrderReceivedFromForeignDealerEquivalent).toBe(6);
        expect(OrderOrigination.OrderReceivedFromExecutionOnlyService).toBe(7);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            OrderOrigination.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            OrderOrigination.OrderReceivedFromCustomer,
            OrderOrigination.OrderReceivedFromWithinFirm,
            OrderOrigination.OrderReceivedFromAnotherBrokerDealer,
            OrderOrigination.OrderReceivedFromCustomerOrWithFirm,
            OrderOrigination.OrderReceivedFromDirectAccessCustomer,
            OrderOrigination.OrderReceivedFromForeignDealerEquivalent,
            OrderOrigination.OrderReceivedFromExecutionOnlyService,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for OrderOrigination type', () => {
        const validate = (value: OrderOrigination) => {
            expect(Object.values(OrderOrigination)).toContain(value);
        };

        validate(OrderOrigination.OrderReceivedFromCustomer);
        validate(OrderOrigination.OrderReceivedFromWithinFirm);
        validate(OrderOrigination.OrderReceivedFromAnotherBrokerDealer);
        validate(OrderOrigination.OrderReceivedFromCustomerOrWithFirm);
        validate(OrderOrigination.OrderReceivedFromDirectAccessCustomer);
        validate(OrderOrigination.OrderReceivedFromForeignDealerEquivalent);
        validate(OrderOrigination.OrderReceivedFromExecutionOnlyService);
    });

    test('should be immutable', () => {
        const ref = OrderOrigination;
        expect(() => {
            ref.OrderReceivedFromCustomer = 10;
        }).toThrowError();
    });
});
