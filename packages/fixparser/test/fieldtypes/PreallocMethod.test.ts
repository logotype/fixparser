import { PreallocMethod } from '../../src/fieldtypes/PreallocMethod';

describe('PreallocMethod', () => {
    test('should have the correct values', () => {
        expect(PreallocMethod.ProRata).toBe('0');
        expect(PreallocMethod.DoNotProRata).toBe('1');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PreallocMethod.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PreallocMethod.ProRata, PreallocMethod.DoNotProRata];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for PreallocMethod type', () => {
        const validate = (value: PreallocMethod) => {
            expect(Object.values(PreallocMethod)).toContain(value);
        };

        validate(PreallocMethod.ProRata);
        validate(PreallocMethod.DoNotProRata);
    });

    test('should be immutable', () => {
        const ref = PreallocMethod;
        expect(() => {
            ref.ProRata = 10;
        }).toThrowError();
    });
});
