import { TrdRegTimestampType } from '../../src/fieldtypes/TrdRegTimestampType';

describe('TrdRegTimestampType', () => {
    test('should have the correct values', () => {
        expect(TrdRegTimestampType.ExecutionTime).toBe(1);
        expect(TrdRegTimestampType.TimeIn).toBe(2);
        expect(TrdRegTimestampType.TimeOut).toBe(3);
        expect(TrdRegTimestampType.BrokerReceipt).toBe(4);
        expect(TrdRegTimestampType.BrokerExecution).toBe(5);
        expect(TrdRegTimestampType.DeskReceipt).toBe(6);
        expect(TrdRegTimestampType.SubmissionToClearing).toBe(7);
        expect(TrdRegTimestampType.TimePriority).toBe(8);
        expect(TrdRegTimestampType.OrderbookEntryTime).toBe(9);
        expect(TrdRegTimestampType.OrderSubmissionTime).toBe(10);
        expect(TrdRegTimestampType.PubliclyReported).toBe(11);
        expect(TrdRegTimestampType.PublicReportUpdated).toBe(12);
        expect(TrdRegTimestampType.NonPubliclyReported).toBe(13);
        expect(TrdRegTimestampType.NonPublicReportUpdated).toBe(14);
        expect(TrdRegTimestampType.SubmittedForConfirmation).toBe(15);
        expect(TrdRegTimestampType.UpdatedForConfirmation).toBe(16);
        expect(TrdRegTimestampType.Confirmed).toBe(17);
        expect(TrdRegTimestampType.UpdatedForClearing).toBe(18);
        expect(TrdRegTimestampType.Cleared).toBe(19);
        expect(TrdRegTimestampType.AllocationsSubmitted).toBe(20);
        expect(TrdRegTimestampType.AllocationsUpdated).toBe(21);
        expect(TrdRegTimestampType.AllocationsCompleted).toBe(22);
        expect(TrdRegTimestampType.SubmittedToRepository).toBe(23);
        expect(TrdRegTimestampType.PostTrdContntnEvnt).toBe(24);
        expect(TrdRegTimestampType.PostTradeValuation).toBe(25);
        expect(TrdRegTimestampType.PreviousTimePriority).toBe(26);
        expect(TrdRegTimestampType.IdentifierAssigned).toBe(27);
        expect(TrdRegTimestampType.PreviousIdentifierAssigned).toBe(28);
        expect(TrdRegTimestampType.OrderCancellationTime).toBe(29);
        expect(TrdRegTimestampType.OrderModificationTime).toBe(30);
        expect(TrdRegTimestampType.OrderRoutingTime).toBe(31);
        expect(TrdRegTimestampType.TradeCancellationTime).toBe(32);
        expect(TrdRegTimestampType.TradeModificationTime).toBe(33);
        expect(TrdRegTimestampType.ReferenceTimeForNBBO).toBe(34);
        expect(TrdRegTimestampType.Affirmed).toBe(35);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TrdRegTimestampType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TrdRegTimestampType.ExecutionTime,
            TrdRegTimestampType.TimeIn,
            TrdRegTimestampType.TimeOut,
            TrdRegTimestampType.BrokerReceipt,
            TrdRegTimestampType.BrokerExecution,
            TrdRegTimestampType.DeskReceipt,
            TrdRegTimestampType.SubmissionToClearing,
            TrdRegTimestampType.TimePriority,
            TrdRegTimestampType.OrderbookEntryTime,
            TrdRegTimestampType.OrderSubmissionTime,
            TrdRegTimestampType.PubliclyReported,
            TrdRegTimestampType.PublicReportUpdated,
            TrdRegTimestampType.NonPubliclyReported,
            TrdRegTimestampType.NonPublicReportUpdated,
            TrdRegTimestampType.SubmittedForConfirmation,
            TrdRegTimestampType.UpdatedForConfirmation,
            TrdRegTimestampType.Confirmed,
            TrdRegTimestampType.UpdatedForClearing,
            TrdRegTimestampType.Cleared,
            TrdRegTimestampType.AllocationsSubmitted,
            TrdRegTimestampType.AllocationsUpdated,
            TrdRegTimestampType.AllocationsCompleted,
            TrdRegTimestampType.SubmittedToRepository,
            TrdRegTimestampType.PostTrdContntnEvnt,
            TrdRegTimestampType.PostTradeValuation,
            TrdRegTimestampType.PreviousTimePriority,
            TrdRegTimestampType.IdentifierAssigned,
            TrdRegTimestampType.PreviousIdentifierAssigned,
            TrdRegTimestampType.OrderCancellationTime,
            TrdRegTimestampType.OrderModificationTime,
            TrdRegTimestampType.OrderRoutingTime,
            TrdRegTimestampType.TradeCancellationTime,
            TrdRegTimestampType.TradeModificationTime,
            TrdRegTimestampType.ReferenceTimeForNBBO,
            TrdRegTimestampType.Affirmed,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TrdRegTimestampType type', () => {
        const validate = (value: TrdRegTimestampType) => {
            expect(Object.values(TrdRegTimestampType)).toContain(value);
        };

        validate(TrdRegTimestampType.ExecutionTime);
        validate(TrdRegTimestampType.TimeIn);
        validate(TrdRegTimestampType.TimeOut);
        validate(TrdRegTimestampType.BrokerReceipt);
        validate(TrdRegTimestampType.BrokerExecution);
        validate(TrdRegTimestampType.DeskReceipt);
        validate(TrdRegTimestampType.SubmissionToClearing);
        validate(TrdRegTimestampType.TimePriority);
        validate(TrdRegTimestampType.OrderbookEntryTime);
        validate(TrdRegTimestampType.OrderSubmissionTime);
        validate(TrdRegTimestampType.PubliclyReported);
        validate(TrdRegTimestampType.PublicReportUpdated);
        validate(TrdRegTimestampType.NonPubliclyReported);
        validate(TrdRegTimestampType.NonPublicReportUpdated);
        validate(TrdRegTimestampType.SubmittedForConfirmation);
        validate(TrdRegTimestampType.UpdatedForConfirmation);
        validate(TrdRegTimestampType.Confirmed);
        validate(TrdRegTimestampType.UpdatedForClearing);
        validate(TrdRegTimestampType.Cleared);
        validate(TrdRegTimestampType.AllocationsSubmitted);
        validate(TrdRegTimestampType.AllocationsUpdated);
        validate(TrdRegTimestampType.AllocationsCompleted);
        validate(TrdRegTimestampType.SubmittedToRepository);
        validate(TrdRegTimestampType.PostTrdContntnEvnt);
        validate(TrdRegTimestampType.PostTradeValuation);
        validate(TrdRegTimestampType.PreviousTimePriority);
        validate(TrdRegTimestampType.IdentifierAssigned);
        validate(TrdRegTimestampType.PreviousIdentifierAssigned);
        validate(TrdRegTimestampType.OrderCancellationTime);
        validate(TrdRegTimestampType.OrderModificationTime);
        validate(TrdRegTimestampType.OrderRoutingTime);
        validate(TrdRegTimestampType.TradeCancellationTime);
        validate(TrdRegTimestampType.TradeModificationTime);
        validate(TrdRegTimestampType.ReferenceTimeForNBBO);
        validate(TrdRegTimestampType.Affirmed);
    });

    test('should be immutable', () => {
        const ref = TrdRegTimestampType;
        expect(() => {
            ref.ExecutionTime = 10;
        }).toThrowError();
    });
});
