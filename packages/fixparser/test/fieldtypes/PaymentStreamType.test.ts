import { PaymentStreamType } from '../../src/fieldtypes/PaymentStreamType';

describe('PaymentStreamType', () => {
    test('should have the correct values', () => {
        expect(PaymentStreamType.Periodic).toBe(0);
        expect(PaymentStreamType.Initial).toBe(1);
        expect(PaymentStreamType.Single).toBe(2);
        expect(PaymentStreamType.Dividend).toBe(3);
        expect(PaymentStreamType.Interest).toBe(4);
        expect(PaymentStreamType.DividendReturn).toBe(5);
        expect(PaymentStreamType.PriceReturn).toBe(6);
        expect(PaymentStreamType.TotalReturn).toBe(7);
        expect(PaymentStreamType.Variance).toBe(8);
        expect(PaymentStreamType.Correlation).toBe(9);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStreamType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStreamType.Periodic,
            PaymentStreamType.Initial,
            PaymentStreamType.Single,
            PaymentStreamType.Dividend,
            PaymentStreamType.Interest,
            PaymentStreamType.DividendReturn,
            PaymentStreamType.PriceReturn,
            PaymentStreamType.TotalReturn,
            PaymentStreamType.Variance,
            PaymentStreamType.Correlation,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStreamType type', () => {
        const validate = (value: PaymentStreamType) => {
            expect(Object.values(PaymentStreamType)).toContain(value);
        };

        validate(PaymentStreamType.Periodic);
        validate(PaymentStreamType.Initial);
        validate(PaymentStreamType.Single);
        validate(PaymentStreamType.Dividend);
        validate(PaymentStreamType.Interest);
        validate(PaymentStreamType.DividendReturn);
        validate(PaymentStreamType.PriceReturn);
        validate(PaymentStreamType.TotalReturn);
        validate(PaymentStreamType.Variance);
        validate(PaymentStreamType.Correlation);
    });

    test('should be immutable', () => {
        const ref = PaymentStreamType;
        expect(() => {
            ref.Periodic = 10;
        }).toThrowError();
    });
});
