import { PosAmtReason } from '../../src/fieldtypes/PosAmtReason';

describe('PosAmtReason', () => {
    test('should have the correct values', () => {
        expect(PosAmtReason.OptionsSettlement).toBe(0);
        expect(PosAmtReason.PendingErosionAdjustment).toBe(1);
        expect(PosAmtReason.FinalErosionAdjustment).toBe(2);
        expect(PosAmtReason.TearUpCouponAmount).toBe(3);
        expect(PosAmtReason.PriceAlignmentInterest).toBe(4);
        expect(PosAmtReason.DeliveryInvoiceCharges).toBe(5);
        expect(PosAmtReason.DeliveryStorageCharges).toBe(6);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PosAmtReason.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PosAmtReason.OptionsSettlement,
            PosAmtReason.PendingErosionAdjustment,
            PosAmtReason.FinalErosionAdjustment,
            PosAmtReason.TearUpCouponAmount,
            PosAmtReason.PriceAlignmentInterest,
            PosAmtReason.DeliveryInvoiceCharges,
            PosAmtReason.DeliveryStorageCharges,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PosAmtReason type', () => {
        const validate = (value: PosAmtReason) => {
            expect(Object.values(PosAmtReason)).toContain(value);
        };

        validate(PosAmtReason.OptionsSettlement);
        validate(PosAmtReason.PendingErosionAdjustment);
        validate(PosAmtReason.FinalErosionAdjustment);
        validate(PosAmtReason.TearUpCouponAmount);
        validate(PosAmtReason.PriceAlignmentInterest);
        validate(PosAmtReason.DeliveryInvoiceCharges);
        validate(PosAmtReason.DeliveryStorageCharges);
    });

    test('should be immutable', () => {
        const ref = PosAmtReason;
        expect(() => {
            ref.OptionsSettlement = 10;
        }).toThrowError();
    });
});
