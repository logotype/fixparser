import { MarketSegmentRelationship } from '../../src/fieldtypes/MarketSegmentRelationship';

describe('MarketSegmentRelationship', () => {
    test('should have the correct values', () => {
        expect(MarketSegmentRelationship.MarketSegmentPoolMember).toBe(1);
        expect(MarketSegmentRelationship.RetailSegment).toBe(2);
        expect(MarketSegmentRelationship.WholesaleSegment).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            MarketSegmentRelationship.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            MarketSegmentRelationship.MarketSegmentPoolMember,
            MarketSegmentRelationship.RetailSegment,
            MarketSegmentRelationship.WholesaleSegment,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for MarketSegmentRelationship type', () => {
        const validate = (value: MarketSegmentRelationship) => {
            expect(Object.values(MarketSegmentRelationship)).toContain(value);
        };

        validate(MarketSegmentRelationship.MarketSegmentPoolMember);
        validate(MarketSegmentRelationship.RetailSegment);
        validate(MarketSegmentRelationship.WholesaleSegment);
    });

    test('should be immutable', () => {
        const ref = MarketSegmentRelationship;
        expect(() => {
            ref.MarketSegmentPoolMember = 10;
        }).toThrowError();
    });
});
