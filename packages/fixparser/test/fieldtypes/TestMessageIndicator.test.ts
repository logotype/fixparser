import { TestMessageIndicator } from '../../src/fieldtypes/TestMessageIndicator';

describe('TestMessageIndicator', () => {
    test('should have the correct values', () => {
        expect(TestMessageIndicator.False).toBe('N');
        expect(TestMessageIndicator.True).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TestMessageIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [TestMessageIndicator.False, TestMessageIndicator.True];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for TestMessageIndicator type', () => {
        const validate = (value: TestMessageIndicator) => {
            expect(Object.values(TestMessageIndicator)).toContain(value);
        };

        validate(TestMessageIndicator.False);
        validate(TestMessageIndicator.True);
    });

    test('should be immutable', () => {
        const ref = TestMessageIndicator;
        expect(() => {
            ref.False = 10;
        }).toThrowError();
    });
});
