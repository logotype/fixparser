import { RespondentType } from '../../src/fieldtypes/RespondentType';

describe('RespondentType', () => {
    test('should have the correct values', () => {
        expect(RespondentType.AllMarketParticipants).toBe(1);
        expect(RespondentType.SpecifiedMarketParticipants).toBe(2);
        expect(RespondentType.AllMarketMakers).toBe(3);
        expect(RespondentType.PrimaryMarketMaker).toBe(4);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RespondentType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RespondentType.AllMarketParticipants,
            RespondentType.SpecifiedMarketParticipants,
            RespondentType.AllMarketMakers,
            RespondentType.PrimaryMarketMaker,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RespondentType type', () => {
        const validate = (value: RespondentType) => {
            expect(Object.values(RespondentType)).toContain(value);
        };

        validate(RespondentType.AllMarketParticipants);
        validate(RespondentType.SpecifiedMarketParticipants);
        validate(RespondentType.AllMarketMakers);
        validate(RespondentType.PrimaryMarketMaker);
    });

    test('should be immutable', () => {
        const ref = RespondentType;
        expect(() => {
            ref.AllMarketParticipants = 10;
        }).toThrowError();
    });
});
