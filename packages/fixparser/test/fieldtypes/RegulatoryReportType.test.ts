import { RegulatoryReportType } from '../../src/fieldtypes/RegulatoryReportType';

describe('RegulatoryReportType', () => {
    test('should have the correct values', () => {
        expect(RegulatoryReportType.RT).toBe(0);
        expect(RegulatoryReportType.PET).toBe(1);
        expect(RegulatoryReportType.Snapshot).toBe(2);
        expect(RegulatoryReportType.Confirmation).toBe(3);
        expect(RegulatoryReportType.RTPET).toBe(4);
        expect(RegulatoryReportType.PETConfirmation).toBe(5);
        expect(RegulatoryReportType.RTPETConfirmation).toBe(6);
        expect(RegulatoryReportType.PostTrade).toBe(7);
        expect(RegulatoryReportType.Verification).toBe(8);
        expect(RegulatoryReportType.PstTrdEvnt).toBe(9);
        expect(RegulatoryReportType.PstTrdEvntRTReportable).toBe(10);
        expect(RegulatoryReportType.LMTF).toBe(11);
        expect(RegulatoryReportType.DATF).toBe(12);
        expect(RegulatoryReportType.VOLO).toBe(13);
        expect(RegulatoryReportType.FWAF).toBe(14);
        expect(RegulatoryReportType.IDAF).toBe(15);
        expect(RegulatoryReportType.VOLW).toBe(16);
        expect(RegulatoryReportType.FULF).toBe(17);
        expect(RegulatoryReportType.FULA).toBe(18);
        expect(RegulatoryReportType.FULV).toBe(19);
        expect(RegulatoryReportType.FULJ).toBe(20);
        expect(RegulatoryReportType.COAF).toBe(21);
        expect(RegulatoryReportType.Order).toBe(22);
        expect(RegulatoryReportType.ChildOrder).toBe(23);
        expect(RegulatoryReportType.OrderRoute).toBe(24);
        expect(RegulatoryReportType.Trade).toBe(25);
        expect(RegulatoryReportType.Quote).toBe(26);
        expect(RegulatoryReportType.Supplement).toBe(27);
        expect(RegulatoryReportType.NewTransaction).toBe(28);
        expect(RegulatoryReportType.TransactionCorrection).toBe(29);
        expect(RegulatoryReportType.TransactionModification).toBe(30);
        expect(RegulatoryReportType.CollateralUpdate).toBe(31);
        expect(RegulatoryReportType.MarginUpdate).toBe(32);
        expect(RegulatoryReportType.TransactionReportedInError).toBe(33);
        expect(RegulatoryReportType.TerminationEarlyTermination).toBe(34);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RegulatoryReportType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RegulatoryReportType.RT,
            RegulatoryReportType.PET,
            RegulatoryReportType.Snapshot,
            RegulatoryReportType.Confirmation,
            RegulatoryReportType.RTPET,
            RegulatoryReportType.PETConfirmation,
            RegulatoryReportType.RTPETConfirmation,
            RegulatoryReportType.PostTrade,
            RegulatoryReportType.Verification,
            RegulatoryReportType.PstTrdEvnt,
            RegulatoryReportType.PstTrdEvntRTReportable,
            RegulatoryReportType.LMTF,
            RegulatoryReportType.DATF,
            RegulatoryReportType.VOLO,
            RegulatoryReportType.FWAF,
            RegulatoryReportType.IDAF,
            RegulatoryReportType.VOLW,
            RegulatoryReportType.FULF,
            RegulatoryReportType.FULA,
            RegulatoryReportType.FULV,
            RegulatoryReportType.FULJ,
            RegulatoryReportType.COAF,
            RegulatoryReportType.Order,
            RegulatoryReportType.ChildOrder,
            RegulatoryReportType.OrderRoute,
            RegulatoryReportType.Trade,
            RegulatoryReportType.Quote,
            RegulatoryReportType.Supplement,
            RegulatoryReportType.NewTransaction,
            RegulatoryReportType.TransactionCorrection,
            RegulatoryReportType.TransactionModification,
            RegulatoryReportType.CollateralUpdate,
            RegulatoryReportType.MarginUpdate,
            RegulatoryReportType.TransactionReportedInError,
            RegulatoryReportType.TerminationEarlyTermination,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RegulatoryReportType type', () => {
        const validate = (value: RegulatoryReportType) => {
            expect(Object.values(RegulatoryReportType)).toContain(value);
        };

        validate(RegulatoryReportType.RT);
        validate(RegulatoryReportType.PET);
        validate(RegulatoryReportType.Snapshot);
        validate(RegulatoryReportType.Confirmation);
        validate(RegulatoryReportType.RTPET);
        validate(RegulatoryReportType.PETConfirmation);
        validate(RegulatoryReportType.RTPETConfirmation);
        validate(RegulatoryReportType.PostTrade);
        validate(RegulatoryReportType.Verification);
        validate(RegulatoryReportType.PstTrdEvnt);
        validate(RegulatoryReportType.PstTrdEvntRTReportable);
        validate(RegulatoryReportType.LMTF);
        validate(RegulatoryReportType.DATF);
        validate(RegulatoryReportType.VOLO);
        validate(RegulatoryReportType.FWAF);
        validate(RegulatoryReportType.IDAF);
        validate(RegulatoryReportType.VOLW);
        validate(RegulatoryReportType.FULF);
        validate(RegulatoryReportType.FULA);
        validate(RegulatoryReportType.FULV);
        validate(RegulatoryReportType.FULJ);
        validate(RegulatoryReportType.COAF);
        validate(RegulatoryReportType.Order);
        validate(RegulatoryReportType.ChildOrder);
        validate(RegulatoryReportType.OrderRoute);
        validate(RegulatoryReportType.Trade);
        validate(RegulatoryReportType.Quote);
        validate(RegulatoryReportType.Supplement);
        validate(RegulatoryReportType.NewTransaction);
        validate(RegulatoryReportType.TransactionCorrection);
        validate(RegulatoryReportType.TransactionModification);
        validate(RegulatoryReportType.CollateralUpdate);
        validate(RegulatoryReportType.MarginUpdate);
        validate(RegulatoryReportType.TransactionReportedInError);
        validate(RegulatoryReportType.TerminationEarlyTermination);
    });

    test('should be immutable', () => {
        const ref = RegulatoryReportType;
        expect(() => {
            ref.RT = 10;
        }).toThrowError();
    });
});
