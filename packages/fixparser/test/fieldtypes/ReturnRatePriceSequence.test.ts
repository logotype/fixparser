import { ReturnRatePriceSequence } from '../../src/fieldtypes/ReturnRatePriceSequence';

describe('ReturnRatePriceSequence', () => {
    test('should have the correct values', () => {
        expect(ReturnRatePriceSequence.Initial).toBe(0);
        expect(ReturnRatePriceSequence.Interim).toBe(1);
        expect(ReturnRatePriceSequence.Final).toBe(2);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ReturnRatePriceSequence.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ReturnRatePriceSequence.Initial,
            ReturnRatePriceSequence.Interim,
            ReturnRatePriceSequence.Final,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ReturnRatePriceSequence type', () => {
        const validate = (value: ReturnRatePriceSequence) => {
            expect(Object.values(ReturnRatePriceSequence)).toContain(value);
        };

        validate(ReturnRatePriceSequence.Initial);
        validate(ReturnRatePriceSequence.Interim);
        validate(ReturnRatePriceSequence.Final);
    });

    test('should be immutable', () => {
        const ref = ReturnRatePriceSequence;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
