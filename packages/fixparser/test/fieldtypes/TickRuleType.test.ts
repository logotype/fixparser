import { TickRuleType } from '../../src/fieldtypes/TickRuleType';

describe('TickRuleType', () => {
    test('should have the correct values', () => {
        expect(TickRuleType.RegularTrading).toBe(0);
        expect(TickRuleType.VariableCabinet).toBe(1);
        expect(TickRuleType.FixedCabinet).toBe(2);
        expect(TickRuleType.TradedAsASpreadLeg).toBe(3);
        expect(TickRuleType.SettledAsASpreadLeg).toBe(4);
        expect(TickRuleType.TradedAsSpread).toBe(5);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            TickRuleType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            TickRuleType.RegularTrading,
            TickRuleType.VariableCabinet,
            TickRuleType.FixedCabinet,
            TickRuleType.TradedAsASpreadLeg,
            TickRuleType.SettledAsASpreadLeg,
            TickRuleType.TradedAsSpread,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for TickRuleType type', () => {
        const validate = (value: TickRuleType) => {
            expect(Object.values(TickRuleType)).toContain(value);
        };

        validate(TickRuleType.RegularTrading);
        validate(TickRuleType.VariableCabinet);
        validate(TickRuleType.FixedCabinet);
        validate(TickRuleType.TradedAsASpreadLeg);
        validate(TickRuleType.SettledAsASpreadLeg);
        validate(TickRuleType.TradedAsSpread);
    });

    test('should be immutable', () => {
        const ref = TickRuleType;
        expect(() => {
            ref.RegularTrading = 10;
        }).toThrowError();
    });
});
