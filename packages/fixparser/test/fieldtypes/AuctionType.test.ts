import { AuctionType } from '../../src/fieldtypes/AuctionType';

describe('AuctionType', () => {
    test('should have the correct values', () => {
        expect(AuctionType.None).toBe(0);
        expect(AuctionType.BlockOrderAuction).toBe(1);
        expect(AuctionType.DirectedOrderAuction).toBe(2);
        expect(AuctionType.ExposureOrderAuction).toBe(3);
        expect(AuctionType.FlashOrderAuction).toBe(4);
        expect(AuctionType.FacilitationOrderAuction).toBe(5);
        expect(AuctionType.SolicitationOrderAuction).toBe(6);
        expect(AuctionType.PriceImprovementMechanism).toBe(7);
        expect(AuctionType.DirectedOrderPriceImprovementMechanism).toBe(8);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AuctionType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AuctionType.None,
            AuctionType.BlockOrderAuction,
            AuctionType.DirectedOrderAuction,
            AuctionType.ExposureOrderAuction,
            AuctionType.FlashOrderAuction,
            AuctionType.FacilitationOrderAuction,
            AuctionType.SolicitationOrderAuction,
            AuctionType.PriceImprovementMechanism,
            AuctionType.DirectedOrderPriceImprovementMechanism,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AuctionType type', () => {
        const validate = (value: AuctionType) => {
            expect(Object.values(AuctionType)).toContain(value);
        };

        validate(AuctionType.None);
        validate(AuctionType.BlockOrderAuction);
        validate(AuctionType.DirectedOrderAuction);
        validate(AuctionType.ExposureOrderAuction);
        validate(AuctionType.FlashOrderAuction);
        validate(AuctionType.FacilitationOrderAuction);
        validate(AuctionType.SolicitationOrderAuction);
        validate(AuctionType.PriceImprovementMechanism);
        validate(AuctionType.DirectedOrderPriceImprovementMechanism);
    });

    test('should be immutable', () => {
        const ref = AuctionType;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
