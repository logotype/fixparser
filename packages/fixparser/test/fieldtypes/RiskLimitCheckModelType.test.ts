import { RiskLimitCheckModelType } from '../../src/fieldtypes/RiskLimitCheckModelType';

describe('RiskLimitCheckModelType', () => {
    test('should have the correct values', () => {
        expect(RiskLimitCheckModelType.None).toBe(0);
        expect(RiskLimitCheckModelType.PlusOneModel).toBe(1);
        expect(RiskLimitCheckModelType.PingModel).toBe(2);
        expect(RiskLimitCheckModelType.PushModel).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            RiskLimitCheckModelType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            RiskLimitCheckModelType.None,
            RiskLimitCheckModelType.PlusOneModel,
            RiskLimitCheckModelType.PingModel,
            RiskLimitCheckModelType.PushModel,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for RiskLimitCheckModelType type', () => {
        const validate = (value: RiskLimitCheckModelType) => {
            expect(Object.values(RiskLimitCheckModelType)).toContain(value);
        };

        validate(RiskLimitCheckModelType.None);
        validate(RiskLimitCheckModelType.PlusOneModel);
        validate(RiskLimitCheckModelType.PingModel);
        validate(RiskLimitCheckModelType.PushModel);
    });

    test('should be immutable', () => {
        const ref = RiskLimitCheckModelType;
        expect(() => {
            ref.None = 10;
        }).toThrowError();
    });
});
