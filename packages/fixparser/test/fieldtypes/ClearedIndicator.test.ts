import { ClearedIndicator } from '../../src/fieldtypes/ClearedIndicator';

describe('ClearedIndicator', () => {
    test('should have the correct values', () => {
        expect(ClearedIndicator.NotCleared).toBe(0);
        expect(ClearedIndicator.Cleared).toBe(1);
        expect(ClearedIndicator.Submitted).toBe(2);
        expect(ClearedIndicator.Rejected).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            ClearedIndicator.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ClearedIndicator.NotCleared,
            ClearedIndicator.Cleared,
            ClearedIndicator.Submitted,
            ClearedIndicator.Rejected,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for ClearedIndicator type', () => {
        const validate = (value: ClearedIndicator) => {
            expect(Object.values(ClearedIndicator)).toContain(value);
        };

        validate(ClearedIndicator.NotCleared);
        validate(ClearedIndicator.Cleared);
        validate(ClearedIndicator.Submitted);
        validate(ClearedIndicator.Rejected);
    });

    test('should be immutable', () => {
        const ref = ClearedIndicator;
        expect(() => {
            ref.NotCleared = 10;
        }).toThrowError();
    });
});
