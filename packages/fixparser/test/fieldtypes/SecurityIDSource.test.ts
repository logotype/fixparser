import { SecurityIDSource } from '../../src/fieldtypes/SecurityIDSource';

describe('SecurityIDSource', () => {
    test('should have the correct values', () => {
        expect(SecurityIDSource.CUSIP).toBe('1');
        expect(SecurityIDSource.SEDOL).toBe('2');
        expect(SecurityIDSource.QUIK).toBe('3');
        expect(SecurityIDSource.ISINNumber).toBe('4');
        expect(SecurityIDSource.RICCode).toBe('5');
        expect(SecurityIDSource.ISOCurrencyCode).toBe('6');
        expect(SecurityIDSource.ISOCountryCode).toBe('7');
        expect(SecurityIDSource.ExchangeSymbol).toBe('8');
        expect(SecurityIDSource.ConsolidatedTapeAssociation).toBe('9');
        expect(SecurityIDSource.BloombergSymbol).toBe('A');
        expect(SecurityIDSource.Wertpapier).toBe('B');
        expect(SecurityIDSource.Dutch).toBe('C');
        expect(SecurityIDSource.Valoren).toBe('D');
        expect(SecurityIDSource.Sicovam).toBe('E');
        expect(SecurityIDSource.Belgian).toBe('F');
        expect(SecurityIDSource.Common).toBe('G');
        expect(SecurityIDSource.ClearingHouse).toBe('H');
        expect(SecurityIDSource.ISDAFpMLSpecification).toBe('I');
        expect(SecurityIDSource.OptionPriceReportingAuthority).toBe('J');
        expect(SecurityIDSource.ISDAFpMLURL).toBe('K');
        expect(SecurityIDSource.LetterOfCredit).toBe('L');
        expect(SecurityIDSource.MarketplaceAssignedIdentifier).toBe('M');
        expect(SecurityIDSource.MarkitREDEntityCLIP).toBe('N');
        expect(SecurityIDSource.MarkitREDPairCLIP).toBe('P');
        expect(SecurityIDSource.CFTCCommodityCode).toBe('Q');
        expect(SecurityIDSource.ISDACommodityReferencePrice).toBe('R');
        expect(SecurityIDSource.FinancialInstrumentGlobalIdentifier).toBe('S');
        expect(SecurityIDSource.LegalEntityIdentifier).toBe('T');
        expect(SecurityIDSource.Synthetic).toBe('U');
        expect(SecurityIDSource.FidessaInstrumentMnemonic).toBe('V');
        expect(SecurityIDSource.IndexName).toBe('W');
        expect(SecurityIDSource.UniformSymbol).toBe('X');
        expect(SecurityIDSource.DigitalTokenIdentifier).toBe('Y');
    });

    test('should not allow additional properties', () => {
        expect(() => {
            SecurityIDSource.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            SecurityIDSource.CUSIP,
            SecurityIDSource.SEDOL,
            SecurityIDSource.QUIK,
            SecurityIDSource.ISINNumber,
            SecurityIDSource.RICCode,
            SecurityIDSource.ISOCurrencyCode,
            SecurityIDSource.ISOCountryCode,
            SecurityIDSource.ExchangeSymbol,
            SecurityIDSource.ConsolidatedTapeAssociation,
            SecurityIDSource.BloombergSymbol,
            SecurityIDSource.Wertpapier,
            SecurityIDSource.Dutch,
            SecurityIDSource.Valoren,
            SecurityIDSource.Sicovam,
            SecurityIDSource.Belgian,
            SecurityIDSource.Common,
            SecurityIDSource.ClearingHouse,
            SecurityIDSource.ISDAFpMLSpecification,
            SecurityIDSource.OptionPriceReportingAuthority,
            SecurityIDSource.ISDAFpMLURL,
            SecurityIDSource.LetterOfCredit,
            SecurityIDSource.MarketplaceAssignedIdentifier,
            SecurityIDSource.MarkitREDEntityCLIP,
            SecurityIDSource.MarkitREDPairCLIP,
            SecurityIDSource.CFTCCommodityCode,
            SecurityIDSource.ISDACommodityReferencePrice,
            SecurityIDSource.FinancialInstrumentGlobalIdentifier,
            SecurityIDSource.LegalEntityIdentifier,
            SecurityIDSource.Synthetic,
            SecurityIDSource.FidessaInstrumentMnemonic,
            SecurityIDSource.IndexName,
            SecurityIDSource.UniformSymbol,
            SecurityIDSource.DigitalTokenIdentifier,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for SecurityIDSource type', () => {
        const validate = (value: SecurityIDSource) => {
            expect(Object.values(SecurityIDSource)).toContain(value);
        };

        validate(SecurityIDSource.CUSIP);
        validate(SecurityIDSource.SEDOL);
        validate(SecurityIDSource.QUIK);
        validate(SecurityIDSource.ISINNumber);
        validate(SecurityIDSource.RICCode);
        validate(SecurityIDSource.ISOCurrencyCode);
        validate(SecurityIDSource.ISOCountryCode);
        validate(SecurityIDSource.ExchangeSymbol);
        validate(SecurityIDSource.ConsolidatedTapeAssociation);
        validate(SecurityIDSource.BloombergSymbol);
        validate(SecurityIDSource.Wertpapier);
        validate(SecurityIDSource.Dutch);
        validate(SecurityIDSource.Valoren);
        validate(SecurityIDSource.Sicovam);
        validate(SecurityIDSource.Belgian);
        validate(SecurityIDSource.Common);
        validate(SecurityIDSource.ClearingHouse);
        validate(SecurityIDSource.ISDAFpMLSpecification);
        validate(SecurityIDSource.OptionPriceReportingAuthority);
        validate(SecurityIDSource.ISDAFpMLURL);
        validate(SecurityIDSource.LetterOfCredit);
        validate(SecurityIDSource.MarketplaceAssignedIdentifier);
        validate(SecurityIDSource.MarkitREDEntityCLIP);
        validate(SecurityIDSource.MarkitREDPairCLIP);
        validate(SecurityIDSource.CFTCCommodityCode);
        validate(SecurityIDSource.ISDACommodityReferencePrice);
        validate(SecurityIDSource.FinancialInstrumentGlobalIdentifier);
        validate(SecurityIDSource.LegalEntityIdentifier);
        validate(SecurityIDSource.Synthetic);
        validate(SecurityIDSource.FidessaInstrumentMnemonic);
        validate(SecurityIDSource.IndexName);
        validate(SecurityIDSource.UniformSymbol);
        validate(SecurityIDSource.DigitalTokenIdentifier);
    });

    test('should be immutable', () => {
        const ref = SecurityIDSource;
        expect(() => {
            ref.CUSIP = 10;
        }).toThrowError();
    });
});
