import { PayRequestTransType } from '../../src/fieldtypes/PayRequestTransType';

describe('PayRequestTransType', () => {
    test('should have the correct values', () => {
        expect(PayRequestTransType.New).toBe(0);
        expect(PayRequestTransType.Cancel).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PayRequestTransType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [PayRequestTransType.New, PayRequestTransType.Cancel];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PayRequestTransType type', () => {
        const validate = (value: PayRequestTransType) => {
            expect(Object.values(PayRequestTransType)).toContain(value);
        };

        validate(PayRequestTransType.New);
        validate(PayRequestTransType.Cancel);
    });

    test('should be immutable', () => {
        const ref = PayRequestTransType;
        expect(() => {
            ref.New = 10;
        }).toThrowError();
    });
});
