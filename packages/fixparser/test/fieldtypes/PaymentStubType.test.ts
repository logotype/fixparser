import { PaymentStubType } from '../../src/fieldtypes/PaymentStubType';

describe('PaymentStubType', () => {
    test('should have the correct values', () => {
        expect(PaymentStubType.Initial).toBe(0);
        expect(PaymentStubType.Final).toBe(1);
        expect(PaymentStubType.CompoundingInitial).toBe(2);
        expect(PaymentStubType.CompoundingFinal).toBe(3);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            PaymentStubType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            PaymentStubType.Initial,
            PaymentStubType.Final,
            PaymentStubType.CompoundingInitial,
            PaymentStubType.CompoundingFinal,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for PaymentStubType type', () => {
        const validate = (value: PaymentStubType) => {
            expect(Object.values(PaymentStubType)).toContain(value);
        };

        validate(PaymentStubType.Initial);
        validate(PaymentStubType.Final);
        validate(PaymentStubType.CompoundingInitial);
        validate(PaymentStubType.CompoundingFinal);
    });

    test('should be immutable', () => {
        const ref = PaymentStubType;
        expect(() => {
            ref.Initial = 10;
        }).toThrowError();
    });
});
