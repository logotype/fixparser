import { AllocRejCode } from '../../src/fieldtypes/AllocRejCode';

describe('AllocRejCode', () => {
    test('should have the correct values', () => {
        expect(AllocRejCode.UnknownAccount).toBe(0);
        expect(AllocRejCode.IncorrectQuantity).toBe(1);
        expect(AllocRejCode.IncorrectAveragePrice).toBe(2);
        expect(AllocRejCode.UnknownExecutingBrokerMnemonic).toBe(3);
        expect(AllocRejCode.CommissionDifference).toBe(4);
        expect(AllocRejCode.UnknownOrderID).toBe(5);
        expect(AllocRejCode.UnknownListID).toBe(6);
        expect(AllocRejCode.OtherSeeText).toBe(7);
        expect(AllocRejCode.IncorrectAllocatedQuantity).toBe(8);
        expect(AllocRejCode.CalculationDifference).toBe(9);
        expect(AllocRejCode.UnknownOrStaleExecID).toBe(10);
        expect(AllocRejCode.MismatchedData).toBe(11);
        expect(AllocRejCode.UnknownClOrdID).toBe(12);
        expect(AllocRejCode.WarehouseRequestRejected).toBe(13);
        expect(AllocRejCode.DuplicateOrMissingIndividualAllocID).toBe(14);
        expect(AllocRejCode.TradeNotRecognized).toBe(15);
        expect(AllocRejCode.DuplicateTrade).toBe(16);
        expect(AllocRejCode.IncorrectOrMissingInstrument).toBe(17);
        expect(AllocRejCode.IncorrectOrMissingSettlDate).toBe(18);
        expect(AllocRejCode.IncorrectOrMissingFundIDOrFundName).toBe(19);
        expect(AllocRejCode.IncorrectOrMissingSettlInstructions).toBe(20);
        expect(AllocRejCode.IncorrectOrMissingFees).toBe(21);
        expect(AllocRejCode.IncorrectOrMissingTax).toBe(22);
        expect(AllocRejCode.UnknownOrMissingParty).toBe(23);
        expect(AllocRejCode.IncorrectOrMissingSide).toBe(24);
        expect(AllocRejCode.IncorrectOrMissingNetMoney).toBe(25);
        expect(AllocRejCode.IncorrectOrMissingTradeDate).toBe(26);
        expect(AllocRejCode.IncorrectOrMissingSettlCcyInstructions).toBe(27);
        expect(AllocRejCode.IncorrectOrMissingProcessCode).toBe(28);
        expect(AllocRejCode.Other).toBe(99);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            AllocRejCode.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            AllocRejCode.UnknownAccount,
            AllocRejCode.IncorrectQuantity,
            AllocRejCode.IncorrectAveragePrice,
            AllocRejCode.UnknownExecutingBrokerMnemonic,
            AllocRejCode.CommissionDifference,
            AllocRejCode.UnknownOrderID,
            AllocRejCode.UnknownListID,
            AllocRejCode.OtherSeeText,
            AllocRejCode.IncorrectAllocatedQuantity,
            AllocRejCode.CalculationDifference,
            AllocRejCode.UnknownOrStaleExecID,
            AllocRejCode.MismatchedData,
            AllocRejCode.UnknownClOrdID,
            AllocRejCode.WarehouseRequestRejected,
            AllocRejCode.DuplicateOrMissingIndividualAllocID,
            AllocRejCode.TradeNotRecognized,
            AllocRejCode.DuplicateTrade,
            AllocRejCode.IncorrectOrMissingInstrument,
            AllocRejCode.IncorrectOrMissingSettlDate,
            AllocRejCode.IncorrectOrMissingFundIDOrFundName,
            AllocRejCode.IncorrectOrMissingSettlInstructions,
            AllocRejCode.IncorrectOrMissingFees,
            AllocRejCode.IncorrectOrMissingTax,
            AllocRejCode.UnknownOrMissingParty,
            AllocRejCode.IncorrectOrMissingSide,
            AllocRejCode.IncorrectOrMissingNetMoney,
            AllocRejCode.IncorrectOrMissingTradeDate,
            AllocRejCode.IncorrectOrMissingSettlCcyInstructions,
            AllocRejCode.IncorrectOrMissingProcessCode,
            AllocRejCode.Other,
        ];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for AllocRejCode type', () => {
        const validate = (value: AllocRejCode) => {
            expect(Object.values(AllocRejCode)).toContain(value);
        };

        validate(AllocRejCode.UnknownAccount);
        validate(AllocRejCode.IncorrectQuantity);
        validate(AllocRejCode.IncorrectAveragePrice);
        validate(AllocRejCode.UnknownExecutingBrokerMnemonic);
        validate(AllocRejCode.CommissionDifference);
        validate(AllocRejCode.UnknownOrderID);
        validate(AllocRejCode.UnknownListID);
        validate(AllocRejCode.OtherSeeText);
        validate(AllocRejCode.IncorrectAllocatedQuantity);
        validate(AllocRejCode.CalculationDifference);
        validate(AllocRejCode.UnknownOrStaleExecID);
        validate(AllocRejCode.MismatchedData);
        validate(AllocRejCode.UnknownClOrdID);
        validate(AllocRejCode.WarehouseRequestRejected);
        validate(AllocRejCode.DuplicateOrMissingIndividualAllocID);
        validate(AllocRejCode.TradeNotRecognized);
        validate(AllocRejCode.DuplicateTrade);
        validate(AllocRejCode.IncorrectOrMissingInstrument);
        validate(AllocRejCode.IncorrectOrMissingSettlDate);
        validate(AllocRejCode.IncorrectOrMissingFundIDOrFundName);
        validate(AllocRejCode.IncorrectOrMissingSettlInstructions);
        validate(AllocRejCode.IncorrectOrMissingFees);
        validate(AllocRejCode.IncorrectOrMissingTax);
        validate(AllocRejCode.UnknownOrMissingParty);
        validate(AllocRejCode.IncorrectOrMissingSide);
        validate(AllocRejCode.IncorrectOrMissingNetMoney);
        validate(AllocRejCode.IncorrectOrMissingTradeDate);
        validate(AllocRejCode.IncorrectOrMissingSettlCcyInstructions);
        validate(AllocRejCode.IncorrectOrMissingProcessCode);
        validate(AllocRejCode.Other);
    });

    test('should be immutable', () => {
        const ref = AllocRejCode;
        expect(() => {
            ref.UnknownAccount = 10;
        }).toThrowError();
    });
});
