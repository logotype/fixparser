import { StreamType } from '../../src/fieldtypes/StreamType';

describe('StreamType', () => {
    test('should have the correct values', () => {
        expect(StreamType.PaymentCashSettlement).toBe(0);
        expect(StreamType.PhysicalDelivery).toBe(1);
    });

    test('should not allow additional properties', () => {
        expect(() => {
            StreamType.NewValue = 5;
        }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [StreamType.PaymentCashSettlement, StreamType.PhysicalDelivery];

        values.forEach((value) => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for StreamType type', () => {
        const validate = (value: StreamType) => {
            expect(Object.values(StreamType)).toContain(value);
        };

        validate(StreamType.PaymentCashSettlement);
        validate(StreamType.PhysicalDelivery);
    });

    test('should be immutable', () => {
        const ref = StreamType;
        expect(() => {
            ref.PaymentCashSettlement = 10;
        }).toThrowError();
    });
});
