import { randomInt } from 'node:crypto';

import { FIXParser } from '../src/FIXParser';
import { EncryptMethod, FIXServer, Field, Fields, type Message, Messages, ResetSeqNumFlag } from '../src/FIXServer';

jest.setTimeout(30000);

const RANDOMIZED_PORT = randomInt(9900, 12000);

describe('FIXServer WebSocket', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });
    it('End-to-end: connect and Logon', (done) => {
        const fixServer: FIXServer = new FIXServer({
            fixVersion: 'FIX.4.6',
            logging: false,
        });
        const fixParser: FIXParser = new FIXParser({
            fixVersion: 'FIX.4.6',
            logging: false,
        });
        const HOST: string = 'localhost';

        const START_MESSAGE_SEQUENCE = 112233;

        // Start up a server
        fixServer.createServer({
            host: HOST,
            port: RANDOMIZED_PORT,
            protocol: 'websocket',
            sender: 'SERVER2',
            target: 'CLIENT2',
            onMessage: (message: Message) => {
                expect(message.description).toEqual('Logon');
                expect(message.messageString).toMatchSnapshot();
                expect(fixServer.heartBeatInterval).toEqual(128);
                expect(fixServer.fixVersion).toEqual('FIX.4.6');
                fixParser.close();
                fixServer.destroy();
            },
            onReady: () => {
                // Connect with a client
                fixParser.connect({
                    host: HOST,
                    port: RANDOMIZED_PORT,
                    protocol: 'websocket',
                    sender: 'CLIENT2',
                    target: 'SERVER2',
                    fixVersion: 'FIX.4.6',
                    logging: false,
                    onMessage: (message: Message) => {
                        // Verify that Logon Acknowledge sequence matches what was sent by the initiator.
                        expect(message.messageString).toMatchSnapshot();
                        expect(message.getField(Fields.MsgSeqNum)?.value).toEqual(START_MESSAGE_SEQUENCE);
                        done();
                    },
                    onOpen: () => {
                        fixParser.setNextTargetMsgSeqNum(START_MESSAGE_SEQUENCE);

                        // Send a Logon message
                        const logon: Message = fixParser.createMessage(
                            new Field(Fields.MsgType, Messages.Logon),
                            new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
                            new Field(Fields.SenderCompID, fixParser.sender),
                            new Field(Fields.SendingTime, fixParser.getTimestamp()),
                            new Field(Fields.TargetCompID, fixParser.target),
                            new Field(Fields.ResetSeqNumFlag, ResetSeqNumFlag.No),
                            new Field(Fields.EncryptMethod, EncryptMethod.None),
                            new Field(Fields.HeartBtInt, 128),
                        );
                        fixParser.send(logon);
                    },
                    onError: (error?: Error) => console.log('FIXParser: ', error),
                });
            },
            onError: (error?: Error) => console.log('FIXServer: ', error),
        });
    });
});
