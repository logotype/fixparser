import { FIXParser, type Message } from '../src/FIXParser';
import { type TestMessage, testMessages } from './test-messages';

describe('FIXParser', () => {
    const fixParser: FIXParser = new FIXParser();

    function processTest(fixMessage: TestMessage) {
        describe(`#parse: ${fixMessage.description}`, () => {
            const messages: Message[] = fixParser.parse(fixMessage.fix);

            it('should have parsed the FIX message', () => {
                expect(messages.length).toEqual(fixMessage.numMessages ? fixMessage.numMessages : 1);
                expect(messages[0].data[0].name).toBe('BeginString');
                expect(messages[0].data[1].name).toBe('BodyLength');
                expect(messages[0].data[2].name).toBe('MsgType');
            });

            it(`should have MsgType ${messages[0].description}`, () => {
                expect(messages[0].description).toBe(fixMessage.description);
            });

            it(`should have valid BodyLength: ${messages[0].bodyLengthValid}`, () => {
                expect(messages[0].bodyLengthValid).toBe(fixMessage.bodyLengthValid);
            });

            it(`should have valid CheckSum: ${messages[0].checksumValid}`, () => {
                expect(messages[0].checksumValid).toBe(fixMessage.checksumValid);
            });

            it('should output .toFIXJSON()', () => {
                expect(messages[0].toFIXJSON()).toMatchSnapshot();
            });
        });
    }

    testMessages.forEach((messages) => {
        processTest(messages);
    });
});
