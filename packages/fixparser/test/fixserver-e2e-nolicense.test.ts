import { randomInt } from 'node:crypto';

import { FIXParser } from '../src/FIXParser';
import { mockLicense } from './setup';

jest.setTimeout(30000);

const RANDOMIZED_PORT = randomInt(9900, 12000);

describe('FIXParser', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });
    it('#connect with no license', () => {
        const fixParser: FIXParser = new FIXParser();

        mockLicense.mockReturnValue(false);

        expect(() => {
            // Attempt to connect with a client
            fixParser.connect({
                host: 'localhost',
                port: RANDOMIZED_PORT,
                protocol: 'tcp',
            });
        }).toThrow('Requires FIXParser Pro license');

        expect(mockLicense).toHaveBeenCalled();
        mockLicense.mockReturnValue(true);
    });
});
