import { randomInt } from 'node:crypto';

import { FIXParser } from '../src/FIXParser';
import { EncryptMethod, FIXServer, Field, Fields, type Message, Messages } from '../src/FIXServer';

jest.setTimeout(30000);

const RANDOMIZED_PORT = randomInt(9900, 12000);

describe('FIXServer WebSocket', () => {
    afterEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });
    it('End-to-end: invalid Logon', (done) => {
        const fixServer: FIXServer = new FIXServer({
            fixVersion: 'FIX.5.0',
            logging: false,
        });
        const fixParser: FIXParser = new FIXParser({
            fixVersion: 'FIX.5.0',
            logging: false,
        });
        const HOST: string = 'localhost';

        // Start up a server
        fixServer.createServer({
            host: HOST,
            port: RANDOMIZED_PORT,
            protocol: 'websocket',
            sender: 'SERVER33',
            target: 'CLIENT33',
            onMessage: (message: Message) => {
                /* message.description can sometimes include "Heartbeat" if the test runs slowly,
                 * as the system may send a heartbeat message during extended processing time. */
                expect(message.description).toMatch(/^(Logout|Logon|Heartbeat)$/);
                expect(message.messageString).toMatchSnapshot();
            },
            onReady: () => {
                // Connect with a client
                fixParser.connect({
                    host: HOST,
                    port: RANDOMIZED_PORT,
                    protocol: 'websocket',
                    sender: 'INVALID_SENDER',
                    target: 'INVALID_TARGET',
                    onOpen: () => {
                        // Send a Logon message
                        const logon: Message = fixParser.createMessage(
                            new Field(Fields.MsgType, Messages.Logon),
                            new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
                            new Field(Fields.SenderCompID, fixParser.sender),
                            new Field(Fields.SendingTime, fixParser.getTimestamp()),
                            new Field(Fields.TargetCompID, fixParser.target),
                            new Field(Fields.ResetSeqNumFlag, 'Y'),
                            new Field(Fields.EncryptMethod, EncryptMethod.None),
                            new Field(Fields.HeartBtInt, 60),
                        );
                        fixParser.send(logon);
                    },
                    onMessage: (message: Message) => {
                        expect(message.description).toEqual('Logout');
                        expect(message.messageString).toMatchSnapshot();
                        fixParser.close();
                        fixServer.destroy();
                        done();
                    },
                    onError: (error?: Error) => {},
                });
            },
            onError: (error?: Error) => {},
        });
    });
});
