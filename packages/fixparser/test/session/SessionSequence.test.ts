import type { IFIXParser } from '../../src/IFIXParser';
import { Field as FieldTypes } from '../../src/fieldtypes/Field';
import { Message as MessageType } from '../../src/fieldtypes/Message';
import type { Message } from '../../src/message/Message';
import { handleSequence } from '../../src/session/SessionSequence';

describe('handleSequence', () => {
    let mockParser: IFIXParser;
    let mockMessage: Message;
    let mockLogWarning: jest.Mock;
    let mockCreateMessage: jest.Mock;
    let mockSend: jest.Mock;

    beforeEach(() => {
        mockParser = {
            protocol: 'tcp',
            nextNumIn: 10,
            connectionType: 'initiator',
            sender: 'SenderCompID',
            target: 'TargetCompID',
            getNextTargetMsgSeqNum: jest.fn(),
            getTimestamp: jest.fn(),
            logger: {
                logWarning: jest.fn(),
                log: jest.fn(),
            },
            createMessage: jest.fn().mockReturnValue({ encode: jest.fn() }),
            send: jest.fn(),
            messageStoreIn: {
                _seqNum: 10,
                getNextMsgSeqNum: jest.fn(function () {
                    return this._seqNum;
                }),
                setNextMsgSeqNum: jest.fn(function (seqNum) {
                    this._seqNum = seqNum;
                    return this._seqNum;
                }),
            },
        } as unknown as IFIXParser;

        mockMessage = {
            messageSequence: 9,
            messageType: MessageType.Logon,
            encode: jest.fn(),
        } as unknown as Message;

        mockLogWarning = mockParser.logger.logWarning;
        mockCreateMessage = mockParser.createMessage;
        mockSend = mockParser.send;
        mockParser.getNextTargetMsgSeqNum.mockReturnValue(11);
        mockParser.getTimestamp.mockReturnValue('2025-01-01T00:00:00Z');
    });

    it('should log a warning and send a ResendRequest when sequence number is incorrect and message is not a Logon/SequenceReset', () => {
        mockMessage.messageSequence = 9;
        mockMessage.messageType = MessageType.Confirmation;

        const mockResendRequest = {
            encode: jest.fn(),
        } as unknown as Message;
        mockCreateMessage.mockReturnValueOnce(mockResendRequest);

        const result = handleSequence(mockParser, mockMessage);

        expect(mockLogWarning).toHaveBeenCalledWith({
            message: 'FIXServer (TCP): Expected MsgSeqNum 10, but got 9',
        });

        expect(mockCreateMessage).toHaveBeenCalledWith(
            expect.objectContaining({ tag: FieldTypes.MsgType }),
            expect.objectContaining({ tag: FieldTypes.MsgSeqNum, value: 11 }),
            expect.objectContaining({ tag: FieldTypes.SenderCompID }),
            expect.objectContaining({ tag: FieldTypes.SendingTime }),
            expect.objectContaining({ tag: FieldTypes.TargetCompID }),
            expect.objectContaining({ tag: FieldTypes.BeginSeqNo, value: 10 }),
            expect.objectContaining({ tag: FieldTypes.EndSeqNo, value: 0 }),
        );

        expect(mockSend).toHaveBeenCalledWith(mockResendRequest);

        expect(result).toBe(false);
    });

    it('should not send a ResendRequest and return true when sequence number is correct', () => {
        mockMessage.messageSequence = 10;

        const result = handleSequence(mockParser, mockMessage);
        expect(mockLogWarning).not.toHaveBeenCalled();
        expect(mockCreateMessage).not.toHaveBeenCalled();
        expect(mockSend).not.toHaveBeenCalled();
        expect(result).toBe(true);
    });

    it('should not send a ResendRequest and return true when connectionType is "acceptor" and messageType is Logon', () => {
        mockParser.connectionType = 'acceptor';
        mockMessage.messageType = MessageType.Logon;
        mockMessage.messageSequence = 9;

        const result = handleSequence(mockParser, mockMessage);
        expect(mockLogWarning).not.toHaveBeenCalled();
        expect(mockCreateMessage).not.toHaveBeenCalled();
        expect(mockSend).not.toHaveBeenCalled();
        expect(result).toBe(true);
    });
});
