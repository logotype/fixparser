import type { IFIXParser } from '../../src/IFIXParser';
import type { Message } from '../../src/message/Message';
import { handleSequenceReset } from '../../src/session/SessionSequenceReset';

describe('handleSequenceReset', () => {
    let mockParser: IFIXParser;
    let mockMessage: Message;
    let mockLoggerLog: jest.Mock;

    beforeEach(() => {
        mockParser = {
            parserName: 'TestParser',
            protocol: 'tcp',
            setNextTargetMsgSeqNum: jest.fn(),
            logger: {
                log: jest.fn(),
            },
            messageStoreIn: {
                _seqNum: 0,
                getNextMsgSeqNum: jest.fn(function () {
                    return this._seqNum;
                }),
                setNextMsgSeqNum: jest.fn(function (seqNum) {
                    this._seqNum = seqNum;
                    return this._seqNum;
                }),
            },
        } as unknown as IFIXParser;

        mockMessage = {
            getField: jest.fn(),
            encode: jest.fn(),
        } as unknown as Message;

        mockLoggerLog = mockParser.logger.log;

        jest.clearAllMocks();
    });

    it('should log the new sequence number and update nextNumIn when parserName is not "FIXServer"', () => {
        const newSeqNo = 1234;
        mockMessage.getField.mockReturnValueOnce({
            value: newSeqNo,
        });

        handleSequenceReset(mockParser, mockMessage);

        expect(mockLoggerLog).toHaveBeenCalledWith({
            level: 'info',
            message: 'TestParser (TCP): -- SequenceReset: new sequence number 1234',
        });

        expect(mockParser.messageStoreIn.getNextMsgSeqNum()).toBe(1233);
    });

    it('should log the new sequence number and update parser.messageStoreIn.getNextMsgSeqNum()', () => {
        mockParser.parserName = 'FIXServer';

        const newSeqNo = 5678;
        mockMessage.getField.mockReturnValueOnce({
            value: newSeqNo,
        });

        handleSequenceReset(mockParser, mockMessage);

        expect(mockLoggerLog).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): -- SequenceReset: new sequence number 5678',
        });

        expect(mockParser.messageStoreIn.getNextMsgSeqNum()).toEqual(newSeqNo - 1);
    });

    it('should not log or update anything when NewSeqNo is not provided', () => {
        mockMessage.getField.mockReturnValueOnce(undefined);

        handleSequenceReset(mockParser, mockMessage);

        expect(mockLoggerLog).not.toHaveBeenCalled();

        expect(mockParser.messageStoreIn.getNextMsgSeqNum()).toBe(0);
        expect(mockParser.setNextTargetMsgSeqNum).not.toHaveBeenCalled();
    });

    it('should not log or update anything when NewSeqNo is invalid', () => {
        mockMessage.getField.mockReturnValueOnce({
            value: Number.NaN,
        });

        handleSequenceReset(mockParser, mockMessage);

        expect(mockLoggerLog).not.toHaveBeenCalled();

        expect(mockParser.messageStoreIn.getNextMsgSeqNum()).toBe(0);
        expect(mockParser.setNextTargetMsgSeqNum).not.toHaveBeenCalled();
    });
});
