import { MessageBuffer } from 'fixparser-common';
import type { IFIXParser } from '../../src/IFIXParser';
import { Field } from '../../src/fields/Field';
import { Field as FieldType } from '../../src/fieldtypes/Field';
import { Message as MessageType } from '../../src/fieldtypes/Message';
import { Message } from '../../src/message/Message';
import { handleResendRequest } from '../../src/session/SessionResendRequest';
import { DEFAULT_FIX_VERSION, timestamp } from '../../src/util/util';

describe('handleResendRequest', () => {
    let mockParser: IFIXParser;
    let mockLogger: jest.Mock;
    let mockLoggerWarning: jest.Mock;
    let mockSend: jest.Mock;

    const createMessage = (seqNum: number, sender: string, target: string): Message => {
        return new Message(
            DEFAULT_FIX_VERSION,
            new Field(FieldType.BeginString, DEFAULT_FIX_VERSION),
            new Field(FieldType.MsgType, MessageType.Heartbeat),
            new Field(FieldType.MsgSeqNum, seqNum),
            new Field(FieldType.SenderCompID, sender),
            new Field(FieldType.TargetCompID, target),
            new Field(FieldType.SendingTime, timestamp(new Date(), { log: jest.fn() })),
        );
    };

    beforeEach(() => {
        mockParser = {
            parserName: 'FIXServer',
            fixVersion: 'FIXT.1.1',
            sender: 'snd',
            target: 'tgt',
            protocol: 'tcp',
            createMessage: (...fields: Field[]) => new Message(DEFAULT_FIX_VERSION, ...fields),
            setNextTargetMsgSeqNum: jest.fn(),
            getNextTargetMsgSeqNum: jest.fn(),
            send: jest.fn(),
            logger: {
                log: jest.fn(),
                logWarning: jest.fn(),
            },
            getTimestamp: jest.fn().mockReturnValue('2025-01-01T00:00:00Z'),
        } as unknown as IFIXParser;

        mockLogger = mockParser.logger.log;
        mockLoggerWarning = mockParser.logger.logWarning;
        mockSend = mockParser.send;
    });

    it('should resend messages in the specified range', () => {
        const resendMessage: Message = new Message(
            DEFAULT_FIX_VERSION,
            new Field(FieldType.BeginSeqNo, 2),
            new Field(FieldType.EndSeqNo, 7),
        );

        const SENDER = 'snd';
        const TARGET = 'tgt';

        const mb = new MessageBuffer<Message>();
        mb.add(createMessage(0, SENDER, TARGET));
        mb.add(createMessage(1, SENDER, TARGET));
        mb.add(createMessage(2, SENDER, TARGET));
        mb.add(createMessage(3, SENDER, TARGET));
        mb.add(createMessage(4, SENDER, TARGET));
        mb.add(createMessage(5, SENDER, TARGET));
        mb.add(createMessage(6, SENDER, TARGET));
        mb.add(createMessage(7, SENDER, TARGET));
        mb.add(createMessage(8, SENDER, TARGET));
        mb.add(createMessage(9, SENDER, TARGET));

        expect(mb.size()).toEqual(10);
        handleResendRequest(mockParser, mb, resendMessage);
        expect(mockSend).toHaveBeenCalledTimes(5);

        expect(mockLogger.mock.calls[0]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=2|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=3|10=019|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 2 (replaced administrative message)',
            },
        ]);
        expect(mockLogger.mock.calls[1]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=3|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=4|10=021|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 3 (replaced administrative message)',
            },
        ]);
        expect(mockLogger.mock.calls[2]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=4|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=5|10=023|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 4 (replaced administrative message)',
            },
        ]);
        expect(mockLogger.mock.calls[3]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=5|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=6|10=025|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 5 (replaced administrative message)',
            },
        ]);
        expect(mockLogger.mock.calls[4]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=6|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=7|10=027|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 6 (replaced administrative message)',
            },
        ]);
        expect(mockLogger.mock.calls[5]).toEqual([
            {
                level: 'info',
                message: 'FIXServer (TCP): >> sent message sequence from 2 to 7',
            },
        ]);
    });

    it('should resend a single message', () => {
        const resendMessage: Message = new Message(
            DEFAULT_FIX_VERSION,
            new Field(FieldType.BeginSeqNo, 4),
            new Field(FieldType.EndSeqNo, 4),
        );

        const SENDER = 'snd';
        const TARGET = 'tgt';

        const mb = new MessageBuffer<Message>();
        mb.add(createMessage(0, SENDER, TARGET));
        mb.add(createMessage(1, SENDER, TARGET));
        mb.add(createMessage(2, SENDER, TARGET));
        mb.add(createMessage(3, SENDER, TARGET));
        mb.add(createMessage(4, SENDER, TARGET));
        mb.add(createMessage(5, SENDER, TARGET));
        mb.add(createMessage(6, SENDER, TARGET));
        mb.add(createMessage(7, SENDER, TARGET));
        mb.add(createMessage(8, SENDER, TARGET));
        mb.add(createMessage(9, SENDER, TARGET));

        expect(mb.size()).toEqual(10);
        handleResendRequest(mockParser, mb, resendMessage);
        expect(mockSend).toHaveBeenCalledTimes(1);

        expect(mockLogger.mock.calls[0]).toEqual([
            {
                fix: '8=FIXT.1.1|9=59|35=4|34=4|49=snd|56=tgt|52=2025-01-01T00:00:00Z|123=Y|36=5|10=023|',
                level: 'info',
                message: 'FIXServer (TCP): >> resending message with sequence 4 (replaced administrative message)',
            },
        ]);
    });

    it('should log a warning if message with specified sequence number is not found', () => {
        const resendMessage: Message = new Message(
            DEFAULT_FIX_VERSION,
            new Field(FieldType.BeginSeqNo, 1399),
            new Field(FieldType.EndSeqNo, 1405),
        );

        const SENDER = 'the_sender';
        const TARGET = 'the_target';

        const mb = new MessageBuffer<Message>();
        mb.add(createMessage(0, SENDER, TARGET));
        mb.add(createMessage(1, SENDER, TARGET));
        mb.add(createMessage(2, SENDER, TARGET));

        expect(mb.size()).toEqual(3);
        handleResendRequest(mockParser, mb, resendMessage);
        expect(mockLoggerWarning).toHaveBeenCalledWith({
            message:
                'FIXServer (TCP): -- Could not find message with sequence 1399. Sending SequenceReset-GapFill from 1399 to 1405...',
        });
    });

    it('should not resend messages if BeginSeqNo or EndSeqNo is out of range', () => {
        const resendMessage: Message = new Message(
            DEFAULT_FIX_VERSION,
            new Field(FieldType.BeginSeqNo, 18400),
            new Field(FieldType.EndSeqNo, 123),
        );

        const SENDER = 'the_sender';
        const TARGET = 'the_target';

        const mb = new MessageBuffer<Message>();
        mb.add(createMessage(0, SENDER, TARGET));
        mb.add(createMessage(1, SENDER, TARGET));
        mb.add(createMessage(2, SENDER, TARGET));

        expect(mb.size()).toEqual(3);
        handleResendRequest(mockParser, mb, resendMessage);
        expect(mockLoggerWarning).toHaveBeenCalledWith({
            message: 'FIXServer (TCP): -- BeginSeqNo<18400> or EndSeqNo<123> out of range',
        });
        expect(mockSend).not.toHaveBeenCalled();
    });

    it('should ignore resend messages when BeginSeqNo is set but EndSeqNo is missing', () => {
        const resendMessage: Message = new Message(DEFAULT_FIX_VERSION, new Field(FieldType.BeginSeqNo, 6));

        const SENDER = 'snd';
        const TARGET = 'tgt';

        const mb = new MessageBuffer<Message>();
        mb.add(createMessage(0, SENDER, TARGET));
        mb.add(createMessage(1, SENDER, TARGET));
        mb.add(createMessage(2, SENDER, TARGET));
        mb.add(createMessage(3, SENDER, TARGET));
        mb.add(createMessage(4, SENDER, TARGET));
        mb.add(createMessage(5, SENDER, TARGET));
        mb.add(createMessage(6, SENDER, TARGET));
        mb.add(createMessage(7, SENDER, TARGET));
        mb.add(createMessage(8, SENDER, TARGET));
        mb.add(createMessage(9, SENDER, TARGET));

        expect(mb.size()).toEqual(10);
        handleResendRequest(mockParser, mb, resendMessage);
        expect(mockSend).toHaveBeenCalledTimes(0);
    });
});
