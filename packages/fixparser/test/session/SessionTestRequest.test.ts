import type { IFIXParser } from '../../src/IFIXParser';
import { Field } from '../../src/fields/Field';
import { Field as FieldType } from '../../src/fieldtypes/Field';
import type { Message } from '../../src/message/Message';
import { heartBeat } from '../../src/messagetemplates/MessageTemplates';
import { handleTestRequest } from '../../src/session/SessionTestRequest';

jest.mock('../../src/fields/Field');
jest.mock('../../src/fieldtypes/Field');
jest.mock('../../src/message/Message');
jest.mock('../../src/messagetemplates/MessageTemplates');

describe('handleTestRequest', () => {
    let mockParser: IFIXParser;
    let mockMessage: Message;
    let mockSend: jest.Mock;
    let mockLog: jest.Mock;

    beforeEach(() => {
        mockParser = {
            parserName: 'TestParser',
            protocol: 'tcp',
            send: jest.fn(),
            logger: {
                log: jest.fn(),
            },
        } as unknown as IFIXParser;

        mockMessage = {
            getField: jest.fn(),
            encode: jest.fn(),
        } as unknown as Message;

        mockSend = mockParser.send;
        mockLog = mockParser.logger.log;

        jest.clearAllMocks();
    });

    it('should respond with Heartbeat and log the correct message when TestReqID is not provided', () => {
        mockMessage.getField.mockReturnValue(undefined);

        const mockHeartBeat = heartBeat as jest.Mock;
        mockHeartBeat.mockReturnValue(mockMessage);
        handleTestRequest(mockParser, mockMessage);
        expect(mockSend).toHaveBeenCalledWith(mockMessage);
        expect(mockLog).toHaveBeenCalledWith({
            level: 'info',
            message: 'TestParser (TCP): >> responded to TestRequest with Heartbeat',
        });
    });

    it('should respond with Heartbeat and log the correct message when TestReqID is provided', () => {
        const testReqId = '12345';
        mockMessage.getField.mockReturnValue({
            value: testReqId,
        });

        const mockHeartBeat = heartBeat as jest.Mock;
        mockHeartBeat.mockReturnValue(mockMessage);
        handleTestRequest(mockParser, mockMessage);
        expect(mockSend).toHaveBeenCalledWith(mockMessage);
        expect(mockLog).toHaveBeenCalledWith({
            level: 'info',
            message: 'TestParser (TCP): >> responded to TestRequest with Heartbeat<TestReqID=12345>',
        });
    });

    it('should create a Field with TestReqID and pass it to heartBeat', () => {
        const testReqId = '12345';
        mockMessage.getField.mockReturnValue({
            value: testReqId,
        });

        const FieldMock = Field as jest.Mock;
        FieldMock.mockImplementationOnce((type, value) => new Field(type, value));

        const mockHeartBeat = heartBeat as jest.Mock;
        mockHeartBeat.mockReturnValue(mockMessage);
        handleTestRequest(mockParser, mockMessage);
        expect(FieldMock).toHaveBeenCalledWith(FieldType.TestReqID, testReqId);
        expect(mockHeartBeat).toHaveBeenCalledWith(mockParser, expect.any(Field));
    });
});
