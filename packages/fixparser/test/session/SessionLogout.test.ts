import type { IFIXParser } from '../../src/IFIXParser';
import { Field } from '../../src/fields/Field';
import { Field as FieldType } from '../../src/fieldtypes/Field';
import type { Message } from '../../src/message/Message';
import { handleLogout } from '../../src/session/SessionLogout';

jest.mock('../../src/fields/Field');
jest.mock('../../src/fieldtypes/Field');
jest.mock('../../src/fieldtypes/Message');
jest.mock('../../src/IFIXParser');
jest.mock('../../src/message/Message');

describe('handleLogout', () => {
    let parser: IFIXParser;
    let message: Message;

    beforeEach(() => {
        parser = {
            isLoggedIn: true,
            requestedLogout: false,
            createMessage: jest.fn().mockReturnValue({ encode: jest.fn() }),
            getNextTargetMsgSeqNum: jest.fn().mockReturnValue(123),
            getTimestamp: jest.fn().mockReturnValue('2025-01-19T00:00:00Z'),
            send: jest.fn(),
            stopHeartbeat: jest.fn(),
            close: jest.fn(),
            logger: { log: jest.fn() },
            sender: 'senderCompID',
            target: 'targetCompID',
            protocol: 'tcp',
        };

        message = {
            getField: jest.fn().mockImplementation((fieldType) => {
                if (fieldType === FieldType.SenderCompID) {
                    return { value: 'senderCompID' };
                }
                if (fieldType === FieldType.TargetCompID) {
                    return { value: 'targetCompID' };
                }
                return null;
            }),
            encode: jest.fn(),
        } as unknown as Message;
    });

    it('should set isLoggedIn to false', () => {
        handleLogout(parser, message);
        expect(parser.isLoggedIn).toBe(false);
    });

    it('should create and send a logout acknowledge message', () => {
        handleLogout(parser, message);
        expect(parser.createMessage).toHaveBeenCalledTimes(1);
        expect(parser.createMessage).toHaveBeenCalledWith(
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
        );
        expect(parser.send).toHaveBeenCalledTimes(1);
    });

    it('should log the correct message when sending logout acknowledge', () => {
        handleLogout(parser, message);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): >> sent Logout acknowledge',
        });
    });

    it('should call stopHeartbeat and close', () => {
        handleLogout(parser, message);
        expect(parser.stopHeartbeat).toHaveBeenCalledTimes(1);
        expect(parser.close).toHaveBeenCalledTimes(1);
    });

    it('should not create and send a logout acknowledge message if requestedLogout is true', () => {
        parser.requestedLogout = true;
        handleLogout(parser, message);
        expect(parser.createMessage).not.toHaveBeenCalled();
        expect(parser.send).not.toHaveBeenCalled();
    });
});
