import type { IMessageStore, MessageBuffer } from 'fixparser-common';
import type { IFIXParser } from '../../src/IFIXParser';
import { Field as FieldType } from '../../src/fieldtypes/Field';
import type { Message } from '../../src/message/Message';
import { handleLogon } from '../../src/session/SessionLogon';

jest.mock('../../src/fields/Field');
jest.mock('../../src/fieldtypes/EncryptMethod');
jest.mock('../../src/fieldtypes/Field');
jest.mock('../../src/fieldtypes/Message');
jest.mock('../../src/IFIXParser');
jest.mock('../../src/message/Message');
jest.mock('../../../fixparser-common/src/MessageBuffer');

describe('handleLogon', () => {
    let parser: IFIXParser;
    let messageStore: IMessageStore<Message>;
    let message: Message;

    beforeEach(() => {
        parser = {
            isLoggedIn: false,
            connectionType: 'initiator',
            protocol: 'tcp',
            sender: 'INITIATOR',
            target: 'client1',
            fixVersion: '',
            heartBeatInterval: 30,
            createMessage: jest.fn().mockReturnValue({ encode: jest.fn() }),
            getNextTargetMsgSeqNum: jest.fn().mockReturnValue(123),
            getTimestamp: jest.fn().mockReturnValue('2025-01-19T00:00:00Z'),
            send: jest.fn(),
            logger: { log: jest.fn(), logWarning: jest.fn() },
            setNextTargetMsgSeqNum: jest.fn(),
            startHeartbeat: jest.fn(),
            stopHeartbeat: jest.fn(),
            close: jest.fn(),
            fixParser: { fixVersion: '', heartBeatInterval: 30 },
            messageStoreIn: {
                _seqNum: 1,
                getNextMsgSeqNum: jest.fn(function () {
                    return this._seqNum;
                }),
                setNextMsgSeqNum: jest.fn(function (seqNum) {
                    this._seqNum = seqNum;
                    return this._seqNum;
                }),
            },
        };

        messageStore = {} as MessageBuffer;

        message = {
            getField: jest.fn().mockImplementation((fieldType) => {
                if (fieldType === FieldType.BeginString) {
                    return { value: 'FIX.4.2' };
                }
                if (fieldType === FieldType.SenderCompID) {
                    return { value: 'client1' };
                }
                if (fieldType === FieldType.TargetCompID) {
                    return { value: 'INITIATOR' };
                }
                if (fieldType === FieldType.ResetSeqNumFlag) {
                    return { value: 'Y' };
                }
                if (fieldType === FieldType.HeartBtInt) {
                    return { value: 60 };
                }
                return null;
            }),
            encode: jest.fn(),
        } as unknown as Message;
    });

    it('should log and acknowledge logon when already logged in as acceptor', () => {
        parser.isLoggedIn = true;
        parser.connectionType = 'acceptor';
        handleLogon(parser, messageStore, message);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): -- Logon acknowledged by acceptor.',
        });
        expect(parser.messageStoreIn.getNextMsgSeqNum()).toBe(1);
    });

    it('should log and acknowledge logon when already logged in as initiator', () => {
        parser.isLoggedIn = true;
        handleLogon(parser, messageStore, message);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): -- Logon acknowledged by initiator.',
        });
    });

    it('should successfully process logon as initiator for valid logon', () => {
        handleLogon(parser, messageStore, message);
        expect(parser.isLoggedIn).toBe(true);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): >> Logon successful by initiator',
        });
        expect(parser.send).toHaveBeenCalledTimes(0);
        expect(parser.createMessage).toHaveBeenCalledTimes(0);
        expect(parser.startHeartbeat).toHaveBeenCalledWith(60);
    });

    it('should successfully process logon as initiator for valid logon', () => {
        parser.connectionType = 'acceptor';
        handleLogon(parser, messageStore, message);
        expect(parser.isLoggedIn).toBe(true);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): >> sent Logon acknowledge',
        });
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): >> Logon successful by acceptor',
        });
        expect(parser.send).toHaveBeenCalledTimes(1);
        expect(parser.createMessage).toHaveBeenCalled();
        expect(parser.startHeartbeat).toHaveBeenCalledWith(60);
    });

    it('should reject logon and send logout for invalid sender or target', () => {
        message.getField.mockImplementation((fieldType) => {
            if (fieldType === FieldType.SenderCompID) {
                return { value: 'invalidSender' };
            }
            return null;
        });

        const result = handleLogon(parser, messageStore, message);
        expect(result).toBe(false);
        expect(parser.isLoggedIn).toBe(false);
        expect(parser.send).toHaveBeenCalledTimes(1);
        expect(parser.logger.logWarning).toHaveBeenCalledWith({
            message: 'FIXServer (TCP): -- Expected SenderCompID=client1, but got invalidSender',
        });
        expect(parser.logger.logWarning).toHaveBeenCalledWith({
            message: 'FIXServer (TCP): >> sent Logout due to invalid Logon',
        });
        expect(parser.stopHeartbeat).toHaveBeenCalledTimes(1);
        expect(parser.close).toHaveBeenCalledTimes(1);
    });

    it('should reset sequence numbers and send acknowledgment if ResetSeqNumFlag is Y', () => {
        handleLogon(parser, messageStore, message);
        expect(parser.messageStoreIn.getNextMsgSeqNum()).toBe(1);
        expect(parser.setNextTargetMsgSeqNum).toHaveBeenCalledWith(2);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): -- Logon contains ResetSeqNumFlag=Y, resetting sequence numbers to 1',
        });
    });

    it('should log and process the logon message with FIX version', () => {
        handleLogon(parser, messageStore, message);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): -- FIX version set to FIX.4.2',
        });
    });
});
