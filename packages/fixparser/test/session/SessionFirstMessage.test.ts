import type { IFIXParser } from '../../src/IFIXParser';
import { Field } from '../../src/fields/Field';
import { Field as FieldType } from '../../src/fieldtypes/Field';
import { Message as MessageType } from '../../src/fieldtypes/Message';
import type { Message } from '../../src/message/Message';
import { handleFirstMessage } from '../../src/session/SessionFirstMessage';

jest.mock('../../src/fields/Field');
jest.mock('../../src/fieldtypes/Field');
jest.mock('../../src/fieldtypes/Message');
jest.mock('../../src/IFIXParser');
jest.mock('../../src/message/Message');

describe('handleFirstMessage', () => {
    let parser: IFIXParser;
    let message: Message;

    beforeEach(() => {
        parser = {
            sender: 'senderCompID',
            target: 'targetCompID',
            protocol: 'tcp',
            createMessage: jest.fn().mockReturnValue({ encode: jest.fn() }),
            getNextTargetMsgSeqNum: jest.fn().mockReturnValue(123),
            getTimestamp: jest.fn().mockReturnValue('2025-01-19T00:00:00Z'),
            send: jest.fn(),
            logger: { log: jest.fn() },
            stopHeartbeat: jest.fn(),
            close: jest.fn(),
        };

        message = {
            getField: jest.fn().mockImplementation((fieldType) => {
                if (fieldType === FieldType.MsgType) {
                    return { value: MessageType.Logon };
                }
                if (fieldType === FieldType.SenderCompID) {
                    return { value: 'senderCompID' };
                }
                if (fieldType === FieldType.TargetCompID) {
                    return { value: 'targetCompID' };
                }
                return null;
            }),
            encode: jest.fn(),
        } as unknown as Message;
    });

    it('should return true if message is a Logon', () => {
        const result = handleFirstMessage(parser, message);
        expect(result).toBe(true);
    });

    it('should send Logout and return false if message is not a Logon', () => {
        message.getField.mockImplementationOnce((fieldType) => {
            if (fieldType === FieldType.MsgType) {
                return { value: MessageType.Logout };
            }
            return null;
        });

        const result = handleFirstMessage(parser, message);
        expect(result).toBe(false);
        expect(parser.send).toHaveBeenCalledTimes(1);
        expect(parser.logger.log).toHaveBeenCalledWith({
            level: 'info',
            message: 'FIXServer (TCP): >> sent Logout',
        });
        expect(parser.stopHeartbeat).toHaveBeenCalledTimes(1);
        expect(parser.close).toHaveBeenCalledTimes(1);
    });

    it('should create the correct Logout message if the MsgType is not Logon', () => {
        message.getField.mockImplementationOnce((fieldType) => {
            if (fieldType === FieldType.MsgType) {
                return { value: MessageType.Logout };
            }
            return null;
        });

        handleFirstMessage(parser, message);
        expect(parser.createMessage).toHaveBeenCalledWith(
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
            expect.any(Field),
        );
    });
});
