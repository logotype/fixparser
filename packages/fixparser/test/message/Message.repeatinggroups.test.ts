import { FIXParser, Fields, MDEntryType, MDUpdateType, SubscriptionRequestType } from '../../src/FIXParser';
import { Field } from '../../src/fields/Field';
import { Message as MessageType } from '../../src/fieldtypes/Message';
import { Message } from '../../src/message/Message';

describe('Message', () => {
    const fixVersion: string = 'FIX.4.4';

    const fixParser = new FIXParser({
        fixVersion,
    });
    const fields: Field[] = [
        new Field(Fields.BeginString, fixVersion),
        new Field(Fields.MsgType, MessageType.MarketDataRequest),
        new Field(Fields.MsgSeqNum, 1),
        new Field(Fields.SenderCompID, 'SENDER'),
        new Field(Fields.TargetCompID, 'TARGET'),
        new Field(Fields.SendingTime, '20090323-15:40:29'),
        new Field(Fields.MDReqID, 1),
        new Field(Fields.SubscriptionRequestType, SubscriptionRequestType.SnapshotAndUpdates),
        new Field(Fields.MarketDepth, 0),
        new Field(Fields.MDUpdateType, MDUpdateType.FullRefresh),

        new Field(Fields.NoMDEntries, 5),
        /* */ new Field(Fields.MDEntryType, MDEntryType.Bid),
        /* */ new Field(Fields.MDEntryType, MDEntryType.Offer),
        /* */ new Field(Fields.MDEntryType, MDEntryType.MarketBid),
        /* */ new Field(Fields.MDEntryType, MDEntryType.MarketOffer),
        /* */ new Field(Fields.MDEntryType, MDEntryType.TradeVolume),

        new Field(Fields.NoRelatedSym, 10),
        /* */ new Field(Fields.Symbol, 'A'),
        /* */ new Field(Fields.Symbol, 'B'),
        /* */ new Field(Fields.Symbol, 'C'),
        /* */ new Field(Fields.Symbol, 'D'),
        /* */ new Field(Fields.Symbol, 'E'),
        /* */ new Field(Fields.Symbol, 1),
        /* */ new Field(Fields.Symbol, 2),
        /* */ new Field(Fields.Symbol, 3),
        /* */ new Field(Fields.Symbol, 4),
        /* */ new Field(Fields.Symbol, 5),
    ];
    const newMessage: Message = new Message(fixVersion, ...fields);
    const encoded: string = newMessage.encode();
    const message: Message = fixParser.parse(encoded)[0];

    it('#getBriefDescription()', () => {
        expect(message.getBriefDescription()).toEqual('MarketDataRequest');
    });

    it('#getField()', () => {
        expect(message.getField(Fields.MsgSeqNum)!.tag).toEqual(fields[2].tag);
        expect(message.getField(Fields.MsgSeqNum)!.value).toEqual(fields[2].value);
    });

    it('#getFields()', () => {
        expect(message.getFields(Fields.MsgSeqNum)!.length).toEqual(1);
        expect(message.getFields(Fields.MsgSeqNum)![0].tag).toEqual(fields[2].tag);
        expect(message.getFields(Fields.MsgSeqNum)![0].value).toEqual(fields[2].value);
    });

    it('#getFieldValues()', () => {
        expect(message.getFieldValues()).toEqual({
            8: 'FIX.4.4',
            10: '009',
            34: 1,
            35: 'V',
            49: 'SENDER',
            56: 'TARGET',
            52: '20090323-15:40:29',
            264: 0,
            265: '0',
            146: 10,
            262: '1',
            263: '1',
            268: 5,
            269: ['0', '1', 'b', 'c', 'B'],
            55: ['A', 'B', 'C', 'D', 'E', '1', '2', '3', '4', '5'],
            9: 168,
        });
    });

    it('#getFieldNameValues()', () => {
        expect(message.getFieldNameValues()).toEqual({
            BeginString: 'FIX.4.4',
            BodyLength: 168,
            CheckSum: '009',
            MsgSeqNum: 1,
            MsgType: 'V',
            SenderCompID: 'SENDER',
            SendingTime: '20090323-15:40:29',
            TargetCompID: 'TARGET',
            SubscriptionRequestType: '1',
            MarketDepth: 0,
            MDReqID: '1',
            MDUpdateType: '0',
            NoMDEntries: 5,
            MDEntryType: ['0', '1', 'b', 'c', 'B'],
            NoRelatedSym: 10,
            Symbol: ['A', 'B', 'C', 'D', 'E', '1', '2', '3', '4', '5'],
        });
    });

    it('#getFieldExplains()', () => {
        expect(message.getFieldExplains()).toEqual({
            BeginString: 'FIX44',
            BodyLength: 168,
            CheckSum: '009',
            MsgSeqNum: 1,
            MsgType: 'MarketDataRequest',
            SenderCompID: 'SENDER',
            SendingTime: '20090323-15:40:29',
            TargetCompID: 'TARGET',
            SubscriptionRequestType: 'SnapshotAndUpdates',
            MarketDepth: 0,
            MDReqID: '1',
            MDUpdateType: 'FullRefresh',
            NoMDEntries: 5,
            MDEntryType: ['Bid', 'Offer', 'MarketBid', 'MarketOffer', 'TradeVolume'],
            NoRelatedSym: 10,
            Symbol: ['A', 'B', 'C', 'D', 'E', '1', '2', '3', '4', '5'],
        });
    });

    it('#toFIXJSON() - multiple groups', () => {
        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIX44',
                MsgType: 'MarketDataRequest',
                BodyLength: 168,
                MsgSeqNum: 1,
                SenderCompID: 'SENDER',
                TargetCompID: 'TARGET',
                SendingTime: '20090323-15:40:29',
            },
            Body: {
                MDReqID: '1',
                MDUpdateType: 'FullRefresh',
                MarketDepth: 0,
                NoMDEntries: [
                    { MDEntryType: 'Bid' },
                    { MDEntryType: 'Offer' },
                    { MDEntryType: 'MarketBid' },
                    { MDEntryType: 'MarketOffer' },
                    { MDEntryType: 'TradeVolume' },
                ],
                NoRelatedSym: [
                    { Symbol: 'A' },
                    { Symbol: 'B' },
                    { Symbol: 'C' },
                    { Symbol: 'D' },
                    { Symbol: 'E' },
                    { Symbol: '1' },
                    { Symbol: '2' },
                    { Symbol: '3' },
                    { Symbol: '4' },
                    { Symbol: '5' },
                ],
                SubscriptionRequestType: 'SnapshotAndUpdates',
            },
            Trailer: {
                CheckSum: '009',
            },
        });
    });

    it('#toFIXJSON() - group with single instance', () => {
        const fields: Field[] = [
            // Header
            new Field(Fields.BeginString, fixVersion),
            new Field(Fields.MsgType, MessageType.MarketDataRequest),
            new Field(Fields.MsgSeqNum, 1),
            new Field(Fields.SenderCompID, 'SENDER'),
            new Field(Fields.TargetCompID, 'TARGET'),
            new Field(Fields.SendingTime, '20090323-15:40:29'),

            // Body
            new Field(Fields.MarketDepth, 0),
            new Field(Fields.MDUpdateType, MDUpdateType.FullRefresh),
            new Field(Fields.SubscriptionRequestType, 1),

            // Repeating Group
            new Field(Fields.NoMDEntryTypes, 1),
            new Field(Fields.MDEntryType, MDEntryType.TradeVolume),

            // Body
            new Field(Fields.MDReqID, 1),
        ];
        const newMessage: Message = new Message(fixVersion, ...fields);
        const encoded: string = newMessage.encode();
        const message: Message = fixParser.parse(encoded)[0];

        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIX44',
                MsgType: 'MarketDataRequest',
                BodyLength: 87,
                MsgSeqNum: 1,
                SenderCompID: 'SENDER',
                TargetCompID: 'TARGET',
                SendingTime: '20090323-15:40:29',
            },
            Body: {
                MarketDepth: 0,
                MDUpdateType: 'FullRefresh',
                SubscriptionRequestType: 'SnapshotAndUpdates',
                NoMDEntryTypes: [{ MDEntryType: 'TradeVolume' }],
                MDReqID: '1',
            },
            Trailer: {
                CheckSum: '026',
            },
        });
    });

    it('#toFIXJSON() - varied instance fields', () => {
        const fields: Field[] = [
            new Field(Fields.BeginString, 'FIXT.1.1'),
            new Field(Fields.MsgType, MessageType.MarketDataSnapshotFullRefresh),
            new Field(Fields.MsgSeqNum, 4567),
            new Field(Fields.SenderCompID, 'SENDER'),
            new Field(Fields.TargetCompID, 'TARGET'),
            new Field(Fields.SendingTime, '20160802-21:14:38.717'),

            new Field(Fields.SecurityIDSource, '8'),
            new Field(Fields.SecurityID, 'ESU6'),
            new Field(Fields.MDReqID, '789'),

            new Field(Fields.NoMDEntries, 2),

            new Field(Fields.MDEntryType, MDEntryType.Bid),
            new Field(Fields.MDEntryPx, '2179.75'),
            new Field(Fields.MDEntryTime, '21:14:38.688'),

            new Field(Fields.MDEntryType, MDEntryType.Offer),
            new Field(Fields.MDEntryPx, '2180.25'),
            new Field(Fields.MDEntrySize, '125'),
            new Field(Fields.MDEntryDate, '20160812'),
            new Field(Fields.MDEntryTime, '21:15:12.332'),
        ];
        const newMessage: Message = new Message(fixVersion, ...fields);
        const encoded: string = newMessage.encode();
        const message: Message = fixParser.parse(encoded)[0];

        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIXT11',
                MsgType: 'MarketDataSnapshotFullRefresh',
                BodyLength: 176,
                MsgSeqNum: 4567,
                SenderCompID: 'SENDER',
                TargetCompID: 'TARGET',
                SendingTime: '20160802-21:14:38.717',
            },
            Body: {
                SecurityIDSource: 'ExchangeSymbol',
                SecurityID: 'ESU6',
                MDReqID: '789',
                NoMDEntries: [
                    {
                        MDEntryType: 'Bid',
                        MDEntryPx: 2179.75,
                        MDEntryTime: '21:14:38.688',
                    },
                    {
                        MDEntryType: 'Offer',
                        MDEntryPx: 2180.25,
                        MDEntrySize: 125,
                        MDEntryDate: '20160812',
                        MDEntryTime: '21:15:12.332',
                    },
                ],
            },
            Trailer: {
                CheckSum: '244',
            },
        });
    });
    it('#toFIXJSON()', () => {
        const fields: Field[] = [
            new Field(Fields.BeginString, 'FIXT.1.1'),
            new Field(Fields.MsgType, MessageType.MarketDataSnapshotFullRefresh),
            new Field(Fields.MsgSeqNum, 4567),
            new Field(Fields.SenderCompID, 'SENDER'),
            new Field(Fields.TargetCompID, 'TARGET'),
            new Field(Fields.SendingTime, '20160802-21:14:38.717'),

            new Field(Fields.SecurityIDSource, '8'),
            new Field(Fields.SecurityID, 'ESU6'),
            new Field(Fields.MDReqID, '789'),

            new Field(Fields.NoMDEntries, 2),

            new Field(Fields.MDEntryType, MDEntryType.Bid),
            new Field(Fields.MDEntryPx, '2179.75'),
            new Field(Fields.MDEntrySize, '175'),
            new Field(Fields.MDEntryDate, '20160812'),
            new Field(Fields.MDEntryTime, '21:14:38.688'),

            new Field(Fields.MDEntryType, MDEntryType.Offer),
            new Field(Fields.MDEntryPx, '2180.25'),
            new Field(Fields.MDEntrySize, '125'),
            new Field(Fields.MDEntryDate, '20160812'),
            new Field(Fields.MDEntryTime, '21:14:38.688'),
        ];
        const newMessage: Message = new Message(fixVersion, ...fields);
        const encoded: string = newMessage.encode();
        const message: Message = fixParser.parse(encoded)[0];

        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIXT11',
                MsgType: 'MarketDataSnapshotFullRefresh',
                BodyLength: 197,
                MsgSeqNum: 4567,
                SenderCompID: 'SENDER',
                TargetCompID: 'TARGET',
                SendingTime: '20160802-21:14:38.717',
            },
            Body: {
                SecurityIDSource: 'ExchangeSymbol',
                SecurityID: 'ESU6',
                MDReqID: '789',
                NoMDEntries: [
                    {
                        MDEntryType: 'Bid',
                        MDEntryPx: 2179.75,
                        MDEntrySize: 175,
                        MDEntryDate: '20160812',
                        MDEntryTime: '21:14:38.688',
                    },
                    {
                        MDEntryType: 'Offer',
                        MDEntryPx: 2180.25,
                        MDEntrySize: 125,
                        MDEntryDate: '20160812',
                        MDEntryTime: '21:14:38.688',
                    },
                ],
            },
            Trailer: {
                CheckSum: '238',
            },
        });
    });

    it('#toFIXJSON() - nested groups, varied instance fields', () => {
        const fields: Field[] = [
            new Field(Fields.BeginString, 'FIXT.1.1'),
            new Field(Fields.MsgType, MessageType.MarketDataSnapshotFullRefresh),
            new Field(Fields.MsgSeqNum, 4567),
            new Field(Fields.SenderCompID, 'SENDER'),
            new Field(Fields.TargetCompID, 'TARGET'),
            new Field(Fields.SendingTime, '20160802-21:14:38.717'),

            new Field(Fields.NoPartyIDs, 3),

            /* */ new Field(Fields.PartyID, 'DEU'),
            /* */ new Field(Fields.PartyIDSource, 'B'), // (Bank Identifier Code (BIC) ISO 9362)
            /* */ new Field(Fields.PartyRole, 1), // (Executing Firm)
            /* */ new Field(Fields.NoPartySubIDs, 1),
            /* */ /* */ new Field(Fields.PartySubID, 'A1'),
            /* */ /* */ new Field(Fields.PartySubIDType, 10), // (Securities account number)

            /* */ new Field(Fields.PartyID, 104317),
            /* */ new Field(Fields.PartyIDSource, 'H'), // (CSD Participant Number)
            /* */ new Field(Fields.PartyRole, 83), // (Clearing Account)

            /* */ new Field(Fields.PartyID, 'GSI'),
            /* */ new Field(Fields.PartyIDSource, 'B'), // (Bank Identifier Code (BIC) ISO 9362)
            /* */ new Field(Fields.PartyRole, 4), // (Clearing Firm)
            /* */ new Field(Fields.PartyRoleQualifier, 23), //  (Firm or legal entity)
            /* */ new Field(Fields.NoPartySubIDs, 1),
            /* */ /* */ new Field(Fields.PartySubID, 'C3'),
            /* */ /* */ new Field(Fields.PartySubIDType, 10), // (Securities account number)
        ];
        const newMessage: Message = new Message(fixVersion, ...fields);
        const encoded: string = newMessage.encode('|');
        const message: Message = fixParser.parse(encoded)[0];

        expect(encoded).toEqual(
            '8=FIXT.1.1|9=176|35=W|34=4567|49=SENDER|56=TARGET|52=20160802-21:14:38.717|453=3|448=DEU|447=B|452=1|802=1|523=A1|803=10|448=104317|447=H|452=83|448=GSI|447=B|452=4|2376=23|802=1|523=C3|803=10|10=034|',
        );

        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIXT11',
                BodyLength: 176,
                MsgSeqNum: 4567,
                MsgType: 'MarketDataSnapshotFullRefresh',
                SenderCompID: 'SENDER',
                SendingTime: '20160802-21:14:38.717',
                TargetCompID: 'TARGET',
            },
            Body: {
                NoPartyIDs: [
                    {
                        PartyID: 'DEU',
                        PartyIDSource: 'BIC',
                        PartyRole: 'ExecutingFirm',
                        NoPartySubIDs: [
                            {
                                PartySubID: 'A1',
                                PartySubIDType: 'SecuritiesAccountNumber',
                            },
                        ],
                    },
                    {
                        PartyID: '104317',
                        PartyIDSource: 'CSDParticipant',
                        PartyRole: 'ClearingAccount',
                    },
                    {
                        PartyID: 'GSI',
                        PartyIDSource: 'BIC',
                        PartyRole: 'ClearingFirm',
                        PartyRoleQualifier: '23',
                        NoPartySubIDs: [
                            {
                                PartySubID: 'C3',
                                PartySubIDType: 'SecuritiesAccountNumber',
                            },
                        ],
                    },
                ],
            },
            Trailer: {
                CheckSum: '034',
            },
        });
    });

    it('#toFIXJSON() - multiple groups - related symbols', () => {
        const message: Message = fixParser.parse(
            '8=FIX.4.4|9=158|35=V|49=quote.INTLTEST.013|56=demo.fxgrid|128=BRKF|34=2|50=INTLTEST|52=20250127-18:45:42.428|262=1|264=0|265=0|267=2|269=0|269=1|146=1|55=EUR/GBP|460=4|167=FOR|263=1|10=070',
        )[0];

        expect(message.toFIXJSON()).toEqual({
            Header: {
                BeginString: 'FIX44',
                BodyLength: 158,
                MsgType: 'MarketDataRequest',
                SenderCompID: 'quote.INTLTEST.013',
                TargetCompID: 'demo.fxgrid',
                DeliverToCompID: 'BRKF',
                MsgSeqNum: 2,
                SenderSubID: 'INTLTEST',
                SendingTime: '20250127-18:45:42.428',
            },
            Body: {
                MDReqID: '1',
                MarketDepth: 0,
                MDUpdateType: 'FullRefresh',
                NoMDEntryTypes: [{ MDEntryType: 'Bid' }, { MDEntryType: 'Offer' }],
                NoRelatedSym: [
                    {
                        Symbol: 'EUR/GBP',
                        Product: 'CURRENCY',
                        SecurityType: 'ForeignExchangeContract',
                    },
                ],
                SubscriptionRequestType: 'SnapshotAndUpdates',
            },
            Trailer: {},
        });
    });
});
