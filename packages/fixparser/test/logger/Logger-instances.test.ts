import { Logger } from '../../src/logger/Logger';

describe('Logger multiple instances', () => {
    beforeEach(() => {
        jest.clearAllMocks();
        jest.unmock('../../../fixparser-common/src/uuidv4');
        jest.dontMock('../../../fixparser-common/src/uuidv4');
        jest.resetModules();
        jest.restoreAllMocks();
    });

    it('should generate unique IDs for each logger instance', () => {
        const logger1 = new Logger();
        const logger2 = new Logger();
        expect(logger1.id).not.toBe(logger2.id);
    });
});
