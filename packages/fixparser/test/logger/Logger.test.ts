import type { LogMessage } from 'fixparser-common';
import { type ConsoleFormat, ConsoleLogTransport } from '../../../fixparser-plugin-log-console/src/ConsoleLogTransport';
import { Logger } from '../../src/logger/Logger';

jest.mock('../../../fixparser-common/src/uuidv4', () => ({
    uuidv4: jest.fn().mockReturnValue('mock-uuid'),
}));

describe('Logger', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should initialize logger with default options', () => {
        const logger = new Logger();
        expect(logger.name).toBe('');
        expect(logger.id).toBe('mock-uuid');
        expect(logger.level).toBe('info');
        expect(logger.format).toBe('json');
    });

    it('should initialize logger with custom options', () => {
        const logger = new Logger({
            name: 'TestLogger',
            id: 'custom-id',
            level: 'warn',
            format: 'console',
        });
        expect(logger.name).toBe('TestLogger');
        expect(logger.id).toBe('custom-id');
        expect(logger.level).toBe('warn');
        expect(logger.format).toBe('console');
    });

    it('should log an info message when level is info', () => {
        const consoleTransport = new ConsoleLogTransport({ format: 'console' as ConsoleFormat });
        const logger = new Logger({ name: 'TestLogger', level: 'info', transport: consoleTransport });
        console.log = jest.fn();

        const message: LogMessage = { level: 'info', message: 'This is an info message' };
        logger.log(message);

        expect(console.log).toHaveBeenCalledWith(
            'TestLogger mock-uuid: This is an info message',
            'timestamp: 1629064307365',
        );
    });

    it('should log a warning message when level is warn', () => {
        const logger = new Logger({ name: 'Some FIXParser Logger Instance', level: 'warn' });
        console.log = jest.fn();

        const message: LogMessage = { level: 'warn', message: 'This is a warning' };
        logger.log(message);

        expect(console.log).toHaveBeenCalledWith(
            JSON.stringify({
                name: 'Some FIXParser Logger Instance',
                id: 'mock-uuid',
                timestamp: 1629064307365,
                level: 'warn',
                message: 'This is a warning',
            }),
        );
    });

    it('should log an error message when level is error', () => {
        const logger = new Logger({ level: 'error' });
        console.log = jest.fn();

        const message: LogMessage = { level: 'error', message: 'This is an error' };
        logger.log(message);

        expect(console.log).toHaveBeenCalledWith(
            JSON.stringify({
                name: '',
                id: 'mock-uuid',
                timestamp: 1629064307365,
                level: 'error',
                message: 'This is an error',
            }),
        );
    });

    it('should not log an info message when level is warn', () => {
        const logger = new Logger({ level: 'warn' });
        console.log = jest.fn();

        const message: LogMessage = { level: 'info', message: 'This is an info message' };
        logger.log(message);

        expect(console.log).not.toHaveBeenCalled();
    });

    it('should log an error message in json format', () => {
        const logger = new Logger({ level: 'info' });
        console.log = jest.fn();

        const message: LogMessage = { level: 'error', message: 'This is an error' };
        logger.log(message);

        expect(console.log).toHaveBeenCalledWith(
            JSON.stringify({
                name: '',
                id: 'mock-uuid',
                timestamp: 1629064307365,
                level: 'error',
                message: 'This is an error',
            }),
        );
    });

    it('should log an error message in console format', () => {
        const logger = new Logger({ level: 'info', format: 'console' });
        console.log = jest.fn();

        const message: LogMessage = { level: 'error', message: 'This is an error' };
        logger.log(message);

        expect(console.log).toHaveBeenCalledWith('mock-uuid: This is an error', 'timestamp: 1629064307365');
    });

    it('should respect silent mode and not log any messages', () => {
        const logger = new Logger({ level: 'info' });
        logger.silent = true;

        const message: LogMessage = { level: 'info', message: 'This should not be logged' };
        console.log = jest.fn();
        logger.log(message);

        expect(console.log).not.toHaveBeenCalled();
    });
});
