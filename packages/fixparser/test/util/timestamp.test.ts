import type { Logger } from '../../src/logger/Logger';
import { timestamp } from '../../src/util/util';

describe('timestamp', () => {
    let logger: Logger;
    let date: Date;

    beforeEach(() => {
        // Mock the Logger object
        logger = { log: jest.fn() };

        // Create a date object
        date = new Date('2025-01-14T10:30:45.123Z');
    });

    it('should return a correctly formatted timestamp for a valid date', () => {
        const result = timestamp(date, logger);

        // Check that the date is correctly adjusted and formatted
        expect(result).toBe('20250114-10:30:45.123');
    });

    it('should log an error for an invalid date', () => {
        const invalidDate = new Date('invalid date');

        timestamp(invalidDate, logger);

        // Ensure the logger logs an error when the date is invalid
        expect(logger.log).toHaveBeenCalledWith({
            level: 'error',
            message: 'Invalid date specified!',
        });
    });

    it('should adjust the date according to the timezone offset (positive offset)', () => {
        // Create a date that would be adjusted by the timezone offset
        const dateWithOffset = new Date('August 19, 2025 23:15:30 GMT-00:30'); // UTC date

        // Mock getTimezoneOffset to return a non-zero offset (e.g., +30 minutes)
        const mockGetTimezoneOffset = jest.spyOn(dateWithOffset, 'getTimezoneOffset').mockReturnValue(30); // 30 minutes (UTC+00:30)

        // Call the timestamp function
        const result = timestamp(dateWithOffset, logger);

        // Expect the timestamp to reflect the timezone offset (30 minutes behind)
        expect(result).toBe('20250819-23:45:30.000');

        mockGetTimezoneOffset.mockRestore(); // Restore the original method
    });

    it('should adjust the date according to the timezone offset (negative offset)', () => {
        // Create a date that would be adjusted by the timezone offset
        const dateWithOffset = new Date('August 19, 2025 23:15:30 GMT+07:00'); // GMT+7

        // Mock getTimezoneOffset to return a non-zero offset (e.g., -3 hours)
        const mockGetTimezoneOffset = jest.spyOn(dateWithOffset, 'getTimezoneOffset').mockReturnValue(-420); // -420 minutes (UTC-7)

        // Call the timestamp function
        const result = timestamp(dateWithOffset, logger);

        // Expect the timestamp to reflect the timezone offset (7 hours ahead)
        expect(result).toBe('20250819-16:15:30.000');

        mockGetTimezoneOffset.mockRestore(); // Restore the original method
    });
});
