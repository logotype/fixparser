import { ApplVerID } from '../../src/fieldtypes/ApplVerID';
import {
    DEFAULT_FIX_VERSION,
    READY_MS,
    RE_ESCAPE,
    RE_FIND,
    SOH,
    STRING_EQUALS,
    pad,
    parseFixVersion,
} from '../../src/util/util';

describe('utils', () => {
    describe('#parseFixVersion', () => {
        it('should correctly map ApplVerID to FIX version string', () => {
            expect(parseFixVersion(ApplVerID.FIX27)).toBe('FIX.2.7');
            expect(parseFixVersion(ApplVerID.FIX30)).toBe('FIX.3.0');
            expect(parseFixVersion(ApplVerID.FIX50)).toBe('FIX.5.0');
            expect(parseFixVersion(ApplVerID.FIXLatest)).toBe('FIXT.1.1');
        });

        it('should return the original version if it is not a valid ApplVerID', () => {
            expect(parseFixVersion('nonValidVersion')).toBe('nonValidVersion');
            expect(parseFixVersion(12345)).toBe(12345);
        });
    });

    describe('DEFAULT_FIX_VERSION', () => {
        it('should correctly reference the latest FIX version', () => {
            expect(DEFAULT_FIX_VERSION).toBe('FIXT.1.1');
        });
    });

    describe('util constants', () => {
        it('should define the SOH character as the correct control character', () => {
            expect(SOH).toBe('\x01');
        });

        it('should define the STRING_EQUALS constant as "="', () => {
            expect(STRING_EQUALS).toBe('=');
        });

        it('should define the RE_ESCAPE regular expression', () => {
            expect(RE_ESCAPE).toEqual(/[.*+?^${}()|[\]\\]/g);
        });

        it('should define the RE_FIND regular expression', () => {
            expect(RE_FIND).toEqual(/8=FIXT?\.\d\.\d([^\d]+)/i);
        });

        it('should define READY_MS as 100', () => {
            expect(READY_MS).toBe(100);
        });
    });

    describe('#pad function', () => {
        it('should pad numbers to the correct length', () => {
            expect(pad(1, 2)).toBe('01');
            expect(pad(45, 2)).toBe('45');
            expect(pad(7, 3)).toBe('007');
            expect(pad(123, 3)).toBe('123');
            expect(pad(9, 5)).toBe('00009');
        });

        it('should handle padding correctly for single-digit and multi-digit numbers', () => {
            expect(pad(9, 2)).toBe('09');
            expect(pad(1234, 5)).toBe('01234');
        });
    });
});
