import { PassThrough, Transform } from 'node:stream';
import { FrameDecoder } from '../../src/util/FrameDecoder';

describe('FrameDecoder', () => {
    let decoder: FrameDecoder;
    let passThrough: PassThrough;

    beforeEach(() => {
        decoder = new FrameDecoder();
        passThrough = new PassThrough();
    });

    it('should be an instance of Transform stream', () => {
        expect(decoder).toBeInstanceOf(FrameDecoder);
        expect(decoder).toBeInstanceOf(Transform);
    });

    it('should split data correctly based on the regex pattern', (done) => {
        const inputData = '8=FIXT.1.1\x0110=001\x018=FIX.4.4\x0110=002\x01';
        const expectedChunks = ['8=FIXT.1.1\x0110=001\x01', '8=FIX.4.4\x0110=002\x01'];

        const resultChunks: string[] = [];
        passThrough
            .pipe(decoder)
            .on('data', (chunk) => {
                resultChunks.push(chunk.toString());
            })
            .on('end', () => {
                expect(resultChunks).toEqual(expectedChunks);
                done();
            });

        passThrough.write(inputData);
        passThrough.end();
    });

    it('should handle partial frames correctly', (done) => {
        const inputData1 = '8=FIXT.1.1\x0110=001\x01';
        const inputData2 = '8=FIX.4.4\x0110=002\x01';
        const expectedChunks = ['8=FIXT.1.1\x0110=001\x01', '8=FIX.4.4\x0110=002\x01'];

        const resultChunks: string[] = [];
        passThrough
            .pipe(decoder)
            .on('data', (chunk) => {
                resultChunks.push(chunk.toString());
            })
            .on('end', () => {
                expect(resultChunks).toEqual(expectedChunks);
                done();
            });

        // Write data in two parts, simulating partial frames
        passThrough.write(inputData1);
        passThrough.write(inputData2);
        passThrough.end();
    });

    it('should correctly handle a chunk that doesn’t complete a frame', (done) => {
        const partialData = '8=FIXT.1.1\x0110=001';
        const expectedRemainingData = '8=FIXT.1.1\x0110=001'; // It should be left in the "data" property

        passThrough
            .pipe(decoder)
            .on('data', () => {})
            .on('end', () => {
                expect(decoder.data).toBe(expectedRemainingData);
                done();
            });

        passThrough.write(partialData);
        passThrough.end();
    });

    it('should clear the data on destroy', () => {
        decoder.data = '8=FIXT.1.1\x0110=001';
        expect(decoder.data).toBe('8=FIXT.1.1\x0110=001');

        decoder.destroy();

        expect(decoder.data).toBeUndefined();
    });

    it('should not push incomplete data if it doesn’t match the regex pattern', (done) => {
        const inputData = '8=FIXT.1.1\x0110=001';
        const expectedChunks: string[] = [];

        const resultChunks: string[] = [];
        passThrough
            .pipe(decoder)
            .on('data', (chunk) => {
                resultChunks.push(chunk.toString());
            })
            .on('end', () => {
                expect(resultChunks).toEqual(expectedChunks);
                done();
            });

        passThrough.write(inputData);
        passThrough.end();
    });

    it('should push complete frames immediately after receiving full data', (done) => {
        const inputData = '8=FIXT.1.1\x0110=001\x018=FIX.4.4\x0110=002\x01';
        const expectedChunks = ['8=FIXT.1.1\x0110=001\x01', '8=FIX.4.4\x0110=002\x01'];

        const resultChunks: string[] = [];
        passThrough
            .pipe(decoder)
            .on('data', (chunk) => {
                resultChunks.push(chunk.toString());
            })
            .on('end', () => {
                expect(resultChunks).toEqual(expectedChunks);
                done();
            });

        passThrough.write(inputData);
        passThrough.end();
    });
});
