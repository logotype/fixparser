#!/usr/bin/env bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <filename>"
    exit 1
fi

FILE="$1"

# Check if the file exists
if [[ ! -f "$FILE" ]]; then
    echo "$FILE not found!"
    exit 1
fi

# Replace the root "specs" folder with "output" in the file path
OUTPUT_FILE=".processed-specs/${FILE#specs/}"

# Create the output directory if it doesn't exist
OUTPUT_DIR=$(dirname "$OUTPUT_FILE")
mkdir -p "$OUTPUT_DIR"

echo -e "\e[92mStart processing of $FILE \e[39m"

# Use sed to replace all occurrences of 'fixr:' with an empty string and save to the new file
sed 's/fixr://g' "$FILE" > "$OUTPUT_FILE"

echo -e "\e[92mFinished processing $FILE \e[39m"
echo -e "\e[92mProcessed file saved as $OUTPUT_FILE \e[39m"