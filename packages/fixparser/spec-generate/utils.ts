export const escapeAngleBrackets = (str: string): string => str.replace(/</g, '\\<').replace(/>/g, '\\>');
