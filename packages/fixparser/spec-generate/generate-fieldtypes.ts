import { readFileSync, writeFileSync } from 'node:fs';
// https://github.com/FIXTradingCommunity/orchestrations/tree/master/FIX%20Standard
import { join } from 'node:path';

import parser from 'xml2json';

import { escapeAngleBrackets } from './utils';

const TARGET_DIR = join(__dirname, '../src/fieldtypes');
const TARGET_TEST_DIR = join(__dirname, '../test/fieldtypes');

const fixLatest = readFileSync('./.processed-specs/OrchestraFIXLatest.xml', { encoding: 'utf8', flag: 'r' });
const FIX44 = readFileSync('./.processed-specs/OrchestraFIX44.xml', { encoding: 'utf8', flag: 'r' });
const FIX42 = readFileSync('./.processed-specs/OrchestraFIX42.xml', { encoding: 'utf8', flag: 'r' });

const fields = JSON.parse(parser.toJson(fixLatest)).repository.fields.field;
const fields44 = JSON.parse(parser.toJson(FIX44)).repository.fields.field;
const fields42 = JSON.parse(parser.toJson(FIX42)).repository.fields.field;

const codeSets = JSON.parse(parser.toJson(fixLatest)).repository.codeSets.codeSet;
const codeSets44 = JSON.parse(parser.toJson(FIX44)).repository.codeSets.codeSet;
const codeSets42 = JSON.parse(parser.toJson(FIX42)).repository.codeSets.codeSet;

const messages = JSON.parse(parser.toJson(fixLatest)).repository.messages.message;
const messages44 = JSON.parse(parser.toJson(FIX44)).repository.messages.message;
const messages42 = JSON.parse(parser.toJson(FIX42)).repository.messages.message;

const indexSet = new Set<string>();
const codeSetFilenames = new Set<string>();
const codeSetTestFilenames = new Set<string>();

const removeCodeSet = (name: string) => name.replace('CodeSet', '');
const mapCodeSetType = (value: string, valueType: string) => {
    if (valueType === 'int' || valueType === 'NumInGroup') {
        return { value: `${value}`, typeName: 'number' };
    }
    return { value: `'${value}'`, typeName: 'string' };
};

const processCodeSets = (codeSetArray, fixVersion?: string) => {
    codeSetArray.forEach((codeSet) => {
        if (codeSet.code && Array.isArray(codeSet.code)) {
            const codeSetName = removeCodeSet(codeSet.name);
            const indexFileEntry = `export { ${codeSetName} } from './${codeSetName}';`;
            indexSet.add(indexFileEntry);

            const codeSetArray = [];
            const codeSetTestArray_1 = [];
            const codeSetTestArray_2 = [];
            const codeSetTestArray_3 = [];
            let codeSetTestFirstValue: string;

            codeSet.code
                .sort((a, b) => Number(a.sort) - Number(b.sort))
                .forEach((value, index) => {
                    const docsCode = value.annotation?.documentation?.$t
                        ? `/** ${escapeAngleBrackets(value.annotation.documentation.$t)} */\n    `
                        : '';
                    codeSetArray.push(`${docsCode}${value.name}: ${mapCodeSetType(value.value, codeSet.type).value}`);
                    codeSetTestArray_1.push(
                        `expect(${codeSetName}.${value.name}).toBe(${mapCodeSetType(value.value, codeSet.type).value});`,
                    );
                    codeSetTestArray_2.push(`${codeSetName}.${value.name},`);
                    codeSetTestArray_3.push(`validate(${codeSetName}.${value.name});`);
                    if (index === 0) {
                        codeSetTestFirstValue = value.name;
                    }
                });

            const docsCodeSet = `/**
 * ${codeSet.annotation?.documentation.$t || codeSetName}
 * - Tag: ${codeSet.id}
 * - FIX Specification type: ${codeSet.type}
 ${
     fixVersion
         ? `* - FIX Specification version: ${fixVersion}
 `
         : ''
 }* - Mapped type: ${mapCodeSetType('', codeSet.type).typeName}
 * @readonly
 * @public
 */\n`;
            const codeSetString = `${docsCodeSet}export const ${codeSetName} = Object.freeze({
    ${codeSetArray.join(',\n    ')},
} as const);

type ${codeSetName} = (typeof ${codeSetName})[keyof typeof ${codeSetName}];\n`;

            const codeSetTest = `import { ${codeSetName} } from './../../src/fieldtypes/${codeSetName}';

describe('${codeSetName}', () => {
    test('should have the correct values', () => {
        ${codeSetTestArray_1.join('\n        ')}
    });

    test('should not allow additional properties', () => {
        expect(() => { ${codeSetName}.NewValue = 5 }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ${codeSetTestArray_2.join('\n            ')}
        ];

        values.forEach(value => {
            expect(typeof value).toBe('${mapCodeSetType('', codeSet.type).typeName}');
        });
    });

    test('should allow only valid values for ${codeSetName} type', () => {
        const validate = (value: ${codeSetName}) => {
            expect(Object.values(${codeSetName})).toContain(value);
        };

        ${codeSetTestArray_3.join('\n        ')}
    });

    test('should be immutable', () => {
        const ref = ${codeSetName};
        expect(() => {
            ref.${codeSetTestFirstValue} = 10;
        }).toThrowError();
    });
});
`;

            if (!codeSetTestFilenames.has(`${TARGET_TEST_DIR}/${codeSetName}.test.ts`)) {
                writeFileSync(`${TARGET_TEST_DIR}/${codeSetName}.test.ts`, codeSetTest);
                codeSetTestFilenames.add(`${TARGET_TEST_DIR}/${codeSetName}.test.ts`);
            } else {
                console.log(`File ${codeSetName}.test.ts already exists. Skipping write.`);
            }

            if (!codeSetFilenames.has(`${TARGET_DIR}/${codeSetName}.ts`)) {
                writeFileSync(`${TARGET_DIR}/${codeSetName}.ts`, codeSetString);
                codeSetFilenames.add(`${TARGET_DIR}/${codeSetName}.ts`);
            } else {
                console.log(`File ${codeSetName}.ts already exists. Skipping write.`);
            }
        }
    });
};
processCodeSets(codeSets);
processCodeSets(codeSets44, 'FIX44');
processCodeSets(codeSets42, 'FIX42');

// FIELD
const fieldMap = new Map<
    string,
    { id: number; docs: string; name: string; nameValue: string; nameEqualsValue: string }
>();
const processFields = (fieldArray) => {
    fieldArray
        .sort((a, b) => Number(a.id) - Number(b.id))
        .forEach((field) => {
            const fieldDocs = field.annotation?.documentation?.$t
                ? `/** ${escapeAngleBrackets(field.annotation.documentation.$t)} */\n    `
                : '';
            if (!fieldMap.has(field.name)) {
                fieldMap.set(field.name, {
                    id: Number(field.id),
                    docs: fieldDocs,
                    name: `${field.name}`,
                    nameValue: `${field.name}: ${field.id}`,
                    nameEqualsValue: `${field.name}= ${field.id}`,
                });
            }
        });
};

processFields(fields);
processFields(fields44);
processFields(fields42);

let fieldCodeSetString = `/**
 * Field is a predefined data element, identified by a unique tag number,
 * that represents a specific piece of information within a message
 * (such as price, quantity, or order ID).
 *
 * @public
 */
export const Field = Object.freeze({\n    `;

const fieldCodeSetTestArray_1 = [];
const fieldCodeSetTestArray_2 = [];
const fieldCodeSetTestArray_3 = [];
let fieldCodeSetTestFirstValue: string;

Array.from(fieldMap.values())
    .sort((a, b) => a.id - b.id)
    .forEach((field, index, arr) => {
        fieldCodeSetString += `${field.docs}${field.nameValue},`;
        fieldCodeSetTestArray_1.push(`expect(Field.${field.name}).toBe(${field.id});`);
        fieldCodeSetTestArray_2.push(`Field.${field.name},`);
        fieldCodeSetTestArray_3.push(`validate(Field.${field.name});`);
        if (index < arr.length - 1) {
            fieldCodeSetString += '\n    ';
        } else {
            fieldCodeSetString += '\n';
        }
        if (index === 0) {
            fieldCodeSetTestFirstValue = `${field.name}`;
        }
    });
fieldCodeSetString += `} as const);

type Field = (typeof Field)[keyof typeof Field];\n`;

const fieldCodeSetTest = `import { Field } from './../../src/fieldtypes/Field';

describe('Field', () => {
    test('should have the correct values', () => {
        ${fieldCodeSetTestArray_1.join('\n        ')}
    });

    test('should not allow additional properties', () => {
        expect(() => { Field.NewValue = 5 }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ${fieldCodeSetTestArray_2.join('\n            ')}
        ];

        values.forEach(value => {
            expect(typeof value).toBe('number');
        });
    });

    test('should allow only valid values for Field type', () => {
        const validate = (value: Field) => {
            expect(Object.values(Field)).toContain(value);
        };

        ${fieldCodeSetTestArray_3.join('\n        ')}
    });

    test('should be immutable', () => {
        const ref = Field;
        expect(() => {
            ref.${fieldCodeSetTestFirstValue} = 10;
        }).toThrowError();
    });
});
`;

if (!codeSetTestFilenames.has(`${TARGET_TEST_DIR}/Field.test.ts`)) {
    writeFileSync(`${TARGET_TEST_DIR}/Field.test.ts`, fieldCodeSetTest);
    codeSetTestFilenames.add(`${TARGET_TEST_DIR}/Field.test.ts`);
} else {
    console.log('File Field.test.ts already exists. Skipping write.');
}

writeFileSync(`${TARGET_DIR}/Field.ts`, fieldCodeSetString);

const fieldCodeSetEntry = `export { Field as Fields } from './Field';`;
indexSet.add(fieldCodeSetEntry);

// MESSAGE
const messageMap = new Map<string, { id: number; docs: string; name: string; value: string; nameValue: string }>();
const processMessages = (messageArray) => {
    messageArray
        .sort((a, b) => Number(a.id) - Number(b.id))
        .forEach((message) => {
            const messageDocs = message.annotation?.documentation?.$t
                ? `/** ${escapeAngleBrackets(message.annotation.documentation.$t)} */\n    `
                : '';
            if (!messageMap.has(message.name)) {
                messageMap.set(message.name, {
                    id: Number(message.id),
                    docs: messageDocs,
                    name: `${message.name}`,
                    value: `${message.msgType}`,
                    nameValue: `${message.name}: '${message.msgType}'`,
                });
            }
        });
};
processMessages(messages);
processMessages(messages44);
processMessages(messages42);

let messageCodeSetString = `/**
 * The Message object contains all possible FIX message types,
 * such as HeartBeat, TestRequest, and NewOrderSingle, each
 * representing a specific kind of communication or action
 * within the FIX protocol.
 *
 * @public
 */
export const Message = Object.freeze({\n    `;

const messageCodeSetTestArray_1 = [];
const messageCodeSetTestArray_2 = [];
const messageCodeSetTestArray_3 = [];
let messageCodeSetTestFirstName: string;

Array.from(messageMap.values())
    .sort((a, b) => a.id - b.id)
    .forEach((message, index, arr) => {
        messageCodeSetString += `${message.docs}${message.nameValue},`;
        messageCodeSetTestArray_1.push(`expect(Message.${message.name}).toBe('${message.value}');`);
        messageCodeSetTestArray_2.push(`Message.${message.name},`);
        messageCodeSetTestArray_3.push(`validate(Message.${message.name});`);
        if (index < arr.length - 1) {
            messageCodeSetString += '\n    ';
        } else {
            messageCodeSetString += '\n';
        }
        if (index === 0) {
            messageCodeSetTestFirstName = `${message.name}`;
        }
    });
messageCodeSetString += `} as const);

type Message = (typeof Message)[keyof typeof Message];\n`;

const codeSetTest = `import { Message } from './../../src/fieldtypes/Message';

describe('Message', () => {
    test('should have the correct values', () => {
        ${messageCodeSetTestArray_1.join('\n        ')}
    });

    test('should not allow additional properties', () => {
        expect(() => { Message.NewValue = 5 }).toThrowError();
    });

    test('should have the correct type', () => {
        const values: number[] = [
            ${messageCodeSetTestArray_2.join('\n            ')}
        ];

        values.forEach(value => {
            expect(typeof value).toBe('string');
        });
    });

    test('should allow only valid values for Message type', () => {
        const validate = (value: Message) => {
            expect(Object.values(Message)).toContain(value);
        };

        ${messageCodeSetTestArray_3.join('\n        ')}
    });

    test('should be immutable', () => {
        const ref = Message;
        expect(() => {
            ref.${messageCodeSetTestFirstName} = 10;
        }).toThrowError();
    });
});
`;

if (!codeSetTestFilenames.has(`${TARGET_TEST_DIR}/Message.test.ts`)) {
    writeFileSync(`${TARGET_TEST_DIR}/Message.test.ts`, codeSetTest);
    codeSetTestFilenames.add(`${TARGET_TEST_DIR}/Message.test.ts`);
} else {
    console.log('File Message.test.ts already exists. Skipping write.');
}

writeFileSync(`${TARGET_DIR}/Message.ts`, messageCodeSetString);

const messageCodeSetEntry = `export { Message as Messages } from './Message';`;
indexSet.add(messageCodeSetEntry);

// MAIN INDEX FILE
const indexFileString = `${Array.from(indexSet).join('\n')}\n`;
writeFileSync(`${TARGET_DIR}/index.ts`, indexFileString);
