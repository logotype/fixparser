import type { ILogTransporter, Level, LogMessage } from 'fixparser-common';
import { uuidv4 } from 'fixparser-common';
import { ConsoleLogTransport, ConsoleFormat as Format } from 'fixparser-plugin-log-console';

/**
 * Configuration options for logging output.
 *
 * @public
 */
export type LogOptions = {
    name?: string;
    id?: string;
    level?: Level;
    format?: Format;
    transport?: ILogTransporter;
};

const shouldLog = (message: LogMessage, level: Level): boolean => {
    if (level === 'error' && message.level === 'error') {
        return true;
    }
    if (level === 'warn' && (message.level === 'error' || message.level === 'warn')) {
        return true;
    }
    return level === 'info' && (message.level === 'error' || message.level === 'warn' || message.level === 'info');
};

export class Logger {
    public name: string;
    public id: string;
    readonly #enabled = true;
    readonly #transport: ILogTransporter;
    readonly #format: Format;
    #level: Level;

    constructor(options?: LogOptions) {
        this.name = options?.name ?? '';
        this.id = options?.id ?? uuidv4();
        this.#level = options?.level ?? 'info';
        this.#level = options?.level ?? 'info';
        this.#format = options?.format ?? 'json';
        this.#transport = options?.transport ?? new ConsoleLogTransport({ format: this.#format });
    }

    private processMessage(msg: LogMessage): this {
        if (this.#level === 'silent' || !this.#enabled) return this;

        if (!shouldLog(msg, this.#level)) {
            return this;
        }

        const processedMessage = {
            name: this.name,
            id: this.id,
            timestamp: Date.now(),
            ...msg,
        };

        this.#transport.send(processedMessage);

        return this;
    }

    public log(msg: LogMessage): this {
        return this.processMessage({ ...msg, level: msg.level ? msg.level : ('info' as Level) });
    }

    public logWarning(msg: Omit<LogMessage, 'level'>): this {
        return this.processMessage({ ...msg, level: 'warn' as Level } as LogMessage);
    }

    public logError(msg: Omit<LogMessage, 'level'>): this {
        return this.processMessage({ ...msg, level: 'error' as Level } as LogMessage);
    }

    public set silent(silent: boolean) {
        this.#level = silent ? 'silent' : 'info';
    }

    public get silent(): boolean {
        return this.#level === 'silent';
    }

    public get level(): Level {
        return this.#level;
    }

    public get format(): Format {
        return this.#format;
    }
}

export { Format };
