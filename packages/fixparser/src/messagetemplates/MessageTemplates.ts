import type { IFIXParser } from '../IFIXParser';
import { Field } from '../fields/Field';
import { GapFillFlag } from '../fieldtypes';
import { Field as FieldType } from '../fieldtypes/Field';
import { Message as MessageType } from '../fieldtypes/Message';
import { LicenseManager } from '../licensemanager/LicenseManager';
import type { Message } from '../message/Message';

export const heartBeat = (parser: IFIXParser, testReqId?: Field): Message => {
    if (!LicenseManager.validateLicense()) {
        throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
    }
    const fields: Field[] = [
        new Field(FieldType.BeginString, parser.fixVersion),
        new Field(FieldType.MsgType, MessageType.Heartbeat),
        new Field(FieldType.MsgSeqNum, parser.getNextTargetMsgSeqNum()),
        new Field(FieldType.SenderCompID, parser.sender),
        new Field(FieldType.TargetCompID, parser.target),
        new Field(FieldType.SendingTime, parser.getTimestamp(new Date())),
    ];

    if (testReqId) {
        fields.push(testReqId);
    }

    return parser.createMessage(...fields);
};

export const sequenceReset = (
    parser: IFIXParser,
    seqNo: number,
    newSeqNo: number,
    gapFillFlag: 'Y' | 'N' = GapFillFlag.GapFillMessage,
): Message => {
    if (!LicenseManager.validateLicense()) {
        throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
    }
    const fields: Field[] = [
        new Field(FieldType.BeginString, parser.fixVersion),
        new Field(FieldType.MsgType, MessageType.SequenceReset),
        new Field(FieldType.MsgSeqNum, seqNo),
        new Field(FieldType.SenderCompID, parser.sender),
        new Field(FieldType.TargetCompID, parser.target),
        new Field(FieldType.SendingTime, parser.getTimestamp(new Date())),
        new Field(FieldType.GapFillFlag, gapFillFlag),
        new Field(FieldType.NewSeqNo, newSeqNo + 1),
    ];

    return parser.createMessage(...fields);
};
