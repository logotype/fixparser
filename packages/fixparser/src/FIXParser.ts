import { Socket } from 'node:net';
import { type ConnectionOptions, connect as TLSConnect, type TLSSocket } from 'node:tls';
import { WebSocket } from 'ws';

import { type IMessageStore, MessageBuffer } from 'fixparser-common';
import type { ProxyAgent } from 'proxy-agent';
import { type BaseOptions, type ConnectionType, FIXParserBase, type Options, type Protocol } from './FIXParserBase';
import type { IFIXParser } from './IFIXParser';
import { Field } from './fields/Field';
import * as Constants from './fieldtypes';
import { Field as FieldType } from './fieldtypes/Field';
import { Message as MessageType } from './fieldtypes/Message';
import { LicenseManager } from './licensemanager/LicenseManager';
import { type LogOptions, Logger } from './logger/Logger';
import { Message } from './message/Message';
import { heartBeat } from './messagetemplates/MessageTemplates';
import { clientProcessMessage } from './session/ClientMessageProcessor';
import { FrameDecoder } from './util/FrameDecoder';
import {
    DEFAULT_FIX_VERSION,
    DEFAULT_HEARTBEAT_SECONDS,
    type Parser,
    READY_MS,
    type Version,
    parseFixVersion,
    timestamp,
    version,
} from './util/util';

/**
 * FIXParser class.
 * @public
 */
class FIXParser implements IFIXParser {
    /* FIXParser Version */
    public static readonly version: Version = version;
    /* Name of current parser name */
    public readonly parserName: Parser = 'FIXParser';
    /* Instance of FIXParser base class */
    public readonly fixParserBase: FIXParserBase = new FIXParserBase();
    /* Heartbeat interval ID */
    public heartBeatIntervalId: ReturnType<typeof setInterval> | undefined = undefined;
    /* Type of socket connection (Socket, WebSocket, TLSSocket) */
    public socket: Socket | WebSocket | TLSSocket | undefined = undefined;
    /* Current connection status */
    public connected = false;
    /* Target hostname or IP address */
    public host: string | undefined;
    /* Target port */
    public port: number | undefined;
    /* Type of protocol */
    public protocol: Protocol | undefined = 'tcp';
    /* Assigned value used to identify firm sending message, SenderCompID tag 49 */
    public sender: string | undefined = undefined;
    /* Assigned value used to identify receiving firm, TargetCompID tag 56 */
    public target: string | undefined = undefined;
    /* Heartbeat interval in seconds */
    public heartBeatInterval: number = DEFAULT_HEARTBEAT_SECONDS;
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    public fixVersion: string = DEFAULT_FIX_VERSION;
    /* Message buffer of 2500 incoming messages */
    public messageStoreIn: IMessageStore<Message> = new MessageBuffer();
    /* Message buffer of 2500 outgoing messages */
    public messageStoreOut: IMessageStore<Message> = new MessageBuffer();
    /* Whether an outgoing message contains a Logout */
    public requestedLogout = false;
    /* Indicates whether this instance is acceptor or initiator */
    public connectionType: ConnectionType = 'initiator';
    /* Logger instance */
    public logger: Logger = new Logger();
    /* Logging */
    public logging = true;
    /* Log options */
    public logOptions: LogOptions | undefined;
    /* Proxy */
    public proxy?: ProxyAgent;
    /* TLS Connection Options */
    public tlsOptions?: ConnectionOptions;
    /* TLS Skip Standard Input pipe of socket */
    public tlsSkipStdInPipe?: boolean;

    /**
     * Constructor for initializing the FIXParser instance with the provided options.
     *
     * @param {BaseOptions} [options={ logging: true, logOptions: undefined, fixVersion: DEFAULT_FIX_VERSION, skipValidation: false }] - The options to configure the instance.
     * @param {boolean} [options.logging=true] - Whether logging is enabled (defaults to `true`).
     * @param {LogOptions} [options.logOptions=undefined] - Options to customize logging behavior.
     * @param {string} [options.fixVersion=DEFAULT_FIX_VERSION] - The FIX protocol version to use (defaults to `DEFAULT_FIX_VERSION`).
     * @param {boolean} [options.skipValidation=false] - Whether to skip validation of FIX messages (defaults to `false`).
     */
    constructor(
        options: BaseOptions = {
            logging: true,
            logOptions: undefined,
            fixVersion: DEFAULT_FIX_VERSION,
            skipValidation: false,
        },
    ) {
        this.logging = options.logging ?? true;
        this.logOptions = options.logOptions;
        this.fixParserBase.fixVersion = parseFixVersion(options.fixVersion);
        this.fixVersion = this.fixParserBase.fixVersion;
        this.fixParserBase.skipValidation = options.skipValidation;
    }

    /**
     * onMessageCallback is called when a message has been received
     *
     * @remarks FIXParser~onMessageCallback
     * @param message - A Message class instance
     */
    private onMessageCallback: Options['onMessage'] = (message) => {};

    /**
     * onOpenCallback is called the FIX connection has been initiated
     *
     * @remarks FIXParser~onOpenCallback
     */
    private onOpenCallback: Options['onOpen'] = () => {};

    /**
     * onErrorCallback is called the FIX connection failed
     *
     * @remarks FIXParser~onErrorCallback
     * @param error - Error object
     */
    private onErrorCallback: Options['onError'] = (error) => {};

    /**
     * onCloseCallback is called the FIX connection has been closed
     *
     * @remarks FIXParser~onCloseCallback
     */
    private onCloseCallback: Options['onClose'] = () => {};

    /**
     * onReadyCallback is called the FIX connection is opened and ready
     *
     * @remarks FIXParser~onReadyCallback
     */
    private onReadyCallback: Options['onReady'] = () => {};

    /**
     * Establishes a connection to a specified host and port using the given options and protocol type.
     * The method supports multiple connection protocols, including TCP, SSL/TCP, and WebSocket. It handles the
     * initialization of connection parameters, logging, and license validation before attempting to connect.
     *
     * - **TCP** (`tcp`): Connects using a basic TCP connection.
     * - **Secure TCP** (`ssl-tcp`, `tls-tcp`): Connects using a secure TLS/SSL connection.
     * - **WebSocket** (`websocket`): Establishes a WebSocket connection.
     *
     * The connection will invoke the appropriate callback functions for each connection event:
     * - `onMessage`: Called when a new message is received.
     * - `onOpen`: Called when the connection is successfully established.
     * - `onError`: Called when an error occurs.
     * - `onClose`: Called when the connection is closed.
     * - `onReady`: Called after a short delay once the connection is ready.
     *
     * @param {Options} [options={}] - Connection options for setting up the connection.
     * @param {string} [options.host='localhost'] - Host to connect to.
     * @param {number} [options.port=9878] - Port to connect to.
     * @param {string} [options.protocol='tcp'] - Protocol to use for the connection ('tcp', 'ssl-tcp', 'tls-tcp', 'websocket').
     * @param {string} [options.sender='SENDER'] - Sender ID for the FIX connection.
     * @param {string} [options.target='TARGET'] - Target ID for the FIX connection.
     * @param {number} [options.heartbeatIntervalSeconds=DEFAULT_HEARTBEAT_SECONDS] - Interval in seconds for sending heartbeat messages.
     * @param {string} [options.messageStoreIn=new MessageBuffer()] - Optional custom message buffer for incoming messages.
     * @param {string} [options.messageStoreOut=new MessageBuffer()] - Optional custom message buffer for outgoing messages.
     * @param {string | Buffer | undefined} [options.tlsKey] - Optional TLS private key for secure connections.
     * @param {string | Buffer | undefined} [options.tlsCert] - Optional TLS certificate for secure connections.
     * @param {boolean} [options.tlsUseSNI=false] - Whether to use Server Name Indication (SNI) for secure connections.
     * @param {boolean} [options.tlsSkipStdInPipe=false] - Whether to skip piping stdin for secure connections.
     * @param {any} [options.proxy=null] - Optional proxy configuration for WebSocket connections.
     * @param {Function} [options.onMessage=this.onMessageCallback] - Callback function to handle incoming messages.
     * @param {Function} [options.onOpen=this.onOpenCallback] - Callback function to handle connection opening.
     * @param {Function} [options.onError=this.onErrorCallback] - Callback function to handle errors.
     * @param {Function} [options.onClose=this.onCloseCallback] - Callback function to handle connection closing.
     * @param {Function} [options.onReady=this.onReadyCallback] - Callback function to handle connection readiness.
     *
     * @returns {void}
     */
    public connect(options: Options = {}): void {
        const {
            host = 'localhost',
            port = 9878,
            protocol = 'tcp',
            sender = 'SENDER',
            target = 'TARGET',
            heartbeatIntervalSeconds = DEFAULT_HEARTBEAT_SECONDS,
            messageStoreIn = new MessageBuffer(),
            messageStoreOut = new MessageBuffer(),
            tlsOptions = {},
            tlsSkipStdInPipe = false,
            proxy = undefined,
            onMessage = this.onMessageCallback,
            onOpen = this.onOpenCallback,
            onError = this.onErrorCallback,
            onClose = this.onCloseCallback,
            onReady = this.onReadyCallback,
        } = options;
        this.logger = new Logger(
            this.logOptions
                ? {
                      name: this.logOptions.name ?? 'fixparser',
                      ...this.logOptions,
                  }
                : {
                      name: 'fixparser',
                      format: 'json',
                  },
        );
        if (!this.logging) {
            this.logger.silent = true;
        }
        LicenseManager.setLogger(this.logger);
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        this.connectionType = 'initiator';
        this.messageStoreIn = messageStoreIn;
        this.messageStoreOut = messageStoreOut;
        this.protocol = protocol;
        this.sender = sender;
        this.target = target;
        this.port = port;
        this.host = host;
        this.proxy = proxy;
        if (tlsOptions) this.tlsOptions = tlsOptions as ConnectionOptions;
        if (tlsSkipStdInPipe) this.tlsSkipStdInPipe = tlsSkipStdInPipe;
        this.heartBeatInterval = heartbeatIntervalSeconds;
        this.onMessageCallback = onMessage;
        this.onOpenCallback = onOpen;
        this.onErrorCallback = onError;
        this.onCloseCallback = onClose;
        this.onReadyCallback = onReady;

        switch (protocol) {
            case 'tcp':
                this.connectTcp();
                break;
            case 'ssl-tcp':
            case 'tls-tcp':
                this.connectTlsTcp();
                break;
            case 'websocket':
                this.connectWebsocket();
                break;
            default:
                throw new Error(`Unsupported protocol: ${protocol}`);
        }
    }

    /**
     * Establishes a TCP connection to the specified host and port, sets up the socket for communication,
     * and handles various socket events such as 'data', 'close', 'ready', 'timeout', and 'error'.
     *
     * - Upon successful connection, the socket is ready to receive and process messages.
     * - Incoming messages are parsed and processed through a message buffer and the provided callback.
     * - Handles error scenarios such as timeout and connection errors, with appropriate cleanup and notifications.
     *
     * @returns {void}
     */
    private connectTcp() {
        this.socket = new Socket();
        this.socket.setEncoding('ascii');
        this.socket.pipe(new FrameDecoder()).on('data', (data: string) => {
            const messages: Message[] = this.parse(data.toString());
            let i = 0;
            for (i; i < messages.length; i++) {
                clientProcessMessage(this, messages[i]);
                this.messageStoreIn.add(messages[i]);
                this.onMessageCallback?.(messages[i]);
            }
        });
        this.socket.connect(Number(this.port), String(this.host), () => {
            this.connected = true;
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Connected`,
            });
            this.onOpenCallback?.();
        });
        this.socket.on('close', () => {
            this.connected = false;
            this.onCloseCallback?.();
            this.stopHeartbeat();
        });
        this.socket.on('ready', () => {
            setTimeout(() => this.onReadyCallback?.(), READY_MS);
        });
        this.socket.on('timeout', () => {
            this.connected = false;
            const socket: Socket = this.socket! as Socket;
            this.onCloseCallback?.();
            socket.end();
            this.stopHeartbeat();
        });
        this.socket.on('error', (error) => {
            this.connected = false;
            this.onErrorCallback?.(error);
            this.stopHeartbeat();
        });
    }

    /**
     * Establishes a secure TCP (TLS) connection to the specified host and port with optional TLS certificates and keys.
     * Configures the socket for secure communication and handles various socket events such as 'data', 'close', 'timeout', and 'error'.
     *
     * - Supports server-side SSL/TLS authentication with optional key and certificate.
     * - Enables Server Name Indication (SNI) for secure connections if `tlsUseSNI` is true.
     * - Handles input piping from `stdin` if `tlsSkipStdInPipe` is false.
     * - Provides error handling for connection and timeouts, with appropriate cleanup and notifications.
     *
     * @returns {void}
     */
    private connectTlsTcp() {
        if (!this.tlsOptions) {
            this.tlsOptions = {};
        }
        if (!this.tlsOptions.host) {
            this.tlsOptions.host = this.host;
        }
        if (!this.tlsOptions.port) {
            this.tlsOptions.port = this.port;
        }

        try {
            this.socket = TLSConnect(
                Number(this.port ?? this.tlsOptions?.port),
                String(this.host ?? this.tlsOptions?.host),
                this.tlsOptions,
                () => {
                    this.connected = true;
                    this.onOpenCallback?.();
                    this.logger.log({
                        level: 'info',
                        message: `FIXParser (${this.protocol?.toUpperCase()}): -- Connected through TLS`,
                    });

                    if (!this.tlsSkipStdInPipe) {
                        process.stdin.pipe(this.socket as TLSSocket);
                        process.stdin.resume();
                    }
                },
            );
        } catch (error: unknown) {
            if (error instanceof Error) {
                this.logger.logError({
                    level: 'info',
                    message: `FIXParser (${this.protocol?.toUpperCase()}): -- Client connection to ${this.host}:${this.port} failed with error: ${error.message}.`,
                });
            } else {
                this.logger.logError({
                    level: 'info',
                    message: `FIXParser (${this.protocol?.toUpperCase()}): -- Client connection to ${this.host}:${this.port} failed with unspecified error.`,
                });
            }
            return;
        }

        this.socket.setEncoding('utf8');
        (this.socket as TLSSocket).pipe(new FrameDecoder()).on('data', (data: string) => {
            const messages: Message[] = this.parse(data.toString());
            let i = 0;
            for (i; i < messages.length; i++) {
                clientProcessMessage(this, messages[i]);
                this.messageStoreIn.add(messages[i]);
                this.onMessageCallback?.(messages[i]);
            }
        });
        this.socket.on('close', () => {
            this.connected = false;
            this.onCloseCallback?.();
            this.stopHeartbeat();
        });
        this.socket.on('timeout', () => {
            const socket: TLSSocket = this.socket! as TLSSocket;
            this.connected = false;
            this.onCloseCallback?.();
            socket.end();
            this.stopHeartbeat();
        });
        this.socket.on('error', (error: Error) => {
            this.connected = false;
            this.onErrorCallback?.(error);
            this.stopHeartbeat();
        });
        setTimeout(() => this.onReadyCallback?.(), READY_MS);
    }

    /**
     * Establishes a WebSocket connection to the specified host and port, with optional proxy support.
     * Configures the socket for communication and handles various WebSocket events such as 'message', 'open', and 'close'.
     *
     * - If the connection string does not include `ws://` or `wss://`, it prepends `ws://` to the host and port.
     * - Optionally uses a proxy if `proxy` is provided.
     * - Handles incoming messages, processes them, and invokes appropriate callbacks.
     * - Handles WebSocket open and close events, managing connection state and triggering callbacks.
     *
     * @returns {void}
     */
    private connectWebsocket() {
        const connectionString: string =
            String(this.host).indexOf('ws://') === -1 && String(this.host).indexOf('wss://') === -1
                ? `ws://${String(this.host)}:${Number(this.port)}`
                : `${String(this.host)}:${Number(this.port)}`;
        if (this.proxy) {
            this.socket = new WebSocket(connectionString, { agent: this.proxy });
        } else {
            this.socket = new WebSocket(connectionString);
        }
        this.socket.on('message', (data: string | Buffer) => {
            const messages: Message[] = this.parse(data.toString());
            let i = 0;
            for (i; i < messages.length; i++) {
                clientProcessMessage(this, messages[i]);
                this.messageStoreIn.add(messages[i]);
                this.onMessageCallback?.(messages[i]);
            }
        });
        this.socket.on('open', () => {
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Connected`,
            });
            this.connected = true;
            this.onOpenCallback?.();
        });
        this.socket.on('close', () => {
            this.connected = false;
            this.onCloseCallback?.();
            this.stopHeartbeat();
        });
        if (
            this.socket.readyState === (WebSocket.OPEN || WebSocket.CLOSED || WebSocket.CLOSING || WebSocket.CONNECTING)
        ) {
            setTimeout(() => this.onReadyCallback?.(), READY_MS);
        }
    }

    /**
     * Get the next outgoing message sequence number.
     *
     * @returns The next outgoing message sequence number.
     */
    public getNextTargetMsgSeqNum(): number {
        return this.messageStoreOut.getNextMsgSeqNum();
    }

    /**
     * Set the next outgoing message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns The next outgoing message sequence number.
     */
    public setNextTargetMsgSeqNum(nextMsgSeqNum: number): number {
        return this.messageStoreOut.setNextMsgSeqNum(nextMsgSeqNum);
    }

    /**
     * Get the next incoming message sequence number.
     *
     * @returns The next outgoing message sequence number.
     */
    public getNextSenderMsgSeqNum(): number {
        return this.messageStoreIn.getNextMsgSeqNum();
    }

    /**
     * Set the next incoming message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns The next incoming message sequence number.
     */
    public setNextSenderMsgSeqNum(nextMsgSeqNum: number): number {
        return this.messageStoreIn.setNextMsgSeqNum(nextMsgSeqNum);
    }

    /**
     * Get current timestamp.
     *
     * @param dateObject - An instance of a Date class.
     * @returns The current timestamp.
     */
    public getTimestamp(dateObject = new Date()): string {
        return timestamp(dateObject, this.logger);
    }

    /**
     * Create an instance of a FIX Message.
     *
     * @param fields - An array of Fields.
     * @returns A FIX Message class instance.
     */
    public createMessage(...fields: Field[]): Message {
        const message: Message = new Message(this.fixVersion, ...fields);
        message.messageSequence = this.getNextTargetMsgSeqNum();
        return message;
    }

    /**
     * Parse a FIX message string into Message instance(s).
     *
     * @param data - FIX message string.
     * @returns FIX Message class instance(s).
     */
    public parse(data: string): Message[] {
        return this.fixParserBase.parse(data);
    }

    /**
     * Send a FIX message.
     *
     * @param message - FIX Message class instance.
     */
    public send(message: Message): void {
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }

        const messageType: Field | undefined = message.getField(FieldType.MsgType);
        if (messageType && messageType.value === MessageType.Logout) {
            this.requestedLogout = true;
        }

        const encodedMessage: string = message.encode();
        if (this.protocol === 'tcp' && this.connected) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as Socket).write(encodedMessage);
            this.messageStoreOut.add(message.clone());
            this.restartHeartbeat();
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol.toUpperCase()}): >> sent`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        } else if (this.protocol === 'websocket' && (this.socket! as WebSocket).readyState === WebSocket.OPEN) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as WebSocket).send(encodedMessage);
            this.messageStoreOut.add(message.clone());
            this.restartHeartbeat();
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol.toUpperCase()}): >> sent`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        } else if ((this.protocol === 'ssl-tcp' || this.protocol === 'tls-tcp') && this.connected) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            (this.socket! as TLSSocket).write(encodedMessage);
            this.messageStoreOut.add(message.clone());
            this.restartHeartbeat();
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol.toUpperCase()}): >> sent`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        } else {
            this.logger.logError({
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Could not send message, no connection`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        }
    }

    /**
     * Get connection status.
     *
     * @returns Current connection status.
     */
    public isConnected(): boolean {
        return this.connected;
    }

    /**
     * Close current connection.
     */
    public close(): void {
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            if (socket) {
                socket.destroy();
                this.connected = false;
            } else {
                this.logger.logError({
                    message: `FIXParser (${this.protocol.toUpperCase()}): -- Could not close socket, connection not open`,
                });
            }
        } else if (this.protocol === 'websocket') {
            const socket: WebSocket = this.socket! as WebSocket;
            if (socket) {
                try {
                    socket.close();
                } catch (error) {
                    this.logger.logError({
                        message: `FIXParser (${this.protocol.toUpperCase()}): -- Could not close socket,`,
                        error,
                    });
                }
                this.connected = false;
            } else {
                this.logger.logError({
                    message: `FIXParser (${this.protocol.toUpperCase()}): -- Could not close socket, connection not open`,
                });
            }
        } else if (this.protocol === 'ssl-tcp' || this.protocol === 'tls-tcp') {
            const socket: TLSSocket = this.socket! as TLSSocket;
            if (socket) {
                socket.destroy();
                this.connected = false;
            } else {
                this.logger.logError({
                    message: `FIXParser (${this.protocol.toUpperCase()}): -- Could not close socket, connection not open`,
                });
            }
        }
    }

    /**
     * Stop heartbeat interval.
     */
    public stopHeartbeat(): void {
        clearInterval(this.heartBeatIntervalId);
    }

    /**
     * Restart heartbeat interval.
     */
    public restartHeartbeat(): void {
        this.stopHeartbeat();
        this.startHeartbeat(this.heartBeatInterval, true);
    }

    /**
     * Start heartbeat interval.
     *
     * @param heartBeatInterval - Heartbeat interval in seconds.
     * @param disableLog - Whether to disable heartbeat logs.
     */
    public startHeartbeat(heartBeatInterval: number = this.heartBeatInterval, disableLog?: boolean): void {
        this.stopHeartbeat();
        if (!disableLog) {
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Heartbeat configured to ${heartBeatInterval} seconds`,
            });
        }
        this.heartBeatInterval = heartBeatInterval;
        this.heartBeatIntervalId = setInterval(() => {
            const heartBeatMessage: Message = heartBeat(this);
            this.send(heartBeatMessage);
            const encodedMessage: string = heartBeatMessage.encode();
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): >> sent Heartbeat`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        }, this.heartBeatInterval * 1000);
    }
}

export * from './fieldtypes';
export type { ConnectionOptions };
export { Logger } from './logger/Logger';
export { LicenseManager } from './licensemanager/LicenseManager';
export { MessageBuffer };
export { Constants };
export { Field };
export { Message };
export { FIXParser };
export type { IFIXParser };
export type { LogMessage, Level, ILogTransporter } from 'fixparser-common';
export type { FIXValue } from './util/util';
export type { LogOptions, Format } from './logger/Logger';
export type { IMessageStore } from 'fixparser-common';
export type { Protocol, Options, BaseOptions } from './FIXParserBase';
export type { MessageError } from './message/Message';
