import {
    type CleartextMessage,
    type Key,
    type VerifyMessageResult,
    readCleartextMessage,
    readKey,
    verify,
    // @ts-ignore
} from '../../../../node_modules/openpgp/dist/openpgp.mjs';
// TODO: revert back to 'openpgp' once the issue has been resolved here: https://github.com/openpgpjs/openpgpjs/issues/1814
import type { Logger } from '../logger/Logger';
import { atob } from './LicenseManagerUtils';

const missingOrEmpty = (value?: string | undefined): boolean => {
    return value == null || value.length === 0;
};

declare global {
    const __RELEASE_INFORMATION__: string;
}

/**
 * The LicenseManager class is responsible for validating license keys,
 * ensuring that only users with valid licenses can access the
 * Pro version features of the FIXParsers.
 *
 * @public
 */
class LicenseManager {
    static readonly #RELEASE_INFORMATION: string | undefined =
        __RELEASE_INFORMATION__ || process.env.__RELEASE_INFORMATION__;
    static #licenseKey: string;
    static #licenseKeyId: string | undefined = undefined;
    static #licenseExpiry: string | undefined = undefined;
    static #licenseIsTrial: boolean | undefined = false;
    static #licenseProcessing = false;
    static #logger: Logger;
    static readonly #PUBLIC_KEY = `-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEYSAlmRYJKwYBBAHaRw8BAQdATxsL8ZGu79iIXGoMwAGxis0Ot6zN+c+H
FiEfymdU5QHNIGZpeHBhcnNlci5pbyA8aW5mb0BmaXhwYXJzZXIuaW8+wowE
EBYKAB0FAmEgJZkECwkHCAMVCAoEFgACAQIZAQIbAwIeAQAhCRDpKZsYDAPF
cxYhBOT5iMsY4w3omYDb+ukpmxgMA8VziRIA/A5mUMldqdrKsxJddLiMfJ30
DVyKt8dXn6Fu6b2riwrHAP0YqK+goCIt7y6de9KTWmnWBgMAxX5XNAK5B41A
DsfYCM44BGEgJZkSCisGAQQBl1UBBQEBB0A0y8VUtHP6LESsJZN7yEpOKHtR
2JCLu/swlNU2QfojLwMBCAfCeAQYFggACQUCYSAlmQIbDAAhCRDpKZsYDAPF
cxYhBOT5iMsY4w3omYDb+ukpmxgMA8VzB/QBAOzjjWDBPlSDYpPMv13s8OS6
Tzi6Zidom0ZY6lkJMBNCAQCNwsJXAWbxHmHmMJq5yLe5GaL9YQWxeUM4AdUa
GqHtBQ===YpfV
-----END PGP PUBLIC KEY BLOCK-----`;

    public static readonly ERROR_MESSAGE_NO_LICENSE = 'Requires FIXParser Pro license';

    private constructor() {
        // Private constructor prevents instantiation
    }

    public static readonly validateLicense: () => boolean = Object.freeze(() => {
        if (LicenseManager.#licenseProcessing || LicenseManager.#licenseKeyId) {
            return true;
        }
        LicenseManager.outputInvalidLicenseKey();
        return false;
    });

    public static setLogger(loggerInstance: Logger) {
        LicenseManager.#logger = loggerInstance;
    }

    public static async setLicenseKey(licenseKey: string): Promise<void> {
        if (LicenseManager.#licenseProcessing) {
            return;
        }

        if (!(`${licenseKey}`.startsWith('LS0tLS1CRU') || `${licenseKey}`.startsWith('Ci0tLS0tQk'))) {
            LicenseManager.outputInvalidLicenseKey();
            LicenseManager.#licenseProcessing = false;
            return;
        }

        LicenseManager.#licenseProcessing = true;
        LicenseManager.#licenseKey = licenseKey;

        if (missingOrEmpty(LicenseManager.#licenseKey)) {
            LicenseManager.outputMissingLicenseKey();
        } else {
            try {
                const readPublicKey: Key = await readKey({ armoredKey: LicenseManager.#PUBLIC_KEY });
                const signedMessage: CleartextMessage = await readCleartextMessage({
                    cleartextMessage: atob(LicenseManager.#licenseKey)!,
                });
                const verificationResult: VerifyMessageResult = await verify({
                    message: signedMessage as any,
                    verificationKeys: readPublicKey,
                });
                const { verified, keyID } = verificationResult.signatures[0];
                await verified;
                const [, , expiryTimestamp, isTrial]: string[] = signedMessage.getText().split('|');
                LicenseManager.validateKey(keyID.toHex(), Number(expiryTimestamp), isTrial === 'true');
            } catch (e) {
                LicenseManager.outputInvalidLicenseKey();
                LicenseManager.#licenseProcessing = false;
                return;
            }
        }
        return Promise.resolve();
    }

    private static getReleaseDate(): Date {
        return new Date(Number.parseInt(atob(LicenseManager.#RELEASE_INFORMATION!)!, 10));
    }

    private static formatDate(date: Date): string {
        const monthNames: string[] = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        const day: number = date.getDate();
        const monthIndex: number = date.getMonth();
        const year: number = date.getFullYear();

        return `${day} ${monthNames[monthIndex]} ${year}`;
    }

    private static validateKey(keyId: string, expiryTimestamp: number, isTrial: boolean): boolean {
        const releaseDate: Date = LicenseManager.getReleaseDate();
        const expiry: Date = new Date(expiryTimestamp);

        let valid = false;
        let current = false;
        if (!Number.isNaN(expiry.getTime())) {
            valid = true;
            current = releaseDate < expiry;
        }

        if (!valid) {
            LicenseManager.outputInvalidLicenseKey();
            return false;
        }
        if (!current) {
            const formattedExpiryDate: string = LicenseManager.formatDate(expiry);
            const formattedReleaseDate: string = LicenseManager.formatDate(releaseDate);
            LicenseManager.outputIncompatibleVersion(formattedExpiryDate, formattedReleaseDate);
            return false;
        }
        LicenseManager.#licenseKeyId = keyId;
        LicenseManager.#licenseProcessing = false;
        LicenseManager.#licenseExpiry = LicenseManager.formatDate(expiry);
        LicenseManager.#licenseIsTrial = isTrial;
        LicenseManager.outputValidLicense();
        return true;
    }

    private static outputValidLicense(): void {
        const licenseMessage = `[FIXParser Pro ${LicenseManager.#licenseIsTrial ? 'TRIAL ' : ''}License] - Valid until ${
            LicenseManager.#licenseExpiry
        }`;
        if (LicenseManager.#logger) {
            LicenseManager.#logger.log({
                level: 'info',
                message: licenseMessage,
            });
        } else {
            console.log(licenseMessage);
        }
    }

    private static outputInvalidLicenseKey(): void {
        console.error(`
****************************************************************************************************
************************************** FIXParser Pro License ***************************************
***************************************** Invalid License ******************************************
*                  -- Access to this feature requires license for FIXParser Pro --                 *
*        Your license for FIXParser Pro is not valid - please contact sales@fixparser.dev to       *
*                                     obtain a valid license.                                      *
****************************************************************************************************
****************************************************************************************************
`);
    }

    private static outputMissingLicenseKey(): void {
        console.error(`
****************************************************************************************************
*************************************** FIXParser Pro License **************************************
*************************************** License Key Not Found **************************************
*               Please visit https://fixparser.dev to purchase a FIXParser Pro license             *
****************************************************************************************************
****************************************************************************************************
`);
    }

    private static outputIncompatibleVersion(formattedExpiryDate: string, formattedReleaseDate: string): void {
        const padStringRight = (str: string, length: number): string => {
            if (length <= str.length) {
                return str;
            }
            return str + ' '.repeat(length - str.length);
        };
        console.error(`
****************************************************************************************************
****************************************************************************************************
*                                       FIXParser Pro License                                      *
*                  License not compatible with installed version of FIXParser Pro.                 *
*                                                                                                  *
* Your FIXParser License entitles you to all versions of FIXParser that we release within the time *
* covered by your license - typically we provide one year licenses which entitles you to all       *
* releases / updates of FIXParser within that year. Your license has an end (expiry) date which    *
* stops the license key working with versions of FIXParser released after the license end date.    *
* The license key that you have expires on ${padStringRight(`${formattedExpiryDate}, however, the version of FIXParser`, 56)}*
* you are trying to use was released on ${padStringRight(`${formattedReleaseDate}.`, 59)}*
*                                                                                                  *
* Please contact sales@fixparser.dev to renew your subscription to new versions and get a new      *
* license key to work with this version of FIXParser.                                              *
****************************************************************************************************
****************************************************************************************************
`);
    }
}

// Prevent LicenseManager.validateLicense from being modified
Object.defineProperty(LicenseManager, 'validateLicense', {
    writable: false,
    configurable: false,
});

export { LicenseManager };
