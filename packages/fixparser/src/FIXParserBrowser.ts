import { type IMessageStore, MessageBuffer } from 'fixparser-common';
import type { ProxyAgent } from 'proxy-agent';
import {
    type BaseOptions,
    type ConnectionType,
    FIXParserBase,
    type Options as FIXParserOptions,
    type Protocol,
} from './FIXParserBase';
import type { IFIXParser } from './IFIXParser';
import { Field } from './fields/Field';
import * as Constants from './fieldtypes';
import { LicenseManager } from './licensemanager/LicenseManager';
import { type LogOptions, Logger } from './logger/Logger';
import { Message } from './message/Message';
import { heartBeat } from './messagetemplates/MessageTemplates';
import { clientProcessMessage } from './session/ClientMessageProcessor';
import {
    DEFAULT_FIX_VERSION,
    DEFAULT_HEARTBEAT_SECONDS,
    type Parser,
    type Version,
    parseFixVersion,
    timestamp,
    version,
} from './util/util';

export type Options = Pick<
    FIXParserOptions,
    | 'host'
    | 'port'
    | 'sender'
    | 'target'
    | 'heartbeatIntervalSeconds'
    | 'fixVersion'
    | 'messageStoreIn'
    | 'messageStoreOut'
    | 'logging'
    | 'logOptions'
    | 'onMessage'
    | 'onOpen'
    | 'onError'
    | 'onClose'
    | 'onReady'
>;

class FIXParserBrowser implements IFIXParser {
    /* FIXParser Version */
    public static readonly version: Version = version;
    /* Name of current parser name */
    public readonly parserName: Parser = 'FIXParserBrowser';
    /* Instance of FIXParser base class */
    public readonly fixParserBase: FIXParserBase = new FIXParserBase();
    /* Heartbeat interval ID */
    public heartBeatIntervalId: ReturnType<typeof setInterval> | undefined = undefined;
    /* Websocket connection */
    public socket: WebSocket | undefined = undefined;
    /* Current connection status */
    public connected = false;
    /* Target hostname or IP address */
    public host: string | undefined = undefined;
    /* Target port */
    public port: number | undefined = undefined;
    /* Type of protocol */
    public protocol: Protocol | undefined = 'websocket';
    /* Assigned value used to identify firm sending message, SenderCompID tag 49 */
    public sender: string | undefined = undefined;
    /* Assigned value used to identify receiving firm, TargetCompID tag 56 */
    public target: string | undefined = undefined;
    /* Heartbeat interval in seconds */
    public heartBeatInterval: number = DEFAULT_HEARTBEAT_SECONDS;
    /* Protocol version, used by BeginString tag 8 */
    public fixVersion: string = DEFAULT_FIX_VERSION;
    /* Message buffer of 2500 incoming messages */
    public messageStoreIn: IMessageStore<Message> = new MessageBuffer();
    /* Message buffer of 2500 outgoing messages */
    public messageStoreOut: IMessageStore<Message> = new MessageBuffer();
    /* Indicates whether this instance is acceptor or initiator */
    public connectionType: ConnectionType = 'initiator';
    /* Logger instance */
    public logger: Logger = new Logger();
    /* Logging */
    public logging = true;
    /* Log options */
    public logOptions: LogOptions | undefined;
    /* Proxy */
    public proxy?: ProxyAgent;

    constructor(
        options: BaseOptions = {
            logging: true,
            logOptions: undefined,
            fixVersion: DEFAULT_FIX_VERSION,
            skipValidation: false,
        },
    ) {
        this.logging = options.logging ?? true;
        this.logOptions = options.logOptions;
        this.fixParserBase.fixVersion = parseFixVersion(options.fixVersion);
        this.fixVersion = this.fixParserBase.fixVersion;
        this.fixParserBase.skipValidation = options.skipValidation;
    }

    /**
     * onMessageCallback is called when a message has been received
     *
     * @remarks FIXParser~onMessageCallback
     * @param message - A Message class instance
     */
    private onMessageCallback: Options['onMessage'] = (message) => {};

    /**
     * onOpenCallback is called the FIX connection has been initiated
     *
     * @remarks FIXParser~onOpenCallback
     */
    private onOpenCallback: Options['onOpen'] = () => {};

    /**
     * onErrorCallback is called the FIX connection failed
     *
     * @remarks FIXParser~onErrorCallback
     * @param error - Error object
     */
    private onErrorCallback: Options['onError'] = (error) => {};

    /**
     * onCloseCallback is called the FIX connection has been closed
     *
     * @remarks FIXParser~onCloseCallback
     */
    private onCloseCallback: Options['onClose'] = () => {};

    /**
     * onReadyCallback is called the FIX connection is opened and ready
     *
     * @remarks FIXParser~onReadyCallback
     */
    private onReadyCallback: Options['onReady'] = () => {};

    /**
     * Establishes a connection to a FIX server using the specified options. The connection
     * is made via the WebSocket protocol, and various callbacks can be provided to handle
     * events like message reception, connection opening, errors, and closing.
     *
     * - Sets up logging with optional configurations.
     * - Validates the license before proceeding with the connection.
     * - Initializes connection parameters including sender, target, and heartbeat interval.
     * - Establishes the connection over WebSocket by calling `connectWebsocket()`.
     *
     * @param {Options} [options={}] - The configuration options for the connection.
     * @param {string} [options.host='localhost'] - The host address of the FIX server.
     * @param {number} [options.port=9878] - The port number for the FIX server.
     * @param {string} [options.sender='SENDER'] - The sender identifier for the connection.
     * @param {string} [options.target='TARGET'] - The target identifier for the connection.
     * @param {number} [options.heartbeatIntervalSeconds=DEFAULT_HEARTBEAT_SECONDS] - The heartbeat interval in seconds.
     * @param {string} [options.messageStoreIn=new MessageBuffer()] - Optional custom message buffer for incoming messages.
     * @param {string} [options.messageStoreOut=new MessageBuffer()] - Optional custom message buffer for outgoing messages.
     * @param {function} [options.onMessage=this.onMessageCallback] - Callback for handling incoming messages.
     * @param {function} [options.onOpen=this.onOpenCallback] - Callback for handling connection open event.
     * @param {function} [options.onError=this.onErrorCallback] - Callback for handling errors.
     * @param {function} [options.onClose=this.onCloseCallback] - Callback for handling connection close event.
     * @param {function} [options.onReady=this.onReadyCallback] - Callback for handling ready event.
     *
     * @returns {void}
     */
    public connect(options: Options = {}): void {
        const {
            host = 'localhost',
            port = 9878,
            sender = 'SENDER',
            target = 'TARGET',
            heartbeatIntervalSeconds = DEFAULT_HEARTBEAT_SECONDS,
            messageStoreIn = new MessageBuffer(),
            messageStoreOut = new MessageBuffer(),
            onMessage = this.onMessageCallback,
            onOpen = this.onOpenCallback,
            onError = this.onErrorCallback,
            onClose = this.onCloseCallback,
            onReady = this.onReadyCallback,
        } = options;
        this.logger = new Logger(
            this.logOptions
                ? {
                      name: this.logOptions.name ?? 'fixparser-browser',
                      ...this.logOptions,
                  }
                : {
                      name: 'fixparser-browser',
                      format: 'json',
                  },
        );
        if (!this.logging) {
            this.logger.silent = true;
        }
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        this.connectionType = 'initiator';
        this.messageStoreIn = messageStoreIn;
        this.messageStoreOut = messageStoreOut;
        this.sender = sender;
        this.target = target;
        this.port = port;
        this.host = host;
        this.protocol = 'websocket';
        this.heartBeatInterval = heartbeatIntervalSeconds;

        this.onMessageCallback = onMessage;
        this.onOpenCallback = onOpen;
        this.onErrorCallback = onError;
        this.onCloseCallback = onClose;
        this.onReadyCallback = onReady;

        this.connectWebsocket();
    }

    /**
     * Establishes a WebSocket connection to the FIX server. This method configures event listeners
     * for WebSocket events such as connection opening, closing, message reception, and logging the events.
     *
     * - Builds the WebSocket URL based on the provided host and port.
     * - Sets up event listeners for `open`, `close`, and `message` events.
     * - Logs connection status and relevant details such as `readyState` during connection and closure.
     * - On receiving a message, it is parsed using `fixParserBase.parse()`, and each parsed message is processed and passed to the callback functions.
     * - The `onOpenCallback`, `onCloseCallback`, `onMessageCallback` methods are invoked when corresponding events occur.
     * - The heartbeat mechanism is stopped when the connection closes.
     *
     * @returns {void}
     */
    private connectWebsocket() {
        this.socket = new WebSocket(
            String(this.host).indexOf('ws://') === -1 && String(this.host).indexOf('wss://') === -1
                ? `ws://${this.host}:${this.port}`
                : `${this.host}:${this.port}`,
        );
        this.socket.addEventListener('open', (event) => {
            this.connected = true;
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Connected: ${event.target}, readyState: ${
                    this.socket?.readyState
                }`,
            });
            this.onOpenCallback?.();
        });
        this.socket.addEventListener('close', (event) => {
            this.connected = false;
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Connection closed: ${event.target}, readyState: ${
                    this.socket?.readyState
                }`,
            });
            this.onCloseCallback?.();
            this.stopHeartbeat();
        });
        this.socket.addEventListener('message', (event) => {
            const messages: Message[] = this.fixParserBase.parse(event.data as string);
            let i = 0;
            for (i; i < messages.length; i++) {
                clientProcessMessage(this, messages[i]);
                this.messageStoreIn.add(messages[i]);
                this.onMessageCallback?.(messages[i]);
            }
        });
    }

    /**
     * Get the next outgoing message sequence number.
     *
     * @returns The next outgoing message sequence number.
     */
    public getNextTargetMsgSeqNum(): number {
        return this.messageStoreOut.getNextMsgSeqNum();
    }

    /**
     * Set the next outgoing message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns The next outgoing message sequence number.
     */
    public setNextTargetMsgSeqNum(nextMsgSeqNum: number): number {
        return this.messageStoreOut.setNextMsgSeqNum(nextMsgSeqNum);
    }

    /**
     * Get current timestamp.
     *
     * @param dateObject - An instance of a Date class.
     * @returns The current timestamp.
     */
    public getTimestamp(dateObject = new Date()): string {
        return timestamp(dateObject, this.logger);
    }

    /**
     * Create an instance of a FIX Message.
     *
     * @param fields - An array of Fields.
     * @returns A FIX Message class instance.
     */
    public createMessage(...fields: Field[]): Message {
        return new Message(this.fixVersion, ...fields);
    }

    /**
     * Parse a FIX message string into Message instance(s).
     *
     * @param data - FIX message string.
     * @returns FIX Message class instance(s).
     */
    public parse(data: string): Message[] {
        return this.fixParserBase.parse(data);
    }

    /**
     * Send a FIX message.
     *
     * @param message - FIX Message class instance.
     */
    public send(message: Message): void {
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        if (this.socket?.readyState === 1) {
            this.setNextTargetMsgSeqNum(this.getNextTargetMsgSeqNum() + 1);
            this.socket?.send(message.encode());
            this.messageStoreOut.add(message.clone());
            this.restartHeartbeat();
        } else {
            this.logger.logError({
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Could not send message, socket not open`,
                fix: message,
            });
        }
    }

    /**
     * Get connection status.
     *
     * @returns Current connection status.
     */
    public isConnected(): boolean {
        return this.connected;
    }

    /**
     * Close current connection.
     */
    public close(): void {
        this.socket?.close();
        this.connected = false;
    }

    /**
     * Stop heartbeat interval.
     */
    public stopHeartbeat(): void {
        clearInterval(this.heartBeatIntervalId);
    }

    /**
     * Restart heartbeat interval.
     */
    public restartHeartbeat(): void {
        this.stopHeartbeat();
        this.startHeartbeat(this.heartBeatInterval, true);
    }

    /**
     * Start heartbeat interval.
     *
     * @param heartBeatInterval - Heartbeat interval in seconds.
     * @param disableLog - Whether to disable heartbeat logs.
     */
    public startHeartbeat(heartBeatInterval: number = this.heartBeatInterval, disableLog?: boolean): void {
        this.stopHeartbeat();
        if (!disableLog) {
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): -- Heartbeat configured to ${heartBeatInterval} seconds`,
            });
        }
        this.heartBeatInterval = heartBeatInterval;
        this.heartBeatIntervalId = setInterval(() => {
            const heartBeatMessage: Message = heartBeat(this);
            this.send(heartBeatMessage);
            const encodedMessage: string = heartBeatMessage.encode();
            this.logger.log({
                level: 'info',
                message: `FIXParser (${this.protocol?.toUpperCase()}): >> sent Heartbeat`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        }, this.heartBeatInterval * 1000);
    }
}

export * from './fieldtypes';
export { Logger } from './logger/Logger';
export { LicenseManager } from './licensemanager/LicenseManager';
export { MessageBuffer };
export { Constants };
export { Field };
export { Message };
export { FIXParserBrowser as FIXParser };
export type { IFIXParser };
export type { LogMessage, Level, ILogTransporter } from 'fixparser-common';
export type { FIXValue } from './util/util';
export type { LogOptions, Format } from './logger/Logger';
export type { IMessageStore } from 'fixparser-common';
export type { Protocol, BaseOptions } from './FIXParserBase';
export type { MessageError } from './message/Message';
