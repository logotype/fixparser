import { Field as FieldType } from '../fieldtypes/Field';
import { Message as MessageType } from '../fieldtypes/Message';
import { GROUPS, type ISpecGroups } from '../spec/SpecGroups';

export class Groups {
    public groups: ISpecGroups[] = GROUPS;
    public cacheMap = new Map<number, ISpecGroups>();
    public cacheMapByName = new Map<string, ISpecGroups>();

    constructor() {
        this.groups.forEach((component) => {
            this.cacheMap.set(component.id, component);
        });
        this.groups.forEach((component) => {
            this.cacheMapByName.set(component.name, component);
        });
    }

    public find(componentId: number): ISpecGroups | undefined {
        return this.cacheMap.get(componentId);
    }

    public findByName(name: string): ISpecGroups | undefined {
        return this.cacheMapByName.get(name);
    }

    public filterMapByNumInGroupId(tag: number, messageType: string): ISpecGroups | undefined {
        // If tag === 268 and MessageType W then we have a MDFullGrp (pick group 2031)
        if (tag === FieldType.NoMDEntries && messageType === MessageType.MarketDataSnapshotFullRefresh) {
            return this.find(2031);
        }

        // If tag === 268 and MessageType X then we have a MDIncGrp (pick group 2032)
        if (tag === FieldType.NoMDEntries && messageType === MessageType.MarketDataIncrementalRefresh) {
            return this.find(2032);
        }

        for (const [, value] of this.cacheMap.entries()) {
            if (value.numInGroup?.id === tag) {
                return value;
            }
        }

        return undefined;
    }
}
