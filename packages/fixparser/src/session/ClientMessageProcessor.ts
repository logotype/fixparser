import type { FIXParser } from '../FIXParser';
import type { FIXParser as FIXParserBrowser } from '../FIXParserBrowser';
import { Message as MessageType } from '../fieldtypes/Message';
import type { Message } from '../message/Message';
import { handleLogon } from './SessionLogon';
import { handleLogout } from './SessionLogout';
import { handleResendRequest } from './SessionResendRequest';
import { handleSequence } from './SessionSequence';
import { handleSequenceReset } from './SessionSequenceReset';
import { handleTestRequest } from './SessionTestRequest';

export const clientProcessMessage = (parser: FIXParser | FIXParserBrowser, message: Message): void => {
    handleSequence(parser, message);
    parser.logger.log({
        level: 'info',
        message: `FIXParser (${parser.protocol?.toUpperCase()}): << received ${message.description}`,
        fix: message.encode('|'),
    });
    parser.restartHeartbeat();

    if (message.messageType === MessageType.SequenceReset) {
        handleSequenceReset(parser, message);
    } else if (message.messageType === MessageType.TestRequest) {
        handleTestRequest(parser, message);
    } else if (message.messageType === MessageType.Logon) {
        handleLogon(parser, parser.messageStoreOut, message);
    } else if (message.messageType === MessageType.Logout) {
        handleLogout(parser, message);
    } else if (message.messageType === MessageType.ResendRequest) {
        handleResendRequest(parser, parser.messageStoreOut, message);
    }
    parser.messageStoreIn.setNextMsgSeqNum(parser.messageStoreIn.getNextMsgSeqNum() + 1);
};
