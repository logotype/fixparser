import type { IMessageStore } from 'fixparser-common';
import type { IMessage } from 'fixparser-common';
import type { IFIXParser } from '../IFIXParser';
import { Field } from '../fields/Field';
import { GapFillFlag, MsgType, PossDupFlag } from '../fieldtypes';
import { Field as FieldType } from '../fieldtypes/Field';
import type { Message } from '../message/Message';
import { sequenceReset } from './../messagetemplates/MessageTemplates';

export const handleResendRequest = (
    parser: IFIXParser,
    messageStore: IMessageStore<Message>,
    message: Message,
): void => {
    const from: number | undefined = message.getField(FieldType.BeginSeqNo)
        ? Number(message.getField(FieldType.BeginSeqNo)?.value)
        : undefined;
    let to: number | undefined = message.getField(FieldType.EndSeqNo)
        ? Number(message.getField(FieldType.EndSeqNo)?.value)
        : undefined;

    if (from === undefined || to === undefined) {
        parser.logger.logWarning({
            message: `${
                parser.parserName
            } (${parser.protocol?.toUpperCase()}): -- Ignoring ResendRequest - Invalid BeginSeqNo or EndSeqNo`,
        });
        return;
    }

    if (to === 0) {
        to = parser.getNextTargetMsgSeqNum() > from ? parser.getNextTargetMsgSeqNum() - 1 : from;
    }

    // If the message store does not have the requested messages then force reset sequence numbers
    if (messageStore.size() === 0) {
        parser.logger.logWarning({
            message: `${
                parser.parserName
            } (${parser.protocol?.toUpperCase()}): -- Message Store does not contain the messages requested by the ResendRequest. Sending SequenceReset.`,
        });
        parser.send(sequenceReset(parser, from, to, GapFillFlag.SequenceReset));
        parser.setNextTargetMsgSeqNum(to + 1);
        return;
    }

    if (from >= 1 && to >= from) {
        let i: number = from;
        let targetMsgSeqNum: number = to;
        for (i; i <= to; i++) {
            const messageBySequenceFromStore: IMessage | undefined = messageStore.getByMsgSequence(i);
            if (messageBySequenceFromStore) {
                const messageBySequence = messageBySequenceFromStore as Message;
                const messageType = messageBySequence.getField(FieldType.MsgType)?.value;
                if (
                    // Administrative messages
                    messageType === MsgType.Heartbeat ||
                    messageType === MsgType.Logon ||
                    messageType === MsgType.Logout ||
                    messageType === MsgType.Reject ||
                    messageType === MsgType.ResendRequest ||
                    messageType === MsgType.SequenceReset ||
                    messageType === MsgType.TestRequest ||
                    messageType === MsgType.XMLnonFIX ||
                    // Session-level messages
                    messageType === MsgType.MarketDataSnapshotFullRefresh ||
                    messageType === MsgType.SecurityDefinition
                ) {
                    if (from === to || i < to) {
                        const seqResetMessage = sequenceReset(parser, i, i, GapFillFlag.GapFillMessage);
                        parser.send(seqResetMessage);
                        parser.logger.log({
                            level: 'info',
                            message: `${parser.parserName} (${parser.protocol?.toUpperCase()}): >> resending message with sequence ${i} (replaced administrative message)`,
                            fix: seqResetMessage.encode('|'),
                        });
                    }
                } else {
                    messageBySequence.removeFieldByTag(FieldType.PossDupFlag);
                    messageBySequence.addField(new Field(FieldType.PossDupFlag, PossDupFlag.PossibleDuplicate));

                    if (messageBySequence.getField(FieldType.SendingTime)) {
                        const originalSendingTime: string = `${messageBySequence.getField(FieldType.SendingTime)?.value}`;
                        messageBySequence.removeFieldByTag(FieldType.SendingTime);
                        messageBySequence.addField(new Field(FieldType.SendingTime, parser.getTimestamp(new Date())));

                        messageBySequence.removeFieldByTag(FieldType.OrigSendingTime);
                        messageBySequence.addField(new Field(FieldType.OrigSendingTime, originalSendingTime));
                    }
                    parser.send(messageBySequence);
                    parser.logger.log({
                        level: 'info',
                        message: `${parser.parserName} (${parser.protocol?.toUpperCase()}): >> resending message with sequence ${i}`,
                        fix: messageBySequence.encode('|'),
                    });
                }
                targetMsgSeqNum = i + 1;
            } else {
                parser.send(sequenceReset(parser, i, to, GapFillFlag.GapFillMessage));
                parser.logger.logWarning({
                    message: `${
                        parser.parserName
                    } (${parser.protocol?.toUpperCase()}): -- Could not find message with sequence ${i}. Sending SequenceReset-GapFill from ${i} to ${to}...`,
                });
                targetMsgSeqNum = to + 1;
                break;
            }
        }
        parser.setNextTargetMsgSeqNum(targetMsgSeqNum);
        parser.logger.log({
            level: 'info',
            message: `${parser.parserName} (${parser.protocol?.toUpperCase()}): >> sent message sequence from ${from} to ${to}`,
        });
    } else {
        parser.logger.logWarning({
            message: `${parser.parserName} (${parser.protocol?.toUpperCase()}): -- BeginSeqNo<${from}> or EndSeqNo<${to}> out of range`,
        });
    }
};
