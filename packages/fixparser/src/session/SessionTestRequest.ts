import type { IFIXParser } from '../IFIXParser';
import { Field } from '../fields/Field';
import { Field as FieldType } from '../fieldtypes/Field';
import type { Message } from '../message/Message';
import { heartBeat } from '../messagetemplates/MessageTemplates';

export const handleTestRequest = (parser: IFIXParser, message: Message): void => {
    let heartBeatMessage: Message = heartBeat(parser);
    const testReqIdValue: string | undefined = (message.getField(FieldType.TestReqID)?.value as string) ?? null;
    if (testReqIdValue !== null && testReqIdValue !== '') {
        const testReqId: Field = new Field(FieldType.TestReqID, testReqIdValue);
        heartBeatMessage = heartBeat(parser, testReqId);
        parser.send(heartBeatMessage);
        parser.logger.log({
            level: 'info',
            message: `${
                parser.parserName
            } (${parser.protocol?.toUpperCase()}): >> responded to TestRequest with Heartbeat<TestReqID=${testReqIdValue}>`,
            fix: heartBeatMessage.encode('|'),
        });
    } else {
        parser.send(heartBeatMessage);
        parser.logger.log({
            level: 'info',
            message: `${parser.parserName} (${parser.protocol?.toUpperCase()}): >> responded to TestRequest with Heartbeat`,
            fix: heartBeatMessage.encode('|'),
        });
    }
};
