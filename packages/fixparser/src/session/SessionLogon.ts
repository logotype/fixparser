import type { IMessageStore } from 'fixparser-common';
import type { IFIXParser } from '../IFIXParser';
import { Field } from '../fields/Field';
import { EncryptMethod, ResetSeqNumFlag } from '../fieldtypes';
import { Field as FieldType } from '../fieldtypes/Field';
import { Message as MessageType } from '../fieldtypes/Message';
import type { Message } from '../message/Message';

export const handleLogon = (parser: IFIXParser, messageStore: IMessageStore<Message>, message: Message): boolean => {
    if (parser.isLoggedIn) {
        if (parser.connectionType === 'acceptor') {
            if (
                message.getField(FieldType.ResetSeqNumFlag) &&
                message.getField(FieldType.ResetSeqNumFlag)!.value!.toString() === 'Y'
            ) {
                parser.logger.log({
                    level: 'info',
                    message: `FIXServer (${parser.protocol!.toUpperCase()}): -- Logon acknowledged by acceptor.`,
                    fix: message.encode('|'),
                });
                parser.messageStoreIn.setNextMsgSeqNum(1);
            }
        } else {
            parser.logger.log({
                level: 'info',
                message: `FIXServer (${parser.protocol!.toUpperCase()}): -- Logon acknowledged by initiator.`,
                fix: message.encode('|'),
            });
        }
        return true;
    }

    const fixVersion: string | undefined = String(message.getField(FieldType.BeginString)?.value);
    let validSender = true;
    let validTarget = true;

    if (fixVersion) {
        parser.logger.log({
            level: 'info',
            message: `FIXServer (${parser.protocol?.toUpperCase()}): -- FIX version set to ${fixVersion}`,
            fix: message.encode('|'),
        });
        parser.fixVersion = fixVersion;
        if (parser.fixParser) {
            parser.fixParser.fixVersion = fixVersion;
        }
    }

    const target: string | undefined = message.getField(FieldType.TargetCompID)?.value?.toString() ?? parser.sender;
    const sender: string | undefined = message.getField(FieldType.SenderCompID)?.value?.toString() ?? parser.target;

    if (target && target !== parser.sender) {
        parser.logger.logWarning({
            message: `FIXServer (${parser.protocol!.toUpperCase()}): -- Expected TargetCompID=${
                parser.sender
            }, but got ${target}`,
            fix: message.encode('|'),
        });
        validTarget = false;
    }
    if (sender && sender !== parser.target) {
        parser.logger.logWarning({
            message: `FIXServer (${parser.protocol?.toUpperCase()}): -- Expected SenderCompID=${
                parser.target
            }, but got ${sender}`,
            fix: message.encode('|'),
        });
        validSender = false;
    }

    if (validSender && validTarget) {
        if (parser.connectionType === 'acceptor') {
            const logonAcknowledge: Message = parser.createMessage(
                new Field(FieldType.MsgType, MessageType.Logon),
                new Field(FieldType.MsgSeqNum, message.getField(FieldType.MsgSeqNum)?.value?.toString()),
                new Field(FieldType.SenderCompID, target),
                new Field(FieldType.SendingTime, parser.getTimestamp(new Date())),
                new Field(FieldType.TargetCompID, sender),
                new Field(
                    FieldType.ResetSeqNumFlag,
                    message.getField(FieldType.ResetSeqNumFlag)?.value?.toString() === 'Y'
                        ? ResetSeqNumFlag.Yes
                        : ResetSeqNumFlag.No,
                ),
                new Field(FieldType.EncryptMethod, EncryptMethod.None),
                new Field(
                    FieldType.HeartBtInt,
                    message.getField(FieldType.HeartBtInt)
                        ? (message.getField(FieldType.HeartBtInt)?.value as number)
                        : parser.heartBeatInterval,
                ),
            );
            parser.send(logonAcknowledge);
            parser.logger.log({
                level: 'info',
                message: `FIXServer (${parser.protocol?.toUpperCase()}): >> sent Logon acknowledge`,
                fix: logonAcknowledge.encode('|'),
            });
        }

        if (
            message.getField(FieldType.ResetSeqNumFlag) &&
            message.getField(FieldType.ResetSeqNumFlag)?.value?.toString() === 'Y'
        ) {
            parser.logger.log({
                level: 'info',
                message: `FIXServer (${parser.protocol?.toUpperCase()}): -- Logon contains ResetSeqNumFlag=Y, resetting sequence numbers to 1`,
                fix: message.encode('|'),
            });
            parser.messageStoreIn.setNextMsgSeqNum(1);
            parser.setNextTargetMsgSeqNum(2);
        }

        parser.isLoggedIn = true;
        parser.logger.log({
            level: 'info',
            message: `FIXServer (${parser.protocol?.toUpperCase()}): >> Logon successful by ${parser.connectionType}`,
            fix: message.encode('|'),
        });
        const heartBeatInterval: number = message.getField(FieldType.HeartBtInt)
            ? Number(message.getField(FieldType.HeartBtInt)?.value!)
            : parser.heartBeatInterval;
        parser.heartBeatInterval = heartBeatInterval;
        if (parser.fixParser) {
            parser.fixParser.heartBeatInterval = heartBeatInterval;
        }
        parser.startHeartbeat(heartBeatInterval);
        return true;
    }
    const logonReject: Message = parser.createMessage(
        new Field(FieldType.MsgType, MessageType.Logout),
        new Field(FieldType.MsgSeqNum, parser.getNextTargetMsgSeqNum()),
        new Field(FieldType.SenderCompID, validSender ? sender : 'INVALID_SENDER'),
        new Field(FieldType.SendingTime, parser.getTimestamp(new Date())),
        new Field(FieldType.TargetCompID, validTarget ? target : 'INVALID_TARGET'),
        new Field(FieldType.Text, 'Invalid Logon TARGET or SENDER.'),
    );
    parser.isLoggedIn = false;
    parser.send(logonReject);
    parser.logger.logWarning({
        message: `FIXServer (${parser.protocol?.toUpperCase()}): >> sent Logout due to invalid Logon`,
        fix: message.encode('|'),
    });
    parser.stopHeartbeat();
    parser.close();
    return false;
};
