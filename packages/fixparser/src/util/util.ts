import { ApplVerID } from '../fieldtypes/ApplVerID';

export type Version = {
    version: string | undefined;
    build: string | undefined;
};

declare global {
    const __PACKAGE_VERSION__: string;
    const __BUILD_TIME__: string;
}

export const version: Version = {
    version: __PACKAGE_VERSION__ || process.env.__PACKAGE_VERSION__,
    build: __BUILD_TIME__ || process.env.__BUILD_TIME__,
};

export type FIXValue = number | string | boolean | null | undefined;
export type Parser = 'FIXServer' | 'FIXParser' | 'FIXParserBrowser';
export const parseFixVersion = (version: FIXValue): string => {
    return FIX_VERSIONS[version as string] ?? version;
};
const FIX_VERSIONS: Record<string, string> = {
    [ApplVerID.FIX27]: 'FIX.2.7',
    [ApplVerID.FIX30]: 'FIX.3.0',
    [ApplVerID.FIX40]: 'FIX.4.0',
    [ApplVerID.FIX41]: 'FIX.4.1',
    [ApplVerID.FIX42]: 'FIX.4.2',
    [ApplVerID.FIX43]: 'FIX.4.3',
    [ApplVerID.FIX44]: 'FIX.4.4',
    [ApplVerID.FIX50]: 'FIX.5.0',
    [ApplVerID.FIX50SP1]: 'FIX.5.0SP1',
    [ApplVerID.FIX50SP2]: 'FIX.5.0SP2',
    [ApplVerID.FIXLatest]: 'FIXT.1.1',
};
export const DEFAULT_FIX_VERSION: string = FIX_VERSIONS[ApplVerID.FIXLatest];
export const DEFAULT_HEARTBEAT_SECONDS: number = 30;
export const SOH: string = '\x01';
export const STRING_EQUALS: string = '=';
export const RE_ESCAPE: RegExp = /[.*+?^${}()|[\]\\]/g;
export const RE_FIND: RegExp = /8=FIXT?\.\d\.\d([^\d]+)/i;
export const READY_MS: number = 100;

export const pad = (value: number, size: number): string => {
    const paddedString = `0000000000000000${value}`;
    return paddedString.slice(-size);
};

export { timestamp } from './timestamp';
