import type { Logger } from '../logger/Logger';

export const timestamp = (dateObject: Date, logger: Logger): string => {
    if (Number.isNaN(dateObject.getTime())) {
        logger.log({ level: 'error', message: 'Invalid date specified!' });
        return '';
    }

    const utcDate = new Date(
        Date.UTC(
            dateObject.getUTCFullYear(),
            dateObject.getUTCMonth(),
            dateObject.getUTCDate(),
            dateObject.getUTCHours(),
            dateObject.getUTCMinutes(),
            dateObject.getUTCSeconds(),
            dateObject.getUTCMilliseconds(),
        ),
    );

    const year = utcDate.getUTCFullYear();
    const month = String(utcDate.getUTCMonth() + 1).padStart(2, '0');
    const day = String(utcDate.getUTCDate()).padStart(2, '0');
    const hoursUTC = String(utcDate.getUTCHours()).padStart(2, '0');
    const minutesUTC = String(utcDate.getUTCMinutes()).padStart(2, '0');
    const secondsUTC = String(utcDate.getUTCSeconds()).padStart(2, '0');
    const millisecondsUTC = String(utcDate.getUTCMilliseconds()).padStart(3, '0');

    return `${year}${month}${day}-${hoursUTC}:${minutesUTC}:${secondsUTC}.${millisecondsUTC}`;
};
