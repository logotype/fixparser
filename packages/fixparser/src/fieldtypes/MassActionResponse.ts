/**
 * Specifies the action taken by counterparty order handling system as a result of the action type indicated in MassActionType of the Order Mass Action Request.
 * - Tag: 1375
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MassActionResponse = Object.freeze({
    /** Rejected - See MassActionRejectReason(1376) */
    Rejected: 0,
    /** Accepted */
    Accepted: 1,
    /** Completed */
    Completed: 2,
} as const);

type MassActionResponse = (typeof MassActionResponse)[keyof typeof MassActionResponse];
