/**
 * Source for the price of a related entity, e.g. price of the underlying instrument in an Underlying Price Contingency (UPC) order. Can be used together with RelatedHighPrice (1819) and/or RelatedLowPrice (1820).
 * - Tag: 1821
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelatedPriceSource = Object.freeze({
    /** NBB (National Best Bid) */
    NBBid: 1,
    /** NBO (National Best Offer) */
    NBOffer: 2,
} as const);

type RelatedPriceSource = (typeof RelatedPriceSource)[keyof typeof RelatedPriceSource];
