/**
 * Specifies when the payout is to occur.
 * - Tag: 2121
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexOptPayoutTime = Object.freeze({
    /** Close */
    Close: 0,
    /** Open */
    Open: 1,
    /** Official settlement */
    OfficialSettl: 2,
    /** Valuation time */
    ValuationTime: 3,
    /** Exchange settlement time */
    ExcahgneSettlTime: 4,
    /** Derivatives close */
    DerivativesClose: 5,
    /** As specified in master confirmation */
    AsSpecified: 6,
} as const);

type ComplexOptPayoutTime = (typeof ComplexOptPayoutTime)[keyof typeof ComplexOptPayoutTime];
