/**
 * Indicates the seniority level of the lien in a loan.
 * - Tag: 1954
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LienSeniority = Object.freeze({
    /** Unknown */
    Unknown: 0,
    /** First lien */
    FirstLien: 1,
    /** Second lien */
    SecondLien: 2,
    /** Third lien */
    ThirdLien: 3,
} as const);

type LienSeniority = (typeof LienSeniority)[keyof typeof LienSeniority];
