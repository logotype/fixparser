/**
 * The results returned to a Security Request message
 * - Tag: 560
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SecurityRequestResult = Object.freeze({
    /** Valid request */
    ValidRequest: 0,
    /** Invalid or unsupported request */
    InvalidOrUnsupportedRequest: 1,
    /** No instruments found that match selection criteria */
    NoInstrumentsFound: 2,
    /** Not authorized to retrieve instrument data */
    NotAuthorizedToRetrieveInstrumentData: 3,
    /** Instrument data temporarily unavailable */
    InstrumentDataTemporarilyUnavailable: 4,
    /** Request for instrument data not supported */
    RequestForInstrumentDataNotSupported: 5,
} as const);

type SecurityRequestResult = (typeof SecurityRequestResult)[keyof typeof SecurityRequestResult];
