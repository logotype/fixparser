/**
 * Result party detail definition request.
 * - Tag: 1877
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyDetailRequestResult = Object.freeze({
    /** Successful (default) */
    Successful: 0,
    /** Invalid party(-ies) */
    InvalidParty: 1,
    /** Invalid related party(-ies) */
    InvalidRelatedParty: 2,
    /** Invalid party status(es) */
    InvalidPartyStatus: 3,
    /** Not authorized */
    NotAuthorized: 98,
    /** Other */
    Other: 99,
} as const);

type PartyDetailRequestResult = (typeof PartyDetailRequestResult)[keyof typeof PartyDetailRequestResult];
