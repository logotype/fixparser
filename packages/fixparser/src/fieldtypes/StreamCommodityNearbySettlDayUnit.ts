/**
 * Time unit associated with the nearby settlement day.
 * - Tag: 41267
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const StreamCommodityNearbySettlDayUnit = Object.freeze({
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
} as const);

type StreamCommodityNearbySettlDayUnit =
    (typeof StreamCommodityNearbySettlDayUnit)[keyof typeof StreamCommodityNearbySettlDayUnit];
