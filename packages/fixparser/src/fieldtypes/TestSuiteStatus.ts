/**
 * Identifies the overall test result of a group of individual test scenarios.
 * - Tag: 3070
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestSuiteStatus = Object.freeze({
    /** Undefined */
    Undefined: 0,
    /** Pass */
    Pass: 1,
    /** Fail */
    Fail: 2,
    /** Warning */
    Warning: 3,
} as const);

type TestSuiteStatus = (typeof TestSuiteStatus)[keyof typeof TestSuiteStatus];
