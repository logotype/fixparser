/**
 * The type of instrument relationship
 * - Tag: 1648
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelatedInstrumentType = Object.freeze({
    /** "hedges for" instrument */
    HedgesForInstrument: 1,
    /** Underlier */
    Underlier: 2,
    /** Equity equivalent */
    EquityEquivalent: 3,
    /** Nearest exchange traded contract */
    NearestExchangeTradedContract: 4,
    RetailEquivalent: 5,
    Leg: 6,
} as const);

type RelatedInstrumentType = (typeof RelatedInstrumentType)[keyof typeof RelatedInstrumentType];
