/**
 * Identifies the type of execution destination for the order.
 * - Tag: 2704
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExDestinationType = Object.freeze({
    NoRestriction: 0,
    TradedOnlyOnTradingVenue: 1,
    TradedOnlyOnSI: 2,
    TradedOnTradingVenueOrSI: 3,
} as const);

type ExDestinationType = (typeof ExDestinationType)[keyof typeof ExDestinationType];
