/**
 * The program under which a commercial paper offering is exempt from SEC registration identified by the paragraph number(s) within the US Securities Act of 1933 or as identified below.
 * - Tag: 875
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CPProgram = Object.freeze({
    Program3a3: 1,
    Program42: 2,
    Program3a2: 3,
    Program3a3And3c7: 4,
    Program3a4: 5,
    Program3a5: 6,
    Program3a7: 7,
    Program3c7: 8,
    /** Other */
    Other: 99,
} as const);

type CPProgram = (typeof CPProgram)[keyof typeof CPProgram];
