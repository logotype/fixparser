/**
 * Used to describe the ownership of the position.
 * - Tag: 1834
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PositionCapacity = Object.freeze({
    /** Principal */
    Principal: 0,
    /** Agent */
    Agent: 1,
    /** Customer */
    Customer: 2,
    /** Counterparty */
    Counterparty: 3,
} as const);

type PositionCapacity = (typeof PositionCapacity)[keyof typeof PositionCapacity];
