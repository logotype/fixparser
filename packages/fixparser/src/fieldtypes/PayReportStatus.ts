/**
 * Identifies status of the payment report.
 * - Tag: 2806
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PayReportStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    /** Rejected */
    Rejected: 2,
    Disputed: 3,
} as const);

type PayReportStatus = (typeof PayReportStatus)[keyof typeof PayReportStatus];
