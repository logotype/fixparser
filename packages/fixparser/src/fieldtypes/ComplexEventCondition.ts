/**
 * ComplexEventCondition
 * - Tag: 1490
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventCondition = Object.freeze({
    /** And */
    And: 1,
    /** Or */
    Or: 2,
} as const);

type ComplexEventCondition = (typeof ComplexEventCondition)[keyof typeof ComplexEventCondition];
