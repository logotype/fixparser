/**
 * Specifies the type of secondary size.
 * - Tag: 1178
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDSecSizeType = Object.freeze({
    Customer: 1,
    CustomerProfessional: 2,
    DoNotTradeThrough: 3,
} as const);

type MDSecSizeType = (typeof MDSecSizeType)[keyof typeof MDSecSizeType];
