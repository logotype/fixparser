/**
 * Identifies class of alternative SecurityID
 * - Tag: 22
 * - FIX Specification type: String
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IDSource = Object.freeze({
    /** CUSIP */
    CUSIP: '1',
    /** SEDOL */
    SEDOL: '2',
    /** QUIK */
    QUIK: '3',
    /** ISIN number */
    ISINNumber: '4',
    /** RIC code */
    RICCode: '5',
    /** ISO Currency Code */
    ISOCurrencyCode: '6',
    /** ISO Country Code */
    ISOCountryCode: '7',
    /** Exchange Symbol */
    ExchangeSymbol: '8',
    /** Consolidated Tape Association (CTA) Symbol (SIAC CTS/CQS line format) */
    ConsolidatedTapeAssociation: '9',
} as const);

type IDSource = (typeof IDSource)[keyof typeof IDSource];
