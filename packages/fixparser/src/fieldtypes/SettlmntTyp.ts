/**
 * Indicates order settlement period. Absence of this field is interpreted as Regular. Regular is defined as the default settlement period for the particular security on the exchange of execution.
 * - Tag: 63
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlmntTyp = Object.freeze({
    /** Regular */
    Regular: '0',
    /** Cash */
    Cash: '1',
    /** Next Day */
    NextDay: '2',
    /** T+2 */
    TPlus2: '3',
    /** T+3 */
    TPlus3: '4',
    /** T+4 */
    TPlus4: '5',
    /** Future */
    Future: '6',
    /** When Issued */
    WhenAndIfIssued: '7',
    /** Sellers Option */
    SellersOption: '8',
    /** T+ 5 */
    TPlus5: '9',
} as const);

type SettlmntTyp = (typeof SettlmntTyp)[keyof typeof SettlmntTyp];
