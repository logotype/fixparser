/**
 * Indicates if a Cancel/Replace has caused an order to lose book priority.
 * - Tag: 638
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PriorityIndicator = Object.freeze({
    /** Priority unchanged */
    PriorityUnchanged: 0,
    /** Lost Priority as result of order change */
    LostPriorityAsResultOfOrderChange: 1,
} as const);

type PriorityIndicator = (typeof PriorityIndicator)[keyof typeof PriorityIndicator];
