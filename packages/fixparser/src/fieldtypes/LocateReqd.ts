/**
 * Indicates whether the broker is to locate the stock in conjunction with a short sell order.
 * - Tag: 114
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LocateReqd = Object.freeze({
    /** Indicates the broker is not required to locate */
    No: 'N',
    /** Indicates the broker is responsible for locating the stock */
    Yes: 'Y',
} as const);

type LocateReqd = (typeof LocateReqd)[keyof typeof LocateReqd];
