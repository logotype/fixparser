/**
 * Indicates whether a message decrements the number of outstanding requests, e.g. one where ThrottleType = Outstanding Requests.
 * - Tag: 1686
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ThrottleCountIndicator = Object.freeze({
    /** Outstanding requests unchanged */
    OutstandingRequestsUnchanged: 0,
    /** Outstanding requests decreased */
    OutstandingRequestsDecreased: 1,
} as const);

type ThrottleCountIndicator = (typeof ThrottleCountIndicator)[keyof typeof ThrottleCountIndicator];
