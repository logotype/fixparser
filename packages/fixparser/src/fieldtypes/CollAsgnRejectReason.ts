/**
 * Collateral Assignment Reject Reason
 * - Tag: 906
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollAsgnRejectReason = Object.freeze({
    /** Unknown deal (order / trade) */
    UnknownDeal: 0,
    /** Unknown or invalid instrument */
    UnknownOrInvalidInstrument: 1,
    /** Unauthorized transaction */
    UnauthorizedTransaction: 2,
    /** Insufficient collateral */
    InsufficientCollateral: 3,
    /** Invalid type of collateral */
    InvalidTypeOfCollateral: 4,
    /** Excessive substitution */
    ExcessiveSubstitution: 5,
    /** Other */
    Other: 99,
} as const);

type CollAsgnRejectReason = (typeof CollAsgnRejectReason)[keyof typeof CollAsgnRejectReason];
