/**
 * Type of collateral assignment response.
 * - Tag: 905
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollAsgnRespType = Object.freeze({
    /** Received */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    /** Declined */
    Declined: 2,
    /** Rejected */
    Rejected: 3,
    TransactionPending: 4,
    TransactionCompletedWithWarning: 5,
} as const);

type CollAsgnRespType = (typeof CollAsgnRespType)[keyof typeof CollAsgnRespType];
