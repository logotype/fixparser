/**
 * Used to identify the source of the Account (1) code. This is especially useful if the account is a new account that the Respondent may not have setup yet in their system.
 * - Tag: 660
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AcctIDSource = Object.freeze({
    /** BIC */
    BIC: 1,
    /** SID Code */
    SIDCode: 2,
    /** TFM (GSPTA) */
    TFM: 3,
    /** OMGEO (Alert ID) */
    OMGEO: 4,
    /** DTCC Code */
    DTCCCode: 5,
    SPSAID: 6,
    /** Other (custom or proprietary) */
    Other: 99,
} as const);

type AcctIDSource = (typeof AcctIDSource)[keyof typeof AcctIDSource];
