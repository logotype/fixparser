/**
 * The OrdType the order should have after the trigger has hit. Required to express orders that change from Limit to Market. Other values from OrdType (40) may be used if appropriate and bilaterally agreed upon.
 * - Tag: 1111
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TriggerOrderType = Object.freeze({
    /** Market */
    Market: '1',
    /** Limit */
    Limit: '2',
} as const);

type TriggerOrderType = (typeof TriggerOrderType)[keyof typeof TriggerOrderType];
