/**
 * Number of Side repeating group instances.
 * - Tag: 552
 * - FIX Specification type: NumInGroup
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NoSides = Object.freeze({
    /** One Side */
    OneSide: 1,
    /** Both Sides */
    BothSides: 2,
} as const);

type NoSides = (typeof NoSides)[keyof typeof NoSides];
