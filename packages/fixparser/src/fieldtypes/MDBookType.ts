/**
 * Describes the type of book for which the feed is intended. Used when multiple feeds are provided over the same connection
 * - Tag: 1021
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDBookType = Object.freeze({
    /** Top of Book */
    TopOfBook: 1,
    /** Price Depth */
    PriceDepth: 2,
    /** Order Depth */
    OrderDepth: 3,
} as const);

type MDBookType = (typeof MDBookType)[keyof typeof MDBookType];
