/**
 * Indicates that both sides of the FIX session should reset sequence numbers.
 * - Tag: 141
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ResetSeqNumFlag = Object.freeze({
    /** No */
    No: 'N',
    /** Yes, reset sequence numbers */
    Yes: 'Y',
} as const);

type ResetSeqNumFlag = (typeof ResetSeqNumFlag)[keyof typeof ResetSeqNumFlag];
