/**
 * Specifies the type of action to take or was taken for a given party.
 * - Tag: 2329
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyActionType = Object.freeze({
    /** Suspend */
    Suspend: 0,
    /** Halt trading */
    HaltTrading: 1,
    /** Reinstate */
    Reinstate: 2,
} as const);

type PartyActionType = (typeof PartyActionType)[keyof typeof PartyActionType];
