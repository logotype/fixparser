/**
 * Reason for order being unaffected by mass action even though it belongs to the orders covered by MassActionScope(1374).
 * - Tag: 2677
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NotAffectedReason = Object.freeze({
    /** Order suspended */
    OrderSuspended: 0,
    /** Instrument suspended */
    InstrumentSuspended: 1,
} as const);

type NotAffectedReason = (typeof NotAffectedReason)[keyof typeof NotAffectedReason];
