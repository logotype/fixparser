/**
 * Time unit associated with the frequency of payments.
 * - Tag: 40754
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PaymentStreamPaymentFrequencyUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
    /** Term */
    Term: 'T',
} as const);

type PaymentStreamPaymentFrequencyUnit =
    (typeof PaymentStreamPaymentFrequencyUnit)[keyof typeof PaymentStreamPaymentFrequencyUnit];
