/**
 * Status of entitlement definition for one party.
 * - Tag: 1883
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const EntitlementStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Accepted with changes */
    AcceptedWithChanges: 1,
    /** Rejected */
    Rejected: 2,
    Pending: 3,
    Requested: 4,
    Deferred: 5,
} as const);

type EntitlementStatus = (typeof EntitlementStatus)[keyof typeof EntitlementStatus];
