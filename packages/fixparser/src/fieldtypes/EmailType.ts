/**
 * Email message type.
 * - Tag: 94
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const EmailType = Object.freeze({
    /** New */
    New: '0',
    /** Reply */
    Reply: '1',
    /** Admin Reply */
    AdminReply: '2',
} as const);

type EmailType = (typeof EmailType)[keyof typeof EmailType];
