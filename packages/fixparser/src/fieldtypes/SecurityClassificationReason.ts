/**
 * Allows classification of instruments according to a set of high level reasons. Classification reasons describe the classes in which the instrument participates.
 * - Tag: 1583
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SecurityClassificationReason = Object.freeze({
    /** Fee */
    Fee: 0,
    /** Credit Controls */
    CreditControls: 1,
    /** Margin */
    Margin: 2,
    /** Entitlement / Eligibility */
    EntitlementOrEligibility: 3,
    /** Market Data */
    MarketData: 4,
    /** Account Selection */
    AccountSelection: 5,
    /** Delivery Process */
    DeliveryProcess: 6,
    /** Sector */
    Sector: 7,
} as const);

type SecurityClassificationReason = (typeof SecurityClassificationReason)[keyof typeof SecurityClassificationReason];
