/**
 * Type of data source identifier.
 * - Tag: 41282
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamCommodityDataSourceIDType = Object.freeze({
    /** City (4 character business center code) */
    City: 0,
    /** Airport (IATA standard) */
    Airport: 1,
    /** Weather station WBAN (Weather Bureau Army Navy) */
    WeatherStation: 2,
    /** Weather index WMO (World Meteorological Organization) */
    WeatherIndex: 3,
} as const);

type StreamCommodityDataSourceIDType =
    (typeof StreamCommodityDataSourceIDType)[keyof typeof StreamCommodityDataSourceIDType];
