/**
 * Result of Position Maintenance Request.
 * - Tag: 723
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosMaintResult = Object.freeze({
    /** Successful Completion - no warnings or errors */
    SuccessfulCompletion: 0,
    /** Rejected */
    Rejected: 1,
    /** Other */
    Other: 99,
} as const);

type PosMaintResult = (typeof PosMaintResult)[keyof typeof PosMaintResult];
