/**
 * Indicates that this message is to serve as the final and legal confirmation.
 * - Tag: 650
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LegalConfirm = Object.freeze({
    /** Does not consitute a Legal Confirm */
    DoesNotConsituteALegalConfirm: 'N',
    /** Legal Confirm */
    LegalConfirm: 'Y',
} as const);

type LegalConfirm = (typeof LegalConfirm)[keyof typeof LegalConfirm];
