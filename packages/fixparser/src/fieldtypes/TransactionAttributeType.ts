/**
 * Type of attribute(s) or characteristic(s) associated with the transaction.
 * - Tag: 2872
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransactionAttributeType = Object.freeze({
    ExclusiveArrangement: 0,
    CollateralReuse: 1,
    CollateralArrangmentType: 2,
} as const);

type TransactionAttributeType = (typeof TransactionAttributeType)[keyof typeof TransactionAttributeType];
