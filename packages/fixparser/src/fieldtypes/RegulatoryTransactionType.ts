/**
 * Specifies the regulatory mandate or rule that the transaction complies with.
 * - Tag: 2347
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RegulatoryTransactionType = Object.freeze({
    None: 0,
    SEFRequiredTransaction: 1,
    SEFPermittedTransaction: 2,
} as const);

type RegulatoryTransactionType = (typeof RegulatoryTransactionType)[keyof typeof RegulatoryTransactionType];
