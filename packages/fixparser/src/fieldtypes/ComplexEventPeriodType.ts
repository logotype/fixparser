/**
 * Specifies the period type.
 * - Tag: 41011
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventPeriodType = Object.freeze({
    /** Asian Out */
    AsianOut: 0,
    /** Asian In */
    AsianIn: 1,
    /** Barrier Cap */
    BarrierCap: 2,
    /** Barrier Floor */
    BarrierFloor: 3,
    /** Knock Out */
    KnockOut: 4,
    /** Knock In */
    KnockIn: 5,
} as const);

type ComplexEventPeriodType = (typeof ComplexEventPeriodType)[keyof typeof ComplexEventPeriodType];
