/**
 * Identifies whether the rate spread is applied to a long or short position.
 * - Tag: 40795
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamRateSpreadPositionType = Object.freeze({
    /** Short */
    Short: 0,
    /** Long */
    Long: 1,
} as const);

type PaymentStreamRateSpreadPositionType =
    (typeof PaymentStreamRateSpreadPositionType)[keyof typeof PaymentStreamRateSpreadPositionType];
