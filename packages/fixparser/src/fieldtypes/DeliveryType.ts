/**
 * Identifies type of settlement
 * - Tag: 919
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryType = Object.freeze({
    /** "Versus Payment": Deliver (if sell) or Receive (if buy) vs. (against) Payment */
    VersusPayment: 0,
    /** "Free": Deliver (if sell) or Receive (if buy) Free */
    Free: 1,
    /** Tri-Party */
    TriParty: 2,
    /** Hold In Custody */
    HoldInCustody: 3,
    DeliverByValue: 4,
} as const);

type DeliveryType = (typeof DeliveryType)[keyof typeof DeliveryType];
