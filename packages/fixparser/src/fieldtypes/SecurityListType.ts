/**
 * Specifies a type of Security List.
 * - Tag: 1470
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SecurityListType = Object.freeze({
    /** Industry Classification */
    IndustryClassification: 1,
    /** Trading List */
    TradingList: 2,
    /** Market / Market Segment List */
    Market: 3,
    /** Newspaper List */
    NewspaperList: 4,
} as const);

type SecurityListType = (typeof SecurityListType)[keyof typeof SecurityListType];
