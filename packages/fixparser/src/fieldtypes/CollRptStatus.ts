/**
 * The status of the collateral report.
 * - Tag: 2488
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollRptStatus = Object.freeze({
    /** Accepted (successfully processed) */
    Accepted: 0,
    /** Received (not yet processed) */
    Received: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type CollRptStatus = (typeof CollRptStatus)[keyof typeof CollRptStatus];
