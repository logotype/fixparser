/**
 * Used to identify whether the order initiator is an aggressor or not in the trade.
 * - Tag: 1057
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AggressorIndicator = Object.freeze({
    /** Order initiator is aggressor */
    OrderInitiatorIsAggressor: 'Y',
    /** Order initiator is passive */
    OrderInitiatorIsPassive: 'N',
} as const);

type AggressorIndicator = (typeof AggressorIndicator)[keyof typeof AggressorIndicator];
