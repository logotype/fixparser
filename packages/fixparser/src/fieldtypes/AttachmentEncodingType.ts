/**
 * AttachmentEncodingType
 * - Tag: 2109
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AttachmentEncodingType = Object.freeze({
    Base64: 0,
    RawBinary: 1,
} as const);

type AttachmentEncodingType = (typeof AttachmentEncodingType)[keyof typeof AttachmentEncodingType];
