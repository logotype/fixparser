/**
 * Method of trading
 * - Tag: 338
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesMethod = Object.freeze({
    /** Electronic */
    Electronic: 1,
    /** Open Outcry */
    OpenOutcry: 2,
    /** Two Party */
    TwoParty: 3,
    /** Voice */
    Voice: 4,
} as const);

type TradSesMethod = (typeof TradSesMethod)[keyof typeof TradSesMethod];
