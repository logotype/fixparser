/**
 * Quantity (e.g. number of shares) in numeric form or relative size.
 * - Tag: 27
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IOIQty = Object.freeze({
    /** Small */
    Small: 'S',
    /** Medium */
    Medium: 'M',
    /** Large */
    Large: 'L',
    /** Undisclosed Quantity */
    UndisclosedQuantity: 'U',
} as const);

type IOIQty = (typeof IOIQty)[keyof typeof IOIQty];
