/**
 * Indicates the reason a short sale order is exempted from applicable regulation (e.g. Reg SHO addendum (b)(1) in the U.S.).
 * - Tag: 1688
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ShortSaleExemptionReason = Object.freeze({
    ExemptionReasonUnknown: 0,
    IncomingSSE: 1,
    AboveNationalBestBid: 2,
    DelayedDelivery: 3,
    OddLot: 4,
    DomesticArbitrage: 5,
    InternationalArbitrage: 6,
    UnderwriterOrSyndicateDistribution: 7,
    RisklessPrincipal: 8,
    VWAP: 9,
} as const);

type ShortSaleExemptionReason = (typeof ShortSaleExemptionReason)[keyof typeof ShortSaleExemptionReason];
