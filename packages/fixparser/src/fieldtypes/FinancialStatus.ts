/**
 * Identifies a firm's or a security's financial status
 * - Tag: 291
 * - FIX Specification type: MultipleCharValue
 * - Mapped type: string
 * @readonly
 * @public
 */
export const FinancialStatus = Object.freeze({
    /** Bankrupt */
    Bankrupt: '1',
    /** Pending delisting */
    PendingDelisting: '2',
    /** Restricted */
    Restricted: '3',
} as const);

type FinancialStatus = (typeof FinancialStatus)[keyof typeof FinancialStatus];
