/**
 * Identifies the form of delivery.
 * - Tag: 668
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryForm = Object.freeze({
    /** Book Entry (default) */
    BookEntry: 1,
    /** Bearer */
    Bearer: 2,
} as const);

type DeliveryForm = (typeof DeliveryForm)[keyof typeof DeliveryForm];
