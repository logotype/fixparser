/**
 * Describes the type of relationship between the order identified by RelatedOrderID(2887) and the order outside of the RelatedOrderGrp component.
 * - Tag: 2890
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OrderRelationship = Object.freeze({
    /** Not specified */
    NotSpecified: 0,
    OrderAggregation: 1,
    OrderSplit: 2,
} as const);

type OrderRelationship = (typeof OrderRelationship)[keyof typeof OrderRelationship];
