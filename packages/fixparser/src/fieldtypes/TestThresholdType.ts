/**
 * Identifies whether the value of a measure needs to be over or under a specific threshold to be successful.
 * - Tag: 3058
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestThresholdType = Object.freeze({
    /** Under */
    Under: 0,
    /** Over */
    Over: 1,
} as const);

type TestThresholdType = (typeof TestThresholdType)[keyof typeof TestThresholdType];
