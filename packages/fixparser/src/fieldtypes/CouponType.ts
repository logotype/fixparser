/**
 * Coupon type of the bond.
 * - Tag: 1946
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CouponType = Object.freeze({
    /** Zero */
    Zero: 0,
    /** Fixed rate */
    FixedRate: 1,
    /** Floating rate */
    FloatingRate: 2,
    /** Structured */
    Structured: 3,
} as const);

type CouponType = (typeof CouponType)[keyof typeof CouponType];
