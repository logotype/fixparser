/**
 * CrossPrioritization
 * - Tag: 550
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CrossPrioritization = Object.freeze({
    /** None */
    None: 0,
    /** Buy side is prioritized */
    BuySideIsPrioritized: 1,
    /** Sell side is prioritized */
    SellSideIsPrioritized: 2,
} as const);

type CrossPrioritization = (typeof CrossPrioritization)[keyof typeof CrossPrioritization];
