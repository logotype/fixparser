/**
 * Specifies the type of action to take or that was taken for a given test suite.
 * - Tag: 3067
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestActionType = Object.freeze({
    /** Start */
    Start: 0,
    /** Stop */
    Stop: 1,
    /** State */
    State: 2,
} as const);

type TestActionType = (typeof TestActionType)[keyof typeof TestActionType];
