/**
 * Type of cross being submitted to a market
 * - Tag: 549
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CrossType = Object.freeze({
    CrossAON: 1,
    CrossIOC: 2,
    CrossOneSide: 3,
    CrossSamePrice: 4,
    BasisCross: 5,
    ContingentCross: 6,
    VWAPCross: 7,
    STSCross: 8,
    CustomerToCustomer: 9,
} as const);

type CrossType = (typeof CrossType)[keyof typeof CrossType];
