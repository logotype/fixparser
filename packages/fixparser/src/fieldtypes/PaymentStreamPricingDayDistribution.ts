/**
 * The distribution of pricing days.
 * - Tag: 41214
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamPricingDayDistribution = Object.freeze({
    /** All */
    All: 0,
    /** First */
    First: 1,
    /** Last */
    Last: 2,
    /** Penultimate */
    Penultimate: 3,
} as const);

type PaymentStreamPricingDayDistribution =
    (typeof PaymentStreamPricingDayDistribution)[keyof typeof PaymentStreamPricingDayDistribution];
