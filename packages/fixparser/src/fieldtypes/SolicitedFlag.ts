/**
 * Indicates whether or not the order was solicited.
 * - Tag: 377
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SolicitedFlag = Object.freeze({
    /** Was not solicited */
    WasNotSolicited: 'N',
    /** Was solicited */
    WasSolicited: 'Y',
} as const);

type SolicitedFlag = (typeof SolicitedFlag)[keyof typeof SolicitedFlag];
