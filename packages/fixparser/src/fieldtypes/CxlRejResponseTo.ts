/**
 * Identifies the type of request that a Cancel Reject is in response to.
 * - Tag: 434
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const CxlRejResponseTo = Object.freeze({
    /** Order cancel request */
    OrderCancelRequest: '1',
    /** Order cancel/replace request */
    OrderCancelReplaceRequest: '2',
} as const);

type CxlRejResponseTo = (typeof CxlRejResponseTo)[keyof typeof CxlRejResponseTo];
