/**
 * Payment side of this individual payment from the requesting firm's perspective.
 * - Tag: 2819
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PostTradePaymentDebitOrCredit = Object.freeze({
    /** Debit / Pay */
    DebitPay: 0,
    /** Credit / Receive */
    CreditReceive: 1,
} as const);

type PostTradePaymentDebitOrCredit = (typeof PostTradePaymentDebitOrCredit)[keyof typeof PostTradePaymentDebitOrCredit];
