/**
 * Specifies the type of date (e.g. adjusted for holidays).
 * - Tag: 40827
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NonDeliverableFixingDateType = Object.freeze({
    /** Unadjusted */
    Unadjusted: 0,
    /** Adjusted */
    Adjusted: 1,
} as const);

type NonDeliverableFixingDateType = (typeof NonDeliverableFixingDateType)[keyof typeof NonDeliverableFixingDateType];
