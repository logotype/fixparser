/**
 * Indication of how a trade was verified.
 * - Tag: 1931
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const VerificationMethod = Object.freeze({
    /** Non-electronic */
    NonElectronic: 0,
    /** Electronic */
    Electronic: 1,
} as const);

type VerificationMethod = (typeof VerificationMethod)[keyof typeof VerificationMethod];
