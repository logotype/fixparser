/**
 * Identifies the status of the Confirmation.
 * - Tag: 665
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ConfirmStatus = Object.freeze({
    /** Received */
    Received: 1,
    /** Mismatched Account */
    MismatchedAccount: 2,
    /** Missing Settlement Instructions */
    MissingSettlementInstructions: 3,
    /** Confirmed */
    Confirmed: 4,
    /** Request Rejected */
    RequestRejected: 5,
} as const);

type ConfirmStatus = (typeof ConfirmStatus)[keyof typeof ConfirmStatus];
