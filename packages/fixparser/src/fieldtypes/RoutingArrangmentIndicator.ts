/**
 * RoutingArrangmentIndicator
 * - Tag: 2883
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RoutingArrangmentIndicator = Object.freeze({
    /** No routing arrangement in place */
    NoRoutingArrangmentInPlace: 0,
    /** Routing arrangement in place */
    RoutingArrangementInPlace: 1,
} as const);

type RoutingArrangmentIndicator = (typeof RoutingArrangmentIndicator)[keyof typeof RoutingArrangmentIndicator];
