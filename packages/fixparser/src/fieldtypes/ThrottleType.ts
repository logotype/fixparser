/**
 * Type of throttle.
 * - Tag: 1612
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ThrottleType = Object.freeze({
    /** Inbound Rate */
    InboundRate: 0,
    /** Outstanding Requests */
    OutstandingRequests: 1,
} as const);

type ThrottleType = (typeof ThrottleType)[keyof typeof ThrottleType];
