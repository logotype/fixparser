/**
 * Protection term event qualifier. Used to further qualify ProtectionTermEventType(40192).
 * - Tag: 40200
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ProtectionTermEventQualifier = Object.freeze({
    RestructuringMultipleHoldingObligations: 'H',
    RestructuringMultipleCreditEventNotices: 'E',
    FloatingRateInterestShortfall: 'C',
} as const);

type ProtectionTermEventQualifier = (typeof ProtectionTermEventQualifier)[keyof typeof ProtectionTermEventQualifier];
