/**
 * Used to identify the calculation agent. The calculation agent may be identified in ProvisionCalculationAgent(40098) or in the ProvisionParties component.
 * - Tag: 40098
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionCalculationAgent = Object.freeze({
    /** Exercising party */
    ExercisingParty: 0,
    /** Non-exercising party */
    NonExercisingParty: 1,
    /** As specified in the master agreement */
    MasterAgreeent: 2,
    /** As specified in the standard terms supplement */
    Supplement: 3,
} as const);

type ProvisionCalculationAgent = (typeof ProvisionCalculationAgent)[keyof typeof ProvisionCalculationAgent];
