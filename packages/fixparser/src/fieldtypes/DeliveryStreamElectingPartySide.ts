/**
 * A reference to the party able to choose whether the gas is delivered for a particular period as found in a swing or interruptible contract.
 * - Tag: 41080
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamElectingPartySide = Object.freeze({
    /** Buyer */
    Buyer: 0,
    /** Seller */
    Seller: 1,
} as const);

type DeliveryStreamElectingPartySide =
    (typeof DeliveryStreamElectingPartySide)[keyof typeof DeliveryStreamElectingPartySide];
