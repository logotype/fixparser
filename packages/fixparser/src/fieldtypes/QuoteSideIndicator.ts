/**
 * Indicates whether single sided quotes are allowed.
 * - Tag: 2559
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const QuoteSideIndicator = Object.freeze({
    No: 'N',
    Yes: 'Y',
} as const);

type QuoteSideIndicator = (typeof QuoteSideIndicator)[keyof typeof QuoteSideIndicator];
