/**
 * Status of party details definition request.
 * - Tag: 1878
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyDetailRequestStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Accepted with changes */
    AcceptedWithChanges: 1,
    /** Rejected */
    Rejected: 2,
    /** Acceptance pending */
    AcceptancePending: 3,
} as const);

type PartyDetailRequestStatus = (typeof PartyDetailRequestStatus)[keyof typeof PartyDetailRequestStatus];
