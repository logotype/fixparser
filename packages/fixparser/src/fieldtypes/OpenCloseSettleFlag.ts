/**
 * Flag that identifies a price.
 * - Tag: 286
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OpenCloseSettleFlag = Object.freeze({
    /** Daily Open / Close / Settlement price */
    DailyOpen: '0',
    /** Session Open / Close / Settlement price */
    SessionOpen: '1',
    /** Delivery Settlement price */
    DeliverySettlementEntry: '2',
} as const);

type OpenCloseSettleFlag = (typeof OpenCloseSettleFlag)[keyof typeof OpenCloseSettleFlag];
