/**
 * Describes whether peg is static or floats
 * - Tag: 835
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PegMoveType = Object.freeze({
    /** Floating (default) */
    Floating: 0,
    /** Fixed */
    Fixed: 1,
} as const);

type PegMoveType = (typeof PegMoveType)[keyof typeof PegMoveType];
