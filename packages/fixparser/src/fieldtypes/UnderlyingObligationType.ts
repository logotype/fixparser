/**
 * Type of reference obligation for credit derivatives contracts.
 * - Tag: 2012
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const UnderlyingObligationType = Object.freeze({
    /** Bond */
    Bond: '0',
    /** Convertible bond */
    ConvertibleBond: '1',
    /** Mortgage */
    Mortgage: '2',
    /** Loan */
    Loan: '3',
} as const);

type UnderlyingObligationType = (typeof UnderlyingObligationType)[keyof typeof UnderlyingObligationType];
