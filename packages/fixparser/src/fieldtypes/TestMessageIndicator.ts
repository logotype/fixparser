/**
 * Indicates whether or not this FIX Session is a "test" vs. "production" connection. Useful for preventing "accidents".
 * - Tag: 464
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TestMessageIndicator = Object.freeze({
    /** False (production) */
    False: 'N',
    /** True (test) */
    True: 'Y',
} as const);

type TestMessageIndicator = (typeof TestMessageIndicator)[keyof typeof TestMessageIndicator];
