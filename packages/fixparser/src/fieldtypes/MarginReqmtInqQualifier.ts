/**
 * Qualifier for MarginRequirementInquiry to identify a specific report.
 * - Tag: 1637
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarginReqmtInqQualifier = Object.freeze({
    /** Summary */
    Summary: 0,
    /** Detail */
    Detail: 1,
    /** Excess/Deficit */
    ExcessDeficit: 2,
    /** Net Position */
    NetPosition: 3,
} as const);

type MarginReqmtInqQualifier = (typeof MarginReqmtInqQualifier)[keyof typeof MarginReqmtInqQualifier];
