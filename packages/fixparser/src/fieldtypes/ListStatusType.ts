/**
 * Code to represent the status type.
 * - Tag: 429
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ListStatusType = Object.freeze({
    /** Ack */
    Ack: 1,
    /** Response */
    Response: 2,
    /** Timed */
    Timed: 3,
    /** Exec Started */
    ExecStarted: 4,
    /** All Done */
    AllDone: 5,
    /** Alert */
    Alert: 6,
} as const);

type ListStatusType = (typeof ListStatusType)[keyof typeof ListStatusType];
