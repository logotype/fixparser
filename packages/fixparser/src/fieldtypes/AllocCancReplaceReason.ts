/**
 * Reason for cancelling or replacing an Allocation Instruction or Allocation Report message
 * - Tag: 796
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocCancReplaceReason = Object.freeze({
    /** Original details incomplete/incorrect */
    OriginalDetailsIncomplete: 1,
    /** Change in underlying order details */
    ChangeInUnderlyingOrderDetails: 2,
    /** Cancelled by give-up firm */
    CancelledByGiveupFirm: 3,
    /** Other */
    Other: 99,
} as const);

type AllocCancReplaceReason = (typeof AllocCancReplaceReason)[keyof typeof AllocCancReplaceReason];
