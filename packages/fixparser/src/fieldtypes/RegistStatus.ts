/**
 * Registration status as returned by the broker or (for CIV) the fund manager:
 * - Tag: 506
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const RegistStatus = Object.freeze({
    /** Accepted */
    Accepted: 'A',
    /** Rejected */
    Rejected: 'R',
    /** Held */
    Held: 'H',
    /** Reminder - i.e. Registration Instructions are still outstanding */
    Reminder: 'N',
} as const);

type RegistStatus = (typeof RegistStatus)[keyof typeof RegistStatus];
