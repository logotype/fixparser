/**
 * Specifies the scope to which the RegulatoryTradeID(1903) applies. Used when a trade must be assigned more than one identifier, e.g. one for the clearing member and another for the client on a cleared trade as with the principal model in Europe.
 * - Tag: 2397
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RegulatoryTradeIDScope = Object.freeze({
    /** Clearing member */
    ClearingMember: 1,
    /** Client */
    Client: 2,
} as const);

type RegulatoryTradeIDScope = (typeof RegulatoryTradeIDScope)[keyof typeof RegulatoryTradeIDScope];
