/**
 * The consequences of market disruption events.
 * - Tag: 41087
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketDisruptionProvision = Object.freeze({
    /** Not applicable */
    NotApplicable: 0,
    /** Applicable */
    Applicable: 1,
    /** As specified in master agreement */
    AsInMasterAgreement: 2,
    /** As specified in confirmation */
    AsInConfirmation: 3,
} as const);

type MarketDisruptionProvision = (typeof MarketDisruptionProvision)[keyof typeof MarketDisruptionProvision];
