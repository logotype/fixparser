/**
 * Code to identify the type of liquidity indicator.
 * - Tag: 409
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LiquidityIndType = Object.freeze({
    /** 5-day moving average */
    FiveDayMovingAverage: 1,
    /** 20-day moving average */
    TwentyDayMovingAverage: 2,
    /** Normal market size */
    NormalMarketSize: 3,
    /** Other */
    Other: 4,
} as const);

type LiquidityIndType = (typeof LiquidityIndType)[keyof typeof LiquidityIndType];
