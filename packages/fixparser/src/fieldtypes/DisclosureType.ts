/**
 * Information subject to disclosure.
 * - Tag: 1813
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DisclosureType = Object.freeze({
    /** Volume */
    Volume: 1,
    /** Price */
    Price: 2,
    /** Side */
    Side: 3,
    /** AON */
    AON: 4,
    General: 5,
    /** Clearing account */
    ClearingAccount: 6,
    /** CMTA account */
    CMTAAccount: 7,
} as const);

type DisclosureType = (typeof DisclosureType)[keyof typeof DisclosureType];
