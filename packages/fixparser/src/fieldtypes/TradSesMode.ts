/**
 * Trading Session Mode
 * - Tag: 339
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesMode = Object.freeze({
    /** Testing */
    Testing: 1,
    /** Simulated */
    Simulated: 2,
    /** Production */
    Production: 3,
} as const);

type TradSesMode = (typeof TradSesMode)[keyof typeof TradSesMode];
