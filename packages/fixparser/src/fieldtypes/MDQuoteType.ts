/**
 * Identifies market data quote type.
 * - Tag: 1070
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDQuoteType = Object.freeze({
    /** Indicative */
    Indicative: 0,
    /** Tradeable */
    Tradeable: 1,
    /** Restricted Tradeable */
    RestrictedTradeable: 2,
    /** Counter */
    Counter: 3,
    /** Indicative and Tradeable */
    IndicativeAndTradeable: 4,
} as const);

type MDQuoteType = (typeof MDQuoteType)[keyof typeof MDQuoteType];
