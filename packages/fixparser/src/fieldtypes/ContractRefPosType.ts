/**
 * Additional information related to the pricing of a commodity swaps position, specifically an indicator referring to the position type.
 * - Tag: 1833
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ContractRefPosType = Object.freeze({
    /** Two component intercommodity spread */
    TwoComponentIntercommoditySpread: 0,
    /** Index or basket */
    IndexOrBasket: 1,
    /** Two component locational basis */
    TwoComponentLocationBasis: 2,
    /** Other */
    Other: 99,
} as const);

type ContractRefPosType = (typeof ContractRefPosType)[keyof typeof ContractRefPosType];
