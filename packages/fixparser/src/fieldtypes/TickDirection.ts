/**
 * Direction of the "tick".
 * - Tag: 274
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TickDirection = Object.freeze({
    /** Plus Tick */
    PlusTick: '0',
    /** Zero-Plus Tick */
    ZeroPlusTick: '1',
    /** Minus Tick */
    MinusTick: '2',
    /** Zero-Minus Tick */
    ZeroMinusTick: '3',
} as const);

type TickDirection = (typeof TickDirection)[keyof typeof TickDirection];
