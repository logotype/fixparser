/**
 * Indicates whether application level recovery is needed.
 * - Tag: 1744
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ApplLevelRecoveryIndicator = Object.freeze({
    /** Application level recovery is not needed (default) */
    NoApplRecoveryNeeded: 0,
    /** Application level recovery is needed */
    ApplRecoveryNeeded: 1,
} as const);

type ApplLevelRecoveryIndicator = (typeof ApplLevelRecoveryIndicator)[keyof typeof ApplLevelRecoveryIndicator];
