/**
 * Defines the type of price protection the customer requires on their order.
 * - Tag: 1092
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PriceProtectionScope = Object.freeze({
    /** None */
    None: '0',
    /** Local (Exchange, ECN, ATS) */
    Local: '1',
    /** National (Across all national markets) */
    National: '2',
    /** Global (Across all markets) */
    Global: '3',
} as const);

type PriceProtectionScope = (typeof PriceProtectionScope)[keyof typeof PriceProtectionScope];
