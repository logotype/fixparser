/**
 * Specifies whether or not SettlCurrFxRate (155) should be multiplied or divided.
 * - Tag: 156
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlCurrFxRateCalc = Object.freeze({
    /** Multiply */
    Multiply: 'M',
    /** Divide */
    Divide: 'D',
} as const);

type SettlCurrFxRateCalc = (typeof SettlCurrFxRateCalc)[keyof typeof SettlCurrFxRateCalc];
