/**
 * Indicates whether the tolerance is at the seller's or buyer's option.
 * - Tag: 41075
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamToleranceOptionSide = Object.freeze({
    /** Buyer */
    Buyer: 1,
    /** Seller */
    Seller: 2,
} as const);

type DeliveryStreamToleranceOptionSide =
    (typeof DeliveryStreamToleranceOptionSide)[keyof typeof DeliveryStreamToleranceOptionSide];
