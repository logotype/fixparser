/**
 * Indicates the status of the party identified with PartyDetailID(1691).
 * - Tag: 1672
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyDetailStatus = Object.freeze({
    /** Active (default if not specified) */
    Active: 0,
    /** Suspended */
    Suspended: 1,
    /** Halted */
    Halted: 2,
} as const);

type PartyDetailStatus = (typeof PartyDetailStatus)[keyof typeof PartyDetailStatus];
