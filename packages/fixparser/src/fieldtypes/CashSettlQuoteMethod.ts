/**
 * The type of quote used to determine the cash settlement price.
 * - Tag: 40027
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CashSettlQuoteMethod = Object.freeze({
    /** Bid */
    Bid: 0,
    /** Mid */
    Mid: 1,
    /** Offer */
    Offer: 2,
} as const);

type CashSettlQuoteMethod = (typeof CashSettlQuoteMethod)[keyof typeof CashSettlQuoteMethod];
