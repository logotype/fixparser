/**
 * Specifies the day or group of days for delivery.
 * - Tag: 41052
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleSettlDay = Object.freeze({
    /** Monday */
    Monday: 1,
    /** Tuesday */
    Tuesday: 2,
    /** Wednesday */
    Wednesday: 3,
    /** Thursday */
    Thursday: 4,
    /** Friday */
    Friday: 5,
    /** Saturday */
    Saturday: 6,
    /** Sunday */
    Sunday: 7,
    /** All weekdays */
    AllWeekdays: 8,
    /** All days */
    AllDays: 9,
    /** All weekends */
    AllWeekends: 10,
} as const);

type DeliveryScheduleSettlDay = (typeof DeliveryScheduleSettlDay)[keyof typeof DeliveryScheduleSettlDay];
