/**
 * Specifies the type of date. When specified it applies not only to the current date but to all subsequent dates in the group until overridden with a new type.
 * - Tag: 41139
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OptionExerciseDateType = Object.freeze({
    /** Unadjusted */
    Unadjusted: 0,
    /** Adjusted */
    Adjusted: 1,
} as const);

type OptionExerciseDateType = (typeof OptionExerciseDateType)[keyof typeof OptionExerciseDateType];
