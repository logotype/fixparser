/**
 * Status of the AllocationInstructionAlertRequest(35=DU).
 * - Tag: 2768
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocRequestStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Rejected */
    Rejected: 1,
} as const);

type AllocRequestStatus = (typeof AllocRequestStatus)[keyof typeof AllocRequestStatus];
