/**
 * Specifies the type of trade strategy.
 * - Tag: 2141
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const StrategyType = Object.freeze({
    /** Straddle */
    Straddle: 'STD',
    /** Strangle */
    Strangle: 'STG',
    /** Butterfly */
    Butterfly: 'BF',
    /** Condor */
    Condor: 'CNDR',
    /** Callable inversible snowball */
    CallableInversibleSnowball: 'CISN',
    /** Other */
    Other: 'OTHER',
} as const);

type StrategyType = (typeof StrategyType)[keyof typeof StrategyType];
