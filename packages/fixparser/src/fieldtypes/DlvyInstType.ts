/**
 * Used to indicate whether a delivery instruction is used for securities or cash settlement.
 * - Tag: 787
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DlvyInstType = Object.freeze({
    /** Cash */
    Cash: 'C',
    /** Securities */
    Securities: 'S',
} as const);

type DlvyInstType = (typeof DlvyInstType)[keyof typeof DlvyInstType];
