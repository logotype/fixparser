/**
 * Reference data entry's date-time type.
 * - Tag: 2748
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReferenceDataDateType = Object.freeze({
    AdmitToTradeRequestDate: 0,
    AdmitToTradeApprovalDate: 1,
    AdmitToTradeOrFirstTradeDate: 2,
    TerminationDate: 3,
} as const);

type ReferenceDataDateType = (typeof ReferenceDataDateType)[keyof typeof ReferenceDataDateType];
