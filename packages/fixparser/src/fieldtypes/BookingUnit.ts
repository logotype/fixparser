/**
 * Indicates what constitutes a bookable unit.
 * - Tag: 590
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const BookingUnit = Object.freeze({
    /** Each partial execution is a bookable unit */
    EachPartialExecutionIsABookableUnit: '0',
    /** Aggregate partial executions on this order, and book one trade per order */
    AggregatePartialExecutionsOnThisOrder: '1',
    /** Aggregate executions for this symbol, side, and settlement date */
    AggregateExecutionsForThisSymbol: '2',
} as const);

type BookingUnit = (typeof BookingUnit)[keyof typeof BookingUnit];
