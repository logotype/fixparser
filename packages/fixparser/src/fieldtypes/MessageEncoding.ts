/**
 * Type of message encoding (non-ASCII (non-English) characters) used in a message’s "Encoded" fields.
 * - Tag: 347
 * - FIX Specification type: String
 * - FIX Specification version: FIX44
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MessageEncoding = Object.freeze({
    /** JIS */
    ISO2022JP: 'ISO-2022-JP',
    /** EUC */
    EUCJP: 'EUC-JP',
    /** for using SJIS */
    ShiftJIS: 'Shift_JIS',
    /** Unicode */
    UTF8: 'UTF-8',
} as const);

type MessageEncoding = (typeof MessageEncoding)[keyof typeof MessageEncoding];
