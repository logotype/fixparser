/**
 * Specifies how the transaction was executed, e.g. via an automated execution platform or other method.
 * - Tag: 2405
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExecMethod = Object.freeze({
    Unspecified: 0,
    Manual: 1,
    Automated: 2,
    VoiceBrokered: 3,
} as const);

type ExecMethod = (typeof ExecMethod)[keyof typeof ExecMethod];
