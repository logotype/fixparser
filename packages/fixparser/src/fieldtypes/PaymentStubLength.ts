/**
 * Optional indication whether stub is shorter or longer than the regular swap period.
 * - Tag: 40874
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStubLength = Object.freeze({
    /** Short */
    Short: 0,
    /** Long */
    Long: 1,
} as const);

type PaymentStubLength = (typeof PaymentStubLength)[keyof typeof PaymentStubLength];
