/**
 * Indicates the broad product or asset classification. May be used to provide grouping for the product taxonomy (Product(460), SecurityType(167), etc.) and/or the risk taxonomy (AssetClass(1938), AssetSubClass(1939), AssetType(1940), etc.).
 * - Tag: 2210
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AssetGroup = Object.freeze({
    Financials: 1,
    Commodities: 2,
    AlternativeInvestments: 3,
} as const);

type AssetGroup = (typeof AssetGroup)[keyof typeof AssetGroup];
