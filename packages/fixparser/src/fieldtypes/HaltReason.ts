/**
 * Denotes the reason for the Opening Delay or Trading Halt.
 * - Tag: 327
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const HaltReason = Object.freeze({
    /** News Dissemination */
    NewsDissemination: 0,
    /** Order Influx */
    OrderInflux: 1,
    /** Order Imbalance */
    OrderImbalance: 2,
    /** Additional Information */
    AdditionalInformation: 3,
    /** News Pending */
    NewsPending: 4,
    /** Equipment Changeover */
    EquipmentChangeover: 5,
} as const);

type HaltReason = (typeof HaltReason)[keyof typeof HaltReason];
