/**
 * Specified how the TradeCaptureReport(35=AE) should be handled by the respondent.
 * - Tag: 1123
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TradeHandlingInstr = Object.freeze({
    /** Trade confirmation */
    TradeConfirmation: '0',
    /** Two-party report */
    TwoPartyReport: '1',
    /** One-party report for matching */
    OnePartyReportForMatching: '2',
    OnePartyReportForPassThrough: '3',
    /** Automated floor order routing */
    AutomatedFloorOrderRouting: '4',
    /** Two-party report for claim */
    TwoPartyReportForClaim: '5',
    /** One-party report */
    OnePartyReport: '6',
    ThirdPtyRptForPassThrough: '7',
    OnePartyReportAutoMatch: '8',
} as const);

type TradeHandlingInstr = (typeof TradeHandlingInstr)[keyof typeof TradeHandlingInstr];
