/**
 * Maintenance Action to be performed.
 * - Tag: 712
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosMaintAction = Object.freeze({
    New: 1,
    Replace: 2,
    Cancel: 3,
    Reverse: 4,
} as const);

type PosMaintAction = (typeof PosMaintAction)[keyof typeof PosMaintAction];
