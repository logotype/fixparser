/**
 * Type of reference obligation for credit derivatives contracts.
 * - Tag: 1739
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ObligationType = Object.freeze({
    /** Bond */
    Bond: '0',
    /** Convertible bond */
    ConvertBond: '1',
    /** Mortgage */
    Mortgage: '2',
    /** Loan */
    Loan: '3',
} as const);

type ObligationType = (typeof ObligationType)[keyof typeof ObligationType];
