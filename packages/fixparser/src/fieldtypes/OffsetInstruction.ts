/**
 * Indicates the trade is a result of an offset or onset.
 * - Tag: 1849
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OffsetInstruction = Object.freeze({
    Offset: 0,
    Onset: 1,
} as const);

type OffsetInstruction = (typeof OffsetInstruction)[keyof typeof OffsetInstruction];
