/**
 * Specifies the reason the PartyActionRequest(35=DH) was rejected.
 * - Tag: 2333
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyActionRejectReason = Object.freeze({
    /** Invalid party or parties */
    InvalidParty: 0,
    /** Unknown requesting party */
    UnkReqParty: 1,
    /** Not authorized */
    NotAuthorized: 98,
    /** Other */
    Other: 99,
} as const);

type PartyActionRejectReason = (typeof PartyActionRejectReason)[keyof typeof PartyActionRejectReason];
