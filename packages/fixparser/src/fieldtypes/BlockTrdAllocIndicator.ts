/**
 * Indication that a block trade will be allocated.
 * - Tag: 1980
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const BlockTrdAllocIndicator = Object.freeze({
    /** Block to be allocated */
    BlockToBeAllocated: 0,
    /** Block not to be allocated */
    BlockNotToBeAllocated: 1,
    AllocatedTrade: 2,
} as const);

type BlockTrdAllocIndicator = (typeof BlockTrdAllocIndicator)[keyof typeof BlockTrdAllocIndicator];
