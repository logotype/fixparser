/**
 * Used to indicate whether the quoting system allows only one quote to be active at a time for the quote issuer or market maker.
 * - Tag: 2837
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SingleQuoteIndicator = Object.freeze({
    /** Multiple quotes allowed */
    MultipleQuotesAllowed: 'N',
    /** Only one quote allowed */
    OnlyOneQuoteAllowed: 'Y',
} as const);

type SingleQuoteIndicator = (typeof SingleQuoteIndicator)[keyof typeof SingleQuoteIndicator];
