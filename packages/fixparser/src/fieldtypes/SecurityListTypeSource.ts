/**
 * Specifies a specific source for a SecurityListType. Relevant when a certain type can be provided from various sources.
 * - Tag: 1471
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SecurityListTypeSource = Object.freeze({
    /** ICB (Industry Classification Benchmark) published by Dow Jones and FTSE - www.icbenchmark.com */
    ICB: 1,
    /** NAICS (North American Industry Classification System). Replaced SIC (Standard Industry Classification) www.census.gov/naics or www.naics.com. */
    NAICS: 2,
    /** GICS (Global Industry Classification Standard) published by Standards & Poor */
    GICS: 3,
} as const);

type SecurityListTypeSource = (typeof SecurityListTypeSource)[keyof typeof SecurityListTypeSource];
