/**
 * The side from which the trigger price is reached.
 * - Tag: 1109
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TriggerPriceDirection = Object.freeze({
    /** Trigger if the price of the specified type goes UP to or through the specified Trigger Price. */
    Up: 'U',
    /** Trigger if the price of the specified type goes DOWN to or through the specified Trigger Price. */
    Down: 'D',
} as const);

type TriggerPriceDirection = (typeof TriggerPriceDirection)[keyof typeof TriggerPriceDirection];
