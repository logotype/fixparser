/**
 * Used for derivative products, such as options
 * - Tag: 203
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CoveredOrUncovered = Object.freeze({
    /** Covered */
    Covered: 0,
    /** Uncovered */
    Uncovered: 1,
} as const);

type CoveredOrUncovered = (typeof CoveredOrUncovered)[keyof typeof CoveredOrUncovered];
