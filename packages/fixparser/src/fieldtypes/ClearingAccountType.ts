/**
 * Designates the account type to be used for the order when submitted to clearing.
 * - Tag: 1816
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ClearingAccountType = Object.freeze({
    /** Customer */
    Customer: 1,
    /** Firm */
    Firm: 2,
    /** Market maker */
    MarketMaker: 3,
} as const);

type ClearingAccountType = (typeof ClearingAccountType)[keyof typeof ClearingAccountType];
