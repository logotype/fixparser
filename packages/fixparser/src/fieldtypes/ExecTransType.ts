/**
 * Identifies transaction type
 * - Tag: 20
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ExecTransType = Object.freeze({
    /** New */
    New: '0',
    /** Cancel */
    Cancel: '1',
    /** Correct */
    Correct: '2',
    /** Status */
    Status: '3',
} as const);

type ExecTransType = (typeof ExecTransType)[keyof typeof ExecTransType];
