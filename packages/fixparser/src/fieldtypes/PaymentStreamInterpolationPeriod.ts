/**
 * Defines applicable periods for interpolation.
 * - Tag: 42604
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamInterpolationPeriod = Object.freeze({
    Initial: 0,
    InitialAndFinal: 1,
    Final: 2,
    AnyPeriod: 3,
} as const);

type PaymentStreamInterpolationPeriod =
    (typeof PaymentStreamInterpolationPeriod)[keyof typeof PaymentStreamInterpolationPeriod];
