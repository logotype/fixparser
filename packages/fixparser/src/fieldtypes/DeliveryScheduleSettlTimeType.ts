/**
 * Specifies the format of the delivery start and end time values.
 * - Tag: 41057
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleSettlTimeType = Object.freeze({
    Hour: 0,
    Timestamp: 1,
} as const);

type DeliveryScheduleSettlTimeType = (typeof DeliveryScheduleSettlTimeType)[keyof typeof DeliveryScheduleSettlTimeType];
