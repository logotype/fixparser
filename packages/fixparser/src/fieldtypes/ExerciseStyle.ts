/**
 * Type of exercise of a derivatives security
 * - Tag: 1194
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExerciseStyle = Object.freeze({
    /** European */
    European: 0,
    /** American */
    American: 1,
    /** Bermuda */
    Bermuda: 2,
    /** Other */
    Other: 99,
} as const);

type ExerciseStyle = (typeof ExerciseStyle)[keyof typeof ExerciseStyle];
