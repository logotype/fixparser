/**
 * Time unit associated with the event.
 * - Tag: 1827
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const EventTimeUnit = Object.freeze({
    /** Hour */
    Hour: 'H',
    /** Minute */
    Minute: 'Min',
    /** Second */
    Second: 'S',
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type EventTimeUnit = (typeof EventTimeUnit)[keyof typeof EventTimeUnit];
