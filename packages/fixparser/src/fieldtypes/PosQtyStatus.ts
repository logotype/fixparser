/**
 * Status of this position.
 * - Tag: 706
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosQtyStatus = Object.freeze({
    /** Submitted */
    Submitted: 0,
    /** Accepted */
    Accepted: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type PosQtyStatus = (typeof PosQtyStatus)[keyof typeof PosQtyStatus];
