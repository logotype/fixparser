/**
 * The basis of the return price.
 * - Tag: 42766
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRatePriceBasis = Object.freeze({
    Gross: 0,
    /** Net */
    Net: 1,
    /** Accrued */
    Accrued: 2,
    /** Clean net */
    CleanNet: 3,
} as const);

type ReturnRatePriceBasis = (typeof ReturnRatePriceBasis)[keyof typeof ReturnRatePriceBasis];
