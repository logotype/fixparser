/**
 * Specifies the type of reference entity for first-to-default CDS basket contracts.
 * - Tag: 1956
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReferenceEntityType = Object.freeze({
    /** Asian */
    Asian: 1,
    /** Australian and New Zealand */
    AustralianNewZealand: 2,
    /** European emerging markets */
    EuropeanEmergingMarkets: 3,
    /** Japanese */
    Japanese: 4,
    /** North American high yield */
    NorthAmericanHighYield: 5,
    /** North American insurance */
    NorthAmericanInsurance: 6,
    /** North American investment grade */
    NorthAmericanInvestmentGrade: 7,
    /** Singaporean */
    Singaporean: 8,
    /** Western European */
    WesternEuropean: 9,
    /** Western European insurance */
    WesternEuropeanInsurance: 10,
} as const);

type ReferenceEntityType = (typeof ReferenceEntityType)[keyof typeof ReferenceEntityType];
