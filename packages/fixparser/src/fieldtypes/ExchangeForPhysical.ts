/**
 * Indicates whether or not to exchange for phsyical.
 * - Tag: 411
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ExchangeForPhysical = Object.freeze({
    /** False */
    False: 'N',
    /** True */
    True: 'Y',
} as const);

type ExchangeForPhysical = (typeof ExchangeForPhysical)[keyof typeof ExchangeForPhysical];
