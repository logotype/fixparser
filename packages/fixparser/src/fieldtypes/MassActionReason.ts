/**
 * Reason for submission of mass action.
 * - Tag: 2675
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MassActionReason = Object.freeze({
    /** No special reason (default) */
    None: 0,
    TradingRiskControl: 1,
    ClearingRiskControl: 2,
    MarketMakerProtection: 3,
    StopTrading: 4,
    EmergencyAction: 5,
    SessionLossLogout: 6,
    DuplicateLogin: 7,
    ProductNotTraded: 8,
    InstrumentNotTraded: 9,
    CompleInstrumentDeleted: 10,
    CircuitBreakerActivated: 11,
    /** Other */
    Other: 99,
} as const);

type MassActionReason = (typeof MassActionReason)[keyof typeof MassActionReason];
