/**
 * Type of Trade Capture Report.
 * - Tag: 569
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeRequestType = Object.freeze({
    /** All Trades */
    AllTrades: 0,
    /** Matched trades matching criteria provided on request (Parties, ExecID, TradeID, OrderID, Instrument, InputSource, etc.) */
    MatchedTradesMatchingCriteria: 1,
    /** Unmatched trades that match criteria */
    UnmatchedTradesThatMatchCriteria: 2,
    /** Unreported trades that match criteria */
    UnreportedTradesThatMatchCriteria: 3,
    /** Advisories that match criteria */
    AdvisoriesThatMatchCriteria: 4,
} as const);

type TradeRequestType = (typeof TradeRequestType)[keyof typeof TradeRequestType];
