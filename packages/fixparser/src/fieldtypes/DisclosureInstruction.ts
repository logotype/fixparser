/**
 * Instruction to disclose information or to use default value of the receiver.
 * - Tag: 1814
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DisclosureInstruction = Object.freeze({
    /** No */
    No: 0,
    /** Yes */
    Yes: 1,
    /** Use default setting */
    UseDefaultSetting: 2,
} as const);

type DisclosureInstruction = (typeof DisclosureInstruction)[keyof typeof DisclosureInstruction];
