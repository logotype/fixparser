/**
 * Time unit associated with the provision's tenor period.
 * - Tag: 40097
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ProvisionDateTenorUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type ProvisionDateTenorUnit = (typeof ProvisionDateTenorUnit)[keyof typeof ProvisionDateTenorUnit];
