/**
 * Identifies the Confirmation transaction type.
 * - Tag: 666
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ConfirmTransType = Object.freeze({
    /** New */
    New: 0,
    /** Replace */
    Replace: 1,
    /** Cancel */
    Cancel: 2,
} as const);

type ConfirmTransType = (typeof ConfirmTransType)[keyof typeof ConfirmTransType];
