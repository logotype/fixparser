/**
 * The day of the week on which pricing takes place.
 * - Tag: 41228
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamPricingDayOfWeek = Object.freeze({
    /** Every day (the default if not specified) */
    EveryDay: 0,
    /** Monday */
    Monday: 1,
    /** Tuesday */
    Tuesday: 2,
    /** Wednesday */
    Wednesday: 3,
    /** Thursday */
    Thursday: 4,
    /** Friday */
    Friday: 5,
    /** Saturday */
    Saturday: 6,
    /** Sunday */
    Sunday: 7,
} as const);

type PaymentStreamPricingDayOfWeek = (typeof PaymentStreamPricingDayOfWeek)[keyof typeof PaymentStreamPricingDayOfWeek];
