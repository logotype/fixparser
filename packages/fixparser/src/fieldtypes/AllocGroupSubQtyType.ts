/**
 * Type of trade attribute defining a subgroup in an allocation group.
 * - Tag: 2980
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocGroupSubQtyType = Object.freeze({
    /** Trade type */
    TradeType: 1,
    /** Trade publication indicator */
    TradePublicationIndicator: 2,
    /** Order handling instruction */
    OrderHandlingInstruction: 3,
} as const);

type AllocGroupSubQtyType = (typeof AllocGroupSubQtyType)[keyof typeof AllocGroupSubQtyType];
