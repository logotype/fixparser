/**
 * Used to indicate the type of acknowledgement being sent.
 * - Tag: 1348
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ApplResponseType = Object.freeze({
    /** Request successfully processed */
    RequestSuccessfullyProcessed: 0,
    /** Application does not exist */
    ApplicationDoesNotExist: 1,
    /** Messages not available */
    MessagesNotAvailable: 2,
} as const);

type ApplResponseType = (typeof ApplResponseType)[keyof typeof ApplResponseType];
