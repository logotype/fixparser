/**
 * The specification of any provisions for calculating payment obligations when a floating rate is negative (either due to a quoted negative floating rate or by operation of a spread that is subtracted from the floating rate).
 * - Tag: 40807
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamNegativeRateTreatment = Object.freeze({
    /** Zero interest rate method */
    ZeroInterestRateMethod: 0,
    /** Negative interest rate method */
    NegativeInterestRateMethod: 1,
} as const);

type PaymentStreamNegativeRateTreatment =
    (typeof PaymentStreamNegativeRateTreatment)[keyof typeof PaymentStreamNegativeRateTreatment];
