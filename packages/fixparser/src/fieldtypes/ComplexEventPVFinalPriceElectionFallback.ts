/**
 * Specifies the fallback provisions for the hedging party in the determination of the final settlement price.
 * - Tag: 2599
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventPVFinalPriceElectionFallback = Object.freeze({
    Close: 0,
    HedgeElection: 1,
} as const);

type ComplexEventPVFinalPriceElectionFallback =
    (typeof ComplexEventPVFinalPriceElectionFallback)[keyof typeof ComplexEventPVFinalPriceElectionFallback];
