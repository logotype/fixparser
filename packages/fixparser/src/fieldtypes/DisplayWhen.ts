/**
 * Instructs when to refresh DisplayQty (1138).
 * - Tag: 1083
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DisplayWhen = Object.freeze({
    /** Immediate (after each fill) */
    Immediate: '1',
    /** Exhaust (when DisplayQty = 0) */
    Exhaust: '2',
} as const);

type DisplayWhen = (typeof DisplayWhen)[keyof typeof DisplayWhen];
