/**
 * Identifies beginning of new message and session protocol version by means of a session profile identifier (see FIX Session Layer for details). ALWAYS FIRST FIELD IN MESSAGE. (Always unencrypted).
 * - Tag: 8
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const BeginString = Object.freeze({
    /** Session profile FIX.4.2 */
    FIX42: 'FIX.4.2',
    /** Session profile FIX4 */
    FIX44: 'FIX.4.4',
    FIXT11: 'FIXT.1.1',
} as const);

type BeginString = (typeof BeginString)[keyof typeof BeginString];
