/**
 * Defines how a server handles distribution of a truncated book. Defaults to broker option.
 * - Tag: 547
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MDImplicitDelete = Object.freeze({
    /** Server must send an explicit delete for bids or offers falling outside the requested MarketDepth of the request */
    No: 'N',
    /** Client has responsibility for implicitly deleting bids or offers falling outside the MarketDepth of the request */
    Yes: 'Y',
} as const);

type MDImplicitDelete = (typeof MDImplicitDelete)[keyof typeof MDImplicitDelete];
