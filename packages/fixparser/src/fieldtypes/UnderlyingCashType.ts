/**
 * Used for derivatives that deliver into cash underlying.
 * - Tag: 974
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const UnderlyingCashType = Object.freeze({
    /** FIXED */
    FIXED: 'FIXED',
    /** DIFF */
    DIFF: 'DIFF',
} as const);

type UnderlyingCashType = (typeof UnderlyingCashType)[keyof typeof UnderlyingCashType];
