/**
 * Qualifier for price. May be used when the price needs to be explicitly qualified.
 * - Tag: 2710
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PriceQualifier = Object.freeze({
    AccruedInterestIsFactored: 0,
    TaxIsFactored: 1,
    BondAmortizationIsFactored: 2,
} as const);

type PriceQualifier = (typeof PriceQualifier)[keyof typeof PriceQualifier];
