/**
 * Defines when the trigger will hit, i.e. the action specified by the trigger instructions will come into effect.
 * - Tag: 1100
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TriggerType = Object.freeze({
    /** Partial Execution */
    PartialExecution: '1',
    /** Specified Trading Session */
    SpecifiedTradingSession: '2',
    /** Next Auction */
    NextAuction: '3',
    /** Price Movement */
    PriceMovement: '4',
    /** On Order Entry or order modification entry */
    OnOrderEntryOrModification: '5',
} as const);

type TriggerType = (typeof TriggerType)[keyof typeof TriggerType];
