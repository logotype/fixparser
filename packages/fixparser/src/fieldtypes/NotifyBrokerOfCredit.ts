/**
 * Indicates whether or not details should be communicated to BrokerOfCredit (i.e. step-in broker).
 * - Tag: 208
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const NotifyBrokerOfCredit = Object.freeze({
    /** Details should not be communicated */
    DetailsShouldNotBeCommunicated: 'N',
    /** Details should be communicated */
    DetailsShouldBeCommunicated: 'Y',
} as const);

type NotifyBrokerOfCredit = (typeof NotifyBrokerOfCredit)[keyof typeof NotifyBrokerOfCredit];
