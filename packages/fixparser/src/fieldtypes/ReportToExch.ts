/**
 * Identifies party of trade responsible for exchange reporting.
 * - Tag: 113
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ReportToExch = Object.freeze({
    /** Indicates the party sending message will report trade */
    SenderReports: 'N',
    /** Indicates the party receiving message must report trade */
    ReceiverReports: 'Y',
} as const);

type ReportToExch = (typeof ReportToExch)[keyof typeof ReportToExch];
