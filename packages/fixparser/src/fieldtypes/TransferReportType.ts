/**
 * Indicates the type of transfer report.
 * - Tag: 2444
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferReportType = Object.freeze({
    /** Submit */
    Submit: 0,
    /** Alleged */
    Alleged: 1,
} as const);

type TransferReportType = (typeof TransferReportType)[keyof typeof TransferReportType];
