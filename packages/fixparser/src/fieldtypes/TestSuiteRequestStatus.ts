/**
 * Status of the TestSuiteDefinitionRequest(35=EL) message being responded to.
 * - Tag: 3065
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestSuiteRequestStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    Rejected: 2,
} as const);

type TestSuiteRequestStatus = (typeof TestSuiteRequestStatus)[keyof typeof TestSuiteRequestStatus];
