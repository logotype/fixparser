/**
 * Specifies the condition of title transfer.
 * - Tag: 41069
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamTitleTransferCondition = Object.freeze({
    /** Transfers with risk of loss */
    Transfers: 0,
    /** Does not transfer with risk of loss */
    DoesNotTransfer: 1,
} as const);

type DeliveryStreamTitleTransferCondition =
    (typeof DeliveryStreamTitleTransferCondition)[keyof typeof DeliveryStreamTitleTransferCondition];
