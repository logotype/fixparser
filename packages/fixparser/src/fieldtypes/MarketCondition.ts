/**
 * Market condition. In the context of ESMA RTS 8 it is important that trading venues communicate the condition of the market, particularly "stressed" and "exceptional", in order to provide incentives for firms contributing to liquidity.
 * - Tag: 2705
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketCondition = Object.freeze({
    Normal: 0,
    Stressed: 1,
    Exceptional: 2,
} as const);

type MarketCondition = (typeof MarketCondition)[keyof typeof MarketCondition];
