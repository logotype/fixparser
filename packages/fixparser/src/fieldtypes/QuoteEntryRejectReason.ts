/**
 * Reason Quote Entry was rejected:
 * - Tag: 368
 * - FIX Specification type: int
 * - FIX Specification version: FIX42
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteEntryRejectReason = Object.freeze({
    /** Unknown symbol (Security) */
    UnknownSymbol: 1,
    /** Exchange (Security) closed */
    Exchange: 2,
    /** Quote exceeds limit */
    QuoteExceedsLimit: 3,
    /** Too late to enter */
    TooLateToEnter: 4,
    /** Unknown Quote */
    UnknownQuote: 5,
    /** Duplicate Quote */
    DuplicateQuote: 6,
    /** Invalid bid/ask spread */
    InvalidBidAskSpread: 7,
    /** Invalid price */
    InvalidPrice: 8,
    /** Not authorized to quote security */
    NotAuthorizedToQuoteSecurity: 9,
} as const);

type QuoteEntryRejectReason = (typeof QuoteEntryRejectReason)[keyof typeof QuoteEntryRejectReason];
