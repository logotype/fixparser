/**
 * Expiration Quantity type
 * - Tag: 982
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExpirationQtyType = Object.freeze({
    /** Auto Exercise */
    AutoExercise: 1,
    /** Non Auto Exercise */
    NonAutoExercise: 2,
    /** Final Will Be Exercised */
    FinalWillBeExercised: 3,
    /** Contrary Intention */
    ContraryIntention: 4,
    /** Difference */
    Difference: 5,
} as const);

type ExpirationQtyType = (typeof ExpirationQtyType)[keyof typeof ExpirationQtyType];
