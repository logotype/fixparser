/**
 * Indicates whether this message is the last report message in response to a request message, e.g. OrderMassStatusRequest(35=AF), TradeCaptureReportRequest(35=AD).
 * - Tag: 912
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LastRptRequested = Object.freeze({
    /** Not last message */
    NotLastMessage: 'N',
    /** Last message */
    LastMessage: 'Y',
} as const);

type LastRptRequested = (typeof LastRptRequested)[keyof typeof LastRptRequested];
