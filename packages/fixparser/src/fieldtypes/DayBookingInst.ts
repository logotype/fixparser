/**
 * Indicates whether or not automatic booking can occur.
 * - Tag: 589
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DayBookingInst = Object.freeze({
    /** Can trigger booking without reference to the order initiator ("auto") */
    Auto: '0',
    /** Speak with order initiator before booking ("speak first") */
    SpeakWithOrderInitiatorBeforeBooking: '1',
    /** Accumulate */
    Accumulate: '2',
} as const);

type DayBookingInst = (typeof DayBookingInst)[keyof typeof DayBookingInst];
