/**
 * Identifies the type of ListExecInst (69).
 * - Tag: 433
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ListExecInstType = Object.freeze({
    /** Immediate */
    Immediate: '1',
    /** Wait for Execut Instruction (i.e. a List Execut message or phone call before proceeding with execution of the list) */
    WaitForInstruction: '2',
    /** Exchange/switch CIV order - Sell driven */
    SellDriven: '3',
    /** Exchange/switch CIV order - Buy driven, cash top-up (i.e. additional cash will be provided to fulfill the order) */
    BuyDrivenCashTopUp: '4',
    /** Exchange/switch CIV order - Buy driven, cash withdraw (i.e. additional cash will not be provided to fulfill the order) */
    BuyDrivenCashWithdraw: '5',
} as const);

type ListExecInstType = (typeof ListExecInstType)[keyof typeof ListExecInstType];
