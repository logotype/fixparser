/**
 * Method for price quotation
 * - Tag: 1196
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PriceQuoteMethod = Object.freeze({
    /** Standard, money per unit of a physical */
    Standard: 'STD',
    /** Index */
    Index: 'INX',
    /** Interest rate Index */
    InterestRateIndex: 'INT',
    /** Percent of Par */
    PercentOfPar: 'PCTPAR',
} as const);

type PriceQuoteMethod = (typeof PriceQuoteMethod)[keyof typeof PriceQuoteMethod];
