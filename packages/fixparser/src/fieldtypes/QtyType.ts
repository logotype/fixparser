/**
 * Type of quantity specified in quantity field. ContractMultiplier (tag 231) is required when QtyType = 1 (Contracts). UnitOfMeasure (tag 996) and TimeUnit (tag 997) are required when QtyType = 2 (Units of Measure per Time Unit).
 * - Tag: 854
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QtyType = Object.freeze({
    /** Units (shares, par, currency) */
    Units: 0,
    /** Contracts */
    Contracts: 1,
    /** Unit of Measure per Time Unit */
    UnitsOfMeasurePerTimeUnit: 2,
} as const);

type QtyType = (typeof QtyType)[keyof typeof QtyType];
