/**
 * Code to identify the price a DiscretionOffsetValue (389) is related to and should be mathematically added to.
 * - Tag: 388
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DiscretionInst = Object.freeze({
    /** Related to displayed price */
    RelatedToDisplayedPrice: '0',
    /** Related to market price */
    RelatedToMarketPrice: '1',
    /** Related to primary price */
    RelatedToPrimaryPrice: '2',
    /** Related to local primary price */
    RelatedToLocalPrimaryPrice: '3',
    /** Related to midpoint price */
    RelatedToMidpointPrice: '4',
    /** Related to last trade price */
    RelatedToLastTradePrice: '5',
    /** Related to VWAP */
    RelatedToVWAP: '6',
    /** Average Price Guarantee */
    AveragePriceGuarantee: '7',
} as const);

type DiscretionInst = (typeof DiscretionInst)[keyof typeof DiscretionInst];
