/**
 * Specifies a suitable settlement sub-method for a given settlement method.
 * - Tag: 2579
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlSubMethod = Object.freeze({
    Shares: 1,
    Derivatives: 2,
    PaymentVsPayment: 3,
    Notional: 4,
    Cascade: 5,
    Repurchase: 6,
    Other: 99,
} as const);

type SettlSubMethod = (typeof SettlSubMethod)[keyof typeof SettlSubMethod];
