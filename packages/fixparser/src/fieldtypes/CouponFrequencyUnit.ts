/**
 * Time unit associated with the frequency of the bond's coupon payment.
 * - Tag: 1949
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const CouponFrequencyUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
    /** Hour */
    Hour: 'H',
    /** Minute */
    Minute: 'Min',
    /** Second */
    Second: 'S',
    /** Term */
    Term: 'T',
} as const);

type CouponFrequencyUnit = (typeof CouponFrequencyUnit)[keyof typeof CouponFrequencyUnit];
