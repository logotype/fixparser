/**
 * The inflation lag period day type.
 * - Tag: 40810
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamInflationLagDayType = Object.freeze({
    /** Business */
    Business: 0,
    /** Calendar */
    Calendar: 1,
    /** Commodity business */
    CommodityBusiness: 2,
    /** Currency business */
    CurrencyBusiness: 3,
    /** Exchange business */
    ExchangeBusiness: 4,
    /** Scheduled trading day */
    ScheduledTradingDay: 5,
} as const);

type PaymentStreamInflationLagDayType =
    (typeof PaymentStreamInflationLagDayType)[keyof typeof PaymentStreamInflationLagDayType];
