/**
 * Used for options when delivering the order to an execution system/exchange to specify if the order is for a customer or the firm placing the order itself.
 * - Tag: 204
 * - FIX Specification type: int
 * - FIX Specification version: FIX42
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CustomerOrFirm = Object.freeze({
    /** Customer */
    Customer: 0,
    /** Firm */
    Firm: 1,
} as const);

type CustomerOrFirm = (typeof CustomerOrFirm)[keyof typeof CustomerOrFirm];
