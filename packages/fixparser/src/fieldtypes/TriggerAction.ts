/**
 * Defines the type of action to take when the trigger hits.
 * - Tag: 1101
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TriggerAction = Object.freeze({
    /** Activate */
    Activate: '1',
    /** Modify */
    Modify: '2',
    /** Cancel */
    Cancel: '3',
} as const);

type TriggerAction = (typeof TriggerAction)[keyof typeof TriggerAction];
