/**
 * Indicates the type of transfer.
 * - Tag: 2441
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferScope = Object.freeze({
    /** Inter-firm transfer */
    InterFirmTransfer: 0,
    /** Intra-firm transfer */
    IntraFirmTransfer: 1,
    /** Clearing Member Trade Assignment */
    CMTA: 2,
} as const);

type TransferScope = (typeof TransferScope)[keyof typeof TransferScope];
