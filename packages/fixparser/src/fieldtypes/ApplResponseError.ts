/**
 * Used to return an error code or text associated with a response to an Application Request.
 * - Tag: 1354
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ApplResponseError = Object.freeze({
    /** Application does not exist */
    ApplicationDoesNotExist: 0,
    /** Messages requested are not available */
    MessagesRequestedAreNotAvailable: 1,
    /** User not authorized for application */
    UserNotAuthorizedForApplication: 2,
} as const);

type ApplResponseError = (typeof ApplResponseError)[keyof typeof ApplResponseError];
