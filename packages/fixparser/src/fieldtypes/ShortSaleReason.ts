/**
 * Reason for short sale.
 * - Tag: 853
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ShortSaleReason = Object.freeze({
    /** Dealer Sold Short */
    DealerSoldShort: 0,
    /** Dealer Sold Short Exempt */
    DealerSoldShortExempt: 1,
    /** Selling Customer Sold Short */
    SellingCustomerSoldShort: 2,
    /** Selling Customer Sold Short Exempt */
    SellingCustomerSoldShortExempt: 3,
    /** Qualified Service Representative (QSR) or Automatic Give-up (AGU) Contra Side Sold Short */
    QualifiedServiceRepresentative: 4,
    /** QSR or AGU Contra Side Sold Short Exempt */
    QSROrAGUContraSideSoldShortExempt: 5,
} as const);

type ShortSaleReason = (typeof ShortSaleReason)[keyof typeof ShortSaleReason];
