/**
 * Code to identify the type of BidDescriptor (400).
 * - Tag: 399
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const BidDescriptorType = Object.freeze({
    /** Sector */
    Sector: 1,
    /** Country */
    Country: 2,
    /** Index */
    Index: 3,
} as const);

type BidDescriptorType = (typeof BidDescriptorType)[keyof typeof BidDescriptorType];
