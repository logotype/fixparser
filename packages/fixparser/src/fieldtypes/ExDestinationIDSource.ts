/**
 * The ID source of ExDestination
 * - Tag: 1133
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ExDestinationIDSource = Object.freeze({
    /** BIC (Bank Identification Code) (ISO 9362) */
    BIC: 'B',
    /** Generally accepted market participant identifier (e.g. NASD mnemonic) */
    GeneralIdentifier: 'C',
    /** Proprietary / Custom code */
    Proprietary: 'D',
    /** ISO Country Code */
    ISOCountryCode: 'E',
    /** MIC (ISO 10383 - Market Identifier Code) */
    MIC: 'G',
} as const);

type ExDestinationIDSource = (typeof ExDestinationIDSource)[keyof typeof ExDestinationIDSource];
