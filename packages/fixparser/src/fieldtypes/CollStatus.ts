/**
 * Collateral Status
 * - Tag: 910
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollStatus = Object.freeze({
    /** Unassigned */
    Unassigned: 0,
    /** Partially Assigned */
    PartiallyAssigned: 1,
    /** Assignment Proposed */
    AssignmentProposed: 2,
    /** Assigned (Accepted) */
    Assigned: 3,
    /** Challenged */
    Challenged: 4,
    Reused: 5,
} as const);

type CollStatus = (typeof CollStatus)[keyof typeof CollStatus];
