/**
 * Forward start premium type.
 * - Tag: 41160
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentForwardStartType = Object.freeze({
    /** Prepaid */
    Prepaid: 0,
    /** Post-paid */
    Postpaid: 1,
    /** Variable */
    Variable: 2,
    /** Fixed */
    Fixed: 3,
} as const);

type PaymentForwardStartType = (typeof PaymentForwardStartType)[keyof typeof PaymentForwardStartType];
