/**
 * Method under which assignment was conducted
 * - Tag: 1049
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const InstrmtAssignmentMethod = Object.freeze({
    /** Pro rata */
    ProRata: 'P',
    /** Random */
    Random: 'R',
} as const);

type InstrmtAssignmentMethod = (typeof InstrmtAssignmentMethod)[keyof typeof InstrmtAssignmentMethod];
