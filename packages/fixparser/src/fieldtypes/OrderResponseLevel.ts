/**
 * The level of response requested from receiver of mass order messages. A default value should be bilaterally agreed.
 * - Tag: 2427
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OrderResponseLevel = Object.freeze({
    NoAck: 0,
    MinimumAck: 1,
    AckEach: 2,
    SummaryAck: 3,
} as const);

type OrderResponseLevel = (typeof OrderResponseLevel)[keyof typeof OrderResponseLevel];
