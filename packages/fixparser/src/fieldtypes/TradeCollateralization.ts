/**
 * TradeCollateralization
 * - Tag: 1936
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeCollateralization = Object.freeze({
    /** Uncollateralized */
    Uncollateralized: 0,
    /** Partially collateralized */
    PartiallyCollateralized: 1,
    /** One-way collaterallization */
    OneWayCollaterallization: 2,
    /** Fully collateralized */
    FullyCollateralized: 3,
    NetExposure: 4,
} as const);

type TradeCollateralization = (typeof TradeCollateralization)[keyof typeof TradeCollateralization];
