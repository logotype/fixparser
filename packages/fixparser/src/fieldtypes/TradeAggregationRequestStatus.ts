/**
 * Status of the trade aggregation request.
 * - Tag: 2790
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeAggregationRequestStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Rejected */
    Rejected: 1,
} as const);

type TradeAggregationRequestStatus = (typeof TradeAggregationRequestStatus)[keyof typeof TradeAggregationRequestStatus];
