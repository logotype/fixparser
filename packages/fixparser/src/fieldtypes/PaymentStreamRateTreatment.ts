/**
 * Specifies the yield calculation treatment for the index.
 * - Tag: 40796
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamRateTreatment = Object.freeze({
    /** Bond equivalent yield */
    BondEquivalentYield: 0,
    /** Money market yield */
    MoneyMarketYield: 1,
} as const);

type PaymentStreamRateTreatment = (typeof PaymentStreamRateTreatment)[keyof typeof PaymentStreamRateTreatment];
