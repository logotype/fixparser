/**
 * Specifies the action to be taken for the given order.
 * - Tag: 2429
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OrderEntryAction = Object.freeze({
    /** Add */
    Add: '1',
    /** Modify */
    Modify: '2',
    /** Delete / Cancel */
    Delete: '3',
    /** Suspend */
    Suspend: '4',
    /** Release */
    Release: '5',
} as const);

type OrderEntryAction = (typeof OrderEntryAction)[keyof typeof OrderEntryAction];
