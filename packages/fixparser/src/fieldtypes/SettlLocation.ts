/**
 * Identifies Settlement Depository or Country Code (ISITC spec)
 * - Tag: 166
 * - FIX Specification type: String
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlLocation = Object.freeze({
    /** CEDEL */
    CEDEL: 'CED',
    /** Depository Trust Company */
    DepositoryTrustCompany: 'DTC',
    /** Euroclear */
    EuroClear: 'EUR',
    /** Federal Book Entry */
    FederalBookEntry: 'FED',
    /** Local Market Settle Location */
    LocalMarketSettleLocation: 'ISO Country Code',
    /** Physical */
    Physical: 'PNY',
    /** Participant Trust Company */
    ParticipantTrustCompany: 'PTC',
} as const);

type SettlLocation = (typeof SettlLocation)[keyof typeof SettlLocation];
