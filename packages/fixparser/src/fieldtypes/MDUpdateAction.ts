/**
 * Type of Market Data update action.
 * - Tag: 279
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MDUpdateAction = Object.freeze({
    /** New */
    New: '0',
    /** Change */
    Change: '1',
    /** Delete */
    Delete: '2',
    /** Delete Thru */
    DeleteThru: '3',
    /** Delete From */
    DeleteFrom: '4',
    /** Overlay */
    Overlay: '5',
} as const);

type MDUpdateAction = (typeof MDUpdateAction)[keyof typeof MDUpdateAction];
