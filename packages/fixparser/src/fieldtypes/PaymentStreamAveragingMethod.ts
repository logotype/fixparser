/**
 * When rate averaging is applicable, used to specify whether a weighted or unweighted average calculation method is to be used.
 * - Tag: 40806
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamAveragingMethod = Object.freeze({
    /** Unweighted */
    Unweighted: 0,
    /** Weighted */
    Weighted: 1,
} as const);

type PaymentStreamAveragingMethod = (typeof PaymentStreamAveragingMethod)[keyof typeof PaymentStreamAveragingMethod];
