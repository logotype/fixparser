/**
 * Identifies a specific settlement session
 * - Tag: 716
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlSessID = Object.freeze({
    /** Intraday */
    Intraday: 'ITD',
    /** Regular Trading Hours */
    RegularTradingHours: 'RTH',
    /** Electronic Trading Hours */
    ElectronicTradingHours: 'ETH',
    /** End Of Day */
    EndOfDay: 'EOD',
} as const);

type SettlSessID = (typeof SettlSessID)[keyof typeof SettlSessID];
