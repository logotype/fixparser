/**
 * Type of matching exception.
 * - Tag: 2773
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MatchExceptionType = Object.freeze({
    /** No matching confirmation */
    NoMatchingConfirmation: 0,
    /** No matching allocation */
    NoMatchingAllocation: 1,
    /** Allocation data element missing */
    AllocationDataElementMissing: 2,
    /** Confirmation data element missing */
    ConfirmationDataElementMissing: 3,
    /** Data difference not within tolerance */
    DataDifferenceNotWithinTolerance: 4,
    /** Match within tolerance */
    MatchWithinTolerance: 5,
    /** Other */
    Other: 99,
} as const);

type MatchExceptionType = (typeof MatchExceptionType)[keyof typeof MatchExceptionType];
