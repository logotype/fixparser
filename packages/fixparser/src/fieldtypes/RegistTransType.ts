/**
 * Identifies Registration Instructions transaction type
 * - Tag: 514
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const RegistTransType = Object.freeze({
    /** New */
    New: '0',
    /** Cancel */
    Cancel: '2',
    /** Replace */
    Replace: '1',
} as const);

type RegistTransType = (typeof RegistTransType)[keyof typeof RegistTransType];
