/**
 * Indicates the current state of the instrument.
 * - Tag: 965
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SecurityStatus = Object.freeze({
    Active: '1',
    Inactive: '2',
    ActiveClosingOrdersOnly: '3',
    Expired: '4',
    Delisted: '5',
    KnockedOut: '6',
    KnockOutRevoked: '7',
    PendingExpiry: '8',
    Suspended: '9',
    Published: '10',
    PendingDeletion: '11',
} as const);

type SecurityStatus = (typeof SecurityStatus)[keyof typeof SecurityStatus];
