/**
 * Identifies whether the rate spread is an absolute value to be added to the index rate or a percentage of the index rate.
 * - Tag: 41206
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamRateSpreadType = Object.freeze({
    /** Absolute */
    Absolute: 0,
    /** Percentage */
    Percentage: 1,
} as const);

type PaymentStreamRateSpreadType = (typeof PaymentStreamRateSpreadType)[keyof typeof PaymentStreamRateSpreadType];
