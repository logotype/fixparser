/**
 * TargetStrategy
 * - Tag: 847
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TargetStrategy = Object.freeze({
    /** VWAP */
    VWAP: 1,
    /** Participate (i.e. aim to be x percent of the market volume) */
    Participate: 2,
    /** Mininize market impact */
    MininizeMarketImpact: 3,
} as const);

type TargetStrategy = (typeof TargetStrategy)[keyof typeof TargetStrategy];
