/**
 * Identifies class or source of the Currency(15) value.
 * - Tag: 2897
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const CurrencyCodeSource = Object.freeze({
    /** CUSIP */
    CUSIP: '1',
    /** SEDOL */
    SEDOL: '2',
    /** ISIN */
    ISINNumber: '4',
    /** ISO Currency Code (ISO 4217) */
    ISOCurrencyCode: '6',
    FinancialInstrumentGlobalIdentifier: 'S',
    /** Digital Token Identifier (ISO 24165) */
    DigitalTokenIdentifier: 'Y',
} as const);

type CurrencyCodeSource = (typeof CurrencyCodeSource)[keyof typeof CurrencyCodeSource];
