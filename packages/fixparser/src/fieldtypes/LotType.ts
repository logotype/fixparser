/**
 * Defines the lot type assigned to the order.
 * - Tag: 1093
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LotType = Object.freeze({
    /** Odd Lot */
    OddLot: '1',
    /** Round Lot */
    RoundLot: '2',
    /** Block Lot */
    BlockLot: '3',
    /** Round lot based upon UnitOfMeasure(996) */
    RoundLotBasedUpon: '4',
} as const);

type LotType = (typeof LotType)[keyof typeof LotType];
