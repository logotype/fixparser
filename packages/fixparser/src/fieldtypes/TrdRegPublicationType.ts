/**
 * TrdRegPublicationType
 * - Tag: 2669
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TrdRegPublicationType = Object.freeze({
    PreTradeTransparencyWaiver: 0,
    PostTradeDeferral: 1,
    ExemptFromPublication: 2,
    OrderLevelPublicationToSubscribers: 3,
    PriceLevelPublicationToSubscribers: 4,
    OrderLevelPublicationToThePublic: 5,
    PublicationInternalToExecutionVenue: 6,
} as const);

type TrdRegPublicationType = (typeof TrdRegPublicationType)[keyof typeof TrdRegPublicationType];
