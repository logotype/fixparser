/**
 * Used to specify whether the principal is paying or receiving the fixed rate in an interest rate swap.
 * - Tag: 1933
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IRSDirection = Object.freeze({
    Pay: 'PAY',
    /** Principal is receiving fixed rate */
    Rcv: 'RCV',
    /** Swap is float/float or fixed/fixed */
    NA: 'NA',
} as const);

type IRSDirection = (typeof IRSDirection)[keyof typeof IRSDirection];
