/**
 * Status of the trade give-up relative to the group identified in AllocGroupID(1730).
 * - Tag: 2767
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocGroupStatus = Object.freeze({
    Added: 0,
    Canceled: 1,
    Replaced: 2,
    Changed: 3,
    Pending: 4,
} as const);

type AllocGroupStatus = (typeof AllocGroupStatus)[keyof typeof AllocGroupStatus];
