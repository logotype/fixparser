/**
 * Specifies the party's or parties' intention to clear the trade.
 * - Tag: 1924
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ClearingIntention = Object.freeze({
    /** Do not intend to clear */
    DoNotIntendToClear: 0,
    /** Intend to clear */
    IntendToClear: 1,
} as const);

type ClearingIntention = (typeof ClearingIntention)[keyof typeof ClearingIntention];
