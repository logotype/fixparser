/**
 * Result of the credit limit check request.
 * - Tag: 2326
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckRequestResult = Object.freeze({
    /** Successful (default) */
    Successful: 0,
    /** Invalid party */
    InvalidParty: 1,
    /** Requested amount exceeds credit limit */
    ReqExceedsCreditLimit: 2,
    /** Requested amount exceeds clip size limit */
    ReqExceedsClipSizeLimit: 3,
    /** Request exceeds maximum notional order amount */
    ReqExceedsMaxNotional: 4,
    /** Other */
    Other: 99,
} as const);

type RiskLimitCheckRequestResult = (typeof RiskLimitCheckRequestResult)[keyof typeof RiskLimitCheckRequestResult];
