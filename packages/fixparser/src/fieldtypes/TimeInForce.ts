/**
 * Specifies how long the order remains in effect. Absence of this field is interpreted as DAY. NOTE not applicable to CIV Orders.
 * - Tag: 59
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TimeInForce = Object.freeze({
    Day: '0',
    GoodTillCancel: '1',
    AtTheOpening: '2',
    ImmediateOrCancel: '3',
    FillOrKill: '4',
    GoodTillCrossing: '5',
    GoodTillDate: '6',
    AtTheClose: '7',
    GoodThroughCrossing: '8',
    AtCrossing: '9',
    GoodForTime: 'A',
    GoodForAuction: 'B',
    GoodForMonth: 'C',
} as const);

type TimeInForce = (typeof TimeInForce)[keyof typeof TimeInForce];
