/**
 * Time unit multiplier for the relative initial fixing date offset.
 * - Tag: 40760
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PaymentStreamPaymentDateOffsetUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type PaymentStreamPaymentDateOffsetUnit =
    (typeof PaymentStreamPaymentDateOffsetUnit)[keyof typeof PaymentStreamPaymentDateOffsetUnit];
