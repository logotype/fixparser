/**
 * Action to take to resolve an application message queue (backlog).
 * - Tag: 815
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ApplQueueAction = Object.freeze({
    /** No Action Taken */
    NoActionTaken: 0,
    /** Queue Flushed */
    QueueFlushed: 1,
    /** Overlay Last */
    OverlayLast: 2,
    /** End Session */
    EndSession: 3,
} as const);

type ApplQueueAction = (typeof ApplQueueAction)[keyof typeof ApplQueueAction];
