/**
 * Identifies the message transaction type.
 * - Tag: 2811
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PayRequestTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
} as const);

type PayRequestTransType = (typeof PayRequestTransType)[keyof typeof PayRequestTransType];
