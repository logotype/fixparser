/**
 * Indicates the status of the risk limit check performed on a trade.
 * - Tag: 2343
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckStatus = Object.freeze({
    Accepted: 0,
    Rejected: 1,
    ClaimRequired: 2,
    PreDefinedLimitCheckSucceeded: 3,
    PreDefinedLimitCheckFailed: 4,
    PreDefinedAutoAcceptRuleInvoked: 5,
    PreDefinedAutoRejectRuleInvoked: 6,
    AcceptedByClearingFirm: 7,
    RejectedByClearingFirm: 8,
    Pending: 9,
    AcceptedByCreditHub: 10,
    RejectedByCreditHub: 11,
    PendingCreditHubCheck: 12,
    AcceptedByExecVenue: 13,
    RejectedByExecVenue: 14,
} as const);

type RiskLimitCheckStatus = (typeof RiskLimitCheckStatus)[keyof typeof RiskLimitCheckStatus];
