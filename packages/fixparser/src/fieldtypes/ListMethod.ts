/**
 * Indicates whether instruments are pre-listed only or can also be defined via user request
 * - Tag: 1198
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ListMethod = Object.freeze({
    /** pre-listed only */
    PreListedOnly: 0,
    /** user requested */
    UserRequested: 1,
} as const);

type ListMethod = (typeof ListMethod)[keyof typeof ListMethod];
