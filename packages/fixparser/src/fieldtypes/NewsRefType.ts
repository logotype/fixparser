/**
 * Type of reference to another News(35=B) message item.
 * - Tag: 1477
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NewsRefType = Object.freeze({
    /** Replacement */
    Replacement: 0,
    /** Other language */
    OtherLanguage: 1,
    /** Complimentary */
    Complimentary: 2,
    Withdrawal: 3,
} as const);

type NewsRefType = (typeof NewsRefType)[keyof typeof NewsRefType];
