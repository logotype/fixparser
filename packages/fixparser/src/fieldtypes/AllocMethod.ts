/**
 * Specifies the method under which a trade quantity was allocated.
 * - Tag: 1002
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocMethod = Object.freeze({
    /** Automatic */
    Automatic: 1,
    /** Guarantor */
    Guarantor: 2,
    /** Manual */
    Manual: 3,
    /** Broker assigned */
    BrokerAssigned: 4,
} as const);

type AllocMethod = (typeof AllocMethod)[keyof typeof AllocMethod];
