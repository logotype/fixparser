/**
 * Specifies whether a quote is public, i.e. available to the market, or private, i.e. available to a specified counterparty only.
 * - Tag: 1171
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PrivateQuote = Object.freeze({
    /** Private Quote */
    PrivateQuote: 'Y',
    /** Public Quote */
    PublicQuote: 'N',
} as const);

type PrivateQuote = (typeof PrivateQuote)[keyof typeof PrivateQuote];
