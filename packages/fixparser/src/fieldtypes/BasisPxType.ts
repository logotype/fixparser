/**
 * Code to represent the basis price type.
 * - Tag: 419
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const BasisPxType = Object.freeze({
    /** Closing price at morning session */
    ClosingPriceAtMorningSession: '2',
    /** Closing price */
    ClosingPrice: '3',
    /** Current price */
    CurrentPrice: '4',
    /** SQ */
    SQ: '5',
    /** VWAP through a day */
    VWAPThroughADay: '6',
    /** VWAP through a morning session */
    VWAPThroughAMorningSession: '7',
    /** VWAP through an afternoon session */
    VWAPThroughAnAfternoonSession: '8',
    /** VWAP through a day except "YORI" (an opening auction) */
    VWAPThroughADayExcept: '9',
    /** VWAP through a morning session except "YORI" (an opening auction) */
    VWAPThroughAMorningSessionExcept: 'A',
    /** VWAP through an afternoon session except "YORI" (an opening auction) */
    VWAPThroughAnAfternoonSessionExcept: 'B',
    /** Strike */
    Strike: 'C',
    /** Open */
    Open: 'D',
    /** Others */
    Others: 'Z',
} as const);

type BasisPxType = (typeof BasisPxType)[keyof typeof BasisPxType];
