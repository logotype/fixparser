/**
 * An indicator to override the normal procedure to roll up allocations for the same take-up firm.
 * - Tag: 1735
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocationRollupInstruction = Object.freeze({
    /** Roll up */
    Rollup: 0,
    /** Do not roll up */
    DoNotRollUp: 1,
} as const);

type AllocationRollupInstruction = (typeof AllocationRollupInstruction)[keyof typeof AllocationRollupInstruction];
