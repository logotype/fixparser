/**
 * Collateral Assignment Transaction Type
 * - Tag: 903
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollAsgnTransType = Object.freeze({
    /** New */
    New: 0,
    /** Replace */
    Replace: 1,
    /** Cancel */
    Cancel: 2,
    /** Release */
    Release: 3,
    /** Reverse */
    Reverse: 4,
} as const);

type CollAsgnTransType = (typeof CollAsgnTransType)[keyof typeof CollAsgnTransType];
