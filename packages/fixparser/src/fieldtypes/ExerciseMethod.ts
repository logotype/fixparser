/**
 * Exercise Method used to in performing assignment.
 * - Tag: 747
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ExerciseMethod = Object.freeze({
    /** Automatic */
    Automatic: 'A',
    /** Manual */
    Manual: 'M',
} as const);

type ExerciseMethod = (typeof ExerciseMethod)[keyof typeof ExerciseMethod];
