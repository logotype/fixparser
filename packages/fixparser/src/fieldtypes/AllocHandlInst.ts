/**
 * Indicates how the receiver (i.e. third party) of allocation information should handle/process the account details.
 * - Tag: 209
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocHandlInst = Object.freeze({
    /** Match */
    Match: 1,
    /** Forward */
    Forward: 2,
    /** Forward and Match */
    ForwardAndMatch: 3,
    AutoClaimGiveUp: 4,
} as const);

type AllocHandlInst = (typeof AllocHandlInst)[keyof typeof AllocHandlInst];
