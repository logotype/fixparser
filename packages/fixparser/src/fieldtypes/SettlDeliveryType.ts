/**
 * Identifies type of settlement
 * - Tag: 172
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlDeliveryType = Object.freeze({
    /** "Versus. Payment": Deliver (if Sell) or Receive (if Buy) vs. (Against) Payment */
    Versus: 0,
    /** "Free": Deliver (if Sell) or Receive (if Buy) Free */
    Free: 1,
    /** Tri-Party */
    TriParty: 2,
    /** Hold In Custody */
    HoldInCustody: 3,
} as const);

type SettlDeliveryType = (typeof SettlDeliveryType)[keyof typeof SettlDeliveryType];
