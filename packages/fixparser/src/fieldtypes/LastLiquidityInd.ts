/**
 * Indicator to identify whether this fill was a result of a liquidity provider providing or liquidity taker taking the liquidity.
 * - Tag: 851
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LastLiquidityInd = Object.freeze({
    NeitherAddedNorRemovedLiquidity: 0,
    /** Added Liquidity */
    AddedLiquidity: 1,
    /** Removed Liquidity */
    RemovedLiquidity: 2,
    /** Liquidity Routed Out */
    LiquidityRoutedOut: 3,
    /** Auction execution */
    Auction: 4,
    TriggeredStopOrder: 5,
    TriggeredContingencyOrder: 6,
    TriggeredMarketOrder: 7,
    RemovedLiquidityAfterFirmOrderCommitment: 8,
    AuctionExecutionAfterFirmOrderCommitment: 9,
    Unknown: 10,
    Other: 11,
} as const);

type LastLiquidityInd = (typeof LastLiquidityInd)[keyof typeof LastLiquidityInd];
