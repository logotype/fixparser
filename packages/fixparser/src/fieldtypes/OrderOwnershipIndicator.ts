/**
 * Change of ownership of an order to a specific party.
 * - Tag: 2679
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OrderOwnershipIndicator = Object.freeze({
    /** No change of ownership (default) */
    NoChange: 0,
    ExecutingPartyChange: 1,
    EnteringPartyChange: 2,
    SpecifiedPartyChange: 3,
} as const);

type OrderOwnershipIndicator = (typeof OrderOwnershipIndicator)[keyof typeof OrderOwnershipIndicator];
