/**
 * Specifies the type of limit check message.
 * - Tag: 2321
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckType = Object.freeze({
    Submit: 0,
    LimitConsumed: 1,
} as const);

type RiskLimitCheckType = (typeof RiskLimitCheckType)[keyof typeof RiskLimitCheckType];
