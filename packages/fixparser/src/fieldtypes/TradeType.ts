/**
 * Code to represent the type of trade.
 * - Tag: 418
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TradeType = Object.freeze({
    /** Agency */
    Agency: 'A',
    /** VWAP Guarantee */
    VWAPGuarantee: 'G',
    /** Guaranteed Close */
    GuaranteedClose: 'J',
    /** Risk Trade */
    RiskTrade: 'R',
} as const);

type TradeType = (typeof TradeType)[keyof typeof TradeType];
