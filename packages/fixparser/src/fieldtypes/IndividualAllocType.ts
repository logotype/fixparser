/**
 * Identifies whether the allocation is to be sub-allocated or allocated to a third party
 * - Tag: 992
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const IndividualAllocType = Object.freeze({
    /** Sub Allocate */
    SubAllocate: 1,
    /** Third Party Allocation */
    ThirdPartyAllocation: 2,
} as const);

type IndividualAllocType = (typeof IndividualAllocType)[keyof typeof IndividualAllocType];
