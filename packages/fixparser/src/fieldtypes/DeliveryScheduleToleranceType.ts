/**
 * Specifies the tolerance value type.
 * - Tag: 41046
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleToleranceType = Object.freeze({
    /** Absolute */
    Absolute: 0,
    /** Percentage */
    Percentage: 1,
} as const);

type DeliveryScheduleToleranceType = (typeof DeliveryScheduleToleranceType)[keyof typeof DeliveryScheduleToleranceType];
