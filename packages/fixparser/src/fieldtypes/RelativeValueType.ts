/**
 * Indicates the type of relative value measurement being specified.
 * - Tag: 2530
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelativeValueType = Object.freeze({
    ASWSpread: 1,
    OIS: 2,
    ZSpread: 3,
    DiscountMargin: 4,
    ISpread: 5,
    OAS: 6,
    GSpread: 7,
    CDSBasis: 8,
    CDSInterpolatedBasis: 9,
    DV01: 10,
    PV01: 11,
    CS01: 12,
} as const);

type RelativeValueType = (typeof RelativeValueType)[keyof typeof RelativeValueType];
