/**
 * Type of price used to determine upfront payment for swaps contracts.
 * - Tag: 1741
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const UpfrontPriceType = Object.freeze({
    /** Percentage (i.e. percent of par) (often called "dollar price" for fixed income) */
    Percentage: 1,
    /** Fixed amount (absolute value) */
    FixedAmount: 3,
} as const);

type UpfrontPriceType = (typeof UpfrontPriceType)[keyof typeof UpfrontPriceType];
