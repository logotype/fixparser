/**
 * Indicates that IOI is the result of an existing agency order or a facilitation position resulting from an agency order, not from principal trading or order solicitation activity.
 * - Tag: 130
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IOINaturalFlag = Object.freeze({
    /** Not Natural */
    NotNatural: 'N',
    /** Natural */
    Natural: 'Y',
} as const);

type IOINaturalFlag = (typeof IOINaturalFlag)[keyof typeof IOINaturalFlag];
