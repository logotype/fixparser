/**
 * Reason for the rejection of a Market Data request.
 * - Tag: 281
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MDReqRejReason = Object.freeze({
    /** Unknown symbol */
    UnknownSymbol: '0',
    /** Duplicate MDReqID */
    DuplicateMDReqID: '1',
    /** Insufficient Bandwidth */
    InsufficientBandwidth: '2',
    /** Insufficient Permissions */
    InsufficientPermissions: '3',
    /** Unsupported SubscriptionRequestType */
    UnsupportedSubscriptionRequestType: '4',
    /** Unsupported MarketDepth */
    UnsupportedMarketDepth: '5',
    /** Unsupported MDUpdateType */
    UnsupportedMDUpdateType: '6',
    /** Unsupported AggregatedBook */
    UnsupportedAggregatedBook: '7',
    /** Unsupported MDEntryType */
    UnsupportedMDEntryType: '8',
    /** Unsupported TradingSessionID */
    UnsupportedTradingSessionID: '9',
    /** Unsupported Scope */
    UnsupportedScope: 'A',
    /** Unsupported OpenCloseSettleFlag */
    UnsupportedOpenCloseSettleFlag: 'B',
    /** Unsupported MDImplicitDelete */
    UnsupportedMDImplicitDelete: 'C',
    /** Insufficient credit */
    InsufficientCredit: 'D',
} as const);

type MDReqRejReason = (typeof MDReqRejReason)[keyof typeof MDReqRejReason];
