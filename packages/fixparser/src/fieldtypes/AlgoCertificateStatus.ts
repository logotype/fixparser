/**
 * Status of the certification as provided by the regulatory authority.
 * - Tag: 3022
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AlgoCertificateStatus = Object.freeze({
    Draft: 0,
    Approved: 1,
    Submitted: 2,
} as const);

type AlgoCertificateStatus = (typeof AlgoCertificateStatus)[keyof typeof AlgoCertificateStatus];
