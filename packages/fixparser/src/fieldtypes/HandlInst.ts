/**
 * Instructions for order handling on Broker trading floor
 * - Tag: 21
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const HandlInst = Object.freeze({
    /** Automated execution order, private, no Broker intervention */
    AutomatedExecutionNoIntervention: '1',
    /** Automated execution order, public, Broker intervention OK */
    AutomatedExecutionInterventionOK: '2',
    /** Manual order, best execution */
    ManualOrder: '3',
} as const);

type HandlInst = (typeof HandlInst)[keyof typeof HandlInst];
