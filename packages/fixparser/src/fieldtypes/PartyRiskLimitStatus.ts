/**
 * The status of risk limits for a party.
 * - Tag: 2355
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyRiskLimitStatus = Object.freeze({
    Disabled: 0,
    Enabled: 1,
} as const);

type PartyRiskLimitStatus = (typeof PartyRiskLimitStatus)[keyof typeof PartyRiskLimitStatus];
