/**
 * Status of the SettlementStatusRequest(35=EC) message being responded to.
 * - Tag: 2966
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlStatusRequestStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    Rejected: 2,
} as const);

type SettlStatusRequestStatus = (typeof SettlStatusRequestStatus)[keyof typeof SettlStatusRequestStatus];
