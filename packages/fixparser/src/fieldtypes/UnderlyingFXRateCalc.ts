/**
 * Specifies whether the UnderlyingFxRate(1045) should be multiplied or divided.
 * - Tag: 1046
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const UnderlyingFXRateCalc = Object.freeze({
    /** Divide */
    Divide: 'D',
    /** Multiply */
    Multiply: 'M',
} as const);

type UnderlyingFXRateCalc = (typeof UnderlyingFXRateCalc)[keyof typeof UnderlyingFXRateCalc];
