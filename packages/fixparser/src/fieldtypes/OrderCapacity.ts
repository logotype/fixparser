/**
 * OrderCapacity
 * - Tag: 528
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OrderCapacity = Object.freeze({
    /** Agency */
    Agency: 'A',
    /** Proprietary */
    Proprietary: 'G',
    /** Individual */
    Individual: 'I',
    Principal: 'P',
    /** Riskless Principal */
    RisklessPrincipal: 'R',
    /** Agent for Other Member */
    AgentForOtherMember: 'W',
    /** Mixed capacity */
    MixedCapacity: 'M',
} as const);

type OrderCapacity = (typeof OrderCapacity)[keyof typeof OrderCapacity];
