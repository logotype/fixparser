/**
 * Type of relationship between two or more market segments.
 * - Tag: 2547
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketSegmentRelationship = Object.freeze({
    MarketSegmentPoolMember: 1,
    RetailSegment: 2,
    WholesaleSegment: 3,
} as const);

type MarketSegmentRelationship = (typeof MarketSegmentRelationship)[keyof typeof MarketSegmentRelationship];
