/**
 * Indicate whether a trade is eligible to be reported to more than one regulatory jurisdictions, e.g. due to overlapping reporting rules that require reporting to different jurisdictions.
 * - Tag: 2963
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MultiJurisdictionReportingIndicator = Object.freeze({
    /** Trade not eligible for multi-jurisdiction reporting */
    NotMultiJrsdctnEligible: 0,
    /** Trade eligible for multi-jurisdiction reporting */
    MultiJrsdctnEligible: 1,
} as const);

type MultiJurisdictionReportingIndicator =
    (typeof MultiJurisdictionReportingIndicator)[keyof typeof MultiJurisdictionReportingIndicator];
