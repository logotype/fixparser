/**
 * RateSource
 * - Tag: 1446
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RateSource = Object.freeze({
    /** Bloomberg */
    Bloomberg: 0,
    /** Reuters */
    Reuters: 1,
    /** Telerate */
    Telerate: 2,
    ISDARateOption: 3,
    /** Other */
    Other: 99,
} as const);

type RateSource = (typeof RateSource)[keyof typeof RateSource];
