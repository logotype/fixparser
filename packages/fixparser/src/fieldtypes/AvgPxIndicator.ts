/**
 * Average pricing indicator.
 * - Tag: 819
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AvgPxIndicator = Object.freeze({
    /** No average pricing */
    NoAveragePricing: 0,
    /** Trade is part of an average price group identified by the AvgPxGroupID(1731) */
    Trade: 1,
    /** Last trade of the average price group identified by the AvgPxGroupID(1731) */
    LastTrade: 2,
    NotionalValueAveragePxGroupTrade: 3,
    /** Trade is average priced */
    AveragePricedTrade: 4,
} as const);

type AvgPxIndicator = (typeof AvgPxIndicator)[keyof typeof AvgPxIndicator];
