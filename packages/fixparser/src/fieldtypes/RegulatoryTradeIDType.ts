/**
 * RegulatoryTradeIDType
 * - Tag: 1906
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RegulatoryTradeIDType = Object.freeze({
    Current: 0,
    Previous: 1,
    Block: 2,
    Related: 3,
    ClearedBlockTrade: 4,
    TradingVenueTransactionIdentifier: 5,
    ReportTrackingNumber: 6,
} as const);

type RegulatoryTradeIDType = (typeof RegulatoryTradeIDType)[keyof typeof RegulatoryTradeIDType];
