/**
 * Specifies the type of credit limit check model workflow to apply for the specified party
 * - Tag: 2339
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckModelType = Object.freeze({
    None: 0,
    PlusOneModel: 1,
    PingModel: 2,
    PushModel: 3,
} as const);

type RiskLimitCheckModelType = (typeof RiskLimitCheckModelType)[keyof typeof RiskLimitCheckModelType];
