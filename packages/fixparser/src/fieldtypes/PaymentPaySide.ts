/**
 * The side of the party paying the payment.
 * - Tag: 40214
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentPaySide = Object.freeze({
    /** Buy */
    Buy: 1,
    /** Sell */
    Sell: 2,
} as const);

type PaymentPaySide = (typeof PaymentPaySide)[keyof typeof PaymentPaySide];
