/**
 * Status of mass order request.
 * - Tag: 2425
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MassOrderRequestStatus = Object.freeze({
    /** Accepted */
    Accepted: 1,
    /** Accepted with additional events */
    AcceptedWithAdditionalEvents: 2,
    /** Rejected */
    Rejected: 3,
} as const);

type MassOrderRequestStatus = (typeof MassOrderRequestStatus)[keyof typeof MassOrderRequestStatus];
