/**
 * State of the trading session.
 * - Tag: 340
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesStatus = Object.freeze({
    /** Unknown */
    Unknown: 0,
    /** Halted */
    Halted: 1,
    /** Open */
    Open: 2,
    /** Closed */
    Closed: 3,
    /** Pre-Open */
    PreOpen: 4,
    /** Pre-Close */
    PreClose: 5,
    /** Request Rejected */
    RequestRejected: 6,
} as const);

type TradSesStatus = (typeof TradSesStatus)[keyof typeof TradSesStatus];
