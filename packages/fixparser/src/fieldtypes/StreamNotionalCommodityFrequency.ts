/**
 * The commodity's notional or quantity delivery frequency.
 * - Tag: 41308
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamNotionalCommodityFrequency = Object.freeze({
    /** Term */
    Term: 0,
    /** Per business day */
    PerBusinessDay: 1,
    /** Per calculation period */
    PerCalculationPeriod: 2,
    /** Per settlement period */
    PerSettlPeriod: 3,
    /** Per calendar day */
    PerCalendarDay: 4,
    /** Per hour */
    PerHour: 5,
    /** Per month */
    PerMonth: 6,
} as const);

type StreamNotionalCommodityFrequency =
    (typeof StreamNotionalCommodityFrequency)[keyof typeof StreamNotionalCommodityFrequency];
