/**
 * Specifies how or the timing when the quote is to be obtained.
 * - Tag: 42748
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRateQuoteTimeType = Object.freeze({
    Open: 0,
    OfficialSettlPx: 1,
    Xetra: 2,
    Close: 3,
    DerivativesClose: 4,
    High: 5,
    Low: 6,
    /** As specified in the master confirmation */
    AsSpecifiedInMasterConfirmation: 7,
} as const);

type ReturnRateQuoteTimeType = (typeof ReturnRateQuoteTimeType)[keyof typeof ReturnRateQuoteTimeType];
