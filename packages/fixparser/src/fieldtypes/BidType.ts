/**
 * Code to identify the type of Bid Request.
 * - Tag: 394
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const BidType = Object.freeze({
    /** "Non Disclosed" style (e.g. US/European) */
    NonDisclosed: 1,
    /** "Disclosed" style (e.g. Japanese) */
    Disclosed: 2,
    /** No bidding process */
    NoBiddingProcess: 3,
} as const);

type BidType = (typeof BidType)[keyof typeof BidType];
