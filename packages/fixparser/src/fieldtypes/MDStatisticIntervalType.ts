/**
 * Type of interval over which statistic is calculated.
 * - Tag: 2464
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDStatisticIntervalType = Object.freeze({
    SlidingWindow: 1,
    SlidingWindowPeak: 2,
    FixedDateRange: 3,
    FixedTimeRange: 4,
    CurrentTimeUnit: 5,
    PreviousTimeUnit: 6,
    MaximumRange: 7,
    MaximumRangeUpToPreviousTimeUnit: 8,
} as const);

type MDStatisticIntervalType = (typeof MDStatisticIntervalType)[keyof typeof MDStatisticIntervalType];
