/**
 * SymbolSfx
 * - Tag: 65
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SymbolSfx = Object.freeze({
    /** EUCP with lump-sum interest rather than discount price */
    EUCPWithLumpSumInterest: 'CD',
    /** "When Issued" for a security to be reissued under an old CUSIP or ISIN */
    WhenIssued: 'WI',
} as const);

type SymbolSfx = (typeof SymbolSfx)[keyof typeof SymbolSfx];
