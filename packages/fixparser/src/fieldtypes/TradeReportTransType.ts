/**
 * TradeReportTransType
 * - Tag: 487
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeReportTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
    /** Replace */
    Replace: 2,
    /** Release */
    Release: 3,
    /** Reverse */
    Reverse: 4,
    /** Cancel Due To Back Out of Trade */
    CancelDueToBackOutOfTrade: 5,
} as const);

type TradeReportTransType = (typeof TradeReportTransType)[keyof typeof TradeReportTransType];
