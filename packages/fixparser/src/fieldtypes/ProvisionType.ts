/**
 * Type of provisions.
 * - Tag: 40091
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionType = Object.freeze({
    /** Mandatory early termination */
    MandatoryEarlyTermination: 0,
    /** Optional early termination */
    OptionalEarlyTermination: 1,
    /** Cancelable */
    Cancelable: 2,
    Extendable: 3,
    /** Mutual early termination */
    MutualEarlyTermination: 4,
    Evergreen: 5,
    Callable: 6,
    Puttable: 7,
} as const);

type ProvisionType = (typeof ProvisionType)[keyof typeof ProvisionType];
