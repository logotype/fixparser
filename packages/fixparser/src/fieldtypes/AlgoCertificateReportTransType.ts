/**
 * Identifies the message transaction type.
 * - Tag: 3020
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AlgoCertificateReportTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
    /** Replace */
    Replace: 2,
} as const);

type AlgoCertificateReportTransType =
    (typeof AlgoCertificateReportTransType)[keyof typeof AlgoCertificateReportTransType];
