/**
 * Specifies the type of date (e.g. adjusted for holidays).
 * - Tag: 40173
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionCashSettlPaymentDateType = Object.freeze({
    /** Unadjusted */
    Unadjusted: 0,
    /** Adjusted */
    Adjusted: 1,
} as const);

type ProvisionCashSettlPaymentDateType =
    (typeof ProvisionCashSettlPaymentDateType)[keyof typeof ProvisionCashSettlPaymentDateType];
