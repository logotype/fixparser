/**
 * Action proposed for an Underlying Instrument instance.
 * - Tag: 944
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollAction = Object.freeze({
    /** Retain */
    Retain: 0,
    /** Add */
    Add: 1,
    /** Remove */
    Remove: 2,
} as const);

type CollAction = (typeof CollAction)[keyof typeof CollAction];
