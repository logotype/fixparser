/**
 * Specifies the type of action requested
 * - Tag: 1373
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MassActionType = Object.freeze({
    /** Suspend orders */
    SuspendOrders: 1,
    /** Release orders from suspension */
    ReleaseOrdersFromSuspension: 2,
    /** Cancel orders */
    CancelOrders: 3,
} as const);

type MassActionType = (typeof MassActionType)[keyof typeof MassActionType];
