/**
 * Request result of mass order request.
 * - Tag: 2426
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MassOrderRequestResult = Object.freeze({
    /** Successful */
    Successful: 0,
    /** Response level not supported */
    ResponseLevelNotSupported: 1,
    /** Invalid market */
    InvalidMarket: 2,
    /** Invalid market segment */
    InvalidMarketSegment: 3,
    /** Other */
    Other: 99,
} as const);

type MassOrderRequestResult = (typeof MassOrderRequestResult)[keyof typeof MassOrderRequestResult];
