/**
 * Indicates whether or not message is being sent as a result of a subscription request or not.
 * - Tag: 325
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const UnsolicitedIndicator = Object.freeze({
    /** Message is being sent as a result of a prior request */
    MessageIsBeingSentAsAResultOfAPriorRequest: 'N',
    /** Message is being sent unsolicited */
    MessageIsBeingSentUnsolicited: 'Y',
} as const);

type UnsolicitedIndicator = (typeof UnsolicitedIndicator)[keyof typeof UnsolicitedIndicator];
