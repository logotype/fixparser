/**
 * Indicates whether an ISDA price option applies, and if applicable which type of price.
 * - Tag: 42759
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRateValuationPriceOption = Object.freeze({
    /** None (the default) */
    None: 0,
    FuturesPrice: 1,
    OptionsPrice: 2,
} as const);

type ReturnRateValuationPriceOption =
    (typeof ReturnRateValuationPriceOption)[keyof typeof ReturnRateValuationPriceOption];
