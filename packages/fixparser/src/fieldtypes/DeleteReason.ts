/**
 * Reason for deletion.
 * - Tag: 285
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DeleteReason = Object.freeze({
    /** Cancellation / Trade Bust */
    Cancellation: '0',
    /** Error */
    Error: '1',
} as const);

type DeleteReason = (typeof DeleteReason)[keyof typeof DeleteReason];
