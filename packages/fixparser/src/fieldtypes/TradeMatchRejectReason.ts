/**
 * Reason the trade match report submission was rejected.
 * - Tag: 1897
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeMatchRejectReason = Object.freeze({
    /** Successful */
    Successful: 0,
    /** Invalid party information */
    InvalidPartyInformation: 1,
    /** Unknown instrument */
    UnknownInstrument: 2,
    /** Not authorized to report trades */
    Unauthorized: 3,
    /** Invalid trade type */
    InvalidTradeType: 4,
    /** Other */
    Other: 99,
} as const);

type TradeMatchRejectReason = (typeof TradeMatchRejectReason)[keyof typeof TradeMatchRejectReason];
