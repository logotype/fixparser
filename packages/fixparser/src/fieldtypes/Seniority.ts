/**
 * Seniority
 * - Tag: 1450
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const Seniority = Object.freeze({
    /** Senior Secured */
    SeniorSecured: 'SD',
    /** Senior */
    Senior: 'SR',
    /** Subordinated */
    Subordinated: 'SB',
    Junior: 'JR',
    Mezzanine: 'MZ',
    SeniorNonPreferred: 'SN',
} as const);

type Seniority = (typeof Seniority)[keyof typeof Seniority];
