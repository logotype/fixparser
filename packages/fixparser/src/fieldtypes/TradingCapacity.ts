/**
 * Designates the capacity in which the order is submitted for trading by the market participant.
 * - Tag: 1815
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradingCapacity = Object.freeze({
    /** Customer */
    Customer: 1,
    /** Customer professional */
    CustomerProfessional: 2,
    /** Broker-dealer */
    BrokerDealer: 3,
    /** Customer broker-dealer */
    CustomerBrokerDealer: 4,
    /** Principal */
    Principal: 5,
    /** Market maker */
    MarketMaker: 6,
    /** Away market maker */
    AwayMarketMaker: 7,
    /** Systematic internaliser */
    SystematicInternaliser: 8,
} as const);

type TradingCapacity = (typeof TradingCapacity)[keyof typeof TradingCapacity];
