/**
 * If provided, then Instrument occurrence has explicitly changed
 * - Tag: 1324
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ListUpdateAction = Object.freeze({
    /** Add */
    Add: 'A',
    /** Delete */
    Delete: 'D',
    /** Modify */
    Modify: 'M',
    /** Snapshot */
    Snapshot: 'S',
} as const);

type ListUpdateAction = (typeof ListUpdateAction)[keyof typeof ListUpdateAction];
