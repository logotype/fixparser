/**
 * Indicates whether follow-up confirmation of exercise (written or electronic) is required following telephonic notice by the buyer to the seller or seller's agent.
 * - Tag: 41111
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExerciseConfirmationMethod = Object.freeze({
    /** Not required */
    NotRequired: 0,
    /** Non-electronic */
    NonElectronic: 1,
    /** Electronic */
    Electronic: 2,
    /** Unknown at time of report */
    Unknown: 3,
} as const);

type ExerciseConfirmationMethod = (typeof ExerciseConfirmationMethod)[keyof typeof ExerciseConfirmationMethod];
