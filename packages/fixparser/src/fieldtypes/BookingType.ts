/**
 * Method for booking out this order. Used when notifying a broker that an order to be settled by that broker is to be booked out as an OTC derivative (e.g. CFD or similar).
 * - Tag: 775
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const BookingType = Object.freeze({
    /** Regular booking */
    RegularBooking: 0,
    /** CFD (Contract for difference) */
    CFD: 1,
    /** Total Return Swap */
    TotalReturnSwap: 2,
} as const);

type BookingType = (typeof BookingType)[keyof typeof BookingType];
