/**
 * CustOrderCapacity
 * - Tag: 582
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CustOrderCapacity = Object.freeze({
    /** Member trading for their own account */
    MemberTradingForTheirOwnAccount: 1,
    /** Clearing firm trading for its proprietary account */
    ClearingFirmTradingForItsProprietaryAccount: 2,
    /** Member trading for another member */
    MemberTradingForAnotherMember: 3,
    /** All other */
    AllOther: 4,
    RetailCustomer: 5,
} as const);

type CustOrderCapacity = (typeof CustOrderCapacity)[keyof typeof CustOrderCapacity];
