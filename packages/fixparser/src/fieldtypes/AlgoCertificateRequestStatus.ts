/**
 * Status of the AlgoCertificateRequest(35=EH) message being responded to.
 * - Tag: 3017
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AlgoCertificateRequestStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    Rejected: 2,
} as const);

type AlgoCertificateRequestStatus = (typeof AlgoCertificateRequestStatus)[keyof typeof AlgoCertificateRequestStatus];
