/**
 * Used to specify the type of position request being made.
 * - Tag: 724
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosReqType = Object.freeze({
    /** Positions */
    Positions: 0,
    /** Trades */
    Trades: 1,
    /** Exercises */
    Exercises: 2,
    /** Assignments */
    Assignments: 3,
    /** Settlement Activity */
    SettlementActivity: 4,
    /** Backout Message */
    BackoutMessage: 5,
    /** Delta Positions */
    DeltaPositions: 6,
    /** Net Position */
    NetPosition: 7,
    /** Large Positions Reporting */
    LargePositionsReporting: 8,
    /** Exercise Position Reporting Submission */
    ExercisePositionReportingSubmission: 9,
    /** Position Limit Reporting Submission */
    PositionLimitReportingSubmissing: 10,
} as const);

type PosReqType = (typeof PosReqType)[keyof typeof PosReqType];
