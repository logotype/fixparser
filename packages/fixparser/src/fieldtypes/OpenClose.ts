/**
 * Indicates whether the resulting position after a trade should be an opening position or closing position. Used for omnibus accounting - where accounts are held on a gross basis instead of being netted together.
 * - Tag: 77
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OpenClose = Object.freeze({
    /** Close */
    Close: 'C',
    /** Open */
    Open: 'O',
} as const);

type OpenClose = (typeof OpenClose)[keyof typeof OpenClose];
