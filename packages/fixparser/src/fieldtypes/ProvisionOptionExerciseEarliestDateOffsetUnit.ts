/**
 * Time unit associated with the interval to the first (and possibly only) exercise date in the exercise period.
 * - Tag: 40126
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ProvisionOptionExerciseEarliestDateOffsetUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type ProvisionOptionExerciseEarliestDateOffsetUnit =
    (typeof ProvisionOptionExerciseEarliestDateOffsetUnit)[keyof typeof ProvisionOptionExerciseEarliestDateOffsetUnit];
