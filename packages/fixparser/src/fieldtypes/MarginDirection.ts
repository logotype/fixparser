/**
 * Indicates whether the margin described is posted or received.
 * - Tag: 2851
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarginDirection = Object.freeze({
    Posted: 0,
    Received: 1,
} as const);

type MarginDirection = (typeof MarginDirection)[keyof typeof MarginDirection];
