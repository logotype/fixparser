/**
 * Specifies the type of Market Data update.
 * - Tag: 265
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDUpdateType = Object.freeze({
    /** Full refresh */
    FullRefresh: 0,
    /** Incremental refresh */
    IncrementalRefresh: 1,
} as const);

type MDUpdateType = (typeof MDUpdateType)[keyof typeof MDUpdateType];
