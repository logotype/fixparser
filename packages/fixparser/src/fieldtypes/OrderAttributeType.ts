/**
 * The type of order attribute.
 * - Tag: 2594
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OrderAttributeType = Object.freeze({
    AggregatedOrder: 0,
    PendingAllocation: 1,
    LiquidityProvisionActivityOrder: 2,
    RiskReductionOrder: 3,
    AlgorithmicOrder: 4,
    SystematicInternaliserOrder: 5,
    AllExecutionsSubmittedToAPA: 6,
    OrderExecutionInstructedByClient: 7,
    LargeInScale: 8,
    Hidden: 9,
    SubjectToEUSTO: 10,
    SubjectToUKSTO: 11,
    RepresentativeOrder: 12,
    LinkageType: 13,
    ExemptFromSTO: 14,
} as const);

type OrderAttributeType = (typeof OrderAttributeType)[keyof typeof OrderAttributeType];
