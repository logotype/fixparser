/**
 * Result of Request for Positions.
 * - Tag: 728
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosReqResult = Object.freeze({
    /** Valid request */
    ValidRequest: 0,
    /** Invalid or unsupported request */
    InvalidOrUnsupportedRequest: 1,
    /** No positions found that match criteria */
    NoPositionsFoundThatMatchCriteria: 2,
    /** Not authorized to request positions */
    NotAuthorizedToRequestPositions: 3,
    /** Request for position not supported */
    RequestForPositionNotSupported: 4,
    Other: 99,
} as const);

type PosReqResult = (typeof PosReqResult)[keyof typeof PosReqResult];
