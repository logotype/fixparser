/**
 * Used to indicate how the multi-legged security (e.g. option strategies, spreads, etc.) is being reported.
 * - Tag: 442
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MultiLegReportingType = Object.freeze({
    /** Single security (default if not specified) */
    SingleSecurity: '1',
    /** Individual leg of a multi-leg security */
    IndividualLegOfAMultiLegSecurity: '2',
    /** Multi-leg security */
    MultiLegSecurity: '3',
} as const);

type MultiLegReportingType = (typeof MultiLegReportingType)[keyof typeof MultiLegReportingType];
