/**
 * Specifies the kind of priority given to customers.
 * - Tag: 2570
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CustomerPriority = Object.freeze({
    NoPriority: 0,
    UnconditionalPriority: 1,
} as const);

type CustomerPriority = (typeof CustomerPriority)[keyof typeof CustomerPriority];
