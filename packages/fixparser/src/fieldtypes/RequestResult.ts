/**
 * Result of a request as identified by the appropriate request ID field
 * - Tag: 1511
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RequestResult = Object.freeze({
    /** Valid request */
    ValidRequest: 0,
    /** Invalid or unsupported request */
    InvalidOrUnsupportedRequest: 1,
    /** No data found that match selection criteria */
    NoDataFound: 2,
    /** Not authorized to retrieve data */
    NotAuthorized: 3,
    /** Data temporarily unavailable */
    DataTemporarilyUnavailable: 4,
    /** Request for data not supported */
    RequestForDataNotSupported: 5,
    /** Other (further information in RejectText (1328) field) */
    Other: 99,
} as const);

type RequestResult = (typeof RequestResult)[keyof typeof RequestResult];
