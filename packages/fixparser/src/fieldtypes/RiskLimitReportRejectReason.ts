/**
 * The reason for rejecting the PartyRiskLimitsReport(35=CM) or PartyRiskLimitsUpdateReport(35=CR).
 * - Tag: 2317
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitReportRejectReason = Object.freeze({
    /** Unknown RiskLimitReportID(1667) */
    UnkRiskLmtRprtID: 0,
    /** Unknown party */
    UnkPty: 1,
    /** Other */
    Other: 99,
} as const);

type RiskLimitReportRejectReason = (typeof RiskLimitReportRejectReason)[keyof typeof RiskLimitReportRejectReason];
