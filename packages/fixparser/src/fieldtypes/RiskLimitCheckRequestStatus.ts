/**
 * Indicates the status of the risk limit check request.
 * - Tag: 2325
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckRequestStatus = Object.freeze({
    Approved: 0,
    PartiallyApproved: 1,
    /** Rejected */
    Rejected: 2,
    /** Approval pending */
    ApprovalPending: 3,
    /** Cancelled */
    Cancelled: 4,
} as const);

type RiskLimitCheckRequestStatus = (typeof RiskLimitCheckRequestStatus)[keyof typeof RiskLimitCheckRequestStatus];
