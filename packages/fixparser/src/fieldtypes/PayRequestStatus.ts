/**
 * Identifies status of the request being responded to.
 * - Tag: 2813
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PayRequestStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    /** Rejected */
    Rejected: 2,
    Disputed: 3,
} as const);

type PayRequestStatus = (typeof PayRequestStatus)[keyof typeof PayRequestStatus];
