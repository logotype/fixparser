/**
 * Type of MarginRequirementReport.
 * - Tag: 1638
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarginReqmtRptType = Object.freeze({
    /** Summary */
    Summary: 0,
    /** Detail */
    Detail: 1,
    /** Excess/Deficit */
    ExcessDeficit: 2,
} as const);

type MarginReqmtRptType = (typeof MarginReqmtRptType)[keyof typeof MarginReqmtRptType];
