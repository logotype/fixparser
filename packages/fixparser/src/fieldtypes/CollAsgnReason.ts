/**
 * Reason for Collateral Assignment
 * - Tag: 895
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollAsgnReason = Object.freeze({
    /** Initial */
    Initial: 0,
    /** Scheduled */
    Scheduled: 1,
    /** Time Warning */
    TimeWarning: 2,
    MarginDeficiency: 3,
    MarginExcess: 4,
    /** Forward Collateral Demand */
    ForwardCollateralDemand: 5,
    /** Event of default */
    EventOfDefault: 6,
    /** Adverse tax event */
    AdverseTaxEvent: 7,
    TransferDeposit: 8,
    TransferWithdrawal: 9,
    Pledge: 10,
} as const);

type CollAsgnReason = (typeof CollAsgnReason)[keyof typeof CollAsgnReason];
