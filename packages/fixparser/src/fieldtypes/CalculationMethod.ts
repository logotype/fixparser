/**
 * Specifies how the calculation will be made.
 * - Tag: 2592
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CalculationMethod = Object.freeze({
    Automatic: 0,
    Manual: 1,
} as const);

type CalculationMethod = (typeof CalculationMethod)[keyof typeof CalculationMethod];
