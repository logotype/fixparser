/**
 * Specifies the type of multileg order. Defines whether the security is pre-defined or user-defined. Note that MultilegModel(1377)=2(User-defined, Non-Securitized, Multileg) does not apply for Securities.
 * - Tag: 1377
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MultilegModel = Object.freeze({
    /** Predefined Multileg Security */
    PredefinedMultilegSecurity: 0,
    /** User-defined Multileg Security */
    UserDefinedMultilegSecurity: 1,
    /** User-defined, Non-Securitized, Multileg */
    UserDefined: 2,
} as const);

type MultilegModel = (typeof MultilegModel)[keyof typeof MultilegModel];
