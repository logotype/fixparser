/**
 * Indicates possible retransmission of message with this sequence number
 * - Tag: 43
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PossDupFlag = Object.freeze({
    /** Original transmission */
    OriginalTransmission: 'N',
    /** Possible duplicate */
    PossibleDuplicate: 'Y',
} as const);

type PossDupFlag = (typeof PossDupFlag)[keyof typeof PossDupFlag];
