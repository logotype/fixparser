/**
 * A one character code identifying whether the Fund based renewal commission is to be waived.
 * - Tag: 497
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const FundRenewWaiv = Object.freeze({
    /** No */
    No: 'N',
    /** Yes */
    Yes: 'Y',
} as const);

type FundRenewWaiv = (typeof FundRenewWaiv)[keyof typeof FundRenewWaiv];
