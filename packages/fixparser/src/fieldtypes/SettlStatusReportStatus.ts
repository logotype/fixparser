/**
 * Status of the report being responded to.
 * - Tag: 2973
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlStatusReportStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    Rejected: 2,
} as const);

type SettlStatusReportStatus = (typeof SettlStatusReportStatus)[keyof typeof SettlStatusReportStatus];
