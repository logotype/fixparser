/**
 * Settlement method for a contract or instrument. Additional values may be used with bilateral agreement.
 * - Tag: 1193
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlMethod = Object.freeze({
    /** Cash settlement required */
    CashSettlementRequired: 'C',
    /** Physical settlement required */
    PhysicalSettlementRequired: 'P',
    Election: 'E',
} as const);

type SettlMethod = (typeof SettlMethod)[keyof typeof SettlMethod];
