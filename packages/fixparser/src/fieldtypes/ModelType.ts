/**
 * Type of pricing model used
 * - Tag: 1434
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ModelType = Object.freeze({
    /** Utility provided standard model */
    UtilityProvidedStandardModel: 0,
    /** Proprietary (user supplied) model */
    ProprietaryModel: 1,
} as const);

type ModelType = (typeof ModelType)[keyof typeof ModelType];
