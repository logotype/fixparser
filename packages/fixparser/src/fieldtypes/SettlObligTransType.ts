/**
 * Transaction Type - required except where SettlInstMode is 5=Reject SSI request
 * - Tag: 1162
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlObligTransType = Object.freeze({
    /** Cancel */
    Cancel: 'C',
    /** New */
    New: 'N',
    /** Replace */
    Replace: 'R',
    /** Restate */
    Restate: 'T',
} as const);

type SettlObligTransType = (typeof SettlObligTransType)[keyof typeof SettlObligTransType];
