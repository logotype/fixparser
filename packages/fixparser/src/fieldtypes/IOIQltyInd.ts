/**
 * Relative quality of indication
 * - Tag: 25
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IOIQltyInd = Object.freeze({
    /** High */
    High: 'H',
    /** Low */
    Low: 'L',
    /** Medium */
    Medium: 'M',
} as const);

type IOIQltyInd = (typeof IOIQltyInd)[keyof typeof IOIQltyInd];
