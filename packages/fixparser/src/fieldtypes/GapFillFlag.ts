/**
 * Indicates that the Sequence Reset message is replacing administrative or application messages which will not be resent.
 * - Tag: 123
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const GapFillFlag = Object.freeze({
    /** Sequence Reset, Ignore Msg Seq Num (N/A For FIXML - Not Used) */
    SequenceReset: 'N',
    /** Gap Fill Message, Msg Seq Num Field Valid */
    GapFillMessage: 'Y',
} as const);

type GapFillFlag = (typeof GapFillFlag)[keyof typeof GapFillFlag];
