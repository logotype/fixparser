/**
 * Indicates that the order or trade originates from a computer program or algorithm requiring little-to-no human intervention.
 * - Tag: 2667
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AlgorithmicTradeIndicator = Object.freeze({
    /** Non-algorithmic trade */
    NonAlgorithmicTrade: 0,
    AlgorithmicTrade: 1,
} as const);

type AlgorithmicTradeIndicator = (typeof AlgorithmicTradeIndicator)[keyof typeof AlgorithmicTradeIndicator];
