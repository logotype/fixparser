/**
 * Used to identify whether these delivery instructions are for the buyside or the sellside.
 * - Tag: 1164
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlObligSource = Object.freeze({
    /** Instructions of Broker */
    InstructionsOfBroker: '1',
    /** Instructions for Institution */
    InstructionsForInstitution: '2',
    /** Investor */
    Investor: '3',
    /** Buyer's settlement instructions */
    BuyersSettlementInstructions: '4',
    /** Seller's settlement instructions */
    SellersSettlementInstructions: '5',
} as const);

type SettlObligSource = (typeof SettlObligSource)[keyof typeof SettlObligSource];
