/**
 * Indicates if a trade should be or has been published via a market publication service. The indicator governs all publication services of the recipient. Replaces PublishTrdIndicator(852).
 * - Tag: 1390
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradePublishIndicator = Object.freeze({
    /** Do Not Publish Trade */
    DoNotPublishTrade: 0,
    /** Publish Trade */
    PublishTrade: 1,
    /** Deferred Publication */
    DeferredPublication: 2,
    Published: 3,
} as const);

type TradePublishIndicator = (typeof TradePublishIndicator)[keyof typeof TradePublishIndicator];
