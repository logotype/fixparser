/**
 * Result of Trade Request
 * - Tag: 749
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeRequestResult = Object.freeze({
    /** Successful (default) */
    Successful: 0,
    /** Invalid or unknown instrument */
    InvalidOrUnknownInstrument: 1,
    /** Invalid type of trade requested */
    InvalidTypeOfTradeRequested: 2,
    /** Invalid parties */
    InvalidParties: 3,
    /** Invalid transport type requested */
    InvalidTransportTypeRequested: 4,
    /** Invalid destination requested */
    InvalidDestinationRequested: 5,
    /** TradeRequestType not supported */
    TradeRequestTypeNotSupported: 8,
    /** Not authorized */
    NotAuthorized: 9,
    /** Other */
    Other: 99,
} as const);

type TradeRequestResult = (typeof TradeRequestResult)[keyof typeof TradeRequestResult];
