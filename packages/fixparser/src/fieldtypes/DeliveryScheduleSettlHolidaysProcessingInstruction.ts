/**
 * Indicates whether holidays are included in the settlement periods. Required for electricity contracts.
 * - Tag: 41050
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleSettlHolidaysProcessingInstruction = Object.freeze({
    /** Do not include holidays */
    DoNotIncludeHolidays: 0,
    /** Include holidays */
    IncludeHolidays: 1,
} as const);

type DeliveryScheduleSettlHolidaysProcessingInstruction =
    (typeof DeliveryScheduleSettlHolidaysProcessingInstruction)[keyof typeof DeliveryScheduleSettlHolidaysProcessingInstruction];
