/**
 * Identifies the type of quote cancel.
 * - Tag: 298
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteCancelType = Object.freeze({
    /** Cancel quotes for one or more securities */
    CancelForOneOrMoreSecurities: 1,
    /** Cancel quotes for security type(s) */
    CancelForSecurityType: 2,
    /** Cancel quotes for underlying security */
    CancelForUnderlyingSecurity: 3,
    /** Cancel all quotes */
    CancelAllQuotes: 4,
    CancelSpecifiedSingleQuote: 5,
    CancelByTypeOfQuote: 6,
    /** Cancel quotes for an issuer */
    CancelForSecurityIssuer: 7,
    /** Cancel quotes for an issuer of underlying security */
    CancelForIssuerOfUnderlyingSecurity: 8,
} as const);

type QuoteCancelType = (typeof QuoteCancelType)[keyof typeof QuoteCancelType];
