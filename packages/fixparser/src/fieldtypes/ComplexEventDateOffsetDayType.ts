/**
 * Specifies the day type of the relative date offset.
 * - Tag: 41024
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventDateOffsetDayType = Object.freeze({
    /** Business */
    Business: 0,
    /** Calendar */
    Calendar: 1,
    /** Commodity business */
    CommodityBusiness: 2,
    /** Currency business */
    CurrencyBusiness: 3,
    /** Exchange business */
    ExchangeBusiness: 4,
    /** Scheduled trading day */
    ScheduledTradingDay: 5,
} as const);

type ComplexEventDateOffsetDayType = (typeof ComplexEventDateOffsetDayType)[keyof typeof ComplexEventDateOffsetDayType];
