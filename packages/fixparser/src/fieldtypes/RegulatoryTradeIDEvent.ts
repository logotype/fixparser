/**
 * Identifies the event which caused origination of the identifier in RegulatoryTradeID(1903). When more than one event is the cause, use the higher enumeration value. For example, if the identifier is originated due to an allocated trade which was cleared and reported, use the enumeration value 2 (Clearing).
 * - Tag: 1904
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RegulatoryTradeIDEvent = Object.freeze({
    InitialBlockTrade: 0,
    Allocation: 1,
    /** Clearing */
    Clearing: 2,
    /** Compression */
    Compression: 3,
    /** Novation */
    Novation: 4,
    /** Termination */
    Termination: 5,
    /** Post-trade valuation */
    PostTrdVal: 6,
} as const);

type RegulatoryTradeIDEvent = (typeof RegulatoryTradeIDEvent)[keyof typeof RegulatoryTradeIDEvent];
