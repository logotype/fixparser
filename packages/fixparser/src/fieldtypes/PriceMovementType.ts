/**
 * Describes the format of the PriceMovementValue(1921).
 * - Tag: 1923
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PriceMovementType = Object.freeze({
    /** Amount */
    Amount: 0,
    /** Percentage */
    Percentage: 1,
} as const);

type PriceMovementType = (typeof PriceMovementType)[keyof typeof PriceMovementType];
