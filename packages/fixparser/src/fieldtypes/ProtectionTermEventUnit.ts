/**
 * Time unit associated with protection term events.
 * - Tag: 40196
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ProtectionTermEventUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type ProtectionTermEventUnit = (typeof ProtectionTermEventUnit)[keyof typeof ProtectionTermEventUnit];
