/**
 * TimeUnit
 * - Tag: 997
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TimeUnit = Object.freeze({
    /** Hour */
    Hour: 'H',
    /** Minute */
    Minute: 'Min',
    /** Second */
    Second: 'S',
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
    /** Quarter */
    Quarter: 'Q',
    EndOfMonth: 'EOM',
    Flexible: 'F',
} as const);

type TimeUnit = (typeof TimeUnit)[keyof typeof TimeUnit];
