/**
 * Source of NBBO information.
 * - Tag: 2834
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NBBOSource = Object.freeze({
    NotApplicable: 0,
    Direct: 1,
    SIP: 2,
    Hybrid: 3,
} as const);

type NBBOSource = (typeof NBBOSource)[keyof typeof NBBOSource];
