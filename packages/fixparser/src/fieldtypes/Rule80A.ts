/**
 * Note that the name of this field is changing to "OrderCapacity" as Rule80A is a very US market-specific term. Other world markets need to convey similar information, however, often a subset of the US values. . See the "Rule80A (aka OrderCapacity) Usage by Market" appendix for market-specific usage of this field.
 * - Tag: 47
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const Rule80A = Object.freeze({
    /** Agency single order */
    AgencySingleOrder: 'A',
    /** Short exempt transaction (refer to A type) */
    ShortExemptTransactionAType: 'B',
    /** Program Order, non-index arb, for Member firm/org */
    ProprietaryNonAlgo: 'C',
    /** Program Order, index arb, for Member firm/org */
    ProgramOrderMember: 'D',
    /** Registered Equity Market Maker trades */
    ShortExemptTransactionForPrincipal: 'E',
    /** Short exempt transaction (refer to W type) */
    ShortExemptTransactionWType: 'F',
    /** Short exempt transaction (refer to I type) */
    ShortExemptTransactionIType: 'H',
    /** Individual Investor, single order */
    IndividualInvestor: 'I',
    /** Program Order, index arb, for individual customer */
    ProprietaryAlgo: 'J',
    /** Program Order, non-index arb, for individual customer */
    AgencyAlgo: 'K',
    /** Short exempt transaction for member competing market-maker affiliated with the firm clearing the trade (refer to P and O types) */
    ShortExemptTransactionMemberAffliated: 'L',
    /** Program Order, index arb, for other member */
    ProgramOrderOtherMember: 'M',
    /** Program Order, non-index arb, for other member */
    AgentForOtherMember: 'N',
    /** Competing dealer trades */
    ProprietaryTransactionAffiliated: 'O',
    /** Principal */
    Principal: 'P',
    /** Competing dealer trades */
    TransactionNonMember: 'R',
    /** Specialist trades */
    SpecialistTrades: 'S',
    /** Competing dealer trades */
    TransactionUnaffiliatedMember: 'T',
    /** Program Order, index arb, for other agency */
    AgencyIndexArb: 'U',
    /** All other orders as agent for other member */
    AllOtherOrdersAsAgentForOtherMember: 'W',
    /** Short exempt transaction for member competing market-maker not affiliated with the firm clearing the trade (refer to W and T types) */
    ShortExemptTransactionMemberNotAffliated: 'X',
    /** Program Order, non-index arb, for other agency */
    AgencyNonAlgo: 'Y',
    /** Short exempt transaction for non-member competing market-maker (refer to A and R types) */
    ShortExemptTransactionNonMember: 'Z',
} as const);

type Rule80A = (typeof Rule80A)[keyof typeof Rule80A];
