/**
 * Status of the TestActionRequest(35=EN) message being responded to.
 * - Tag: 3068
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestActionRequestStatus = Object.freeze({
    /** Received, not yet processed */
    Received: 0,
    /** Accepted */
    Accepted: 1,
    Rejected: 2,
} as const);

type TestActionRequestStatus = (typeof TestActionRequestStatus)[keyof typeof TestActionRequestStatus];
