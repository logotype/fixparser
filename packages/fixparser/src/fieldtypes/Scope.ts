/**
 * Specifies the market scope of the market data.
 * - Tag: 546
 * - FIX Specification type: MultipleCharValue
 * - Mapped type: string
 * @readonly
 * @public
 */
export const Scope = Object.freeze({
    /** Local Market (Exchange, ECN, ATS) */
    LocalMarket: '1',
    /** National */
    National: '2',
    /** Global */
    Global: '3',
} as const);

type Scope = (typeof Scope)[keyof typeof Scope];
