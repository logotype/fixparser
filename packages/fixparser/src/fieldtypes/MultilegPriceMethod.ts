/**
 * MultilegPriceMethod
 * - Tag: 1378
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MultilegPriceMethod = Object.freeze({
    /** Net Price */
    NetPrice: 0,
    /** Reversed Net Price */
    ReversedNetPrice: 1,
    /** Yield Difference */
    YieldDifference: 2,
    /** Individual */
    Individual: 3,
    /** Contract Weighted Average Price */
    ContractWeightedAveragePrice: 4,
    /** Multiplied Price */
    MultipliedPrice: 5,
} as const);

type MultilegPriceMethod = (typeof MultilegPriceMethod)[keyof typeof MultilegPriceMethod];
