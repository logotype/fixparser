/**
 * Indicates whether a message was queued as a result of throttling.
 * - Tag: 1609
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ThrottleStatus = Object.freeze({
    /** Throttle limit not exceeded, not queued */
    ThrottleLimitNotExceededNotQueued: 0,
    /** Queued due to throttle limit exceeded */
    QueuedDueToThrottleLimitExceeded: 1,
} as const);

type ThrottleStatus = (typeof ThrottleStatus)[keyof typeof ThrottleStatus];
