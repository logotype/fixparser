/**
 * Status of Position Maintenance Request
 * - Tag: 722
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosMaintStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Accepted With Warnings */
    AcceptedWithWarnings: 1,
    /** Rejected */
    Rejected: 2,
    /** Completed */
    Completed: 3,
    /** Completed With Warnings */
    CompletedWithWarnings: 4,
} as const);

type PosMaintStatus = (typeof PosMaintStatus)[keyof typeof PosMaintStatus];
