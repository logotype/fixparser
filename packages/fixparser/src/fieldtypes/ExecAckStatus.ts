/**
 * The status of this execution acknowledgement message.
 * - Tag: 1036
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ExecAckStatus = Object.freeze({
    /** Received, not yet processed */
    Received: '0',
    /** Accepted */
    Accepted: '1',
    /** Don't know / Rejected */
    DontKnow: '2',
} as const);

type ExecAckStatus = (typeof ExecAckStatus)[keyof typeof ExecAckStatus];
