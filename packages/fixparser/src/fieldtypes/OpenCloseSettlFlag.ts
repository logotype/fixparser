/**
 * Flag that identifies a market data entry. (Prior to FIX 4.3 this field was of type char)
 * - Tag: 286
 * - FIX Specification type: MultipleCharValue
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OpenCloseSettlFlag = Object.freeze({
    /** Daily Open / Close / Settlement entry */
    DailyOpen: '0',
    /** Session Open / Close / Settlement entry */
    SessionOpen: '1',
    /** Delivery Settlement entry */
    DeliverySettlementEntry: '2',
    /** Expected entry */
    ExpectedEntry: '3',
    /** Entry from previous business day */
    EntryFromPreviousBusinessDay: '4',
    /** Theoretical Price value */
    TheoreticalPriceValue: '5',
} as const);

type OpenCloseSettlFlag = (typeof OpenCloseSettlFlag)[keyof typeof OpenCloseSettlFlag];
