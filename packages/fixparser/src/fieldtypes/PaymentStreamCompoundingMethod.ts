/**
 * Compounding method.
 * - Tag: 40747
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamCompoundingMethod = Object.freeze({
    /** None */
    None: 0,
    /** Flat */
    Flat: 1,
    /** Straight */
    Straight: 2,
    /** Spread exclusive */
    SpreadExclusive: 3,
} as const);

type PaymentStreamCompoundingMethod =
    (typeof PaymentStreamCompoundingMethod)[keyof typeof PaymentStreamCompoundingMethod];
