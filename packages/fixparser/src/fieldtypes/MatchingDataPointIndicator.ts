/**
 * Data point's matching type.
 * - Tag: 2782
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MatchingDataPointIndicator = Object.freeze({
    /** Mandatory */
    Mandatory: 1,
    /** Optional */
    Optional: 2,
} as const);

type MatchingDataPointIndicator = (typeof MatchingDataPointIndicator)[keyof typeof MatchingDataPointIndicator];
