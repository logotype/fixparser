/**
 * QuoteType
 * - Tag: 537
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteType = Object.freeze({
    /** Indicative */
    Indicative: 0,
    /** Tradeable */
    Tradeable: 1,
    /** Restricted tradeable */
    RestrictedTradeable: 2,
    /** Counter (tradeable) */
    Counter: 3,
    /** Initially tradeable */
    InitiallyTradeable: 4,
} as const);

type QuoteType = (typeof QuoteType)[keyof typeof QuoteType];
