/**
 * The classification or type of swap. Additional values may be used by mutual agreement of the counterparties.
 * - Tag: 1941
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SwapClass = Object.freeze({
    /** Basis swap */
    BasisSwap: 'BS',
    /** Index swap */
    IndexSwap: 'IX',
    /** Broad-based security swap */
    BroadBasedSecuritySwap: 'BB',
    /** Basket swap */
    BasketSwap: 'SK',
} as const);

type SwapClass = (typeof SwapClass)[keyof typeof SwapClass];
