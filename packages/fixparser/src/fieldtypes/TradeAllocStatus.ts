/**
 * TradeAllocStatus
 * - Tag: 1840
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeAllocStatus = Object.freeze({
    /** Pending clear */
    PendingClear: 0,
    /** Claimed */
    Claimed: 1,
    /** Cleared */
    Cleared: 2,
    /** Rejected */
    Rejected: 3,
} as const);

type TradeAllocStatus = (typeof TradeAllocStatus)[keyof typeof TradeAllocStatus];
