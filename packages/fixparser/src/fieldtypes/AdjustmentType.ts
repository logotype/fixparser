/**
 * Type of adjustment to be applied. Used for Position Change Submission (PCS), Position Adjustment (PAJ), and Customer Gross Margin (CGM).
 * - Tag: 718
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AdjustmentType = Object.freeze({
    /** Process request as margin disposition */
    ProcessRequestAsMarginDisposition: 0,
    /** Delta plus */
    DeltaPlus: 1,
    /** Delta minus */
    DeltaMinus: 2,
    /** Final */
    Final: 3,
    /** Customer specific position */
    CustomerSpecificPosition: 4,
} as const);

type AdjustmentType = (typeof AdjustmentType)[keyof typeof AdjustmentType];
