/**
 * Specifies the conditions that govern the adjustment to the number of units of the return swap.
 * - Tag: 2617
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const UnderlyingNotionalAdjustments = Object.freeze({
    Execution: 0,
    PortfolioRebalancing: 1,
    Standard: 2,
} as const);

type UnderlyingNotionalAdjustments = (typeof UnderlyingNotionalAdjustments)[keyof typeof UnderlyingNotionalAdjustments];
