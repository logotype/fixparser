/**
 * Indicates whether a given timestamp was manually captured.
 * - Tag: 2839
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TrdRegTimestampManualIndicator = Object.freeze({
    /** Not manually captured */
    NotManuallyCaptured: 'N',
    /** Manually captured */
    ManuallyCaptured: 'Y',
} as const);

type TrdRegTimestampManualIndicator =
    (typeof TrdRegTimestampManualIndicator)[keyof typeof TrdRegTimestampManualIndicator];
