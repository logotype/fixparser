/**
 * Indicates request for forex accommodation trade to be executed along with security transaction.
 * - Tag: 121
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ForexReq = Object.freeze({
    /** Do Not Execute Forex After Security Trade */
    DoNotExecuteForexAfterSecurityTrade: 'N',
    /** Execute Forex After Security Trade */
    ExecuteForexAfterSecurityTrade: 'Y',
} as const);

type ForexReq = (typeof ForexReq)[keyof typeof ForexReq];
