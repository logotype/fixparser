/**
 * Indicates the type of investment the cash collateral is re-invested in.
 * - Tag: 2844
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollateralReinvestmentType = Object.freeze({
    MoneyMarketFund: 0,
    OtherComingledPool: 1,
    RepoMarket: 2,
    DirectPurchaseOfSecurities: 3,
    OtherInvestments: 4,
} as const);

type CollateralReinvestmentType = (typeof CollateralReinvestmentType)[keyof typeof CollateralReinvestmentType];
