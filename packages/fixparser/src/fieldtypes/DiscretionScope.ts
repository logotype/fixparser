/**
 * The scope of the discretion
 * - Tag: 846
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DiscretionScope = Object.freeze({
    /** Local (Exchange, ECN, ATS) */
    Local: 1,
    /** National */
    National: 2,
    /** Global */
    Global: 3,
    /** National excluding local */
    NationalExcludingLocal: 4,
} as const);

type DiscretionScope = (typeof DiscretionScope)[keyof typeof DiscretionScope];
