/**
 * Identifies the model used for asset valuation or pricing calculations.
 * - Tag: 2994
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AssetValuationModel = Object.freeze({
    /** Black-Scholes */
    BlackScholes: 1,
    /** Whaley */
    Whaley: 2,
    /** Bachelier */
    Bachelier: 3,
    /** Kirk */
    Kirk: 4,
    /** Curran */
    Curran: 5,
    /** Black-76 */
    Black76: 6,
    /** Binomial */
    Binomial: 7,
    /** Other model */
    OtherModel: 99,
} as const);

type AssetValuationModel = (typeof AssetValuationModel)[keyof typeof AssetValuationModel];
