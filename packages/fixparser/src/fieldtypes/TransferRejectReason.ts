/**
 * Reason the transfer instruction was rejected.
 * - Tag: 2443
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferRejectReason = Object.freeze({
    /** Success */
    Success: 0,
    /** Invalid party */
    InvalidParty: 1,
    /** Unknown instrument */
    UnknownInstrument: 2,
    /** Not authorized to submit transfers */
    UnauthorizedToSubmitXfer: 3,
    /** Unknown position */
    UnknownPosition: 4,
    /** Other */
    Other: 99,
} as const);

type TransferRejectReason = (typeof TransferRejectReason)[keyof typeof TransferRejectReason];
