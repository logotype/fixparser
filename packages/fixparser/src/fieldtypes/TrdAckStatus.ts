/**
 * Used to indicate the status of the trade submission (not the trade report)
 * - Tag: 1523
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TrdAckStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Rejected */
    Rejected: 1,
    /** Received */
    Received: 2,
} as const);

type TrdAckStatus = (typeof TrdAckStatus)[keyof typeof TrdAckStatus];
