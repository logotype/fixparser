/**
 * Indicates whether the order or quote was crossed with another order or quote having the same context, e.g. having accounts with a common ownership.
 * - Tag: 2523
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CrossedIndicator = Object.freeze({
    NoCross: 0,
    CrossRejected: 1,
    CrossAccepted: 2,
} as const);

type CrossedIndicator = (typeof CrossedIndicator)[keyof typeof CrossedIndicator];
