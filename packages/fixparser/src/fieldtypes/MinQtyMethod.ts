/**
 * Indicates how the minimum quantity should be applied when executing the order.
 * - Tag: 1822
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MinQtyMethod = Object.freeze({
    /** Once (applies only to first execution) */
    Once: 1,
    /** Multiple (applies to every execution) */
    Multiple: 2,
} as const);

type MinQtyMethod = (typeof MinQtyMethod)[keyof typeof MinQtyMethod];
