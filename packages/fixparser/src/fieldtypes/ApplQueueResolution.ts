/**
 * Resolution taken when ApplQueueDepth (813) exceeds ApplQueueMax (812) or system specified maximum queue size.
 * - Tag: 814
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ApplQueueResolution = Object.freeze({
    /** No Action Taken */
    NoActionTaken: 0,
    /** Queue Flushed */
    QueueFlushed: 1,
    /** Overlay Last */
    OverlayLast: 2,
    /** End Session */
    EndSession: 3,
} as const);

type ApplQueueResolution = (typeof ApplQueueResolution)[keyof typeof ApplQueueResolution];
