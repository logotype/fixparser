/**
 * Identifies the type of Quote Response.
 * - Tag: 694
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteRespType = Object.freeze({
    /** Hit/Lift */
    Hit: 1,
    /** Counter */
    Counter: 2,
    /** Expired */
    Expired: 3,
    Cover: 4,
    DoneAway: 5,
    /** Pass */
    Pass: 6,
    EndTrade: 7,
    /** Timed out */
    TimedOut: 8,
    Tied: 9,
    TiedCover: 10,
    Accept: 11,
    TerminateContract: 12,
} as const);

type QuoteRespType = (typeof QuoteRespType)[keyof typeof QuoteRespType];
