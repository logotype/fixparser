/**
 * Specifies the valuation type applicable to the return rate date.
 * - Tag: 42710
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRateDateMode = Object.freeze({
    /** Price valuation */
    PriceValuation: 0,
    /** Dividend valuation */
    DividendValuation: 1,
} as const);

type ReturnRateDateMode = (typeof ReturnRateDateMode)[keyof typeof ReturnRateDateMode];
