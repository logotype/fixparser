/**
 * Specifies the location of the fallback provision documentation.
 * - Tag: 41088
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketDisruptionFallbackProvision = Object.freeze({
    /** As specified in master agreement */
    MasterAgreement: 0,
    /** As specified in confirmation */
    Confirmation: 1,
} as const);

type MarketDisruptionFallbackProvision =
    (typeof MarketDisruptionFallbackProvision)[keyof typeof MarketDisruptionFallbackProvision];
