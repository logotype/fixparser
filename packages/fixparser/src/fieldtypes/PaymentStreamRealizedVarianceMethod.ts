/**
 * Indicates which price to use to satisfy the boundary condition.
 * - Tag: 42679
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamRealizedVarianceMethod = Object.freeze({
    Previous: 0,
    Last: 1,
    Both: 2,
} as const);

type PaymentStreamRealizedVarianceMethod =
    (typeof PaymentStreamRealizedVarianceMethod)[keyof typeof PaymentStreamRealizedVarianceMethod];
