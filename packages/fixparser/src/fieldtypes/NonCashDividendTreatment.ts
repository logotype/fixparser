/**
 * Defines the treatment of non-cash dividends.
 * - Tag: 42258
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NonCashDividendTreatment = Object.freeze({
    PotentialAdjustment: 0,
    CashEquivalent: 1,
} as const);

type NonCashDividendTreatment = (typeof NonCashDividendTreatment)[keyof typeof NonCashDividendTreatment];
