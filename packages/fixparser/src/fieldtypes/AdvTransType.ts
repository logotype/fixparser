/**
 * Identifies advertisement message transaction type
 * - Tag: 5
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AdvTransType = Object.freeze({
    /** New */
    New: 'N',
    /** Cancel */
    Cancel: 'C',
    /** Replace */
    Replace: 'R',
} as const);

type AdvTransType = (typeof AdvTransType)[keyof typeof AdvTransType];
