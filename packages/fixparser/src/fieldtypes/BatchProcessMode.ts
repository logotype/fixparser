/**
 * Indicates the processing mode for a batch of messages.
 * - Tag: 50002
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const BatchProcessMode = Object.freeze({
    /** Update/incremental (default if not specified) */
    Update: 0,
    Snapshot: 1,
} as const);

type BatchProcessMode = (typeof BatchProcessMode)[keyof typeof BatchProcessMode];
