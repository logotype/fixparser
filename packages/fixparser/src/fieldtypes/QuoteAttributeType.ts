/**
 * The type of attribute for the quote.
 * - Tag: 2707
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteAttributeType = Object.freeze({
    QuoteAboveStandardMarketSize: 0,
    QuoteAboveSpecificInstrumentSize: 1,
    QuoteApplicableForLiquidtyProvisionActivity: 2,
    QuoteIssuerStatus: 3,
    BidOrAskRequest: 4,
} as const);

type QuoteAttributeType = (typeof QuoteAttributeType)[keyof typeof QuoteAttributeType];
