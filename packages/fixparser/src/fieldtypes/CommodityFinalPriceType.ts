/**
 * Final price type of the commodity as specified by the trading venue.
 * - Tag: 2736
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CommodityFinalPriceType = Object.freeze({
    /** Argus McCloskey */
    ArgusMcCloskey: 0,
    /** Baltic */
    Baltic: 1,
    /** Exchange */
    Exchange: 2,
    /** Global Coal */
    GlobalCoal: 3,
    /** IHS McCloskey */
    IHSMcCloskey: 4,
    /** Platts */
    Platts: 5,
    /** Other */
    Other: 99,
} as const);

type CommodityFinalPriceType = (typeof CommodityFinalPriceType)[keyof typeof CommodityFinalPriceType];
