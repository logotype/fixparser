/**
 * Indicates source of Settlement Instructions
 * - Tag: 165
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlInstSource = Object.freeze({
    /** Broker's Instructions */
    BrokerCredit: '1',
    /** Institution's Instructions */
    Institution: '2',
    /** Investor (e.g. CIV use) */
    Investor: '3',
} as const);

type SettlInstSource = (typeof SettlInstSource)[keyof typeof SettlInstSource];
