/**
 * Code to represent whether value is net (inclusive of tax) or gross.
 * - Tag: 416
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const IncTaxInd = Object.freeze({
    /** Net */
    Net: 1,
    /** Gross */
    Gross: 2,
} as const);

type IncTaxInd = (typeof IncTaxInd)[keyof typeof IncTaxInd];
