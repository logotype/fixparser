/**
 * The average pricing model used for block trades.
 * - Tag: 2763
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AveragePriceType = Object.freeze({
    TimeWeightedAveragePrice: 0,
    VolumeWeightedAveragePrice: 1,
    PercentOfVolumeAveragePrice: 2,
    LimitOrderAveragePrice: 3,
} as const);

type AveragePriceType = (typeof AveragePriceType)[keyof typeof AveragePriceType];
