/**
 * Type of Peg Offset value
 * - Tag: 836
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PegOffsetType = Object.freeze({
    /** Price (default) */
    Price: 0,
    /** Basis Points */
    BasisPoints: 1,
    /** Ticks */
    Ticks: 2,
    /** Price Tier / Level */
    PriceTier: 3,
    /** Percentage */
    Percentage: 4,
} as const);

type PegOffsetType = (typeof PegOffsetType)[keyof typeof PegOffsetType];
