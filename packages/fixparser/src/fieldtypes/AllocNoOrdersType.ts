/**
 * Indicates how the orders being booked and allocated by an AllocationInstruction or AllocationReport message are identified, e.g. by explicit definition in the OrdAllocGrp or ExecAllocGrp components, or not identified explicitly.
 * - Tag: 857
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocNoOrdersType = Object.freeze({
    /** Not specified */
    NotSpecified: 0,
    /** Explicit list provided */
    ExplicitListProvided: 1,
} as const);

type AllocNoOrdersType = (typeof AllocNoOrdersType)[keyof typeof AllocNoOrdersType];
