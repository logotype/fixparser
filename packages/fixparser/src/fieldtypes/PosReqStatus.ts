/**
 * Status of Request for Positions
 * - Tag: 729
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosReqStatus = Object.freeze({
    /** Completed */
    Completed: 0,
    /** Completed With Warnings */
    CompletedWithWarnings: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type PosReqStatus = (typeof PosReqStatus)[keyof typeof PosReqStatus];
