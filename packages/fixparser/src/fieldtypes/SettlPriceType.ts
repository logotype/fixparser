/**
 * Type of settlement price
 * - Tag: 731
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlPriceType = Object.freeze({
    /** Final */
    Final: 1,
    /** Theoretical */
    Theoretical: 2,
} as const);

type SettlPriceType = (typeof SettlPriceType)[keyof typeof SettlPriceType];
