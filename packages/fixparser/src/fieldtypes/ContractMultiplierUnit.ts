/**
 * Indicates the type of multiplier being applied to the contract. Can be optionally used to further define what unit ContractMultiplier(tag 231) is expressed in.
 * - Tag: 1435
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ContractMultiplierUnit = Object.freeze({
    /** Shares */
    Shares: 0,
    /** Hours */
    Hours: 1,
    /** Days */
    Days: 2,
} as const);

type ContractMultiplierUnit = (typeof ContractMultiplierUnit)[keyof typeof ContractMultiplierUnit];
