/**
 * Code to represent whether value is net (inclusive of tax) or gross.
 * - Tag: 430
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NetGrossInd = Object.freeze({
    /** Net */
    Net: 1,
    /** Gross */
    Gross: 2,
} as const);

type NetGrossInd = (typeof NetGrossInd)[keyof typeof NetGrossInd];
