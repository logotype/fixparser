/**
 * Collateral inquiry qualifiers:
 * - Tag: 896
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollInquiryQualifier = Object.freeze({
    /** Trade Date */
    TradeDate: 0,
    /** GC Instrument */
    GCInstrument: 1,
    /** Collateral Instrument */
    CollateralInstrument: 2,
    /** Substitution Eligible */
    SubstitutionEligible: 3,
    /** Not Assigned */
    NotAssigned: 4,
    /** Partially Assigned */
    PartiallyAssigned: 5,
    /** Fully Assigned */
    FullyAssigned: 6,
    /** Outstanding Trades (Today \< end date) */
    OutstandingTrades: 7,
} as const);

type CollInquiryQualifier = (typeof CollInquiryQualifier)[keyof typeof CollInquiryQualifier];
