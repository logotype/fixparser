/**
 * Indicates the status of a network connection
 * - Tag: 928
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StatusValue = Object.freeze({
    /** Connected */
    Connected: 1,
    /** Not Connected - down expected up */
    NotConnectedUnexpected: 2,
    /** Not Connected - down expected down */
    NotConnectedExpected: 3,
    /** In Process */
    InProcess: 4,
} as const);

type StatusValue = (typeof StatusValue)[keyof typeof StatusValue];
