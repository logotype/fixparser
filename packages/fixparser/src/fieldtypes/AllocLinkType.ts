/**
 * Identifies the type of Allocation linkage when AllocLinkID(196) is used.
 * - Tag: 197
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocLinkType = Object.freeze({
    /** FX Netting */
    FXNetting: 0,
    /** FX Swap */
    FXSwap: 1,
} as const);

type AllocLinkType = (typeof AllocLinkType)[keyof typeof AllocLinkType];
