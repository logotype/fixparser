/**
 * Reject reason code for rejecting the collateral report.
 * - Tag: 2487
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollRptRejectReason = Object.freeze({
    /** Unknown trade or transaction */
    UnknownTrade: 0,
    /** Unknown or invalid instrument */
    UnknownInstrument: 1,
    /** Unknown or invalid counterparty */
    UnknownCounterparty: 2,
    /** Unknown or invalid position */
    UnknownPosition: 3,
    /** Unacceptable or invalid type of collateral */
    UnacceptableCollateral: 4,
    /** Other */
    Other: 99,
} as const);

type CollRptRejectReason = (typeof CollRptRejectReason)[keyof typeof CollRptRejectReason];
