/**
 * conveys how the collateral should be/has been applied
 * - Tag: 1043
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollApplType = Object.freeze({
    /** Specific Deposit */
    SpecificDeposit: 0,
    /** General */
    General: 1,
} as const);

type CollApplType = (typeof CollApplType)[keyof typeof CollApplType];
