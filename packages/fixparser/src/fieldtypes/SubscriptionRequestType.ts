/**
 * Subscription Request Type
 * - Tag: 263
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SubscriptionRequestType = Object.freeze({
    /** Snapshot */
    Snapshot: '0',
    /** Snapshot + Updates (Subscribe) */
    SnapshotAndUpdates: '1',
    /** Disable previous Snapshot + Update Request (Unsubscribe) */
    DisablePreviousSnapshot: '2',
} as const);

type SubscriptionRequestType = (typeof SubscriptionRequestType)[keyof typeof SubscriptionRequestType];
