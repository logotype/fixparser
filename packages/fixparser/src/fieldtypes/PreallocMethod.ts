/**
 * Indicates the method of preallocation.
 * - Tag: 591
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PreallocMethod = Object.freeze({
    /** Pro rata */
    ProRata: '0',
    /** Do not pro-rata - discuss first */
    DoNotProRata: '1',
} as const);

type PreallocMethod = (typeof PreallocMethod)[keyof typeof PreallocMethod];
