/**
 * Payment settlement style.
 * - Tag: 40227
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentSettlStyle = Object.freeze({
    /** Standard */
    Standard: 0,
    /** Net */
    Net: 1,
    /** Standard and net */
    StandardfNet: 2,
} as const);

type PaymentSettlStyle = (typeof PaymentSettlStyle)[keyof typeof PaymentSettlStyle];
