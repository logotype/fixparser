/**
 * Identifies whether the current entry contributes to the trade or transaction economics, i.e. affects NetMoney(118).
 * - Tag: 2712
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MiscFeeQualifier = Object.freeze({
    Contributes: 0,
    DoesNotContribute: 1,
} as const);

type MiscFeeQualifier = (typeof MiscFeeQualifier)[keyof typeof MiscFeeQualifier];
