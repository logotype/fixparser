/**
 * Specifies the type of delivery stream.
 * - Tag: 41058
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamType = Object.freeze({
    /** Periodic (default if not specified) */
    Periodic: 0,
    /** Initial */
    Initial: 1,
    /** Single */
    Single: 2,
} as const);

type DeliveryStreamType = (typeof DeliveryStreamType)[keyof typeof DeliveryStreamType];
