/**
 * Indicates whether a restriction applies to short selling a security.
 * - Tag: 1687
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ShortSaleRestriction = Object.freeze({
    /** No restrictions */
    NoRestrictions: 0,
    /** Security is not shortable */
    SecurityNotShortable: 1,
    /** Security not shortable at or below the best bid */
    SecurityNotShortableAtOrBelowBestBid: 2,
    /** Security is not shortable without pre-borrow */
    SecurityNotShortableWithoutPreBorrow: 3,
} as const);

type ShortSaleRestriction = (typeof ShortSaleRestriction)[keyof typeof ShortSaleRestriction];
