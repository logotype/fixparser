/**
 * Status of risk limit report.
 * - Tag: 2316
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitReportStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Rejected */
    Rejected: 1,
} as const);

type RiskLimitReportStatus = (typeof RiskLimitReportStatus)[keyof typeof RiskLimitReportStatus];
