/**
 * Indicates what type of commission is being expressed in CommissionAmount(2640).
 * - Tag: 2641
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CommissionAmountType = Object.freeze({
    /** Unspecified */
    Unspecified: 0,
    Acceptance: 1,
    Broker: 2,
    ClearingBroker: 3,
    Retail: 4,
    SalesCommission: 5,
    LocalCommission: 6,
    /** Research payment */
    ResearchPayment: 7,
} as const);

type CommissionAmountType = (typeof CommissionAmountType)[keyof typeof CommissionAmountType];
