/**
 * Indicates whether or not the halt was due to the Related Security being halted.
 * - Tag: 329
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DueToRelated = Object.freeze({
    /** Halt was not related to a halt of the related security */
    NotRelatedToSecurityHalt: 'N',
    /** Halt was due to related security being halted */
    RelatedToSecurityHalt: 'Y',
} as const);

type DueToRelated = (typeof DueToRelated)[keyof typeof DueToRelated];
