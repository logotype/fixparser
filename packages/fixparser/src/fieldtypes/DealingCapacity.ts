/**
 * Identifies role of dealer; Agent, Principal, RisklessPrincipal
 * - Tag: 1048
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DealingCapacity = Object.freeze({
    /** Agent */
    Agent: 'A',
    /** Principal */
    Principal: 'P',
    /** Riskless Principal */
    RisklessPrincipal: 'R',
} as const);

type DealingCapacity = (typeof DealingCapacity)[keyof typeof DealingCapacity];
