/**
 * Instruction to define conditions under which to release a locked order or parts of it.
 * - Tag: 1810
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReleaseInstruction = Object.freeze({
    /** Intermarket Sweep Order (ISO) */
    ISO: 1,
    /** No Away Market Better check */
    NoAwayMarketBetterCheck: 2,
} as const);

type ReleaseInstruction = (typeof ReleaseInstruction)[keyof typeof ReleaseInstruction];
