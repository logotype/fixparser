/**
 * For foreign exchange Quanto option feature.
 * - Tag: 2126
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventQuoteBasis = Object.freeze({
    /** Currency 1 per currency 2 */
    Currency1PerCurrency2: 0,
    /** Currency 2 per currency 1 */
    Currency2PerCurrency1: 1,
} as const);

type ComplexEventQuoteBasis = (typeof ComplexEventQuoteBasis)[keyof typeof ComplexEventQuoteBasis];
