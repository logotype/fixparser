/**
 * Specifies an option instrument's "in the money" condition.
 * - Tag: 2681
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const InTheMoneyCondition = Object.freeze({
    StandardITM: 0,
    ATMITM: 1,
    ATMCallITM: 2,
    ATMPutITM: 3,
} as const);

type InTheMoneyCondition = (typeof InTheMoneyCondition)[keyof typeof InTheMoneyCondition];
