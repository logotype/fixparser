/**
 * The method of Forward Rate Agreement (FRA) discounting, if any, that will apply.
 * - Tag: 40816
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamFRADiscounting = Object.freeze({
    /** None */
    None: 0,
    /** International Swaps and Derivatives Association (ISDA) */
    ISDA: 1,
    /** Australian Financial Markets Association (AFMA) */
    AFMA: 2,
} as const);

type PaymentStreamFRADiscounting = (typeof PaymentStreamFRADiscounting)[keyof typeof PaymentStreamFRADiscounting];
