/**
 * RestructuringType
 * - Tag: 1449
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const RestructuringType = Object.freeze({
    /** Full Restructuring */
    FullRestructuring: 'FR',
    /** Modified Restructuring */
    ModifiedRestructuring: 'MR',
    /** Modified Mod Restructuring */
    ModifiedModRestructuring: 'MM',
    /** No Restructuring specified */
    NoRestructuringSpecified: 'XR',
} as const);

type RestructuringType = (typeof RestructuringType)[keyof typeof RestructuringType];
