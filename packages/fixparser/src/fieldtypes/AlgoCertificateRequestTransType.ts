/**
 * Identifies the message transaction type.
 * - Tag: 3016
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AlgoCertificateRequestTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
} as const);

type AlgoCertificateRequestTransType =
    (typeof AlgoCertificateRequestTransType)[keyof typeof AlgoCertificateRequestTransType];
