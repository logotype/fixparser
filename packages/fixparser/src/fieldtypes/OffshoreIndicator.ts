/**
 * Indicates the type of the currency rate being used. This is relevant for currencies that have offshore rate that different from onshore rate.
 * - Tag: 2795
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OffshoreIndicator = Object.freeze({
    Regular: 0,
    Offshore: 1,
    Onshore: 2,
} as const);

type OffshoreIndicator = (typeof OffshoreIndicator)[keyof typeof OffshoreIndicator];
