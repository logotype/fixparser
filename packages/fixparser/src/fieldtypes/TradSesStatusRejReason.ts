/**
 * Indicates the reason a Trading Session Status Request was rejected.
 * - Tag: 567
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesStatusRejReason = Object.freeze({
    /** Unknown or invalid TradingSessionID */
    UnknownOrInvalidTradingSessionID: 1,
    /** Other */
    Other: 99,
} as const);

type TradSesStatusRejReason = (typeof TradSesStatusRejReason)[keyof typeof TradSesStatusRejReason];
