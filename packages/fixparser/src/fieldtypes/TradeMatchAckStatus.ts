/**
 * Used to indicate the status of the trade match report submission.
 * - Tag: 1896
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeMatchAckStatus = Object.freeze({
    /** Received, not yet processed */
    ReceivedNotProcessed: 0,
    /** Accepted */
    Accepted: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type TradeMatchAckStatus = (typeof TradeMatchAckStatus)[keyof typeof TradeMatchAckStatus];
