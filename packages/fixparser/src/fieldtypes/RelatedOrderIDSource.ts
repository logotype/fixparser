/**
 * Describes the source of the identifier that RelatedOrderID(2887) represents.
 * - Tag: 2888
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelatedOrderIDSource = Object.freeze({
    /** Non-FIX Source */
    NonFIXSource: 0,
    SystemOrderIdentifier: 1,
    ClientOrderIdentifier: 2,
    SecondaryOrderIdentifier: 3,
    SecondaryClientOrderIdentifier: 4,
} as const);

type RelatedOrderIDSource = (typeof RelatedOrderIDSource)[keyof typeof RelatedOrderIDSource];
