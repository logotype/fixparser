/**
 * Specifies the transaction type of the risk limit check request.
 * - Tag: 2320
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
    /** Replace */
    Replace: 2,
} as const);

type RiskLimitCheckTransType = (typeof RiskLimitCheckTransType)[keyof typeof RiskLimitCheckTransType];
