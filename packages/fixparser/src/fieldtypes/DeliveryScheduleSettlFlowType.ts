/**
 * Specifies the commodity delivery flow type.
 * - Tag: 41049
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleSettlFlowType = Object.freeze({
    /** All times */
    AllTimes: 0,
    /** On peak */
    OnPeak: 1,
    /** Off peak */
    OffPeak: 2,
    /** Base */
    Base: 3,
    /** Block hours */
    BlockHours: 4,
    /** Other */
    Other: 5,
} as const);

type DeliveryScheduleSettlFlowType = (typeof DeliveryScheduleSettlFlowType)[keyof typeof DeliveryScheduleSettlFlowType];
