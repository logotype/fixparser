/**
 * Indicates the method of execution reporting requested by issuer of the order.
 * - Tag: 563
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MultiLegRptTypeReq = Object.freeze({
    /** Report by mulitleg security only (do not report legs) */
    ReportByMulitlegSecurityOnly: 0,
    /** Report by multileg security and by instrument legs belonging to the multileg security */
    ReportByMultilegSecurityAndInstrumentLegs: 1,
    /** Report by instrument legs belonging to the multileg security only (do not report status of multileg security) */
    ReportByInstrumentLegsOnly: 2,
} as const);

type MultiLegRptTypeReq = (typeof MultiLegRptTypeReq)[keyof typeof MultiLegRptTypeReq];
