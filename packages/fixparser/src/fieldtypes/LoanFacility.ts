/**
 * Specifies the type of loan when the credit default swap's reference obligation is a loan.
 * - Tag: 1955
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LoanFacility = Object.freeze({
    /** Bridge loan */
    BridgeLoan: 0,
    /** Letter of credit */
    LetterOfCredit: 1,
    /** Revolving loan */
    RevolvingLoan: 2,
    /** Swingline funding */
    SwinglineFunding: 3,
    /** Term loan */
    TermLoan: 4,
    /** Trade claim */
    TradeClaim: 5,
} as const);

type LoanFacility = (typeof LoanFacility)[keyof typeof LoanFacility];
