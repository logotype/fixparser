/**
 * Type of entitlement.
 * - Tag: 1775
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const EntitlementType = Object.freeze({
    /** Trade */
    Trade: 0,
    /** Make markets */
    MakeMarkets: 1,
    /** Hold positions */
    HoldPositions: 2,
    /** Perform give-ups */
    PerformGiveUps: 3,
    /** Submit Indications of Interest (IOIs) */
    SubmitIOIs: 4,
    /** Subscribe to market data */
    SubscribeMarketData: 5,
    ShortWithPreBorrow: 6,
    SubmitQuoteRequests: 7,
    RespondToQuoteRequests: 8,
} as const);

type EntitlementType = (typeof EntitlementType)[keyof typeof EntitlementType];
