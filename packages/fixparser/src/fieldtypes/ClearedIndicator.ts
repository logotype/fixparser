/**
 * Indicates whether the trade or position being reported was cleared through a clearing organization.
 * - Tag: 1832
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ClearedIndicator = Object.freeze({
    NotCleared: 0,
    Cleared: 1,
    Submitted: 2,
    Rejected: 3,
} as const);

type ClearedIndicator = (typeof ClearedIndicator)[keyof typeof ClearedIndicator];
