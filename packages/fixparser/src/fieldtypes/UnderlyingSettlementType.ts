/**
 * Indicates order settlement period for the underlying instrument.
 * - Tag: 975
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const UnderlyingSettlementType = Object.freeze({
    /** T+1 */
    TPlus1: 2,
    /** T+3 */
    TPlus3: 4,
    /** T+4 */
    TPlus4: 5,
} as const);

type UnderlyingSettlementType = (typeof UnderlyingSettlementType)[keyof typeof UnderlyingSettlementType];
