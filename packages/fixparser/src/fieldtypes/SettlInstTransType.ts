/**
 * Settlement Instructions message transaction type
 * - Tag: 163
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SettlInstTransType = Object.freeze({
    /** New */
    New: 'N',
    /** Cancel */
    Cancel: 'C',
    /** Replace */
    Replace: 'R',
    /** Restate */
    Restate: 'T',
} as const);

type SettlInstTransType = (typeof SettlInstTransType)[keyof typeof SettlInstTransType];
