/**
 * Indicates how control of trading session and subsession transitions are performed.
 * - Tag: 1785
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesControl = Object.freeze({
    /** Automatic (Default) */
    Automatic: 0,
    /** Manual */
    Manual: 1,
} as const);

type TradSesControl = (typeof TradSesControl)[keyof typeof TradSesControl];
