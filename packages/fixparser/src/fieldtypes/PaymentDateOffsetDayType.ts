/**
 * Specifies the day type of the relative payment date offset.
 * - Tag: 41159
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentDateOffsetDayType = Object.freeze({
    /** Business */
    Business: 0,
    /** Calendar */
    Calendar: 1,
    /** Commodity business */
    Commodity: 2,
    /** Currency business */
    Currency: 3,
    /** Exchange business */
    Exchange: 4,
    /** Scheduled trading day */
    Scheduled: 5,
} as const);

type PaymentDateOffsetDayType = (typeof PaymentDateOffsetDayType)[keyof typeof PaymentDateOffsetDayType];
