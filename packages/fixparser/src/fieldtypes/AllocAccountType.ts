/**
 * Type of account associated with a confirmation or other trade-level message
 * - Tag: 798
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocAccountType = Object.freeze({
    /** Account is carried pn customer side of books */
    CarriedCustomerSide: 1,
    /** Account is carried on non-customer side of books */
    CarriedNonCustomerSide: 2,
    /** House trader */
    HouseTrader: 3,
    /** Floor trader */
    FloorTrader: 4,
    /** Account is carried on non-customer side of books and is cross margined */
    CarriedNonCustomerSideCrossMargined: 6,
    /** Account is house trader and is cross margined */
    HouseTraderCrossMargined: 7,
    /** Joint back office account (JBO) */
    JointBackOfficeAccount: 8,
} as const);

type AllocAccountType = (typeof AllocAccountType)[keyof typeof AllocAccountType];
