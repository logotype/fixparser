/**
 * Defines how the composition of dividends is to be determined.
 * - Tag: 42259
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DividendComposition = Object.freeze({
    EquityAmountReceiver: 0,
    CalculationAgent: 1,
} as const);

type DividendComposition = (typeof DividendComposition)[keyof typeof DividendComposition];
