/**
 * Format used to generate the MaturityMonthYear for each option
 * - Tag: 1303
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MaturityMonthYearFormat = Object.freeze({
    /** YearMonth Only (default) */
    YearMonthOnly: 0,
    /** YearMonthDay */
    YearMonthDay: 1,
    /** YearMonthWeek */
    YearMonthWeek: 2,
} as const);

type MaturityMonthYearFormat = (typeof MaturityMonthYearFormat)[keyof typeof MaturityMonthYearFormat];
