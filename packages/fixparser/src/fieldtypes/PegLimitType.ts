/**
 * Type of Peg Limit
 * - Tag: 837
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PegLimitType = Object.freeze({
    /** Or better (default) - price improvement allowed */
    OrBetter: 0,
    /** Strict - limit is a strict limit */
    Strict: 1,
    /** Or worse - for a buy the peg limit is a minimum and for a sell the peg limit is a maximum (for use for orders which have a price range) */
    OrWorse: 2,
} as const);

type PegLimitType = (typeof PegLimitType)[keyof typeof PegLimitType];
