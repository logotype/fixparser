/**
 * Reference to the buyer of the cap rate option through its trade side.
 * - Tag: 40798
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamCapRateBuySide = Object.freeze({
    /** Buyer of the trade */
    Buyer: 1,
    /** Seller of the trade */
    Seller: 2,
} as const);

type PaymentStreamCapRateBuySide = (typeof PaymentStreamCapRateBuySide)[keyof typeof PaymentStreamCapRateBuySide];
