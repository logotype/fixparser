/**
 * The scope of the peg
 * - Tag: 840
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PegScope = Object.freeze({
    /** Local (Exchange, ECN, ATS) */
    Local: 1,
    /** National */
    National: 2,
    /** Global */
    Global: 3,
    /** National excluding local */
    NationalExcludingLocal: 4,
} as const);

type PegScope = (typeof PegScope)[keyof typeof PegScope];
