/**
 * OddLot
 * - Tag: 575
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OddLot = Object.freeze({
    /** Treat as round lot (default) */
    TreatAsRoundLot: 'N',
    /** Treat as odd lot */
    TreatAsOddLot: 'Y',
} as const);

type OddLot = (typeof OddLot)[keyof typeof OddLot];
