/**
 * Scope details of the statistics to reduce the number of events being used as basis for the statistics.
 * - Tag: 2459
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDStatisticScopeType = Object.freeze({
    /** Entry rate */
    EntryRate: 1,
    /** Modification rate */
    ModificationRate: 2,
    /** Cancel rate */
    CancelRate: 3,
    /** Downward move */
    DownwardMove: 4,
    /** Upward move */
    UpwardMove: 5,
} as const);

type MDStatisticScopeType = (typeof MDStatisticScopeType)[keyof typeof MDStatisticScopeType];
