/**
 * Subtype of an entitlement specified in EntitlementType(1775).
 * - Tag: 2402
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const EntitlementSubType = Object.freeze({
    OrderEntry: 1,
    HItLift: 2,
    ViewIndicativePx: 3,
    ViewExecutablePx: 4,
    SingleQuote: 5,
    StreamingQuotes: 6,
    SingleBroker: 7,
    MultiBrokers: 8,
} as const);

type EntitlementSubType = (typeof EntitlementSubType)[keyof typeof EntitlementSubType];
