/**
 * Driver and part of trade in the event that the Security Master file was wrong at the point of entry(Note tag # was reserved in FIX 4.1, added in FIX 4.3)
 * - Tag: 258
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TradedFlatSwitch = Object.freeze({
    /** Not Traded Flat */
    NotTradedFlat: 'N',
    /** Traded Flat */
    TradedFlat: 'Y',
} as const);

type TradedFlatSwitch = (typeof TradedFlatSwitch)[keyof typeof TradedFlatSwitch];
