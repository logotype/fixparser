/**
 * Specifies how weather index units are to be calculated.
 * - Tag: 41199
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamSettlLevel = Object.freeze({
    Average: 0,
    Maximum: 1,
    Minimum: 2,
    Cumulative: 3,
} as const);

type PaymentStreamSettlLevel = (typeof PaymentStreamSettlLevel)[keyof typeof PaymentStreamSettlLevel];
