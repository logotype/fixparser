/**
 * Specifies the action taken as a result of the PartyActionType(2239) of the PartyActionRequest(35=DH) message.
 * - Tag: 2332
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyActionResponse = Object.freeze({
    Accepted: 0,
    Completed: 1,
    Rejected: 2,
} as const);

type PartyActionResponse = (typeof PartyActionResponse)[keyof typeof PartyActionResponse];
