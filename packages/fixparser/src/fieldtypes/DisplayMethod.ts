/**
 * Defines what value to use in DisplayQty (1138). If not specified the default DisplayMethod is "1"
 * - Tag: 1084
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DisplayMethod = Object.freeze({
    /** Initial (use original DisplayQty) */
    Initial: '1',
    /** New (use RefreshQty) */
    New: '2',
    /** Random (randomize value) */
    Random: '3',
    /** Undisclosed (invisible order) */
    Undisclosed: '4',
} as const);

type DisplayMethod = (typeof DisplayMethod)[keyof typeof DisplayMethod];
