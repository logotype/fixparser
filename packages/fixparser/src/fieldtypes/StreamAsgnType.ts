/**
 * The type of assignment being affected in the Stream Assignment Report.
 * - Tag: 1617
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamAsgnType = Object.freeze({
    /** Assignment */
    Assignment: 1,
    /** Rejected */
    Rejected: 2,
    /** Terminate/Unassign */
    Terminate: 3,
} as const);

type StreamAsgnType = (typeof StreamAsgnType)[keyof typeof StreamAsgnType];
