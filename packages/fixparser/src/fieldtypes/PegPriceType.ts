/**
 * Defines the type of peg.
 * - Tag: 1094
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PegPriceType = Object.freeze({
    /** Last peg (last sale) */
    LastPeg: 1,
    /** Mid-price peg (midprice of inside quote) */
    MidPricePeg: 2,
    /** Opening peg */
    OpeningPeg: 3,
    /** Market peg */
    MarketPeg: 4,
    /** Primary peg (primary market - buy at bid or sell at offer) */
    PrimaryPeg: 5,
    /** Peg to VWAP */
    PegToVWAP: 7,
    /** Trailing Stop Peg */
    TrailingStopPeg: 8,
    /** Peg to Limit Price */
    PegToLimitPrice: 9,
    ShortSaleMinPricePeg: 10,
} as const);

type PegPriceType = (typeof PegPriceType)[keyof typeof PegPriceType];
