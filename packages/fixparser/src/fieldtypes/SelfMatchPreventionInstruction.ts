/**
 * Indicate the instruction for self-match prevention when the incoming (aggressive) order has the same SelfMatchPreventionID(2362) as a resting (passive) order.
 * - Tag: 2964
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SelfMatchPreventionInstruction = Object.freeze({
    CancelAggressive: 1,
    CancelPassive: 2,
    CancelAggressivePassive: 3,
} as const);

type SelfMatchPreventionInstruction =
    (typeof SelfMatchPreventionInstruction)[keyof typeof SelfMatchPreventionInstruction];
