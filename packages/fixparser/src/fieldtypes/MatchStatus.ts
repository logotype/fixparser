/**
 * The status of this trade with respect to matching or comparison.
 * - Tag: 573
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MatchStatus = Object.freeze({
    /** Compared, matched or affirmed */
    Compared: '0',
    /** Uncompared, unmatched, or unaffirmed */
    Uncompared: '1',
    /** Advisory or alert */
    AdvisoryOrAlert: '2',
    Mismatched: '3',
} as const);

type MatchStatus = (typeof MatchStatus)[keyof typeof MatchStatus];
