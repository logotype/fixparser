/**
 * Quote model type
 * - Tag: 2403
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteModelType = Object.freeze({
    QuoteEntry: 1,
    QuoteModification: 2,
} as const);

type QuoteModelType = (typeof QuoteModelType)[keyof typeof QuoteModelType];
