/**
 * Specifies whether the PaymentScheduleStepRate(40847) or PaymentScheduleStepOffsetValue(40846) should be applied to the initial notional or the previous notional in order to calculate the notional step change amount.
 * - Tag: 40849
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentScheduleStepRelativeTo = Object.freeze({
    /** Initial */
    Initial: 0,
    /** Previous */
    Previous: 1,
} as const);

type PaymentScheduleStepRelativeTo = (typeof PaymentScheduleStepRelativeTo)[keyof typeof PaymentScheduleStepRelativeTo];
