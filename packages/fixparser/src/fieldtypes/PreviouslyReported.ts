/**
 * Indicates if the transaction was previously reported to the counterparty or market.
 * - Tag: 570
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PreviouslyReported = Object.freeze({
    NotReportedToCounterparty: 'N',
    PreviouslyReportedToCounterparty: 'Y',
} as const);

type PreviouslyReported = (typeof PreviouslyReported)[keyof typeof PreviouslyReported];
