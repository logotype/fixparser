/**
 * Specifies the type of date (e.g. adjusted for holidays).
 * - Tag: 40144
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionOptionExerciseFixedDateType = Object.freeze({
    /** Unadjusted */
    Unadjusted: 0,
    /** Adjusted */
    Adjusted: 1,
} as const);

type ProvisionOptionExerciseFixedDateType =
    (typeof ProvisionOptionExerciseFixedDateType)[keyof typeof ProvisionOptionExerciseFixedDateType];
