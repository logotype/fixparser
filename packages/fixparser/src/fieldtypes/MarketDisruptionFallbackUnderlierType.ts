/**
 * The type of reference price underlier.
 * - Tag: 41097
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketDisruptionFallbackUnderlierType = Object.freeze({
    /** Basket */
    Basket: 0,
    /** Bond */
    Bond: 1,
    /** Cash */
    Cash: 2,
    /** Commodity */
    Commodity: 3,
    /** Convertible bond */
    ConvertibleBond: 4,
    /** Equity */
    Equity: 5,
    /** Exchange traded fund */
    ExchangeTradedFund: 6,
    /** Future */
    Future: 7,
    /** Index */
    Index: 8,
    /** Loan */
    Loan: 9,
    /** Mortgage */
    Mortgage: 10,
    /** Mutual fund */
    MutualFund: 11,
} as const);

type MarketDisruptionFallbackUnderlierType =
    (typeof MarketDisruptionFallbackUnderlierType)[keyof typeof MarketDisruptionFallbackUnderlierType];
