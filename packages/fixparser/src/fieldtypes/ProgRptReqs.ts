/**
 * Code to identify the desired frequency of progress reports.
 * - Tag: 414
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProgRptReqs = Object.freeze({
    /** Buy-side explicitly requests status using Statue Request (default), the sell-side firm can, however, send a DONE status List STatus Response in an unsolicited fashion */
    BuySideRequests: 1,
    /** Sell-side periodically sends status using List Status. Period optionally specified in ProgressPeriod. */
    SellSideSends: 2,
    /** Real-time execution reports (to be discourage) */
    RealTimeExecutionReports: 3,
} as const);

type ProgRptReqs = (typeof ProgRptReqs)[keyof typeof ProgRptReqs];
