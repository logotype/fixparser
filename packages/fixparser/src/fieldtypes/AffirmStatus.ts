/**
 * Specifies the affirmation status of the confirmation.
 * - Tag: 940
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AffirmStatus = Object.freeze({
    /** Received */
    Received: 1,
    /** Confirm rejected, i.e. not affirmed */
    ConfirmRejected: 2,
    /** Affirmed */
    Affirmed: 3,
} as const);

type AffirmStatus = (typeof AffirmStatus)[keyof typeof AffirmStatus];
