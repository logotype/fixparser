/**
 * Specifies the type of price sequence of the return rate.
 * - Tag: 42736
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRatePriceSequence = Object.freeze({
    /** Initial */
    Initial: 0,
    /** Interim */
    Interim: 1,
    /** Final */
    Final: 2,
} as const);

type ReturnRatePriceSequence = (typeof ReturnRatePriceSequence)[keyof typeof ReturnRatePriceSequence];
