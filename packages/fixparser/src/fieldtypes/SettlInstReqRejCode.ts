/**
 * Identifies reason for rejection (of a settlement instruction request message).
 * - Tag: 792
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlInstReqRejCode = Object.freeze({
    /** Unable to process request */
    UnableToProcessRequest: 0,
    /** Unknown account */
    UnknownAccount: 1,
    /** No matching settlement instructions found */
    NoMatchingSettlementInstructionsFound: 2,
    /** Other */
    Other: 99,
} as const);

type SettlInstReqRejCode = (typeof SettlInstReqRejCode)[keyof typeof SettlInstReqRejCode];
