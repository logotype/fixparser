/**
 * Defines the contract event which the receiver of the derivative is entitled to the dividend.
 * - Tag: 42246
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DividendEntitlementEvent = Object.freeze({
    ExDate: 0,
    RecordDate: 1,
} as const);

type DividendEntitlementEvent = (typeof DividendEntitlementEvent)[keyof typeof DividendEntitlementEvent];
