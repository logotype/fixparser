/**
 * The reason for updating the RefOrdID
 * - Tag: 1431
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RefOrdIDReason = Object.freeze({
    /** GTC from previous day */
    GTCFromPreviousDay: 0,
    /** Partial Fill Remaining */
    PartialFillRemaining: 1,
    /** Order Changed */
    OrderChanged: 2,
} as const);

type RefOrdIDReason = (typeof RefOrdIDReason)[keyof typeof RefOrdIDReason];
