/**
 * For equity swaps this specifies the conditions that govern the adjustment to the number of units of the swap.
 * - Tag: 42787
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamNotionalAdjustments = Object.freeze({
    Execution: 0,
    PortfolioRebalancing: 1,
    Standard: 2,
} as const);

type StreamNotionalAdjustments = (typeof StreamNotionalAdjustments)[keyof typeof StreamNotionalAdjustments];
