/**
 * Identifies the class or source of DeliveryStreamDeliveryPoint(41062).
 * - Tag: 42192
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamDeliveryPointSource = Object.freeze({
    /** Proprietary */
    Proprietary: 0,
    EIC: 1,
} as const);

type DeliveryStreamDeliveryPointSource =
    (typeof DeliveryStreamDeliveryPointSource)[keyof typeof DeliveryStreamDeliveryPointSource];
