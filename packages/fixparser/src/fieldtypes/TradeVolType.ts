/**
 * Define the type of trade volume applicable for the MinTradeVol(562) and MaxTradeVol(1140)
 * - Tag: 1786
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeVolType = Object.freeze({
    /** Number of units (e.g. share, par, currency, contracts) (default) */
    NumberOfUnits: 0,
    /** Number of round lots */
    NumberOfRoundLots: 1,
} as const);

type TradeVolType = (typeof TradeVolType)[keyof typeof TradeVolType];
