/**
 * Identifies IOI message transaction type
 * - Tag: 28
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IOITransType = Object.freeze({
    /** New */
    New: 'N',
    /** Cancel */
    Cancel: 'C',
    /** Replace */
    Replace: 'R',
} as const);

type IOITransType = (typeof IOITransType)[keyof typeof IOITransType];
