/**
 * Indicates whether an option contract is a put, call, chooser or undetermined.
 * - Tag: 201
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PutOrCall = Object.freeze({
    Put: 0,
    Call: 1,
    Other: 2,
    Chooser: 3,
} as const);

type PutOrCall = (typeof PutOrCall)[keyof typeof PutOrCall];
