/**
 * Indicates how the gross cash dividend amount per share is determined.
 * - Tag: 42247
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DividendAmountType = Object.freeze({
    RecordAmount: 0,
    ExAmount: 1,
    PaidAmount: 2,
    PerMasterConfirm: 3,
} as const);

type DividendAmountType = (typeof DividendAmountType)[keyof typeof DividendAmountType];
