/**
 * Type of stream assignment request.
 * - Tag: 1498
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamAsgnReqType = Object.freeze({
    /** Stream assignment for new customer(s) */
    StreamAssignmentForNewCustomer: 1,
    /** Stream assignment for existing customer(s) */
    StreamAssignmentForExistingCustomer: 2,
} as const);

type StreamAsgnReqType = (typeof StreamAsgnReqType)[keyof typeof StreamAsgnReqType];
