/**
 * Used to describe the origin of the market data entry.
 * - Tag: 1024
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDOriginType = Object.freeze({
    /** Book */
    Book: 0,
    /** Off-Book */
    OffBook: 1,
    /** Cross */
    Cross: 2,
    QuoteDrivenMarket: 3,
    /** Dark order book */
    DarkOrderBook: 4,
    AuctionDrivenMarket: 5,
    QuoteNegotiation: 6,
    VoiceNegotiation: 7,
    HybridMarket: 8,
    OtherMarket: 9,
} as const);

type MDOriginType = (typeof MDOriginType)[keyof typeof MDOriginType];
