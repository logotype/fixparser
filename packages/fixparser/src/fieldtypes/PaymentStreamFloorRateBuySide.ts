/**
 * Reference to the buyer of the floor rate option through its trade side.
 * - Tag: 40801
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamFloorRateBuySide = Object.freeze({
    /** Buyer of the trade */
    Buyer: 1,
    /** Seller of the trade */
    Seller: 2,
} as const);

type PaymentStreamFloorRateBuySide = (typeof PaymentStreamFloorRateBuySide)[keyof typeof PaymentStreamFloorRateBuySide];
