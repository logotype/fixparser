/**
 * Status of Trade Request.
 * - Tag: 750
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeRequestStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Completed */
    Completed: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type TradeRequestStatus = (typeof TradeRequestStatus)[keyof typeof TradeRequestStatus];
