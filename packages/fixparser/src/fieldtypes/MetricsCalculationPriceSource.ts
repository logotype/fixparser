/**
 * Specifies the source of the price(s) of the security used in the calculation of the metrics or analytics data.
 * - Tag: 2993
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MetricsCalculationPriceSource = Object.freeze({
    Realtime: 1,
    EndOfDay: 2,
} as const);

type MetricsCalculationPriceSource = (typeof MetricsCalculationPriceSource)[keyof typeof MetricsCalculationPriceSource];
