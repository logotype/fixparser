/**
 * Specifies the type of tick rule which is being described
 * - Tag: 1209
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TickRuleType = Object.freeze({
    /** Regular trading */
    RegularTrading: 0,
    /** Variable cabinet */
    VariableCabinet: 1,
    /** Fixed cabinet */
    FixedCabinet: 2,
    /** Traded as a spread leg */
    TradedAsASpreadLeg: 3,
    /** Settled as a spread leg */
    SettledAsASpreadLeg: 4,
    TradedAsSpread: 5,
} as const);

type TickRuleType = (typeof TickRuleType)[keyof typeof TickRuleType];
