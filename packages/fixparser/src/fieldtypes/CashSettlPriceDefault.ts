/**
 * The default election for determining settlement price.
 * - Tag: 42217
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CashSettlPriceDefault = Object.freeze({
    Close: 0,
    Hedge: 1,
} as const);

type CashSettlPriceDefault = (typeof CashSettlPriceDefault)[keyof typeof CashSettlPriceDefault];
