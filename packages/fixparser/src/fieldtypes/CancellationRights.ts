/**
 * For CIV - A one character code identifying whether Cancellation rights/Cooling off period applies.
 * - Tag: 480
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const CancellationRights = Object.freeze({
    /** Yes */
    Yes: 'Y',
    /** No - Execution Only */
    NoExecutionOnly: 'N',
    /** No - Waiver agreement */
    NoWaiverAgreement: 'M',
    /** No - Institutional */
    NoInstitutional: 'O',
} as const);

type CancellationRights = (typeof CancellationRights)[keyof typeof CancellationRights];
