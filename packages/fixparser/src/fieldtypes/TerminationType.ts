/**
 * Type of financing termination.
 * - Tag: 788
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TerminationType = Object.freeze({
    /** Overnight */
    Overnight: 1,
    /** Term */
    Term: 2,
    /** Flexible */
    Flexible: 3,
    /** Open */
    Open: 4,
} as const);

type TerminationType = (typeof TerminationType)[keyof typeof TerminationType];
