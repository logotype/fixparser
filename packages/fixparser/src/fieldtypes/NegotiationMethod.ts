/**
 * Specifies the negotiation method to be used.
 * - Tag: 2115
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NegotiationMethod = Object.freeze({
    AutoSpot: 0,
    NegotiatedSpot: 1,
    PhoneSpot: 2,
} as const);

type NegotiationMethod = (typeof NegotiationMethod)[keyof typeof NegotiationMethod];
