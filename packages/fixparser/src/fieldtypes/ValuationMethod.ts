/**
 * Specifies the type of valuation method applied.
 * - Tag: 1197
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const ValuationMethod = Object.freeze({
    /** premium style */
    PremiumStyle: 'EQTY',
    /** futures style mark-to-market */
    FuturesStyleMarkToMarket: 'FUT',
    /** futures style with an attached cash adjustment */
    FuturesStyleWithAnAttachedCashAdjustment: 'FUTDA',
    /** CDS style collateralization of market to market and coupon */
    CDSStyleCollateralization: 'CDS',
    /** CDS in delivery - use recovery rate to calculate obligation */
    CDSInDeliveryUseRecoveryRateToCalculate: 'CDSD',
} as const);

type ValuationMethod = (typeof ValuationMethod)[keyof typeof ValuationMethod];
