/**
 * Identifies an event related to a TradSesStatus(340). An event occurs and is gone, it is not a state that applies for a period of time.
 * - Tag: 1368
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradSesEvent = Object.freeze({
    /** Trading resumes (after Halt) */
    TradingResumes: 0,
    /** Change of Trading Session */
    ChangeOfTradingSession: 1,
    /** Change of Trading Subsession */
    ChangeOfTradingSubsession: 2,
    /** Change of Trading Status */
    ChangeOfTradingStatus: 3,
} as const);

type TradSesEvent = (typeof TradSesEvent)[keyof typeof TradSesEvent];
