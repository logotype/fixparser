/**
 * Describes the source of the identifier that RelatedTradeID(1856) represents.
 * - Tag: 1857
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelatedTradeIDSource = Object.freeze({
    /** Non-FIX source */
    NonFIXSource: 0,
    /** Trade ID */
    TradeID: 1,
    /** Secondary trade ID */
    SecondaryTradeID: 2,
    /** Trade report ID */
    TradeReportID: 3,
    /** Firm trade ID */
    FirmTradeID: 4,
    /** Secondary firm Trade ID */
    SecondaryFirmTradeID: 5,
    /** Regulatory trade ID */
    RegulatoryTradeID: 6,
} as const);

type RelatedTradeIDSource = (typeof RelatedTradeIDSource)[keyof typeof RelatedTradeIDSource];
