/**
 * Technical event within market data feed.
 * - Tag: 2535
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDReportEvent = Object.freeze({
    StartInstrumentRefData: 1,
    EndInstrumentRefData: 2,
    StartOffMarketTrades: 3,
    EndOffMarketTrades: 4,
    StartOrderBookTrades: 5,
    EndOrderBookTrades: 6,
    StartOpenInterest: 7,
    EndOpenInterest: 8,
    StartSettlementPrices: 9,
    EndSettlementPrices: 10,
    StartStatsRefData: 11,
    EndStatsRefData: 12,
    StartStatistics: 13,
    EndStatistics: 14,
} as const);

type MDReportEvent = (typeof MDReportEvent)[keyof typeof MDReportEvent];
