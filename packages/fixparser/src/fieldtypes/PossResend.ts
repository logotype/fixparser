/**
 * Indicates that message may contain information that has been sent under another sequence number.
 * - Tag: 97
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PossResend = Object.freeze({
    /** Original Transmission */
    OriginalTransmission: 'N',
    /** Possible Resend */
    PossibleResend: 'Y',
} as const);

type PossResend = (typeof PossResend)[keyof typeof PossResend];
