/**
 * Specifies the activity state the test suite is in.
 * - Tag: 3069
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestSuiteActivityState = Object.freeze({
    Scheduled: 0,
    Completed: 1,
    Cancelled: 2,
} as const);

type TestSuiteActivityState = (typeof TestSuiteActivityState)[keyof typeof TestSuiteActivityState];
