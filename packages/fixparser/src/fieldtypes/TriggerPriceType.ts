/**
 * The type of price that the trigger is compared to.
 * - Tag: 1107
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TriggerPriceType = Object.freeze({
    /** Best Offer */
    BestOffer: '1',
    /** Last Trade */
    LastTrade: '2',
    /** Best Bid */
    BestBid: '3',
    /** Best Bid or Last Trade */
    BestBidOrLastTrade: '4',
    /** Best Offer or Last Trade */
    BestOfferOrLastTrade: '5',
    /** Best Mid */
    BestMid: '6',
} as const);

type TriggerPriceType = (typeof TriggerPriceType)[keyof typeof TriggerPriceType];
