/**
 * Defines the scope of TriggerAction(1101) when it is set to "cancel" (3).
 * - Tag: 1628
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TriggerScope = Object.freeze({
    /** This order (default) */
    ThisOrder: 0,
    /** Other order (use RefID) */
    OtherOrder: 1,
    /** All other orders for the given security */
    AllOtherOrdersForGivenSecurity: 2,
    /** All other orders for the given security and price */
    AllOtherOrdersForGivenSecurityAndPrice: 3,
    /** All other orders for the given security and side */
    AllOtherOrdersForGivenSecurityAndSide: 4,
    /** All other orders for the given security, price and side */
    AllOtherOrdersForGivenSecurityPriceAndSide: 5,
} as const);

type TriggerScope = (typeof TriggerScope)[keyof typeof TriggerScope];
