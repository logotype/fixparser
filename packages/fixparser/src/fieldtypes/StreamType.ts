/**
 * Type of swap stream.
 * - Tag: 40050
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamType = Object.freeze({
    /** Payment / cash settlement */
    PaymentCashSettlement: 0,
    /** Physical delivery */
    PhysicalDelivery: 1,
} as const);

type StreamType = (typeof StreamType)[keyof typeof StreamType];
