/**
 * RemunerationIndicator
 * - Tag: 2356
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RemunerationIndicator = Object.freeze({
    /** No remuneration paid */
    NoRemunerationPaid: 0,
    /** Remuneration paid */
    RemunerationPaid: 1,
} as const);

type RemunerationIndicator = (typeof RemunerationIndicator)[keyof typeof RemunerationIndicator];
