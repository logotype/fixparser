/**
 * Action to take should throttle limit be exceeded.
 * - Tag: 1611
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ThrottleAction = Object.freeze({
    /** Queue inbound */
    QueueInbound: 0,
    /** Queue outbound */
    QueueOutbound: 1,
    /** Reject */
    Reject: 2,
    /** Disconnect */
    Disconnect: 3,
    /** Warning */
    Warning: 4,
} as const);

type ThrottleAction = (typeof ThrottleAction)[keyof typeof ThrottleAction];
