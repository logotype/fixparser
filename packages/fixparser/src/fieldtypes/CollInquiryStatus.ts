/**
 * Status of Collateral Inquiry
 * - Tag: 945
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollInquiryStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Accepted With Warnings */
    AcceptedWithWarnings: 1,
    /** Completed */
    Completed: 2,
    /** Completed With Warnings */
    CompletedWithWarnings: 3,
    /** Rejected */
    Rejected: 4,
} as const);

type CollInquiryStatus = (typeof CollInquiryStatus)[keyof typeof CollInquiryStatus];
