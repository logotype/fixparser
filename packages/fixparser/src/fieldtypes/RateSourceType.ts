/**
 * Indicates whether the rate source specified is a primary or secondary source.
 * - Tag: 1447
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RateSourceType = Object.freeze({
    /** Primary */
    Primary: 0,
    /** Secondary */
    Secondary: 1,
} as const);

type RateSourceType = (typeof RateSourceType)[keyof typeof RateSourceType];
