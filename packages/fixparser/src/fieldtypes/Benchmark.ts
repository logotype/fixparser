/**
 * For Fixed Income. Identifies the benchmark (e.g. used in conjunction with the SpreadToBenchmark field).
 * - Tag: 219
 * - FIX Specification type: char
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const Benchmark = Object.freeze({
    /** CURVE */
    CURVE: '1',
    /** 5-YR */
    FiveYR: '2',
    /** OLD-5 */
    OLD5: '3',
    /** 10-YR */
    TenYR: '4',
    /** OLD-10 */
    OLD10: '5',
    /** 30-YR */
    ThirtyYR: '6',
    /** OLD-30 */
    OLD30: '7',
    /** 3-MO-LIBOR */
    ThreeMOLIBOR: '8',
    /** 6-MO-LIBOR */
    SixMOLIBOR: '9',
} as const);

type Benchmark = (typeof Benchmark)[keyof typeof Benchmark];
