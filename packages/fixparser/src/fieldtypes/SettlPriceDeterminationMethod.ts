/**
 * Calculation method used to determine settlement price.
 * - Tag: 2451
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlPriceDeterminationMethod = Object.freeze({
    /** Unknown */
    Unknown: 0,
    /** Last trade price */
    LastTradePrice: 1,
    /** Last bid price */
    LastBidPrice: 2,
    /** Last offer price */
    LastOfferPrice: 3,
    MidPrice: 4,
    AverageLastTradePrice: 5,
    AverageLastTradePeriod: 6,
    UnderlyingPrice: 7,
    CalculatedPrice: 8,
    ManualPrice: 9,
} as const);

type SettlPriceDeterminationMethod = (typeof SettlPriceDeterminationMethod)[keyof typeof SettlPriceDeterminationMethod];
