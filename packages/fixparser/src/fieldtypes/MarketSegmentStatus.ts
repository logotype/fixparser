/**
 * Status of market segment.
 * - Tag: 2542
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketSegmentStatus = Object.freeze({
    Active: 1,
    Inactive: 2,
    Published: 3,
} as const);

type MarketSegmentStatus = (typeof MarketSegmentStatus)[keyof typeof MarketSegmentStatus];
