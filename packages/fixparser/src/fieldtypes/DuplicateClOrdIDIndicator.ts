/**
 * DuplicateClOrdIDIndicator
 * - Tag: 2829
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DuplicateClOrdIDIndicator = Object.freeze({
    /** Unique ClOrdID(11) */
    UniqueClOrdID: 'N',
    /** Duplicate ClOrdID(11) */
    DuplicateClOrdID: 'Y',
} as const);

type DuplicateClOrdIDIndicator = (typeof DuplicateClOrdIDIndicator)[keyof typeof DuplicateClOrdIDIndicator];
