/**
 * Used to specify the day of the week in which the reset occurs for payments that reset on a weekly basis.
 * - Tag: 40766
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PaymentStreamResetWeeklyRollConvention = Object.freeze({
    /** Monday */
    Monday: 'MON',
    /** Tuesday */
    Tuesday: 'TUE',
    /** Wednesday */
    Wednesday: 'WED',
    /** Thursday */
    Thursday: 'THU',
    /** Friday */
    Friday: 'FRI',
    /** Saturday */
    Saturday: 'SAT',
    /** Sunday */
    Sunday: 'SUN',
} as const);

type PaymentStreamResetWeeklyRollConvention =
    (typeof PaymentStreamResetWeeklyRollConvention)[keyof typeof PaymentStreamResetWeeklyRollConvention];
