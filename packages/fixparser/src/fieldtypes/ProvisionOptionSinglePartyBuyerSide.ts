/**
 * If optional early termination is not available to both parties then this component identifies the buyer of the option through its side of the trade.
 * - Tag: 40099
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionOptionSinglePartyBuyerSide = Object.freeze({
    /** Buy */
    Buy: 1,
    /** Sell */
    Sell: 2,
} as const);

type ProvisionOptionSinglePartyBuyerSide =
    (typeof ProvisionOptionSinglePartyBuyerSide)[keyof typeof ProvisionOptionSinglePartyBuyerSide];
