/**
 * Status for a statistic to indicate its availability.
 * - Tag: 2477
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDStatisticStatus = Object.freeze({
    /** Active (default) */
    Active: 1,
    /** Inactive (not disseminated) */
    Inactive: 2,
} as const);

type MDStatisticStatus = (typeof MDStatisticStatus)[keyof typeof MDStatisticStatus];
