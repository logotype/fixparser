/**
 * Number of shares in numeric or relative size.
 * - Tag: 27
 * - FIX Specification type: String
 * - FIX Specification version: FIX42
 * - Mapped type: string
 * @readonly
 * @public
 */
export const IOIShares = Object.freeze({
    /** Large */
    Large: 'L',
    /** Medium */
    Medium: 'M',
    /** Small */
    Small: 'S',
} as const);

type IOIShares = (typeof IOIShares)[keyof typeof IOIShares];
