/**
 * Specifies whether a party to a swap is using an exception to a clearing requirement. In the US, one such clearing requirement is CFTC's rule pursuant to CEA Section 2(h)(1).
 * - Tag: 1932
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ClearingRequirementException = Object.freeze({
    /** No exception */
    NoException: 0,
    Exception: 1,
    EndUserException: 2,
    InterAffiliateException: 3,
    TreasuryAffiliateException: 4,
    CooperativeException: 5,
} as const);

type ClearingRequirementException = (typeof ClearingRequirementException)[keyof typeof ClearingRequirementException];
