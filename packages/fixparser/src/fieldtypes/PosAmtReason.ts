/**
 * Specifies the reason for an amount type when reported on a position. Useful when multiple instances of the same amount type are reported.
 * - Tag: 1585
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PosAmtReason = Object.freeze({
    /** Options settlement */
    OptionsSettlement: 0,
    /** Pending erosion adjustment */
    PendingErosionAdjustment: 1,
    /** Final erosion adjustment */
    FinalErosionAdjustment: 2,
    /** Tear-up coupon amount */
    TearUpCouponAmount: 3,
    PriceAlignmentInterest: 4,
    /** Delivery invoice charges */
    DeliveryInvoiceCharges: 5,
    /** Delivery storage charges */
    DeliveryStorageCharges: 6,
} as const);

type PosAmtReason = (typeof PosAmtReason)[keyof typeof PosAmtReason];
