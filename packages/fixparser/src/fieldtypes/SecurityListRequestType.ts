/**
 * Identifies the type/criteria of Security List Request
 * - Tag: 559
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SecurityListRequestType = Object.freeze({
    /** Symbol */
    Symbol: 0,
    /** SecurityType and/or CFICode */
    SecurityTypeAnd: 1,
    /** Product */
    Product: 2,
    /** TradingSessionID */
    TradingSessionID: 3,
    /** All Securities */
    AllSecurities: 4,
    /** MarketID or MarketID + MarketSegmentID */
    MarketIDOrMarketID: 5,
} as const);

type SecurityListRequestType = (typeof SecurityListRequestType)[keyof typeof SecurityListRequestType];
