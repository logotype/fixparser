/**
 * Identifies the reason for rejection of a New Order List message. Note that OrdRejReason(103) is used if the rejection is based on properties of an individual order part of the List.
 * - Tag: 1386
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ListRejectReason = Object.freeze({
    /** Broker / Exchange option */
    BrokerCredit: 0,
    /** Exchange closed */
    ExchangeClosed: 2,
    /** Too late to enter */
    TooLateToEnter: 4,
    /** Unknown order */
    UnknownOrder: 5,
    /** Duplicate Order (e.g. dupe ClOrdID) */
    DuplicateOrder: 6,
    /** Unsupported order characteristic */
    UnsupportedOrderCharacteristic: 11,
    /** Other */
    Other: 99,
} as const);

type ListRejectReason = (typeof ListRejectReason)[keyof typeof ListRejectReason];
