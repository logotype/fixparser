/**
 * Specifies how the underlying price is determined at the point of option exercise. The underlying price may be set to the current settlement price, set to a special reference, set to the optimal value of the underlying during the defined period ("Look-back") or set to the average value of the underlying during the defined period ("Asian option").
 * - Tag: 1481
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const UnderlyingPriceDeterminationMethod = Object.freeze({
    /** Regular */
    Regular: 1,
    /** Special reference */
    SpecialReference: 2,
    /** Optimal value (Lookback) */
    OptimalValue: 3,
    /** Average value (Asian option) */
    AverageValue: 4,
} as const);

type UnderlyingPriceDeterminationMethod =
    (typeof UnderlyingPriceDeterminationMethod)[keyof typeof UnderlyingPriceDeterminationMethod];
