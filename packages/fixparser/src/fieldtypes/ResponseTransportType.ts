/**
 * Identifies how the response to the request should be transmitted.
 * - Tag: 725
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ResponseTransportType = Object.freeze({
    Inband: 0,
    OutOfBand: 1,
} as const);

type ResponseTransportType = (typeof ResponseTransportType)[keyof typeof ResponseTransportType];
