/**
 * Indicates whether the resulting position after a trade should be an opening position or closing position. Used for omnibus accounting - where accounts are held on a gross basis instead of being netted together.
 * - Tag: 1047
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AllocPositionEffect = Object.freeze({
    /** Open */
    Open: 'O',
    /** Close */
    Close: 'C',
    /** Rolled */
    Rolled: 'R',
    /** FIFO */
    FIFO: 'F',
} as const);

type AllocPositionEffect = (typeof AllocPositionEffect)[keyof typeof AllocPositionEffect];
