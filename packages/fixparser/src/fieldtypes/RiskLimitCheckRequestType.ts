/**
 * Specifies the type of limit amount check being requested.
 * - Tag: 2323
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitCheckRequestType = Object.freeze({
    AllOrNone: 0,
    Partial: 1,
} as const);

type RiskLimitCheckRequestType = (typeof RiskLimitCheckRequestType)[keyof typeof RiskLimitCheckRequestType];
