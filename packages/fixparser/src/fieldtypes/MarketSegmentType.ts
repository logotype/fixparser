/**
 * Used to classify the type of market segment.
 * - Tag: 2543
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketSegmentType = Object.freeze({
    Pool: 1,
    Retail: 2,
    Wholesale: 3,
} as const);

type MarketSegmentType = (typeof MarketSegmentType)[keyof typeof MarketSegmentType];
