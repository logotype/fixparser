/**
 * Code to represent the status of a list order.
 * - Tag: 431
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ListOrderStatus = Object.freeze({
    /** In bidding process */
    InBiddingProcess: 1,
    /** Received for execution */
    ReceivedForExecution: 2,
    /** Executing */
    Executing: 3,
    /** Cancelling */
    Cancelling: 4,
    /** Alert */
    Alert: 5,
    /** All Done */
    AllDone: 6,
    /** Reject */
    Reject: 7,
} as const);

type ListOrderStatus = (typeof ListOrderStatus)[keyof typeof ListOrderStatus];
