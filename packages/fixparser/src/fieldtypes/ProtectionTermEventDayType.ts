/**
 * Day type for events that specify a period and unit.
 * - Tag: 40197
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProtectionTermEventDayType = Object.freeze({
    /** Business */
    Business: 0,
    /** Calendar */
    Calendar: 1,
    /** Commodity business */
    CommodityBusiness: 2,
    /** Currency business */
    CurrencyBusiness: 3,
    /** Exchange business */
    ExchangeBusiness: 4,
    /** Scheduled trading day */
    ScheduledTradingDay: 5,
} as const);

type ProtectionTermEventDayType = (typeof ProtectionTermEventDayType)[keyof typeof ProtectionTermEventDayType];
