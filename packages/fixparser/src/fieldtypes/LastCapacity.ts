/**
 * Broker capacity in order execution
 * - Tag: 29
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LastCapacity = Object.freeze({
    /** Agent */
    Agent: '1',
    /** Cross as agent */
    CrossAsAgent: '2',
    /** Cross as principal */
    CrossAsPrincipal: '3',
    /** Principal */
    Principal: '4',
    /** Riskless principal */
    RisklessPrincipal: '5',
} as const);

type LastCapacity = (typeof LastCapacity)[keyof typeof LastCapacity];
