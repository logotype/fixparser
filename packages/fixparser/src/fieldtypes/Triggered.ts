/**
 * Indicates whether order has been triggered during its lifetime. Applies to cases where original information, e.g. OrdType(40), is modified when the order is triggered.
 * - Tag: 1823
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const Triggered = Object.freeze({
    /** Not triggered (default) */
    NotTriggered: 0,
    /** Triggered */
    Triggered: 1,
    /** Stop order triggered */
    StopOrderTriggered: 2,
    /** One Cancels the Other (OCO) order triggered */
    OCOOrderTriggered: 3,
    /** One Triggers the Other (OTO) order triggered */
    OTOOrderTriggered: 4,
    /** One Updates the Other (OUO) order triggered */
    OUOOrderTriggered: 5,
} as const);

type Triggered = (typeof Triggered)[keyof typeof Triggered];
