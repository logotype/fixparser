/**
 * Describes the how the price limits are expressed.
 * - Tag: 1306
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PriceLimitType = Object.freeze({
    /** Price (default) */
    Price: 0,
    /** Ticks */
    Ticks: 1,
    /** Percentage */
    Percentage: 2,
} as const);

type PriceLimitType = (typeof PriceLimitType)[keyof typeof PriceLimitType];
