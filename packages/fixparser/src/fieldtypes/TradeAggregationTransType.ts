/**
 * Identifies the trade aggregation transaction type.
 * - Tag: 2788
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeAggregationTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
    /** Replace */
    Replace: 2,
} as const);

type TradeAggregationTransType = (typeof TradeAggregationTransType)[keyof typeof TradeAggregationTransType];
