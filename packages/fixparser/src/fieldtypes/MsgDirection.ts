/**
 * Specifies the direction of the message.
 * - Tag: 385
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MsgDirection = Object.freeze({
    /** Receive */
    Receive: 'R',
    /** Send */
    Send: 'S',
} as const);

type MsgDirection = (typeof MsgDirection)[keyof typeof MsgDirection];
