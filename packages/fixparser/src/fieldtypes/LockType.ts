/**
 * Indicates whether an order is locked and for what reason.
 * - Tag: 1807
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LockType = Object.freeze({
    /** Not locked */
    NotLocked: 0,
    /** Away market better */
    AwayMarketNetter: 1,
    /** Three tick locked */
    ThreeTickLocked: 2,
    /** Locked by market maker */
    LockedByMarketMaker: 3,
    /** Directed order lock */
    DirectedOrderLock: 4,
    MultilegLock: 5,
    /** Market order lock */
    MarketOrderLock: 6,
    /** Pre-assignment lock */
    PreAssignmentLock: 7,
} as const);

type LockType = (typeof LockType)[keyof typeof LockType];
