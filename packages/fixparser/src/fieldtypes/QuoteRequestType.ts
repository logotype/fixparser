/**
 * Indicates the type of Quote Request being generated
 * - Tag: 303
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteRequestType = Object.freeze({
    /** Manual */
    Manual: 1,
    /** Automatic */
    Automatic: 2,
    /** Confirm quote */
    ConfirmQuote: 3,
} as const);

type QuoteRequestType = (typeof QuoteRequestType)[keyof typeof QuoteRequestType];
