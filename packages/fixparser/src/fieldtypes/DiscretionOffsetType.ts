/**
 * Type of Discretion Offset value
 * - Tag: 842
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DiscretionOffsetType = Object.freeze({
    /** Price (default) */
    Price: 0,
    /** Basis Points */
    BasisPoints: 1,
    /** Ticks */
    Ticks: 2,
    /** Price Tier / Level */
    PriceTier: 3,
} as const);

type DiscretionOffsetType = (typeof DiscretionOffsetType)[keyof typeof DiscretionOffsetType];
