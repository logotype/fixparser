/**
 * Specifies which type of identifier is specified in RefRiskLimitCheckID(2334) field.
 * - Tag: 2335
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RefRiskLimitCheckIDType = Object.freeze({
    /** RiskLimitRequestID(1666) */
    RiskLimitRequestID: 0,
    /** RiskLimitCheckID(2319) */
    RiskLimitCheckID: 1,
    /** Out of band identifier */
    OutOfBandID: 3,
} as const);

type RefRiskLimitCheckIDType = (typeof RefRiskLimitCheckIDType)[keyof typeof RefRiskLimitCheckIDType];
