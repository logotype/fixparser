/**
 * Indicates to recipient whether trade is clearing at execution prices LastPx(tag 31) or alternate clearing prices SideClearingTradePrice(tag 1597).
 * - Tag: 1598
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SideClearingTradePriceType = Object.freeze({
    /** Trade Clearing at Execution Price */
    TradeClearingAtExecutionPrice: 0,
    /** Trade Clearing at Alternate Clearing Price */
    TradeClearingAtAlternateClearingPrice: 1,
} as const);

type SideClearingTradePriceType = (typeof SideClearingTradePriceType)[keyof typeof SideClearingTradePriceType];
