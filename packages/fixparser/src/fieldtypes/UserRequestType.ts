/**
 * Indicates the action required by a User Request Message
 * - Tag: 924
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const UserRequestType = Object.freeze({
    /** Log On User */
    LogOnUser: 1,
    /** Log Off User */
    LogOffUser: 2,
    /** Change Password For User */
    ChangePasswordForUser: 3,
    /** Request Individual User Status */
    RequestIndividualUserStatus: 4,
    /** Request Throttle Limit */
    RequestThrottleLimit: 5,
} as const);

type UserRequestType = (typeof UserRequestType)[keyof typeof UserRequestType];
