/**
 * Reason code for stream assignment request reject.
 * - Tag: 1502
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamAsgnRejReason = Object.freeze({
    /** Unknown client */
    UnknownClient: 0,
    /** Exceeds maximum size */
    ExceedsMaximumSize: 1,
    /** Unknown or Invalid currency pair */
    UnknownOrInvalidCurrencyPair: 2,
    /** No available stream */
    NoAvailableStream: 3,
    /** Other */
    Other: 99,
} as const);

type StreamAsgnRejReason = (typeof StreamAsgnRejReason)[keyof typeof StreamAsgnRejReason];
