/**
 * Indicates market maker participation in security.
 * - Tag: 1655
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MarketMakerActivity = Object.freeze({
    /** No participation */
    NoParticipation: 0,
    /** Buy participation */
    BuyParticipation: 1,
    /** Sell participation */
    SellParticipation: 2,
    /** Both buy and sell participation */
    BothBuyAndSellParticipation: 3,
} as const);

type MarketMakerActivity = (typeof MarketMakerActivity)[keyof typeof MarketMakerActivity];
