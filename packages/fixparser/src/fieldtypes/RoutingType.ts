/**
 * Indicates the type of RoutingID (217) specified.
 * - Tag: 216
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RoutingType = Object.freeze({
    /** Target Firm */
    TargetFirm: 1,
    /** Target List */
    TargetList: 2,
    /** Block Firm */
    BlockFirm: 3,
    /** Block List */
    BlockList: 4,
    /** Target Person */
    TargetPerson: 5,
    /** Block Person */
    BlockPerson: 6,
} as const);

type RoutingType = (typeof RoutingType)[keyof typeof RoutingType];
