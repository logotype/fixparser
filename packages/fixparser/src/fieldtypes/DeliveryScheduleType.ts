/**
 * Specifies the type of delivery schedule.
 * - Tag: 41038
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryScheduleType = Object.freeze({
    /** Notional */
    Notional: 0,
    /** Delivery */
    Delivery: 1,
    /** Physical settlement period */
    PhysicalSettlPeriods: 2,
} as const);

type DeliveryScheduleType = (typeof DeliveryScheduleType)[keyof typeof DeliveryScheduleType];
