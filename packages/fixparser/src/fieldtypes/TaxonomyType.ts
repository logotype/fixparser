/**
 * The type of identification taxonomy used to identify the security.
 * - Tag: 2375
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TaxonomyType = Object.freeze({
    ISINOrAltInstrmtID: 'I',
    InterimTaxonomy: 'E',
} as const);

type TaxonomyType = (typeof TaxonomyType)[keyof typeof TaxonomyType];
