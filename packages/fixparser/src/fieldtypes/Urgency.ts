/**
 * Urgency flag
 * - Tag: 61
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const Urgency = Object.freeze({
    /** Normal */
    Normal: '0',
    /** Flash */
    Flash: '1',
    /** Background */
    Background: '2',
} as const);

type Urgency = (typeof Urgency)[keyof typeof Urgency];
