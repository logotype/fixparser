/**
 * The source of the payment stream floating rate index.
 * - Tag: 40790
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamRateIndexSource = Object.freeze({
    /** Bloomberg */
    Bloomberg: 0,
    /** Reuters */
    Reuters: 1,
    /** Telerate */
    Telerate: 2,
    /** Other */
    Other: 99,
} as const);

type PaymentStreamRateIndexSource = (typeof PaymentStreamRateIndexSource)[keyof typeof PaymentStreamRateIndexSource];
