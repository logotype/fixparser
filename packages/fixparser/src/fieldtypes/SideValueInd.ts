/**
 * Code to identify which "SideValue" the value refers to. SideValue1 and SideValue2 are used as opposed to Buy or Sell so that the basket can be quoted either way as Buy or Sell.
 * - Tag: 401
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SideValueInd = Object.freeze({
    /** Side Value 1 */
    SideValue1: 1,
    /** Side Value 2 */
    SideValue2: 2,
} as const);

type SideValueInd = (typeof SideValueInd)[keyof typeof SideValueInd];
