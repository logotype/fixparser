/**
 * MatchExceptionToleranceValueType
 * - Tag: 2779
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MatchExceptionToleranceValueType = Object.freeze({
    FixedAmount: 1,
    /** Percentage */
    Percentage: 2,
} as const);

type MatchExceptionToleranceValueType =
    (typeof MatchExceptionToleranceValueType)[keyof typeof MatchExceptionToleranceValueType];
