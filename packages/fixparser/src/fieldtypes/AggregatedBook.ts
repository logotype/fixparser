/**
 * Specifies whether or not book entries should be aggregated. (Not specified) = broker option
 * - Tag: 266
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AggregatedBook = Object.freeze({
    /** book entries to be aggregated */
    BookEntriesToBeAggregated: 'Y',
    /** book entries should not be aggregated */
    BookEntriesShouldNotBeAggregated: 'N',
} as const);

type AggregatedBook = (typeof AggregatedBook)[keyof typeof AggregatedBook];
