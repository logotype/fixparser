/**
 * BidTradeType
 * - Tag: 418
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const BidTradeType = Object.freeze({
    /** Agency */
    Agency: 'A',
    /** VWAP Guarantee */
    VWAPGuarantee: 'G',
    /** Guaranteed Close */
    GuaranteedClose: 'J',
    /** Risk Trade */
    RiskTrade: 'R',
} as const);

type BidTradeType = (typeof BidTradeType)[keyof typeof BidTradeType];
