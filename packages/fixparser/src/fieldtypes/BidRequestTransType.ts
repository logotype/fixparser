/**
 * Identifies the Bid Request message type.
 * - Tag: 374
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const BidRequestTransType = Object.freeze({
    /** Cancel */
    Cancel: 'C',
    /** New */
    New: 'N',
} as const);

type BidRequestTransType = (typeof BidRequestTransType)[keyof typeof BidRequestTransType];
