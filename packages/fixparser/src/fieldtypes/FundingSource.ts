/**
 * Specifies the funding source used to finance margin or collateralized loan.
 * - Tag: 2846
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const FundingSource = Object.freeze({
    Repo: 0,
    Cash: 1,
    FreeCedits: 2,
    CustomerShortSales: 3,
    BrokerShortSales: 4,
    UnsecuredBorrowing: 5,
    Other: 99,
} as const);

type FundingSource = (typeof FundingSource)[keyof typeof FundingSource];
