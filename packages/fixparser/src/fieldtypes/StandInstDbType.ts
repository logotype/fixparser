/**
 * Identifies the Standing Instruction database used
 * - Tag: 169
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StandInstDbType = Object.freeze({
    /** Other */
    Other: 0,
    /** DTC SID */
    DTCSID: 1,
    /** Thomson ALERT */
    ThomsonALERT: 2,
    /** A Global Custodian (StandInstDBName (70) must be provided) */
    AGlobalCustodian: 3,
    /** AccountNet */
    AccountNet: 4,
} as const);

type StandInstDbType = (typeof StandInstDbType)[keyof typeof StandInstDbType];
