/**
 * Status of the transfer.
 * - Tag: 2442
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferStatus = Object.freeze({
    /** Received */
    Received: 0,
    /** Rejected by intermediary */
    RejectedByIntermediary: 1,
    /** Accept pending */
    AcceptPending: 2,
    /** Accepted */
    Accepted: 3,
    /** Declined */
    Declined: 4,
    /** Cancelled */
    Cancelled: 5,
} as const);

type TransferStatus = (typeof TransferStatus)[keyof typeof TransferStatus];
