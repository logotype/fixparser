/**
 * Identifies the type of venue where a trade was executed.
 * - Tag: 1430
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const VenueType = Object.freeze({
    /** Electronic exchange */
    Electronic: 'E',
    /** Pit */
    Pit: 'P',
    /** Ex-pit */
    ExPit: 'X',
    /** Clearinghouse */
    ClearingHouse: 'C',
    RegisteredMarket: 'R',
    OffMarket: 'O',
    /** Central limit order book */
    CentralLimitOrderBook: 'B',
    /** Quote driven market */
    QuoteDrivenMarket: 'Q',
    /** Dark order book */
    DarkOrderBook: 'D',
    AuctionDrivenMarket: 'A',
    QuoteNegotiation: 'N',
    VoiceNegotiation: 'V',
    HybridMarket: 'H',
    OtherMarket: 'z',
} as const);

type VenueType = (typeof VenueType)[keyof typeof VenueType];
