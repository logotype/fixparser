/**
 * Matching Instruction for the order.
 * - Tag: 1625
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MatchInst = Object.freeze({
    /** Match */
    Match: 1,
    /** Do Not Match */
    DoNotMatch: 2,
} as const);

type MatchInst = (typeof MatchInst)[keyof typeof MatchInst];
