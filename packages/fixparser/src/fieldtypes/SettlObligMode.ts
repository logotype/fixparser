/**
 * Used to identify the reporting mode of the settlement obligation which is either preliminary or final
 * - Tag: 1159
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlObligMode = Object.freeze({
    /** Preliminary */
    Preliminary: 1,
    /** Final */
    Final: 2,
} as const);

type SettlObligMode = (typeof SettlObligMode)[keyof typeof SettlObligMode];
