/**
 * Indicates the type of trade quantity in TradeQty(1843).
 * - Tag: 1842
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeQtyType = Object.freeze({
    /** Cleared quantity */
    ClearedQuantity: 0,
    /** Long side claimed quantity */
    LongSideClaimedQuantity: 1,
    /** Short side claimed quantity */
    ShortSideClaimedQuantity: 2,
    /** Long side rejected quantity */
    LongSideRejectedQuantity: 3,
    /** Short side rejected quantity */
    ShortSideRejectedQuantity: 4,
    /** Pending quantity */
    PendingQuantity: 5,
    /** Transaction quantity */
    TransactionQuantity: 6,
    RemainingQuantity: 7,
    PreviousRemainingQuantity: 8,
} as const);

type TradeQtyType = (typeof TradeQtyType)[keyof typeof TradeQtyType];
