/**
 * RoundingDirection
 * - Tag: 468
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const RoundingDirection = Object.freeze({
    /** Round to nearest */
    RoundToNearest: '0',
    /** Round down */
    RoundDown: '1',
    /** Round up */
    RoundUp: '2',
} as const);

type RoundingDirection = (typeof RoundingDirection)[keyof typeof RoundingDirection];
