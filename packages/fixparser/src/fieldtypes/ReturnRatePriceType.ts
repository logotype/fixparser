/**
 * Specifies whether the ReturnRatePrice(42767) is expressed in absolute or relative terms.
 * - Tag: 42769
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ReturnRatePriceType = Object.freeze({
    /** Absolute terms */
    AbsoluteTerms: 0,
    /** Percentage of notional */
    PercentageOfNotional: 1,
} as const);

type ReturnRatePriceType = (typeof ReturnRatePriceType)[keyof typeof ReturnRatePriceType];
