/**
 * Broker's side of advertised trade
 * - Tag: 4
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AdvSide = Object.freeze({
    /** Buy */
    Buy: 'B',
    /** Sell */
    Sell: 'S',
    /** Trade */
    Trade: 'T',
    /** Cross */
    Cross: 'X',
} as const);

type AdvSide = (typeof AdvSide)[keyof typeof AdvSide];
