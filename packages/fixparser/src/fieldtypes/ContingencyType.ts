/**
 * Defines the type of contingency.
 * - Tag: 1385
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ContingencyType = Object.freeze({
    /** One Cancels the Other (OCO) */
    OneCancelsTheOther: 1,
    /** One Triggers the Other (OTO) */
    OneTriggersTheOther: 2,
    /** One Updates the Other (OUO) - Absolute Quantity Reduction */
    OneUpdatesTheOtherAbsolute: 3,
    /** One Updates the Other (OUO) - Proportional Quantity Reduction */
    OneUpdatesTheOtherProportional: 4,
    /** Bid and Offer */
    BidAndOffer: 5,
    /** Bid and Offer OCO */
    BidAndOfferOCO: 6,
} as const);

type ContingencyType = (typeof ContingencyType)[keyof typeof ContingencyType];
