/**
 * Instruction related to system generated auctions, e.g. flash order auctions.
 * - Tag: 1805
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AuctionInstruction = Object.freeze({
    /** Automatic auction permitted (default) */
    AutomatedAuctionPermitted: 0,
    /** Automatic auction not permitted */
    AutomatedAuctionNotPermitted: 1,
} as const);

type AuctionInstruction = (typeof AuctionInstruction)[keyof typeof AuctionInstruction];
