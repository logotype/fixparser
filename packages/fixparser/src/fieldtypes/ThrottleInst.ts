/**
 * Describes action recipient should take if a throttle limit were exceeded.
 * - Tag: 1685
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ThrottleInst = Object.freeze({
    /** Reject if throttle limit exceeded */
    RejectIfThrottleLimitExceeded: 0,
    /** Queue if throttle limit exceeded */
    QueueIfThrottleLimitExceeded: 1,
} as const);

type ThrottleInst = (typeof ThrottleInst)[keyof typeof ThrottleInst];
