/**
 * Operator to perform on the instrument(s) specified
 * - Tag: 1535
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const InstrumentScopeOperator = Object.freeze({
    /** Include */
    Include: 1,
    /** Exclude */
    Exclude: 2,
} as const);

type InstrumentScopeOperator = (typeof InstrumentScopeOperator)[keyof typeof InstrumentScopeOperator];
