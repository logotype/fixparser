/**
 * Time unit associated with the floating rate index.
 * - Tag: 40791
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PaymentStreamRateIndexCurveUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type PaymentStreamRateIndexCurveUnit =
    (typeof PaymentStreamRateIndexCurveUnit)[keyof typeof PaymentStreamRateIndexCurveUnit];
