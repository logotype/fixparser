/**
 * Identifies the type of adjustment.
 * - Tag: 334
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const Adjustment = Object.freeze({
    /** Cancel */
    Cancel: 1,
    /** Error */
    Error: 2,
    /** Correction */
    Correction: 3,
} as const);

type Adjustment = (typeof Adjustment)[keyof typeof Adjustment];
