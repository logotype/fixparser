/**
 * The relationship between Registration parties.
 * - Tag: 517
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const OwnershipType = Object.freeze({
    /** Joint Investors */
    JointInvestors: 'J',
    /** Tenants in Common */
    TenantsInCommon: 'T',
    /** Joint Trustees */
    JointTrustees: '2',
} as const);

type OwnershipType = (typeof OwnershipType)[keyof typeof OwnershipType];
