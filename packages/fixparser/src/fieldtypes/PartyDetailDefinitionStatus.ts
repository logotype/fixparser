/**
 * Status of party detail definition for one party.
 * - Tag: 1879
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PartyDetailDefinitionStatus = Object.freeze({
    /** Accepted */
    Accepted: 0,
    /** Accepted with changes */
    AcceptedWithChanges: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type PartyDetailDefinitionStatus = (typeof PartyDetailDefinitionStatus)[keyof typeof PartyDetailDefinitionStatus];
