/**
 * A one character code identifying Money laundering status.
 * - Tag: 481
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const MoneyLaunderingStatus = Object.freeze({
    /** Passed */
    Passed: 'Y',
    /** Not Checked */
    NotChecked: 'N',
    /** Exempt - Below the Limit */
    ExemptBelowLimit: '1',
    /** Exempt - Client Money Type exemption */
    ExemptMoneyType: '2',
    /** Exempt - Authorised Credit or financial institution */
    ExemptAuthorised: '3',
} as const);

type MoneyLaunderingStatus = (typeof MoneyLaunderingStatus)[keyof typeof MoneyLaunderingStatus];
