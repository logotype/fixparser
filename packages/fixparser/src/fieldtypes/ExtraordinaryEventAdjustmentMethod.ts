/**
 * Defines how adjustments will be made to the contract should one or more of the extraordinary events occur.
 * - Tag: 2602
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ExtraordinaryEventAdjustmentMethod = Object.freeze({
    CalculationAgent: 0,
    OptionsExchange: 1,
} as const);

type ExtraordinaryEventAdjustmentMethod =
    (typeof ExtraordinaryEventAdjustmentMethod)[keyof typeof ExtraordinaryEventAdjustmentMethod];
