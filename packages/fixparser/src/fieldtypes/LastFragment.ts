/**
 * Indicates whether this message is the last in a sequence of messages for those messages that support fragmentation, such as Allocation Instruction, Mass Quote, Security List, Derivative Security List
 * - Tag: 893
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const LastFragment = Object.freeze({
    /** Not Last Message */
    NotLastMessage: 'N',
    /** Last Message */
    LastMessage: 'Y',
} as const);

type LastFragment = (typeof LastFragment)[keyof typeof LastFragment];
