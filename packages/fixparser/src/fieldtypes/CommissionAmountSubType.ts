/**
 * Further sub classification of the CommissionAmountType(2641).
 * - Tag: 2725
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CommissionAmountSubType = Object.freeze({
    /** Research payment account (RPA) */
    ResearchPaymentAccount: 0,
    /** Commission sharing agreement (CSA) */
    CommissionSharingAgreement: 1,
    OtherTypeResearchPayment: 2,
} as const);

type CommissionAmountSubType = (typeof CommissionAmountSubType)[keyof typeof CommissionAmountSubType];
