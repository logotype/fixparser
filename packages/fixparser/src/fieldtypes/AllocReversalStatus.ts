/**
 * Identifies the status of a reversal transaction.
 * - Tag: 1738
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocReversalStatus = Object.freeze({
    /** Completed */
    Completed: 0,
    /** Refused */
    Refused: 1,
    /** Cancelled */
    Cancelled: 2,
} as const);

type AllocReversalStatus = (typeof AllocReversalStatus)[keyof typeof AllocReversalStatus];
