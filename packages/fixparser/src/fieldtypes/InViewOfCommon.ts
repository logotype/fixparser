/**
 * Indicates whether or not the halt was due to Common Stock trading being halted.
 * - Tag: 328
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const InViewOfCommon = Object.freeze({
    /** Halt was not related to a halt of the common stock */
    HaltWasNotRelatedToAHaltOfTheCommonStock: 'N',
    /** Halt was due to common stock being halted */
    HaltWasDueToCommonStockBeingHalted: 'Y',
} as const);

type InViewOfCommon = (typeof InViewOfCommon)[keyof typeof InViewOfCommon];
