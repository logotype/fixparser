/**
 * OrderHandlingInstSource
 * - Tag: 1032
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OrderHandlingInstSource = Object.freeze({
    /** FINRA OATS */
    FINRAOATS: 1,
    /** FIA Execution Source Code */
    FIAExecutionSourceCode: 2,
} as const);

type OrderHandlingInstSource = (typeof OrderHandlingInstSource)[keyof typeof OrderHandlingInstSource];
