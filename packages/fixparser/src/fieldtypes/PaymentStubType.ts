/**
 * Stub type.
 * - Tag: 40873
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStubType = Object.freeze({
    /** Initial */
    Initial: 0,
    /** Final */
    Final: 1,
    /** Compounding initial */
    CompoundingInitial: 2,
    /** Compounding final */
    CompoundingFinal: 3,
} as const);

type PaymentStubType = (typeof PaymentStubType)[keyof typeof PaymentStubType];
