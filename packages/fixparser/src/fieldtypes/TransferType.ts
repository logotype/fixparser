/**
 * Indicates the type of transfer request.
 * - Tag: 2440
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferType = Object.freeze({
    /** Request transfer */
    RequestTransfer: 0,
    /** Accept transfer */
    AcceptTransfer: 1,
    /** Decline transfer */
    DeclineTransfer: 2,
} as const);

type TransferType = (typeof TransferType)[keyof typeof TransferType];
