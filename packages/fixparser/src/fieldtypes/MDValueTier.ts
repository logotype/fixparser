/**
 * MDValueTier
 * - Tag: 2711
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDValueTier = Object.freeze({
    /** Range 1 */
    Range1: 1,
    /** Range 2 */
    Range2: 2,
    /** Range 3 */
    Range3: 3,
} as const);

type MDValueTier = (typeof MDValueTier)[keyof typeof MDValueTier];
