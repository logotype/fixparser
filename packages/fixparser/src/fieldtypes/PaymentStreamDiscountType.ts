/**
 * The method of calculating discounted payment amounts
 * - Tag: 40744
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamDiscountType = Object.freeze({
    /** Standard */
    Standard: 0,
    /** Forward Rate Agreement (FRA) */
    FRA: 1,
} as const);

type PaymentStreamDiscountType = (typeof PaymentStreamDiscountType)[keyof typeof PaymentStreamDiscountType];
