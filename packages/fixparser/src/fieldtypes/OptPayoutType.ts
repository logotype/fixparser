/**
 * Indicates the type of valuation method or payout trigger for an in-the-money option.
 * - Tag: 1482
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const OptPayoutType = Object.freeze({
    /** Vanilla */
    Vanilla: 1,
    /** Capped */
    Capped: 2,
    /** Digital (Binary) */
    Binary: 3,
    /** Asian */
    Asian: 4,
    /** Barrier */
    Barrier: 5,
    /** Digital Barrier */
    DigitalBarrier: 6,
    /** Lookback */
    Lookback: 7,
    /** Other path dependent */
    OtherPathDependent: 8,
    /** Other */
    Other: 99,
} as const);

type OptPayoutType = (typeof OptPayoutType)[keyof typeof OptPayoutType];
