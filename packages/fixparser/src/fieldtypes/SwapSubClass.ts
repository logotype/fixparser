/**
 * The sub-classification or notional schedule type of the swap.
 * - Tag: 1575
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SwapSubClass = Object.freeze({
    /** Amortizing notional schedule */
    Amortizing: 'AMTZ',
    /** Compounding */
    Compounding: 'COMP',
    /** Constant notional schedule */
    ConstantNotionalSchedule: 'CNST',
    /** Accreting notional schedule */
    AccretingNotionalSchedule: 'ACRT',
    /** Custom notional schedule */
    CustomNotionalSchedule: 'CUST',
} as const);

type SwapSubClass = (typeof SwapSubClass)[keyof typeof SwapSubClass];
