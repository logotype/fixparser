/**
 * TradingSessionID
 * - Tag: 336
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const TradingSessionID = Object.freeze({
    /** Day */
    Day: '1',
    /** HalfDay */
    HalfDay: '2',
    /** Morning */
    Morning: '3',
    /** Afternoon */
    Afternoon: '4',
    /** Evening */
    Evening: '5',
    /** After-hours */
    AfterHours: '6',
    /** Holiday */
    Holiday: '7',
} as const);

type TradingSessionID = (typeof TradingSessionID)[keyof typeof TradingSessionID];
