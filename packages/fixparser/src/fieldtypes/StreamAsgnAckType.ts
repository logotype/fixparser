/**
 * Type of acknowledgement.
 * - Tag: 1503
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StreamAsgnAckType = Object.freeze({
    /** Assignment Accepted */
    AssignmentAccepted: 0,
    /** Assignment Rejected */
    AssignmentRejected: 1,
} as const);

type StreamAsgnAckType = (typeof StreamAsgnAckType)[keyof typeof StreamAsgnAckType];
