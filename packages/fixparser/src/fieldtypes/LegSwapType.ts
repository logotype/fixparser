/**
 * For Fixed Income, used instead of LegOrderQty(685) to requests the respondent to calculate the quantity based on the quantity on the opposite side of the swap.
 * - Tag: 690
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LegSwapType = Object.freeze({
    /** Par For Par */
    ParForPar: 1,
    /** Modified Duration */
    ModifiedDuration: 2,
    /** Risk */
    Risk: 4,
    /** Proceeds */
    Proceeds: 5,
} as const);

type LegSwapType = (typeof LegSwapType)[keyof typeof LegSwapType];
