/**
 * Identifies the type of Confirmation message being sent.
 * - Tag: 773
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ConfirmType = Object.freeze({
    /** Status */
    Status: 1,
    /** Confirmation */
    Confirmation: 2,
    /** Confirmation Request Rejected (reason can be stated in Text (58) field) */
    ConfirmationRequestRejected: 3,
} as const);

type ConfirmType = (typeof ConfirmType)[keyof typeof ConfirmType];
