/**
 * Specifies the basis or unit used to calculate the total commission based on the rate.
 * - Tag: 13
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const CommType = Object.freeze({
    PerUnit: '1',
    /** Percent */
    Percent: '2',
    Absolute: '3',
    PercentageWaivedCashDiscount: '4',
    PercentageWaivedEnhancedUnits: '5',
    PointsPerBondOrContract: '6',
    BasisPoints: '7',
    AmountPerContract: '8',
} as const);

type CommType = (typeof CommType)[keyof typeof CommType];
