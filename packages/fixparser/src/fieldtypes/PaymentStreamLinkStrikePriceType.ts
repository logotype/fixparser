/**
 * For a variance swap specifies how PaymentStreamLinkStrikePrice(42673) is expressed.
 * - Tag: 42674
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamLinkStrikePriceType = Object.freeze({
    /** Volatility */
    Volatility: 0,
    /** Variance */
    Variance: 1,
} as const);

type PaymentStreamLinkStrikePriceType =
    (typeof PaymentStreamLinkStrikePriceType)[keyof typeof PaymentStreamLinkStrikePriceType];
