/**
 * Defines the unit for a miscellaneous fee.
 * - Tag: 891
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MiscFeeBasis = Object.freeze({
    Absolute: 0,
    PerUnit: 1,
    Percentage: 2,
} as const);

type MiscFeeBasis = (typeof MiscFeeBasis)[keyof typeof MiscFeeBasis];
