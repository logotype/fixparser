/**
 * Specifies the consequences of bullion settlement disruption events.
 * - Tag: 2143
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SettlDisruptionProvision = Object.freeze({
    /** Negotiation */
    Negotiation: 1,
    /** Cancellation and payment */
    Cancellation: 2,
} as const);

type SettlDisruptionProvision = (typeof SettlDisruptionProvision)[keyof typeof SettlDisruptionProvision];
