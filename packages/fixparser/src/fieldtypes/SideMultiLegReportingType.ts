/**
 * Used to indicate if the side being reported on Trade Capture Report represents a leg of a multileg instrument or a single security.
 * - Tag: 752
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const SideMultiLegReportingType = Object.freeze({
    /** Single Security (default if not specified) */
    SingleSecurity: 1,
    /** Individual leg of a multileg security */
    IndividualLegOfAMultilegSecurity: 2,
    /** Multileg Security */
    MultilegSecurity: 3,
} as const);

type SideMultiLegReportingType = (typeof SideMultiLegReportingType)[keyof typeof SideMultiLegReportingType];
