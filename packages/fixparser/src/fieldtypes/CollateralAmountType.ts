/**
 * The type of value in CurrentCollateralAmount(1704).
 * - Tag: 2632
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CollateralAmountType = Object.freeze({
    /** Market valuation (the default) */
    MarketValuation: 0,
    /** Portfolio value before processing pledge request */
    PortfolioValue: 1,
    /** Value confirmed as "locked-up" for processing a pledge request */
    ValueConfirmed: 2,
    /** Credit value of collateral at CCP processing a pledge request */
    CollateralCreditValue: 3,
    AdditionalCollateralValue: 4,
    EstimatedMarketValuation: 5,
} as const);

type CollateralAmountType = (typeof CollateralAmountType)[keyof typeof CollateralAmountType];
