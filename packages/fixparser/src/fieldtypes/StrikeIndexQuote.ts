/**
 * The quote side from which the index price is to be determined.
 * - Tag: 2601
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StrikeIndexQuote = Object.freeze({
    Bid: 0,
    /** Mid */
    Mid: 1,
    /** Offer */
    Offer: 2,
} as const);

type StrikeIndexQuote = (typeof StrikeIndexQuote)[keyof typeof StrikeIndexQuote];
