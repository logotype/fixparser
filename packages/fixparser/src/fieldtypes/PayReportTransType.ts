/**
 * Identifies the message transaction type.
 * - Tag: 2804
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PayReportTransType = Object.freeze({
    /** New */
    New: 0,
    /** Replace */
    Replace: 1,
    Status: 2,
} as const);

type PayReportTransType = (typeof PayReportTransType)[keyof typeof PayReportTransType];
