/**
 * Method by which short positions are assigned to an exercise notice during exercise and assignment processing
 * - Tag: 744
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const AssignmentMethod = Object.freeze({
    /** Pro rata */
    ProRata: 'P',
    /** Random */
    Random: 'R',
} as const);

type AssignmentMethod = (typeof AssignmentMethod)[keyof typeof AssignmentMethod];
