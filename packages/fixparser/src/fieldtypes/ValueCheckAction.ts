/**
 * Action to be taken for the ValueCheckType(1869).
 * - Tag: 1870
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ValueCheckAction = Object.freeze({
    DoNotCheck: 0,
    Check: 1,
    BestEffort: 2,
} as const);

type ValueCheckAction = (typeof ValueCheckAction)[keyof typeof ValueCheckAction];
