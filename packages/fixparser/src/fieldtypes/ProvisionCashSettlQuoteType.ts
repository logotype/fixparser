/**
 * Identifies the type of quote to be used.
 * - Tag: 40111
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionCashSettlQuoteType = Object.freeze({
    /** Bid */
    Bid: 0,
    /** Mid */
    Mid: 1,
    /** Offer */
    Offer: 2,
    ExercisingPartyPays: 3,
} as const);

type ProvisionCashSettlQuoteType = (typeof ProvisionCashSettlQuoteType)[keyof typeof ProvisionCashSettlQuoteType];
