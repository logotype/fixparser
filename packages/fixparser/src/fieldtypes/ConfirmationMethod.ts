/**
 * Specifies how a trade was confirmed.
 * - Tag: 1927
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ConfirmationMethod = Object.freeze({
    /** Non-electronic */
    NonElectronic: 0,
    /** Electronic */
    Electronic: 1,
    /** Unconfirmed */
    Unconfirmed: 2,
} as const);

type ConfirmationMethod = (typeof ConfirmationMethod)[keyof typeof ConfirmationMethod];
