/**
 * Indicates whether the resulting position after a trade should be an opening position or closing position. Used for omnibus accounting - where accounts are held on a gross basis instead of being netted together.
 * - Tag: 77
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PositionEffect = Object.freeze({
    /** Close */
    Close: 'C',
    /** FIFO */
    FIFO: 'F',
    /** Open */
    Open: 'O',
    /** Rolled */
    Rolled: 'R',
    /** Close but notify on open */
    CloseButNotifyOnOpen: 'N',
    /** Default */
    Default: 'D',
} as const);

type PositionEffect = (typeof PositionEffect)[keyof typeof PositionEffect];
