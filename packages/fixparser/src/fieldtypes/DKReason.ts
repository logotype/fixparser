/**
 * Reason for execution rejection.
 * - Tag: 127
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const DKReason = Object.freeze({
    /** Unknown security */
    UnknownSymbol: 'A',
    /** Wrong side */
    WrongSide: 'B',
    /** Quantity exceeds order */
    QuantityExceedsOrder: 'C',
    /** No matching order */
    NoMatchingOrder: 'D',
    /** Price exceeds limit */
    PriceExceedsLimit: 'E',
    /** Calculation difference */
    CalculationDifference: 'F',
    /** No matching ExecutionReport(35=8) */
    NoMatchingExecutionReport: 'G',
    /** Other */
    Other: 'Z',
} as const);

type DKReason = (typeof DKReason)[keyof typeof DKReason];
