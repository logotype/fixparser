/**
 * Specifies the side of the relative value.
 * - Tag: 2532
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelativeValueSide = Object.freeze({
    /** Bid */
    Bid: 1,
    /** Mid */
    Mid: 2,
    /** Offer */
    Offer: 3,
} as const);

type RelativeValueSide = (typeof RelativeValueSide)[keyof typeof RelativeValueSide];
