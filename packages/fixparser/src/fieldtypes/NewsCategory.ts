/**
 * Category of news message.
 * - Tag: 1473
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NewsCategory = Object.freeze({
    /** Company News */
    CompanyNews: 0,
    /** Marketplace News */
    MarketplaceNews: 1,
    /** Financial Market News */
    FinancialMarketNews: 2,
    /** Technical News */
    TechnicalNews: 3,
    /** Other News */
    OtherNews: 99,
} as const);

type NewsCategory = (typeof NewsCategory)[keyof typeof NewsCategory];
