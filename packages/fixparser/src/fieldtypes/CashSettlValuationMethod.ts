/**
 * CashSettlValuationMethod
 * - Tag: 40038
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const CashSettlValuationMethod = Object.freeze({
    /** Market */
    Market: 0,
    /** Highest */
    Highest: 1,
    /** Average market */
    AverageMarket: 2,
    /** Average highest */
    AverageHighest: 3,
    /** Blended market */
    BlendedMarket: 4,
    /** Blended highest */
    BlendedHighest: 5,
    /** Average blended market */
    AverageBlendedMarket: 6,
    /** Average blended highest */
    AverageBlendedHighest: 7,
} as const);

type CashSettlValuationMethod = (typeof CashSettlValuationMethod)[keyof typeof CashSettlValuationMethod];
