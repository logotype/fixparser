/**
 * Indicates the type of entity who initiated an event, e.g. modification or cancellation of an order or quote.
 * - Tag: 2830
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const EventInitiatorType = Object.freeze({
    /** Customer or client */
    CustomerOrClient: 'C',
    /** Exchange or execution venue */
    ExchangeOrExecutionVenue: 'E',
    /** Firm or broker */
    FirmOrBroker: 'F',
} as const);

type EventInitiatorType = (typeof EventInitiatorType)[keyof typeof EventInitiatorType];
