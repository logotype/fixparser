/**
 * Indicates if a trade should be reported via a market reporting service.
 * - Tag: 852
 * - FIX Specification type: Boolean
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PublishTrdIndicator = Object.freeze({
    /** Do Not Report Trade */
    DoNotReportTrade: 'N',
    /** Report Trade */
    ReportTrade: 'Y',
} as const);

type PublishTrdIndicator = (typeof PublishTrdIndicator)[keyof typeof PublishTrdIndicator];
