/**
 * Specifies the type of respondents requested.
 * - Tag: 1172
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RespondentType = Object.freeze({
    /** All market participants */
    AllMarketParticipants: 1,
    /** Specified market participants */
    SpecifiedMarketParticipants: 2,
    /** All Market Makers */
    AllMarketMakers: 3,
    /** Primary Market Maker(s) */
    PrimaryMarketMaker: 4,
} as const);

type RespondentType = (typeof RespondentType)[keyof typeof RespondentType];
