/**
 * Ratios between various entities.
 * - Tag: 2472
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDStatisticRatioType = Object.freeze({
    BuyersToSellers: 1,
    UpticksToDownticks: 2,
    MarketMakerToNonMarketMaker: 3,
    AutomatedToNonAutomated: 4,
    OrdersToTrades: 5,
    QuotesToTrades: 6,
    OrdersAndQuotesToTrades: 7,
    FailedToTotalTradedValue: 8,
    BenefitsToTotalTradedValue: 9,
    FeesToTotalTradedValue: 10,
    TradeVolumeToTotalTradedVolume: 11,
    OrdersToTotalNumberOrders: 12,
} as const);

type MDStatisticRatioType = (typeof MDStatisticRatioType)[keyof typeof MDStatisticRatioType];
