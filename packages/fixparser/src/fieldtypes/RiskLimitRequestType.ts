/**
 * Type of risk limit information.
 * - Tag: 1760
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RiskLimitRequestType = Object.freeze({
    /** Definitions(Default) */
    Definitions: 1,
    /** Utilization */
    Utilization: 2,
    /** Definitions and utilization */
    DefinitionsAndUtilizations: 3,
} as const);

type RiskLimitRequestType = (typeof RiskLimitRequestType)[keyof typeof RiskLimitRequestType];
