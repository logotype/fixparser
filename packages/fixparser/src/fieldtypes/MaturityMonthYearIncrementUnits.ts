/**
 * Unit of measure for the Maturity Month Year Increment
 * - Tag: 1302
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MaturityMonthYearIncrementUnits = Object.freeze({
    /** Months */
    Months: 0,
    /** Days */
    Days: 1,
    /** Weeks */
    Weeks: 2,
    /** Years */
    Years: 3,
} as const);

type MaturityMonthYearIncrementUnits =
    (typeof MaturityMonthYearIncrementUnits)[keyof typeof MaturityMonthYearIncrementUnits];
