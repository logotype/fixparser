/**
 * Type of fee elected for the break provision.
 * - Tag: 42707
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ProvisionBreakFeeElection = Object.freeze({
    /** Flat fee */
    FlatFee: 0,
    /** Amortized fee */
    AmortizedFee: 1,
    /** Funding fee */
    FundingFee: 2,
    /** Flat fee and funding fee */
    FlatAndFundingFee: 3,
    /** Amortized fee and funding fee */
    AmortizedAndFundingFee: 4,
} as const);

type ProvisionBreakFeeElection = (typeof ProvisionBreakFeeElection)[keyof typeof ProvisionBreakFeeElection];
