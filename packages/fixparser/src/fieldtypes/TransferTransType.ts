/**
 * Indicates the type of transfer transaction.
 * - Tag: 2439
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TransferTransType = Object.freeze({
    /** New */
    New: 0,
    /** Replace */
    Replace: 1,
    /** Cancel */
    Cancel: 2,
} as const);

type TransferTransType = (typeof TransferTransType)[keyof typeof TransferTransType];
