/**
 * Type of value to be checked.
 * - Tag: 1869
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ValueCheckType = Object.freeze({
    PriceCheck: 1,
    NotionalValueCheck: 2,
    QuantityCheck: 3,
} as const);

type ValueCheckType = (typeof ValueCheckType)[keyof typeof ValueCheckType];
