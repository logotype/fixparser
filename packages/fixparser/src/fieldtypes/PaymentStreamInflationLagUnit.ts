/**
 * Time unit associated with the inflation lag period.
 * - Tag: 40809
 * - FIX Specification type: String
 * - Mapped type: string
 * @readonly
 * @public
 */
export const PaymentStreamInflationLagUnit = Object.freeze({
    /** Day */
    Day: 'D',
    /** Week */
    Week: 'Wk',
    /** Month */
    Month: 'Mo',
    /** Year */
    Year: 'Yr',
} as const);

type PaymentStreamInflationLagUnit = (typeof PaymentStreamInflationLagUnit)[keyof typeof PaymentStreamInflationLagUnit];
