/**
 * NetworkRequestType
 * - Tag: 935
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NetworkRequestType = Object.freeze({
    /** Snapshot */
    Snapshot: 1,
    /** Subscribe */
    Subscribe: 2,
    /** Stop Subscribing */
    StopSubscribing: 4,
    /** Level of Detail, then NoCompID's becomes required */
    LevelOfDetail: 8,
} as const);

type NetworkRequestType = (typeof NetworkRequestType)[keyof typeof NetworkRequestType];
