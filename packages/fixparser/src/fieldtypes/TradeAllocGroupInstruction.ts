/**
 * Instruction on how to add a trade to an allocation group when it is being given-up.
 * - Tag: 1848
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeAllocGroupInstruction = Object.freeze({
    /** Add to an existing allocation group if one exists. */
    Add: 0,
    /** Do not add the trade to an allocation group. */
    DoNotAdd: 1,
} as const);

type TradeAllocGroupInstruction = (typeof TradeAllocGroupInstruction)[keyof typeof TradeAllocGroupInstruction];
