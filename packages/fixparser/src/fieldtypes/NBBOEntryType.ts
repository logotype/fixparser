/**
 * Type of NBBO information.
 * - Tag: 2831
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NBBOEntryType = Object.freeze({
    Bid: 0,
    Offer: 1,
    /** Mid-price */
    MidPrice: 2,
} as const);

type NBBOEntryType = (typeof NBBOEntryType)[keyof typeof NBBOEntryType];
