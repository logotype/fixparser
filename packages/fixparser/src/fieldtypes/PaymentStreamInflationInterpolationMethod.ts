/**
 * The method used when calculating the Inflation Index Level from multiple points - the most common is Linear.
 * - Tag: 40811
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PaymentStreamInflationInterpolationMethod = Object.freeze({
    /** None */
    None: 0,
    /** Linear zero yield */
    LinearZeroYield: 1,
} as const);

type PaymentStreamInflationInterpolationMethod =
    (typeof PaymentStreamInflationInterpolationMethod)[keyof typeof PaymentStreamInflationInterpolationMethod];
