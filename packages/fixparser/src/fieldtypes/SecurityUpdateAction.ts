/**
 * Specifies the action taken or to be taken for the specified instrument or list of instruments.
 * - Tag: 980
 * - FIX Specification type: char
 * - Mapped type: string
 * @readonly
 * @public
 */
export const SecurityUpdateAction = Object.freeze({
    /** Add */
    Add: 'A',
    /** Delete */
    Delete: 'D',
    /** Modify */
    Modify: 'M',
} as const);

type SecurityUpdateAction = (typeof SecurityUpdateAction)[keyof typeof SecurityUpdateAction];
