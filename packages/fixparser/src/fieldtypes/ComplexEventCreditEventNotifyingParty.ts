/**
 * The notifying party is the party that notifies the other party when a credit event has occurred by means of a credit event notice. If more than one party is referenced as being the notifying party then either party may notify the other of a credit event occurring.
 * - Tag: 2134
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const ComplexEventCreditEventNotifyingParty = Object.freeze({
    /** Seller notifies */
    SellerNotifies: 0,
    /** Buyer notifies */
    BuyerNotifies: 1,
    /** Seller or buyer notifies */
    SellerOrBuyerNotifies: 2,
} as const);

type ComplexEventCreditEventNotifyingParty =
    (typeof ComplexEventCreditEventNotifyingParty)[keyof typeof ComplexEventCreditEventNotifyingParty];
