/**
 * Level of Response requested from receiver of quote messages. A default value should be bilaterally agreed.
 * - Tag: 301
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteResponseLevel = Object.freeze({
    /** No Acknowledgement */
    NoAcknowledgement: 0,
    /** Acknowledge only negative or erroneous quotes */
    AcknowledgeOnlyNegativeOrErroneousQuotes: 1,
    /** Acknowledge each quote message */
    AcknowledgeEachQuoteMessage: 2,
    /** Summary Acknowledgement */
    SummaryAcknowledgement: 3,
} as const);

type QuoteResponseLevel = (typeof QuoteResponseLevel)[keyof typeof QuoteResponseLevel];
