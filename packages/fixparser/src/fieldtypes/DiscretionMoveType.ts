/**
 * Describes whether discretionay price is static or floats
 * - Tag: 841
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DiscretionMoveType = Object.freeze({
    /** Floating (default) */
    Floating: 0,
    /** Fixed */
    Fixed: 1,
} as const);

type DiscretionMoveType = (typeof DiscretionMoveType)[keyof typeof DiscretionMoveType];
