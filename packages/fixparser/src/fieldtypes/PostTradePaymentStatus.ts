/**
 * Used to indicate the status of a post-trade payment.
 * - Tag: 2823
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const PostTradePaymentStatus = Object.freeze({
    New: 0,
    Initiated: 1,
    Pending: 2,
    Confirmed: 3,
    Rejected: 4,
} as const);

type PostTradePaymentStatus = (typeof PostTradePaymentStatus)[keyof typeof PostTradePaymentStatus];
