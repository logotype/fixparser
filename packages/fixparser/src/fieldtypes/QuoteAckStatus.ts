/**
 * Acknowledgement status of a Quote(35=S) or QuoteCancel(35=Z) message submission.
 * - Tag: 1865
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const QuoteAckStatus = Object.freeze({
    /** Received, not yet processed */
    ReceivedNotYetProcessed: 0,
    /** Accepted */
    Accepted: 1,
    /** Rejected */
    Rejected: 2,
} as const);

type QuoteAckStatus = (typeof QuoteAckStatus)[keyof typeof QuoteAckStatus];
