/**
 * Describes the source of the identifier that RelatedPositionID(1862) represents.
 * - Tag: 1863
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const RelatedPositionIDSource = Object.freeze({
    /** Position maintenance report ID - PosMaintRptID(721) */
    PosMaintRptID: 1,
    /** Position transfer ID - TransferID(2437) */
    TransferID: 2,
    /** Position entity ID - PositionID(2618) */
    PositionEntityID: 3,
} as const);

type RelatedPositionIDSource = (typeof RelatedPositionIDSource)[keyof typeof RelatedPositionIDSource];
