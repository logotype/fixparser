/**
 * Type of statistical value.
 * - Tag: 2479
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const MDStatisticValueType = Object.freeze({
    /** Absolute */
    Absolute: 1,
    /** Percentage */
    Percentage: 2,
} as const);

type MDStatisticValueType = (typeof MDStatisticValueType)[keyof typeof MDStatisticValueType];
