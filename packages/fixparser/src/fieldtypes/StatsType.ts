/**
 * Type of statistics
 * - Tag: 1176
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const StatsType = Object.freeze({
    /** Exchange Last */
    ExchangeLast: 1,
    /** High / Low Price */
    High: 2,
    /** Average Price (VWAP, TWAP ... ) */
    AveragePrice: 3,
    /** Turnover (Price * Qty) */
    Turnover: 4,
} as const);

type StatsType = (typeof StatsType)[keyof typeof StatsType];
