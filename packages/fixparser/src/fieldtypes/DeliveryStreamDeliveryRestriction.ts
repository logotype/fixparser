/**
 * Specifies under what conditions the buyer and seller should be excused of their delivery obligations.
 * - Tag: 41063
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const DeliveryStreamDeliveryRestriction = Object.freeze({
    Firm: 1,
    NonFirm: 2,
    ForceMajeure: 3,
    SystemFirm: 4,
    UnitFirm: 5,
} as const);

type DeliveryStreamDeliveryRestriction =
    (typeof DeliveryStreamDeliveryRestriction)[keyof typeof DeliveryStreamDeliveryRestriction];
