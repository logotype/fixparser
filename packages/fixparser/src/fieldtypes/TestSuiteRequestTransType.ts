/**
 * Identifies the message transaction type.
 * - Tag: 3064
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TestSuiteRequestTransType = Object.freeze({
    /** New */
    New: 0,
    /** Cancel */
    Cancel: 1,
} as const);

type TestSuiteRequestTransType = (typeof TestSuiteRequestTransType)[keyof typeof TestSuiteRequestTransType];
