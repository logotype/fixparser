/**
 * Response to allocation to be communicated to a counterparty through an intermediary, i.e. clearing house. Used in conjunction with AllocType = "Request to Intermediary" and AllocReportType = "Request to Intermediary"
 * - Tag: 808
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const AllocIntermedReqType = Object.freeze({
    /** Pending Accept */
    PendingAccept: 1,
    /** Pending Release */
    PendingRelease: 2,
    /** Pending Reversal */
    PendingReversal: 3,
    /** Accept */
    Accept: 4,
    /** Block Level Reject */
    BlockLevelReject: 5,
    /** Account Level Reject */
    AccountLevelReject: 6,
} as const);

type AllocIntermedReqType = (typeof AllocIntermedReqType)[keyof typeof AllocIntermedReqType];
