/**
 * Indicates the type of Network Response Message.
 * - Tag: 937
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const NetworkStatusResponseType = Object.freeze({
    /** Full */
    Full: 1,
    /** Incremental Update */
    IncrementalUpdate: 2,
} as const);

type NetworkStatusResponseType = (typeof NetworkStatusResponseType)[keyof typeof NetworkStatusResponseType];
