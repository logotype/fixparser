/**
 * Code to identify whether to book out executions on a part-filled GT order on the day of execution or to accumulate.
 * - Tag: 427
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const GTBookingInst = Object.freeze({
    /** Book out all trades on day of execution */
    BookOutAllTradesOnDayOfExecution: 0,
    /** Accumulate executions until order is filled or expires */
    AccumulateUntilFilledOrExpired: 1,
    /** Accumulate until verbally notified otherwise */
    AccumulateUntilVerballyNotifiedOtherwise: 2,
} as const);

type GTBookingInst = (typeof GTBookingInst)[keyof typeof GTBookingInst];
