/**
 * Identifies the type of limit amount expressed in LastLimitAmt(1632) and LimitAmtRemaining(1633).
 * - Tag: 1631
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const LimitAmtType = Object.freeze({
    /** Credit limit */
    CreditLimit: 0,
    /** Gross position limit */
    GrossPositionLimit: 1,
    /** Net position limit */
    NetPositionLimit: 2,
    /** Risk exposure limit */
    RiskExposureLimit: 3,
    /** Long position limit */
    LongPositionLimit: 4,
    /** Short position limit */
    ShortPositionLimit: 5,
} as const);

type LimitAmtType = (typeof LimitAmtType)[keyof typeof LimitAmtType];
