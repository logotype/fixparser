/**
 * Reason for trade aggregation request being rejected.
 * - Tag: 2791
 * - FIX Specification type: int
 * - Mapped type: number
 * @readonly
 * @public
 */
export const TradeAggregationRejectReason = Object.freeze({
    /** Unknown order(s) */
    UnknownOrders: 0,
    /** Unknown execution/fill(s) */
    UnknownExecutionFills: 1,
    /** Other */
    Other: 99,
} as const);

type TradeAggregationRejectReason = (typeof TradeAggregationRejectReason)[keyof typeof TradeAggregationRejectReason];
