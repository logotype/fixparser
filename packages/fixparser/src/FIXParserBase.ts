import type { ConnectionOptions, KeyObject, TlsOptions } from 'node:tls';
import type { ProxyAgent } from 'proxy-agent';

import type { IMessageStore } from 'fixparser-common';
import { Enums as EnumsCache } from './enums/Enums';
import { Field } from './fields/Field';
import { Fields as FieldsCache } from './fields/Fields';
import { Field as FieldType } from './fieldtypes/Field';
import type { LogOptions } from './logger/Logger';
import { Message } from './message/Message';
import { DEFAULT_FIX_VERSION, RE_ESCAPE, RE_FIND, SOH, STRING_EQUALS } from './util/util';

/**
 * Connection transport type.
 * @public
 */
export type Protocol = 'tcp' | 'ssl-tcp' | 'tls-tcp' | 'websocket';
/**
 * Type of connection (client or server).
 * @public
 */
export type ConnectionType = 'acceptor' | 'initiator';

/**
 * Represents the configuration options for initializing the instance.
 *
 * @typedef {Object} BaseOptions
 * @property {boolean} [skipValidation=false] - Whether to skip validation of FIX messages. Defaults to `false`.
 * @property {string} [fixVersion="FIXT.1.1"] - The FIX protocol version to use. Defaults to `"FIXT.1.1"`, which is typically used by the BeginString tag (tag 8).
 * @property {boolean} [logging=true] - Whether to enable or disable logging. Defaults to `true`.
 * @property {LogOptions} [logOptions] - Options for customizing logging behavior. If not provided, no specific log configuration is applied.
 */
export type BaseOptions = {
    /* Skip validation of fix messages */
    skipValidation?: boolean;
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    fixVersion?: string;
    /* Disable logs */
    logging?: boolean;
    /* Log options */
    logOptions?: LogOptions;
};

/**
 * FIXParser initialization options.
 * @public
 */
export type Options = {
    /* Target hostname or IP address */
    host?: string;
    /* Target port */
    port?: number;
    /* Type of protocol */
    protocol?: Protocol;
    /* Assigned value used to identify firm sending message, SenderCompID tag 49 */
    sender?: string;
    /* Assigned value used to identify receiving firm, TargetCompID tag 56 */
    target?: string;
    /* Heartbeat interval in seconds */
    heartbeatIntervalSeconds?: number;
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    fixVersion?: string;
    /* Message buffer of incoming messages, defaults to built-in MessageBuffer() */
    messageStoreIn?: IMessageStore<Message>;
    /* Message buffer of outgoing messages, defaults to built-in MessageBuffer() */
    messageStoreOut?: IMessageStore<Message>;
    /* Skip Standard Input pipe of TLS socket */
    tlsSkipStdInPipe?: boolean;
    /* TLS Connection Options */
    tlsOptions?: ConnectionOptions | TlsOptions;
    /* Disable logs */
    logging?: boolean;
    /* Log options */
    logOptions?: LogOptions;
    /* Use a proxy connection */
    proxy?: ProxyAgent;
    /* Skip validation of fix messages */
    skipValidation?: boolean;
    /* Called when the FIX connection is opened and ready */
    onReady?: () => void;
    /* Called when a message has been received */
    onMessage?: (message: Message) => void;
    /* Called when the FIX connection has been initiated */
    onOpen?: () => void;
    /* Called when the FIX connection has been closed */
    onClose?: () => void;
    /* Called if the FIX connection has failed */
    onError?: (error?: Error) => void;
};

/**
 * Base class responsible for parsing and processing FIX messages.
 *
 * This class handles parsing of FIX messages, processing message fields and tags,
 * and performing validation on specific fields such as the body length, checksum,
 * and protocol version. It provides methods to process raw message data, extract
 * tag-value pairs, and validate the structure and content of FIX messages.
 */
export class FIXParserBase {
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    public fixVersion: string = DEFAULT_FIX_VERSION;
    /* Current Message instance being processed */
    public message: Message | undefined = undefined;
    /* Current FIX message tag=value pair */
    public messageTags: string[] = [];
    /* Current FIX message in string format */
    public messageString = '';
    /* Collection of field values from the FIX specification */
    public fields: FieldsCache = new FieldsCache();
    /* Collection of enum values from the FIX specification */
    public enums: EnumsCache = new EnumsCache();
    /* Proxy */
    public proxy?: ProxyAgent = undefined;
    /* TLS Private keys in PEM format */
    public tlsKey?: string | Buffer | Array<string | Buffer | KeyObject>;
    /* TLS Certificate chains in PEM format */
    public tlsCert?: string | Buffer | Array<string | Buffer>;
    /* TLS Use Server Name Indication extension (requires FQDN hostname) */
    public tlsUseSNI?: boolean;
    /* TLS Skip Standard Input pipe of socket */
    public tlsSkipStdInPipe?: boolean;
    /* Skip validation of fix messages */
    public skipValidation?: boolean;

    /**
     * Processes the message string by searching for a specific pattern and replacing occurrences
     * of a delimiter with a special character (SOH). It then sets the processed string into the
     * message object and splits the string into tags for further processing.
     *
     * - Uses a regular expression to find specific patterns in the message string.
     * - Replaces occurrences of the matched pattern with a delimiter (SOH).
     * - Sets the processed message string into the `message` object and splits it into `messageTags`.
     * - If the message format is invalid, the method resets the `message` and clears the `messageTags`.
     *
     * @returns {void}
     */
    public processMessage(): void {
        const matches: RegExpExecArray | null = RE_FIND.exec(this.messageString);
        if (matches && matches.length === 2) {
            const stringData: string = this.messageString.replace(
                new RegExp(matches[1].replace(RE_ESCAPE, '\\$&'), 'g'),
                SOH,
            );
            if (this.message) {
                this.message.messageString = stringData;
            }
            this.messageTags = stringData.split(SOH);
        } else {
            this.message = undefined;
            this.messageTags = [];
        }
    }

    /**
     * Processes the fields of the message by iterating over the tags and values, extracting
     * the tag-value pairs, and performing validation or other processing for specific fields.
     *
     * - Each tag-value pair is extracted from `messageTags` and converted into a `Field` object.
     * - The field is processed by both `fields.processField()` and `enums.processEnum()` methods.
     * - Special handling is applied to certain fields such as `BeginString`, `BodyLength`, and `CheckSum`.
     * - Validates the body length and checksum for the message, and updates the `fixVersion` based on the `BeginString` field.
     * - Adds each processed field to the `message` object for further processing or storage.
     *
     * @returns {void}
     */
    public processFields(): void {
        let tag: number;
        let value: string | number | undefined;
        let i = 0;
        let equalsOperator: number;
        let field: Field;

        for (i; i < this.messageTags.length - 1; i++) {
            equalsOperator = this.messageTags[i].indexOf(STRING_EQUALS);

            tag = Number(this.messageTags[i].substring(0, equalsOperator));
            value = this.messageTags[i].substring(equalsOperator + 1);

            field = new Field(tag, value);

            this.fields.processField(this.message!, field);
            this.enums.processEnum(field);

            if (field.tag === FieldType.BeginString) {
                this.message!.fixVersion = String(field.value);
            } else if (field.tag === FieldType.BodyLength) {
                this.message?.validateBodyLength(value);
            } else if (field.tag === FieldType.CheckSum) {
                this.message?.validateChecksum(value);
            }

            this.message?.addField(field);
        }
    }

    /**
     * Parses a string of data into an array of `Message` objects by splitting the input data
     * into individual FIX messages and processing each message's fields and tags.
     *
     * The data is split using a regular expression to identify FIX message boundaries, specifically
     * looking for the `8=FIX` marker. Each resulting message string is processed, and its fields
     * are extracted and validated.
     *
     * - If the message string contains the `SOH` character, it is directly processed and split into tags.
     * - If `SOH` is absent, the message is processed using the `processMessage()` method.
     * - After processing, each `Message` object is populated with fields and added to the result array.
     *
     * @param {string} data - The raw data string containing one or more FIX messages.
     * @returns {Message[]} An array of `Message` objects parsed from the input string.
     */
    public parse(data: string): Message[] {
        let i = 0;

        const messageStrings: string[] = data ? data.split(/(?<!\d)8=FIX/) : [];
        const messages: Message[] = [];

        for (i; i < messageStrings.length; i++) {
            this.message = new Message(this.fixVersion);
            this.messageString = `8=FIX${messageStrings[i]}`;
            if (this.messageString.indexOf(SOH) > -1) {
                this.message.messageString = this.messageString;
                this.messageTags = this.messageString.split(SOH);
            } else {
                this.processMessage();
            }

            if (this.message) {
                if (!this.skipValidation) {
                    this.processFields();
                }
                messages.push(this.message);
            }
        }

        return messages;
    }
}
