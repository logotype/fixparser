import { Components } from '../components/Components';
import { Enums } from '../enums/Enums';
import { Field } from '../fields/Field';
import { Fields } from '../fields/Fields';
import { DataTypes } from '../fields/datatypes/Datatypes';
import { Field as FieldType } from '../fieldtypes/Field';
import { Groups } from '../groups/Groups';
import { LicenseManager } from '../licensemanager/LicenseManager';
import type { RefType, Structure } from '../messagetype/MessageType';
import type { ISpecComponents, Ref } from '../spec/SpecComponents';
import type { ISpecEnums } from '../spec/SpecEnums';
import type { ISpecFields } from '../spec/SpecFields';
import type { ISpecGroups, NumInGroup } from '../spec/SpecGroups';
import { DEFAULT_FIX_VERSION, type FIXValue, SOH, pad, parseFixVersion } from '../util/util';

const TAG_CHECKSUM: string = '10=';
const TAG_MSGTYPE: string = '35=';
const MARKER_BODYLENGTH: string = '\x02';
const MARKER_CHECKSUM: string = '\x03';

type FieldValues = {
    [tag: string]: any;
};

/**
 * The MessageError object is used by FIXParser validation to
 * represent detailed error information when validation fails,
 * including the field, tag, value, expected value, and a
 * descriptive error message.
 *
 * @public
 */
export type MessageError = {
    field: RefType | ISpecFields | undefined;
    name: string | undefined;
    tag: number;
    value: FIXValue;
    expectedValue?: FIXValue;
    error: string;
};

/**
 * Represents a repeating group in the FIX protocol.
 * A repeating group is a collection of FIX fields that can appear multiple times in a message.
 *
 * @typedef {Object} RepeatingGroup
 * @property {string} name - The name of the repeating group.
 * @property {string} parentName - The name of the parent field or group that this repeating group belongs to.
 * @property {Set<number>} availableTagsInGroup - A set of tag numbers that are available in the group.
 * @property {number | undefined} valueInstanceFirstTag - The tag number of the first instance in the repeating group, or undefined if not specified.
 * @property {Record<string, FIXValue | FIXValue[]>} valueInstance - An object representing the value(s) associated with a specific instance of the repeating group.
 * @property {Array<Record<string, FIXValue | FIXValue[]>>} values - An array of objects representing multiple instances of the repeating group, with each instance containing key-value pairs.
 */
type RepeatingGroup = {
    name: string;
    parentName: string;
    availableTagsInGroup: Set<number>;
    valueInstanceFirstTag: number | undefined;
    valueInstance: Record<string, FIXValue | FIXValue[]>;
    values: Array<Record<string, FIXValue | FIXValue[]>>;
};

/**
 * Represents a complete FIX (Financial Information eXchange) message structure.
 * The message is divided into three main parts: Header, Body, and Trailer.
 *
 * @typedef {Object} FIXJSON
 * @property {Record<string, FIXValue | FIXValue[]>} Header - The header section of the FIX message, which includes metadata such as the sender, target, and timestamp.
 * @property {Record<string, FIXValue | FIXValue[] | Record<string, FIXValue | FIXValue[]>[]}> Body - The body section of the FIX message, which contains the actual transaction or data being conveyed. It can contain nested groups.
 * @property {Record<string, FIXValue | FIXValue[]>} Trailer - The trailer section of the FIX message, which typically contains checksum or other closing information.
 */
type FIXJSON = {
    Header: Record<string, FIXValue | FIXValue[]>;
    Body: Record<string, FIXValue | FIXValue[] | Record<string, FIXValue | FIXValue[]>[]>;
    Trailer: Record<string, FIXValue | FIXValue[]>;
};

const explain = (field: Field) => field.enumeration?.name ?? field.value;
const setFieldValue = (field: Field, values: FieldValues, useName = false): void => {
    const fieldValue = useName ? explain(field) : field.value;
    const fieldKey = field.name ?? field.tag;

    if (values[fieldKey]) {
        values[fieldKey] = Array.isArray(values[fieldKey])
            ? [...values[fieldKey], fieldValue]
            : [values[fieldKey], fieldValue];
    } else {
        values[fieldKey] = fieldValue;
    }
};

/**
 * A FIX message is a standardized electronic communication used in
 * financial markets to exchange information such as orders,
 * executions, and market data between trading participants,
 * following the FIX protocol's predefined structure and rules.
 *
 * @public
 */
export class Message {
    /** The fix version associated with the message. */
    public fixVersion: string = DEFAULT_FIX_VERSION;
    /** The data fields associated with the message. */
    public data: Field[] = [];
    /** The FIX string representation of the message. */
    public messageString = '';
    /** The description of the message. */
    public description = '';
    /** The type of the message. */
    public messageType = '';
    /** A description of the message type. */
    public messageTypeDescription = '';
    /** The sequence number of the message. */
    public messageSequence = -1;
    /** The structure of the message, can be null if not defined. */
    public structure: Structure[] | undefined = undefined;
    /** Flag indicating whether the body length is valid. */
    public bodyLengthValid = false;
    /** Flag indicating whether the checksum is valid. */
    public checksumValid = false;
    /** The specified checksum value of the message, can be null if not available. */
    public checksumValue: string | undefined = undefined;
    /** The actual checksum value of the message, can be null if not provided. */
    public checksumExpected: string | undefined = undefined;
    /** The specified body length of the message, can be null if not available. */
    public bodyLengthValue: number | undefined = undefined;
    /** The actual body length of the message, can be null if not provided. */
    public bodyLengthExpected: number | undefined = undefined;

    // Lazily initialized
    #_specEnums: Enums | undefined = undefined;
    #_specFields: Fields | undefined = undefined;
    #_specDatatypes: DataTypes | undefined = undefined;
    #_specComponents: Components | undefined = undefined;
    #_specGroups: Groups | undefined = undefined;

    /**
     * Creates a new FIX Message instance.
     *
     * The constructor initializes the FIX Message with the specified FIX version and optional
     * custom fields. It processes the fields to set specific FIX tags such as `BeginString`,
     * `MsgSeqNum`, and `MsgType`, and updates the message accordingly.
     *
     * @param {string} [fixVersion=DEFAULT_FIX_VERSION] - The FIX protocol version. Defaults to `DEFAULT_FIX_VERSION`.
     * @param {...Field} fields - A variable number of `Field` objects that represent individual tags in the FIX message.
     * Each field object must contain a `tag` and a `value`, where `tag` indicates the field type (e.g., `BeginString`, `MsgSeqNum`, etc.).
     *
     * @throws {Error} If the field contains an invalid tag or value type.
     *
     * @example
     * const message = new Message('FIXT.1.1', new Field(FieldType.MsgType, Messages.NewOrderSingle), new Field(Fields.OrderQty, 10000));
     *
     */
    constructor(fixVersion: string = DEFAULT_FIX_VERSION, ...fields: Field[]) {
        this.fixVersion = parseFixVersion(fixVersion);
        this.reset();

        // Add other tags
        fields.forEach((field: Field) => {
            if (field.tag === FieldType.BeginString) {
                this.fixVersion = String(field.value);
            }
            if (field.tag === FieldType.MsgSeqNum) {
                this.messageSequence = Number(field.value);
            }

            if (field.tag === FieldType.MsgType) {
                this.data.splice(0, 0, field);
            } else {
                this.data.push(field);
            }
        });
    }

    readonly #initializeCacheMaps = () => {
        if (!this.#_specEnums) {
            this.#_specEnums = new Enums();
        }
        if (!this.#_specFields) {
            this.#_specFields = new Fields();
        }
        if (!this.#_specDatatypes) {
            this.#_specDatatypes = new DataTypes();
        }
        if (!this.#_specComponents) {
            this.#_specComponents = new Components();
        }
        if (!this.#_specGroups) {
            this.#_specGroups = new Groups();
        }
    };

    readonly #calculateBodyLength = (value: string): number => {
        const startLength: number = value.indexOf(TAG_MSGTYPE) === -1 ? 0 : value.indexOf(TAG_MSGTYPE) + 1;
        const endLength: number =
            value.lastIndexOf(TAG_CHECKSUM) === -1 ? value.length : value.lastIndexOf(TAG_CHECKSUM) + 1;

        return endLength - startLength;
    };

    readonly #calculateChecksum = (value: string): string => {
        let integerValues = 0;

        let i = 0;
        for (i; i < value.length; i++) {
            integerValues += value.charCodeAt(i);
        }

        return pad(integerValues & 255, 3);
    };

    readonly #nonEmpty = (parts: TemplateStringsArray, ...args: string[]): string => {
        let res: string = parts[0];
        let i = 1;
        for (i; i < parts.length; i++) {
            if (args[i - 1] || args[i - 1] === '0') {
                res += args[i - 1];
            }
            res += parts[i];
        }
        return res.replace(/\s+/g, ' ');
    };

    readonly #validateRef = (
        structureItem: Structure,
        refType: 'fieldRef' | 'componentRef' | 'groupRef',
        tagSet: Set<number>,
    ): MessageError | undefined => {
        if (structureItem?.data[refType] && structureItem?.presence === 'required') {
            const list = structureItem?.data[refType].filter((ref) => ref.presence === 'required');
            for (const ref of list) {
                if (tagSet.has(Number(ref.id))) {
                    continue;
                }

                return {
                    field: {
                        ...ref,
                    },
                    name: ref.name,
                    tag: Number(ref.id),
                    value: null,
                    error: `Missing field ${ref.name}`,
                };
            }
        }
        return undefined;
    };

    readonly #validateMessage = (message: Message, requiredFieldsOnly = true): MessageError[] => {
        this.#initializeCacheMaps();

        const result: MessageError[] = [];
        const fields: Field[] = structuredClone(message.data);
        const tagSet = new Set(fields.map((field) => field.tag));

        const addError = (field: Field, error: string, expectedValue?: any) => {
            const fieldSpec = this.#_specFields?.cacheMap.get(field.tag);
            result.push({
                field: fieldSpec,
                name: field.name,
                tag: field.tag,
                value: field.value,
                error,
                expectedValue,
            });
        };

        // Validate fields
        for (const field of fields) {
            const hasEnum = this.#_specEnums?.getEnumByTag(String(field.tag));
            const isValidEnum = hasEnum && this.#_specEnums?.getEnum(String(field.tag), field.value);
            const isValidDatatype = this.#_specDatatypes?.validateDatatype(field.value, field.type?.name);

            if ((hasEnum && !isValidEnum) || !isValidDatatype) {
                addError(field, 'Incorrect data format for value');
            }

            if (field.tag === FieldType.BodyLength && !this.validateBodyLength(String(this.bodyLengthValue))) {
                addError(field, 'Incorrect value for BodyLength', this.bodyLengthExpected);
            }

            if (field.tag === FieldType.CheckSum && !this.validateChecksum(String(this.checksumValue))) {
                addError(field, 'Incorrect value for CheckSum', this.checksumExpected);
            }
        }

        // Validate structure
        if (this.structure) {
            for (const item of this.structure) {
                if (requiredFieldsOnly && item.presence !== 'required') continue;

                const { id, name, fieldRef, groupRef, componentRef } = item.data;
                const fieldId = Number(id);

                const isFieldMissing =
                    !tagSet.has(fieldId) && !fieldRef && !groupRef && !componentRef && fieldId !== 1003;

                /*
                    Application Version Precedence
                    Lowest: DefaultApplVerID(1137)
                    Supersedes: RefMsgType(372), RefApplVerID(1130), DefaultVerIndicator(1410),
                    Highest, explicit: ApplVerID(1128)
                 */
                const isSuperseded =
                    [1137, 372, 1130, 1410, 1128].includes(fieldId) &&
                    [1137, 372, 1130, 1410, 1128].some((tag) => tagSet.has(tag));

                // Single field type
                if (isFieldMissing && !isSuperseded) {
                    if (item.data.type === 'component') {
                        const componentFields = this.#extractComponentFields(fieldId);
                        const messageFields = new Set(fields.map((field) => field.tag));
                        if (componentFields.length > 0 && !componentFields.some((field) => messageFields.has(field))) {
                            result.push({
                                field: { ...item.data },
                                tag: Number(item.data.id),
                                name: item.data.name,
                                value: null,
                                error: `Missing block ${item.data.name}`,
                            });
                        }
                    } else {
                        addError({ tag: fieldId, name } as unknown as Field, `Missing field ${name}`);
                    }
                }

                // Validate references
                (['fieldRef', 'componentRef', 'groupRef'] as ('fieldRef' | 'componentRef' | 'groupRef')[]).forEach(
                    (refType) => {
                        const refError = this.#validateRef(item, refType, tagSet);
                        if (refError) result.push(refError);
                    },
                );
            }
        }

        return result;
    };

    readonly #extractComponentFields = (componentId: number): number[] => {
        const componentSpec = this.#_specComponents?.cacheMap.get(componentId);
        if (!componentSpec) return [];

        const { fieldRef = [], componentRef = [], groupRef = [] } = componentSpec;
        return [
            ...fieldRef.map((ref) => ref.id),
            ...componentRef.map((ref) => ref.id),
            ...groupRef.map((ref) => ref.id),
        ];
    };

    /**
     * Adds a single `Field` object to the message data.
     *
     * @param {Field} field - The `Field` object to be added.
     */
    public addField(field: Field): void {
        this.data.push(field);
    }

    /**
     * Adds multiple `Field` objects to the message data.
     * If the field's tag is `MsgType`, it will be added at the beginning of the `data` array.
     * Otherwise, it will be appended to the end.
     *
     * @param {...Field} fields - One or more `Field` objects to be added.
     */
    public addFields(...fields: Field[]): void {
        fields.forEach((field: Field) => {
            if (field.tag === FieldType.MsgType) {
                this.data.splice(0, 0, field);
            } else {
                this.data.push(field);
            }
        });
    }

    /**
     * Removes a field from the message data by its `tag`.
     *
     * @param {number} tag - The tag of the field to be removed.
     */
    public removeFieldByTag(tag: number): void {
        const index: number = this.data.findIndex((field: Field) => field.tag === tag);
        if (index > -1) {
            this.data.splice(index, 1);
        }
    }

    /**
     * Retrieves a single field from the message data by its `tag`.
     *
     * @param {number} tag - The tag of the field to be retrieved.
     * @returns {Field | undefined} The `Field` object if found, otherwise `undefined`.
     */
    public getField(tag: number): Field | undefined {
        return this.data.find((field: Field) => field.tag === tag);
    }

    /**
     * Retrieves all fields from the message data that have the specified `tag`.
     *
     * @param {number} tag - The tag of the fields to be retrieved.
     * @returns {Field[]} An array of `Field` objects that match the specified `tag`, or an empty array if no fields match.
     */
    public getFields(tag: number): Field[] | undefined {
        return this.data.filter((field: Field) => field.tag === tag);
    }

    /**
     * Returns an object containing the name-explain pairs of all fields in the message.
     * The `name` of each field is used as the key, and the enumeration of the (`value`) is assigned to it.
     * For example, `MsgType: 'D'` gets assigned `MsgType: 'NewOrderSingle'`,
     *
     * @returns {FieldValues} An object where the keys are field names and the values are the explained field values.
     * @example
     * console.log(message.getFieldExplains());
     * {
     *   '47': 'A',
     *   BeginString: 'FIX42',
     *   BodyLength: 146,
     *   MsgType: 'NewOrderSingle',
     *   MsgSeqNum: 4,
     *   SenderCompID: 'ABC_DEFG01',
     *   SendingTime: '20090323-15:40:29',
     *   TargetCompID: 'CCG',
     *   OnBehalfOfCompID: 'XYZ',
     *   ClOrdID: 'NF 0542/03232009',
     *   Side: 'Buy',
     *   OrderQty: 100,
     *   Symbol: 'CVS',
     *   OrdType: 'Market',
     *   TimeInForce: 'Day',
     *   TransactTime: '20090323-15:40:29',
     *   HandlInst: 'AutomatedExecutionNoIntervention',
     *   SecurityExchange: 'N',
     *   CheckSum: '195'
     * }
     */
    public getFieldExplains(): FieldValues {
        const values: FieldValues = {};
        this.data.forEach((field: Field) => {
            setFieldValue(field, values, true);
        });
        return values;
    }

    /**
     * Returns an object containing the name-value pairs of all fields in the message.
     * The `name` of each field is used as the key, and the `value` is assigned to it.
     *
     * @returns {FieldValues} An object where the keys are field names and the values are field values.
     * @example
     * console.log(message.getFieldNameValues());
     * {
     *   '47': 'A',
     *   BeginString: 'FIX.4.2',
     *   BodyLength: 146,
     *   MsgType: 'D',
     *   MsgSeqNum: 4,
     *   SenderCompID: 'ABC_DEFG01',
     *   SendingTime: '20090323-15:40:29',
     *   TargetCompID: 'CCG',
     *   OnBehalfOfCompID: 'XYZ',
     *   ClOrdID: 'NF 0542/03232009',
     *   Side: '1',
     *   OrderQty: 100,
     *   Symbol: 'CVS',
     *   OrdType: '1',
     *   TimeInForce: '0',
     *   TransactTime: '20090323-15:40:29',
     *   HandlInst: '1',
     *   SecurityExchange: 'N',
     *   CheckSum: '195'
     * }
     */
    public getFieldNameValues(): FieldValues {
        const values: FieldValues = {};
        this.data.forEach((field: Field) => {
            setFieldValue(field, values, false);
        });
        return values;
    }

    /**
     * Returns an object containing the values of all fields in the message, indexed by their `tag`.
     * If a tag has multiple fields, the values will be stored in an array.
     *
     * @returns {FieldValues} An object where the keys are field tags and the values are field values (or arrays of values).
     * @example
     * console.log(message.getFieldValues());
     * {
     *   '8': 'FIX.4.2',
     *   '9': 146,
     *   '10': '195',
     *   '11': 'NF 0542/03232009',
     *   '21': '1',
     *   '34': 4,
     *   '35': 'D',
     *   '38': 100,
     *   '40': '1',
     *   '47': 'A',
     *   '49': 'ABC_DEFG01',
     *   '52': '20090323-15:40:29',
     *   '54': '1',
     *   '55': 'CVS',
     *   '56': 'CCG',
     *   '59': '0',
     *   '60': '20090323-15:40:29',
     *   '115': 'XYZ',
     *   '207': 'N'
     * }
     */
    public getFieldValues(): FieldValues {
        const values: FieldValues = {};
        this.data.forEach((field: Field) => {
            if (values[field.tag]) {
                if (Array.isArray(values[field.tag])) {
                    values[field.tag] = [...values[field.tag], field.value];
                } else {
                    values[field.tag] = [values[field.tag], field.value];
                }
            } else {
                values[field.tag] = field.value;
            }
        });
        return values;
    }

    /**
     * Updates a field in the message data based on its `tag`.
     * If a field with the same tag exists, it will be replaced with the new `Field` object.
     *
     * @param {Field} field - The `Field` object to update in the data.
     */
    public setField(field: Field): void {
        const index: number = this.data.findIndex((item: Field) => item.tag === field.tag);
        if (index > -1) {
            this.data[index] = field;
        }
    }

    /**
     * Retrieves the enumeration for a given `tag` and `value` based on the message's `MsgType`.
     * This method ensures that the `MsgType` field is present and has a value before attempting
     * to retrieve the corresponding enumeration.
     *
     * @param {number} tag - The tag of the field for which the enum is to be retrieved.
     * @param {FIXValue} value - The value of the field for which the enum is to be retrieved.
     * @returns {ISpecEnums | undefined} The corresponding enumeration if found, or `undefined` if the `MsgType` field is missing
     * or has no value, or if no matching enum is found.
     *
     * @example
     * const enumValue = message.getEnum(35, 'D');
     * console.log(`Enum: ${enumValue}`);
     */
    public getEnum(tag: number, value: FIXValue): ISpecEnums | undefined {
        if (!this.getField(FieldType.MsgType)?.tag) {
            return;
        }

        if (!this.getField(FieldType.MsgType)?.value) {
            return;
        }

        const enums = new Enums();
        return enums.getEnum(tag.toString(), value);
    }

    /**
     * Generates a brief Bloomberg-style description of the current message based on its fields, such as `Side`, `OrderQty`, `Symbol`, `Price`, etc.
     * The description is typically used for a quick summary of an order or trade message, formatted with key field values.
     *
     * Depending on the available fields, the method formats and returns a string summarizing the message's main details,
     * such as quantity, price, symbol, and order type. If certain fields (like `LeavesQty`, `OrderQty`, `Price`, etc.) are not present,
     * it adjusts the description accordingly.
     *
     * @returns {string | null} A brief string description summarizing the message. If no description can be formed, `null` is returned.
     *
     * @example
     * const description = message.getBriefDescription();
     * console.log(description); // "100 AAPL LMT @123.45 GTC"
     */
    public getBriefDescription(): string | undefined {
        let returnValue = '';
        const sideField: Field | undefined = this.getField(FieldType.Side);
        let side: string | undefined = '';
        if (sideField?.enumeration) {
            side = sideField.enumeration.name;
            side = side ? side.replace('Sell', 'SL').toUpperCase() : undefined;
        }

        if (this.getField(FieldType.LeavesQty) !== undefined) {
            let quantity = '';

            if (this.getField(FieldType.ContraTradeQty)) {
                quantity = String(this.getField(FieldType.ContraTradeQty)?.value);
            } else {
                quantity = String(this.getField(FieldType.OrderQty)?.value ?? '');
            }
            const leavesQuantity: string = String(this.getField(FieldType.LeavesQty)?.value);
            const lastPrice: number = this.getField(FieldType.LastPx)
                ? Number(this.getField(FieldType.LastPx)?.value)
                : 0;
            returnValue = this.#nonEmpty`${quantity} @${
                lastPrice || lastPrice === 0 ? lastPrice.toFixed(2) : '0.00'
            } ${this.getField(FieldType.LeavesQty)!.name!.replace('LeavesQty', 'LvsQty')} ${Number.parseInt(
                leavesQuantity,
                10,
            ).toString()}`;
        } else if (this.getField(FieldType.OrderQty)) {
            const orderQuantity: string = String(this.getField(FieldType.OrderQty)?.value);
            const symbol: string = this.getField(FieldType.Symbol)
                ? String(this.getField(FieldType.Symbol)?.value)
                : '';
            const orderType: Field = this.getField(FieldType.OrdType)!;
            let name = '';
            if (orderType?.enumeration?.name) {
                name = orderType.enumeration.name;
            }
            const timeInForceField = this.getField(FieldType.TimeInForce)!;
            let timeInForce = '';
            if (timeInForceField?.enumeration) {
                timeInForce = String(timeInForceField.enumeration.name);
            }

            const priceField = this.getField(FieldType.Price);
            if (priceField) {
                let price: string = String(priceField.value);
                const priceValue = Number(priceField.value);

                if (priceValue >= 1) {
                    price = priceValue.toFixed(2);
                } else if (priceValue < 1) {
                    price = priceValue.toString().replace('0.', '.');
                }

                const sidePart = side ? `${side} ` : '';
                const orderQuantityPart = orderQuantity ? `${orderQuantity} ` : '';
                const symbolPart = symbol ? `${symbol.toUpperCase()} ` : '';
                const namePart = name ? `${name.replace('Market', 'MKT').replace('Limit', 'LMT').toUpperCase()} ` : '';
                const pricePart = `@${price} `;
                const timeInForcePart = timeInForce ? timeInForce.toUpperCase() : '';

                const fullString = sidePart + orderQuantityPart + symbolPart + namePart + pricePart + timeInForcePart;
                returnValue = this.#nonEmpty`${fullString}`;
            } else {
                returnValue = this.#nonEmpty`${side ?? ''} ${orderQuantity} ${symbol ? symbol.toUpperCase() : ''} ${
                    name ? name.replace('Market', 'MKT').replace('Limit', 'LMT').toUpperCase() : ''
                } ${timeInForce ? timeInForce.toUpperCase() : ''}`;
            }
        } else {
            const messageType = this.getField(FieldType.MsgType);
            if (messageType?.tag && messageType.value) {
                const name = this.getEnum(messageType.tag, String(messageType.value))?.name;
                return name ?? '';
            }
            return undefined;
        }

        return returnValue.trim();
    }

    /**
     * Validates the body length of the FIX message by comparing the provided value to the actual body length.
     * The body length is calculated as the difference between the positions of the `MsgType` tag and the `Checksum` tag.
     *
     * @param {string} value - The body length value to validate against the calculated body length.
     * @returns {boolean} `true` if the provided body length matches the calculated body length, otherwise `false`.
     *
     * @example
     * const isValid = message.validateBodyLength('123');
     * console.log(isValid); // true if the body length is correct, false otherwise
     */
    public validateBodyLength(value: string): boolean {
        const index: number = this.messageString.indexOf(TAG_MSGTYPE);
        const lastIndex: number = this.messageString.lastIndexOf(TAG_CHECKSUM);
        const startLength: number = index === -1 ? 0 : index;
        const endLength: number = lastIndex === -1 ? this.messageString.length : lastIndex;
        const bodyLength: number = endLength - startLength;

        this.bodyLengthValue = Number(value);
        this.bodyLengthExpected = bodyLength;
        this.bodyLengthValid = Number(value) === bodyLength;
        return this.bodyLengthValid;
    }

    /**
     * Validates the checksum of the FIX message by comparing the provided checksum value to the calculated checksum.
     * The checksum is calculated based on the substring of the message string before the `Checksum` tag.
     *
     * @param {string} value - The checksum value to validate against the calculated checksum.
     * @returns {boolean} `true` if the provided checksum matches the calculated checksum, otherwise `false`.
     *
     * @example
     * const isValid = message.validateChecksum('123456');
     * console.log(isValid); // true if the checksum is valid, false otherwise
     */
    public validateChecksum(value: string): boolean {
        const lastIndex: number = this.messageString.lastIndexOf(TAG_CHECKSUM);
        const length: number = lastIndex === -1 ? this.messageString.length : lastIndex;
        const data: string = this.messageString.substring(0, length);
        const calculatedChecksum: string = this.#calculateChecksum(data);

        this.checksumValue = value;
        this.checksumExpected = calculatedChecksum;
        this.checksumValid = value === calculatedChecksum;
        return this.checksumValid;
    }

    /**
     * Validates the structure and content of a given FIX message.
     *
     * This method checks the fields in the provided `message` for correct data format, required fields,
     * and field references (such as `fieldRef`, `groupRef`, `componentRef`). It also validates specific fields
     * like `BodyLength` and `CheckSum` against expected values.
     *
     * The validation process can be constrained to only required fields, based on the `requiredFieldsOnly` flag.
     *
     * @param {boolean} [requiredFieldsOnly=true] - If `true`, only required fields (as specified in the message structure) are validated.
     * Defaults to `true`.
     *
     * @returns {MessageError[]} - An array of validation errors. Each error includes details about the field that caused the error,
     * such as the field's `tag`, `name`, `value`, the error message, and any expected value (if applicable).
     *
     * @throws {Error} If any internal validation process fails or an invalid message is encountered.
     *
     * @example
     * const validationErrors = message.validate();
     *
     * if (validationErrors.length > 0) {
     *   validationErrors.forEach(error => {
     *     console.error(`Field ${error.name} (Tag ${error.tag}) has error: ${error.error}`);
     *   });
     * }
     */
    public validate(requiredFieldsOnly = true): MessageError[] {
        return this.#validateMessage(this, requiredFieldsOnly);
    }

    /**
     * Encodes the FIX message into a string format based on the current message data, including its header, body, and trailer.
     * This method first validates the license, then constructs the message by adding fields such as `BeginString`, `BodyLength`,
     * and `CheckSum`. It also calculates the body length and checksum dynamically, ensuring the message is correctly formatted
     * according to the FIX protocol.
     *
     * @param {string} [separator=SOH] - The separator used to separate fields in the encoded FIX message. Defaults to `SOH` (Start of Header).
     * @returns {string} The encoded FIX message string, including calculated body length and checksum.
     * @throws {Error} If the license is invalid, it throws an error with a message indicating no valid license.
     *
     * @example
     * const fixMessage = message.encode();
     * console.log(fixMessage); // "8=FIX.4.4|35=D|49=SenderCompID|56=TargetCompID|34=1|10=123"
     */
    public encode(separator: string = SOH): string {
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        const fields: Field[] = this.data.map((field: Field) => new Field(field.tag, field.value));
        const data: string[] = [];

        let beginString: string = new Field(FieldType.BeginString, this.fixVersion).toString();
        let bodyLength: string = new Field(FieldType.BodyLength, MARKER_BODYLENGTH).toString();
        let checksum: string = new Field(FieldType.CheckSum, MARKER_CHECKSUM).toString();
        let index: number = fields.findIndex((field) => field.tag === FieldType.BeginString);

        // Check for header
        if (index > -1) {
            const version = parseFixVersion(fields[index].value);
            beginString = new Field(FieldType.BeginString, version).toString();
            fields.splice(index, 1);
        }

        // Check for body length
        index = fields.findIndex((field) => field.tag === FieldType.BodyLength);
        if (index > -1) {
            bodyLength = fields[index].toString();
            fields.splice(index, 1);
        }

        // Check for trailer
        index = fields.findIndex((field) => field.tag === FieldType.CheckSum);
        if (index > -1) {
            checksum = fields[index].toString();
            fields.splice(index, 1);
        }

        data.push(beginString);
        data.push(bodyLength);

        // Add other fields
        fields.forEach((field) => {
            data.push(field.toString());
        });

        data.push(checksum);

        let fixMessage = `${data.join(separator)}${separator}`;
        fixMessage = fixMessage.replace(MARKER_BODYLENGTH, this.#calculateBodyLength(fixMessage).toString());

        const length: number =
            fixMessage.lastIndexOf(TAG_CHECKSUM) === -1 ? fixMessage.length : fixMessage.lastIndexOf(TAG_CHECKSUM);
        const calculatedChecksum: string = this.#calculateChecksum(fixMessage.substring(0, length));
        fixMessage = fixMessage.replace(MARKER_CHECKSUM, calculatedChecksum);

        return fixMessage;
    }

    /**
     * Converts the current FIX message to a JSON-like structure (FIX JSON), representing the message in a format that mirrors the FIX protocol.
     * The structure includes three main sections: `Header`, `Body`, and `Trailer`. Each section contains fields and, where applicable,
     * groups of repeating fields. The method processes the fields in the `data` array and organizes them into these sections, handling
     * groups, references, and field types appropriately.
     *
     * @returns {FIXJSON} The FIX message converted into a structured JSON-like format with `Header`, `Body`, and `Trailer` sections.
     *
     * @throws {Error} If the license is invalid, an error is thrown with a message indicating that no valid license is available.
     *
     * @example
     * const fixJson = message.toFIXJSON();
     * console.log(fixJson);
     * // {
     * //   "Header": {
     * //     "BeginString": "FIXT.1.1",
     * //     "MsgType": "NewOrderSingle",
     * //     "SenderCompID": "ABC",
     * //     "TargetCompID": "XYZ"
     * //   },
     * //   "Body": {
     * //     "OrderQty": 1000,
     * //     "Price": 50.25,
     * //     "Symbol": "AAPL",
     * //     "Side": "Buy"
     * //   },
     * //   "Trailer": {
     * //     "CheckSum": "123"
     * //   }
     * // }
     */
    public toFIXJSON(): FIXJSON {
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        this.#initializeCacheMaps();
        const headerFields: number[] | undefined = this.#_specComponents?.getTagsByComponentName('StandardHeader');
        const trailerFields: number[] | undefined = this.#_specComponents?.getTagsByComponentName('StandardTrailer');

        const header: Record<string, FIXValue | FIXValue[]> = {};
        const body: Record<string, FIXValue | FIXValue[] | Record<string, FIXValue | FIXValue[]>[]> = {};
        const trailer: Record<string, FIXValue | FIXValue[]> = {};

        let groupIndex = 0;
        const groups: RepeatingGroup[] = [];

        const closeGroup = () => {
            pushInstanceToValues();

            const group: RepeatingGroup = groups[groupIndex];
            const parent: RepeatingGroup | undefined | typeof body =
                group.parentName === 'Body' ? body : groups.find((g) => g.name === group.parentName);

            if (parent) {
                if (group.parentName === 'Body') {
                    body[group.name] = group.values;
                } else {
                    (parent.valueInstance as typeof body)[group.name] = group.values;
                }
            }

            groups.splice(groupIndex, 1);
            groupIndex--;
        };

        const closeAllGroups = (field: Field) => {
            while (groups.length > 0 && !groups[groupIndex].availableTagsInGroup.has(field.tag)) {
                closeGroup();
            }
        };

        const createGroup = (field: Field, index: number) => {
            const parentName = groups.length > 0 ? groups[groupIndex].name : 'Body';
            const group: RepeatingGroup = {
                name: field.name ?? `${field.tag}`,
                parentName,
                availableTagsInGroup: new Set<number>(),
                valueInstanceFirstTag: this.data[index + 1].tag,
                valueInstance: {},
                values: [],
            };

            groups.push(group);
            groupIndex = groups.length === 1 ? 0 : groupIndex + 1;

            const groupFields = this.#_specGroups?.filterMapByNumInGroupId(field.tag, this.messageType);
            if (!groupFields) return;

            extractRefsIntoGroup(groupFields);
        };

        const extractRefsIntoGroup = (groupFields: ISpecGroups | ISpecComponents) => {
            if (!groupFields) return;

            const addRef = (ref: NumInGroup) => groups[groupIndex].availableTagsInGroup.add(Number(ref.id));

            if (groupFields.numInGroup) {
                addRef(groupFields.numInGroup);
            }

            if (groupFields.fieldRef) {
                groupFields.fieldRef.forEach((ref) => {
                    addRef(ref);
                });
            }

            const processRefs = (refs: Ref[], spec: Groups | Components) => {
                refs.forEach((ref) => {
                    const refFields = spec.find(Number(ref.id));
                    refFields && extractRefsIntoGroup(refFields);
                });
            };

            if (groupFields.groupRef && this.#_specGroups) processRefs(groupFields.groupRef, this.#_specGroups);
            if (groupFields.componentRef && this.#_specComponents)
                processRefs(groupFields.componentRef, this.#_specComponents);
        };

        const pushInstanceToValues = () => {
            const { valueInstance, values } = groups[groupIndex];
            if (Object.keys(valueInstance).length > 0) {
                values.push(valueInstance);
                groups[groupIndex].valueInstance = {};
            }
        };

        this.data.forEach((field: Field, index: number) => {
            const tag = field.tag;
            const value = field.enumeration?.name ?? field.value;
            const fieldName: string = field.name ?? `${field.tag}`;

            if (headerFields?.includes(tag)) {
                header[fieldName] = value;
                return;
            }

            if (trailerFields?.includes(tag)) {
                closeAllGroups(field);
                trailer[fieldName] = value;
                return;
            }

            if (field?.type?.name === 'NumInGroup') {
                if (groups.length === 0 || (groups.length > 0 && groups[groupIndex]?.availableTagsInGroup.has(tag))) {
                    createGroup(field, index);
                } else {
                    closeGroup();
                    createGroup(field, index);
                }
                return;
            }

            if (groups.length > 0 && groups[groupIndex].availableTagsInGroup.has(tag)) {
                if (groups[groupIndex].valueInstanceFirstTag === tag) {
                    pushInstanceToValues();
                    groups[groupIndex].valueInstance[fieldName] = value;
                } else {
                    groups[groupIndex].valueInstance = {
                        ...groups[groupIndex].valueInstance,
                        [field?.name ?? String(tag)]: value,
                    };
                }
                return;
            }

            closeAllGroups(field);

            if (groups[groupIndex]?.valueInstanceFirstTag === tag) {
                pushInstanceToValues();
                groups[groupIndex].valueInstance[fieldName] = value;
            }

            if (!groups[groupIndex]?.availableTagsInGroup.has(tag)) {
                body[fieldName] = value;
            }
        });

        return {
            Header: header,
            Body: body,
            Trailer: trailer,
        };
    }

    /**
     * Creates and returns a new instance of the `Message` class, which is a deep clone of the current `Message` instance.
     * The cloned instance has the same `fixVersion`, `messageSequence`, and `messageType`, but the `data` array is also cloned to avoid references to the original fields.
     *
     * @returns {Message} A new `Message` instance that is a clone of the current one.
     *
     * @example
     * const originalMessage = new Message('FIXT.1.1', field1, field2, field3);
     * const clonedMessage = originalMessage.clone();
     * console.log(clonedMessage); // A new Message instance with the same data as the original
     */
    public clone(): Message {
        const cloned: Message = new Message(this.fixVersion, ...this.data);
        cloned.messageSequence = this.messageSequence;
        cloned.messageType = this.messageType;
        return cloned;
    }

    /**
     * Resets the internal state of the `Message` instance to its initial, empty state.
     * This includes clearing all data fields, resetting various properties to their default values,
     * and ensuring the message is in a clean state ready for processing.
     *
     * This method is typically used when you want to clear the state of the current `Message` object
     * and prepare it for new data, e.g., when parsing or constructing a new FIX message.
     *
     * @private
     * @returns {void} This method does not return any value.
     *
     * @example
     * const message = new Message('FIX.4.4', field1, field2);
     * message.reset(); // Clears all properties and prepares the message for a new state
     */
    private reset(): void {
        this.data = [];
        this.messageString = '';
        this.description = '';
        this.messageType = '';
        this.messageSequence = -1;
        this.structure = undefined;
        this.bodyLengthValid = false;
        this.checksumValid = false;
        this.checksumValue = undefined;
        this.checksumExpected = undefined;
        this.bodyLengthValue = undefined;
        this.bodyLengthExpected = undefined;
    }
}
