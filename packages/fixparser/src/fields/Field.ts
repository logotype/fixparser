import type { EnumType } from '../enums/EnumType';
import type { ISpecDatatypes } from '../spec/SpecDatatypes';
import type { FIXValue } from '../util/util';
import type { CategoryType } from './categories/CategoryType';
import type { SectionType } from './sections/SectionType';

/**
 * Field is a predefined data element, identified by a unique tag number,
 * that represents a specific piece of information within a message
 * (such as price, quantity, or order ID).
 *
 * @public
 */
export class Field {
    public tag: number;
    public value: FIXValue;
    public name: string | undefined = undefined;
    public description: string | undefined = undefined;
    public type: ISpecDatatypes | undefined = undefined;
    public category: CategoryType | undefined = undefined;
    public section: SectionType | undefined = undefined;
    public enumeration: EnumType | undefined = undefined;

    constructor(tag: number, value: FIXValue) {
        this.tag = tag >> 0;
        this.value = value;
        this.name = undefined;
        this.description = undefined;
        this.type = undefined;
        this.category = undefined;
        this.section = undefined;
        this.enumeration = undefined;
    }

    public setTag(tag: number): void {
        this.tag = tag >> 0;
    }

    public setValue(value: FIXValue): void {
        this.value = value;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public setDescription(description: string): void {
        this.description = description;
    }

    public setType(type: ISpecDatatypes | undefined): void {
        this.type = type;
    }

    public setCategory(category: CategoryType): void {
        this.category = category;
    }

    public setSection(section: SectionType): void {
        this.section = section;
    }

    public setEnumeration(enumeration: EnumType): void {
        this.enumeration = enumeration;
    }

    public toString(): string {
        return `${this.tag}=${this.value}`;
    }
}
