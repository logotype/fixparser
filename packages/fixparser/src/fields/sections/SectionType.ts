import type { ISpecSections } from '../../spec/SpecSections';

export class SectionType {
    public sectionID: string | undefined = undefined;
    public name: string | undefined = undefined;
    public displayOrder: number | undefined = undefined;
    public volume: string | undefined = undefined;
    public notReqXML: boolean | undefined = undefined;
    public fixmlFileName: string | undefined = undefined;
    public description: string | undefined = undefined;

    public setSection(section: ISpecSections): void {
        this.sectionID = section.SectionID;
        this.name = section.Name;
        this.displayOrder = section.DisplayOrder;
        this.volume = section.Volume;
        this.notReqXML = section.NotReqXML === 1;
        this.fixmlFileName = section.FIXMLFileName;
        this.description = section.Description;
    }

    public reset(): void {
        this.sectionID = undefined;
        this.name = undefined;
        this.displayOrder = undefined;
        this.volume = undefined;
        this.notReqXML = undefined;
        this.fixmlFileName = undefined;
        this.description = undefined;
    }
}
