import type { ISpecCategories } from '../../spec/SpecCategories';

export class CategoryType {
    public categoryID: string | undefined = undefined;
    public fixmlFileName: string | undefined = undefined;
    public componentType: string | undefined = undefined;
    public sectionID: string | undefined = undefined;
    public includeFile: string | undefined = undefined;

    public setCategory(category: ISpecCategories): void {
        this.categoryID = category.name;
        this.fixmlFileName = category.FIXMLFileName;
        this.componentType = category.componentType;
        this.sectionID = category.section!;
        this.includeFile = category.includeFile!;
    }

    public reset(): void {
        this.categoryID = undefined;
        this.fixmlFileName = undefined;
        this.componentType = undefined;
        this.sectionID = undefined;
        this.includeFile = undefined;
    }
}
