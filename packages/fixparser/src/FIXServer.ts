import { type Server, type Socket, createServer as createTcpServer } from 'node:net';
import { type TLSSocket, type TlsOptions, createServer as createTcpTlsServer } from 'node:tls';
import { type ServerOptions, type WebSocket, WebSocketServer } from 'ws';

import { type IMessageStore, MessageBuffer } from 'fixparser-common';
import { FIXParser } from './FIXParser';
import type { BaseOptions, ConnectionType, Options as FIXParserOptions, Protocol } from './FIXParserBase';
import type { IFIXParser } from './IFIXParser';
import { Field } from './fields/Field';
import * as Constants from './fieldtypes';
import { LicenseManager } from './licensemanager/LicenseManager';
import { type LogOptions, Logger } from './logger/Logger';
import { Message } from './message/Message';
import { heartBeat } from './messagetemplates/MessageTemplates';
import { serverProcessMessage } from './session/ServerMessageProcessor';
import { FrameDecoder } from './util/FrameDecoder';
import {
    DEFAULT_FIX_VERSION,
    DEFAULT_HEARTBEAT_SECONDS,
    type Parser,
    READY_MS,
    type Version,
    parseFixVersion,
    version,
} from './util/util';

type Options = Pick<
    FIXParserOptions,
    | 'host'
    | 'port'
    | 'protocol'
    | 'sender'
    | 'target'
    | 'heartbeatIntervalSeconds'
    | 'fixVersion'
    | 'messageStoreIn'
    | 'messageStoreOut'
    | 'logging'
    | 'logOptions'
    | 'tlsOptions'
    | 'tlsSkipStdInPipe'
    | 'onMessage'
    | 'onOpen'
    | 'onError'
    | 'onClose'
    | 'onReady'
>;

class FIXServer implements IFIXParser {
    /* FIXParser Version */
    public static readonly version: Version = version;
    /* Name of current parser name */
    public readonly parserName: Parser = 'FIXServer';
    /* Instance of FIXParser class */
    public readonly fixParser: FIXParser = new FIXParser();
    /* Heartbeat interval ID */
    public heartBeatIntervalId: ReturnType<typeof setInterval> | undefined = undefined;
    /* Type of socket connection (Socket, WebSocket, TLSSocket) */
    public socket: WebSocket | Socket | undefined = undefined;
    /* Current connection status */
    public connected = false;
    /* Target hostname or IP address */
    public host = 'localhost';
    /* Target port */
    public port = 9878;
    /* Type of protocol */
    public protocol: Protocol = 'tcp';
    /* Assigned value used to identify firm sending message, SenderCompID tag 49 */
    public sender = '';
    /* Assigned value used to identify receiving firm, TargetCompID tag 56 */
    public target = '';
    /* Heartbeat interval in seconds */
    public heartBeatInterval: number = DEFAULT_HEARTBEAT_SECONDS;
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    public fixVersion: string = DEFAULT_FIX_VERSION;
    /* Message buffer of 2500 incoming messages */
    public messageStoreIn: IMessageStore<Message> = new MessageBuffer();
    /* Message buffer of 2500 outgoing messages */
    public messageStoreOut: IMessageStore<Message> = new MessageBuffer();
    /* Whether an outgoing message contains a Logout */
    public requestedLogout = false;
    /* Indicates whether this instance is acceptor or initiator */
    public connectionType: ConnectionType = 'acceptor';
    /* Logger instance */
    public logger: Logger = new Logger();
    /* Logging */
    public logging = true;
    /* Log options */
    public logOptions: LogOptions | undefined;
    /* TLS Server Options */
    public tlsOptions?: TlsOptions;
    /* TLS Skip Standard Input pipe of socket */
    public tlsSkipStdInPipe?: boolean;
    /* Server socket connection */
    public server: Server | WebSocketServer | undefined = undefined;
    /* Whether current FIX session has logged in */
    public isLoggedIn = false;
    /* Message counter */
    public messageCounter = 0;

    /**
     * Constructor for initializing an instance with the provided options.
     *
     * @param {BaseOptions} [options={ logging: true, logOptions: undefined, fixVersion: DEFAULT_FIX_VERSION, skipValidation: false }] - The options to configure the instance.
     * @param {boolean} [options.logging=true] - Whether logging is enabled (defaults to `true`).
     * @param {LogOptions} [options.logOptions=undefined] - Options to customize logging behavior.
     * @param {string} [options.fixVersion=DEFAULT_FIX_VERSION] - The FIX protocol version to use (defaults to `DEFAULT_FIX_VERSION`).
     * @param {boolean} [options.skipValidation=false] - Whether to skip validation of FIX messages (defaults to `false`).
     */
    constructor(
        options: BaseOptions = {
            logging: true,
            logOptions: undefined,
            fixVersion: DEFAULT_FIX_VERSION,
            skipValidation: false,
        },
    ) {
        this.logging = options.logging ?? true;
        this.logOptions = options.logOptions;
        this.fixParser.fixVersion = options.fixVersion ? parseFixVersion(options.fixVersion) : DEFAULT_FIX_VERSION;
        this.fixVersion = this.fixParser.fixVersion;
        this.fixParser.fixParserBase.skipValidation = options.skipValidation;
    }

    /**
     * onMessageCallback is called when a message has been received
     *
     * @remarks FIXParser~onMessageCallback
     * @param message
     */
    private onMessageCallback: Options['onMessage'] = (message) => {};

    /**
     * onOpenCallback is called the FIX connection has been initiated
     *
     * @remarks FIXParser~onOpenCallback
     */
    private onOpenCallback: Options['onOpen'] = () => {};

    /**
     * onErrorCallback is called the FIX connection failed
     *
     * @remarks FIXParser~onErrorCallback
     * @param error
     */
    private onErrorCallback: Options['onError'] = (error) => {};

    /**
     * onCloseCallback is called the FIX connection has been closed
     *
     * @remarks FIXParser~onCloseCallback
     */
    private onCloseCallback: Options['onClose'] = () => {};

    /**
     * onReadyCallback is called the FIX connection is opened and ready
     *
     * @remarks FIXParser~onReadyCallback
     */
    private onReadyCallback: Options['onReady'] = () => {};

    /**
     * Creates a FIX server that listens for incoming connections. This method sets up
     * various configurations, including logging, connection options, and callbacks for
     * events like message reception, connection opening, error handling, and closure.
     *
     * - Initializes logging based on the provided options.
     * - Validates the license before proceeding with server creation.
     * - Sets up connection parameters such as host, port, sender, and target identifiers.
     * - Sets the heartbeat interval and FIX version.
     * - Configures callback functions for handling messages, connection events, and errors.
     * - Calls the `initialize()` method to begin server operation.
     *
     * @param {Options} [options={}] - The configuration options for the server.
     * @param {string} [options.host=this.host] - The host address the server will bind to.
     * @param {number} [options.port=this.port] - The port number the server will listen on.
     * @param {string} [options.protocol=this.protocol] - The protocol to be used ('tcp', 'ssl-tcp', 'websocket', etc.).
     * @param {string} [options.sender=this.sender] - The sender identifier for the server.
     * @param {string} [options.target=this.target] - The target identifier for the server.
     * @param {number} [options.heartbeatIntervalSeconds=DEFAULT_HEARTBEAT_SECONDS] - The heartbeat interval in seconds.
     * @param {string} [options.messageStoreIn=new MessageBuffer()] - Optional custom message buffer for incoming messages.
     * @param {string} [options.messageStoreOut=new MessageBuffer()] - Optional custom message buffer for outgoing messages.
     * @param {function} [options.onMessage=this.onMessageCallback] - Callback function for handling incoming messages.
     * @param {function} [options.onOpen=this.onOpenCallback] - Callback function for handling connection open event.
     * @param {function} [options.onError=this.onErrorCallback] - Callback function for handling errors.
     * @param {function} [options.onClose=this.onCloseCallback] - Callback function for handling connection close event.
     * @param {function} [options.onReady=this.onReadyCallback] - Callback function for handling the ready event.
     *
     * @returns {void}
     */
    public createServer(options: Options = {}): void {
        const {
            host = this.host,
            port = this.port,
            protocol = this.protocol,
            sender = this.sender,
            target = this.target,
            heartbeatIntervalSeconds = DEFAULT_HEARTBEAT_SECONDS,
            messageStoreIn = new MessageBuffer(),
            messageStoreOut = new MessageBuffer(),
            tlsOptions = {},
            tlsSkipStdInPipe = false,
            onMessage = this.onMessageCallback,
            onOpen = this.onOpenCallback,
            onError = this.onErrorCallback,
            onClose = this.onCloseCallback,
            onReady = this.onReadyCallback,
        } = options;
        this.logger = new Logger(
            this.logOptions
                ? {
                      name: this.logOptions.name ?? 'fixserver',
                      ...this.logOptions,
                  }
                : {
                      name: 'fixserver',
                      format: 'json',
                  },
        );
        if (!this.logging) {
            this.logger.silent = true;
        }
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }

        this.connectionType = 'acceptor';
        this.fixVersion = parseFixVersion(this.fixVersion);
        this.fixParser.fixVersion = parseFixVersion(this.fixVersion);
        this.messageStoreIn = messageStoreIn;
        this.messageStoreOut = messageStoreOut;
        this.protocol = protocol;
        this.sender = sender;
        this.target = target;
        this.port = port;
        this.host = host;
        if (tlsOptions) this.tlsOptions = tlsOptions as TlsOptions;
        if (tlsSkipStdInPipe) this.tlsSkipStdInPipe = tlsSkipStdInPipe;
        this.fixParser.sender = sender;
        this.fixParser.target = target;
        this.heartBeatInterval = heartbeatIntervalSeconds;

        this.onMessageCallback = onMessage;
        this.onOpenCallback = onOpen;
        this.onErrorCallback = onError;
        this.onCloseCallback = onClose;
        this.onReadyCallback = onReady;

        this.initialize();
    }

    /**
     * Initializes the server by setting up the appropriate connection based on the specified protocol.
     *
     * - This method sets the `messageCounter` to zero, which tracks the number of messages sent.
     * - Depending on the `protocol` (either 'tcp' or 'websocket'), it calls the appropriate server creation method.
     * - If the protocol is invalid, an error is logged indicating the issue.
     *
     * @returns {void}
     */
    private initialize() {
        this.messageCounter = 0;
        if (this.protocol === 'tcp') {
            this.setupTcpServer();
        } else if (this.protocol === 'tls-tcp') {
            this.setupTcpTlsServer();
        } else if (this.protocol === 'websocket') {
            this.createWebsocketServer();
        } else {
            this.logger.logError({
                message: `FIXServer: Create server, invalid protocol: ${this.protocol.toUpperCase()}`,
            });
        }
    }

    /**
     * Creates and starts a TCP server to handle incoming connections.
     * The server listens for incoming client connections on the specified `host` and `port`.
     * For each connection, it sets up socket event listeners to manage data transmission,
     * connection lifecycle (open, close, error), and communication processing.
     *
     * This method is used to create a standard TCP server and is invoked when the `protocol`
     * is set to `'tcp'`.
     *
     * @returns {void}
     *
     * @throws {Error} Will throw an error if the server fails to start or if there is a problem
     * with socket connection handling.
     */
    private setupTcpServer() {
        this.server = createTcpServer((socket: Socket) => {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Connection established`,
            });
            this.connected = true;
            this.onOpenCallback?.();
            this.setupSocket(socket);
        });

        this.server.listen(this.port, this.host, () => {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Listening for connections at ${this.host}:${this.port}...`,
            });
            setTimeout(() => this.onReadyCallback?.(), READY_MS);
        });
    }

    /**
     * Sets up and starts a TCP/TLS server to handle incoming connections.
     * The server is created using the `createTcpTlsServer` function and listens for TLS-TCP
     * connections based on the provided `tlsOptions`. The method sets up the
     * necessary event listeners to handle connection events, such as data reception,
     * connection establishment, and errors.
     *
     * @returns {void}
     *
     * @throws {Error} Will throw an error if the server fails to start or if there is an issue
     * with the socket connection or TLS setup.
     */
    private setupTcpTlsServer() {
        try {
            this.server = createTcpTlsServer(
                this.tlsOptions ?? ({ host: this.host, port: this.port } as TlsOptions),
                (socket: TLSSocket) => {
                    this.logger.log({
                        level: 'info',
                        message: `FIXServer (${this.protocol.toUpperCase()}): -- Connection established`,
                    });
                    this.connected = true;
                    this.onOpenCallback?.();
                    this.setupSocket(socket);
                },
            );
        } catch (error: unknown) {
            if (error instanceof Error) {
                this.logger.logError({
                    level: 'info',
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Server at ${this.host}:${this.port} failed with error: ${error.message}.`,
                });
            } else {
                this.logger.logError({
                    level: 'info',
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Server at ${this.host}:${this.port} failed with unspecified error.`,
                });
            }
            return;
        }

        this.server.listen(this.port, this.host, () => {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Listening for connections at ${this.host}:${this.port}...`,
            });
            setTimeout(() => this.onReadyCallback?.(), READY_MS);
        });
    }

    /**
     * Sets up event listeners and data handling for the given socket.
     * This function is used for both TCP and TLS-TCP connections to handle
     * incoming data, connection lifecycle events (open, close, timeout, error),
     * and manages communication state (such as heartbeat, message buffer, etc.).
     *
     * @param {Socket | TLSSocket} socket - The socket object representing the client connection.
     * This can be a regular TCP socket (`net.Socket`) or a TLS socket (`TLSSocket`).
     *
     * @returns {void}
     *
     * @throws {Error} Will throw an error if there is an issue with socket events or data processing.
     */
    private setupSocket(socket: Socket | TLSSocket) {
        this.socket = socket;

        this.socket.pipe(new FrameDecoder()).on('data', (data: string) => {
            this.connected = true;
            const messages: Message[] = this.parse(data.toString());
            for (const message of messages) {
                serverProcessMessage(this, message);
                this.messageStoreIn.add(message);
                this.onMessageCallback?.(message);
            }
        });

        this.socket.on('connect', () => {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Connection established`,
            });
            this.connected = true;
            this.onOpenCallback?.();
        });

        this.socket.on('close', () => {
            this.connected = false;
            this.stopHeartbeat();
            this.resetSession();
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Closed connection`,
            });
            this.onCloseCallback?.();
        });

        this.socket.on('timeout', () => {
            this.connected = false;
            this.stopHeartbeat();
            this.close();
            this.resetSession();
            this.logger.logError({
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Connection timeout`,
            });
            this.onCloseCallback?.();
        });

        this.socket.on('error', (error: Error) => {
            this.connected = false;
            this.stopHeartbeat();
            this.close();
            this.resetSession();
            this.logger.logError({
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Error`,
                error,
            });
            this.onErrorCallback?.(error);
        });
    }

    /**
     * Creates a WebSocket server that listens for incoming connections and handles communication.
     *
     * - Sets up a WebSocket server with specified `host` and `port`.
     * - Handles various WebSocket events: 'connection', 'message', 'close', 'error', and 'listening'.
     * - Logs connection status, errors, and message reception using the logger.
     * - Processes incoming messages and invokes the appropriate callbacks for message handling, connection open/close, and errors.
     *
     * @returns {void}
     */
    private createWebsocketServer() {
        const serverOptions: ServerOptions = {
            host: this.host,
            port: this.port,
        };
        this.server = new WebSocketServer(serverOptions);
        this.server.on('connection', (socket) => {
            this.connected = true;
            socket.on('message', (data: string | Buffer) => {
                const messages: Message[] = this.parse(data.toString());
                let i = 0;
                for (i; i < messages.length; i++) {
                    serverProcessMessage(this, messages[i]);
                    this.messageStoreIn.add(messages[i]);
                    this.onMessageCallback?.(messages[i]);
                }
            });
        });
        this.server.on('close', () => {
            this.connected = false;
            this.stopHeartbeat();
            this.onCloseCallback?.();
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Closed connection`,
            });
        });
        this.server.on('error', (error) => {
            this.connected = false;
            this.stopHeartbeat();
            this.close();
            this.onErrorCallback?.(error);
            this.logger.logError({
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Error`,
                error,
            });
        });
        this.server.on('listening', () => {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Listening for connections at ${this.host}:${
                    this.port
                }...`,
            });
            setTimeout(() => this.onReadyCallback?.(), READY_MS);
        });
    }

    /**
     * Get the next outgoing message sequence number.
     *
     * @returns The next outgoing message sequence number.
     */
    public getNextTargetMsgSeqNum(): number {
        return this.fixParser.getNextTargetMsgSeqNum();
    }

    /**
     * Set the next outgoing message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns The next outgoing message sequence number.
     */
    public setNextTargetMsgSeqNum(nextMsgSeqNum: number): number {
        return this.fixParser.setNextTargetMsgSeqNum(nextMsgSeqNum);
    }

    /**
     * Get current timestamp.
     *
     * @param dateObject - An instance of a Date class.
     * @returns The current timestamp.
     */
    public getTimestamp(dateObject = new Date()): string {
        return this.fixParser.getTimestamp(dateObject);
    }

    /**
     * Create an instance of a FIX Message.
     *
     * @param fields - An array of Fields.
     * @returns A FIX Message class instance.
     */
    public createMessage(...fields: Field[]): Message {
        return this.fixParser.createMessage(...fields);
    }

    /**
     * Parse a FIX message string into Message instance(s).
     *
     * @param data - FIX message string.
     * @returns FIX Message class instance(s).
     */
    public parse(data: string): Message[] {
        return this.fixParser.parse(data);
    }

    /**
     * Send a FIX message.
     *
     * @param message - FIX Message class instance.
     */
    public send(message: Message): void {
        if (!LicenseManager.validateLicense()) {
            throw new Error(LicenseManager.ERROR_MESSAGE_NO_LICENSE);
        }
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            const encodedMessage: string = message.encode();
            this.fixParser.setNextTargetMsgSeqNum(this.fixParser.getNextTargetMsgSeqNum() + 1);
            if (!socket.write(encodedMessage)) {
                this.logger.logError({
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Could not send message, socket not open`,
                    fix: encodedMessage.replace(/\x01/g, '|'),
                });
            } else {
                this.messageStoreOut.add(message.clone());
                this.restartHeartbeat();
                this.logger.log({
                    level: 'info',
                    message: `FIXServer (${this.protocol.toUpperCase()}): >> sent`,
                    fix: encodedMessage.replace(/\x01/g, '|'),
                });
            }
        } else if (this.protocol === 'websocket') {
            const server: WebSocketServer = this.server! as WebSocketServer;
            const encodedMessage: string = message.encode();
            if (server?.clients && server.clients.size > 0) {
                server.clients.forEach((client: WebSocket) => {
                    if (client.readyState === client.OPEN) {
                        this.fixParser.setNextTargetMsgSeqNum(this.fixParser.getNextTargetMsgSeqNum() + 1);
                        client.send(encodedMessage);
                        this.messageStoreOut.add(message.clone());
                        this.restartHeartbeat();
                        this.logger.log({
                            level: 'info',
                            message: `FIXServer (${this.protocol.toUpperCase()}): >> sent`,
                            fix: encodedMessage.replace(/\x01/g, '|'),
                        });
                    }
                });
            } else {
                this.logger.logError({
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Could not send message, socket not connected`,
                    fix: message,
                });
            }
        }
    }

    /**
     * Get connection status.
     *
     * @returns Current connection status.
     */
    public isConnected(): boolean {
        return this.connected;
    }

    /**
     * Resets the current session.
     */
    private resetSession() {
        this.isLoggedIn = false;
        this.messageCounter = 0;
    }

    /**
     * Close current connection.
     */
    public close(): void {
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket! as Socket;
            const server: Server = this.server! as Server;
            if (socket?.end) {
                socket.end(() => {
                    if (server) {
                        server.close(() => {
                            this.logger.log({
                                level: 'info',
                                message: `FIXServer (${this.protocol.toUpperCase()}): -- Ended session`,
                            });
                            this.initialize();
                        });
                    }
                });
            }
        } else if (this.protocol === 'websocket') {
            const server: WebSocketServer = this.server! as WebSocketServer;
            if (server) {
                server.clients.forEach((client: WebSocket) => {
                    client.close();
                });
                server.close(() => {
                    this.logger.log({
                        level: 'info',
                        message: `FIXServer (${this.protocol.toUpperCase()}): -- Ended session`,
                    });
                    this.initialize();
                });
            }
        }
    }

    /**
     * Destroys the server and closes the connection.
     *
     * - Stops the heartbeat mechanism to prevent any ongoing activity.
     * - If the protocol is TCP, it destroys the socket and closes the server.
     * - If the protocol is WebSocket, it closes all client connections and shuts down the server.
     * - Logs the server destruction event.
     *
     * @returns {void}
     */
    public destroy(): void {
        this.stopHeartbeat();
        if (this.protocol === 'tcp') {
            const socket: Socket = this.socket as Socket;
            const server: Server = this.server as Server;
            socket?.destroy();
            server?.close(() => {
                this.logger.log({
                    level: 'info',
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Destroyed`,
                    fix: undefined,
                });
            });
        } else if (this.protocol === 'websocket') {
            const server: WebSocketServer = this.server as WebSocketServer;
            server?.clients.forEach((client: WebSocket) => {
                client.close();
            });
            server?.close(() => {
                this.logger.log({
                    level: 'info',
                    message: `FIXServer (${this.protocol.toUpperCase()}): -- Destroyed`,
                });
            });
        }
    }

    /**
     * Stop heartbeat interval.
     */
    public stopHeartbeat(): void {
        clearInterval(this.heartBeatIntervalId);
    }

    /**
     * Restart heartbeat interval.
     */
    public restartHeartbeat(): void {
        this.stopHeartbeat();
        this.startHeartbeat(this.heartBeatInterval, true);
    }

    /**
     * Start heartbeat interval.
     *
     * @param heartBeatInterval - Heartbeat interval in seconds.
     * @param disableLog - Whether to disable heartbeat logs.
     */
    public startHeartbeat(heartBeatInterval: number = this.heartBeatInterval, disableLog?: boolean): void {
        this.stopHeartbeat();
        if (!disableLog) {
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): -- Heartbeat configured to ${heartBeatInterval} seconds`,
            });
        }
        this.heartBeatInterval = heartBeatInterval;
        this.heartBeatIntervalId = setInterval(() => {
            const heartBeatMessage: Message = heartBeat(this);
            this.send(heartBeatMessage);
            const encodedMessage: string = heartBeatMessage.encode();
            this.logger.log({
                level: 'info',
                message: `FIXServer (${this.protocol.toUpperCase()}): >> sent Heartbeat`,
                fix: encodedMessage.replace(/\x01/g, '|'),
            });
        }, this.heartBeatInterval * 1000);
    }
}

export * from './fieldtypes';
export type { TlsOptions } from 'node:tls';
export { Logger } from './logger/Logger';
export { LicenseManager } from './licensemanager/LicenseManager';
export { MessageBuffer };
export { Constants };
export { Field };
export { Message };
export { FIXServer };
export type { IFIXParser };
export type { LogMessage, Level, ILogTransporter } from 'fixparser-common';
export type { FIXValue } from './util/util';
export type { LogOptions, Format } from './logger/Logger';
export type { IMessageStore } from 'fixparser-common';
export type { Protocol, Options, BaseOptions } from './FIXParserBase';
export type { MessageError } from './message/Message';
