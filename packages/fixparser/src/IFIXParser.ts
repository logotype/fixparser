import type { KeyObject } from 'node:tls';
import type { ProxyAgent } from 'proxy-agent';

import type { IMessageStore } from 'fixparser-common';
import type { FIXParser } from './FIXParser';
import type { ConnectionType, FIXParserBase, Options as FIXParserOptions, Protocol } from './FIXParserBase';
import type { Options as FIXParserBrowserOptions } from './FIXParserBrowser';
import type { Field } from './fields/Field';
import type { Logger } from './logger/Logger';
import type { Message } from './message/Message';
import type { Parser } from './util/util';

export interface IFIXParser {
    /* Target hostname or IP address */
    host: string | undefined;
    /* Target port */
    port: number | undefined;
    /* Type of protocol */
    protocol: Protocol | undefined;
    /* Assigned value used to identify firm sending message, SenderCompID tag 49 */
    sender: string | undefined;
    /* Assigned value used to identify receiving firm, TargetCompID tag 56 */
    target: string | undefined;
    /* Heartbeat interval in seconds */
    heartBeatInterval: number;
    /* Protocol version, defaults to "FIXT.1.1" used by BeginString tag 8 */
    fixVersion: string;
    /* Indicates whether this instance is acceptor or initiator */
    connectionType: ConnectionType;
    /* Name of current parser name */
    parserName: Parser;
    /* Instance of FIXParser base class */
    fixParserBase?: FIXParserBase;
    /* Message counter */
    messageCounter?: number;
    /* Heartbeat interval ID */
    heartBeatIntervalId: ReturnType<typeof setInterval> | undefined;
    /* Current connection status */
    connected: boolean;
    /* Message buffer of 2500 incoming messages */
    messageStoreIn: IMessageStore<Message>;
    /* Message buffer of 2500 outgoing messages */
    messageStoreOut: IMessageStore<Message>;
    /* Instance of FIXParser class */
    fixParser?: FIXParser;
    /* Whether current FIX session has logged in */
    isLoggedIn?: boolean;
    /* Whether an outgoing message contains a Logout */
    requestedLogout?: boolean;
    /* Logger instance */
    logger: Logger;
    /* Proxy */
    proxy?: ProxyAgent;
    /* TLS Private keys in PEM format */
    tlsKey?: string | Buffer | Array<string | Buffer | KeyObject>;
    /* TLS Certificate chains in PEM format */
    tlsCert?: string | Buffer | Array<string | Buffer>;
    /* TLS Use Server Name Indication extension (requires FQDN hostname) */
    tlsUseSNI?: boolean;
    /* TLS Skip Standard Input pipe of socket */
    tlsSkipStdInPipe?: boolean;
    /* Skip validation of fix messages */
    skipValidation?: boolean;

    /**
     * Connect to a remote FIX server/gateway.
     * @param options - Connection options.
     */
    connect?(options: FIXParserOptions | FIXParserBrowserOptions): void;

    /**
     * Get the next outgoing message sequence number.
     *
     * @returns The next outgoing message sequence number.
     */
    getNextTargetMsgSeqNum(): number;

    /**
     * Set the next outgoing message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns The next outgoing message sequence number.
     */
    setNextTargetMsgSeqNum(nextMsgSeqNum: number): number;

    /**
     * Get current timestamp.
     *
     * @param dateObject - An instance of a Date class.
     * @returns The current timestamp.
     */
    getTimestamp(dateObject: Date): string;

    /**
     * Create an instance of a FIX Message.
     *
     * @param fields - An array of Fields.
     * @returns A FIX Message class instance.
     */
    createMessage(...fields: Field[]): Message;

    /**
     * Parse a FIX message string into Message instance(s).
     *
     * @param data - FIX message string.
     * @returns FIX Message class instance(s).
     */
    parse(data: string): Message[];

    /**
     * Send a FIX message.
     *
     * @param message - FIX Message class instance.
     */
    send(message: Message): void;

    /**
     * Get connection status.
     *
     * @returns Current connection status.
     */
    isConnected(): boolean;

    /**
     * Close current connection.
     */
    close(): void;

    /**
     * Stop heartbeat interval.
     */
    stopHeartbeat(): void;

    /**
     * Restart heartbeat interval.
     */
    restartHeartbeat(): void;

    /**
     * Start heartbeat interval.
     *
     * @param heartBeatInterval - Heartbeat interval in seconds.
     * @param disableLog - Whether to disable heartbeat logs.
     */
    startHeartbeat(heartBeatInterval: number, disableLog?: boolean): void;
}
