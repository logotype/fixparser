import type { ISpecEnums } from '../spec/SpecEnums';

export class EnumType {
    public name: string | undefined = undefined;
    public id: string | undefined = undefined;
    public tag: number | undefined = undefined;
    public type: string | undefined = undefined;
    public codeSet: string | undefined = undefined;
    public value: string | undefined = undefined;
    public sort?: number = undefined;
    public group?: string = undefined;
    public added?: string = undefined;
    public addedEP?: string = undefined;
    public updated?: string = undefined;
    public updatedEP?: string = undefined;
    public deprecated?: string = undefined;
    public deprecatedEP?: string = undefined;
    public description?: string = undefined;

    public setEnumeration(enumType: ISpecEnums): void {
        this.name = enumType.name;
        this.id = enumType.id;
        this.tag = enumType.tag;
        this.type = enumType.type;
        this.codeSet = enumType.codeSet;
        this.value = enumType.value;
        this.sort = enumType.sort;
        this.group = enumType.group;
        this.added = enumType.added;
        this.addedEP = enumType.addedEP;
        this.updated = enumType.updated;
        this.updatedEP = enumType.updatedEP;
        this.deprecated = enumType.deprecated;
        this.deprecatedEP = enumType.deprecatedEP;
        this.description = enumType.description;
    }
}
