/**
 * This example will connect to the C++ QuickFIX engine.
 * Clone https://github.com/quickfix/quickfix
 * build and run ./bin/run_executor_cpp.sh.
 *
 * FIX session flow:
 * Initiator (us) connects over TCP to port 5001 on the QuickFIX server.
 * Upon connection, we send a Logon message. QuickFIX responds to the Logon message.
 * Next, we send a NewOrderSingle. QuickFIX responds with a ExecutionReport.
 * Next, we send a Logout request message. QuickFIX responds with a Logout response message.
 * Session is closed.
 */
import {
    ConsoleLogTransport,
    EncryptMethod,
    FIXParser,
    Field,
    Fields,
    HandlInst,
    LicenseManager,
    type Message,
    Messages,
    type Options,
    OrdType,
    ResetSeqNumFlag,
    Side,
    TimeInForce,
} from 'fixparser';

// NOTE: This feature requires a FIXParser Pro license
LicenseManager.setLicenseKey(process.env.FIXPARSER_LICENSE_KEY);

const fixParser = new FIXParser();
const SENDER = 'CLIENT2';
const TARGET = 'EXECUTOR';
const CONNECT_PARAMS: Options = {
    host: 'localhost',
    port: 5001,
    protocol: 'tcp',
    sender: SENDER,
    target: TARGET,
    fixVersion: 'FIX.4.4',
    logging: true,
    logOptions: {
        name: SENDER,
        level: 'info',
        format: 'json',
        transport: new ConsoleLogTransport({ format: 'console' }),
    },
    onOpen: () => {
        console.log('Open');
        sendLogon();
        sendOrder(32768);
        sendLogout();
    },
    onMessage: (message: Message) =>
        console.log('received message', message.description, message.messageString.replace(/\x01/g, '|')),
    onClose: () => {
        console.log('Disconnected');
    },
};

const sendLogon = () => {
    const logon = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.Logon),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.ResetSeqNumFlag, ResetSeqNumFlag.Yes),
        new Field(Fields.EncryptMethod, EncryptMethod.None),
        new Field(Fields.HeartBtInt, 10),
    );
    const messages = fixParser.parse(logon.encode());
    console.log('sending message', messages[0].description, messages[0].messageString);
    fixParser.send(logon);
};

const sendLogout = () => {
    const logout = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.Logout),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.Text, 'Logout request'),
    );
    const messages = fixParser.parse(logout.encode());
    console.log('sending message', messages[0].description, messages[0].messageString);
    fixParser.send(logout);
};

const sendOrder = (orderId: number) => {
    const newOrderSingle = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.NewOrderSingle),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.ClOrdID, String(orderId).padStart(4, '0')),
        new Field(Fields.Side, Side.Buy),
        new Field(Fields.Symbol, 'MSFT'),
        new Field(Fields.OrderQty, 10000),
        new Field(Fields.Price, 100),
        new Field(Fields.OrdType, OrdType.Limit),
        new Field(Fields.HandlInst, HandlInst.ManualOrder),
        new Field(Fields.TimeInForce, TimeInForce.Day),
        new Field(Fields.Text, 'NewOrderSingle'),
        new Field(Fields.TransactTime, fixParser.getTimestamp()),
    );
    const messages = fixParser.parse(newOrderSingle.encode());
    console.log('sending message', messages[0].description, messages[0].messageString.replace(/\x01/g, '|'));
    fixParser.send(newOrderSingle);
};

fixParser.connect(CONNECT_PARAMS);
