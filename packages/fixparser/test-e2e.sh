#!/usr/bin/env bash
set -e

if [ -z "$FIXPARSER_LICENSE_KEY" ]; then
  echo "FIXPARSER_LICENSE_KEY environment variable is not set!"
  exit 1
fi

echo -e "\e[92mRUNNING BUILD \e[39m"
./../../node_modules/turbo/bin/turbo build
mkdir test_e2e
echo -e "\e[92mCOPYING DEMO FILE \e[39m"
cp examples/example_parse_message.ts test_e2e
cp examples/example_e2e.ts test_e2e
echo -e "\e[92mPREPARING TCP SERVER TEST \e[39m"
awk '{gsub(/process.env.FIXPARSER_LICENSE_KEY/, "\"" ENVIRON["FIXPARSER_LICENSE_KEY"] "\""); print}' test_e2e/example_e2e.ts > temp_file && mv temp_file test_e2e/example_e2e.ts
echo -e "\e[92mRUNNING NPM PACK \e[39m"
npm pack --pack-destination="./test_e2e"
echo -e "\e[92mRENAMING PACKAGE \e[39m"
cd test_e2e
for file in fixparser-*.tgz; do mv "$file" "$(echo $file | sed -E 's/-[0-9]+\.[0-9]+\.[0-9]+//')"; done
echo -e "\e[92mINITIALIZING E2E MODULE \e[39m"
if [ "$1" == "esm" ]; then
echo '{
        "name": "fixparser-test-e2e-esm",
        "version": "0.0.0",
        "private": true,
        "type": "module",
        "license": "Proprietary"
      }' > package.json
else
echo '{
        "name": "fixparser-test-e2e",
        "version": "0.0.0",
        "private": true,
        "license": "Proprietary"
      }' > package.json
fi
echo -e "\e[92mINSTALLING DEPENDENCIES \e[39m"
npm install ./fixparser.tgz tsx
echo -e "\e[92mEXECUTING END TO END TEST \e[39m"
npx tsx ./example_parse_message.ts && npx tsx ./example_e2e.ts
cd ..
rm -rf test_e2e
