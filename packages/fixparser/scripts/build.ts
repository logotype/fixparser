import { readFile, writeFile } from 'node:fs';
import { join } from 'node:path';
import { type BuildOptions, build as esbuild } from 'esbuild';
import { buildTime, packageVersion, releaseInformation } from '../globals';

const baseConfig: BuildOptions = {
    bundle: true,
    minify: false,
    target: 'esnext',
    sourcemap: true,
    nodePaths: [join(__dirname, '../src')],
    external: [],
    define: {
        __PACKAGE_VERSION__: JSON.stringify(packageVersion),
        __BUILD_TIME__: JSON.stringify(buildTime),
        __RELEASE_INFORMATION__: JSON.stringify(releaseInformation),
    },
};

const nodeConfig: BuildOptions = {
    ...baseConfig,
    format: 'cjs',
    platform: 'node',
};

const browserConfig: BuildOptions = {
    ...baseConfig,
    format: 'cjs',
    platform: 'browser',
};

const main = async (): Promise<void> => {
    await esbuild({
        ...nodeConfig,
        outdir: join(__dirname, '../build/cjs'),
        entryPoints: [join(__dirname, './../src/FIXParser.ts')],
    });
    await esbuild({
        ...nodeConfig,
        outdir: join(__dirname, '../build/cjs'),
        entryPoints: [join(__dirname, './../src/FIXServer.ts')],
    });
    await esbuild({
        ...browserConfig,
        outdir: join(__dirname, '../build/cjs'),
        entryPoints: [join(__dirname, './../src/FIXParserBrowser.ts')],
    });

    await esbuild({
        ...nodeConfig,
        format: 'esm',
        outExtension: {
            '.js': '.mjs',
        },
        outdir: join(__dirname, '../build/esm'),
        entryPoints: [join(__dirname, './../src/FIXParser.ts')],
        banner: {
            js: "import { createRequire } from 'module';const require = createRequire(import.meta.url);",
        },
    });
    await esbuild({
        ...nodeConfig,
        format: 'esm',
        outExtension: {
            '.js': '.mjs',
        },
        outdir: join(__dirname, '../build/esm'),
        entryPoints: [join(__dirname, './../src/FIXServer.ts')],
        banner: {
            js: "import { createRequire } from 'module';const require = createRequire(import.meta.url);",
        },
    });
    await esbuild({
        ...browserConfig,
        format: 'esm',
        outExtension: {
            '.js': '.mjs',
        },
        outdir: join(__dirname, '../build/esm'),
        entryPoints: [join(__dirname, './../src/FIXParserBrowser.ts')],
    });
};

const patch = (file: string, searchReplaceMap: Map<string, string>) => {
    const filePath = join(__dirname, file);

    readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading the file:', err);
            return;
        }

        let updatedData = data;
        searchReplaceMap.forEach((replaceString, searchString) => {
            // Using RegEx to safely handle multiline strings and special characters
            const searchRegex = new RegExp(escapeRegExp(searchString), 'g');
            updatedData = updatedData.replace(searchRegex, replaceString);
        });

        writeFile(filePath, updatedData, 'utf8', (err) => {
            if (err) {
                console.error('Error writing to the file:', err);
            } else {
                console.log(`${file} updated successfully!`);
            }
        });
    });
};

const escapeRegExp = (string: string) => {
    return string.replace(/[.*+?^=!:${}()|\[\]\/\\]/g, '\\$&');
};

if (require.main === module) {
    main().then(() => {
        // Fix packaging issues with OpenPGP due to ESBuild issues
        // https://github.com/openpgpjs/openpgpjs/issues/1814
        // https://github.com/evanw/esbuild/issues/1492
        const searchReplaceMap = new Map([
            [
                `var createRequire = () => () => {
};`,
                '',
            ],
            ['createRequire()', 'createRequire(import.meta.url)'],
        ]);

        patch('./../build/esm/FIXParser.mjs', searchReplaceMap);
        patch('./../build/esm/FIXServer.mjs', searchReplaceMap);
    });
}
