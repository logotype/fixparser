import fs from 'node:fs/promises';
import path from 'node:path';

const rmrf = async (pathFromRoot: string): Promise<void> => {
    await fs.rm(path.join(__dirname, '../', pathFromRoot), {
        recursive: true,
        force: true,
    });
};

const main = async (): Promise<void> => {
    try {
        await Promise.all([rmrf('build'), rmrf('test-reports'), rmrf('types')]);
    } catch (error) {
        console.error('Failed to clean directories:', error);
        throw error;
    }
};

if (require.main === module) {
    main();
}
