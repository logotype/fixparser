export declare module './c' {
    export function serialize(x: any): ArrayBuffer;
    export function deserialize(x: ArrayBuffer): any;

    export function uncompress(b: Uint8Array | ArrayBuffer): Uint8Array;

    export function u8u16(u16: string): Uint8Array;
    export function u16u8(u8: Uint8Array): string;
    export function compressed(x: ArrayBuffer): boolean;
}
