import type { IFIXParser, IMessageStore, Logger } from 'fixparser';
import { type IMessage, uuidv4 } from 'fixparser-common';
import { MessageBuffer } from 'fixparser-common';
import { WebSocket } from 'ws';

import { deserialize, serialize } from './c';

export type MessageStoreKDBOptions = {
    host: string;
    port: number;
    tableName?: string;
    parser: IFIXParser;
    maxBufferSize?: number;
    writeInterval?: number;
    logger?: Logger;
    onReady: () => void;
};

export class MessageStoreKDB implements IMessageStore<IMessage> {
    /**
     * A number representing the next expected message sequence number.
     * @private
     */
    private nextMsgSeqNum = 1;
    private readonly inMemoryMessageStore: IMessageStore<IMessage> = new MessageBuffer(1048576);
    private readonly host: string;
    private readonly port: number;
    private readonly tableName: string = 'FIXParser';
    private readonly ws: WebSocket;
    private readonly parser: IFIXParser;
    private readonly logger: Logger | undefined;
    /* Time in milliseconds between writes to KDB+ */
    private readonly writeInterval: number = 5000;
    private readonly writeIntervalId: NodeJS.Timeout;
    private maxBufferSize: number;

    constructor({
        host,
        port,
        tableName,
        parser,
        logger,
        onReady,
        maxBufferSize = 2500,
        writeInterval = 5000,
    }: MessageStoreKDBOptions) {
        this.host = host;
        this.port = port;
        this.maxBufferSize = maxBufferSize;
        this.writeInterval = writeInterval;
        this.parser = parser;
        if (tableName) this.tableName = tableName;
        if (logger) this.logger = logger;

        this.logger?.log({
            level: 'info',
            message: `FIXParser (MessageStoreKDB): -- Connecting to ws://${this.host}:${this.port}...`,
        });

        this.ws = new WebSocket(`ws://${this.host}:${this.port}`);
        this.ws.binaryType = 'arraybuffer';
        this.ws.onopen = async () => {
            this.logger?.log({
                level: 'info',
                message: `FIXParser (MessageStoreKDB): -- Successfully opened connection to ws://${this.host}:${this.port}...`,
            });

            // Check if table exists
            const tables = await this.sendAsync('tables[]');
            if (tables?.includes(this.tableName)) {
                this.logger?.log({
                    level: 'info',
                    message: `FIXParser (MessageStoreKDB): -- ${this.tableName} table found.`,
                });
                onReady();
            } else {
                this.logger?.log({
                    level: 'info',
                    message: 'FIXParser (MessageStoreKDB): -- Setting up FIXParser table...',
                });

                // Table structure
                this.send(`meta ${this.tableName}:([] msgSequence:\`int$(); message:())`);

                // Set msgSequence as primary key to prevent duplicates
                this.send(`\`msgSequence xkey \`${this.tableName}`);

                onReady();
            }
        };

        this.ws.onclose = () => {
            this.logger?.log({
                level: 'info',
                message: 'FIXParser (MessageStoreKDB): -- Closed',
            });
            this.stopWriteInterval();
        };

        this.ws.onerror = (error) => {
            this.logger?.log({
                level: 'error',
                message: 'FIXParser (MessageStoreKDB): -- Error:',
                error,
            });
            this.stopWriteInterval();
        };

        // Set up an interval to write the in-memory store to KDB+ asynchronously
        this.writeIntervalId = setInterval(() => this.writeToKDB(), this.writeInterval);
    }

    private send(message: string): void {
        const uuid = uuidv4().replace(/-/g, '');
        const messageWithUUID = `enlist \`${uuid}, ${message}`;
        if (this.ws.readyState !== WebSocket.OPEN) {
            this.logger?.log({
                level: 'warn',
                message: `FIXParser (MessageStoreKDB): -- WebSocket not ready! Ignoring command ${message}...`,
            });
        }
        this.ws.onmessage = (event) => {
            try {
                const data = deserialize(event.data as ArrayBuffer);
                void data;
            } catch (error) {
                this.logger?.log({
                    level: 'warn',
                    message: `FIXParser (MessageStoreKDB): -- Error parsing response to message id ${uuid}`,
                });
            }
        };
        this.ws.send(serialize(messageWithUUID));
    }

    private sendAsync(message: string): Promise<any> {
        const uuid = uuidv4().replace(/-/g, '');
        const messageWithUUID = `enlist \`${uuid}, ${message}`;
        if (this.ws.readyState !== WebSocket.OPEN) {
            this.logger?.log({
                level: 'warn',
                message: `FIXParser (MessageStoreKDB): -- WebSocket not ready! Ignoring command ${message}...`,
            });
            return Promise.resolve(undefined);
        }
        return new Promise((resolve, reject) => {
            this.ws.onmessage = (event) => {
                try {
                    const data = deserialize(event.data as ArrayBuffer);
                    const receivedUUID = data[0][0];
                    const response = data[0].slice(1);
                    if (receivedUUID === uuid) {
                        resolve(response);
                    }
                } catch (error) {
                    this.logger?.log({
                        level: 'warn',
                        message: `FIXParser (MessageStoreKDB): -- Error parsing response to message id ${uuid}`,
                    });
                    reject(new Error(`Error parsing response to message id ${uuid}`));
                }
            };
            this.ws.send(serialize(messageWithUUID));
        });
    }

    private async writeToKDB() {
        const inMemoryMessages = this.inMemoryMessageStore.getAll();
        const clearSequences: number[] = [];
        if (inMemoryMessages.length > 0) {
            for (const message of inMemoryMessages) {
                clearSequences.push(message.messageSequence);
                const messageString = message.encode('|');
                const response = await this.sendAsync(
                    `\`${this.tableName} upsert (${message.messageSequence}i;"${messageString}")`,
                );
                void response;
            }

            for (const seq of clearSequences) {
                this.inMemoryMessageStore.remove(seq);
            }
        }
    }

    public add(message: IMessage) {
        // Add the message to the in-memory store first
        this.inMemoryMessageStore.add(message);

        // Check if we need to write based on buffer size
        const bufferLength = this.size();
        if (bufferLength >= this.maxBufferSize) {
            this.writeToKDB();
        }
    }

    public getByMsgSequence(msgSequence: number): IMessage | undefined {
        throw new Error('Not implemented, please use getByMsgSequenceAsync()');
    }

    public async getByMsgSequenceAsync(msgSequence: number): Promise<IMessage | undefined> {
        const getRowById = `select message from ${this.tableName} where msgSequence=${msgSequence}i`;
        const response = await this.sendAsync(getRowById);
        if (response?.length > 0) {
            return this.parser.parse(response[0].message)[0];
        }
        return undefined;
    }

    /**
     * Updates a `Message` instance in the store.
     *
     * @param {number} msgSequence - The sequence number of the message to update.
     * @param {Message} message - The updated message.
     *
     * @returns {boolean} - Returns `true` if the message was updated successfully, `false` otherwise.
     */
    public update(msgSequence: number, message: IMessage): boolean {
        const updateMessage = `${this.tableName}:${this.tableName} upsert (${msgSequence}i; "${message.encode('|')}")`;
        this.send(updateMessage);
        return true;
    }

    /**
     * Removes a `Message` instance from the store by its sequence number.
     *
     * @param {number} msgSequence - The sequence number of the message to remove.
     *
     * @returns {void}
     */
    public remove(msgSequence: number): void {
        // Remove from in-memory store first
        this.inMemoryMessageStore.remove(msgSequence);

        // Remove from KDB+ if necessary
        const deleteMessage = `delete from ${this.tableName} where msgSequence=${msgSequence}`;
        this.send(deleteMessage);
    }

    /**
     * Checks if a message with a given sequence number exists in the store.
     *
     * @param {number} msgSequence - The sequence number of the message to check.
     * @returns {boolean} - `true` if the message exists, `false` otherwise.
     */
    public exists(msgSequence: number): boolean {
        // Check in-memory store first
        if (this.inMemoryMessageStore.exists(msgSequence)) {
            return true;
        }

        return false;
        // If not found in memory, check KDB+
        // const query = `select from ${this.tableName} where msgSequence=${msgSequence}`;
        // const response = this.send(query);
        //
        // // Return true if found in KDB+
        // return response?.length > 0;
    }

    /**
     * Retrieves all `Message` instances from the store.
     *
     * @returns {Message[]} - An array of all `Message` instances in the store.
     */
    public getAll(): IMessage[] {
        // Get messages from in-memory store
        const inMemoryMessages = this.inMemoryMessageStore.getAll();

        return inMemoryMessages;
        // Get messages from KDB+ (if necessary)
        // const query = `select from ${this.tableName}`;
        // const response = this.send(query);
        //
        // const kdbMessages: IMessage[] =
        //     response?.map((row: any) => {
        //         const message = row['message:char'];
        //         return this.parser.parse(message);
        //     }) || [];
        //
        // // Combine both in-memory and KDB+ messages
        // return [...inMemoryMessages, ...kdbMessages];
    }

    /**
     * Retrieves all `Message` instances from the store.
     *
     * @returns {Message[]} - An array of all `Message` instances in the store.
     */
    public async getAllAsync(): Promise<IMessage[]> {
        // Get messages from in-memory store
        const inMemoryMessages = this.inMemoryMessageStore.getAll();

        if (inMemoryMessages.length > 0) {
            return inMemoryMessages;
        }

        // Get messages from KDB+ (if necessary)
        const query = `select from ${this.tableName}`;
        const response = await this.sendAsync(query);

        const messages: IMessage[] = [];
        for (const item of response) {
            const message = this.parser.parse(item.message)[0];
            messages.push(message);
        }

        return Promise.resolve(messages);

        // const kdbMessages: IMessage[] =
        //     response?.map((row: any) => {
        //         const message = row['message:char'];
        //         return this.parser.parse(message);
        //     }) || [];
        //
        // // Combine both in-memory and KDB+ messages
        // return [...inMemoryMessages, ...kdbMessages];
    }

    public size() {
        // Return size of the in-memory store
        return this.inMemoryMessageStore.size();
    }

    /**
     * Resizes the message buffer's capacity.
     *
     * @param {number} newCapacity - The new maximum capacity for the buffer.
     * @returns {void}
     */
    public resize(newCapacity: number): void {
        this.maxBufferSize = newCapacity;
    }

    public clear() {
        // Clear the in-memory store and KDB+ store
        this.inMemoryMessageStore.clear();
        this.send(`delete from ${this.tableName}`);
    }

    public getCapacity(): number {
        return this.maxBufferSize;
    }

    public stopWriteInterval() {
        clearInterval(this.writeIntervalId);
    }

    /**
     * Set the next message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns {number} - The next message sequence number.
     */
    public setNextMsgSeqNum(nextMsgSeqNum: number): number {
        if (nextMsgSeqNum <= 0) {
            throw new Error('Message sequence number must be positive.');
        }
        this.nextMsgSeqNum = nextMsgSeqNum;
        return this.nextMsgSeqNum;
    }

    /**
     * Get the next message sequence number.
     *
     * @returns {number} - The next message sequence number.
     */
    public getNextMsgSeqNum(): number {
        return this.nextMsgSeqNum;
    }
}
