require('dotenv').config();
const fs = require('node:fs');

const globals = require('../../fixparser/globals');
global.__PACKAGE_VERSION__ = globals.packageVersion;
global.__BUILD_TIME__ = globals.buildTime;
global.__RELEASE_INFORMATION__ = globals.releaseInformation;

const file = `${__dirname}/${process.argv[2]}`;
const fileData: string = fs.readFileSync(file, { encoding: 'utf8', flag: 'r' });

const tmpDir = `${__dirname}/.processed-examples`;

if (!fs.existsSync(tmpDir)) {
    fs.mkdirSync(tmpDir);
}

const replaceString = (from: string, to: string, input: string) => input.toString().replace(new RegExp(from, 'g'), to);

let newFileData = fileData;
newFileData = replaceString("'fixparser'", "'../../../fixparser/src/FIXParser'", newFileData);
newFileData = replaceString("'fixparser/FIXServer'", "'../../../fixparser/src/FIXServer'", newFileData);

const newFileDataBuffer = Buffer.from(newFileData, 'utf8');

fs.writeFileSync(`${__dirname}/.processed-examples/${process.argv[2]}`, newFileDataBuffer);

require(`${__dirname}/.processed-examples/${process.argv[2]}`);
