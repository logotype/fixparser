# fixparser-plugin-messagestore-kdb

`fixparser-plugin-messagestore-kdb` is a FIXParser plugin designed for managing a message buffer in a KDB+ instance. It stores and retrieves FIX messages over a WebSocket connection to KDB+, providing an asynchronous interface for interacting with the data.

## Features
- **WebSocket Connection**: Connects to a KDB+ instance via WebSocket to send and receive messages.
- **Message Buffering**: Stores messages in a configurable buffer, with an option to limit the buffer size.
- **FIX Message Parsing**: Uses a provided FIX parser to serialize/deserialize FIX messages.
- **Asynchronous API**: Supports asynchronous operations for adding, retrieving, and managing messages.

## Installation

To install the dependencies, run:

```bash
npm install fixparser-plugin-messagestore-kdb
```

To setup KDB+, copy the files in `kdb-files/` to the installation directory of KDB+, such as ~/q.
Edit `script.q` to configure the listening port.

```bash
chmod +x ./kdb.sh
./kdb.sh
```


## Usage

### Creating a `MessageStoreKDB` Instance

```typescript
import { FIXParser } from 'fixparser';
import { MessageStoreKDB } from 'fixparser-plugin-messagestore-kdb';

const fixParser = new FIXParser();
const store = new MessageStoreKDB({
    host: 'localhost',
    port: 1234,
    parser: fixParser,
    logger: fixParser.logger,
    onReady: () => {
        console.log('Connection to KDB+ established and ready.');
    }
});

fixParser.connect({
    host: 'localhost',
    port: 9878,
    protocol: 'tcp',
    sender: SENDER,
    target: TARGET,
    messageStoreIn: store,
    onOpen: () => {
        console.log('Connection established.');
    },
    onMessage: (message: Message) => console.log('received message', message.description, message.messageString),
    onClose: () => console.log('Disconnected'),
});

```

### Adding a Message to the Buffer

```typescript
const message = new Message(
    new Field(Fields.BeginString, ApplVerID.FIX42),
    new Field(Fields.MsgType, Messages.ExecutionReport),
    new Field(Fields.SenderCompID, 'ABC'),
    new Field(Fields.TargetCompID, 'XYZ'),
    new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
    new Field(Fields.ExecTransType, ExecTransType.New),
);
store.add(message)
    .then(() => console.log('Message added to the buffer.'));
```

### Retrieving a Message by Sequence Number

```typescript
store.getByMsgSequence(1)
    .then((message) => {
        if (message) {
            console.log('Retrieved message:', message);
        } else {
            console.log('Message not found.');
        }
    });
```

### Getting the Buffer Size

```typescript
store.size()
    .then((size) => {
        console.log('Current buffer size:', size);
    });
```

### Clearing the Buffer

```typescript
store.clear()
    .then(() => console.log('Buffer cleared.'));
```

## Configuration Options

- `host` (string): The hostname of the KDB+ instance.
- `port` (number): The port on which the KDB+ instance is listening.
- `tableName` (string, optional): The table name in the KDB+ instance (default: `FIXParser`).
- `parser` (IFIXParser): The FIX parser used for encoding/decoding messages.
- `maxBufferSize` (number, optional): The maximum size of the message buffer (default: 2500).
- `logger` (Logger, optional): The logger for logging events and errors.
- `onReady` (function): A callback function that is called when the KDB+ connection is ready.

## License
This project is licensed under a Commercial License - see the [LICENSE](https://gitlab.com/logotype/fixparser/-/blob/dev/LICENSE.md) file for details.