import path from 'node:path';
import { type BuildOptions, build as esbuild } from 'esbuild';

const baseConfig: BuildOptions = {
    bundle: true,
    minify: false,
    target: 'esnext',
    sourcemap: true,
    nodePaths: [path.join(__dirname, '../src')],
    external: [],
};

const nodeConfig: BuildOptions = {
    ...baseConfig,
    format: 'cjs',
    platform: 'node',
};

const main = async (): Promise<void> => {
    await esbuild({
        ...nodeConfig,
        outdir: path.join(__dirname, '../build/cjs'),
        entryPoints: [path.join(__dirname, './../src/index.ts')],
    });
    await esbuild({
        ...nodeConfig,
        format: 'esm',
        outExtension: {
            '.js': '.mjs',
        },
        outdir: path.join(__dirname, '../build/esm'),
        entryPoints: [path.join(__dirname, './../src/index.ts')],
    });
};

if (require.main === module) {
    main();
}
