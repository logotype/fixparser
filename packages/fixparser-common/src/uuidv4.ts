let randomIterator = 0;
const timeBasedRandom = (min: number, max: number): number => {
    const timeNow = Date.now() % 1000;
    randomIterator++;
    let x = timeNow ^ randomIterator;
    x ^= x << 21;
    x ^= x >>> 35;
    x ^= x << 4;
    const timeBasedRandom = Math.abs(x % (max - min + 1));
    return min + timeBasedRandom;
};

export const uuidv4 = (): string => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = timeBasedRandom(0, 15);
        const v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};
