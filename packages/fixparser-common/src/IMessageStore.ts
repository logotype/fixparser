/**
 * The `IMessageStore` interface defines the contract for a generic message store.
 * This store is used for storing, retrieving, updating, and removing items of type `T`.
 * It provides methods to add, retrieve, update, and remove items, along with checking
 * the store's size, capacity, and performing batch operations.
 *
 * @interface IMessageStore
 */
export interface IMessageStore<T> {
    /**
     * Adds an item of type `T` to the message store.
     *
     * @param {T} item - The item to add to the store.
     *
     * @returns {void}
     */
    add(item: T): void;

    /**
     * Retrieves an item of type `T` from the store by its sequence number (or any other identifier).
     *
     * @param {number} msgSequence - The sequence number of the item to retrieve.
     *
     * @returns {T | undefined} - The item if found, otherwise `undefined`.
     */
    getByMsgSequence(msgSequence: number): T | undefined;

    /**
     * Removes an item of type `T` from the store by its sequence number.
     *
     * @param {number} msgSequence - The sequence number of the item to remove.
     *
     * @returns {void}
     */
    remove(msgSequence: number): void;

    /**
     * Updates an item of type `T` in the store.
     *
     * @param {number} msgSequence - The sequence number of the item to update.
     * @param {T} item - The updated item.
     *
     * @returns {boolean} - Returns `true` if the item was updated successfully, `false` otherwise.
     */
    update(msgSequence: number, item: T): boolean;

    /**
     * Retrieves all items of type `T` from the store.
     *
     * @returns {T[]} - An array of all items in the store.
     */
    getAll(): T[];

    /**
     * Checks if an item with a given sequence number exists in the store.
     *
     * @param {number} msgSequence - The sequence number of the item to check.
     * @returns {boolean} - `true` if the item exists, `false` otherwise.
     */
    exists(msgSequence: number): boolean;

    /**
     * Returns the current number of items stored in the message store.
     *
     * @returns {number} - The number of items in the store.
     */
    size(): number;

    /**
     * Resizes the message store's capacity.
     *
     * @param {number} newCapacity - The new maximum capacity for the store.
     * @returns {void}
     */
    resize(newCapacity: number): void;

    /**
     * Clears all items from the store, removing them permanently.
     *
     * @returns {void}
     */
    clear(): void;

    /**
     * Returns the maximum capacity of the message store. This indicates how many items
     * the store can hold before it reaches its limit.
     *
     * @returns {number} - The maximum capacity of the store.
     */
    getCapacity(): number;

    /**
     * Set the next message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns {number} - The next message sequence number.
     */
    setNextMsgSeqNum(nextMsgSeqNum: number): number;

    /**
     * Get the next message sequence number.
     *
     * @returns {number} - The next message sequence number.
     */
    getNextMsgSeqNum(): number;
}
