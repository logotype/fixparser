import type { Level } from './Level';

export type LogMessage = {
    level: Level;
    message: string;
    fix?: string;
    [key: string]: unknown;
};
