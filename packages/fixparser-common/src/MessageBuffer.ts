import type { IMessageStore } from './IMessageStore';

/**
 * A buffer that stores items of type `T` up to a specified maximum size.
 * Implements the IMessageStore interface for generic types.
 */
export class MessageBuffer<T> implements IMessageStore<T> {
    /**
     * A number representing the next expected message sequence number.
     * @private
     */
    private nextMsgSeqNum = 1;

    /**
     * An array holding the items in the buffer.
     * @private
     */
    private buffer: T[] = [];

    /**
     * The maximum capacity of the buffer.
     * @private
     */
    private maxBufferSize: number;

    constructor(maxBufferSize = 2500) {
        this.maxBufferSize = maxBufferSize;
    }

    /**
     * Adds a new item to the buffer.
     * If the buffer is full, the oldest item is removed to make space for the new one.
     *
     * @param {T} item - The item to add to the buffer.
     * @returns {void}
     */
    public add(item: T): void {
        if (this.buffer.length === this.maxBufferSize) {
            this.buffer.pop();
        }
        this.buffer.unshift(item);
    }

    /**
     * Retrieves an item from the buffer by its sequence number (or any other identifier).
     *
     * @param {number} msgSequence - The sequence number of the item to retrieve.
     * @returns {T | undefined} The item if found, or `undefined` if not found.
     */
    public getByMsgSequence(msgSequence: number): T | undefined {
        const index: number = this.buffer.findIndex((item: any) => item.messageSequence === msgSequence);
        if (index > -1) {
            return this.buffer[index];
        }
        return undefined;
    }

    /**
     * Removes an item from the buffer by its sequence number.
     *
     * @param {number} msgSequence - The sequence number of the item to remove.
     * @returns {void}
     */
    public remove(msgSequence: number): void {
        const index: number = this.buffer.findIndex((item: any) => item.messageSequence === msgSequence);
        if (index > -1) {
            this.buffer.splice(index, 1);
        }
    }

    /**
     * Updates an item in the buffer.
     *
     * @param {number} msgSequence - The sequence number of the item to update.
     * @param {T} item - The updated item.
     * @returns {boolean} - Returns `true` if the item was updated successfully, `false` otherwise.
     */
    public update(msgSequence: number, item: T): boolean {
        const index: number = this.buffer.findIndex(
            (existingItem: any) => existingItem.messageSequence === msgSequence,
        );
        if (index > -1) {
            this.buffer[index] = item;
            return true;
        }
        return false;
    }

    /**
     * Retrieves all items from the buffer.
     *
     * @returns {T[]} - An array of all items in the buffer.
     */
    public getAll(): T[] {
        return this.buffer;
    }

    /**
     * Checks if an item with a given sequence number exists in the buffer.
     *
     * @param {number} msgSequence - The sequence number of the item to check.
     * @returns {boolean} - `true` if the item exists, `false` otherwise.
     */
    public exists(msgSequence: number): boolean {
        return this.buffer.some((item: any) => item.messageSequence === msgSequence);
    }

    /**
     * Gets the current size of the buffer (the number of items it contains).
     *
     * @returns {number} The number of items currently in the buffer.
     */
    public size(): number {
        return this.buffer.length;
    }

    /**
     * Resizes the buffer's capacity.
     *
     * @param {number} newCapacity - The new maximum capacity for the buffer.
     * @returns {void}
     */
    public resize(newCapacity: number): void {
        this.maxBufferSize = newCapacity;
        // If the buffer is larger than the new capacity, trim it.
        if (this.buffer.length > this.maxBufferSize) {
            this.buffer = this.buffer.slice(0, this.maxBufferSize);
        }
    }

    /**
     * Clears all items from the buffer.
     *
     * @returns {void}
     */
    public clear(): void {
        this.buffer = [];
    }

    /**
     * Gets the maximum capacity of the buffer.
     *
     * @returns {number} The maximum number of items the buffer can hold.
     */
    public getCapacity(): number {
        return this.maxBufferSize;
    }

    /**
     * Set the next message sequence number.
     *
     * @param nextMsgSeqNum - The next message sequence number.
     * @returns {number} - The next message sequence number.
     */
    public setNextMsgSeqNum(nextMsgSeqNum: number): number {
        if (nextMsgSeqNum <= 0) {
            throw new Error('Message sequence number must be positive.');
        }
        this.nextMsgSeqNum = nextMsgSeqNum;
        return this.nextMsgSeqNum;
    }

    /**
     * Get the next message sequence number.
     *
     * @returns {number} - The next message sequence number.
     */
    public getNextMsgSeqNum(): number {
        return this.nextMsgSeqNum;
    }
}
