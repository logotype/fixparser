export interface IMessage {
    messageSequence: number;
    encode: (separator: string) => string;
}
