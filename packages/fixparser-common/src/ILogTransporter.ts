import type { LogMessage } from './LogMessage';

/**
 * A LogTransporter interface for external log storage solutions.
 * @example
 * const consoleTransport = new ConsoleLogTransport({ format: 'console' as ConsoleFormat });
 * const logger = new Logger({ name: 'TestLogger', level: 'info', transport: consoleTransport });
 *
 * const message: LogMessage = { level: 'info', message: 'This is an info message' };
 * logger.log(message);
 */
export interface ILogTransporter {
    /**
     * Initializes the transporter with necessary configuration options.
     * @param config The configuration object for the transporter (e.g. connection info, credentials).
     */
    configure(config: Record<string, any>): void;

    /**
     * Sends a log message to the external service or storage.
     * @param log The log message to be sent.
     */
    send(log: LogMessage): Promise<void>;

    /**
     * Flushes any pending log messages if the transporter uses buffering, and closes any open connections if needed.
     */
    flush(): Promise<void>;

    /**
     * Closes the transporter, cleaning up resources (e.g. closing connections).
     */
    close(): Promise<void>;

    /**
     * Returns the current status of the transporter (e.g., connected, closed, etc.).
     * This can be useful for diagnostic purposes.
     */
    status(): string;
}
