/**
 * Logger output level options.
 *
 * - 'info': General informational messages that highlight the progress or state of the application.
 * - 'warn': Warnings about potential issues that don’t necessarily disrupt the program’s functionality.
 * - 'error': Critical issues that indicate something has gone wrong and may require attention.
 * - 'silent': No log messages are displayed or recorded, effectively suppressing all output.
 *
 * @public
 */
export type Level = 'info' | 'warn' | 'error' | 'silent';
