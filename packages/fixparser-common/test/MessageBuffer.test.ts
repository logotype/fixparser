import { FIXParser } from '../../fixparser/src/FIXParser';
import type { Message } from '../../fixparser/src/message/Message';
import { testMessages } from '../../fixparser/test/test-messages';
import type { IMessageStore } from '../src/IMessageStore';
import { MessageBuffer } from '../src/MessageBuffer';

const NUM_TEST_MSG = testMessages.length;
const parser = new FIXParser();
const createMockMessage = (index: number, messageSequence?: number): Message => {
    const messages: Message[] = parser.parse(testMessages[index].fix);
    if (messageSequence) {
        messages[0].messageSequence = messageSequence;
    }
    return messages[0];
};

describe('MessageBuffer', () => {
    let buffer: IMessageStore<Message>;

    beforeEach(() => {
        buffer = new MessageBuffer();
    });

    describe('add', () => {
        it('should add a message to the buffer', () => {
            const message = createMockMessage(1);
            buffer.add(message);

            expect(buffer.size()).toBe(1);
        });

        it('should add multiple messages, and keep the most recent at the front', () => {
            const message1 = createMockMessage(1, 100);
            const message2 = createMockMessage(2, 200);

            buffer.add(message1);
            buffer.add(message2);

            expect(buffer.size()).toBe(2);
            expect(buffer.getByMsgSequence(100)).toBe(message1);
            expect(buffer.getByMsgSequence(200)).toBe(message2);
        });

        it('should maintain the buffer size limit (buffer.getCapacity())', () => {
            const maxMessages = 3000;
            for (let i = 0; i < maxMessages + 1; i++) {
                buffer.add(createMockMessage(i % NUM_TEST_MSG));
            }

            // Buffer should have a maximum size of buffer.getCapacity()
            expect(buffer.size()).toBe(buffer.getCapacity());
        });

        it('should remove the oldest message when the buffer reaches buffer.getCapacity()', () => {
            const maxMessages = 3000;
            for (let i = 0; i < maxMessages + 1; i++) {
                buffer.add(createMockMessage(i % NUM_TEST_MSG, i));
            }

            // After adding the 2501st message, the first message (message 0) should be removed
            expect(buffer.getByMsgSequence(0)).toBeUndefined();
            expect(buffer.size()).toBe(buffer.getCapacity());
        });
    });

    describe('getByMsgSequence', () => {
        it('should return the message when it exists in the buffer', () => {
            const message = createMockMessage(1, 123);
            buffer.add(message);

            const retrievedMessage = buffer.getByMsgSequence(123);
            expect(retrievedMessage).toBe(message);
        });

        it('should return null when the message is not found in the buffer', () => {
            const message = createMockMessage(1, 321);
            buffer.add(message);

            const retrievedMessage = buffer.getByMsgSequence(111);
            expect(retrievedMessage).toBeUndefined();
        });
    });

    describe('size', () => {
        it('should return the correct size of the buffer', () => {
            expect(buffer.size()).toBe(0);

            const message1 = createMockMessage(1);
            buffer.add(message1);
            expect(buffer.size()).toBe(1);

            const message2 = createMockMessage(2);
            buffer.add(message2);
            expect(buffer.size()).toBe(2);
        });
    });

    describe('clear', () => {
        it('should clear all messages from the buffer', () => {
            buffer.add(createMockMessage(1));
            buffer.add(createMockMessage(2));

            expect(buffer.size()).toBe(2);

            buffer.clear();
            expect(buffer.size()).toBe(0);
            expect(buffer.getByMsgSequence(1)).toBeUndefined();
            expect(buffer.getByMsgSequence(2)).toBeUndefined();
        });
    });
});
