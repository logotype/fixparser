import { uuidv4 } from '../src/uuidv4';

describe('uuidv4', () => {
    it('should return a string', () => {
        const result = uuidv4();
        expect(typeof result).toBe('string');
    });

    it('should match the UUID v4 format', () => {
        const result = uuidv4();
        const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/;
        expect(result).toMatch(regex);
    });

    it('should have the correct version in the 13th character (v4)', () => {
        const result = uuidv4();
        expect(result[14]).toBe('4');
    });

    it('should have the correct variant in the 19th character', () => {
        const result = uuidv4();
        const validVariants = ['8', '9', 'a', 'b'];
        expect(validVariants).toContain(result[19]);
    });
});
