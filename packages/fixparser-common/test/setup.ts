import MockDate from 'mockdate';
import { LicenseManager } from '../../fixparser/src/licensemanager/LicenseManager';
MockDate.set(1629064307365);

// TODO: remove mock once mjs files has been published correctly...
jest.mock('../../../node_modules/openpgp/dist/openpgp.mjs', () => ({}));
jest.mock('../../fixparser/src/licensemanager/LicenseManager');
export const mockLicense = LicenseManager.validateLicense as jest.Mock;
mockLicense.mockReturnValue(true);
