import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    globals: {
        __PACKAGE_VERSION__: true,
        __BUILD_TIME__: true,
        __RELEASE_INFORMATION__: true,
    },
    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: ['**/src/**/*.ts', '!**/node_modules/**', '!**/spec/**'],
    coverageDirectory: 'test-reports',
    coverageReporters: ['text', 'text-summary', 'cobertura', 'lcovonly'],
    reporters: [
        'default',
        [
            'jest-junit',
            {
                outputDirectory: 'test-reports',
                outputName: 'jest-junit.xml',
            },
        ],
    ],
    setupFilesAfterEnv: ['<rootDir>/test/setup-textencoder.ts', '<rootDir>/test/setup.ts'],
    maxWorkers: 4,
    maxConcurrency: 4,
    verbose: true,
    forceExit: true,
    restoreMocks: true,
    testEnvironment: 'node',
    transform: {
        '^.+\\.(t|j)sx?$': '@swc/jest',
    },
    moduleNameMapper: {
        '^fixparser-plugin-log-console(.*)$': '<rootDir>/../../packages/fixparser-plugin-log-console/src',
        '^fixparser-common(.*)$': '<rootDir>/../../packages/fixparser-common/src',
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
export default config;
