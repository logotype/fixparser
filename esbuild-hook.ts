import * as Module from 'node:module';
import { type TransformOptions, transformSync } from 'esbuild';
import * as sourceMapSupport from 'source-map-support';

interface CacheEntry {
    url: string;
    code: string;
    map: string | null;
}

const cache: Record<string, CacheEntry> = {};

function esbuildHook(code: string, filepath: string): string {
    try {
        const options: TransformOptions = {
            target: 'node20',
            sourcemap: 'both',
            loader: 'ts',
            format: 'cjs',
            sourcefile: filepath,
        };

        const result = transformSync(code, options);

        cache[filepath] = {
            url: filepath,
            code: result.code,
            map: result.map,
        };

        return result.code;
    } catch (error) {
        console.error(`Error transforming file ${filepath}:`, error);
        throw error;
    }
}

function retrieveFile(pathOrUrl: string): string | null {
    const file = cache[pathOrUrl];
    return file ? file.code : null;
}

sourceMapSupport.install({
    environment: 'node',
    retrieveFile,
});

const defaultLoader = Module._extensions['.js'];

Module._extensions['.ts'] = (mod: NodeModule, filename: string) => {
    if (filename.includes('node_modules')) {
        return defaultLoader(mod, filename);
    }

    const defaultCompile = mod._compile;
    mod._compile = (code: string) => {
        mod._compile = defaultCompile;
        return mod._compile(esbuildHook(code, filename), filename);
    };

    defaultLoader(mod, filename);
};
