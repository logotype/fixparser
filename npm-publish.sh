#!/bin/bash

# Check if a directory argument is provided
if [ -z "$1" ]; then
  echo "Error: Please provide the package directory."
  echo "Usage: $0 <package_directory> [next]"
  exit 1
fi

PACKAGE_DIR="$1"
PUBLISH_TAG="$2"  # If 'next' is provided, it will be used as the tag

# Change directory to the provided package directory
cd "$PACKAGE_DIR" || { echo "Error: Failed to enter directory $PACKAGE_DIR"; exit 1; }

# If no .npmrc is included in the repo, generate a temporary one that is configured to publish to the public NPM registry
if [[ ! -f .npmrc ]]; then
  echo "//registry.npmjs.org/:_authToken=$NODE_AUTH_TOKEN" > ~/.npmrc
  echo "Created .npmrc with auth token..."
fi

# Install required tools
apt-get update
apt-get install -y jq

# Extract values from package.json
NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
NPM_VERSIONS=$(npm view ${NPM_PACKAGE_NAME} versions --json)
NPM_PREVIOUS_VERSION=$(echo $NPM_VERSIONS | jq -r 'map(select(test("^[0-9]+\\.[0-9]+\\.[0-9]+$"))) | sort_by(split(".") | map(tonumber)) | .[-2]')
NPM_PREVIOUS_PREVIEW_VERSION=$(echo $NPM_VERSIONS | jq -r '.[] | select(test("-"))' | head -n 1 | tail -n 1)

# Check if the previous version was found
if [ -z "$NPM_PREVIOUS_VERSION" ]; then
  echo "Failed to fetch previous version"
  exit 1
fi

# Check if the previous preview version was found
if [ -z "$NPM_PREVIOUS_PREVIEW_VERSION" ]; then
  echo "Failed to fetch previous preview version"
  exit 1
fi

echo "Current version on npm is ${NPM_PACKAGE_NAME}@${NPM_PREVIOUS_VERSION}"

# Function to handle the build, format, and publish process
publish_package() {
  # Build and publish the package
  ./../../node_modules/turbo/bin/turbo build
  npx @biomejs/biome format --write .

  if [ "$PUBLISH_TAG" == "next" ]; then
    npm version "${NPM_PACKAGE_VERSION}-${CI_COMMIT_SHA}"
    npm publish --tag next
    echo "Successfully published ${NPM_PACKAGE_NAME}@${NPM_PACKAGE_VERSION}-${CI_COMMIT_SHA} to the NPM registry with tag 'next'."
    npm deprecate ${NPM_PACKAGE_NAME}@${NPM_PREVIOUS_PREVIEW_VERSION} "Please update ${NPM_PACKAGE_NAME} to latest version!"
    echo "Successfully deprecated previous version ${NPM_PACKAGE_NAME}@${NPM_PREVIOUS_PREVIEW_VERSION}..."
  else
    npm publish
    echo "Successfully published version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to the NPM registry."
    npm deprecate ${NPM_PACKAGE_NAME}@${NPM_PREVIOUS_VERSION} "Please update ${NPM_PACKAGE_NAME} to latest version!"
    echo "Successfully deprecated previous version ${NPM_PACKAGE_NAME}@${NPM_PREVIOUS_VERSION}..."
  fi
}

# Check if version already exists for 'next' tag or regular version
if [ "$PUBLISH_TAG" == "next" ]; then
  if ! npm show ${NPM_PACKAGE_NAME}@${NPM_PACKAGE_VERSION}-${CI_COMMIT_SHA} > /dev/null 2>&1; then
    publish_package
  else
    echo "Version ${NPM_PACKAGE_NAME}@${NPM_PACKAGE_VERSION}-${CI_COMMIT_SHA} has already been published with the 'next' tag."
  fi
else
  if ! npm show ${NPM_PACKAGE_NAME}@${NPM_PACKAGE_VERSION} > /dev/null 2>&1; then
    publish_package
  else
    echo "Version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} has already been published, so no new version has been published."
  fi
fi

