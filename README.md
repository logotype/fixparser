# fixparser
[![FIXParser Demo](https://gitlab.com/logotype/fixparser/-/raw/main/badges/fixparser-demo.svg)](https://fixparser.dev/examples)
[![Pipeline](https://gitlab.com/logotype/fixparser/badges/main/pipeline.svg)](https://gitlab.com/logotype/fixparser/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=logotype_fixparser&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=logotype_fixparser)
[![Coverage](https://gitlab.com/logotype/fixparser/badges/main/coverage.svg?job=test)](https://gitlab.com/logotype/fixparser/-/commits/main)
[![npm version](https://badge.fury.io/js/fixparser.svg)](https://www.npmjs.com/package/fixparser)
[![Downloads](https://img.shields.io/npm/dm/fixparser.svg)](https://www.npmjs.com/package/fixparser)

This is the TypeScript framework for working with FIX protocol messages. Compliant with FIX.Latest / FIX 5.0 SP2.

The Financial Information eXchange (FIX) protocol is an electronic communications protocol initiated in 1992 for international real-time exchange of information related to the securities transactions and markets.

Versions
------------------
<table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr>
<td style="width: 33.3333%;">Feature</td>
<td style="width: 33.3333%;">FIXParser</td>
<td style="width: 33.3333%;">FIXParser Pro</td>
</tr>
<tr>
<td style="width: 33.3333%;">Parse FIX messages</td>
<td style="width: 33.3333%;">●</td>
<td style="width: 33.3333%;">●</td>
</tr>
<tr>
<td style="width: 33.3333%;">Create FIX messages</td>
<td style="width: 33.3333%;"> </td>
<td style="width: 33.3333%;">●</td>
</tr>
<tr>
<td style="width: 33.3333%;">Encode FIX messages</td>
<td style="width: 33.3333%;"> </td>
<td style="width: 33.3333%;">●</td>
</tr>
<tr>
<td style="width: 33.3333%;">Remote connections</td>
<td style="width: 33.3333%;"> </td>
<td style="width: 33.3333%;">●</td>
</tr>
<tr>
<td style="width: 33.3333%;">FIX Server</td>
<td style="width: 33.3333%;"> </td>
<td style="width: 33.3333%;">●</td>
</tr>
</tbody>
</table>

Features
--------
+ Modern, written in Typescript
+ Parse and create FIX messages
+ Connect over TCP/WebSocket, FIX-over-TLS as client or server
+ FIX Session support (Logon, Logout, Heartbeat, etc)
+ Decode message as FIX JSON
+ Fast, single-digit microsecond performance
+ Extendable with plug-ins for logging and message persistence
+ Validation (checksum and body length), includes per-field FIX specification in parsed message
+ Supports various separators/start of headers (e.g. 0x01, ^ and |)
+ Clean and lightweight code
+ Supports both node.js and browser environments (`import { FIXParser } from 'fixparser/FIXParserBrowser';`)
+ Supported FIX versions: FIX.Latest, FIXT.1.1, FIX.5.0SP2, FIX.5.0SP1, FIX.5.0, FIX.4.4, FIX.4.3 and FIX.4.2 (supports encoding/decoding but not validation of older versions such as FIX.2.7 through FIX.4.1)

[FIXParser Pro](https://fixparser.dev)
-----------

The FIXParser Pro version can be purchased at [fixparser.dev](https://fixparser.dev).
A license is valid for 1 year, includes updates and enables all functionality of the fixparser library.

Quick start
-----------

Install with `npm install fixparser`.

Parse a FIX message:

```typescript
import { FIXParser, Message } from 'fixparser';
const fixParser: FIXParser = new FIXParser();
const messages: Message[] = fixParser.parse('8=FIX.4.2|9=51|35=0|34=703|49=ABC|52=20100130-10:53:40.830|56=XYZ|10=249|');
```

```json
{
  "fixVersion": "FIX.4.2",
  "description": "Heartbeat",
  "messageType": "0",
  "messageSequence": 703,
  "data": [
    {
      "tag": 8,
      "value": "FIX.4.2",
      "name": "BeginString",
      "description": "Identifies beginning of new message and protocol version. ALWAYS FIRST FIELD IN MESSAGE. (Always unencrypted)\nValid values:\nFIXT.1.1",
      "type": {
        "name": "String",
        "description": "Alpha-numeric free format strings, can include any character or punctuation except the delimiter. All String fields are case sensitive (i.e. morstatt != Morstatt).",
        "added": "FIX.4.2"
      },
      "category": null,
      "section": null,
      "enumeration": null,
      "validated": false
    },
    ... // Rest of tags...
  ],
  "structure": [
    {
      "id": "1024",
      "added": "FIX.2.7",
      "presence": "required",
      "data": {
        "type": "component",
        "id": "1024",
        "updatedEP": "271",
        "added": "FIX.4.0",
        "category": "Session",
        "updated": "FIX.Latest",
        "abbrName": "Hdr",
        "name": "StandardHeader",
        "fieldRef": [
          {
            "id": "8",
            "added": "FIX.4.0",
            "presence": "required",
            "description": "FIXT.1.1 (Always unencrypted, must be first field in message)",
            "name": "BeginString"
          },
          {
            "id": "9",
            "added": "FIX.4.0",
            ... // Rest of spec...
      ]
    }
  ],
  "bodyLengthValid": true,
  "checksumValid": true,
  "checksumValue": "249",
  "checksumExpected": "249",
  "bodyLengthValue": 51,
  "bodyLengthExpected": 51
}
```

Validate a FIX message:

```typescript
import { FIXParser, Message } from 'fixparser';
const fixParser: FIXParser = new FIXParser();
const message: Message = fixParser.parse('8=FIXT.1.1|9=77|35=A|34=702|1128=10|52=20100130-10:52:40.663|56=XYZ|95=4|96=1234|108=60|98=0|10=209|')[0];
console.log(message.validate());
```

The response from `.validate()` is an array containing any validation errors. In this case the field `SenderCompID<49>` is missing.

```typescript
[
  {
    field: {
      id: '49',
      added: 'FIX.4.0',
      presence: 'required',
      description: '(Always unencrypted)',
      name: 'SenderCompID'
    },
    name: 'SenderCompID',
    tag: 49,
    value: null,
    error: 'Missing field SenderCompID'
  }
]
```

Parse a FIX message as JSON, including nested Repeating Groups:

```typescript
import { FIXParser, Message } from 'fixparser';
const fixParser: FIXParser = new FIXParser();
const messages: Message[] = fixParser.parse('8=FIXT.1.1|9=176|35=W|34=4567|49=SENDER|56=TARGET|52=20160802-21:14:38.717|453=3|448=DEU|447=B|452=1|802=1|523=A1|803=10|448=104317|447=H|452=83|448=GSI|447=B|452=4|2376=23|802=1|523=C3|803=10|10=034|');
console.log(message[0].toFIXJSON());
```

```json
{
  "Header": {
    "BeginString": "FIXT11",
    "BodyLength": 176,
    "MsgSeqNum": 4567,
    "MsgType": "MarketDataSnapshotFullRefresh",
    "SenderCompID": "SENDER",
    "SendingTime": "20160802-21:14:38.717",
    "TargetCompID": "TARGET"
  },
  "Body": {
    "NoPartyIDs": [
      {
        "PartyID": "DEU",
        "PartyIDSource": "BIC",
        "PartyRole": "ExecutingFirm",
        "NoPartySubIDs": [
          {
            "PartySubID": "A1",
            "PartySubIDType": "SecuritiesAccountNumber"
          }
        ]
      },
      {
        "PartyID": "104317",
        "PartyIDSource": "CSDParticipant",
        "PartyRole": "ClearingAccount"
      },
      {
        "PartyID": "GSI",
        "PartyIDSource": "BIC",
        "PartyRole": "ClearingFirm",
        "PartyRoleQualifier": "23",
        "NoPartySubIDs": [
          {
            "PartySubID": "C3",
            "PartySubIDType": "SecuritiesAccountNumber"
          }
        ]
      }
    ]
  },
  "Trailer": {
    "CheckSum": "034"
  }
}
```

**FIXParser Pro** Create a FIX message:

```typescript
import {
    FIXParser,
    Field,
    Fields,
    Message,
    Messages,
    Side,
    OrderTypes,
    HandlInst,
    TimeInForce,
    EncryptMethod,
    LicenseManager
} from 'fixparser';

// NOTE: This feature requires a FIXParser Pro license
void LicenseManager.setLicenseKey('<your license here>');

const fixParser: FIXParser = new FIXParser();
const order: Message = fixParser.createMessage(
    new Field(Fields.MsgType, Messages.NewOrderSingle),
    new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
    new Field(Fields.SenderCompID, 'SENDER'),
    new Field(Fields.SendingTime, fixParser.getTimestamp()),
    new Field(Fields.TargetCompID, 'TARGET'),
    new Field(Fields.ClOrdID, '11223344'),
    new Field(Fields.HandlInst, HandlInst.AutomatedExecutionNoIntervention),
    new Field(Fields.OrderQty, '123'),
    new Field(Fields.TransactTime, fixParser.getTimestamp()),
    new Field(Fields.OrdType, OrderTypes.Market),
    new Field(Fields.Side, Side.Buy),
    new Field(Fields.Symbol, '123.HK'),
    new Field(Fields.TimeInForce, TimeInForce.Day)
);
console.log(order.encode('|'));
// 8=FIX.5.0SP2|9=129|35=D|34=1|49=SENDER|52=20221123-23:04:59.132|56=TARGET|11=11223344|21=1|38=123|60=20221123-23:04:59.132|40=1|54=1|55=123.HK|59=0|10=061|
```

**FIXParser Pro** Connect over TCP socket (as client):

```typescript
import { FIXParser, LicenseManager } from 'fixparser';

// NOTE: This feature requires a FIXParser Pro license
void LicenseManager.setLicenseKey('<your license here>');
const fixParser: FIXParser = new FIXParser();
fixParser.connect({
    host: 'localhost',
    port: 9878,
    protocol: 'tcp',
    sender: 'BANZAI',
    target: 'EXEC',
    fixVersion: 'FIXT.1.1', /* Set the default version of messages */
    logging: true,
    onReady: () => { /* Client is ready to connect */ },
    onOpen: () => { /* Connection is now open */ },
    onMessage: (message: Message) => { /* Received a FIX message */ },
    onError: (error?: Error) => { /* Some error occurred */ },
    onClose: () => { /* Disconnected from remote */ },
});
```

**FIXParser Pro** FIX Server:

```typescript
import { FIXServer, LicenseManager }  from 'fixparser/FIXServer';

// NOTE: This feature requires a FIXParser Pro license
void LicenseManager.setLicenseKey('<your license here>');

const fixServer: FIXServer = new FIXServer();
fixServer.createServer({
    host: 'localhost',
    port: 9878,
    protocol: 'tcp',
    sender: 'SERVER',
    target: 'CLIENT',
    onReady: () => { /* Server is ready */ },
    onOpen: () => { /* Received connection */ },
    onMessage: (message: Message) => { /* Received a FIX message */ },
    onError: (error?: Error) => { /* Some error occurred */ },
    onClose: () => { /* Client disconnected */ },
});
```

Performance
-----------
```bash
┌─────────────────────────────────┬───────────────┬──────────────┬──────────────┐
│ FIX Messages                    │ Messages/sec  │ Microseconds │ Milliseconds │
│ 200,000 iterations (same msg)   │ 479,616 msg/s │ 2.0850 μs    │ 0.0021 ms    │
│ 200,000 iterations (same msg)   │ 477,327 msg/s │ 2.0950 μs    │ 0.0021 ms    │
│ 200,000 iterations (random msg) │ 261,097 msg/s │ 3.8300 μs    │ 0.0038 ms    │
│ 200,000 iterations (same msg)   │ 422,833 msg/s │ 2.3650 μs    │ 0.0024 ms    │
│ 200,000 iterations (random msg) │ 262,123 msg/s │ 3.8150 μs    │ 0.0038 ms    │
│ 200,000 iterations (same msg)   │ 425,532 msg/s │ 2.3500 μs    │ 0.0024 ms    │
│ 200,000 iterations (random msg) │ 260,756 msg/s │ 3.8350 μs    │ 0.0038 ms    │
│ 200,000 iterations (same msg)   │ 422,833 msg/s │ 2.3650 μs    │ 0.0024 ms    │
│ 200,000 iterations (same msg)   │ 421,941 msg/s │ 2.3700 μs    │ 0.0024 ms    │
│ 200,000 iterations (same msg)   │ 421,053 msg/s │ 2.3750 μs    │ 0.0024 ms    │
└─────────────────────────────────┴───────────────┴──────────────┴──────────────┘
```
Apple MacBook Pro M4 Max (128 GB RAM), node 23.2.0, run with `npm run perf`.

Message format
--------------

The general format of a FIX message is a standard header followed by the message body fields and terminated with a standard trailer.

Each message is constructed of a stream of <tag>=<value> fields with a field delimiter between fields in the stream. Tags are of data type TagNum. All tags must have a value specified. Optional fields without values should simply not be specified in the FIX message. A Reject message is the appropriate response to a tag with no value.
Except where noted, fields within a message can be defined in any sequence (Relative position of a field within a message is inconsequential.) The exceptions to this rule are:

- General message format is composed of the standard header followed by the body followed by the standard trailer.
- The first three fields in the standard header are BeginString (tag #8) followed by BodyLength (tag #9) followed by MsgType (tag #35).
- The last field in the standard trailer is the CheckSum (tag #10).
- Fields within repeating data groups must be specified in the order that the fields are specified in the message definition within the FIX specification document. The NoXXX field where XXX is the field being counted specifies the number of repeating group instances that must immediately precede the repeating group contents.
- A tag number (field) should only appear in a message once. If it appears more than once in the message it should be considered an error with the specification document. The error should be pointed out to the FIX Global Technical Committee.

In addition, certain fields of the data type MultipleCharValue can contain multiple individual values separated by a space within the "value" portion of that field followed by a single "SOH" character (e.g. "18=2 9 C<SOH>" represents 3 individual values: '2', '9', and 'C'). Fields of the data type MultipleStringValue can contain multiple values that consists of string values separated by a space within the "value" portion of that field followed by a single "SOH" character (e.g. "277=AA I AJ<SOH>" represents 3 values: 'AA', 'I', 'AJ').

It is also possible for a field to be contained in both the clear text portion and the encrypted data sections of the same message. This is normally used for validation and verification. For example, sending the SenderCompID in the encrypted data section can be used as a rudimentary validation technique. In the cases where the clear text data differs from the encrypted data, the encrypted data should be considered more reliable. (A security warning should be generated).

Copyright
-------

Copyright © 2025 FIXParser Ltd. Released under Commercial license. Check [LICENSE.md](https://gitlab.com/logotype/fixparser/-/blob/main/LICENSE.md).
